<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">jappliedecology</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100205</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Journal of Applied Ecology</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Blackwell Publishing</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00218901</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">13652664</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">41318862</article-id>
         <article-categories>
            <subj-group>
               <subject>Modelling and management</subject>
               <subj-group>
                  <subject>REVIEW</subject>
               </subj-group>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Encounter data in resource management and ecology: pitfalls and possibilities</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Aidan</given-names>
                  <surname>Keane</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Julia P. G.</given-names>
                  <surname>Jones</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>E. J.</given-names>
                  <surname>Milner-Gulland</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>10</month>
            <year>2011</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">48</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">5</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40059626</issue-id>
         <fpage>1164</fpage>
         <lpage>1173</lpage>
         <permissions>
            <copyright-statement>© 2011 British Ecological Society</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:href="http://dx.doi.org/10.1111/j.1365-2664.2011.02034.x"
                   xlink:title="an external site"/>
         <abstract>
            <p>1. Simple indices based on the number of encounters with a study object are used throughout ecology, conservation and natural resource management (e.g. indices of abundance used in animal surveys or catch per unit effort (CPUE) data in fisheries management). All forms of encounter data arise through the interaction of two sets of behaviours: those of the data generators and those of the data collectors. Analyses of encounter data are prone to bias when these behaviours do not conform to the assumptions used to model them. 2. We review the use of CPUE indices derived from patrol data, which have been promoted for the study of rule-breaking in conservation, highlighting potential sources of bias and noting how similar problems have been tackled for other forms of encounter data. 3. We identify several issues that must be addressed for analyses of patrol data to provide useful information, including the definition of suitable measures of catch and effort, the choice of appropriate temporal and spatial scales, the provision of suitable incentives for ranger patrols and the recording of sufficient information to describe the spatial pattern of sampling. The same issues are also relevant to encounter data more generally. 4. Synthesis and applications. This review describes a common conceptual framework for understanding encounter data, based on the interactions that produce them. We anticipate that an appreciation of these commonalities will lead to improvements in the analysis of encounter data in several fields, by highlighting the existence of methodological approaches that could be more widely applied, and important characteristics of these data that have so far been neglected.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d80e201a1310">
            <mixed-citation id="d80e205" publication-type="other">
Abbot, J.I.O. &amp; Mace, R. (1999) Managing protected woodlands: fuelwood
collection and law enforcement in Lake Malawi National Park. Conservation
Biology, 13,418-421.</mixed-citation>
         </ref>
         <ref id="d80e218a1310">
            <mixed-citation id="d80e222" publication-type="other">
Akella, A.S. &amp; Canon, J.B. (2004). Strengthening the weakest link. Strategies for
improving the enforcement of environmental laws globally. CCG Reports.
Centre for Conservation and Governance at Conservation International.</mixed-citation>
         </ref>
         <ref id="d80e235a1310">
            <mixed-citation id="d80e239" publication-type="other">
Arcese, P., Hando, J. &amp; Campbell, K. (1995). Historical and present-day anti-
poaching efforts in Serengeti. In Sinclair, A.R.E &amp; Arcese, P. editors,
Serengeti II: Dynamics, Management, and Conservation of an Ecosystem,
pages 506-533. University of Chicago Press, Chicago &amp; London. 1st edition</mixed-citation>
         </ref>
         <ref id="d80e255a1310">
            <mixed-citation id="d80e259" publication-type="other">
Armsworth, P.R., Gaston, K.J., Hanley, N.D. &amp; Ruffell, R.J. (2009) Contrast-
ing approaches to statistical regression in ecology and economics. Journal of
Applied Ecology, 46, 265-268.</mixed-citation>
         </ref>
         <ref id="d80e273a1310">
            <mixed-citation id="d80e277" publication-type="other">
Arreguin-Sánchez, F. (1996) Catchability: a key parameter for fish stock assess-
ment. Reviews in Fish Biology and Fisheries, 6, 221-242.</mixed-citation>
         </ref>
         <ref id="d80e287a1310">
            <mixed-citation id="d80e291" publication-type="other">
Becker, G.S. (1968) Crime and punishment: an economic approach. Journal of
Political Economy, 76, 169-217.</mixed-citation>
         </ref>
         <ref id="d80e301a1310">
            <mixed-citation id="d80e305" publication-type="other">
Bibby, C.J., Burgess, N.D. &amp; Hill, D.A. (2000) Bird Census Techniques. Aca-
demic Press, London, UK.</mixed-citation>
         </ref>
         <ref id="d80e315a1310">
            <mixed-citation id="d80e319" publication-type="other">
Bigelow, K.A., Hampton, J. &amp; Miyabe, N. (2002) Application of a habitat-
based model to estimate effective longline fishing effort and relative abun-
dance of Pacific bigeye tuna (Thurmus obesus). Fisheries Oceanography, 11,
143-155.</mixed-citation>
         </ref>
         <ref id="d80e335a1310">
            <mixed-citation id="d80e339" publication-type="other">
Bonesi, L. &amp; Macdonald, D.W. (2004) Evaluation of sign surveys as a way to
estimate the relative abundance of American mink (Mustela vison ). Journal
of Zoology, 262, 65-72.</mixed-citation>
         </ref>
         <ref id="d80e352a1310">
            <mixed-citation id="d80e356" publication-type="other">
Bordalo-Machado, P. (2006) Fishing effort analysis and its potential to evalu-
ate stock size. Reviews in Fisheries Science, 14, 369-393.</mixed-citation>
         </ref>
         <ref id="d80e367a1310">
            <mixed-citation id="d80e371" publication-type="other">
BPAMP (2006). Ranger-based data collection. A reference guide and training
manual for protected area staff in Cambodia. Biodiversity and Protected
Areas Management Project.</mixed-citation>
         </ref>
         <ref id="d80e384a1310">
            <mixed-citation id="d80e388" publication-type="other">
Brashares, J.S. &amp; Sam, M. K. (2005) How much is enough? Estimating the
minimum sampling required for effective monitoring of African reserves.
Biodiversity and Conservation, 14, 2709-2722.</mixed-citation>
         </ref>
         <ref id="d80e401a1310">
            <mixed-citation id="d80e405" publication-type="other">
Buckland, S.T. (2001) Introduction to Distance Sampling: Estimating Abun-
dance of Biological Populations. Oxford University Press, Oxford, UK.</mixed-citation>
         </ref>
         <ref id="d80e415a1310">
            <mixed-citation id="d80e419" publication-type="other">
Buckland, S.T. (2006) Point-transect surveys for songbirds: robust methodolo-
gies. The Auk, 123, 345.</mixed-citation>
         </ref>
         <ref id="d80e429a1310">
            <mixed-citation id="d80e433" publication-type="other">
Burn, R.W. &amp; Underwood, F.M. (2000) Statistical aspects of sampling popula-
tions of forest elephants. Natural Resource Modeling, 13, 135-150.</mixed-citation>
         </ref>
         <ref id="d80e443a1310">
            <mixed-citation id="d80e447" publication-type="other">
Burton, M. (1999) An assessment of alternative methods of estimating the
effect of the ivory trade ban on poaching effort. Ecological Economics, 30,
93-106.</mixed-citation>
         </ref>
         <ref id="d80e461a1310">
            <mixed-citation id="d80e465" publication-type="other">
Candy, S.G. (2004) Modelling catch and effort data using generalised linear
models, the Tweedie distribution, random vessel effects and random stra-
tum-by-year effects. CCAMLR Science, 11, 59-80.</mixed-citation>
         </ref>
         <ref id="d80e478a1310">
            <mixed-citation id="d80e482" publication-type="other">
de Merode, E., Smith, K.H., Homewood, K., Pettifor, R., Rowcliffe, M. &amp;
Cowlishaw, G. (2007) The impact of armed conflict on protected-area effi-
cacy in Central Africa. Biology Letters, 3, 299-301.</mixed-citation>
         </ref>
         <ref id="d80e495a1310">
            <mixed-citation id="d80e499" publication-type="other">
Ehrlich, I. (1996) Crime, punishment, and the market for offenses. The Journal
of Economic Perspectives, 10, 43-67.</mixed-citation>
         </ref>
         <ref id="d80e509a1310">
            <mixed-citation id="d80e513" publication-type="other">
Ferraro, P. (2005) Corruption and conservation: the need for empirical analy-
ses. A response to Smith &amp; Walpole. Oryx, 39, 257-259.</mixed-citation>
         </ref>
         <ref id="d80e523a1310">
            <mixed-citation id="d80e527" publication-type="other">
Ferraro, P.J. &amp; Pattanayak, S.K. (2006) Money for nothing? A call for empirical
evaluation of biodiversity conservation investments. PLoS Biology, 4, 105.</mixed-citation>
         </ref>
         <ref id="d80e537a1310">
            <mixed-citation id="d80e541" publication-type="other">
Fewster, R.M., Southwell, C., Borchers, D.L., Buckland, S.T. &amp; Pople, A.R.
(2008) The influence of animal mobility on the assumption of uniform dis-
tances in aerial line-transect surveys. Wildlife Research, 35, 275.</mixed-citation>
         </ref>
         <ref id="d80e555a1310">
            <mixed-citation id="d80e559" publication-type="other">
Flores, O., Rossi, V. &amp; Mortier, F. (2009) Autocorrelation offsets zero-inflation
in models of tropical saplings density. Ecological Modelling, 220, 1797-1809.</mixed-citation>
         </ref>
         <ref id="d80e569a1310">
            <mixed-citation id="d80e573" publication-type="other">
Gaveau, D.L.A., Epting, J., Lyne, O., Linkie, M., Kumara, I., Kanninen, M. &amp;
Leader-Williams, N. (2009) Evaluating whether protected areas reduce trop-
ical deforestation in Sumatra. Journal of Biogeography, 36, 2165-2175.</mixed-citation>
         </ref>
         <ref id="d80e586a1310">
            <mixed-citation id="d80e590" publication-type="other">
Gavin, M.C., Solomon, J.N. &amp; Blank, S.G. (2010) Measuring and monitoring
illegal use of natural resources. Conservation Biology, 24, 89-100.</mixed-citation>
         </ref>
         <ref id="d80e600a1310">
            <mixed-citation id="d80e604" publication-type="other">
Gibson, C. (1995) Transforming rural hunters into conservationists: an assess-
ment of community-based wildlife management programs in Africa. World
Development, 23, 941-957.</mixed-citation>
         </ref>
         <ref id="d80e617a1310">
            <mixed-citation id="d80e621" publication-type="other">
Gray, M. &amp; Kalpers, J. (2005) Ranger based monitoring in the Virunga-Bwindi
region of east-central Africa: a simple data collection tool for park manage-
ment. Biodiversity and Conservation, 14, 2723-2741.</mixed-citation>
         </ref>
         <ref id="d80e634a1310">
            <mixed-citation id="d80e638" publication-type="other">
Handegard, N.O., Michalsen, K. &amp; Tjostheim, D. (2003) Avoidance behaviour
in cod (Gadus morhua) to a bottom-trawling vessel. Aquatic Living Resources,
16,265-270.</mixed-citation>
         </ref>
         <ref id="d80e652a1310">
            <mixed-citation id="d80e656" publication-type="other">
Harley, S.J., Myers, R.A. &amp; Dunn, A. (2001) Is catch-per-unit-effort propor-
tional to abundance? Canadian Journal of Fisheries and Aquatic Sciences, 58,
1760-1772.</mixed-citation>
         </ref>
         <ref id="d80e669a1310">
            <mixed-citation id="d80e673" publication-type="other">
Hart, T., Hart, J., Fimbel, C., Fimbel, R., Laurance, W.F., Oren, C., Struhsak-
er, T.T., Rosenbaum, H.C., Walsh, P.D., Razafindrakoto, Y., Vely, M. &amp;
DeSalle, R. (1997) Conservation and civil strife: two perspectives from Cen-
tral Africa. Conservation Biology, 11, 308-314.</mixed-citation>
         </ref>
         <ref id="d80e689a1310">
            <mixed-citation id="d80e693" publication-type="other">
Hilborn, R. (1985) Fleet dynamics and individual variation: why some people
catch more fish than others. Canadian Journal of Fisheries and Aquatic Sci-
ences, 42, 2-13.</mixed-citation>
         </ref>
         <ref id="d80e706a1310">
            <mixed-citation id="d80e710" publication-type="other">
Hilborn, R. &amp; Walters, C.J. (1992) Quantitative Fisheries Stock Assessment:
Choice, Dynamics, and Uncertainty. Chapman and Hall, London, UK &amp;
New York, USA.</mixed-citation>
         </ref>
         <ref id="d80e723a1310">
            <mixed-citation id="d80e727" publication-type="other">
Hilborn, R., Arcese, P., Borner, M., Hando, J., Hopcraft, G., Loibooki, M.,
Mduma, S. &amp; Sinclair, A.R.E. (2006) Effective enforcement in a conservation
area. Science, 314, 1266.</mixed-citation>
         </ref>
         <ref id="d80e740a1310">
            <mixed-citation id="d80e744" publication-type="other">
Hill, K., McMillan, G. &amp; Farina, R. (2003) Hunting-related changes in game
encounter rates from 1994 to 2001 in the Mbaracayu Reserve, Paraguay.
Conservation Biology, 17, 1312-1323.</mixed-citation>
         </ref>
         <ref id="d80e758a1310">
            <mixed-citation id="d80e762" publication-type="other">
Hoggarth, D.D., Abeyasekera, S., Arthur, R.I., Beddington, J.R., Burn, R.W.,
Halls, A.S. et al. (2006) Stock assessment for fishery management A frame-
work guide to the stock assessment tools of the Fisheries Management
Science Programme (FMSP). FAO Fisheries Technical Paper. No. 487.
FAO, Rome.</mixed-citation>
         </ref>
         <ref id="d80e781a1310">
            <mixed-citation id="d80e785" publication-type="other">
Holmern, T., Muya, J. &amp; Reskaft, E. (2007) Local law enforcement and illegal
bushmeat hunting outside the Serengeti National Park, Tanzania. Environ-
mental Conservation, 34, 55-63.</mixed-citation>
         </ref>
         <ref id="d80e798a1310">
            <mixed-citation id="d80e802" publication-type="other">
Hovgård, H. (1996) A two-step approach to estimating selectivity and fishing
power of research gill nets used in Greenland waters. Canadian Journal of
Fisheries and Aquatic Sciences, 53, 1007-1013.</mixed-citation>
         </ref>
         <ref id="d80e815a1310">
            <mixed-citation id="d80e819" publication-type="other">
Jachmann, H. (2002) Comparison of aerial counts with ground counts for large
African herbivores. Journal of Applied Ecology, 39, 841-852.</mixed-citation>
         </ref>
         <ref id="d80e829a1310">
            <mixed-citation id="d80e833" publication-type="other">
Jachmann, H. (2008) Monitoring law-enforcement performance in nine pro-
tected areas in Ghana. Biological Conservation, 141, 89-99.</mixed-citation>
         </ref>
         <ref id="d80e843a1310">
            <mixed-citation id="d80e847" publication-type="other">
Jachmann, H. &amp; Billiouw, M. (1997) Elephant poaching and law enforcement
in the central Luangwa Valley, Zambia. Journal of Applied Ecology, 34, 233-
244.</mixed-citation>
         </ref>
         <ref id="d80e861a1310">
            <mixed-citation id="d80e865" publication-type="other">
Jachmann, H. &amp; Jeffery, R.C.V. (1998). Monitoring illegal wildlife use and law
enforcement in African savanna rangelands. Wildlife Resource Monitoring
Unit.</mixed-citation>
         </ref>
         <ref id="d80e878a1310">
            <mixed-citation id="d80e882" publication-type="other">
Keane, A., Jones, J.P.G., Edwards-Jones, G. &amp; Milner-Gulland, E.J. (2008)
The sleeping policeman: understanding issues of enforcement and compli-
ance in conservation. Animal Conservation, 11, 75-82.</mixed-citation>
         </ref>
         <ref id="d80e895a1310">
            <mixed-citation id="d80e899" publication-type="other">
Keane, A., Ramarolahy, A.A., Jones, J.P.G. &amp; Milner-Gulland, E.J. (2011)
Evidence for the effects of environmental engagement and education on
knowledge of wildlife laws in Madagascar. Knowledge of wildlife laws in
Madagascar. Conservation Letters, 4(1), 55-63.</mixed-citation>
         </ref>
         <ref id="d80e915a1310">
            <mixed-citation id="d80e919" publication-type="other">
Kennedy, P. (2001) A Guide to Econometrics. MIT Press, Cambridge, USA.</mixed-citation>
         </ref>
         <ref id="d80e926a1310">
            <mixed-citation id="d80e930" publication-type="other">
Koslow, J., Kloser, R. &amp; Stanley, C. (1995) Avoidance of a camera system by a
deepwater fish, the orange roughy (Hoplostethus atlanticus ). Deep Sea
Research Part I: Oceanographic Research Papers, 42, 233-244.</mixed-citation>
         </ref>
         <ref id="d80e943a1310">
            <mixed-citation id="d80e947" publication-type="other">
Leader-Williams, N., Albon, S.D. &amp; Berry, P.S.M. (1990) Illegal exploitation
of black rhinoceros and elephant populations: patterns of decline, law
enforcement and patrol effort in Luangwa Valley, Zambia. Journal of
Applied Ecology,27, 1055-1087.</mixed-citation>
         </ref>
         <ref id="d80e964a1310">
            <mixed-citation id="d80e968" publication-type="other">
Leader-Williams, N. &amp; Milner-Gulland, E.J. (1993) Policies for the enforce-
ment of wildlife laws: the balance between detection and penalties in Lu-
angwa Valley, Zambia. Conservation Biology, 7, 611-617.</mixed-citation>
         </ref>
         <ref id="d80e981a1310">
            <mixed-citation id="d80e985" publication-type="other">
Legendre, P. (1993) Spatial autocorrelation: trouble or new paradigm? Ecology,
74, 1659-1673.</mixed-citation>
         </ref>
         <ref id="d80e995a1310">
            <mixed-citation id="d80e999" publication-type="other">
Lewison, R., Crowder, L., Read, A. &amp; Freeman, S. (2004) Understanding
impacts of fisheries bycatch on marine megafauna. Trends in Ecology &amp; Evo-
lution, 19, 598-604.</mixed-citation>
         </ref>
         <ref id="d80e1012a1310">
            <mixed-citation id="d80e1016" publication-type="other">
Lichstein, J.W., Simons, T.R., Shriner, S.A. &amp; Franzreb, K.E. (2002) Spatial
autocorrelation and autoregressive models in ecology. Ecological Mono-
graphs, 72, 445-463.</mixed-citation>
         </ref>
         <ref id="d80e1029a1310">
            <mixed-citation id="d80e1033" publication-type="other">
Lorenzen, K., Almeida, O., Arthur, R., Garaway, C. &amp; Khoa, S.N. (2006)
Aggregated yield and fishing effort in multispecies fisheries: an
empirical analysis. Canadian Journal of Fisheries and Aquatic Sciences, 63,
1334-1343.</mixed-citation>
         </ref>
         <ref id="d80e1049a1310">
            <mixed-citation id="d80e1053" publication-type="other">
MacKenzie, D.I., Nichols, J.D., Royle, J.A., Pollock, K.H., Bailey, L.L. &amp;
Hines, J.E. (2006) Occupancy Estimation and Modeling: Inferring Patterns
and Dynamics of Species Occurrence. Academic Press, Burlington &amp; San
Diego, USA &amp; London, UK.</mixed-citation>
         </ref>
         <ref id="d80e1070a1310">
            <mixed-citation id="d80e1074" publication-type="other">
Maddala, G.S. (1992) Introduction to Econometrics. Macmillan Publishing
Company, New York, USA.</mixed-citation>
         </ref>
         <ref id="d80e1084a1310">
            <mixed-citation id="d80e1088" publication-type="other">
Marchal, P., Andersen, B., Bromley, D., Iriondo, A., Mahevas, S., Quirijns, F.,
Rackham, B., Santurtun, M., Tien, N. &amp; Ulrich, C. (2006) Improving the
definition of fishing effort for important European fleets by accounting for
the skipper effect. Canadian Journal of Fisheries and Aquatic Sciences, 63,
510-533.</mixed-citation>
         </ref>
         <ref id="d80e1107a1310">
            <mixed-citation id="d80e1111" publication-type="other">
Martin, T.G., Wintle, B.A., Rhodes, J.R., Kuhnert, P.M., Field, S.A., Low-
Choy, S.J., Tyre, A.J. &amp; Possingham, H.P. (2005) Zero tolerance ecology:
improving ecological inference by modelling the source of zero observations.
Ecology Letters, 8, 1235-1246.</mixed-citation>
         </ref>
         <ref id="d80e1127a1310">
            <mixed-citation id="d80e1131" publication-type="other">
Maunder, M. &amp; Punt, A. (2004) Standardizing catch and effort data: a review
of recent approaches. Fisheries Research, 70, 141-159.</mixed-citation>
         </ref>
         <ref id="d80e1141a1310">
            <mixed-citation id="d80e1145" publication-type="other">
Maunder, M.N., Sibert, J.R., Fonteneau, A., Hampton, J., Kleiber, P. &amp; Har-
ley, S.J. (2006) Interpreting catch per unit effort data to assess the status of
individual stocks and communities. ICES Jounal of Marine Science, 63,
1373-1385.</mixed-citation>
         </ref>
         <ref id="d80e1161a1310">
            <mixed-citation id="d80e1165" publication-type="other">
McCullagh, P. &amp; Nelder, J.A. (1989) Generalized Linear Models, Second Edition
(Chapman &amp; Hall/CRC Monographs on Statistics &amp; Applied Probability),
2nd edn. Chapman and Hall/CRC, London, UK &amp; New York, USA.</mixed-citation>
         </ref>
         <ref id="d80e1179a1310">
            <mixed-citation id="d80e1183" publication-type="other">
McShane, T.O. &amp; McShane-Caluzi, E. (1984) Research for management in
Vwaza Marsh Game Reserve, Malawi. Conservation and Wildlife Manage-
ment in Africa (eds R.H.V. Bell &amp; E. McShane-Caluzi), pp. 137-143. U.S.
Peace Corps, Washington, D.C.</mixed-citation>
         </ref>
         <ref id="d80e1199a1310">
            <mixed-citation id="d80e1203" publication-type="other">
Milner-Gulland, E.J. &amp; Clayton, L. (2002) The trade in babirusas and wild pigs
in North Sulawesi, Indonesia. Ecological Economics, 42, 165-183.</mixed-citation>
         </ref>
         <ref id="d80e1213a1310">
            <mixed-citation id="d80e1217" publication-type="other">
Milner-Gulland, E.J. &amp; Rowcliffe, J.M. (2007) Conservation and Sustainable
Use: A Handbook of Techniques (Techniques in Ecology and Conservation ).
Oxford University Press, New York, USA.</mixed-citation>
         </ref>
         <ref id="d80e1230a1310">
            <mixed-citation id="d80e1234" publication-type="other">
Minami, M., Lennertcody, C., Gao, W. &amp; Romanverdesoto, M. (2007) Model-
ing shark bycatch: the zero-inflated negative binomial regression model with
smoothing. Fisheries Research, 84, 210-221.</mixed-citation>
         </ref>
         <ref id="d80e1247a1310">
            <mixed-citation id="d80e1251" publication-type="other">
Nishida, T. &amp; Chen, D. (2004) Incorporating spatial autocorrelation into the
general linear model with an application to the yellowfin tuna (Thunnus
albacares ) longline CPUE data. Fisheries Research, 70, 265-274.</mixed-citation>
         </ref>
         <ref id="d80e1264a1310">
            <mixed-citation id="d80e1268" publication-type="other">
Nyahongo, J.W., East, M.L., Mturi, F.A. &amp; Hofer, H. (2005) Benefits and costs
of illegal grazing and hunting in the Serengeti ecosystem. Environmental Con-
servation, 32, 326-332.</mixed-citation>
         </ref>
         <ref id="d80e1282a1310">
            <mixed-citation id="d80e1286" publication-type="other">
O'Brien, T.G. &amp; Kinnaird, M.F. (2003) Caffeine and Conservation. Science,
300, 587.</mixed-citation>
         </ref>
         <ref id="d80e1296a1310">
            <mixed-citation id="d80e1300" publication-type="other">
Patterson, K.R. (1998) Assessing fish stocks when catches are misreported:
model, simulation tests, and application to cod, haddock, and whiting in the
ICES area. ICES Journal of Marine Science, 55, 878-891.</mixed-citation>
         </ref>
         <ref id="d80e1313a1310">
            <mixed-citation id="d80e1317" publication-type="other">
Polacheck, T. (1988) Analyses of the relationship between the distribution of
searching effort, tuna catches, and dolphin sightings within individual purse
seine cruises. Fishery Bulletin, 86, 351-366.</mixed-citation>
         </ref>
         <ref id="d80e1330a1310">
            <mixed-citation id="d80e1334" publication-type="other">
Poulsen, M.K. &amp; Luanglath, K. (2005) Projects come, projects go: lessons from
participatory monitoring in southern Laos. Biodiversity and Conservation,
14, 2591-2610.</mixed-citation>
         </ref>
         <ref id="d80e1347a1310">
            <mixed-citation id="d80e1351" publication-type="other">
Pullin, A.S. &amp; Knight, T.M. (2001) Effectiveness in conservation practice:
pointers from Medicine and Public Health. Conservation Biology, 15, 50-54.</mixed-citation>
         </ref>
         <ref id="d80e1361a1310">
            <mixed-citation id="d80e1365" publication-type="other">
Punt, A. (2000) Standardization of catch and effort data in a spatially-struc-
tured shark fishery. Fisheries Research, 45, 129-145.</mixed-citation>
         </ref>
         <ref id="d80e1376a1310">
            <mixed-citation id="d80e1380" publication-type="other">
Rist, J. (2007) Bushmeat Catch per Unit Effort in space and time: a monitoring
tool for bushmeat hunting. PhD thesis, Imperial College London, London.</mixed-citation>
         </ref>
         <ref id="d80e1390a1310">
            <mixed-citation id="d80e1394" publication-type="other">
Rist, J., Rowcliffe, M., Cowlishaw, G. &amp; Milner-Gulland, E.J. (2008) Evaluat-
ing measures of hunting effort in a bushmeat system. Biological Conserva-
tion, 141,2086-2099.</mixed-citation>
         </ref>
         <ref id="d80e1407a1310">
            <mixed-citation id="d80e1411" publication-type="other">
Rist, J., Milner-Gulland, E.J., Cowlishaw, G. &amp; Rowcliffe, M. (2010) Hunter
reporting of catch per unit effort as a monitoring tool in a Bushmeat-Har-
vesting system. Conservation Biology, 24, 489-499.</mixed-citation>
         </ref>
         <ref id="d80e1424a1310">
            <mixed-citation id="d80e1428" publication-type="other">
Robinson, E.J.Z., Kumar, A.M. &amp; Albers, H.J. (2010) Protecting developing
countries' forests: enforcement in theory and practice. Journal of Natural
Resources Policy Research, 2, 25-38.</mixed-citation>
         </ref>
         <ref id="d80e1441a1310">
            <mixed-citation id="d80e1445" publication-type="other">
Rosenstock, S.S., Anderson, D.R., Giesen, K.M., Leukering, T. &amp; Carter,
M.F. (2002) Landbird counting techniques: current practices and an alterna-
tive. The Auk, 119,46-53.</mixed-citation>
         </ref>
         <ref id="d80e1458a1310">
            <mixed-citation id="d80e1462" publication-type="other">
Sangster, G. (1998) Gear performance and catch comparison trials between a
single trawl and a twin rigged gear. Fisheries Research, 36, 15-26.</mixed-citation>
         </ref>
         <ref id="d80e1473a1310">
            <mixed-citation id="d80e1477" publication-type="other">
Sauer, J.R., Peteijohn, B.G. &amp; Link, W.A. (1994) Observer differences in the
North American breeding bird survey. The Auk, 111, 50-62.</mixed-citation>
         </ref>
         <ref id="d80e1487a1310">
            <mixed-citation id="d80e1491" publication-type="other">
Steventon, J. (2002). CyberTracker.</mixed-citation>
         </ref>
         <ref id="d80e1498a1310">
            <mixed-citation id="d80e1502" publication-type="other">
Stuart-Hill, G., Diggle, R., Munali, B., Tagg, J. &amp; Ward, D. (2005) The event
book system: a community-based natural resource monitoring system from
Namibia. Biodiversity and Conservation, 14, 2611-2631.</mixed-citation>
         </ref>
         <ref id="d80e1515a1310">
            <mixed-citation id="d80e1519" publication-type="other">
Sutherland, W. (2004) The need for evidence-based conservation. Trends in
Ecology &amp; Evolution, 19, 305-308.</mixed-citation>
         </ref>
         <ref id="d80e1529a1310">
            <mixed-citation id="d80e1533" publication-type="other">
Suuronen, P. (1997) Avoidance and escape behaviour by herring encountering
midwater trawls. Fisheries Research, 29, 13-24.</mixed-citation>
         </ref>
         <ref id="d80e1543a1310">
            <mixed-citation id="d80e1547" publication-type="other">
Thomas, L. (1996) Monitoring long-term population change: why are there so
many analysis methods? Ecology, 77, 49-58.</mixed-citation>
         </ref>
         <ref id="d80e1558a1310">
            <mixed-citation id="d80e1562" publication-type="other">
Thompson, S.K. &amp; Seber, G.A.F. (1996) Adaptive Sampling, 1st edn. John
Wiley &amp; Sons, New York, USA.</mixed-citation>
         </ref>
         <ref id="d80e1572a1310">
            <mixed-citation id="d80e1576" publication-type="other">
Travers, H. (2009) Levelling the Playing Field: The Effects of Institutional Con-
trols on Common Pool Resource Extraction. Master's thesis. Imperial College
London, London, UK.</mixed-citation>
         </ref>
         <ref id="d80e1589a1310">
            <mixed-citation id="d80e1593" publication-type="other">
Vabø, R. (2002) The effect of vessel avoidance of wintering Norwegian spring
spawning herring. Fisheries Research, 58, 59-77.</mixed-citation>
         </ref>
         <ref id="d80e1603a1310">
            <mixed-citation id="d80e1607" publication-type="other">
Walters, C. (2003) Folly and fantasy in the analysis of spatial catch rate data.
Canadian Journal of Fisheries and Aquatic Sciences, 60, 1433-1436.</mixed-citation>
         </ref>
         <ref id="d80e1617a1310">
            <mixed-citation id="d80e1621" publication-type="other">
Williams, B.K., Nichols, J.D. &amp; Conroy, M.J. (2002) Analysis and Management
of Animal Populations, 1st edn. Academic Press, San Diego, USA &amp; London,
UK.</mixed-citation>
         </ref>
         <ref id="d80e1634a1310">
            <mixed-citation id="d80e1638" publication-type="other">
Zuberbühler, K. (1997) Diana monkey long-distance calls: messages for
conspecifics and predators. Animal Behaviour, 53, 589-604.</mixed-citation>
         </ref>
         <ref id="d80e1649a1310">
            <mixed-citation id="d80e1653" publication-type="other">
Zuur, A.F., Ieno, E.N., Walker, N.J., Saveliev, A.A. &amp; Smith, G.M. (2009)
Mixed Effects Models and Extensions in Ecology with R. Springer, New
York.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
