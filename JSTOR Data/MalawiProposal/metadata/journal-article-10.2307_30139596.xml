<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">amerjagriecon</journal-id>
         <journal-id journal-id-type="jstor">j100057</journal-id>
         <journal-title-group>
            <journal-title>American Journal of Agricultural Economics</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Blackwell Publishing, Inc.</publisher-name>
         </publisher>
         <issn pub-type="ppub">00029092</issn>
         <issn pub-type="epub">14678276</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">30139596</article-id>
         <article-id pub-id-type="pub-doi">10.1111/j.1467-8276.2007.01163.x</article-id>
         <title-group>
            <article-title>Welfare Effects of Technological Convergence in Processed Food Industries</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Jun</given-names>
                  <surname>Ruan</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Munisamy</given-names>
                  <surname>Gopinath</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Steven</given-names>
                  <surname>Buccola</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>5</month>
            <year>2008</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">90</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id>i30139583</issue-id>
         <fpage>447</fpage>
         <lpage>462</lpage>
         <permissions>
            <copyright-statement>Copyright 2008 American Agricultural Economics Association</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/30139596"/>
         <abstract>
            <p>We develop a monopolistic competition model to investigate effects of international technological convergence on factor rewards, output composition, and welfare. Comparative static analysis indicates technological convergence improves the follower's--but impairs the leader's-international competitiveness. The leader's welfare improves unambiguously; the follower's welfare depends on the relative strength of convergence's income and terms-of-trade effects. We use data from seventeen food industries in thirty countries, 1993-2001, to test these analytical predictions. Evidence of convergence is found in thirteen of seventeen industries. Convergence lifts followers' relative wages and global valueadded shares. Followers benefit from convergence's positive income effect. Leaders benefit from higher terms of trade.</p>
         </abstract>
         <kwd-group>
            <kwd>competitiveness</kwd>
            <kwd>processed food industries</kwd>
            <kwd>productivity growth</kwd>
            <kwd>technological convergence</kwd>
            <kwd>welfare</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d53e216a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d53e223" publication-type="other">
Bernard and
Jones 1996b</mixed-citation>
            </p>
         </fn>
         <fn id="d53e233a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d53e240" publication-type="other">
Hall et al.'s (1988)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d53e246" publication-type="other">
WDI 2005</mixed-citation>
            </p>
         </fn>
         <fn id="d53e253a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d53e260" publication-type="other">
ISIC 1542</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d53e266" publication-type="other">
ISIC 1532</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d53e272" publication-type="other">
1541</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d53e279" publication-type="other">
1543-44</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d53e285" publication-type="other">
1543-44</mixed-citation>
            </p>
         </fn>
         <fn id="d53e292a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d53e299" publication-type="other">
ISIC 1553</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d53e305" publication-type="other">
ISIC 1514
and 1544</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d53e314" publication-type="other">
ISIC 1544</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d53e330a1310">
            <mixed-citation id="d53e334" publication-type="other">
Anderson, J.E., and E. van Wincoop. 2003. "Gravity
with Gravitas: A Solution to the Border Puz-
zle." American Economic Review 93:170-92.</mixed-citation>
         </ref>
         <ref id="d53e347a1310">
            <mixed-citation id="d53e351" publication-type="other">
Ark, B.V., and D. Pilat. 1993. "Productivity Levels in
Germany, Japan, and the United States: Differ-
ences and Causes." Brookings Papers on Eco-
nomic Activity: Microeconomics: 0:1-48.</mixed-citation>
         </ref>
         <ref id="d53e367a1310">
            <mixed-citation id="d53e371" publication-type="other">
Baumol, W.J., R.R. Nelson, and E.N. Wolff. 1994.
Convergence of Productivity: Cross-National
Studies and Historical Evidence. Oxford: Ox-
ford University Press.</mixed-citation>
         </ref>
         <ref id="d53e387a1310">
            <mixed-citation id="d53e391" publication-type="other">
Bernard, A.B., and C.I. Jones. 1996a. "Comparing
Apples to Oranges: Productivity Convergence
and Measurement across Industries and Coun-
tries." American Economic Review 86:1216-38.</mixed-citation>
         </ref>
         <ref id="d53e408a1310">
            <mixed-citation id="d53e412" publication-type="other">
---.1996b. "Technology and Convergence." Eco-
nomic Journal 106:1037-44.</mixed-citation>
         </ref>
         <ref id="d53e422a1310">
            <mixed-citation id="d53e426" publication-type="other">
Bhagwati, J., A. Panagariya, and T.N. Srinivasan.
2004. "The Muddles over Outsourcing." Jour-
nal of Economic Perspectives 18(4):93-114.</mixed-citation>
         </ref>
         <ref id="d53e439a1310">
            <mixed-citation id="d53e443" publication-type="other">
Chan-Kang, C., S. Buccola, and J. Kerkvliet. 1999.
"Investment and Productivity in Canadian and
U.S. Food Manufacturing." Canadian Journal
ofAgricultural Economics 47:105-118.</mixed-citation>
         </ref>
         <ref id="d53e459a1310">
            <mixed-citation id="d53e463" publication-type="other">
Coe, D.T., and E. Helpman. 1995. "International
R&amp;D Spillovers." European Economic Review
39:903-48.</mixed-citation>
         </ref>
         <ref id="d53e476a1310">
            <mixed-citation id="d53e480" publication-type="other">
Coe, D.T., E. Helpman, and A.W. Hoffmaister.
1997. "North-South R&amp;D Spillovers." Eco-
nomic Journal 107:134-49.</mixed-citation>
         </ref>
         <ref id="d53e493a1310">
            <mixed-citation id="d53e499" publication-type="other">
Cohen, W.M., and R.C. Levin. 1989. "Empiri-
cal Studies of Innovation and Market Struc-
ture." In R. Schmalensee and R.D. Willig, eds.
Handbook of Industrial Organization, Vol. II,
Chap. 18. New York: North-Holland.</mixed-citation>
         </ref>
         <ref id="d53e519a1310">
            <mixed-citation id="d53e523" publication-type="other">
Crego, A., D. Larson, R. Butzer, and Y. Mundlak.
1998. A New Database of Investment and Cap-
ital for Agriculture and Manufacture. World
Bank, Working Paper No. 2013, Washington
DC.</mixed-citation>
         </ref>
         <ref id="d53e542a1310">
            <mixed-citation id="d53e546" publication-type="other">
Feenstra, R.C. 2004. Advanced International Trade:
Theory and Evidence. Princeton and Oxford:
Princeton University Press.</mixed-citation>
         </ref>
         <ref id="d53e559a1310">
            <mixed-citation id="d53e563" publication-type="other">
Gopinath, M. 2003. "Cross-country Differences in
Technology: The Case of the Food Process-
ing Industry." Canadian Journal ofAgricultural
Economics 51:97-107.</mixed-citation>
         </ref>
         <ref id="d53e579a1310">
            <mixed-citation id="d53e585" publication-type="other">
Grossman, G., and E. Helpman. 1990. "Trade, In-
novation, and Growth." American Economic
Review 80:86-91.</mixed-citation>
         </ref>
         <ref id="d53e598a1310">
            <mixed-citation id="d53e602" publication-type="other">
Hall, B.H., C. Cummins, E.S. Laderman, and J.
Mondy. 1988. The R&amp;D Master File Documen-
tation. National Bureau of Economic Research,
Technical Working Paper No. 72, Cambridge,
MA.</mixed-citation>
         </ref>
         <ref id="d53e621a1310">
            <mixed-citation id="d53e625" publication-type="other">
Harrigan, J. 1997. "Technology, Factor Supplies,
and International Specialization: Estimating
the Neoclassical Model." American Economic
Review 87:475-94.</mixed-citation>
         </ref>
         <ref id="d53e642a1310">
            <mixed-citation id="d53e646" publication-type="other">
---. 1999. "Estimation of Cross-Country Differ-
ences in Industry Production Functions." Jour-
nal of International Economics 47:267-93.</mixed-citation>
         </ref>
         <ref id="d53e659a1310">
            <mixed-citation id="d53e663" publication-type="other">
Helpman, E. 1997. R&amp;D and Productivity: The
International Connection. National Bureau of
Economic Research, Working Paper No. 6101,
Cambridge, MA.</mixed-citation>
         </ref>
         <ref id="d53e679a1310">
            <mixed-citation id="d53e683" publication-type="other">
International Trade Statistics. 2005. The World
Trade Organization, Geneva.</mixed-citation>
         </ref>
         <ref id="d53e693a1310">
            <mixed-citation id="d53e697" publication-type="other">
Jones, R.W., and J.A. Scheinkman. 1977. "The Rel-
evance of the Two-Sector Production Model in
Trade Theory." Journal of Political Economy
85:909-35.</mixed-citation>
         </ref>
         <ref id="d53e713a1310">
            <mixed-citation id="d53e717" publication-type="other">
Keller, W. 2001. International Technology Diffu-
sion. National Bureau of Economic Research,
Working Paper No. 8573, Cambridge, MA.</mixed-citation>
         </ref>
         <ref id="d53e730a1310">
            <mixed-citation id="d53e734" publication-type="other">
Krugman, P.R. 1980. "Scale Economies, Product
Differentiation, and the Pattern of Trade."
American Economic Review 70:950-59.</mixed-citation>
         </ref>
         <ref id="d53e748a1310">
            <mixed-citation id="d53e752" publication-type="other">
---. 1990. "A 'Technology Gap' Model of Inter-
national Trade." In Rethinking International
Trade. Cambridge: MIT Press, pp. 152-64.</mixed-citation>
         </ref>
         <ref id="d53e765a1310">
            <mixed-citation id="d53e769" publication-type="other">
Melton, B.E., and W.E. Huffman. 1995. "Beef and
Pork Packing Costs and Input Demands: Ef-
fects of Unionization and Technology." Ameri-
can Journal of Agricultural Economics 77:471-
85.</mixed-citation>
         </ref>
         <ref id="d53e788a1310">
            <mixed-citation id="d53e792" publication-type="other">
Miller, S.M., and M.P. Upadhyay. 2002. "Total Fac-
tor Productivity and the Convergence Hy-
pothesis." Journal of Macroeconomics 24:267-
86.</mixed-citation>
         </ref>
         <ref id="d53e808a1310">
            <mixed-citation id="d53e812" publication-type="other">
Morrison Paul, C. 2000. "Modeling and Measuring
Productivity in the Agri-Food Sector: Trends,
Causes, and Effects." Canadian Journal ofAgri-
cultural Economics 48:217-40.</mixed-citation>
         </ref>
         <ref id="d53e828a1310">
            <mixed-citation id="d53e832" publication-type="other">
Samuelson, P.A. 2004. "Where Ricardo and Mill
Rebut and Confirm Arguments of Main-
stream Economists Supporting Globalization."
Journal of Economic Perspectives 18(3):135-
46.</mixed-citation>
         </ref>
         <ref id="d53e851a1310">
            <mixed-citation id="d53e855" publication-type="other">
Trefler, D. 1993. "International Factor Price Differ-
ences: Leontief was Right!" Journal of Political
Economy 101:961-87.</mixed-citation>
         </ref>
         <ref id="d53e869a1310">
            <mixed-citation id="d53e873" publication-type="other">
United Nations. 2005. UNIDO INDSTAT4 2005 In-
dustrial Statistics Database at the 4-digit level of
ISIC (Rev. 3), Vienna.</mixed-citation>
         </ref>
         <ref id="d53e886a1310">
            <mixed-citation id="d53e890" publication-type="other">
U.S. Department of Agriculture, Economic Re-
search Service. 2005. Foreign Agricultural
Trade of the United States, 2005, Washington
DC.</mixed-citation>
         </ref>
         <ref id="d53e906a1310">
            <mixed-citation id="d53e910" publication-type="other">
U.S. Department of Commerce, Office of Health
and Consumer Goods. 2006. 2006 Processed
Food Outlook, Washington DC.</mixed-citation>
         </ref>
         <ref id="d53e923a1310">
            <mixed-citation id="d53e927" publication-type="other">
World Bank. 2005. World Development Indicators,
2005, Washington DC.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
