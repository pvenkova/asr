<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.cttq43v4</book-id>
      <subj-group>
         <subject content-type="call-number">S494.5.I5C663 2012</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Agricultural innovations</subject>
         <subj-group>
            <subject content-type="lcsh">Developing countries</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Green Revolution</subject>
         <subj-group>
            <subject content-type="lcsh">Developing countries</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Food supply</subject>
         <subj-group>
            <subject content-type="lcsh">Developing countries</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Food security</subject>
         <subj-group>
            <subject content-type="lcsh">Developing countries</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Business</subject>
      </subj-group>
      <book-title-group>
         <book-title>One Billion Hungry</book-title>
         <subtitle>Can We Feed the World?</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>CONWAY</surname>
               <given-names>GORDON</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib2">
            <name name-style="western">
               <surname>Wilson</surname>
               <given-names>Katy</given-names>
            </name>
         </contrib>
         <role content-type="author">with</role>
      </contrib-group>
      <contrib-group>
         <contrib contrib-type="foreword-author" id="contrib3">
            <role>Foreword by</role>
            <name name-style="western">
               <surname>Shah</surname>
               <given-names>Rajiv</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>16</day>
         <month>10</month>
         <year>2012</year>
      </pub-date>
      <isbn content-type="ppub">9780801451331</isbn>
      <isbn content-type="epub">9780801466083</isbn>
      <publisher>
         <publisher-name>Cornell University Press</publisher-name>
         <publisher-loc>Ithaca; London</publisher-loc>
      </publisher>
      <edition>1</edition>
      <permissions>
         <copyright-year>2012</copyright-year>
         <copyright-holder>Cornell University</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7591/j.cttq43v4"/>
      <abstract abstract-type="short">
         <p>Hunger is a daily reality for a billion people. More than six decades after the technological discoveries that led to the Green Revolution aimed at ending world hunger, regular food shortages, malnutrition, and poverty still plague vast swaths of the world. And with increasing food prices, climate change, resource inequality, and an ever-increasing global population, the future holds further challenges.</p>
         <p>In<italic>One Billion Hungry</italic>, Sir Gordon Conway, one of the world's foremost experts on global food needs, explains the many interrelated issues critical to our global food supply from the science of agricultural advances to the politics of food security. He expands the discussion begun in his influential<italic>The Doubly Green Revolution: Food for All in the Twenty-First Century</italic>, emphasizing the essential combination of increased food production, environmental stability, and poverty reduction necessary to end endemic hunger on our planet.</p>
         <p>Beginning with a definition of hunger and how it is calculated, and moving through issues topically both detailed and comprehensive, each chapter focuses on specific challenges and solutions, ranging in scope from the farmer's daily life to the global movement of food, money, and ideas. Drawing on the latest scientific research and the results of projects around the world, Conway addresses the concepts and realities of our global food needs: the legacy of the Green Revolution; the impact of market forces on food availability; the promise and perils of genetically modified foods; agricultural innovation in regard to crops, livestock, pest control, soil, and water; and the need to both adapt to and slow the rate of climate change.<italic>One Billion Hungry</italic>will be welcomed by all readers seeking a multifacted understanding of our global food supply, food security, international agricultural development, and sustainability.</p>
      </abstract>
      <counts>
         <page-count count="456"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq43v4.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq43v4.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq43v4.3</book-part-id>
                  <title-group>
                     <title>Foreword</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Shah</surname>
                           <given-names>Rajiv</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>ix</fpage>
                  <abstract abstract-type="extract">
                     <p>By the late 1990s, global food security had mostly fallen off the world’s agenda. The success of the Green Revolution had helped hundreds of millions of people in Latin America and Asia avoid a life of extreme hunger and poverty. Governments—developed and developing alike—assumed this success would spread and cut their investments in agriculture, allowing them to turn their attention elsewhere.</p>
                     <p>But while many had lost sight of the importance of agricultural development, Gordon Conway stayed focused. In his book, <italic>A Doubly Green Revolution</italic>, published in 1997, Gordon issued a pressing call for the development community to recommit</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq43v4.4</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq43v4.5</book-part-id>
                  <title-group>
                     <title>Editorial Note</title>
                  </title-group>
                  <fpage>xv</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq43v4.6</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Acute and Chronic Crises</title>
                  </title-group>
                  <fpage>3</fpage>
                  <abstract abstract-type="extract">
                     <p>
                        <italic>Hunger</italic> (from the Old English <italic>hungor</italic>) is an evocative, old Germanic word meaning “unease or pain caused by lack of food, craving appetite, debility from lack of food.”² In the developed countries it is a feeling of slight discomfort when a meal is late or missed. By contrast, in the developing countries hunger is a chronic problem. Television images convey the realities of hunger—emaciated and starving children—in war-torn countries or in the aftermath of droughts, floods, or other calamities. Yet for a billion people—men, women, and children—hunger in the developing countries is a day-to-day occurrence, both</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq43v4.7</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>What Is Hunger?</title>
                  </title-group>
                  <fpage>21</fpage>
                  <abstract abstract-type="extract">
                     <p>Homer, in his epic poem <italic>The Odyssey,</italic> recounts how Odysseus and his companions have resisted the lure of the Sirens, sailed safely between Scylla and Charybdis, and have come to the island of Thrinacia where the “Sun-god’s cattle and plump sheep graze.” Odysseus has been warned the animals are not to be harmed, but his companions succumb to the temptation. “To die of hunger,” declares Eurylochus “is the bitterest of fates.” They kill the cattle and feast. No sooner have they set sail again than Zeus sends a hurricane as a punishment. All perish except for Odysseus.</p>
                     <p>Today, there are</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq43v4.8</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>The Green Revolution</title>
                  </title-group>
                  <fpage>41</fpage>
                  <abstract abstract-type="extract">
                     <p>The Green Revolution was the product of two genes—one in wheat in Japan and the other in rice in China—that in the hands of visionary scientists and administrators transformed agriculture, not only in the developing countries but throughout the world. In retrospect it is easy to see the revolution, for revolution it was, as a series of obvious logical steps. But the journey was somewhat circuitous. Roger Thurow and Scott Kilman in their book <italic>Enough</italic> say, “From the beginning, the Green Revolution was the unintended outcome of unlikely work by determined individuals.”² In this chapter I address the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq43v4.9</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>The Political Economy of Food Security</title>
                  </title-group>
                  <fpage>63</fpage>
                  <abstract abstract-type="extract">
                     <p>Hunger has always been political. The practice of agriculture evolved from hunting and gathering in several locations around the world, ten to fifteen thousand years ago, and it is likely that there were disputes between hunter-gatherer communities over access to different parts of the landscape.² Agriculture stimulated the creation of settled communities, and undoubtedly this intensified conflict—over the best arable land and for rights to water and grazing. Disputes would have been acute when harvests were poor.</p>
                     <p>Populations grew and the first states were organized around the provision of food, and especially the control of, and access to, water</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq43v4.10</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>A Doubly Green Revolution</title>
                  </title-group>
                  <fpage>85</fpage>
                  <abstract abstract-type="extract">
                     <p>The concept of a <italic>theory of change</italic> has become common currency in philanthropic organizations such as the Rockefeller Foundation and the Bill &amp; Melinda Gates Foundation. In essence, it is a simple conceptual tool that tries to answer the question: Given a certain goal, what is the route that is most likely to get there? A much quoted (reputedly Chinese) proverb “Give a man a fish and you feed him for a day. Teach a man to fish and you feed him for a lifetime,” sums up a theory of change, not just for a particular objective but for development</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq43v4.11</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Sustainable Intensification</title>
                  </title-group>
                  <fpage>103</fpage>
                  <abstract abstract-type="extract">
                     <p>As I argued in Chapter 1 there is little likelihood of significantly more arable land becoming available; yet we know that we have to approximately double food production by 2050. For the past fifty years the only significant increase in arable land has been for crops such as oil palm and soybean on cleared rainforest or the Brazilian Cerrado. For the future the only solution to the food security problem is to get more production out of the existing land, but to do it in a way that is sustainable. This will depend on human ingenuity, in particular in harnessing</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq43v4.12</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Appropriate Technology</title>
                  </title-group>
                  <fpage>125</fpage>
                  <abstract abstract-type="extract">
                     <p>In the first chapter I described the conditions under which a woman farmer in Kenya labors to feed herself and her children. She has to contend with numerous pests and diseases, has poor access to good seeds and fertilizers, and is afflicted by periodic drought. Yet, as I described, it does not have to be this way. Appropriate technologies and other interventions can provide answers; she can grow enough on her one hectare to feed her family and also grow some crops to provide cash for schooling and health care. With the right technologies she and her family can escape</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq43v4.13</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>Creating Markets</title>
                  </title-group>
                  <fpage>143</fpage>
                  <abstract abstract-type="extract">
                     <p>The success of a Doubly Green Revolution does not just depend on appropriate technologies but also, as the original Green Revolution demonstrated, on the larger policy and institutional context. New technologies will be produced and farmers will take them up only if there is an enabling environment that ensures investors and farmers get a reasonable return. More broadly, the poor will escape hunger and poverty, and the virtuous cycle will take off and accelerate, only if the conditions are right.</p>
                     <p>In this chapter I address these questions:</p>
                     <p>- What is the nature of an enabling environment and why is it</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq43v4.14</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>Designer Crops</title>
                  </title-group>
                  <fpage>167</fpage>
                  <abstract abstract-type="extract">
                     <p>Plant and animal breeding is an art, nearly as old as agriculture itself. The early farmers selected seed from vigorous, high-yielding plants and used them for sowing in the following season. By degrees, wild grasses were transformed into domestic cereals—wheat, barley, maize, rice—the process of selection creating distinctive varieties adapted to local conditions and needs. Farmers soon came to look for promising mutants and natural hybrids. Bread wheat, a natural cross between emmer wheat and a wild goat grass that arose about 7,000 years ago somewhere to the southwest of the Caspian Sea, was recognized and cultivated by</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq43v4.15</book-part-id>
                  <title-group>
                     <label>10</label>
                     <title>The Livestock Revolution</title>
                  </title-group>
                  <fpage>187</fpage>
                  <abstract abstract-type="extract">
                     <p>The Green Revolution was not the only agricultural transformation of the twentieth century. Less well understood and celebrated was a revolutionary growth in livestock production, distinguished by the great diversity of products it embraced: meat, milk, cheese, and eggs from, variously, cattle, sheep, goats, pigs, poultry, and other more exotic creatures.</p>
                     <p>There are nearly 4,000 species of domesticated animals, but only twelve dominate global livestock production.² In Africa, livestock owners rely on cattle, sheep, goats, donkeys, and camels; in central Asia, they keep horses, cattle, goats, sheep, donkeys, and, in some areas, Bactrian camels. Yaks dominate production in the highlands</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq43v4.16</book-part-id>
                  <title-group>
                     <label>11</label>
                     <title>Farmers as Innovators</title>
                  </title-group>
                  <fpage>207</fpage>
                  <abstract abstract-type="extract">
                     <p>Much of the content of previous chapters has represented a “top down” approach to agricultural development. Farmers are seen as recipients of inputs and knowledge, yet as anyone who has worked with farmers in developing countries is aware, they are skilled and knowledgeable and often highly innovative. Where development has not worked, it is often because their needs are not appreciated and their knowledge is ignored.</p>
                     <p>In this chapter I address the following questions:</p>
                     <p>- What is Agroecosystem Analysis (AEA) and how did it arise?</p>
                     <p>- How can AEA and other participatory techniques help farmers to become better analysts?</p>
                     <p>-</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq43v4.17</book-part-id>
                  <title-group>
                     <label>12</label>
                     <title>Controlling Pests</title>
                  </title-group>
                  <fpage>227</fpage>
                  <abstract abstract-type="extract">
                     <p>Pests, pathogens, and weeds are the most visible of threats to stable and resilient food production.² Just how much crop and livestock loss they cause is largely guesswork; estimates range between 10 and 40 percent. But in some situations the potential losses can be considerably higher. Much depends on the nature of the crop: Where a premium is placed on the quality of the harvested product—for example, cotton, fruits, and vegetables—even a small pest or pathogen population can cause the farmer serious financial loss. Staple crops are not in this category, but the increasing intensity of their cultivation</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq43v4.18</book-part-id>
                  <title-group>
                     <label>13</label>
                     <title>Rooted in the Soil</title>
                  </title-group>
                  <fpage>246</fpage>
                  <abstract abstract-type="extract">
                     <p>Lucius Columella, writing in the century after Marcus Varro (quoted at the beginning of Chapter 6), was another Roman landowner who clearly understood the basis of sustainable agriculture: the soil.</p>
                     <p>It is relatively easy to sow a good seed in a pot of well-structured, organic soil, place it in a greenhouse, protected from pests and pathogens, water and fertilize the growing plant when necessary, and be rewarded with a phenomenal crop. Needless to say, conditions on a farm are far from this “ideal” environment. Individual farmers can do much to improve their situation—by buying high-quality seed, applying fertilizer, manure,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq43v4.19</book-part-id>
                  <title-group>
                     <label>14</label>
                     <title>Sustained by Water</title>
                  </title-group>
                  <fpage>268</fpage>
                  <abstract abstract-type="extract">
                     <p>Water is as important for the productivity of plants as is the provision of a good soil structure and sufficient nutrients. A wheat grain may contain up to 25 percent water; a potato 80 percent. For rice, in particular, water is crucial; a gram of grain can require as much as 1,400 grams of water for its production.² Not surprisingly, water stress during growth results in major yield reductions for most crops.</p>
                     <p>In this chapter I address the following questions:</p>
                     <p>- What are the agricultural needs for water?</p>
                     <p>- What are the conflicts over water use and the damaging effects</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq43v4.20</book-part-id>
                  <title-group>
                     <label>15</label>
                     <title>Adapting to Climate Change</title>
                  </title-group>
                  <fpage>286</fpage>
                  <abstract abstract-type="extract">
                     <p>Global climate change has been largely driven by the activities of the industrialized countries. Yet its most severe consequences will be and, indeed, are already being felt by the developing countries. Moreover, it is the poor of those countries who, in part because of their poverty, are most vulnerable. If left unchecked, climate change will increase hunger and cause further deterioration of the environmental resources on which sustainable agriculture depends.</p>
                     <p>In this chapter I will address the following questions:</p>
                     <p>What do we know and not know about climate change?</p>
                     <p>What will be the most serious consequences for agriculture and rural</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq43v4.21</book-part-id>
                  <title-group>
                     <label>16</label>
                     <title>Reducing Greenhouse Gases</title>
                  </title-group>
                  <fpage>306</fpage>
                  <abstract abstract-type="extract">
                     <p>While agriculture is a major victim of climate change, it has considerable potential, albeit largely unrealized, to help mitigate climate change by reducing greenhouse gas (GHG) emissions and by providing low carbon energy. In this chapter I describe this potential, illustrated with examples of where this is beginning to be harnessed.</p>
                     <p>Over the past hundred years the bulk of GHG emissions have come from industrialization and urbanization. The developed countries have been mostly responsible with, in recent de cades, a growing contribution from newly emerging countries such as China, India, and Brazil that are undergoing rapid industrialization. The less-developed countries,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq43v4.22</book-part-id>
                  <title-group>
                     <label>17</label>
                     <title>Conclusion:</title>
                     <subtitle>Can We Feed the World?</subtitle>
                  </title-group>
                  <fpage>329</fpage>
                  <abstract abstract-type="extract">
                     <p>I am by nature an optimist (it is probably in my genes), so my answer to the question posed here and in the book as a whole is “yes.” But it is a qualified yes. We will be able to feed the one billion chronically hungry and get to a food secure world in 2050, but only if we focus our efforts, provide sufficient aid and public and private investment, harness new technologies, remove trade restrictions, create appropriate enabling environments and governance, including efficient, noncorrupt and fair markets, and vigorously tackle climate change. This is a tall order, especially because</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq43v4.23</book-part-id>
                  <title-group>
                     <title>References</title>
                  </title-group>
                  <fpage>349</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq43v4.24</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>429</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq43v4.25</book-part-id>
                  <title-group>
                     <title>[Illustrations]</title>
                  </title-group>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
