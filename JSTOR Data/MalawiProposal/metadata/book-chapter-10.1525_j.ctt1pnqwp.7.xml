<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1pnqwp</book-id>
      <subj-group>
         <subject content-type="call-number">HD1019.Z8 S55 2004</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Land tenure</subject>
         <subj-group>
            <subject content-type="lcsh">Senegal</subject>
            <subj-group>
               <subject content-type="lcsh">Sine-Saloum</subject>
               <subj-group>
                  <subject content-type="lcsh">History</subject>
               </subj-group>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Serer (African people)</subject>
         <subj-group>
            <subject content-type="lcsh">Government relations</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Sine-Saloum (Senegal)</subject>
         <subj-group>
            <subject content-type="lcsh">Social conditions</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Sine-Saloum (Senegal)</subject>
         <subj-group>
            <subject content-type="lcsh">Economic conditions</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Acculturation</subject>
         <subj-group>
            <subject content-type="lcsh">Senegal</subject>
            <subj-group>
               <subject content-type="lcsh">Case studies</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>History</subject>
      </subj-group>
      <book-title-group>
         <book-title>The State Must Be Our Master of Fire</book-title>
         <subtitle>How Peasants Craft Culturally Sustainable Development in Senegal</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>GALVAN</surname>
               <given-names>DENNIS C.</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>18</day>
         <month>05</month>
         <year>2004</year>
      </pub-date>
      <isbn content-type="ppub">9780520227781</isbn>
      <isbn content-type="epub">9780520929425</isbn>
      <publisher>
         <publisher-name>University of California Press</publisher-name>
         <publisher-loc>BERKELEY; LOS ANGELES; LONDON</publisher-loc>
      </publisher>
      <edition>1</edition>
      <permissions>
         <copyright-year>2004</copyright-year>
         <copyright-holder>Regents of the University of California</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.1525/j.ctt1pnqwp"/>
      <abstract abstract-type="short">
         <p>Over several centuries, the Serer of the Siin region of Senegal developed a complex system of land tenure that resulted in a stable rural society, productive agriculture, and a well-managed ecosystem. Dennis Galvan tells the story of what happened when French colonial rulers, and later the government of the newly independent Senegal, imposed new systems of land tenure and cultivation on the Serer of Siin. Galvan's book is a painstaking and skillful autopsy of ruinous Western-style "rational" economic development policy forced upon a fragile, yet self-sustaining, society. It is also a disquieting demonstration of the general folly of such an approach and an attempt to articulate a better, more sensitive, and ultimately more productive model for change—a model Galvan calls "institutional syncretism."</p>
      </abstract>
      <counts>
         <page-count count="331"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pnqwp.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pnqwp.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pnqwp.3</book-part-id>
                  <title-group>
                     <title>Illustrations</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pnqwp.4</book-part-id>
                  <title-group>
                     <title>Tables</title>
                  </title-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pnqwp.5</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <fpage>xiii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pnqwp.6</book-part-id>
                  <title-group>
                     <title>Maps</title>
                  </title-group>
                  <fpage>xv</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pnqwp.7</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>ʺBuying Rope Is a Young Manʹs Jobʺ:</title>
                     <subtitle>Transformations of Culture and Institutions</subtitle>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>In the Siin region of Senegal, itʹs sometimes said that ʺbuying rope is a young manʹs job.ʺ The logic of the free market, by contrast, tells us that anyone, male or female, sixteen or sixty years old, should be able to go and buy rope. After all, standardization—particularly standardization of people in all their uniqueness into consumers, workers, homogeneous interchangeable units—is a basic element of marketization and modernization. Yet to many Serer (the people of the Siin), it isnʹt right for just<italic>anybody</italic>to go buy rope, because rope has long been something that a young man should</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pnqwp.8</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>The Serer of Siin:</title>
                     <subtitle>ʺLe type même du paysan africainʺ</subtitle>
                  </title-group>
                  <fpage>33</fpage>
                  <abstract>
                     <p>The stories of institutional syncretism at the heart of this book are set in a cluster of thirty villages, in a region historically known as Njafaj, the heartland of the precolonial Serer kingdom of Siin. The Siin, situated in the south-central part of Senegalʹs old Peanut Basin, is a densely populated area only a half dayʹs drive from the capital, yet it has long been considered the home of some of the most ʺtraditional peasantsʺ in the old French African empire.</p>
                     <p>As in any case study, we are confronted early on with a number of fundamental questions. Who are these</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pnqwp.9</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>ʺTraditionʺ in the Siin:</title>
                     <subtitle>Contested and Enmeshing</subtitle>
                  </title-group>
                  <fpage>72</fpage>
                  <abstract>
                     <p>Notions of ʺtraditionʺ matter because they shape responses to contemporary land tenure arrangements and elected local government bodies. In this chapter, I use survey data to explore the importance of historical memories of ʺtraditionalʺ institutions in three ways. First, I establish that ʺtraditionʺ in so-called backward or developing societies is neither static nor uniform nor divorced from material conditions but is in fact malleable and contested. Notions of traditional culture and institutions vary according to ordinary indicators of social structural positioning including age, gender, wealth, education, degree of extralocal engagement, caste, authority within the household, and type and degree of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pnqwp.10</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Land Pawning as a Response to the Standardization of Tenure</title>
                  </title-group>
                  <fpage>104</fpage>
                  <abstract>
                     <p>In the last weeks of 1937, Birame Diouf and Waly Sene made an arrangement. In a few days, Diouf would have to pay almost two thousand CFA francs in head tax to the<italic>jaraf</italic>of Tukar, the village representative of the local king, himself a vassal of the French colonial state. If Birame Diouf refused, the king would send his<italic>ceddo</italic>warriors to Dioufʹs house, and they eventually would track him down and deliver him to the<italic>commandant de cercle</italic>at Fatick. Out in the eastern Saluum where they were building new roads, the corvée gangs could always use extra</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pnqwp.11</book-part-id>
                  <title-group>
                     <title>Transitions:</title>
                     <subtitle>The Siin Reordered</subtitle>
                  </title-group>
                  <fpage>125</fpage>
                  <abstract>
                     <p>In the rainy season of 1974, the Ngahoye Pond was a sunbaked, cracked clay sink. Fifty years before, itʹs said, the place was very different. Most summers, Ngahoye used to flood well into surrounding millet fields. On hot August and September afternoons, the pond wore a teeming multicolored halo—half the perimeter crowded with women washing their indigo<italic>pagnes</italic>and<italic>bazin boubous</italic>, the other half barely accommodating the thirsty herds of one-hump cattle and interminable scruffy goats and sheep that had come from across the western Siin to drink. Further into the waters, in the hidden estuaries and narrow canals</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pnqwp.12</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Two Romanticizations:</title>
                     <subtitle>Tenure Confusion after the National Domain Law</subtitle>
                  </title-group>
                  <fpage>131</fpage>
                  <abstract>
                     <p>The National Domain Law and Serer responses to it reveal overlapping and, in some ways, contradictory processes of romanticization and reconstruction of land tenure relations. On the one hand, Senghorʹs 1964 legislation was built on its own romantic notions about unproblematic egalitarian access to abundant commons land in a protosocialist rural Africa.¹ This vision of communitarian tenure relations was not entirely without basis in historical reality. Rather, the philosophy of<italic>négritude</italic>and African socialism read the history of land tenure relations in places like the Siin (ironically, Senghorʹs own homeland) rather selectively. Senghorʹs (and by extension, the stateʹs) memory of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pnqwp.13</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>ʺThe King Has Come—Now Everything Is Ruinedʺ:</title>
                     <subtitle>The Promise and Frustration of Syncretic Rural Democracy</subtitle>
                  </title-group>
                  <fpage>164</fpage>
                  <abstract>
                     <p>On a cool evening in March 1992, the drums of the griots sent out an announcement: an old man, a revered old man, had just died. Next day, Djignak Diouf heard that it was old Matiasse Sene. Djignak Diouf harnessed up his horse, hooked the cart, and set off to pay his respects at the funeral. On the way he passed a field, the one that his grandfather, Birame Diouf, had given in pawn to Waly Sene, the father of Matiasse Sene. It was a good, rich field, and Djignakʹs family hadnʹt put a hoe in that soil for almost</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pnqwp.14</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Culturally Sustainable Development</title>
                  </title-group>
                  <fpage>209</fpage>
                  <abstract>
                     <p>We have now followed three stories of local response to the imposition of formal state and market institutions in the Siin region of west-central Senegal. This matter of local responses to colonialism, Westernization, commodification, capitalist incorporation, development, and charitable (and not-so-charitable) aid constitutes a rich and underspecified realm of research. We know intuitively that peasants, the young, women, urban migrants, refugees, the landless—name your preferred category from among subordinate populations—simply do not accept the systems of production and domination I have placed under the rough heading ʺinstitutional impositionʺ without some effort to protect their own interests. A variety</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pnqwp.15</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>229</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pnqwp.16</book-part-id>
                  <title-group>
                     <title>Glossary</title>
                  </title-group>
                  <fpage>269</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pnqwp.17</book-part-id>
                  <title-group>
                     <title>Bibliography</title>
                  </title-group>
                  <fpage>279</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pnqwp.18</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>301</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
