<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt7p24j</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt4cg75t</book-id>
      <subj-group subj-group-type="discipline">
         <subject>History</subject>
         <subject>Political Science</subject>
         <subject>Education</subject>
      </subj-group>
      <book-title-group>
         <book-title>The Quest for Socialist Utopia</book-title>
         <subtitle>The Ethiopian Student Movement, c. 1960-1974</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>ZEWDE</surname>
               <given-names>BAHRU</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>15</day>
         <month>01</month>
         <year>2014</year>
      </pub-date>
      <isbn content-type="ppub">9781847010858</isbn>
      <isbn content-type="epub">9781782042600</isbn>
      <publisher>
         <publisher-name>Boydell &amp; Brewer</publisher-name>
         <publisher-loc>Woodbridge, Suffolk; Rochester, NY</publisher-loc>
      </publisher>
      <edition>NED - New edition</edition>
      <permissions>
         <copyright-year>2014</copyright-year>
         <copyright-holder>Bahru Zewde</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7722/j.ctt4cg75t"/>
      <abstract abstract-type="short">
         <p>In the second half of the 1960s and the early 1970s, the Ethiopian student movement emerged from rather innocuous beginnings to become the major opposition force against the imperial regime in Ethiopia, contributing perhaps more than any other factor to the eruption of the 1974 revolution, a revolution that brought about not only the end of the long reign of Emperor Haile Sellassie, but also a dynasty of exceptional longevity. The student movement would be of fundamental importance in the shaping of the future Ethiopia, instrumental in both its political and social development. Bahru Zewde, himself one of the students involved in the uprising, draws on interviews with former student leaders and activists, as well as documentary sources, to describe the steady radicalisation of the movement, characterised particularly after 1965 by annual demonstrations against the regime and culminating in the ascendancy of Marxism-Leninism by the early 1970s. Almost in tandem with the global student movement, the year 1969 marked the climax of student opposition to the imperial regime, both at home and abroad. It was also in that year that students broached what came to be famously known as the 'national question', ultimately resulting in the adoption in 1971of the Leninist/Stalinist principle of self-determination up to and including secession. On the eve of the revolution, the student movement abroad split into two rival factions; a split that was ultimately to lead to the liquidation of both and the consolidation of military dictatorship as well as the emergence of the ethno-nationalist agenda as the only viable alternative to the military regime. Bahru Zewde is Emeritus Professor of History at Addis Ababa University and Vice President of the Ethiopian Academy of Sciences. He has authored many books and articles, notably A History of Modern Ethiopia, 1855-1974 and Pioneers of Change in Ethiopia: The Reformist Intellectuals of the Early Twentieth Century.</p>
      </abstract>
      <counts>
         <page-count count="320"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt4cg75t.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt4cg75t.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt4cg75t.3</book-part-id>
                  <title-group>
                     <title>List of Illustrations</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt4cg75t.4</book-part-id>
                  <title-group>
                     <title>Acronyms</title>
                  </title-group>
                  <fpage>x</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt4cg75t.5</book-part-id>
                  <title-group>
                     <title>Abbreviations</title>
                  </title-group>
                  <fpage>xii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt4cg75t.6</book-part-id>
                  <title-group>
                     <title>Glossary</title>
                  </title-group>
                  <fpage>xii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt4cg75t.7</book-part-id>
                  <title-group>
                     <title>Note on Transliteration</title>
                  </title-group>
                  <fpage>xiii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt4cg75t.8</book-part-id>
                  <title-group>
                     <title>Note on the Ethiopian Calendar</title>
                  </title-group>
                  <fpage>xiii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt4cg75t.9</book-part-id>
                  <title-group>
                     <title>Acknowledgements</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Zewde</surname>
                           <given-names>Bahru</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>xv</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt4cg75t.10</book-part-id>
                  <title-group>
                     <title>Introduction</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>My interest in the Ethiopian student movement goes back to 1968, the year of the global student revolution. It was then, whilst I was still a third year student at Haile Sellassie I University, that I contributed an article to the<italic>Journal of the Political Science Association</italic>, entitled ‘Some Thoughts on Student Movements: With Special Reference to Ethiopia’. I revisited that article recently, after composing most of the chapters of this book, and I was struck by the similarity of a number of the conclusions in both works. Appropriately enough, as promised in the title, the article began with a</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt4cg75t.11</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Youth in Revolt</title>
                  </title-group>
                  <fpage>13</fpage>
                  <abstract>
                     <p>As these lines were being penned in 2011–12, the world was gripped by the series of popular challenges to authoritarian rule that has come to be known as the Arab Spring. The revolution that spread across the Arab world like a conflagration was set ablaze by the dramatic act of a Tunisian street vendor by the name of Mohammed Bouazzi, who set himself on fire after security forces overturned his vegetable cart, thereby depriving him of the right to earn his livelihood. The popular revolution that swept across the country did not stop until it got rid of the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt4cg75t.12</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>The Political and Cultural Context</title>
                  </title-group>
                  <fpage>35</fpage>
                  <abstract>
                     <p>The early twentieth century witnessed a vibrant intellectual discourse steered by the people described as ‘pioneers of change’ in Bahru (2002). This fascinating group of intellectuals, who happened to be the first to be exposed, directly or indirectly, to modern education, campaigned tirelessly for reform. They argued for administrative efficiency and social justice. The more advanced amongst them exposed the dependent nature of Ethiopia’s political economy. Through their prolific writings in the fields of history and language in particular, they pushed the frontiers of knowledge. This vibrant discourse reached its zenith in the 1920s, with the weekly<italic>Berhanena Salam</italic>emerging</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt4cg75t.13</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>In the Beginning:</title>
                     <subtitle>‘That Will Be the Day, When . . .’</subtitle>
                  </title-group>
                  <fpage>73</fpage>
                  <abstract>
                     <p>Secondary education is a post-1941 phenomenon in Ethiopia. It started with the founding in 1943 of the first secondary school, Haile Sellasie I Secondary School; in popular parlance, it became more famously known as Kotebe, after the locality in the eastern outskirts of Addis Ababa where it was situated. It was followed three years later by another school in the diametrically opposite western outskirts of the capital: General Wingate Secondary School, named after the charismatic British commander who led the British campaign of liberation in 1940–41 and went on to be a legend in the British Burma campaign. In</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt4cg75t.14</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>The Process of Radicalization</title>
                  </title-group>
                  <fpage>101</fpage>
                  <abstract>
                     <p>Conventionally, the year 1965, when Haile Sellasie I University (HSIU) students marched on the streets of Addis Ababa chanting ‘Land to the Tiller’, is seen as a landmark in the history of the Ethiopian student movement, heralding the start of a new era of revolutionary commitment. But, it makes greater sense to view 1965 as the culmination of a gradual process of radicalization rather than the birth of a new phenomenon that it is generally portrayed to be. In this regard, the coming of African scholarship students in 1958 forms an important development in increasing the political consciousness of Ethiopian</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt4cg75t.15</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>1969:</title>
                     <subtitle>Prelude to Revolution</subtitle>
                  </title-group>
                  <fpage>153</fpage>
                  <abstract>
                     <p>In the history of the Ethiopian student movement, there are years that stand out as defining moments. 1962, when the royal patronage that had characterized student-palace relations unravelled following the controversy over the College Day poetry recital, was one. 1965, when students came out onto the streets with the revolutionary slogan of ‘Land to the Tiller’, was another. But, few years rival in the dizzying cascade of events and their momentous significance as 1969. With understandable exaggeration, the chronicler of the movement, Randi Balsvik, entitles the section that describes those events as ‘The Revolution that Failed’. I have chosen the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt4cg75t.16</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Championing the Cause of the Marginalized:</title>
                     <subtitle>the National Question and the Woman Question</subtitle>
                  </title-group>
                  <fpage>187</fpage>
                  <abstract>
                     <p>There is general consensus that the driving force behind the Ethiopian student movement was rejection of oppression in all its forms. The protests and demonstrations that started to peak after 1965 were directed against one manifestation or another of that oppression. The ‘Land to the Tiller’ demonstration of 1965 had as its objective economic and social equity in the exploitation of the country’s most important social and economic asset – land. In 1966, students rose in defence of the poor who were herded in shelters that were considered sub-human. The 1967 demonstration was in protest at the curtailment of the freedom</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt4cg75t.17</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Fusion and Fission:</title>
                     <subtitle>From Student Unions to Leftist Political Organizations</subtitle>
                  </title-group>
                  <fpage>229</fpage>
                  <abstract>
                     <p>The Ethiopian student movement produced a number of organizations both at home and abroad. The process was characterized by alternating phenomena of fusion and fission. We have already seen in Chapter 3 the emergence of the University College Union (UCU) at University College of Addis Ababa (UCAA) and of the National Union of Ethiopian University Students (NUEUS) as an umbrella national organization inside the country, as well as that of the Ethiopian Students Association in North America (ESANA, later changed to ESUNA when the Association was renamed a union) and the Ethiopian Students Union in Europe (ESUE). The second half</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt4cg75t.18</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>Conclusion:</title>
                     <subtitle>The Legacy</subtitle>
                  </title-group>
                  <fpage>263</fpage>
                  <abstract>
                     <p>For about a decade and a half in the middle of the past century, Ethiopian students made a decisive and fateful intervention in the national affairs of their country. In a system that did not brook any formal political dissent, they effectively became His Majesty’s Disloyal Opposition. They began rather tentatively, reciting poems of social and political satire that were barely tolerated by the imperial regime. They ended with hijackings of passenger aircraft and armed struggle. In between, they punctuated their struggle with annual, almost ritual, demonstrations and, in some extreme circumstances, embassy occupations. The cumulative effect of this unrelenting</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt4cg75t.19</book-part-id>
                  <title-group>
                     <title>Bibliography</title>
                  </title-group>
                  <fpage>281</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt4cg75t.20</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>295</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt4cg75t.21</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>301</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
