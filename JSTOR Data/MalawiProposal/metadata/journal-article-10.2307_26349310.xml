<?xml version="1.0" encoding="UTF-8"?>
<article article-type="research-article" dtd-version="1.0" xml:lang="eng">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="publisher-id">demorese</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50020442</journal-id>
         <journal-title-group>
            <journal-title>Demographic Research</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Max Planck Institute for Demographic Research</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">14359871</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">23637064</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">26349310</article-id>
         <article-categories>
            <subj-group>
               <subject>Research Article</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Overestimating HIV infection</article-title>
            <subtitle>The construction and accuracy of subjective probabilities of HIV infection in rural Malawi</subtitle>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Anglewicz</surname>
                  <given-names>Philip</given-names>
               </string-name>
               <xref ref-type="aff" rid="af1">¹</xref>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Kohler</surname>
                  <given-names>Hans-Peter</given-names>
               </string-name>
               <xref ref-type="aff" rid="af2">²</xref>
            </contrib>
            <aff id="af1">
               <label>¹</label>Population Studies Center, University of Pennsylvania, 3718 Locust Walk, Philadelphia PA 19104-6299, USA.</aff>
            <aff id="af2">
               <label>²</label>Professor of Sociology, University of Pennsylvania, 3718 Locust Walk, Philadelphia PA 19104-6299, USA.</aff>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>1</month>
            <year>2009</year>
            <string-date>JANUARY - JUNE 2009</string-date>
         </pub-date>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>6</month>
            <year>2009</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink">20</volume>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">e26349304</issue-id>
         <fpage>65</fpage>
         <lpage>96</lpage>
         <permissions>
            <copyright-statement>© 2009 Anglewicz &amp; Kohler</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/26349310"/>
         <abstract xml:lang="eng">
            <p>In the absence of HIV testing, how do rural Malawians assess their HIV status? In this paper, we use a unique dataset that includes respondents’ HIV status as well as their subjective likelihood of HIV infection. These data show that many rural Malawians overestimate their likelihood of current HIV infection. The discrepancy between actual and perceived status raises an important question: Why are so many wrong? We begin by identifying determinants of self-assessed HIV status, and then compare these assessments with HIV biomarker results. Finally, we ask what characteristics of individuals are associated with errors in self-assessments.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="unparsed-citations">
         <title>References</title>
         <ref id="ref1">
            <mixed-citation publication-type="other">Allen, S., Serufilira, A., Bogaerts, J., Van de Perre, P., Nsengumuremyi, F., Lindan, C., Carael, M., Wolf, W., Coates T., and Hulley S. (1992). Confidential HIV testing and condom promotion in Africa: Impact on HIV and gonorrhea rates. Journal of the American Medical Association 268(23): 3338-43. doi:10.1001/jama.268.23.3338.</mixed-citation>
         </ref>
         <ref id="ref2">
            <mixed-citation publication-type="other">Allison, P.D. (1999). Logistic Regression Using the SAS System: Theory and Application. Cary, NC: SAS Institute Inc.</mixed-citation>
         </ref>
         <ref id="ref3">
            <mixed-citation publication-type="other">Anglewicz, P. (2007). Migration, risk perception and HIV infection in rural Malawi. Ph.D. Dissertation. Philadelphia: Graduate Group in Demography, Population Studies Center, University of Pennsylvania.</mixed-citation>
         </ref>
         <ref id="ref4">
            <mixed-citation publication-type="other">Anglewicz, P., Adams, J., Obare, F., Kohler, H.-P., and Watkins, S. (2007). The Malawi Diffusion and Ideational Change Project 2004–06: Data collection, data quality and analyses of attrition. Philadelphia: Population Studies Center, University of Pennsylvania (SNP working paper no. 12).</mixed-citation>
         </ref>
         <ref id="ref5">
            <mixed-citation publication-type="other">Bignami-Van Assche, S., Anglewicz P., Chao L.W., Reniers G., Smith K., Thornton R., Watkins S., Weinreb A. (2004). Research Protocol for Collecting STI and Biomarker Samples in Malawi. Philadelphia: Population Studies Center, University of Pennsylvania (SNP working paper no.7).</mixed-citation>
         </ref>
         <ref id="ref6">
            <mixed-citation publication-type="other">Bignami-Van Assche, S., Chao, L.W., Anglewicz, P., Bula, A., Chilongozi, D. (2007). The Validity of Self-Reported Likelihood of HIV Infection Among the General Population of Rural Malawi. Sexually Transmitted Infections 83(1): 35-40. doi:10.1136/sti.2006.020545.</mixed-citation>
         </ref>
         <ref id="ref7">
            <mixed-citation publication-type="other">Bracher, M.D., Santow, G., and Watkins, S. (2003). Moving and Marrying; Modeling HIV Infection Among Newlyweds in Malawi. Demographic Research S1(7): 207-246. doi:10.4054/DemRes.2003.S1.7.</mixed-citation>
         </ref>
         <ref id="ref8">
            <mixed-citation publication-type="other">Coates, T.J., Grinstead, O.A., Gregorich, S.E., Sweat, M.D., Kamenga, M.C., Sangiwa, G., Balmer, D., and Furlonge, C. (2000). Efficacy of voluntary HIV-1 counselling and testing in individuals and couples in Kenya, Tanzania, and Trinidad: A randomised trial. Lancet 356(9224): 103–112. doi:10.1016/S0140-6736(00)02446-6.</mixed-citation>
         </ref>
         <ref id="ref9">
            <mixed-citation publication-type="other">Delavande, A. and Kohler, H.-P. (2007). An Interactive Approach for Eliciting Probabilistic Expectations in a Developing Country Context with High HIVprevalence. Philadelphia: Population Aging Center, University of Pennsylvania (PARC WPS 2007-06).</mixed-citation>
         </ref>
         <ref id="ref10">
            <mixed-citation publication-type="other">Delavande, A., Kohler H.-P. and Anglewicz, P. (2007). Subjective risk perceptions about HIV infection: An interactive approach. Philadelphia: Population Aging Center, University of Pennsylvania (PARC WPS 2007-06).</mixed-citation>
         </ref>
         <ref id="ref11">
            <mixed-citation publication-type="other">Demographic and Health Surveys (2004). Malawi Demographic and Health Survey, 2004. Zomba, Malawi and Calverton, MD USA: National Statistics Office and ORC Macro.</mixed-citation>
         </ref>
         <ref id="ref12">
            <mixed-citation publication-type="other">Department for International Development (2005). About HIV/AIDS in Malawi [electronic resource]. London: Department for International Development. http://www.dfid.gov.uk/pubs/files/malawi-hiv-aids.pdf.</mixed-citation>
         </ref>
         <ref id="ref13">
            <mixed-citation publication-type="other">de Walque, D. (2007). Sero-discordant couples in five African countries: Implications for prevention strategies. Population and Development Review 33(3): 501-523. doi:10.1111/j.1728-4457.2007.00182.x.</mixed-citation>
         </ref>
         <ref id="ref14">
            <mixed-citation publication-type="other">de Zoysa, I., Sweat, M.D. and Denison, J.A. (1996). Faithful but fearful: Reducing HIV transmission in stable relationships. AIDS 10(SA): S197–S203.</mixed-citation>
         </ref>
         <ref id="ref15">
            <mixed-citation publication-type="other">Dunkle, K., Stephenson, R., Karita, E., Chomba, E., Kayitenkore, K., Vwalika, C., Greenberg, L., and Allen, S. (2008). New heterosexually transmitted HIV infections in married or cohabiting couples in urban Zambia and Rwanda: an analysis of survey and clinical data. The Lancet 371(9631): 2183 – 2191. doi:10.1016/S0140-6736(08)60953-8.</mixed-citation>
         </ref>
         <ref id="ref16">
            <mixed-citation publication-type="other">Fennema, J.S., van Ameijden, E.J., Coutinho, R.A., and van den Hoek, J.A.R. (1995). Validity of self-reported sexually transmitted diseases in a cohort of drug-using prostitutes in Amsterdam: trends from 1986 to 1992. International Journal of Epidemiology 24(5): 1034-41. doi:10.1093/ije/24.5.1034.</mixed-citation>
         </ref>
         <ref id="ref17">
            <mixed-citation publication-type="other">Gray, R., Wawer, M., Brookmeyer, R., Sewankambo, N., Serwadda, D., Wabwire-Mangen, F., Lutalo, T., Li, X., vanCott, T., and Quinn, T. (2001). Probability of HIV-1 Transmission Per Coital Act in Monogamous, Heterosexual, HIV-1 Discordant Couples in Rakai, Uganda. The Lancet 357(9263):1149-53. doi:10.1016/S0140-6736(00)04331-2.</mixed-citation>
         </ref>
         <ref id="ref18">
            <mixed-citation publication-type="other">Greene, W.H. (2007). Econometric Analysis. London: Prentice Hall.</mixed-citation>
         </ref>
         <ref id="ref19">
            <mixed-citation publication-type="other">Heimer, C.A. (1988). Social Structure, Psychology, and the Estimation of Risk. Annual Review of Sociology 14:491-519. doi:10.1146/annurev.so.14.080188.002423.</mixed-citation>
         </ref>
         <ref id="ref20">
            <mixed-citation publication-type="other">Heise, L.L. and Elias, C. (1995). Transforming AIDS prevention to meet women's needs. A focus on developing countries. Social Science and Medicine 40(7):931–943. doi:10.1016/0277-9536(94)00165-P.</mixed-citation>
         </ref>
         <ref id="ref21">
            <mixed-citation publication-type="other">Helleringer, S. and Kohler, H.-P. (2006). Social Networks, Risk Perceptions and Changing Attitudes Towards HIV/AIDS: New Evidence from a Longitudinal Study Using Fixed-Effects Analysis. Population Studies 59(3):265-282. doi:10.1080/00324720500212230.</mixed-citation>
         </ref>
         <ref id="ref22">
            <mixed-citation publication-type="other">Holbrooke, R. (2004). Fighting AIDS is Good Business [electronic resource]. Washington D.C. Council on Foreign Relations. http://www.usembassy.it/file2004_04/alia/a4042802.htm</mixed-citation>
         </ref>
         <ref id="ref23">
            <mixed-citation publication-type="other">Hudson, C.P. (1996). The importance of transmission of HIV-1 in discordant couples in the population dynamics of HIV-1 infection in rural Africa. International Journal of STD and AIDS 7(4): 302-304. doi:10.1258/0956462961917906.</mixed-citation>
         </ref>
         <ref id="ref24">
            <mixed-citation publication-type="other">Kahneman, D., Slovic, P., and Tversky, A. (1982). Judgment Under Uncertainty: Heuristics and Biases. Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="ref25">
            <mixed-citation publication-type="other">Kaler, A. (2003). “My Girlfriends Could Fill a Yanu-Yanu Bus”: Rural Malawian Men’s Claims About Their Own Serostatus. Demographic Research S1(7): 349- 72. doi:10.4054/DemRes.2003.S1.11.</mixed-citation>
         </ref>
         <ref id="ref26">
            <mixed-citation publication-type="other">Kengeya-Kayondo, J.F., Carpenter, L.M., Kintu, P.M., Nabaitu, J., Pool, R. and Whitworth, J.A.G. (1999). Risk Perception and HIV-1 Prevalence in 15,000 Adults in Rural South-West Uganda. AIDS 13(16): 2295-2302. doi:10.1097/00002030-199911120-00012.</mixed-citation>
         </ref>
         <ref id="ref27">
            <mixed-citation publication-type="other">King, R., Allen, S., Serufilira, A., Karita, E. and Van de Perre, P. (1993). Voluntary confidential HIV testing for couples in Kigali, Rwanda. AIDS 7(10): 1393–1394. doi:10.1097/00002030-199310000-00018.</mixed-citation>
         </ref>
         <ref id="ref28">
            <mixed-citation publication-type="other">Kohler, H.-P. (1997). Learning in social networks and contraceptive choice. Demography 34(3): 369-383. doi:10.2307/3038290.</mixed-citation>
         </ref>
         <ref id="ref29">
            <mixed-citation publication-type="other">Kohler, H.P., Behrman, J.R. and Watkins, S. C. (2007). Social Networks and {HIV/AIDS} Risk Perceptions. Demography 44(1): 1-33. doi:10.1353/dem.2007.0006.</mixed-citation>
         </ref>
         <ref id="ref30">
            <mixed-citation publication-type="other">McKenna, S.L., Muyinda, G.K., Roth, D., Mwali, M., N'gandu, N., and Myrick, A. (1997). Rapid HIV testing and counseling for voluntary testing centers in Africa. AIDS 11(S1): S103–S110.</mixed-citation>
         </ref>
         <ref id="ref31">
            <mixed-citation publication-type="other">Manski, C. F. (2004). Measuring expectations. Econometrica 72(5):1329–1376. doi:10.1111/j.1468-0262.2004.00537.x.</mixed-citation>
         </ref>
         <ref id="ref32">
            <mixed-citation publication-type="other">Messersmith, L.J., Kane, T.T., Odebiyi, A.I., and Adewuyi, A.A. (2000). Who’s at Risk? Men’s STD Experience and Condom Use in Southwest Nigeria. Studies in Family Planning 31(3):203-216. doi:10.1111/j.1728-4465.2000.00203.x.</mixed-citation>
         </ref>
         <ref id="ref33">
            <mixed-citation publication-type="other">Ministry of Health (Malawi) (2006). Report on ARV Therapy in Malawi – up to September 2005. Malawi: Ministry of Health.</mixed-citation>
         </ref>
         <ref id="ref34">
            <mixed-citation publication-type="other">Montgomery, M.R. (2000). Perceiving Mortality Decline. Population and Development Review 26(4): 795-819. doi:10.1111/j.1728-4457.2000.00795.x.</mixed-citation>
         </ref>
         <ref id="ref35">
            <mixed-citation publication-type="other">National AIDS Commission (Malawi) (2006). Malawi HIV and AIDS Monitoring and Evaluation Report. Malawi: Office of the President and Cabinet.</mixed-citation>
         </ref>
         <ref id="ref36">
            <mixed-citation publication-type="other">Obare, F., Fleming, P., Anglewicz, P., Thornton, R., Martinson, F, Kapatuka, A., Poulin, M., Watkins, S., and Kohler, H.-P. (2008). HIV Incidence and Learning One’s HIV Status: Evidence From Repeat Population-based Voluntary Counseling and Testing in Rural Malawi. Sexually Transmitted Infections online 16 Oct 2008.</mixed-citation>
         </ref>
         <ref id="ref37">
            <mixed-citation publication-type="other">Philipson, T. and Posner, R. (1993). Private Choices and Public Health. Boston, MA: Harvard University Press.</mixed-citation>
         </ref>
         <ref id="ref38">
            <mixed-citation publication-type="other">Rabin, M. (1998). Psychology and Economics. Journal of Economic Literature, American Economic Association 36(1): 11-46.</mixed-citation>
         </ref>
         <ref id="ref39">
            <mixed-citation publication-type="other">Reniers, G. (2005). Nuptual Strategies for Managing the Risk of HIV Infection in Rural Malawi. Paper presented at the Annual Meeting of the Population Association of America, Philadelphia, April 1-3 2005.</mixed-citation>
         </ref>
         <ref id="ref40">
            <mixed-citation publication-type="other">Robinson, N.J., Mulder, V., Auvert, B., Whiworth, A.G., and Hayes, R.J. (1999). Type of partnership and heterosexual spread of HIV infection in rural Uganda: results from simulation modelling. International Journal of STD and AIDS 10: 718-725. doi:10.1258/0956462991913394.</mixed-citation>
         </ref>
         <ref id="ref41">
            <mixed-citation publication-type="other">Smith, K.P. and Watkins, S.C. (2005). Perceptions of Risk and Strategies for Prevention: Responses to HIV/AIDS in Rural Malawi. Social Science and Medicine 60(3): 649-60. doi:10.1016/j.socscimed.2004.06.009.</mixed-citation>
         </ref>
         <ref id="ref42">
            <mixed-citation publication-type="other">Strauss, S.M., Rindskopf, D.M., Deren, S., and Falkin, G.P. (2001). Concurrence of drug users’ self-report of current HIV status and serotest results. Journal of the Acquired Immune Deficiency Syndrome 27(3): 301-7.</mixed-citation>
         </ref>
         <ref id="ref43">
            <mixed-citation publication-type="other">Thomas, W.I. and Thomas, D.S. (1928). The Child in America: Behavior Problems and Programs. New York: Alfred A. Knopf.</mixed-citation>
         </ref>
         <ref id="ref44">
            <mixed-citation publication-type="other">Thornton, R. (2005). Is it Good to Know? The Demand for and Impact of Learning HIV Results. Paper presented at the Northeastern University Development Conference, Brown University, Providence, RI, September 3, 2005.</mixed-citation>
         </ref>
         <ref id="ref45">
            <mixed-citation publication-type="other">Timaeus, I.M. and Jasseh, M. (2004). Adult Mortality in Sub-Saharan Africa: Evidence from Demographic and Health Surveys. Demography 41(4): 757-772. doi:10.1353/dem.2004.0037.</mixed-citation>
         </ref>
         <ref id="ref46">
            <mixed-citation publication-type="other">Tversky, A. and Kahneman, D. (1992). Judgment and Uncertainty: Heuristics and Biases. Science 185(4157): 1124-1131. doi:10.1126/science.185.4157.1124.</mixed-citation>
         </ref>
         <ref id="ref47">
            <mixed-citation publication-type="other">UNAIDS (2004). Malawi: Epidemiological Fact Sheet on HIV/AIDS and Sexually Transmitted Infections [electronic resource] Geneva, UNAIDS. data.unaids.org/Publications/Fact-Sheets01/malawi_en.pdf</mixed-citation>
         </ref>
         <ref id="ref48">
            <mixed-citation publication-type="other">UNAIDS (1999). Sexual Behavior Change for HIV: Where Have Theories Taken Us? [electronic resource] Geneva: UNAIDS. www.who.int/hiv/strategic/surveillance/en/unaids_99_27.pdf</mixed-citation>
         </ref>
         <ref id="ref49">
            <mixed-citation publication-type="other">UNAIDS/World Health Organization (2004). UNAIDS/WHO Policy on HIV Testing [electronic resource]. Geneva: UNAIDS. http://data.unaids.org/unadocs/hivtestingpolicy_en.pdf</mixed-citation>
         </ref>
         <ref id="ref50">
            <mixed-citation publication-type="other">Watkins, S.C. (2004). Navigating AIDS in Rural Malawi. Population and Development Review 30(4): 673-705. doi:10.1111/j.1728-4457.2004.00037.x.</mixed-citation>
         </ref>
         <ref id="ref51">
            <mixed-citation publication-type="other">Watkins, S.C and Swidler, A. (2006). Hearsay Ethnography: Capturing Culture in Action. Los Angeles: California Center for Population Research (On-Line Working Paper Series Paper CCPR-007-06).</mixed-citation>
         </ref>
         <ref id="ref52">
            <mixed-citation publication-type="other">Watkins, S., Behrman, J.R., Kohler, H.-P., and Zulu, E.M. (2003). Introduction to “Research on demographic aspects of HIV/AIDS in rural Africa”. Demographic Research S1(1): 1–30. doi:10.4054/DemRes.2003.S1.1.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
