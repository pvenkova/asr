<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">afrdevafrdev</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50014718</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Africa Development / Afrique et Développement</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>CODESRIA</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">08503907</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">24484709</article-id>
         <title-group>
            <article-title>Higher Education as an African Public Sphere and the University as a Site of Resistance and Claim of Ownership for the National Project</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>N'Dri T.</given-names>
                  <surname>Assié-Lumumba</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>1</month>
            <year>2011</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">36</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i24479784</issue-id>
         <fpage>175</fpage>
         <lpage>206</lpage>
         <permissions>
            <copyright-statement>© Council for the Development of Social Science Research in Africa, 2011</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/24484709"/>
         <abstract>
            <p>Throughout the African continent, albeit a product of imperial domination, every state at independence conceived a national project, which aimed at building a nation-state with a clearly articulated development agenda. Education as a social institution was considered requisite toward the actualisation of the national project. The sub-sector of higher education, and particularly the university, appeared as an indispensable agency. Given the general colonial policy of exclusion of Africans from university education, the right of African states to build their national/public universities epitomised self-determination at independence. The independence movements in the 1950s-1960s coincided also with the regained popularity of human capital theory that stipulated that education, especially the highest levels, constituted an investment for individual socio-economic attainment and social mobility as well as national and structural development. From its inception, the Western style of university that was conceived out of the colonial experience represented a special site for contention and affirmation of the Africans to realize their national projects. In the context of globalisation, international organisations and programmes such as the World Bank and General Agreement on Trade in Services (GATS) have emerged as proxies of the old colonial powers with the same goal of influencing the policies that restrict or shape higher education in African countries. Key constituencies of African universities, namely students and teaching staff, have resisted such infringement on Africans' rights to university education and autonomy in determining their domestic policies. The main objective of this article is to analyse the evolution of the African university as a site for the continued struggle for self-determination. It will be argued that, in spite of the history of a few institutions in a handful of countries, the African university in the 21st Century reflects essentially the colonial relations. Thus, for instance, the new Information and Communication Technologies (ICTs) and distance learning programmes, and the emerging private universities in the context of liberalisation mantra, will also be analysed in the framework of the liberalisation policies that have been promoted by the global colonial proxies. In this article, the public mission of the university, be it public or private, will be examined. The approach will be basically historical, assessing the actors and their transformations and mutations within the same reality of the structural inequality of power in the global system and various African responses through continued resistance and affirmation. It will address the fundamental question of the search for the public university or the university with a public mission for the production of relevant knowledge in the various disciplines, critical thinking and new paradigms, and methodologies to promote social progress amidst the challenges of the dominant liberal globalisation and the objective conditions of the African States, societies, and people. Partout sur le continent africainchaque Etat à l'indépendance a conçu un projet national – bien que ce soit un produit de la domination impériale – visant l'édification d'un État-nation avec un agenda de développement clairement articulé. En tant qu'institution sociale, l'éducation était considérée comme nécessaire en vue de l'actualisation du projet national. Le sous-secteur de l'enseignement supérieur, et en particulier l'université, est apparu comme une agence indispensable. Etant donné la politique coloniale générale d'exclusion des Africains de l'enseignement universitaire, le droit des États africains de construire leurs propres universités nationales/publiques représentait l'autodétermination à l'indépendance. Les mouvements d'indépendance des années 1950-1960 coïncidaient également avec le regain de popularité de la théorie du capital humain qui stipulait que l'éducation, surtout aux niveaux supérieurs, constituait un investissement pour la réalisation socioéconomique et la mobilité sociale de l'individu, ainsi que le développement national et structurel. Depuis sa création, l'université de style occidental, qui avait été conçue à partir de l'expérience coloniale, représentait un site spécial de contestation et d'affirmation des africains pour la réalisation de leurs projets nationaux. Dans le contexte de la mondialisation, des organisations internationales comme la Banque mondiale et des programmes internationaux tels que l'Accord général sur le commerce de services (AGCS ou GATS en anglais pour General Agreement on Trade in Services) sont apparues comme des mandataires des anciennes puissances coloniales avec le même objectif : influencer les politiques qui restreignent ou déterminent l'enseignement supérieur dans les pays africains. Les principaux mandants des universités africaines, à savoir les étudiants et le personnel enseignant, ont résisté à de telles violations des droits des africains à l'enseignement universitaire et à l'autonomie dans la détermination de leurs politiques nationales. L'objectif principal de cet article est d'analyser l'évolution de l'université africaine en tant que site de la lutte continue pour l'autodétermination. Nous soutiendrons que malgré l'histoire de quelques institutions dans un petit nombre de pays, l'université africaine au XXIe siècle est essentiellement le reflet des rapports coloniaux. Ainsi, par exemple, les nouveaux programmes de Technologies de l'Information et de la Communication (TIC) et d'enseignement à distance, et les universités privées émergentes dans le contexte du mantra de la libéralisation, seront également analysés dans le cadre des politiques de libéralisation qui ont été promues par les mandataires coloniaux mondiaux. Dans cet article, la mission publique de l'université, qu'elle soit publique ou privée, sera examinée. Nous adopterons une démarche fondamentalement historique, évaluant les acteurs et leurs transformations et mutations dans la même réalité de l'inégalité structurelle de pouvoir dans le système mondial, et diverses réponses africaines à travers la résistance et l'affirmation continues. Nous traiterons la question fondamentale de la recherche de l'université publique ou de l'université ayant une mission publique pour la production de connaissances pertinentes dans les diverses disciplines, la pensée critique et les nouveaux paradigmes, ainsi que les méthodologies visant à promouvoir le progrès social au milieu des défis de la mondialisation libérale dominante et des conditions objectives des États, des sociétés et des peuples africains.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Note</title>
         <ref id="d309e244a1310">
            <label>1</label>
            <mixed-citation id="d309e251" publication-type="other">
Malawi and Mozambique taken fforn Bloom et al. (2006)</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>Bibliography</title>
         <ref id="d309e267a1310">
            <mixed-citation id="d309e271" publication-type="other">
AAU (Association ofAfrican Uni versities), 1995, 'The University in Africa in the
1990s and beyond: A Summary Report', Accra: Association of African
Universities.</mixed-citation>
         </ref>
         <ref id="d309e284a1310">
            <mixed-citation id="d309e288" publication-type="other">
AAU, 2004, The Implications of WTO/GATS for Higher Education in Africa,
Proceedings of Accra Workshop on GATS, Accra, Ghana.</mixed-citation>
         </ref>
         <ref id="d309e298a1310">
            <mixed-citation id="d309e302" publication-type="other">
ADEA (Association for the Development of Education in Africa), 1998, Tertiary
Distance Learning in Sub-Saharan Africa: Overview and Directory to
Programs, Toronto: Roberts and Associates.</mixed-citation>
         </ref>
         <ref id="d309e315a1310">
            <mixed-citation id="d309e319" publication-type="other">
Aina, T.A., 1995, Quality andRelevance: African Universities in the 2Ist Century,
Accra: Association ofAfrican Universities.</mixed-citation>
         </ref>
         <ref id="d309e330a1310">
            <mixed-citation id="d309e334" publication-type="other">
Ajayi, J.E.A., Goma, Lameck, K.H., Johnson, G Ampah, with a contribution by
Wanjiku Mwotia, 1996, The African experience with higher éducation, Accra:
The Association ofAfrican Universities; London: James Currey; Athens, Ohio:
Ohio University Press.</mixed-citation>
         </ref>
         <ref id="d309e350a1310">
            <mixed-citation id="d309e354" publication-type="other">
Amin, S., 1997, Capitalism in the Age of Globalization: The Management of
Contemporary Society, London; Atlantic Highlands, N.J.: Zed Books.</mixed-citation>
         </ref>
         <ref id="d309e364a1310">
            <mixed-citation id="d309e368" publication-type="other">
Amuwo, K.1999. Confronting the Crisis of the University in Africa: Nigérian
Académies and their many Struggles, Harare: African Association of Political
Science.</mixed-citation>
         </ref>
         <ref id="d309e381a1310">
            <mixed-citation id="d309e385" publication-type="other">
Ashby, E., 1964, African Universities and Western Tradition: Cambridge: Harvard
University Press.</mixed-citation>
         </ref>
         <ref id="d309e395a1310">
            <mixed-citation id="d309e399" publication-type="other">
Ashby, E. in association with Anderson, Mary, 1966, Universities: British, Indian,
African; a study in the ecology of higher éducation, Cambridge: Harvard
University Press.</mixed-citation>
         </ref>
         <ref id="d309e412a1310">
            <mixed-citation id="d309e416" publication-type="other">
Assié-Lumumba, N.T., 1991, The Causes of the Declining Enrollment Rates in
Africa: Fallacies and Facts, Report for the International Institute for
Educational Planning (IIEP)/United Nations Educational, Scientific and Cultural
Organization (UNESCO), Paris, France.</mixed-citation>
         </ref>
         <ref id="d309e433a1310">
            <mixed-citation id="d309e437" publication-type="other">
Assié-Lumumba, N.T., 1996, 'The Future Rôle and Mission of African Higher
Education South African Journal of Higher Education, 10(2), pp.5-12.</mixed-citation>
         </ref>
         <ref id="d309e447a1310">
            <mixed-citation id="d309e451" publication-type="other">
Assié-Lumumba, N.T., ed., 2004a, Cyberspace, Distance Learning, and Higher
Education in Developing Countries, Old and Emergent Issues of Access,
Pedagogy, and Knowledge Production, Leiden and Boston: Brill.</mixed-citation>
         </ref>
         <ref id="d309e464a1310">
            <mixed-citation id="d309e468" publication-type="other">
Assié-Lumumba, N.T., 2004b, 'Sustaining Home-Grown Innovations in Higher
Education in Sub-Saharan Africa: A Critical Reflection', Journal of
International Cooperation in Education, Vol. 7, No. 1, pp.71- 83.</mixed-citation>
         </ref>
         <ref id="d309e481a1310">
            <mixed-citation id="d309e485" publication-type="other">
Assié-Lumumba, N.T., 2005, 'African Higher Education: from Compulsory
Juxtaposition to Fusion by Choice-Forging a New Philosophy of Education
for Social Progress', in Yusef, Waghid, Berte van Wyk, Faried Adams and Ivan
November, eds., African(a) Philosophy of Education: Reconstructions and
Deconstructions, Matieland: Stellenbosch University, pp. 19-53.</mixed-citation>
         </ref>
         <ref id="d309e504a1310">
            <mixed-citation id="d309e508" publication-type="other">
Assie-Lumumba, N., 2006, Higher éducation in Africa: Crises, Reform and
Transformation, Dakar: CODESRIA.</mixed-citation>
         </ref>
         <ref id="d309e518a1310">
            <mixed-citation id="d309e522" publication-type="other">
Assie-Lumumba, N. and Lumumba-Kasongo, T., 2009, 'The Idea of the Public
University and the National Project in Africa: Toward a Füll Circle, from the
1960s to the Présent' in Craig Calhoun and Diana Rhoten, eds, Knowledge
Matters: The Public Mission of the Research University, New York: Columbia
University Press.</mixed-citation>
         </ref>
         <ref id="d309e542a1310">
            <mixed-citation id="d309e546" publication-type="other">
Berstecher, D. and Carr-Hill, R., 1990, Primary Education and Economie Recession
in the Developing WorldSince 1980, Paris: UNESCO, 1990.</mixed-citation>
         </ref>
         <ref id="d309e556a1310">
            <mixed-citation id="d309e560" publication-type="other">
Bloom, D., Canning, D. and Chan, K., 2006, 'Higher Education and Economie
Development in Africa', Research Commissioned by the World Bank (AFTHD),
http://www.worldbank.org/afr/teia/Higher_Education_Economic_
Growth_in_Africa.pdf.</mixed-citation>
         </ref>
         <ref id="d309e576a1310">
            <mixed-citation id="d309e580" publication-type="other">
Brock-Utne, B., 2000, 'Transforming African Universities using Indigenous
perspectives and local experience', in G. R. Teasdale and Z. M. Rhea, eds.
Local knowledge and wisdom in higher éducation, Oxford: Pergamon.</mixed-citation>
         </ref>
         <ref id="d309e593a1310">
            <mixed-citation id="d309e597" publication-type="other">
Cossa, J., 2008, Power, Politics, and Higher Education in Southern Africa:
International Regimes, Local Governments, and Educational Autonomy,
Amherst, NY.</mixed-citation>
         </ref>
         <ref id="d309e610a1310">
            <mixed-citation id="d309e614" publication-type="other">
Coombe, T., 1991, A Consultation on Higher Education in Africa: A Report to the
Ford Foundation and the Rockefeller Foundation, New York: The Ford
Foundation.</mixed-citation>
         </ref>
         <ref id="d309e627a1310">
            <mixed-citation id="d309e631" publication-type="other">
Fourie, P., 2000, 'The African Renaissance and Technology Transfer', Senior
Research OfFicer at the Australian High Commission in Pretoria, South Africa.</mixed-citation>
         </ref>
         <ref id="d309e642a1310">
            <mixed-citation id="d309e646" publication-type="other">
Johnstone, D.B., 2000, 'Higher Education Finance and Accessibility: Tuition Fees
and Student Loans in Sub-Saharan Africa', Journal of Higher Education in
Africa, Vol. 2, No. 2.</mixed-citation>
         </ref>
         <ref id="d309e659a1310">
            <mixed-citation id="d309e663" publication-type="other">
Johnstone, B. and Marcucci, P.N., 2007, Bruce Johnstone and Pamela N. Marcucci
Worldwide Trends in Higher Education Finance: Cost Sharing, Student
Loans and the Support of Académie Research. UNESCO Forum on Higher
Education, Research and Development, Paris.</mixed-citation>
         </ref>
         <ref id="d309e679a1310">
            <mixed-citation id="d309e683" publication-type="other">
Lange, M-F., 1999, L'école au Togo- Le Processus de déscolarisation et institution
de l'école en Afrique, Paris: Karthala.</mixed-citation>
         </ref>
         <ref id="d309e693a1310">
            <mixed-citation id="d309e697" publication-type="other">
Levidow, L., 2000, 'Marketizing Higher Education: Neoliberal Stratégies and
Counter-Strategies', Cultural Logic, Volume 4, Number 1.</mixed-citation>
         </ref>
         <ref id="d309e707a1310">
            <mixed-citation id="d309e711" publication-type="other">
Makulu, H.F., 1971, Education, Development and Nation-Building in Independent
Africa: A Study of the New Trends and Recent Philosophy of Education,
London: S.C.M. Press.</mixed-citation>
         </ref>
         <ref id="d309e724a1310">
            <mixed-citation id="d309e728" publication-type="other">
Mazrui, A.A., 1975, 'The African University as a Multinational Corporation:
Problems of Penetration and Dependency ', Harvard Educational Review, 45 :
191-210.</mixed-citation>
         </ref>
         <ref id="d309e742a1310">
            <mixed-citation id="d309e746" publication-type="other">
Mazrui, A.A., 1992, 'Towards Diagnosing and Treating Cultural Dependency:
The case of the African University', International Journal of Educational
Development, 12 (2), 95-111.</mixed-citation>
         </ref>
         <ref id="d309e759a1310">
            <mixed-citation id="d309e763" publication-type="other">
Negrao, J., 1995fidequate andSustainable Funding of African Universities, Accra:
Association of African Universities.</mixed-citation>
         </ref>
         <ref id="d309e773a1310">
            <mixed-citation id="d309e777" publication-type="other">
Roberts, J., 2003, Poverty Réduction Outcomes in Education and Health: Public
Expenditure and Aid, Centre for Aid and Public Expenditure, London: Overseas
Development Institute.</mixed-citation>
         </ref>
         <ref id="d309e790a1310">
            <mixed-citation id="d309e794" publication-type="other">
Till, G., 2003, Hamessing Distance Leaming and ICT for Higher Education in Sub-
Saharan Africa: an Examination of Expériences Useful for the Design of
Widespread and Effective Tertiary Education in Sub-Saharan Africa. [S.I.: s.n.].</mixed-citation>
         </ref>
         <ref id="d309e807a1310">
            <mixed-citation id="d309e811" publication-type="other">
UNESCO Institute for Statistics, Multi-year data andfigures.</mixed-citation>
         </ref>
         <ref id="d309e818a1310">
            <mixed-citation id="d309e822" publication-type="other">
World Bank, 1988, Education in Sub-Sahara Africa: Policies for Adjustment,
Revitalization, and Expansion, World Bank: Washington, D.C.</mixed-citation>
         </ref>
         <ref id="d309e833a1310">
            <mixed-citation id="d309e837" publication-type="other">
Yesufu, T.M., ed., 1973, Creating the African University: Emerging Issues of the
1970s, Ibadan: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d309e847a1310">
            <mixed-citation id="d309e851" publication-type="other">
Zeleza, P.T., and Olukoshi, A., eds., 2004, African Universities in the 2Ist Century.
Volume 1 : Libéralisation and Internationalisation. Dakar, Senegal: CODESRIA.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
