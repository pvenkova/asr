<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">jinfedise</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000007</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>The Journal of Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Chicago Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00221899</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">25707015</article-id>
         <article-categories>
            <subj-group>
               <subject>SOUTH AFRICA</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Characterization and Molecular Epidemiology of Rotavirus Strains Recovered in Northern Pretoria, South Africa during 2003–2006</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>L. M.</given-names>
                  <surname>Seheri</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>N.</given-names>
                  <surname>Page</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>J. B.</given-names>
                  <surname>Dewar</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>A.</given-names>
                  <surname>Geyer</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>A. L.</given-names>
                  <surname>Nemarude</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>P.</given-names>
                  <surname>Bos</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>M.</given-names>
                  <surname>Esona</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>A. D.</given-names>
                  <surname>Steele</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>9</month>
            <year>2010</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">202</volume>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i25706995</issue-id>
         <fpage>S139</fpage>
         <lpage>S147</lpage>
         <permissions>
            <copyright-statement>© 2010 Infectious Diseases Society of America</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/25707015"/>
         <abstract>
            <p>Rotavirus infection is the most common cause of severe dehydrating gastroenteritis in infants and young children and remains a significant clinical problem worldwide. The severity and the burden of rotavirus disease could be reduced through the implementation of an effective vaccine. The aim of this study was to characterize rotavirus strains circulating in the local community as part of an ongoing hospital burden of disease study when a G1P[8] rotavirus vaccine candidate was being evaluated in the same community. From 2003 through 2006, 729 rotavirus-positive stool specimens were collected from children &lt;5 years of age who were treated for diarrhea at Dr George Mukhari Hospital, Ga-Rankuwa, South Africa. Molecular characterization of the strains was performed by polyacrylamide gel electrophoresis and genotyping of the VP4 and VP7 alleles using well-established seminested multiplex reverse-transcription polymerase chain reaction methods. In 2003, 62% of strains exhibited the short rotavirus electropherotype, and the most common rotavirus strain was G2P[4]. In subsequent years, predominant rotavirus strains included G1P[8] and G1P[6] in 2004, G3P[8] and G3P[6] in 2005, and G1P[8] in 2006. For the 4 years of the study, rotavirus strains with P[6] genotype were detected in 25% of all rotavirus-positive specimens. In addition, unusual G12P[6] and G8 strains were detected at a low frequency. These results reflect the diversity of rotavirus strains circulating in South African communities.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>&lt;bold&gt;References&lt;/bold&gt;</title>
         <ref id="d766e265a1310">
            <label>1</label>
            <mixed-citation id="d766e272" publication-type="other">
WHO. Global networks for surveillance of rotavirus gastroenteritis:
2001-2008. Wkly Epidemiol Rec 2008;83:421-428.</mixed-citation>
         </ref>
         <ref id="d766e282a1310">
            <label>2</label>
            <mixed-citation id="d766e289" publication-type="other">
Estes MK, Cohen J. Rotavirus gene structure and function. Microbiol
Rev 1989;53:410-449.</mixed-citation>
         </ref>
         <ref id="d766e299a1310">
            <label>3</label>
            <mixed-citation id="d766e306" publication-type="other">
Hoshino AJ, Kapikian AZ. Rotavirus serotypes classification and im-
portance in epidemiology, immunity and vaccine development. J
Health Popul Nucto 2000; 18:5-14</mixed-citation>
         </ref>
         <ref id="d766e319a1310">
            <label>4</label>
            <mixed-citation id="d766e326" publication-type="other">
Estes MK, Kapikian AZ. Rotaviruses. In: Fields BN, Knipe DM, Howley
PM, eds. Fields virology. 5th ed. Philadelphia: Lippincott Williams &amp;
Wilkins, 2007:1917-1974.</mixed-citation>
         </ref>
         <ref id="d766e340a1310">
            <label>5</label>
            <mixed-citation id="d766e347" publication-type="other">
McNeal MM, Sestak K, Choi AH-C, et al. Development of a rotavirus-
shedding model in rhesus macaques, using a homologous wild type
rotavirus of a new P genotype. J Virol 2005; 79:944-954.</mixed-citation>
         </ref>
         <ref id="d766e360a1310">
            <label>6</label>
            <mixed-citation id="d766e367" publication-type="other">
Rahman M, Matthijnssens J, Nahar S, et al. Characterization of a novel
P[25], Gil human group A rotavirus. J Clin Microbiol 2005;43:
208-212.</mixed-citation>
         </ref>
         <ref id="d766e380a1310">
            <label>7</label>
            <mixed-citation id="d766e387" publication-type="other">
Khamrin P, Maneekarn N, Peerakome S, et al. Novel porcine rotavirus
of genotype P[27] shares new phylogenetic lineage with G2 porcine
rotavirus strain. Virology 2007;361:243-252.</mixed-citation>
         </ref>
         <ref id="d766e400a1310">
            <label>8</label>
            <mixed-citation id="d766e407" publication-type="other">
Matthijnssens J, Ciarlet M, Rahman M, et al. Recommendation for the
classification of group A rotaviruses using all 11 genomic RNA seg-
ments. Arch Virol 2008; 153:1621-1629.</mixed-citation>
         </ref>
         <ref id="d766e420a1310">
            <label>9</label>
            <mixed-citation id="d766e427" publication-type="other">
Solberg OD, Hasing ME, Trueba G, Eisenberg JN. Characterization of
novel VP7, VP4, and VP6 genotypes of previously untypeable group
A rotavirus. Virology 2009; 385:58-67.</mixed-citation>
         </ref>
         <ref id="d766e440a1310">
            <label>10</label>
            <mixed-citation id="d766e449" publication-type="other">
Adah MI, Ronwedder A, Olaleyle OD, Werchau H. Further charac-
terization of field strains of rotavirus from Nigeria VP4 genotype P6
frequently identified among symptomatically infected children. J Trop
Pediatr 1997;43:267-274.</mixed-citation>
         </ref>
         <ref id="d766e466a1310">
            <label>11</label>
            <mixed-citation id="d766e473" publication-type="other">
Cunliffe NA, Gondwe JS, Broadhead RL, et al. Rotavirus G and P types
in children with acute diarrhoea in Blantyre, Malawi, from 1997 to
1998: predominance of novel P[6] G8 strains. J Med Virol 1999; 57:
308-312.</mixed-citation>
         </ref>
         <ref id="d766e489a1310">
            <label>12</label>
            <mixed-citation id="d766e496" publication-type="other">
Armah G, Pager CT, Asmah RH, et al. Prevalence of unusual human
rotavirus strains in Ghanaian children. J Med Virol 2001;63:67-71.</mixed-citation>
         </ref>
         <ref id="d766e506a1310">
            <label>13</label>
            <mixed-citation id="d766e513" publication-type="other">
Desselberger U, Iturriza-Gomara M, Gray JJ. Rotavirus epidemiology
and surveillance. Novartis Found Symp 2001;238:125-147.</mixed-citation>
         </ref>
         <ref id="d766e523a1310">
            <label>14</label>
            <mixed-citation id="d766e530" publication-type="other">
Heaton PM, Goveia MG, Miller JM, Offit TP, Clark HE Development
of a pentavalent rotavirus vaccines against prevalent serotypes of ro-
tavirus gastroenteristis. J Infec Dis 2005; 192(Suppl 1):S17-S21.</mixed-citation>
         </ref>
         <ref id="d766e543a1310">
            <label>15</label>
            <mixed-citation id="d766e550" publication-type="other">
Vesikari T, Matson DO, Dennehy SE, et al. Safety and efficacy of a
pentavalent human-bovine (WC3) reassortant rotavirus vaccines. N
Engl J Med 2006;354:23-33.</mixed-citation>
         </ref>
         <ref id="d766e563a1310">
            <label>16</label>
            <mixed-citation id="d766e570" publication-type="other">
De Vos B, Vesikari T, Linhares AC, et al. A rotavirus vaccine for pro-
phylaxis of infants against rotavirus gastroenteritis. Pediatr Infect Dis
J 2004;23(Suppl ):S179-S182.</mixed-citation>
         </ref>
         <ref id="d766e584a1310">
            <label>17</label>
            <mixed-citation id="d766e591" publication-type="other">
Ruiz-Palacios GM, Perez-Schael I, Velazquez FR, et al. Safety and ef-
ficacy of an attenuated vaccine against severe rotavirus gastroenteritis.
N Engl J Med 2006;354:11-22.</mixed-citation>
         </ref>
         <ref id="d766e604a1310">
            <label>18</label>
            <mixed-citation id="d766e611" publication-type="other">
Vesikari T, Karvonen A, Prymula R, et al. Efficacy of human rotavirus
vaccine against rotavirus gastroenteritis during the first 2 years of life
in European infants: randomised, double-blinded controlled study.
Lancet 2007;370:1757-1763.</mixed-citation>
         </ref>
         <ref id="d766e627a1310">
            <label>19</label>
            <mixed-citation id="d766e634" publication-type="other">
Steele AD, De Vos B, Tumbo J, et al. Co-administration study in South
African infants of a live-attenuated oral human rotavirus vaccine
(RIX4414) and poliovirus vaccines. Vaccine 2008 (Epub ahead of
print).</mixed-citation>
         </ref>
         <ref id="d766e650a1310">
            <label>20</label>
            <mixed-citation id="d766e657" publication-type="other">
Gouvea V, Glass RI, Woods P, et al. Polymerase chain reaction am-
plification and typing of rotavirus nucleic acids from stool specimens.
J Clin Microbiol 1990;28:276-282.</mixed-citation>
         </ref>
         <ref id="d766e670a1310">
            <label>21</label>
            <mixed-citation id="d766e677" publication-type="other">
Gentsch JR, Glass RI, Woods PA, et al. Identification of group A ro-
tavirus gene 4 by polymerase chain reaction. J Clin Microbiol 1992;
30:1365-1373.</mixed-citation>
         </ref>
         <ref id="d766e690a1310">
            <label>22</label>
            <mixed-citation id="d766e697" publication-type="other">
Das BK, Gentsch JR, Cicirello HG, et al. Characterization of rotavirus
strains from newborns in Delhi, India. J Clin Microbiol 1994; 32:
1820-1822.</mixed-citation>
         </ref>
         <ref id="d766e711a1310">
            <label>23</label>
            <mixed-citation id="d766e718" publication-type="other">
Mphahlele MJ, Steele AD. Relative frequency of human rotavirus VP4
(P) genotypes recovered over a ten-year period from South Africa. J
Med Virol 1999;47:1-5.</mixed-citation>
         </ref>
         <ref id="d766e731a1310">
            <label>24</label>
            <mixed-citation id="d766e738" publication-type="other">
Iturriza-Gomara M, Kang G, Gray J. Rotavirus genotyping: keeping
up with an evolving population of human rotaviruses. J Med Virol
2004;31:259-265.</mixed-citation>
         </ref>
         <ref id="d766e751a1310">
            <label>25</label>
            <mixed-citation id="d766e758" publication-type="other">
Banerjee I, Ramani S, Primrose B, et al. Modification of rotavirus
multiplex RT-PCR for the detection of G12 strains based on charac-
terization of emerging G12 rotavirus strains from South India. J Med
Virol 2007;79:1413-1421.</mixed-citation>
         </ref>
         <ref id="d766e774a1310">
            <label>26</label>
            <mixed-citation id="d766e781" publication-type="other">
Simmonds MK, Armah G, Asmah R, et al. New oligonucleotide primers
for P-typing of rotavirus trains: strategies for typing previously un-
typeable strains. J Clin Virol 2008;42:368-373.</mixed-citation>
         </ref>
         <ref id="d766e794a1310">
            <label>27</label>
            <mixed-citation id="d766e801" publication-type="other">
Gouvea V, Santos N, Timenetsky M do C. Identification of bovine and
porcine G types by PCR. J Clin Microbiol 1994;32:1338-1340.</mixed-citation>
         </ref>
         <ref id="d766e811a1310">
            <label>28</label>
            <mixed-citation id="d766e818" publication-type="other">
Gouvea V, Santos N, Timenetsky M do C. VP4 Typing of bovine and
porcine group A rotaviruses by PCR. J Clin Microbiol 1994; 32:
1333-1337.</mixed-citation>
         </ref>
         <ref id="d766e832a1310">
            <label>29</label>
            <mixed-citation id="d766e839" publication-type="other">
ChromasLite. Version 2.01. http://www.technelysium.com.au/chro-
mas_lite.html. Accessed 15 June 2008.</mixed-citation>
         </ref>
         <ref id="d766e849a1310">
            <label>30</label>
            <mixed-citation id="d766e856" publication-type="other">
Biological sequence alignment editor for win95/98/NT/2K/XP.http://
www.mbio.ncsu.edu/bioEdit/bioedit.html/. Accessed 15 June 2008.</mixed-citation>
         </ref>
         <ref id="d766e866a1310">
            <label>31</label>
            <mixed-citation id="d766e873" publication-type="other">
Page RMD. Treeview: an application to display phylogenetic trees on
personal computers. Comput App Biosc 1996; 12:357-358.</mixed-citation>
         </ref>
         <ref id="d766e883a1310">
            <label>32</label>
            <mixed-citation id="d766e890" publication-type="other">
Seheri LM, Dewar JB, van der Merwe L, et al. Prospective hospital-
based surveillance to estimate rotavirus disease burden in the Gauteng
and North West Province of South Africa during 2003-2005. 2010;
202(Suppl 1):S131-S138 (in this supplement).</mixed-citation>
         </ref>
         <ref id="d766e906a1310">
            <label>33</label>
            <mixed-citation id="d766e913" publication-type="other">
Page NA, Esona M, Armah G, et al. Emergence and characterization
of serotype G9 strains from across Africa. 2010;202(Suppl 1):S55-S63
(in this supplement).</mixed-citation>
         </ref>
         <ref id="d766e926a1310">
            <label>34</label>
            <mixed-citation id="d766e933" publication-type="other">
Page NA, de Beer MC, Seheri LM, Dewar JB, Steele AD. The detection
and molecular characterization of human G12 genotypes in South
Africa. J Med Virol 2009;81:106-113.</mixed-citation>
         </ref>
         <ref id="d766e947a1310">
            <label>35</label>
            <mixed-citation id="d766e954" publication-type="other">
Steele AD, van Niekerk MC, Mphahlele MJ. Geographic distribution
of human rotavirus VP4 genotypes and VP7 serotypes in five South
African regions. J Clin Microbiol 1995;33:1516-1519.</mixed-citation>
         </ref>
         <ref id="d766e967a1310">
            <label>36</label>
            <mixed-citation id="d766e974" publication-type="other">
Steele AD, Parker SP, Peenze I, Pager CT, Taylor MB, Cubitt WD.
Comparative studies of rotavirus serotype G8 strains recovered in South
Africa and United Kingdom. J Gen Virol 1999;80:3029-3034.</mixed-citation>
         </ref>
         <ref id="d766e987a1310">
            <label>37</label>
            <mixed-citation id="d766e994" publication-type="other">
Steele AD, Peenze I, de Beer M, et al. Anticipating rotavirus vaccines:
epidemiology and surveillance of rotavirus in South Africa. Vaccine
2003;21:354-360.</mixed-citation>
         </ref>
         <ref id="d766e1007a1310">
            <label>38</label>
            <mixed-citation id="d766e1014" publication-type="other">
Steele AD, Ivanoff B. Rotavirus strains circulating in Africa during
1996-1999: emergence of G9 strains and P[6] strains. Vaccine 2003;
21:361-367.</mixed-citation>
         </ref>
         <ref id="d766e1027a1310">
            <label>39</label>
            <mixed-citation id="d766e1034" publication-type="other">
Mwenda JM, Ntoto NKM, Abebe A, et al. Burden and epidemiology
of rotavirus diarrhea in selected African countries: preliminary results
from the African Rotavirus Surveillance Network. J Infect Dis 2010;
202(Suppl 1 ):S5—SI 1 (in this supplement).</mixed-citation>
         </ref>
         <ref id="d766e1050a1310">
            <label>40</label>
            <mixed-citation id="d766e1057" publication-type="other">
Gorziglia M, Green K, Nishikawa K, et al. Sequence of the fourth gene
of human rotaviruses recovered from asymptomatic or symptomatic
infections. J Virol 1988;62:2978-2984.</mixed-citation>
         </ref>
         <ref id="d766e1071a1310">
            <label>41</label>
            <mixed-citation id="d766e1078" publication-type="other">
Santos N, Riepenhoff-Talty M, Clark HF, Offit P, Gouvea V. VP4 ge-
notyping of human rotavirus in the United States. J Clin Microbiol
1994;32:205-208.</mixed-citation>
         </ref>
         <ref id="d766e1091a1310">
            <label>42</label>
            <mixed-citation id="d766e1098" publication-type="other">
Timenetsky MC, Santos N, Gouvea V. Survey of rotavirus G and P
types associated with human gastroenteritis in Sao Paulo, Brazil from
1986 to 1992. J Clin Microbiol 1994;32:2622-2624.</mixed-citation>
         </ref>
         <ref id="d766e1111a1310">
            <label>43</label>
            <mixed-citation id="d766e1118" publication-type="other">
Martella V, Banyai K, Ciarlet M, et al. Relationship among porcine
and human P[6] rotaviruses: Evidence that the different human P[6]
lineages have originated from multiple interspecies transmission events.
Virology 2006;344:509-519.</mixed-citation>
         </ref>
         <ref id="d766e1134a1310">
            <label>44</label>
            <mixed-citation id="d766e1141" publication-type="other">
Nguyen TA, Khamrin P, Trinh QD, et al. Sequence analysis of Viet-
namese P[6] rotavirus strains suggests evidendence of interspecies
transmission. J Med Virol 2007;79:1959-1965.</mixed-citation>
         </ref>
         <ref id="d766e1154a1310">
            <label>45</label>
            <mixed-citation id="d766e1161" publication-type="other">
Phan TG, Khamrin P, Quang TD, et al. Detection and genetic char-
acterization of group A rotavirus strains circulating among children
with acute gastroenteritis in Japan. J Virol 2007;81:4645-4653.</mixed-citation>
         </ref>
         <ref id="d766e1174a1310">
            <label>46</label>
            <mixed-citation id="d766e1181" publication-type="other">
Page NA, Steele AD. Antigenic and genetic characterization of serotype
G2 human rotavirus strains from South Africa. J Med Virol 2004; 72:
320-327.</mixed-citation>
         </ref>
         <ref id="d766e1195a1310">
            <label>47</label>
            <mixed-citation id="d766e1202" publication-type="other">
Wang Y, Kobayashi N, Zhou X, et al. Phylogenetic analysis of rota-
viruses with predominant G3 and emerging G9 genotypes from adults
and children in Wuhan, China. J Med Virol 2009;81:382-389.</mixed-citation>
         </ref>
         <ref id="d766e1215a1310">
            <label>48</label>
            <mixed-citation id="d766e1222" publication-type="other">
Phan TG, Trinh QD, Khamrin P, et al. Emergence of new variant
rotavirus G3 among infants and children with acute gastroenteritis in
Japan during 2003-2004. Clin Lab 2007;53:41-48.</mixed-citation>
         </ref>
         <ref id="d766e1235a1310">
            <label>49</label>
            <mixed-citation id="d766e1242" publication-type="other">
Pongsuwanna YR, Guntapong R, Chiwakul M, et al. Detection of hu-
man rotavirus with G12 and P[9] specificity in Thailand. J Clin Mi-
crobiol 2002;40:1390-1394.</mixed-citation>
         </ref>
         <ref id="d766e1255a1310">
            <label>50</label>
            <mixed-citation id="d766e1262" publication-type="other">
Shinozaki K, Okda M, Nagashima S, Kaiho I, Taniguchi K. Charac-
terization of human rotavirus strains with G12 and P[9] detected in
Japan. J Med Virol 2004;73:612-616.</mixed-citation>
         </ref>
         <ref id="d766e1275a1310">
            <label>51</label>
            <mixed-citation id="d766e1282" publication-type="other">
Samajdar S, Varghese V, Barman P, et al. Changing pattern of human
group A rotaviruses emergence of G12 as an important pathogen
among children in eastern India. J Clin Virol 2006;36:183-188.</mixed-citation>
         </ref>
         <ref id="d766e1295a1310">
            <label>52</label>
            <mixed-citation id="d766e1302" publication-type="other">
Castello AA, Arguelles MH, Rota RP, et al. Molecular epidemiology of
group A rotavirus diarrhoea among children in Buenos Aires Argentina
from 1999 to 2003 and emergence of the infrequent genotypes G12. J
Clin Microbiol 2006;44:2046-2050.</mixed-citation>
         </ref>
         <ref id="d766e1319a1310">
            <label>53</label>
            <mixed-citation id="d766e1326" publication-type="other">
Rahman M, Matthijnssens J, Yang X, et al. Evolutionary history and
global spread of the emerging G12 human rotaviruses. J Virol 2007;
81:2382-2390.</mixed-citation>
         </ref>
         <ref id="d766e1339a1310">
            <label>54</label>
            <mixed-citation id="d766e1346" publication-type="other">
Fischer TK, Steinsland H, Molbak K, et al. Genotype profiles of ro-
tavirus strains from children in a suburban community in Guinea-
Bissau, Western Africa. J Clin Microbiol 2000; 38:264-267.</mixed-citation>
         </ref>
         <ref id="d766e1359a1310">
            <label>55</label>
            <mixed-citation id="d766e1366" publication-type="other">
Adah MI, Wade A, Taniguchi K. Molecular epidemiology of rotaviruses
in Nigeria: detection of unusual strains with G2P[6] and G8P[11]
specificities. J Clin Microbiol 2001;39:3969-3975.</mixed-citation>
         </ref>
         <ref id="d766e1379a1310">
            <label>56</label>
            <mixed-citation id="d766e1386" publication-type="other">
Iturriza-Gomara M, Isherwood B, Desselberger U, Gray J. Reassort-
ment in vivo: driving force for diversity of human rotavirus strains
isolated in the United Kingdom between 1995-1999. J Virol 2001; 75:
3696-3705.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
