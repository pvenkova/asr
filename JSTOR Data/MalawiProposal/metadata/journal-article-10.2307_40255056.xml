<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">jinfedise</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000007</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>The Journal of Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Chicago Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00221899</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">40255056</article-id>
         <article-id pub-id-type="pub-doi">10.1086/604731</article-id>
         <article-categories>
            <subj-group>
               <subject>Major Articles and Brief Reports</subject>
               <subj-group>
                  <subject>HIV/AIDS</subject>
               </subj-group>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Rate of Accumulation of Thymidine Analogue Mutations in Patients Continuing to Receive Virologically Failing Regimens Containing Zidovudine or Stavudine: Implications for Antiretroviral Therapy Programs in Resource-Limited Settings</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Alessandro</given-names>
                  <surname>Cozzi-Lepri</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Andrew N.</given-names>
                  <surname>Phillips</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Javier</given-names>
                  <surname>Martinez-Picado</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Antonella d'Arminio</given-names>
                  <surname>Monforte</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Christine</given-names>
                  <surname>Katlama</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Ann-Brit Eg</given-names>
                  <surname>Hansen</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Andrzej</given-names>
                  <surname>Horban</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Johann</given-names>
                  <surname>Bruun</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Bonaventura</given-names>
                  <surname>Clotet</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jens D.</given-names>
                  <surname>Lundgren</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>9</month>
            <year>2009</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">200</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">5</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40009945</issue-id>
         <fpage>687</fpage>
         <lpage>697</lpage>
         <permissions>
            <copyright-statement>Copyright 2009 Infectious Diseases Society of America</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/40255056"/>
         <abstract>
            <p>Background. Because changes in antiretroviral therapy in resource-limited settings (RLSs) are delayed until patients experience immunological or clinical failure, it is important to be able to estimate the consequences in terms of accumulation of thymidine analogue (TA) mutations (TAMs). Methods. The study included patients in EuroSIDA with ≥2 available genotypic resistance tests (GRTs) (human immunodeficiency virus [ HIV] RNA level, &gt; 500 copies/ mL in any measure between tests), provided that the first GRT was performed after the first virological failure of a TA and that the same TA was continued until the second GRT. Results. At the time of the first GRT in a pair (t0), 1 year after virological failure, a median of 3 TAMs were detected, mutations 41L and 215Y in 65% of pairs and 67N in 52%. Overall, 126 TAMs were accumulated during 548 person-years of follow-up (PYFUs) (1/ 4.3 years; 95% confidence interval, 3.7-5.0 years). Greater predicted activity of the TA at tO, TAM profile 2 (TAM2; vs TAM profile 1 [ TAM1]) profiles at t0, use of a nonnucleoside reverse-transcriptase inhibitor (NNRTI) at t0 (vs combined NNRTI and protease inhibitor), and acquisition of HIV infection through heterosexual (vs homosexual) contacts were associated with a faster rate of TAM accumulation. Conclusions. Although the estimated rate of TAM accumulation was lower than anticipated, all possible efforts should be continued to increase the availability of drug options in RLSs.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d2549e307a1310">
            <label>1</label>
            <mixed-citation id="d2549e314" publication-type="other">
World Health Organization (WHO). Antiretroviral therapy for HIV
infection in adults and adolescents: recommendations for a public
health approach. Geneva, Switzerland: WHO, 2006 revision. 22 May
2007. Available at http://www.who.int/hiv/pub/guidelines/artadult
guidelines.pdf. Accessed March 2009.</mixed-citation>
         </ref>
         <ref id="d2549e333a1310">
            <label>2</label>
            <mixed-citation id="d2549e340" publication-type="other">
Siegfried NL, Van Deventer PJ, Mahomed FA, Rutherford GW. Sta-
vudine, lamivudine and nevirapine combination therapy for treatment
of HIV infection and AIDS in adults. Cochrane Database Syst Rev
2006:CD004535.</mixed-citation>
         </ref>
         <ref id="d2549e356a1310">
            <label>3</label>
            <mixed-citation id="d2549e363" publication-type="other">
Tam LW, Hogg RS, Yip B, Montaner JS, Harrigan PR, Brumme CJ.
Performance of a World Health Organization first-line regimen (sta-
vudine/lamivudine/nevirapine) in antiretroviral-naïve individuals in a
Western setting. HIV Med 2007; 8:267-70.</mixed-citation>
         </ref>
         <ref id="d2549e379a1310">
            <label>4</label>
            <mixed-citation id="d2549e386" publication-type="other">
Sungkanuparph S, Manosuthi W, Kiertiburanakul S, Piyavong B,
Chumpathat N, Chantratita W. Options for a second-line antiretroviral
regimen for HIV type 1 -infected patients whose initial regimen of a
fixed-dose combination of stavudine, lamivudine, and nevirapine fails.
Clin Infect Dis 2007; 44:447-52.</mixed-citation>
         </ref>
         <ref id="d2549e406a1310">
            <label>5</label>
            <mixed-citation id="d2549e413" publication-type="other">
Kamya MR, Mayanja-Kizza H, Kambugu A, et al. Predictors of long-
term viral failure among Ugandan children and adults treated with
antiretroviral therapy. Academic Alliance for AIDS Care and Prevention
in Africa. J Acquir Immune Defic Syndr 2007; 46: 187-93.</mixed-citation>
         </ref>
         <ref id="d2549e429a1310">
            <label>6</label>
            <mixed-citation id="d2549e436" publication-type="other">
Ferradini L, Jeannin A, Pinoges L, et al. Scaling up of highly active
antiretroviral therapy in a rural district of Malawi: an effectiveness
assessment. Lancet 2006; 367:1335-42.</mixed-citation>
         </ref>
         <ref id="d2549e449a1310">
            <label>7</label>
            <mixed-citation id="d2549e456" publication-type="other">
Laurent C, Bourgeois A, Mpoudi-Ngolé E, et al. Tolerability and ef-
fectiveness of first-line regimens combining nevirapine and lamivudine
plus zidovudine or stavudine in Cameroon. AIDS Res Hum Retrovi-
ruses 2008; 24:393-400.</mixed-citation>
         </ref>
         <ref id="d2549e472a1310">
            <label>8</label>
            <mixed-citation id="d2549e479" publication-type="other">
Laurent C, Kouanfack C, Koulla-Shiro S, et al. Long-term safety, ef-
fectiveness and quality of a generic fixed-dose combination of nevi-
rapine, stavudine and lamivudine. AIDS 2007; 21:768-71.</mixed-citation>
         </ref>
         <ref id="d2549e492a1310">
            <label>9</label>
            <mixed-citation id="d2549e499" publication-type="other">
Deenan Pillay, C Kityo, Robertson V, et al. Emergence and evolution
of drug resistance in the absence of viral load monitoring during 48
weeks of combivir/tenofovir within the DART trial [abstract 642].
DART Virology Group and Trial Team. In: 14th Conference on Ret-
roviruses and Opportunistic Infections (Los Angeles). Alexandria, VA:
Foundation for Retrovirology and Human Health, 2007.</mixed-citation>
         </ref>
         <ref id="d2549e522a1310">
            <label>10</label>
            <mixed-citation id="d2549e529" publication-type="other">
Ndembi N, Pillay D, Goodall R, et al. Differences in the dynamics
of viral rebound and evolution of resistance between CBV/NVP and
CBV/ABC uncovered in the absence of viral load monitoring in real
time: NORA substudy of the DART trial [abstract 889]. DART Virol-
ogy. In: 15th Conference on Retroviruses and Opportunistic Infec-
tions (Boston). Alexandria, VA: Foundation for Retrovirology and Hu-
man Health, 2008.</mixed-citation>
         </ref>
         <ref id="d2549e556a1310">
            <label>11</label>
            <mixed-citation id="d2549e563" publication-type="other">
Marconi VC, Sunpath H, Lu Z, et al. Prevalence of HIV-1 drug resis-
tance after failure of a first highly active antiretroviral therapy regimen
in KwaZulu Natal, South Africa. South Africa Resistance Cohort Study
Team. Clin Infect Dis 2008; 46: 1589-97.</mixed-citation>
         </ref>
         <ref id="d2549e579a1310">
            <label>12</label>
            <mixed-citation id="d2549e586" publication-type="other">
Goetz MB, Ferguson MR, Han X, et al. Evolution of HIV resistance
mutations in patients maintained on a stable treatment regimen after
virologie failure. J Acquir Immune Defic Syndr 2006; 43:541-9.</mixed-citation>
         </ref>
         <ref id="d2549e599a1310">
            <label>13</label>
            <mixed-citation id="d2549e606" publication-type="other">
Braithwaite RS, Shechter S, Chang CC, Schaefer A, Roberts MS. Es-
timating the rate of accumulating drug resistance mutations in the HIV
genome. Value Health 2007; 10:204-13.</mixed-citation>
         </ref>
         <ref id="d2549e619a1310">
            <label>14</label>
            <mixed-citation id="d2549e626" publication-type="other">
Doualla-Bell F, Avalos A, Brenner B, et al. High prevalence of the K65R
mutation in human immunodeficiency virus type 1 subtype C isolates
from infected patients in Botswana treated with didanosine-based reg-
imens. Antimicrob Agents Chemother 2006; 50:4182-5.</mixed-citation>
         </ref>
         <ref id="d2549e642a1310">
            <label>15</label>
            <mixed-citation id="d2549e649" publication-type="other">
Hosseinipour M, van Oosterhout JJ, Weigel R, et al. Resistance profile
of patients failing first line ART in Malawi when using clinical and
immunologie monitoring [abstract TUAB0104]. In: XVII International
AIDS Conference (Mexico City). Stockholm: International AIDS So-
ciety, 2008.</mixed-citation>
         </ref>
         <ref id="d2549e668a1310">
            <label>16</label>
            <mixed-citation id="d2549e675" publication-type="other">
Laboratory for Clinical and Evolutionary Virology. The Rega algo-
rithms. 3 February 2009. Available at http://www.rega.kuleuven.be/cev/
index.php?id = 30. Accessed March 2009.</mixed-citation>
         </ref>
         <ref id="d2549e689a1310">
            <label>17</label>
            <mixed-citation id="d2549e696" publication-type="other">
Zeger SL, Liang KY. Longitudinal data analysis for discrete and con-
tinuous outcomes. Biometrics 1986; 42:121-30.</mixed-citation>
         </ref>
         <ref id="d2549e706a1310">
            <label>18</label>
            <mixed-citation id="d2549e713" publication-type="other">
HIV French Resistance web site. HIV-1 genotypic drug resistance in-
terpretation's algorithms. February 2008. Available at http://www.hiv
frenchresistance.org/. Accessed March 2009.</mixed-citation>
         </ref>
         <ref id="d2549e726a1310">
            <label>19</label>
            <mixed-citation id="d2549e733" publication-type="other">
Cozzi-Lepri A. Initiatives for developing and comparing genotype in-
terpretation systems: external validation of existing rule-based inter-
pretation systems for abacavir against virological response. Standard-
ization and Clinical Relevance of HIV Drug Resistance Testing Project
for the Forum for Collaborative HIV Research. HIV Med 2008; 9:27-40.</mixed-citation>
         </ref>
         <ref id="d2549e752a1310">
            <label>20</label>
            <mixed-citation id="d2549e759" publication-type="other">
Kuritzkes DR, Bassett RL, Hazelwood JD, et al. Rate of thymidine
analogue resistance mutation accumulation with zidovudine- or sta-
vudine-based regimens. Adult ACTG Protocol 306 370 Teams. J Acquir
Immune Defic Syndr 2004; 36:600-3.</mixed-citation>
         </ref>
         <ref id="d2549e775a1310">
            <label>21</label>
            <mixed-citation id="d2549e782" publication-type="other">
Aleman S, Söderbärg K, Visco-Comandini U, Sitbon G, Sönnerborg
A. Drug resistance at low viraemia in HIV-1 -infected patients with
antiretroviral combination therapy. AIDS 2002; 16:1039-44.</mixed-citation>
         </ref>
         <ref id="d2549e795a1310">
            <label>22</label>
            <mixed-citation id="d2549e802" publication-type="other">
Kantor R, Shafer RW, Follansbee S, et al. Evolution of resistance to
drugs in HIV-1 -infected patients failing antiretroviral therapy. AIDS
2004; 18:1503-11.</mixed-citation>
         </ref>
         <ref id="d2549e816a1310">
            <label>23</label>
            <mixed-citation id="d2549e823" publication-type="other">
Napravnik S, Edwards D, Stewart P, Stalzer B, Matteson E, Eron JJ Jr.
HIV-1 drug resistance evolution among patients on potent combina-
tion antiretroviral therapy with detectable viremia. J Acquir Immune
Defic Syndr 2005; 40:34-40.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
