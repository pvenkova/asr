<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">plantandsoil</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50010412</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Plant and Soil</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Springer</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">0032079X</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15735036</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">42951856</article-id>
         <title-group>
            <article-title>Meta-analysis of maize yield response to woody and herbaceous legumes in sub-Saharan Africa</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Gudeta</given-names>
                  <surname>Sileshi</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Festus K.</given-names>
                  <surname>Akinnifesi</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Oluyede C.</given-names>
                  <surname>Ajayi</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Frank</given-names>
                  <surname>Place</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>6</month>
            <year>2008</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">307</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1/2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40113705</issue-id>
         <fpage>1</fpage>
         <lpage>19</lpage>
         <permissions>
            <copyright-statement>© 2008 Springer</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/42951856"/>
         <abstract>
            <p>A number of studies have tested the effect of woody and herbaceous legumes on soil fertility and maize yields in sub-Saharan Africa. However, their effects on maize productivity are much debated because results have been variable. A meta-analysis was conducted with the aim of evaluating the evidence in support of yield benefits from woody and herbaceous green manure legumes. A total of 94 peerreviewed publications from West, East and southern Africa qualified for inclusion in the analysis. Maize yield from herbaceous green manure legumes (54 publications), non-coppicing legumes (48 publications), coppicing woody legumes (10 publications), natural fallows (29 publications), and fully fertilized monoculture maize (52 publications) were compared. Mixed linear modelling using yield differences (D) and response ratios (RR) indicated that the response to legumes is positive. The mean yield increase (D) over unfertilized maize was highest (2.3 t ha⁻¹) and least variable (CV= 70%) in fully fertilized maize, while it was lowest (0.3 t ha⁻¹) and most variable (CV= 229%) in natural fallows. The increase in yield over unfertilized maize was 1.6 t ha⁻¹ with coppicing woody legumes, 1.3 t ha⁻¹ with non-coppicing woody legumes and 0.8 t ha⁻¹ with herbaceous green manure legumes. Doubling and tripling of yields relative to the control (RR &gt; 2) was recorded in coppicing species (67% of the cases), non-coppicing legumes (45% of the cases), herbaceous green manure legumes (16% of the cases) and natural fallows (19% of the cases). However, doubling or tripling of yields occurred only in low and medium potential sites. Amending post-fallow plots with 50% of the recommended fertilizer dose further increased yields by over 25% indicating that legume rotations may play an important role in reducing fertilizer requirements. Except with the natural fallow, the 95% confidence intervals of D and RR were higher than 1 and 0, respectively indicating significant and positive response to treatments. Therefore, it is concluded that the global maize yield response to legumes is significantly positive and higher than unfertilized maize and natural vegetation fallows.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d50e232a1310">
            <mixed-citation id="d50e236" publication-type="other">
Adu-Gyamfi JJ, Myaka FA, Sakala WD, Odgaard R, Vesterager
JM, Høgh-Jensen H (2007) Biological nitrogen fixation
and nitrogen and phosphorus budgets in farmer-managed
intercrops of maize-pigeonpea in semi-arid southern and
eastern Africa. Plant Soil 295:127-136</mixed-citation>
         </ref>
         <ref id="d50e255a1310">
            <mixed-citation id="d50e259" publication-type="other">
Ajayi OC, Akinnifesi FK, Sileshi G, Chakeredza S (2007)
Adoption of renewable soil fertility replenishment tech-
nologies in southern African region: lessons learnt and
way forward. Nat Resour Forum 31:306-317</mixed-citation>
         </ref>
         <ref id="d50e275a1310">
            <mixed-citation id="d50e279" publication-type="other">
Akinnifesi FK, Makumba W, Sileshi G, Ajayi O, Mweta D
(2007) Synergistic effect of inorganic N and P fertilizers
and organic inputs from Gliricidia sepium on productivity
of intercropped maize in Southern Malawi. Plant Soil
294:203-217</mixed-citation>
         </ref>
         <ref id="d50e298a1310">
            <mixed-citation id="d50e302" publication-type="other">
Akobundu IO, Udensi UE, Chikoye D (2000) Velvetbean
(Mucuna spp.) suppresses speargrass (Imperata cylindrica)
and increases maize yield. Int J Pest Manage 46:103-108</mixed-citation>
         </ref>
         <ref id="d50e316a1310">
            <mixed-citation id="d50e320" publication-type="other">
Arim OJ, Waceke JW, Waudo SW, Kimenju JW (2006) Effects
of Canavalia ensiformis and Mucuna pruriens intercrops
on Pratylenchus zeae damage and yield of maize in
subsistence agriculture. Plant Soil 284:243-251</mixed-citation>
         </ref>
         <ref id="d50e336a1310">
            <mixed-citation id="d50e340" publication-type="other">
Arnqvist G, Wooter D (1995) Meta-analysis: synthesizing
research findings in ecology and evolution. Trend Ecol
Evol 10:236-240</mixed-citation>
         </ref>
         <ref id="d50e353a1310">
            <mixed-citation id="d50e357" publication-type="other">
Bógale T, Assenga RH, Mbaga TE, Friesen DK, Kikafunda J,
Ransom JK (2001) Legume fallows for maize based
systems in eastern Africa: Contribution of legumes to
enhancd maize productivity and reduced nitrogen require-
ments. Proceedings of the Seventh Eastern and Southern
Africa Regional Maize Conference, pp 324-329</mixed-citation>
         </ref>
         <ref id="d50e380a1310">
            <mixed-citation id="d50e384" publication-type="other">
Byerlee D, Anandajayasekeram P, Diallo A, Gelaw B, Heisey
PW, Lopez-Pereira M, Mwangi W, Smale M, Tripp R,
Waddington S (1994) Maize research in Sub-Saharan
Africa: an overview of past impacts and future prospects.
CIMMYT Economics Working Paper 94-03. Mexico, DF,
Mexico.</mixed-citation>
         </ref>
         <ref id="d50e407a1310">
            <mixed-citation id="d50e411" publication-type="other">
Cherr CM, Scholberg JM, McSorley R (2006) Green manure
approaches to crop production: a synthesis. Agron J
98:302-319</mixed-citation>
         </ref>
         <ref id="d50e424a1310">
            <mixed-citation id="d50e428" publication-type="other">
Chikowo R, Mapfumo P, Nyamugafata P, Giller KE (2004)
Maize productivity and mineral N dynamics following
different soil fertility management practices on a depleted
sandy soil in Zimbabwe. Agrie Ecosyst Environ 102:119-
131</mixed-citation>
         </ref>
         <ref id="d50e448a1310">
            <mixed-citation id="d50e452" publication-type="other">
Chintu R, Mafongoya PL, Chirwa TS, Mwale M, Matibini J
(2004) Subsoil nitrogen dynamics as affected by planted
coppicing tree legume fallows in eastern Zambia. Exp
Agric 40:327-340</mixed-citation>
         </ref>
         <ref id="d50e468a1310">
            <mixed-citation id="d50e472" publication-type="other">
Chirwa PW, Ong CK, Maghembe J, Black CR (2007) Soil
water dynamics in intercropping systems containing
Gliricidia sepium, pigeon pea and maize in southern
Malawi. Agroforest Syst 69:29-43</mixed-citation>
         </ref>
         <ref id="d50e488a1310">
            <mixed-citation id="d50e492" publication-type="other">
Drechsel P, Steiner KG, Hagedorn F (1996) A review on the
potential of improved fallows and green manure in
Rwanda. Agroforest Syst 33:109-136</mixed-citation>
         </ref>
         <ref id="d50e505a1310">
            <mixed-citation id="d50e509" publication-type="other">
Gacheru E, Rao MR (2005) The potential of planted shrub
fallows to combat Striga infestation on maize. Int J Pest
Manage 51:91-100</mixed-citation>
         </ref>
         <ref id="d50e522a1310">
            <mixed-citation id="d50e526" publication-type="other">
Gates S (2002) Review of methodology of quantitative reviews
using meta-analysis in ecology. J Anim Ecol 71:547-557</mixed-citation>
         </ref>
         <ref id="d50e536a1310">
            <mixed-citation id="d50e540" publication-type="other">
Giller KE, Cadish G, Ehaliotis C, Adams A, Sakala D,
Mafongoya PL (1997) Building soil nitrogen capital in
Africa. In: Buresh RJ, Sanchez PA, Calhoun F (eds)
Replenishing soil fertility in Africa. SSSA Special Publi-
cation no. 51, Madison, WI, USA, pp 151-192</mixed-citation>
         </ref>
         <ref id="d50e560a1310">
            <mixed-citation id="d50e564" publication-type="other">
Gladwin CH (1991) Gendered impacts of fertilizer subsidy
removal programs in Malawi and Cameroon. Agric Econ
7:141-153</mixed-citation>
         </ref>
         <ref id="d50e577a1310">
            <mixed-citation id="d50e581" publication-type="other">
Gurevitch J, Hedges LV (1999) Statistical issues in ecological
meta-analyses. Ecology 80:1142-1149</mixed-citation>
         </ref>
         <ref id="d50e591a1310">
            <mixed-citation id="d50e595" publication-type="other">
Hammeed KA (1976) The oil revolution and African develop-
ment. Afr Aff 75:349-358</mixed-citation>
         </ref>
         <ref id="d50e605a1310">
            <mixed-citation id="d50e609" publication-type="other">
Hartemink AE, Buresh RJ, Jama B, Janssen BH (1996) Soil
nitrate and water dynamics in Sesbania fallows, weed
fallows, and maize. Soil Sci Soc Am J 60:568-574</mixed-citation>
         </ref>
         <ref id="d50e622a1310">
            <mixed-citation id="d50e626" publication-type="other">
Hauser S, Nolte C, Carsky RJ (2006) What role can planted
fallows play in the humid and sub-humid zone of West and
Central Africa? Nutr Cycl Agroecosyst 76:297-318</mixed-citation>
         </ref>
         <ref id="d50e639a1310">
            <mixed-citation id="d50e643" publication-type="other">
Hedges LV, Gurevitch J, Curtis PS (1999) The meta-analysis of
response ratios in experimental ecology. Ecology
80:1150-1156</mixed-citation>
         </ref>
         <ref id="d50e657a1310">
            <mixed-citation id="d50e661" publication-type="other">
Juo ASR, Dabiri A, Franzluebbers K (1995) Acidification of a
kaolinitic Alfisol under continuous cropping and nitrogen
fertilization in West Africa. Plant Soil 171:245-253</mixed-citation>
         </ref>
         <ref id="d50e674a1310">
            <mixed-citation id="d50e678" publication-type="other">
Kaizzi CK, Ssali H, Vlek PLG (2004) The potential of velvet
bean (Mucuna pruriens) and N fertilizers in maize
production on contrasting soils and agro-ecological zones
of East Uganda. Nutr Cycl Agroecosyst 68:59-72</mixed-citation>
         </ref>
         <ref id="d50e694a1310">
            <mixed-citation id="d50e698" publication-type="other">
Kang BT, Balasubramanian V (1990) Long term fertilizer trials
on Alfisols in West Africa. In Transactions of XIV
International Soil Science Congress, Kyoto, Japan (vol.
4) ISSS, Kyoto, Japan</mixed-citation>
         </ref>
         <ref id="d50e714a1310">
            <mixed-citation id="d50e718" publication-type="other">
Khan ZR, Pickett JA, Wadhams LJ, Hassanali A, Midega CAO
(2006) Combined control of Striga and stemborers in
mmzor-Desmodium spp. intercrops. Crop Prot 25:989-995</mixed-citation>
         </ref>
         <ref id="d50e731a1310">
            <mixed-citation id="d50e735" publication-type="other">
LeMare PH, Pereira J, Goedert WJ (1987) Effects of green
manure on isotopically exchangeable phosphate in a dark-
red latosol in Brazil. J Soil Sci 38:199-209</mixed-citation>
         </ref>
         <ref id="d50e748a1310">
            <mixed-citation id="d50e752" publication-type="other">
Lortie CJ, Dyer AR (1999) Over-interpretation: avoiding the
stigma of non-significance. Oikos 87:183-184</mixed-citation>
         </ref>
         <ref id="d50e763a1310">
            <mixed-citation id="d50e767" publication-type="other">
Lortie CJ, Callaway RM (2006) Re-analysis of meta-analysis:
support for the stress-gradient hypothesis. J Ecol 94:7-16</mixed-citation>
         </ref>
         <ref id="d50e777a1310">
            <mixed-citation id="d50e781" publication-type="other">
Mafongoya PL, Kuntashula E, Sileshi G (2006) Managing soil
fertility and nutrient cycles through fertilizer trees in
southern Africa. In: Uphoff N, Ball AS, Fernandes E,
Herren H, Husson O, Liang M, Palm C, Pretty J, Sanchez
P, Sanginga N, Thies J (eds) Biological approaches to
sustainable soil systems. Taylor &amp; Francis, New York, pp
273-289</mixed-citation>
         </ref>
         <ref id="d50e807a1310">
            <mixed-citation id="d50e811" publication-type="other">
Mekonnen K, Buresh RJ, Jama B (1997) Root and inorganic
nitrogen distributions in sesbania fallow, natural fallow
and maize fields. Plant Soil 188:319-327</mixed-citation>
         </ref>
         <ref id="d50e824a1310">
            <mixed-citation id="d50e828" publication-type="other">
Miguez FE, Bollero GA (2005) Review of corn yield response
under winter cover cropping systems using meta-analytic
methods. Crop Sci 45:2318-2329</mixed-citation>
         </ref>
         <ref id="d50e841a1310">
            <mixed-citation id="d50e845" publication-type="other">
Mureithi JG, Gachene CKK, Ojiem J (2003) The role of green
manure legumes in smallholder farming systems in Kenya:
The Legume Research Network Project. Trop Subtrop
Agroecosyst 1:57-70</mixed-citation>
         </ref>
         <ref id="d50e861a1310">
            <mixed-citation id="d50e865" publication-type="other">
Mwangi WM (1999) Low use of fertilizers and low productiv-
ity in sub-Saharan Africa. Nutr Cycl Agroecosyst 47:135-
147</mixed-citation>
         </ref>
         <ref id="d50e879a1310">
            <mixed-citation id="d50e883" publication-type="other">
Nyamadzawo G, Nyamugafata P, Chikowo R., Giller K (2008)
Residual effects of fallows on selected soil hydraulic
properties in a kaolinitic soil subjected to conventional
tillage (CT) and no tillage (NT). Agroforest Syst 72:161-
168</mixed-citation>
         </ref>
         <ref id="d50e902a1310">
            <mixed-citation id="d50e906" publication-type="other">
Ojiem JO, Vanlauwe B, de Ridder N, Giller KE (2007) Niche-
based assessment of contributions of legumes to the
nitrogen economy of Western Kenya smallholder farms.
Plant Soil 292:119-135</mixed-citation>
         </ref>
         <ref id="d50e922a1310">
            <mixed-citation id="d50e926" publication-type="other">
Osenberg CW, Sarnelle O, Cooper SD, Holt RD (1999)
Resolving ecological questions through meta-analysis:
goals, metrics, and models. Ecology 80:1105-1117</mixed-citation>
         </ref>
         <ref id="d50e939a1310">
            <mixed-citation id="d50e943" publication-type="other">
Phiri E, Verplancke H, Kwesiga F, Mafongoya P (2003) Water
balance and maize yield following improved sesbania
fallow in eastern Zambia. Agroforest Syst 59:197-205</mixed-citation>
         </ref>
         <ref id="d50e956a1310">
            <mixed-citation id="d50e960" publication-type="other">
Randhawa PS, Condron LM, Di HG, Sinaj S, McLenaghen RD
(2005) Effect of green manure addition on soil organic
phosphorus mineralization. Nutr Cycl Agroecosyst 73:
181-189</mixed-citation>
         </ref>
         <ref id="d50e976a1310">
            <mixed-citation id="d50e980" publication-type="other">
Rao MR, Nair PKR, Ong CK (1998) Biophysical interactions
in tropical agroforestry systems. Agroforest Syst 38:3-50</mixed-citation>
         </ref>
         <ref id="d50e991a1310">
            <mixed-citation id="d50e995" publication-type="other">
Sanchez PA (1999) Improved fallow come of age in the tropics.
Agroforest Syst 47:3-12</mixed-citation>
         </ref>
         <ref id="d50e1005a1310">
            <mixed-citation id="d50e1009" publication-type="other">
Sanchez PA (2002) Soil fertility and hunger in Africa. Science
295:2019-2020</mixed-citation>
         </ref>
         <ref id="d50e1019a1310">
            <mixed-citation id="d50e1023" publication-type="other">
Sileshi G, Mafongoya PL (2003) Effect of rotational fallows on
abundance of soil insects and weeds in maize crops in
eastern Zambia. Appl Soil Ecol 23:211-222</mixed-citation>
         </ref>
         <ref id="d50e1036a1310">
            <mixed-citation id="d50e1040" publication-type="other">
Sileshi G, Mafongoya PL, Kwesiga F, Nkunika P (2005)
Termite damage to maize grown in agroforestry systems,
traditional fallows and monoculture on nitrogen-limited
soils in eastern Zambia. Agric For Entomol 7:61-69</mixed-citation>
         </ref>
         <ref id="d50e1056a1310">
            <mixed-citation id="d50e1060" publication-type="other">
Sileshi G, Kuntashula E, Mafongoya PL (2006) Effect of
improved fallows on weed infestation in maize in eastern
Zambia. Zambia J Agric Sci 8:6-12</mixed-citation>
         </ref>
         <ref id="d50e1073a1310">
            <mixed-citation id="d50e1079" publication-type="other">
Sileshi G, Schroth G, Rao MR, Girma H (2008) Weeds,
diseases, insect pests and tri-trophic interactions in tropical
agroforestry. In: Batish DR, Kohli RK, Jose S, Singh HP
(eds) Ecological basis of agroforestry. CRC Press, Boca
Raton, FL, pp 73-94</mixed-citation>
         </ref>
         <ref id="d50e1099a1310">
            <mixed-citation id="d50e1103" publication-type="other">
Sim J, Reid N (1999) Statistical inference by confidence
intervals: Issues of interpretation and utilization. Phys
Ther 79:186-195</mixed-citation>
         </ref>
         <ref id="d50e1116a1310">
            <mixed-citation id="d50e1120" publication-type="other">
Smale M (1995) "Maize is life": Malawi's delayed green
revolution. World Dev 23:819-831</mixed-citation>
         </ref>
         <ref id="d50e1130a1310">
            <mixed-citation id="d50e1134" publication-type="other">
Snapp SS, Mafongoya PL, Waddington SR (1998) Organic
matter technologies to improve nutrient cycling in small
holder cropping systems of southern Africa. Agric Ecosyst
Environ 71:187-202</mixed-citation>
         </ref>
         <ref id="d50e1150a1310">
            <mixed-citation id="d50e1154" publication-type="other">
Stocking MA, Murnaghan N (2001) Handbook for the field
assessment of land degradation. Earthscan, London, UK,
p 176</mixed-citation>
         </ref>
         <ref id="d50e1167a1310">
            <mixed-citation id="d50e1171" publication-type="other">
Styger E, Fernandes ECM (2006) Contribution of managed
fallows to soil fertility recovery. In: Uphoff N, Ball AS,
Fernandes E, Herren H, Husson O, Liang M, Palm C,
Pretty J, Sanchez P, Sanginga N, Thies J (eds) Biological
approaches to sustainable soil systems. Taylor &amp; Francis,
New York, pp 425-437</mixed-citation>
         </ref>
         <ref id="d50e1194a1310">
            <mixed-citation id="d50e1198" publication-type="other">
Szott LT, Palm CA, Buresh RJ (1999) Ecosystem fertility and
fallow function in the humid and subhumid tropics.
Agrofor Syst 47:163-196</mixed-citation>
         </ref>
         <ref id="d50e1212a1310">
            <mixed-citation id="d50e1216" publication-type="other">
Tonitto C, David MB, Drinkwater LE (2006) Replacing bare
fallows with cover crops in fertilizer-intensive cropping
systems: a meta-analysis of crop yield and N dynamics.
Agric Ecosyst Environ 112:58-72</mixed-citation>
         </ref>
         <ref id="d50e1232a1310">
            <mixed-citation id="d50e1236" publication-type="other">
Vanlauwe B, Aihou K, Aman S, Iwuafor ENO, Tossah BK,
Diels J, Sanginga N, Lyasse O, Merckx R, Deckers J
(2001) Maize yield as affected by organic inputs and urea
in the West African moist savanna. Agron J 93:1191-1199</mixed-citation>
         </ref>
         <ref id="d50e1252a1310">
            <mixed-citation id="d50e1256" publication-type="other">
Wang MC, Bushman BJ (1998) Using the normal quantile
plot to explore meta-analytic data sets. Psychol Method
3:46-54</mixed-citation>
         </ref>
         <ref id="d50e1269a1310">
            <mixed-citation id="d50e1273" publication-type="other">
Wang KH, McSorley R, Gallaher RN (2003) Effect of
Crotalaria juncea amendment on nematode communities
in soil with different agricultural histories. J Nematol
35:294-301</mixed-citation>
         </ref>
         <ref id="d50e1289a1310">
            <mixed-citation id="d50e1293" publication-type="other">
Wortmann CS, Kaizzi CK (2000) Tree legumes in medium-
term fallows: nitrogen fixation, nitrate recovery and effects
on subsequent crops. Afr Crop Sci J 8:263-272</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
