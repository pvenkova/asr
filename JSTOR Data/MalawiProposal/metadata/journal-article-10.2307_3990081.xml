<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">worlbankeconrevi</journal-id>
         <journal-id journal-id-type="jstor">j101235</journal-id>
         <journal-title-group>
            <journal-title>The World Bank Economic Review</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>World Bank</publisher-name>
         </publisher>
         <issn pub-type="ppub">02586770</issn>
         <issn pub-type="epub">1564698X</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">3990081</article-id>
         <article-categories>
            <subj-group>
               <subject>A Symposium on Saving in Developing Countries</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>What Drives Consumption Booms?</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Peter J.</given-names>
                  <surname>Montiel</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>9</month>
            <year>2000</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">14</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id>i382812</issue-id>
         <fpage>457</fpage>
         <lpage>480</lpage>
         <page-range>457-480</page-range>
         <permissions>
            <copyright-statement>Copyright 2000 The International Bank for Reconstruction and Development/The World Bank</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/3990081"/>
         <abstract>
            <p>Consumption booms have been common in both industrial and developing countries, and several explanations have been offered for their occurrence. These include economy-wide wealth effects associated with favorable movements in the terms of trade or euphoric expectations triggered by macroeconomic reforms, Ricardian effects associated with fiscal stabilization, lending booms following financial liberalization, and a variety of distortions in intertemporal relative prices. Using a large cross-country sample of booms, this article assesses how widely applicable these explanations are. The key finding is that wealth effects linked to favorable movements in the terms of trade and anticipated improvements in macroeconomic performance seem to have been more important empirically than explanations relying primarily on fiscal phenomena or distortions in intertemporal relative prices.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d29e129a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d29e136" publication-type="other">
Rebelo and Vegh (1996)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d29e142" publication-type="other">
Reinhart and Vegh (1995).</mixed-citation>
            </p>
         </fn>
         <fn id="d29e149a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d29e156" publication-type="other">
Eichengreen, Rose, and Wyplosz (1995).</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d29e172a1310">
            <mixed-citation id="d29e176" publication-type="book">
Calvo, Guillermo. 1989. "Incredible Reforms." In Guillermo Calvo, Ronald Findlay, Penti
Kouri, and Jorge Braga de Macedo, eds., Debt, Stabilization, and Development. Ox-
ford: Basil Blackwell.<person-group>
                  <string-name>
                     <surname>Calvo</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Incredible Reforms</comment>
               <source>Debt, Stabilization, and Development</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d29e208a1310">
            <mixed-citation id="d29e212" publication-type="book">
Caprio, Gerard, and Daniela Klingebiel. 1997. "Bank Insolvency: Bad Luck, Bad Policy,
or Bad Banking?" In Michael Bruno and Boris Pleskovic, eds., Annual World Bank
Conference on Development Economics 1996. Washington, D.C.: World Bank.<person-group>
                  <string-name>
                     <surname>Caprio</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Bank Insolvency: Bad Luck, Bad Policy, or Bad Banking?</comment>
               <source>Annual World Bank Conference on Development Economics 1996</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d29e244a1310">
            <mixed-citation id="d29e248" publication-type="journal">
Deininger, Klaus, and Lyn Squire. 1996. "A New Data Set Measuring Income Inequal-
ity." The World Bank Economic Review10(September):565-91.<person-group>
                  <string-name>
                     <surname>Deininger</surname>
                  </string-name>
               </person-group>
               <issue>September</issue>
               <fpage>565</fpage>
               <volume>10</volume>
               <source>The World Bank Economic Review</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d29e283a1310">
            <mixed-citation id="d29e287" publication-type="book">
Dornbusch, Rudiger. 1985. "External Debt, Budget Deficits, and Disequilibrium Exchange
Rate." In Gordon W. Smith and John T. Cuddington, eds., International Debt and the
Developing Countries, pp. 213-35. Washington, D.C.: World Bank.<person-group>
                  <string-name>
                     <surname>Dornbusch</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">External Debt, Budget Deficits, and Disequilibrium Exchange Rate</comment>
               <fpage>213</fpage>
               <source>International Debt and the Developing Countries</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d29e323a1310">
            <mixed-citation id="d29e327" publication-type="journal">
Eichengreen, Barry, Andrew K. Rose, and Charles Wyplosz. 1995. "Exchange Market
Mayhem: The Antecedents and Aftermath of Speculative Attacks." Economic Policy
21(October):249-312.<object-id pub-id-type="doi">10.2307/1344591</object-id>
               <fpage>249</fpage>
            </mixed-citation>
         </ref>
         <ref id="d29e346a1310">
            <mixed-citation id="d29e350" publication-type="book">
Montiel, Peter J.1998. "Consumption Booms." Williams College, Department of Eco-
nomics, Williamstown, Mass. Processed.<person-group>
                  <string-name>
                     <surname>Montiel</surname>
                  </string-name>
               </person-group>
               <source>Consumption Booms</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d29e375a1310">
            <mixed-citation id="d29e379" publication-type="book">
Rebelo, Sergio, and Carlos A. Vegh. 1996. "Real Effects of Exchange Rate-Based
Stabilizations: An Analysis of Competing Theories." In Ben Bernanke and Julio
Rotemberg, NBER Macroeconomics Annual, pp. 125-73. Cambridge, Mass.: National
Bureau of Economic Research.<person-group>
                  <string-name>
                     <surname>Rebelo</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Real Effects of Exchange Rate-Based Stabilizations: An Analysis of Competing Theories</comment>
               <fpage>125</fpage>
               <source>NBER Macroeconomics Annual</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d29e417a1310">
            <mixed-citation id="d29e421" publication-type="journal">
Reinhart, Carmen M., and Carlos A. Vegh. 1995. "Nominal Interest Rates, Consump-
tion Booms, and Lack of Credibility: A Quantitative Examination." Journal of Devel-
opment Economics46(April):357-78.<person-group>
                  <string-name>
                     <surname>Reinhart</surname>
                  </string-name>
               </person-group>
               <issue>April</issue>
               <fpage>357</fpage>
               <volume>46</volume>
               <source>Journal of Development Economics</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d29e459a1310">
            <mixed-citation id="d29e463" publication-type="journal">
Rodríguez, C. A. 1982. "The Argentine Stabilization Plan of December 20th." World
Development10(September):801-11.<person-group>
                  <string-name>
                     <surname>Rodríguez</surname>
                  </string-name>
               </person-group>
               <issue>September</issue>
               <fpage>801</fpage>
               <volume>10</volume>
               <source>World Development</source>
               <year>1982</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
