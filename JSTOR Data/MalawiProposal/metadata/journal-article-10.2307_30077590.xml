<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">jinfedise</journal-id>
         <journal-id journal-id-type="jstor">j50000007</journal-id>
         <journal-title-group>
            <journal-title>The Journal of Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Chicago Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">00221899</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">30077590</article-id>
         <article-categories>
            <subj-group>
               <subject>Major Article and Brief Reports</subject>
               <subj-group>
                  <subject>Bacteria</subject>
               </subj-group>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Genetic Polymorphism in Mycobacterium tuberculosis Isolates from Patients with Chronic Multidrug-Resistant Tuberculosis</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Frank A.</given-names>
                  <surname>Post</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Paul A.</given-names>
                  <surname>Willcox</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Barun</given-names>
                  <surname>Mathema</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Lafras M.</given-names>
                  <surname>Steyn</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Karen</given-names>
                  <surname>Shean</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Srinivas V.</given-names>
                  <surname>Ramaswamy</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Edward A.</given-names>
                  <surname>Graviss</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Elena</given-names>
                  <surname>Shashkina</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Barry N.</given-names>
                  <surname>Kreiswirth</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Gilla</given-names>
                  <surname>Kaplan</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>7</month>
            <year>2004</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">190</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id>i30077577</issue-id>
         <fpage>99</fpage>
         <lpage>106</lpage>
         <permissions>
            <copyright-statement>Copyright 2004 Infectious Diseases Society of America</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/30077590"/>
         <abstract>
            <p>Multidrug-resistant tuberculosis (MDR-TB) is a major public health problem because treatment is complicated, cure rates are well below those for drug-susceptible tuberculosis (TB), and patients may remain infectious for months or years, despite receiving the best available therapy. To gain a better understanding of MDR-TB, we characterized serial isolates recovered from 13 human immunodeficiency virus-negative patients with MDRTB, by use of IS 6110 restriction fragment-length polymorphism analysis, spacer oligonucleotide genotyping (i.e., aspoligotyping,,), and sequencing of rpoB, katG, mabA-inhA (including promoter), pncA, embB, rpsL, rrs, and gyrA. For all 13 patients, chronic MDR-TB was caused by a single strain of Mycobacterium tuberculosis; 8 (62%) of the 13 strains identified as the cause of MDR-TB belonged to the W-Beijing family. The sputumderived isolates of 4 (31%) of the 13 patients had acquired additional drug-resistance mutations during the study. In these 4 patients, heterogeneous populations of bacilli with different resistance mutations, as well as mixtures of drug-susceptible and drug-resistant genotypes, were observed. This genetic heterogeneity may require treatment targeted at both drug-resistant and drug-susceptible phenotypes.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d1177e284a1310">
            <label>1</label>
            <mixed-citation id="d1177e291" publication-type="other">
Espinal MA, Laszlo A, Simonsen L, et al. Global trends in resistance
to antituberculosis drugs. World Health Organization-International
Union against Tuberculosis and Lung Disease Working Group on Anti-
Tuberculosis Drug Resistance Surveillance. N Engl J Med 2001; 344:
1294-303.</mixed-citation>
         </ref>
         <ref id="d1177e310a1310">
            <label>2</label>
            <mixed-citation id="d1177e317" publication-type="other">
Iseman MD. Drug-resistant tuberculosis: a clinician' s guide to tuber-
culosis. Philadelphia: Lippincott Williams 8c Wilkins, 2000.</mixed-citation>
         </ref>
         <ref id="d1177e327a1310">
            <label>3</label>
            <mixed-citation id="d1177e334" publication-type="other">
Iseman MD. Treatment of multidrug-resistant tuberculosis. N Engl J
Med 1993;329:784-91.</mixed-citation>
         </ref>
         <ref id="d1177e344a1310">
            <label>4</label>
            <mixed-citation id="d1177e351" publication-type="other">
Mitchison DA, Nunn AJ. Influence of initial drug resistance on the
response to short-course chemotherapy of pulmonary tuberculosis. Am
Rev Respir Dis 1986; 133:423-30.</mixed-citation>
         </ref>
         <ref id="d1177e365a1310">
            <label>5</label>
            <mixed-citation id="d1177e372" publication-type="other">
Espinal MA, Kim SJ, Suarez PG, et al. Standard short-course che-
motherapy for drug-resistant tuberculosis: treatment outcomes in 6
countries. JAMA 2000;283:2537-45.</mixed-citation>
         </ref>
         <ref id="d1177e385a1310">
            <label>6</label>
            <mixed-citation id="d1177e392" publication-type="other">
Primary multidrug-resistant tuberculosis-Ivanovo Oblast Russia,
1999. MMWR Morb Mortal Wkly Rep 1999;48:661-4.</mixed-citation>
         </ref>
         <ref id="d1177e402a1310">
            <label>7</label>
            <mixed-citation id="d1177e409" publication-type="other">
Goble M, Iseman MD, Madsen LA, Waite D, Ackerson L, Horsburgh
CR Jr. Treatment of 171 patients with pulmonary tuberculosis resistant
to isoniazid and rifampin. N Engl J Med 1993; 328:527-32.</mixed-citation>
         </ref>
         <ref id="d1177e422a1310">
            <label>8</label>
            <mixed-citation id="d1177e429" publication-type="other">
Suarez PG, Floyd K, Portocarrero J, et al. Feasibility and cost-effec-
tiveness of standardised second-line drug treatment for chronic tu-
berculosis patients: a national cohort study in Peru. Lancet 2002; 359:
1980-9.</mixed-citation>
         </ref>
         <ref id="d1177e445a1310">
            <label>9</label>
            <mixed-citation id="d1177e452" publication-type="other">
Tahaoglu K, Torun T, Sevim T, et al. The treatment of multidrug-
resistant tuberculosis in Turkey. N Engl J Med 2001;345:170-4.</mixed-citation>
         </ref>
         <ref id="d1177e462a1310">
            <label>10</label>
            <mixed-citation id="d1177e469" publication-type="other">
Schaaf HS, Botha P, Beyers N, et al. The 5-year outcome of multidrug
resistant tuberculosis patients in the Cape Province of South Africa.
Trop Med Int Health 1996; 1:718-22.</mixed-citation>
         </ref>
         <ref id="d1177e483a1310">
            <label>11</label>
            <mixed-citation id="d1177e490" publication-type="other">
Saenghirunvattana S, Charoenpan P, Vathesatogkit P, Kiatboonsri S,
Aeursudkij B. Multidrug-resistant tuberculosis: response to treatment.
J Med Assoc Thai 1996;79:601-3.</mixed-citation>
         </ref>
         <ref id="d1177e503a1310">
            <label>12</label>
            <mixed-citation id="d1177e512" publication-type="other">
Ridzon R, Kent JH, Valway S, et al. Outbreak of drug-resistant tuber-
culosis with second-generation transmission in a high school in Cal-
ifornia. J Pediatr 1997; 131:863-8.</mixed-citation>
         </ref>
         <ref id="d1177e525a1310">
            <label>13</label>
            <mixed-citation id="d1177e532" publication-type="other">
Frieden TR, Sherman LF, Maw KL, et al. A multi-institutional outbreak
of highly drug-resistant tuberculosis: epidemiology and clinical out-
comes. JAMA 1996;276:1229-35.</mixed-citation>
         </ref>
         <ref id="d1177e545a1310">
            <label>14</label>
            <mixed-citation id="d1177e552" publication-type="other">
van Rie A, Warren RM, Beyers N, et al. Transmission of a multidrug-
resistant Mycobacterium tuberculosis strain resembling "
strain W" among noninstitutionalized, human immunodeficiency virus-sero-
negative patients. J Infect Dis 1999; 180:1608-15.</mixed-citation>
         </ref>
         <ref id="d1177e568a1310">
            <label>15</label>
            <mixed-citation id="d1177e575" publication-type="other">
Van Rie A, Warren R, Richardson M, et al. Classification of drug-
resistant tuberculosis in an epidemic area. Lancet 2000;356:22-5.</mixed-citation>
         </ref>
         <ref id="d1177e585a1310">
            <label>16</label>
            <mixed-citation id="d1177e592" publication-type="other">
Schaaf HS, Van Rie A, Gie RP, et al. Transmission of multidrug-resistant
tuberculosis. Pediatr Infect Dis J 2000; 19:695-9.</mixed-citation>
         </ref>
         <ref id="d1177e603a1310">
            <label>17</label>
            <mixed-citation id="d1177e610" publication-type="other">
Ramaswamy S, Musser JM. Molecular genetic basis of antimicrobial
agent resistance in Mycobacterium tuberculosis: 1998 update. Tuber
Lung Dis 1998; 79:3-29.</mixed-citation>
         </ref>
         <ref id="d1177e623a1310">
            <label>18</label>
            <mixed-citation id="d1177e630" publication-type="other">
Farmer P, Bayona J, Becerra M, et al. The dilemma of MDR-TB in the
global era. Int J Tuberc Lung Dis 1998;2:869-76.</mixed-citation>
         </ref>
         <ref id="d1177e640a1310">
            <label>19</label>
            <mixed-citation id="d1177e647" publication-type="other">
Canetti G. Present aspects of bacterial resistance in tuberculosis. Am
Rev Respir Dis 1965;92:687-703.</mixed-citation>
         </ref>
         <ref id="d1177e657a1310">
            <label>20</label>
            <mixed-citation id="d1177e664" publication-type="other">
Vandiviere HM, Loring WE, Melvin I, Willis S. The treated pulmonary
lesion and its tubercle bacillus. II. The death and resurrection. Am J
Med Sci 1956;232:30-7.</mixed-citation>
         </ref>
         <ref id="d1177e677a1310">
            <label>21</label>
            <mixed-citation id="d1177e684" publication-type="other">
Kaplan G, Post FA, Moreira AL, et al. Mycobacterium tuberculosis
growth at the cavity surface: a microenvironment with failed immunity.
Infect Immun 2003;71:7099-108.</mixed-citation>
         </ref>
         <ref id="d1177e697a1310">
            <label>22</label>
            <mixed-citation id="d1177e704" publication-type="other">
Condos R, Rom WN, Schluger NW. Treatment of multidrug-resistant
pulmonary tuberculosis with interferon-y via aerosol. Lancet 1997;
349:1513-5.</mixed-citation>
         </ref>
         <ref id="d1177e718a1310">
            <label>23</label>
            <mixed-citation id="d1177e725" publication-type="other">
van Leuven M, De Groot M, Shean KP, von Oppell UO, Willcox PA.
Pulmonary resection as an adjunct in the treatment of multiple drug-
resistant tuberculosis. Ann Thorac Surg 1997;63:1368-72; discussion
1372-3.</mixed-citation>
         </ref>
         <ref id="d1177e741a1310">
            <label>24</label>
            <mixed-citation id="d1177e748" publication-type="other">
Hewlett D Jr, Horn DL, Alfalla C. Drug-resistant tuberculosis: incon-
sistent results of pyrazinamide susceptibility testing. JAMA 1995; 273:
916-7.</mixed-citation>
         </ref>
         <ref id="d1177e761a1310">
            <label>25</label>
            <mixed-citation id="d1177e768" publication-type="other">
Cutler RR, Wilson P, Villarroel J, Clarke FV. Evaluating current meth-
ods for determination of the susceptibility of mycobacteria to pyra-
zinamide, conventional, radiometric Bactec and two methods of pyr-
azinamidase testing. Lett Appl Microbiol 1997;24:127-32.</mixed-citation>
         </ref>
         <ref id="d1177e784a1310">
            <label>26</label>
            <mixed-citation id="d1177e791" publication-type="other">
van Embden JD, Cave MD, Crawford JT, et al. Strain identification of
Mycobacterium tuberculosis by DNA fingerprinting: recommendations
for a standardized methodology. J Clin Microbiol 1993;31:406-9.</mixed-citation>
         </ref>
         <ref id="d1177e804a1310">
            <label>27</label>
            <mixed-citation id="d1177e811" publication-type="other">
Mathema B, Bifani PJ, Driscoll J, et al. Identification and evolution of
an IS6110 low-copy-number Mycobacterium tuberculosis cluster. J Infect
Dis 2002; 185:641-9.</mixed-citation>
         </ref>
         <ref id="d1177e824a1310">
            <label>28</label>
            <mixed-citation id="d1177e831" publication-type="other">
Bifani PJ, Mathema B, Kurepina NE, Kreiswirth BN. Global dissem-
ination of the Mycobacterium tuberculosis W-Beijing family strains.
Trends Microbiol 2002; 10:45-52.</mixed-citation>
         </ref>
         <ref id="d1177e845a1310">
            <label>29</label>
            <mixed-citation id="d1177e852" publication-type="other">
Kurepina NE, Sreevatsan S, Plikaytis BB, et al. Characterization of the
phylogenetic distribution and chromosomal insertion sites of five
IS6110 elements in Mycobacterium tuberculosis: non-random integra-
tion in the dnaA-dnaN region. Tuber Lung Dis 1998;79:31-42.</mixed-citation>
         </ref>
         <ref id="d1177e868a1310">
            <label>30</label>
            <mixed-citation id="d1177e875" publication-type="other">
Yeh RW, Hopewell PC, Daley CL. Simultaneous infection with two
strains of Mycobacterium tuberculosis identified by restriction fragment
length polymorphism analysis. Int J Tuberc Lung Dis 1999; 3:537-9.</mixed-citation>
         </ref>
         <ref id="d1177e888a1310">
            <label>31</label>
            <mixed-citation id="d1177e895" publication-type="other">
Rinder H, Mieskes KT, Loscher T. Heteroresistance in Mycobacterium
tuberculosis. Int J Tuberc Lung Dis 2001;5:339-45.</mixed-citation>
         </ref>
         <ref id="d1177e905a1310">
            <label>32</label>
            <mixed-citation id="d1177e912" publication-type="other">
Glynn JR, Jenkins PA, Fine PE, et al. Patterns of initial and acquired
antituberculosis drug resistance in Karonga District, Malawi. Lancet
1995;345:907-10.</mixed-citation>
         </ref>
         <ref id="d1177e925a1310">
            <label>33</label>
            <mixed-citation id="d1177e932" publication-type="other">
Williams DL, Spring L, Gillis TP, Salfinger M, Persing DH. Evaluation
of a polymerase chain reaction-based universal heteroduplex generator
assay for direct detection of rifampin susceptibility of Mycobacterium
tuberculosis from sputum specimens. Clin Infect Dis 1998;26:446-50.</mixed-citation>
         </ref>
         <ref id="d1177e948a1310">
            <label>34</label>
            <mixed-citation id="d1177e955" publication-type="other">
Heep M, Brandstatter B, Rieger U, et al. Frequency of rpoB mutations</mixed-citation>
         </ref>
         <ref id="d1177e963a1310">
            <mixed-citation id="d1177e967" publication-type="other">
inside and outside the cluster I region in rifampin-resistant clinical
Mycobacterium tuberculosis isolates. J Clin Microbiol 2001;39:107-10.</mixed-citation>
         </ref>
         <ref id="d1177e977a1310">
            <label>35</label>
            <mixed-citation id="d1177e984" publication-type="other">
Diagnostic standards and classification of tuberculosis in adults and
children. This official statement of the American Thoracic Society and
the Centers for Disease Control and Prevention was adopted by the
ATS Board of Directors, July 1999. This statement was endorsed by
the Council of the Infectious Diseases Society of America, September
1999. Am J Respir Crit Care Med 2000; 161:1376-95.</mixed-citation>
         </ref>
         <ref id="d1177e1007a1310">
            <label>36</label>
            <mixed-citation id="d1177e1014" publication-type="other">
Blumberg HM, Burman WJ, Chaisson RE, et al. American Thoracic
Society/Centers for Disease Control and Prevention/Infectious Diseases
Society of America: treatment of tuberculosis. Am J Respir Crit Care
Med 2003; 167:603-62.</mixed-citation>
         </ref>
         <ref id="d1177e1030a1310">
            <label>37</label>
            <mixed-citation id="d1177e1037" publication-type="other">
Niemann S, Richter E, Rusch-Gerdes S. Stability of Mycobacterium
tuberculosis IS6110 restriction fragment length polymorphism patterns
and spoligotypes determined by analyzing serial isolates from patients
with drug-resistant tuberculosis. J Clin Microbiol 1999;37:409-12.</mixed-citation>
         </ref>
         <ref id="d1177e1053a1310">
            <label>38</label>
            <mixed-citation id="d1177e1060" publication-type="other">
Yeh RW, Ponce de Leon A, Agasino CB, et al. Stability of Mycobacterium
tuberculosis DNA genotypes. J Infect Dis 1998; 177:1107-11.</mixed-citation>
         </ref>
         <ref id="d1177e1070a1310">
            <label>39</label>
            <mixed-citation id="d1177e1077" publication-type="other">
van Rie A, Warren R, Richardson M, et al. Exogenous reinfection as a
cause of recurrent tuberculosis after curative treatment. N Engl J Med
1999;341:1174-9.</mixed-citation>
         </ref>
         <ref id="d1177e1091a1310">
            <label>40</label>
            <mixed-citation id="d1177e1098" publication-type="other">
Warren RM, van der Spuy GD, Richardson M, et al. Calculation of
the stability of the IS61iO banding pattern in patients with persistent
Mycobacterium tuberculosis disease. J Clin Microbiol 2002;40:1705-8.</mixed-citation>
         </ref>
         <ref id="d1177e1111a1310">
            <label>41</label>
            <mixed-citation id="d1177e1118" publication-type="other">
de Boer AS, Borgdorff MW, de Haas PE, Nagelkerke NJ, van Embden
JD, van Soolingen D. Analysis of rate of change of IS6110 RFLP patterns
of Mycobacterium tuberculosis based on serial patient isolates. J Infect
Dis 1999;180:1238-44.</mixed-citation>
         </ref>
         <ref id="d1177e1134a1310">
            <label>42</label>
            <mixed-citation id="d1177e1141" publication-type="other">
van Helden PD, Warren RM, Victor TC, van der Spuy G, Richardson
M, Hoal-van Helden E. Strain families of Mycobacterium tuberculosis.
Trends Microbiol 2002; 10:167-8.</mixed-citation>
         </ref>
         <ref id="d1177e1154a1310">
            <label>43</label>
            <mixed-citation id="d1177e1161" publication-type="other">
Werngren J, Hoffner SE. Drug-susceptible Mycobacterium tuberculosis
Beijing genotype does not develop mutation-conferred resistance to
rifampin at an elevated rate. J Clin Microbiol 2003;41:1520-4.</mixed-citation>
         </ref>
         <ref id="d1177e1174a1310">
            <label>44</label>
            <mixed-citation id="d1177e1181" publication-type="other">
Kruuner A, Hoffner SE, Sillastu H, et al. Spread of drug-resistant
pulmonary tuberculosis in Estonia. J Clin Microbiol 2001; 39:3339-45.</mixed-citation>
         </ref>
         <ref id="d1177e1191a1310">
            <label>45</label>
            <mixed-citation id="d1177e1198" publication-type="other">
Bifani PJ, Plikaytis BB, Kapur V, et al. Origin and interstate spread of
a New York City multidrug-resistant Mycobacterium tuberculosis clone
family. JAMA 1996;275:452-7.</mixed-citation>
         </ref>
         <ref id="d1177e1212a1310">
            <label>46</label>
            <mixed-citation id="d1177e1219" publication-type="other">
Anh DD, Borgdorff MW, Van LN, et al. Mycobacterium tuberculosis
Beijing genotype emerging in Vietnam. Emerg Infect Dis 2000; 6:302-5.</mixed-citation>
         </ref>
         <ref id="d1177e1229a1310">
            <label>47</label>
            <mixed-citation id="d1177e1236" publication-type="other">
Torres MJ, Criado A, Palomares JC, Aznar J. Use of real-time PCR and
fluorimetry for rapid detection of rifampin and isoniazid resistance-
associated mutations in Mycobacterium tuberculosis. J Clin Microbiol
2000;38:3194-9.</mixed-citation>
         </ref>
         <ref id="d1177e1252a1310">
            <label>48</label>
            <mixed-citation id="d1177e1259" publication-type="other">
Laszlo A, Rahman M, Espinal M, Raviglione M. Quality assurance
programme for drug susceptibility testing of Mycobacterium tuberculosis
in the WHO/IUATLD Supranational Reference Laboratory Network:
five rounds of proficiency testing, 1994-1998. Int J Tuberc Lung Dis
2002;6:748-56.</mixed-citation>
         </ref>
         <ref id="d1177e1278a1310">
            <label>49</label>
            <mixed-citation id="d1177e1285" publication-type="other">
Escalante P, Ramaswamy S, Sanabria H, et al. Genotypic characteri-
zation of drug-resistant Mycobacterium tuberculosis isolates from Peru.
Tuber Lung Dis 1998;79:111-8.</mixed-citation>
         </ref>
         <ref id="d1177e1298a1310">
            <label>50</label>
            <mixed-citation id="d1177e1305" publication-type="other">
Lee AS, Teo AS, Wong SY. Novel mutations in ndh in isoniazid-resistant
Mycobacterium tuberculosis isolates. Antimicrob Agents Chemother
2001;
45:2157-9.</mixed-citation>
         </ref>
         <ref id="d1177e1321a1310">
            <label>51</label>
            <mixed-citation id="d1177e1328" publication-type="other">
Van Rie A, Warren R, Mshanga I, et al. Analysis for a limited number
of gene codons can predict drug resistance of Mycobacterium tuberculosis
in a high-incidence community. J Clin Microbiol 2001;39:636-41.</mixed-citation>
         </ref>
         <ref id="d1177e1342a1310">
            <label>52</label>
            <mixed-citation id="d1177e1349" publication-type="other">
Mokrousov I, Otten T, Vyshnevskiy B, Narvskaya O. Detection of
embB306 mutations in ethambutol-susceptible clinical isolates of My-
cobacterium tuberculosis from Northwestern Russia: implications for
genotypic resistance testing. J Clin Microbiol 2002;40:3810-3.</mixed-citation>
         </ref>
         <ref id="d1177e1365a1310">
            <label>53</label>
            <mixed-citation id="d1177e1372" publication-type="other">
Outcome of second-line tuberculosis treatment in migrants from Vi-
etnam. International Organization for Migration (IOM) Tuberculosis
Working Group. Trop Med Int Health 1998;3:975-80.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
