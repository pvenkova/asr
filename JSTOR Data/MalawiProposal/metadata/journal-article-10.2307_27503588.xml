<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">populationenviro</journal-id>
         <journal-id journal-id-type="jstor">j50000178</journal-id>
         <journal-title-group>
            <journal-title>Population and Environment</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Human Sciences Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">01990039</issn>
         <issn pub-type="epub">15737810</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">27503588</article-id>
         <title-group>
            <article-title>The Demand for Immigration to the United States</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Philip Q.</given-names>
                  <surname>Yang</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>3</month>
            <year>1998</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">19</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id>i27503584</issue-id>
         <fpage>357</fpage>
         <lpage>383</lpage>
         <permissions>
            <copyright-statement>Copyright 1998 Human Sciences Press, Inc.</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/27503588"/>
         <abstract>
            <p>This paper examines trends and cross-national variation in the active demand for immigration to the United States in the period of 1984-1993, using data from the Visa Office and various other sources. The analysis is restricted to legal immigration in numerically limited categories. The results show that the total number of active immigrant visa applicants steadily increased in the aggregate and in each of the preference categories. Moreover, the active demand for immigration was highly skewed, with the majority of applications coming from a dozen countries: Mexico, the Philippines, India, mainland China, South Korea, Taiwan, Vietnam, the Dominican Republic, El Salvador, Jamaica, Hong Kong, and Pakistan. Most of these highly-backlogged countries displayed a significant increase in the growth rate of demand for immigration. The paper also shows a substantial cross-national variation in the active demand for immigration and explores its structural determinants. The regression results indicate that the level of economic development in sending countries and U.S. economic and cultural relations with sending countries play important roles in the determination process. Policy implications of the findings are also discussed.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Endnotes</title>
         <ref id="d557e123a1310">
            <label>1</label>
            <mixed-citation id="d557e130" publication-type="other">
An earlier version of this paper was presented at the Annual Meeting of the Population
Association of America in San Francisco, CA, April 1995.</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>References</title>
         <ref id="d557e149a1310">
            <mixed-citation id="d557e153" publication-type="other">
Briggs, Vernon M. (1984). Immigration policy and the American labor force. Baltimore, MD:
Johns Hopkins University Press.</mixed-citation>
         </ref>
         <ref id="d557e163a1310">
            <mixed-citation id="d557e167" publication-type="other">
Briggs, Vernon M. (1992). Mass immigration and the national interest. Armonk, New York:
M.E. Sharpe.</mixed-citation>
         </ref>
         <ref id="d557e177a1310">
            <mixed-citation id="d557e181" publication-type="other">
Cheng, Lucie, &amp;amp; Yang, Philip Q. (1994). Global inequality, global interaction, and profes-
sional migration to the United States. Paper presented at the 1994 American Sociological
Association annual meetings, Los Angeles.</mixed-citation>
         </ref>
         <ref id="d557e194a1310">
            <mixed-citation id="d557e198" publication-type="other">
Fawcett, James. (1989). Networks, linkages, and migration systems. International Migration
Review 23, 671–680.</mixed-citation>
         </ref>
         <ref id="d557e209a1310">
            <mixed-citation id="d557e213" publication-type="other">
Fawcett, James T. &amp;amp; Arnold, Fred. (1987). Explaining diversity: Asian and Pacific immigration
systems. In Fawcett, James T. &amp;amp; Carino, Benjamin V. (Eds.). Pacific bridges: The new
immigration from Asia and the Pacific Islands (pp. 453–73). New York: Center for Migra-
tion Studies.</mixed-citation>
         </ref>
         <ref id="d557e229a1310">
            <mixed-citation id="d557e233" publication-type="other">
Fitzpatrick, Gary L., &amp;amp; Modlin, Marilyn J. (1986). Direct-line distances: United States edition.
Metuchen, NJ: Scarecrow Press.</mixed-citation>
         </ref>
         <ref id="d557e243a1310">
            <mixed-citation id="d557e247" publication-type="other">
Greenwood, Michael, &amp;amp; McDowell, John. (1982). The supply of immigrants to the United
States. In Chiswick, Barry R. (Ed.). The Gateway: U.S. Immigration Issues and Policies
(pp. 55–85). Washington, DC: American Enterprise Institute for Public Policy Research.</mixed-citation>
         </ref>
         <ref id="d557e260a1310">
            <mixed-citation id="d557e264" publication-type="other">
Hofstetter, Richard R. (1984). Economic underdevelopment and the population explosion:
Implications for U.S. immigration policy. In Hofstetter, Richard R. (Ed.), U.S. immigration
policy (pp. 55–79). Durham, NC: Duke University Press.</mixed-citation>
         </ref>
         <ref id="d557e277a1310">
            <mixed-citation id="d557e281" publication-type="other">
Immigration and Naturalization Service (INS). (1993). 1992 statistical yearbook of the Immi-
gration and Naturalization Service. Washington DC: U.S. Government Printing Office.</mixed-citation>
         </ref>
         <ref id="d557e291a1310">
            <mixed-citation id="d557e295" publication-type="other">
INS. (1994). 1993 statistical yearbook of the Immigration and Naturalization Service. Wash-
ington DC: U.S. Government Printing Office.</mixed-citation>
         </ref>
         <ref id="d557e306a1310">
            <mixed-citation id="d557e310" publication-type="other">
Institute of International Education. (1983). Open doors: 1981/82 report on international edu-
cational exchange. New York, NY: IIE.</mixed-citation>
         </ref>
         <ref id="d557e320a1310">
            <mixed-citation id="d557e324" publication-type="other">
International Institute for Strategie Studies (IISS). (1981). The military balance 1980–1981.
London: IISS.</mixed-citation>
         </ref>
         <ref id="d557e334a1310">
            <mixed-citation id="d557e338" publication-type="other">
IISS. (1982). The military balance 1981–82. London: IISS.</mixed-citation>
         </ref>
         <ref id="d557e345a1310">
            <mixed-citation id="d557e349" publication-type="other">
IISS. (1983). The military balance 1982–83. London: IISS.</mixed-citation>
         </ref>
         <ref id="d557e356a1310">
            <mixed-citation id="d557e360" publication-type="other">
IISS. (1984). The military balance 1983–84. London: IISS.</mixed-citation>
         </ref>
         <ref id="d557e367a1310">
            <mixed-citation id="d557e371" publication-type="other">
IISS. (1985). The military balance 1984–85. London: IISS.</mixed-citation>
         </ref>
         <ref id="d557e379a1310">
            <mixed-citation id="d557e383" publication-type="other">
International Monetary Fund (IMF). (Various years). Direction of trade statistics yearbook.
Washington, DC: IMF.</mixed-citation>
         </ref>
         <ref id="d557e393a1310">
            <mixed-citation id="d557e397" publication-type="other">
Jasso, Guillermina, &amp;amp; Rosenzweig, Mark R. (1990). The new chosen people: Immigrants in the
United States. New York: Russell Sage Foundation.</mixed-citation>
         </ref>
         <ref id="d557e407a1310">
            <mixed-citation id="d557e411" publication-type="other">
Lamm, Richard, &amp;amp; Imhoff, Gary. (1985). The immigration time bomb: The fragmenting of
America. New York: Truman Tally Books.</mixed-citation>
         </ref>
         <ref id="d557e421a1310">
            <mixed-citation id="d557e425" publication-type="other">
Lande, Stephen, &amp;amp; Crigler, Nellis. (1990). Trade policy as a means to reduce immigration. In
Commission for the Study of International Migration and Cooperative Economic Devel-
opment, Unauthorized migration: Addressing the root causes, Research Addendum, vol.
1 (pp.531–554). Washington, DC: U.S. Government Printing Office.</mixed-citation>
         </ref>
         <ref id="d557e441a1310">
            <mixed-citation id="d557e445" publication-type="other">
Light, Ivan, &amp;amp; Bonacich, Edna. (1988). Immigrant entrepreneurs: Koreans in Los Angeles,
1965–1982. Berkeley: University of California Press.</mixed-citation>
         </ref>
         <ref id="d557e455a1310">
            <mixed-citation id="d557e459" publication-type="other">
Piore, Michael. (1979). Birds of passage. Cambridge, MA: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d557e467a1310">
            <mixed-citation id="d557e471" publication-type="other">
Sassen, Saskia. (1988). The mobility of labor and capital. Cambridge, MA: Cambridge Univer-
sity Press.</mixed-citation>
         </ref>
         <ref id="d557e481a1310">
            <mixed-citation id="d557e485" publication-type="other">
UNESCO. (Various years). UNESCO statistical yearbook. UNESCO.</mixed-citation>
         </ref>
         <ref id="d557e492a1310">
            <mixed-citation id="d557e496" publication-type="other">
United Nations. (1988). National accounts statistics: Analysis of main aggregates, 1985. New
York: United Nations.</mixed-citation>
         </ref>
         <ref id="d557e506a1310">
            <mixed-citation id="d557e510" publication-type="other">
United Nations. (1989). Trends in population policy. New York: United Nations.</mixed-citation>
         </ref>
         <ref id="d557e517a1310">
            <mixed-citation id="d557e521" publication-type="other">
United Nations. (1990). 1988 demographic yearbook. New York: United Nations.</mixed-citation>
         </ref>
         <ref id="d557e528a1310">
            <mixed-citation id="d557e532" publication-type="other">
United States Department of Commerce, Bureau of Economic Analysis. (1985). U.S. direct
investment abroad: 1982 benchmark survey. Washington, DC: U.S. Government Printing
Office.</mixed-citation>
         </ref>
         <ref id="d557e546a1310">
            <mixed-citation id="d557e550" publication-type="other">
United States Department of Commerce, Bureau of Economic Analysis. (1986). U.S direct
investment abroad: Operations of U.S. parent companies and their foreign affiliates: Re-
vised 1983 estimates. Washington, DC: U.S. Government Printing Office.</mixed-citation>
         </ref>
         <ref id="d557e563a1310">
            <mixed-citation id="d557e567" publication-type="other">
United States Department of Commerce, Bureau of Economic Analysis. (1987). U.S. direct
investment abroad: Operations of U.S. parent companies and their foreign affiliates: Re-
vised 1984 estimates. Washington, DC: U.S. Government Printing Office.</mixed-citation>
         </ref>
         <ref id="d557e580a1310">
            <mixed-citation id="d557e584" publication-type="other">
United States Department of Commerce, Bureau of Economic Analysis. (1988). U.S. direct
investment abroad: Operations of U.S. parent companies and their foreign affiliates: Re-
vised 1985 Estimates. Washington, DC: U.S. Government Printing Office.</mixed-citation>
         </ref>
         <ref id="d557e597a1310">
            <mixed-citation id="d557e601" publication-type="other">
United States Department of Commerce, Bureau of Economic Analysis. (1989). U.S. direct
investment abroad: Operations of U.S. parent companies and their foreign affiliates: Re-
vised 1986 estimates. Washington, DC: U.S. Government Printing Office.</mixed-citation>
         </ref>
         <ref id="d557e614a1310">
            <mixed-citation id="d557e618" publication-type="other">
U. S. Department of Commerce, International Trade Administration. (1987). 1986 U.S foreign
trade highlights. Washington, DC: U.S. Government Printing Office.</mixed-citation>
         </ref>
         <ref id="d557e628a1310">
            <mixed-citation id="d557e632" publication-type="other">
U. S. 101st Congress. (1991). United States statutes at large, vol. 104, part 6, public laws
101–626 through 101–650. Washington, DC: U.S. Printing Office.</mixed-citation>
         </ref>
         <ref id="d557e643a1310">
            <mixed-citation id="d557e647" publication-type="other">
Visa Office. (1984). 1984 report of the visa office. Washington, DC: U.S. Printing Office.</mixed-citation>
         </ref>
         <ref id="d557e654a1310">
            <mixed-citation id="d557e658" publication-type="other">
Visa Office. (1985). 1985 report of the visa office. Washington, DC: U.S. Printing Office.</mixed-citation>
         </ref>
         <ref id="d557e665a1310">
            <mixed-citation id="d557e669" publication-type="other">
Visa Office. (1986). 1986 Report of the visa office. Washington, DC: U.S. Printing Office.</mixed-citation>
         </ref>
         <ref id="d557e676a1310">
            <mixed-citation id="d557e680" publication-type="other">
Visa Office. (1987). 1987 Report of the visa office. Washington, DC: US. Printing Office.</mixed-citation>
         </ref>
         <ref id="d557e687a1310">
            <mixed-citation id="d557e691" publication-type="other">
Visa Office. (1988). 1988 Report of the visa office. Washington, DC: U.S. Printing Office.</mixed-citation>
         </ref>
         <ref id="d557e698a1310">
            <mixed-citation id="d557e702" publication-type="other">
Visa Office. (1989). 1989 Report of the visa office. Washington, DC: U.S. Printing Office.</mixed-citation>
         </ref>
         <ref id="d557e710a1310">
            <mixed-citation id="d557e714" publication-type="other">
Visa Office. (1990). 1990 Report of the visa office. Washington, DC: U.S. Printing Office.</mixed-citation>
         </ref>
         <ref id="d557e721a1310">
            <mixed-citation id="d557e725" publication-type="other">
Visa Office. (1992). 1992 Report of the visa office. Washington, DC: U.S. Printing Office.</mixed-citation>
         </ref>
         <ref id="d557e732a1310">
            <mixed-citation id="d557e736" publication-type="other">
Visa Office. (1993). 1993 Report of the visa office. Washington, DC: U.S. Printing Office.</mixed-citation>
         </ref>
         <ref id="d557e743a1310">
            <mixed-citation id="d557e747" publication-type="other">
World Bank. (1983). World development report 1981. New York: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d557e754a1310">
            <mixed-citation id="d557e758" publication-type="other">
World Bank. (1984). World development report 1982. New York: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d557e765a1310">
            <mixed-citation id="d557e769" publication-type="other">
World Bank. (1985). World development report 1983. New York: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d557e777a1310">
            <mixed-citation id="d557e781" publication-type="other">
World Bank. (1986). World development report 1984. New York: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d557e788a1310">
            <mixed-citation id="d557e792" publication-type="other">
World Bank. (1987). World development report 1985. New York: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d557e799a1310">
            <mixed-citation id="d557e803" publication-type="other">
Yang, Philip Q. (1995). Post-1965 Immigration to the United States: Structural determinants.
Westport, CT: Praeger Publishers.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
