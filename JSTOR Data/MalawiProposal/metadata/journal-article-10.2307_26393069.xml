<?xml version="1.0" encoding="UTF-8"?>
<article article-type="research-article" dtd-version="1.0" xml:lang="eng">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="publisher-id">conssoci</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50019729</journal-id>
         <journal-title-group>
            <journal-title>Conservation and Society</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>WOLTERS KLUWER INDIA PRIVATE LIMITED</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">09724923</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">09753133</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">26393069</article-id>
         <article-categories>
            <subj-group>
               <subject>Article</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Smallholders and Communities in Timber Markets</article-title>
            <subtitle>Conditions Shaping Diverse Forms of Engagement in Tropical Latin America</subtitle>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Pacheco</surname>
                  <given-names>Pablo</given-names>
               </string-name>
               <aff>Center for International Forestry Research (CIFOR), Bogor, Indonesia</aff>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>1</month>
            <year>2012</year>
            <string-date>2012</string-date>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink">10</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">e26393065</issue-id>
         <fpage>114</fpage>
         <lpage>123</lpage>
         <permissions>
            <copyright-statement>Copyright: © Pacheco 2012</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/26393069"/>
         <abstract xml:lang="eng">
            <p>Forest tenure reforms have granted smallholders and communities formal rights to land and forest resources. This article explores the various ways in which these local actors engage with timber markets in the context of such reforms. I argue that the economic benefi ts that communities can capture from the use of forest resources, mainly timber, are mediated by two sets of factors that are beyond the process of tenure reform. The first set of factors relate to communities’ capacity to interact with other actors—intermediaries and companies—in timber markets, and the second to specific conditions of market development. Interactions between community capacity and market conditions shape the ways in which smallholders and communities engage with timber markets, thereby influencing the benefits they can obtain from commercial use of their timber forests. This article focuses on forest communities that have acquired legal tenure rights in Latin America; specifi cally four communities located in tropical landscapes in Bolivia, Brazil, and Nicaragua. The types of engagement revealed by the analysis should be taken into account in differentiated public policies in order to improve the outcomes of forest tenure reforms.</p>
         </abstract>
         <kwd-group>
            <label>Keywords:</label>
            <kwd>smallholders</kwd>
            <kwd>community forestry</kwd>
            <kwd>timber markets</kwd>
            <kwd>forest tenure</kwd>
            <kwd>forest policy</kwd>
            <kwd>forest management</kwd>
            <kwd>Latin America</kwd>
         </kwd-group>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="unparsed-citations">
         <title>REFERENCES</title>
         <ref id="ref1">
            <mixed-citation publication-type="other">Adams, W.M., R. Aveling, D. Brockington, B. Dickson, J. Elliott, J. Hutton, D. Roe, et al. 2004. Biodiversity conservation and the eradication of poverty. Science 306(5699): 1146–1149.</mixed-citation>
         </ref>
         <ref id="ref2">
            <mixed-citation publication-type="other">Albornoz, M., P. Cronkleton and M. Toro. 2008. Estudio Regional Guarayos: Historia de la confi guración de un territorio en confl icto. Santa Cruz: Center for Labor and Agrarian Development, Center for International Forestry Research.</mixed-citation>
         </ref>
         <ref id="ref3">
            <mixed-citation publication-type="other">Antinori, C. and D. Bray. 2005. Community forest enterprises as entrepreneurial fi rms: Economic and institutional perspectives from Mexico. World Development 33(9): 1529–1543.</mixed-citation>
         </ref>
         <ref id="ref4">
            <mixed-citation publication-type="other">Argüello, A. 2008. Cadena de valor de la madera de la cooperativa Kiwatingni en Layasiksa-RAAN. Managua: Center for International Forestry Research, Masangni.</mixed-citation>
         </ref>
         <ref id="ref5">
            <mixed-citation publication-type="other">Blaikie, P. 2006. Is small really beautiful? Community-based natural resource management in Malawi and Botswana. World Development 34: 1942–1957.</mixed-citation>
         </ref>
         <ref id="ref6">
            <mixed-citation publication-type="other">Bolivia Sustainable Forest Management Project (BOLFOR) II. 2007.Benefi cios del Plan de Manejo Forestal de la comunidad Cururu, Gestion 2006. Santa Cruz: Proyecto BOLFOR II.</mixed-citation>
         </ref>
         <ref id="ref7">
            <mixed-citation publication-type="other">Bray, D.B., L. Merino-Pérez and D. Barry. 2005. The community forests of Mexico: Managing for sustainable landscapes. Austin, TX: University of Texas Press.</mixed-citation>
         </ref>
         <ref id="ref8">
            <mixed-citation publication-type="other">Contreras, A. 2005. Best practices for improving law compliance in the forest sector. Rome: Food and Agriculture Organization of the United Nations.</mixed-citation>
         </ref>
         <ref id="ref9">
            <mixed-citation publication-type="other">Donovan, J., D. Stoian, S. Grouwles, D. Macqueen, A. van Leeuwen, G.Boetekees and K. Nicholson. 2008a. Towards an enabling environment for small and medium forest enterprise development. San José: Tropical Agricultural Research and Higher Education Center; Food and Agriculture Organization of the United Nations; International Institute for Environment and Development; Netherlands Development Organisation; Interchurch Organization for Development Co-operation.</mixed-citation>
         </ref>
         <ref id="ref10">
            <mixed-citation publication-type="other">Donovan, J., D. Stoian, D. Macqueen and S. Grouwels. 2006. The business side of sustainable forest management: Development of small and medium forest enterprises for poverty reduction. In: Natural Resource Perspectives 104. London: Overseas Development Institute.</mixed-citation>
         </ref>
         <ref id="ref11">
            <mixed-citation publication-type="other">Donovan, J., D. Stoian and N. Poole. 2008b. Global review of rural community enterprises: The long and winding road for creating viable businesses and potential shortcuts. San José: Tropical Agricultural Research and Higher Education Center, School of Oriental and African Studies.</mixed-citation>
         </ref>
         <ref id="ref12">
            <mixed-citation publication-type="other">Ibarguen, R. 2008. La última frontera y las comunidades de pequeños parcelarios en el norte paceño. La Paz: Center for Labor and Agrarian Development; Center for International Forestry Research.</mixed-citation>
         </ref>
         <ref id="ref13">
            <mixed-citation publication-type="other">Kaimowitz, D. 2003. Forest law enforcement and rural livelihoods.International Forestry Review 5(3): 199–210.</mixed-citation>
         </ref>
         <ref id="ref14">
            <mixed-citation publication-type="other">Larson, A. 2008. Land tenure rights and limits to forest management in Nicaragua’s North Atlantic Autonomous Region. In: The 12th Biennial Conference of the International Association for the Study of Commons.Cheltenham: International Association for the Study of Common Property. July 14–18, 2008.</mixed-citation>
         </ref>
         <ref id="ref15">
            <mixed-citation publication-type="other">Larson, A., D. Barry and G. Dahal. 2010. Forests for people: Community rights and forest tenure reform. London: Earthscan.</mixed-citation>
         </ref>
         <ref id="ref16">
            <mixed-citation publication-type="other">Larson, A. and J. Mendoza. 2008. Desafi os en la tenencia comunitaria de bosques en la RAAN de Nicaragua. Managua: Center for International Forestry Research; Rights and Resources Institute;Instituto de Recursos Naturales; Medio Ambiente y Desarrollo Sostenible–Universidad de las Regiones Autónomas de la Costa Caribe Nicaragüense.</mixed-citation>
         </ref>
         <ref id="ref17">
            <mixed-citation publication-type="other">Larson, A.M., P. Cronkleton, D. Barry and P. Pacheco. 2008. Tenure rights and beyond: Community access to forest resources in Latin America.Bogor: Center for International Forestry Research.</mixed-citation>
         </ref>
         <ref id="ref18">
            <mixed-citation publication-type="other">Macqueen, D. 2008. Small and medium forestry enterprise: Supporting small forest enterprises. London: International Institute for Environment and Development.</mixed-citation>
         </ref>
         <ref id="ref19">
            <mixed-citation publication-type="other">Mendoza, J., O.C. Hendy and T.R. Lino. 2008. Estudio de caso: Acceso, medios de vida y gobierno comunitario en las reformas de tenencia forestal Comunidad de Layasiksa, Region Autonoma del Atlantico Norte (RAAN), Nicaragua. Managua: REMADES, Instituto de Recursos Naturales, Medio Ambiente y Desarrollo, Universidad Regional de la Costa del Caribe de Nicaragua.</mixed-citation>
         </ref>
         <ref id="ref20">
            <mixed-citation publication-type="other">Molnar, A., M. Liddle, C. Bracer, A. Khare, A. White and J. Bull. 2007.Community-based forest enterprises in tropical forest countries: Status and potential. Washington, DC: International Tropical Timber Organization; Rights and Resources Initiative; Forest Trends.</mixed-citation>
         </ref>
         <ref id="ref21">
            <mixed-citation publication-type="other">Moreira, E. and H. Hébette. 2003. Estudo socio-economico com vista a criacao da Resex Verde para Sempre. Belem: Universidade Federal do Pará.</mixed-citation>
         </ref>
         <ref id="ref22">
            <mixed-citation publication-type="other">Nunes, W., P. Mourão, R. Lobo and G. Cayres. 2008. Entre sonhos e pesadelos: acesso a terra e manejo fl orestal nas comunidades rurais em Porto de Moz. Belem: Center for International Forestry Research.</mixed-citation>
         </ref>
         <ref id="ref23">
            <mixed-citation publication-type="other">Pacheco, D. 2007. An institutional analysis of decentralization and indigenous timber management in common-property areas of Bolivia’s lowlands.Ph.D. thesis. Indiana University, Bloomington, IN, USA.</mixed-citation>
         </ref>
         <ref id="ref24">
            <mixed-citation publication-type="other">Pacheco, P., D. Barry, P. Cronkleton and A. Larson. 2008. The role of informal institutions in the use of forest resources in Latin America. Bogor: Center for International Forestry Research.</mixed-citation>
         </ref>
         <ref id="ref25">
            <mixed-citation publication-type="other">Pokorny, B. and J. Johnson. 2008. Community forestry in the Amazon: The unsolved challenge of forests and the poor. In: Natural Resources Perspective No. 112. London: Overseas Development Institute.</mixed-citation>
         </ref>
         <ref id="ref26">
            <mixed-citation publication-type="other">Rozo, B. and M. Moreno. 2007. Informe de evaluación para la certifi cación del manejo forestal de Asociación Indígena Maderera Cururú (AIMCU) e INPA Parket Ltda. en Guarayos, Santa Cruz - Bolivia. Santa Cruz: SmartWood.</mixed-citation>
         </ref>
         <ref id="ref27">
            <mixed-citation publication-type="other">Scherr, S.J., A. White and D. Kaimowitz. 2004. A new agenda for forest conservation and poverty reduction: Making markets work for low-income producers. Washington, DC: Forest Trends; Center for International Forestry Research; International Union for Conservation of Nature.</mixed-citation>
         </ref>
         <ref id="ref28">
            <mixed-citation publication-type="other">Solares, A.M. 2008. Las MIPYMES en las exportaciones bolivianas. La Paz: United States Agency for International Development; Instituto Boliviano de Comercio Exterior.</mixed-citation>
         </ref>
         <ref id="ref29">
            <mixed-citation publication-type="other">Taconni, L. 2007. Illegal logging: Law enforcement, livelihoods and the timber trade. London: Earthscan.</mixed-citation>
         </ref>
         <ref id="ref30">
            <mixed-citation publication-type="other">Vallejos, C. 1998. Ascensión de Guarayos: indígenas y madereros. In: Municipios y Gestion Forestal en el Tropico Boliviano (eds. Pacheco, P. and D. Kaimowitz). Pp. 51–82. La Paz: Center for International Forestry Research; Center for Labor and Agrarian Development;Taller de Iniciativas y Estudios Rurales y de Reforma Agraria; Bolivia Sustainable Forest Management Project.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
