<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">eurojepid</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100704</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>European Journal of Epidemiology</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Springer</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">03932990</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15737284</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">27822808</article-id>
         <article-categories>
            <subj-group>
               <subject>SPATIAL EPIDEMIOLOGY</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>A Bayesian multinomial model to analyse spatial patterns of childhood co-morbidity in Malawi</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Lawrence N.</given-names>
                  <surname>Kazembe</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jimmy J.</given-names>
                  <surname>Namangale</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>8</month>
            <year>2007</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">22</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">8</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i27822798</issue-id>
         <fpage>545</fpage>
         <lpage>556</lpage>
         <permissions>
            <copyright-statement>© 2007 Springer Science+Business Media B.V.</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/27822808"/>
         <abstract>
            <p>Children in less developed countries die from relatively small number of infectious disease, some of which epidemiologically overlap. Using self-reported illness data from the 2000 Malawi Demographic and Health Survey, we applied a random effects multinomial model to assess risk factors of childhood co-morbidity of fever, diarrhoea and pneumonia, and quantify area-specific spatial effects. The spatial structure was modelled using the conditional autoregressive prior. Various models were fitted and compared using deviance information criterion. Inference was Bayesian and was based on Markov Chain Monte Carlo simulation techniques. We found spatial variation in childhood co-morbidity and determinants of each outcome category differed. Specifically, risk factors associated with child co-morbidity included age of the child, place of residence, undernutrition, bednet use and Vitamin A. Higher residual risk levels were identified in the central and southern–eastern regions, particularly for fever, diarrhoea and pneumonia; fever and pneumonia; and fever and diarrhoea combinations. This linkage between childhood health and geographical location warrants further research to assess local causes of these clusters. More generally, although each disease has its own mechanism, overlapping risk factors suggest that integrated disease control approach may be cost-effective and should be employed.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d223e217a1310">
            <label>1</label>
            <mixed-citation id="d223e224" publication-type="other">
Black RE, Morris SS, Bryce J. Where and why are 10 million
children dying every year? Lancet 2003;361:2226–34</mixed-citation>
         </ref>
         <ref id="d223e234a1310">
            <label>2</label>
            <mixed-citation id="d223e241" publication-type="other">
Mulholland K. Commentary: comorbidity as a factor in child
health and child survival in developing countries. Int J Epidemiol
2005;34:375–7</mixed-citation>
         </ref>
         <ref id="d223e254a1310">
            <label>3</label>
            <mixed-citation id="d223e261" publication-type="other">
National Statistical Office, ORC Macro. Malawi Demographic
and Health Survey 2004: Preliminary report. Zomba, Malawi:
NSO; 2005.</mixed-citation>
         </ref>
         <ref id="d223e274a1310">
            <label>4</label>
            <mixed-citation id="d223e281" publication-type="other">
World Health Statistics. Country Health System Fact Sheet 2006–
Malawi. Url: [http://www.who.int/whosis/en/</mixed-citation>
         </ref>
         <ref id="d223e292a1310">
            <label>5</label>
            <mixed-citation id="d223e299" publication-type="other">
Fenn B, Morris S, Black RE. Comorbidity in childhood in Ghana:
magnitude, associated factors and impact on mortality. Int J
Epidemiol 2005;34:368–75</mixed-citation>
         </ref>
         <ref id="d223e312a1310">
            <label>6</label>
            <mixed-citation id="d223e319" publication-type="other">
Källander K, Nsungwa-Sabiiti J, Peterson S. Symptom overlap
for malaria and pneumonia-policy implications for home man-
agement strategies. Acta Tropic 2004;90:211–4</mixed-citation>
         </ref>
         <ref id="d223e332a1310">
            <label>7</label>
            <mixed-citation id="d223e339" publication-type="other">
Sachs JD, McArthur JW. The Millennium Project: a plan for
meeting the Millennium Development Goals. Lancet
2005;365:347–53</mixed-citation>
         </ref>
         <ref id="d223e352a1310">
            <label>8</label>
            <mixed-citation id="d223e359" publication-type="other">
Richardson S, Abellan JJ, Best N. Bayesian spatio-temporal
analysis of joint patterns of male and female lung cancer risks in
Yorkshire (UK). Stat Methods Med Res 2006;15:385–407</mixed-citation>
         </ref>
         <ref id="d223e372a1310">
            <label>9</label>
            <mixed-citation id="d223e379" publication-type="other">
Held L, Natario I, Fenton SE, Rue H, Becker N. Towards joint
disease mapping. Stat Methods Med Res 2005;14:61–82</mixed-citation>
         </ref>
         <ref id="d223e389a1310">
            <label>10</label>
            <mixed-citation id="d223e396" publication-type="other">
Raso G, Vounatsou P, Singer BH, N'Goran EK. An integrated
approach for risk profiling and spatial prediction of Schistosoma
mansoni-hookworm coinfection. PNAS 2006;103:6934–9</mixed-citation>
         </ref>
         <ref id="d223e410a1310">
            <label>11</label>
            <mixed-citation id="d223e417" publication-type="other">
Elliot P, Wakefield J, Best N, Briggs DJ (eds). Spatial epidemi-
ology-methods and applications. London: Oxford University
Press; 2000</mixed-citation>
         </ref>
         <ref id="d223e430a1310">
            <label>12</label>
            <mixed-citation id="d223e437" publication-type="other">
National Statistical Office, ORC Macro. Malawi Demographic
and Health Survey 2000. Zomba, Malawi: NSO; 2001</mixed-citation>
         </ref>
         <ref id="d223e447a1310">
            <label>13</label>
            <mixed-citation id="d223e454" publication-type="other">
Kandala N-B. Bayesian geoadditive modelling of childhood
morbidity in Malawi. Appl Stoch Models Busin Industr 2006; 22:
139–54</mixed-citation>
         </ref>
         <ref id="d223e467a1310">
            <label>14</label>
            <mixed-citation id="d223e474" publication-type="other">
Kandala N-B, Magadi MA, Madise NJ. An investigation of dis-
trict spatial variations of childhood diarrhoea and fever morbidity
in Malawi. Soc Sci Med 2006;62:1138–52</mixed-citation>
         </ref>
         <ref id="d223e487a1310">
            <label>15</label>
            <mixed-citation id="d223e494" publication-type="other">
Jin X, Carlin BP, Banerjee S. Generalized hierarchical multi-
variate CAR models for areal data. Biometrics 2005;61:950–61</mixed-citation>
         </ref>
         <ref id="d223e504a1310">
            <label>16</label>
            <mixed-citation id="d223e511" publication-type="other">
Gelfand AE, Vounatsou P. Proper multivariate conditional auto-
regressive models for spatial data analysis. Biostat 2003;4:11–25</mixed-citation>
         </ref>
         <ref id="d223e522a1310">
            <label>17</label>
            <mixed-citation id="d223e529" publication-type="other">
Fahrmeir L, Tutz G. Multivariate statistical modelling based on
generalized linear models. New York: Springer-Verlag; 2001.</mixed-citation>
         </ref>
         <ref id="d223e539a1310">
            <label>18</label>
            <mixed-citation id="d223e546" publication-type="other">
Vounatsou P, Smith T, Gelfand AE. Spatial modelling of multi-
nomial data with latent structure: an application to geographical
mapping of human gene and haplotype frequencies. Biostat
2000;1:177–89</mixed-citation>
         </ref>
         <ref id="d223e562a1310">
            <label>19</label>
            <mixed-citation id="d223e569" publication-type="other">
Fahrmeir L, Lang S. Bayesian semiparametric regression analysis
of multicategorical time-space data. Ann Inst Stat Math
2001;53:11–30</mixed-citation>
         </ref>
         <ref id="d223e582a1310">
            <label>20</label>
            <mixed-citation id="d223e589" publication-type="other">
Lang S, Brezger A. Bayesian P-splines. J Comput Graph Stat
2004;13:183–212</mixed-citation>
         </ref>
         <ref id="d223e599a1310">
            <label>21</label>
            <mixed-citation id="d223e606" publication-type="other">
Dean C, MacNab YC. Modelling of rates over a hierarchical
health administrative structure. Can J Stat 2001;29:405–19</mixed-citation>
         </ref>
         <ref id="d223e616a1310">
            <label>22</label>
            <mixed-citation id="d223e623" publication-type="other">
Banerjee S, Carlin BP, Gelfand AE. Hierarchical modeling and
analysis for spatial data. Chapman and Hall/CRC: London;
2004</mixed-citation>
         </ref>
         <ref id="d223e637a1310">
            <label>23</label>
            <mixed-citation id="d223e644" publication-type="other">
Mugglin AS, Carlin BP, Gelfand AE. Fully model-based ap-
proaches for spatially misaligned data. JAS A 2000;95:877–87</mixed-citation>
         </ref>
         <ref id="d223e654a1310">
            <label>24</label>
            <mixed-citation id="d223e661" publication-type="other">
Besag J, York J, Mollie A. Bayesian image restoration with two
applications in spatial statistics (with discussion). Ann Inst Stat
Math 1991;43:1–59</mixed-citation>
         </ref>
         <ref id="d223e674a1310">
            <label>25</label>
            <mixed-citation id="d223e681" publication-type="other">
Spiegelhalter DJ, Best NG, Carlin BP, van der Linde A. Bayesian
measures of model complexity and fit. J Royal Stat Soc B (with
discussion) 2002;64:583–639</mixed-citation>
         </ref>
         <ref id="d223e694a1310">
            <label>26</label>
            <mixed-citation id="d223e701" publication-type="other">
Brezger A, Kneib T, Lang S. BayesX: Analyzing Bayesian
structured additive regression models. J Stat Soft 2005; 14:11</mixed-citation>
         </ref>
         <ref id="d223e711a1310">
            <label>27</label>
            <mixed-citation id="d223e718" publication-type="other">
Joint United Nations Programme on HIV/AIDS (UNAIDS). 2006
Report on the global AIDS epidemic. A UNAIDS 10th anniver-
sary special edition; 2006. Available online at [http://www.una-
ids.org/en/HIV_data/2006GlobalReport/default.asp.</mixed-citation>
         </ref>
         <ref id="d223e734a1310">
            <label>28</label>
            <mixed-citation id="d223e741" publication-type="other">
Kazembe LN. Spatial analysis of malariometric indicators in
Malawi. PhD Thesis. University of KwaZulu-Natal, South Africa;
2006</mixed-citation>
         </ref>
         <ref id="d223e755a1310">
            <label>29</label>
            <mixed-citation id="d223e762" publication-type="other">
Kazembe LN, Kleinschmidt I, Holtz TH, Sharp BL. Spatial
analysis and mapping of malaria risk in Malawi using point
referenced prevalence of infection data. Int J Health Geogr
2006;5:41</mixed-citation>
         </ref>
         <ref id="d223e778a1310">
            <label>30</label>
            <mixed-citation id="d223e785" publication-type="other">
Caulfield LE, de Onis M, Lössner M, Black RE. Undernutrition
as an underlying cause of child deaths associated with diarrhea,
pneumonia, malaria, and measles. Am J Clin Nutr 2004;80:193–
8</mixed-citation>
         </ref>
         <ref id="d223e801a1310">
            <label>31</label>
            <mixed-citation id="d223e808" publication-type="other">
Benson T, Chamberlin J, Rhinehart I. An investigation of the
spatial determinants of the local prevalence of poverty in rural
Malawi. Food Policy 2005;30:532–50</mixed-citation>
         </ref>
         <ref id="d223e821a1310">
            <label>32</label>
            <mixed-citation id="d223e828" publication-type="other">
World Bank. Profile of poverty in Malawi, 1998. Poverty analysis
of the Malawian Integrated Household Survey, 1997–1998.
Washington, D.C: The World Bank; 2000</mixed-citation>
         </ref>
         <ref id="d223e841a1310">
            <label>33</label>
            <mixed-citation id="d223e848" publication-type="other">
Spiegelhalter DJ, Thomas A, Best NG. WinBUGS Version 1.2
User Manual. MRC Biostatistics Unit. University of London;
1999</mixed-citation>
         </ref>
         <ref id="d223e861a1310">
            <label>34</label>
            <mixed-citation id="d223e868" publication-type="other">
Lambert PC. Comment on article by Browne and Draper.
Bayesian Anal 2006;1:543–6</mixed-citation>
         </ref>
         <ref id="d223e879a1310">
            <label>35</label>
            <mixed-citation id="d223e886" publication-type="other">
Lambert PC, Sutton AJ, Burton PR, Abrams KR, Jones DR. How
vague is vague? A simulation study of the impact of the use of
vague prior distributions in MCMC using WinBUGS. Stat Med
2005;24:2401–28</mixed-citation>
         </ref>
         <ref id="d223e902a1310">
            <label>36</label>
            <mixed-citation id="d223e909" publication-type="other">
Gelman A. Prior distributions for variance parameters in hierar-
chical models. Bayesian Analysis 2006;1:515–34</mixed-citation>
         </ref>
         <ref id="d223e919a1310">
            <label>37</label>
            <mixed-citation id="d223e926" publication-type="other">
Boerma JT, Black RE, Sommerfeit AE, Rustein SO, Bicego GT.
Accuracy and completeness of mother's recall of diarrhoea oc-
curence in pre-school children in demographic and health sur-
veys. Int J Epidemiol 1991;20:1073–80</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
