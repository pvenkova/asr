<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">aejmicrecon</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50001043</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>American Economic Journal: Microeconomics</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>American Economic Association</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">19457669</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">19457685</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">43189654</article-id>
         <title-group>
            <article-title>Risk-Taking and Risk-Sharing Incentives under Moral Hazard</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Mohamed</given-names>
                  <surname>Belhaj</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Renaud</given-names>
                  <surname>Bourlès</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Frédéric</given-names>
                  <surname>Deroïan</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>2</month>
            <year>2014</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">6</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40124851</issue-id>
         <fpage>58</fpage>
         <lpage>90</lpage>
         <permissions>
            <copyright-statement>Copyright © 2014 American Economic Association</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/43189654"/>
         <abstract>
            <p>This paper explores the effect of moral hazard on both risk-taking and informal risk-sharing incentives. Two agents invest in their own project, each choosing a level of risk and effort, and share risk through transfers. This can correspond to farmers in developing countries, who share risk and decide individually upon the adoption of a risky technology. The paper mainly shows that the impact of moral hazard on risk crucially depends on the observability of investment risk, whereas the impact on transfers is much more utility dependent.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d632e186a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d632e193" publication-type="other">
Suri (2011).</mixed-citation>
            </p>
         </fn>
         <fn id="d632e200a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d632e207" publication-type="other">
Townsend (1994),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d632e213" publication-type="other">
Cox and Fafchamps (2008),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d632e219" publication-type="other">
Fafchamps and Lund (2003),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d632e226" publication-type="other">
Fafchamps and Gubert
(2007),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d632e235" publication-type="other">
Angelucci and De Giorgi (2009).</mixed-citation>
            </p>
         </fn>
         <fn id="d632e242a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d632e249" publication-type="other">
Lafontaine (1992);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d632e255" publication-type="other">
Viswanath (2000)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d632e261" publication-type="other">
Roman Palestine, or Simtowe and Zeller (2007)</mixed-citation>
            </p>
         </fn>
         <fn id="d632e268a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d632e275" publication-type="other">
Jindapon and Neilson (2007),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d632e281" publication-type="other">
Modica and Scarsini (2005),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d632e287" publication-type="other">
Crainich
and Eeckhoudt (2008).</mixed-citation>
            </p>
         </fn>
         <fn id="d632e298a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d632e305" publication-type="other">
Fafchamps (2011)</mixed-citation>
            </p>
         </fn>
         <fn id="d632e312a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d632e319" publication-type="other">
Windram (2005)</mixed-citation>
            </p>
         </fn>
         <fn id="d632e326a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d632e333" publication-type="other">
Stracca (2006)</mixed-citation>
            </p>
         </fn>
         <fn id="d632e340a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d632e347" publication-type="other">
Bourlès and Henriet (2012)</mixed-citation>
            </p>
         </fn>
         <fn id="d632e354a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d632e361" publication-type="other">
Belhaj and Deroïan (2011),</mixed-citation>
            </p>
         </fn>
         <fn id="d632e368a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d632e375" publication-type="other">
Coate and Ravaillon (1993);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d632e381" publication-type="other">
Ligon, Thomas, and Worrall (2002);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d632e387" publication-type="other">
Dubois, Jullien, and Magnac (2008).</mixed-citation>
            </p>
         </fn>
         <fn id="d632e395a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d632e402" publication-type="other">
Cox, Galasso, and Jimenez (2006)</mixed-citation>
            </p>
         </fn>
         <fn id="d632e409a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d632e416" publication-type="other">
Hopenhayn and Vereshchagina (2009)</mixed-citation>
            </p>
         </fn>
         <fn id="d632e423a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d632e430" publication-type="other">
Eeckhoudt, Gollier, and Schlessinger (1996)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d632e436" publication-type="other">
Eeckhoudt, Gollier, and Schlessinger (1996)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d632e442" publication-type="other">
Gollier and Pratt (1996)</mixed-citation>
            </p>
         </fn>
         <fn id="d632e449a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d632e456" publication-type="other">
Bramoullé and Kranton 2007</mixed-citation>
            </p>
         </fn>
         <fn id="d632e463a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d632e470" publication-type="other">
Legros and Newman (2002)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d632e476" publication-type="other">
Ackerberg and Botticini (2002),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d632e482" publication-type="other">
Series (2005),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d632e489" publication-type="other">
Ghatak and Karaivanov (2011)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d632e495" publication-type="other">
Ghatak (1999, 2000)</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>REFERENCES</title>
         <ref id="d632e511a1310">
            <mixed-citation id="d632e515" publication-type="other">
Ackerberg, Daniel A., and Maristella Botticini. 2002. "Endogenous Matching and the Empirical Deter-
minants of Contract Form." Journal of Political Economy 110 (3): 564-91.</mixed-citation>
         </ref>
         <ref id="d632e525a1310">
            <mixed-citation id="d632e529" publication-type="other">
Alger, Ingela, and Jörgen W. Weibull. 2010. "Kinship, Incentives, and Evolution." American Economic
Review 100 (4): 1725-58.</mixed-citation>
         </ref>
         <ref id="d632e539a1310">
            <mixed-citation id="d632e543" publication-type="other">
Angelucci, Manuella, and Giacomo De Giorgi. 2009. "Indirect Effects of an Aid Program: How Do
Cash Transfers Affect Inéligibles Consumption?" American Economic Review 99 (1): 486-508.</mixed-citation>
         </ref>
         <ref id="d632e553a1310">
            <mixed-citation id="d632e557" publication-type="other">
Arnott, Richard J., and Joseph E. Stiglitz. 1991. "Moral Hazard and Nonmarket Institutions: Dysfunc-
tional Crowding Out or Peer Monitoring?" American Economic Review 81 (1): 179-90.</mixed-citation>
         </ref>
         <ref id="d632e568a1310">
            <mixed-citation id="d632e572" publication-type="other">
Bandiera, Oriana, and Imran Rasul. 2006. "Social Networks and Technology Adoption in Northern
Mozambique." Economic Journal 116 (514): 869-902.</mixed-citation>
         </ref>
         <ref id="d632e582a1310">
            <mixed-citation id="d632e586" publication-type="other">
Belhaj, Mohamed, and Frédéric Deroïan. 2012. "Risk taking under heterogenous revenue sharing."
Journal of Development Economics 98 (2): 192-202.</mixed-citation>
         </ref>
         <ref id="d632e596a1310">
            <mixed-citation id="d632e600" publication-type="other">
Bhattacharya, Sudipto, and Paul Pfleiderer. 1985. "Delegated portfolio management." Journal of Eco-
nomic Theory 36 (1): 1-25.</mixed-citation>
         </ref>
         <ref id="d632e610a1310">
            <mixed-citation id="d632e614" publication-type="other">
Borch, Karl. 1962. "Equilibrium in a Reinsurance Market." Econometrica 30 (3): 424-44.</mixed-citation>
         </ref>
         <ref id="d632e621a1310">
            <mixed-citation id="d632e625" publication-type="other">
Bourlès, Renaud, and Dominique Henriet. 2012. "Risk-sharing Contracts with Asymmetric Informa-
tion." Geneva Risk and Insurance Review 37 (1): 27-56.</mixed-citation>
         </ref>
         <ref id="d632e635a1310">
            <mixed-citation id="d632e639" publication-type="other">
Bramoullé, Yann, and Rachel Kranton. 2007. "Risk Sharing across Communities." American Eco-
nomic Review 97 (2): 70-7 4.</mixed-citation>
         </ref>
         <ref id="d632e650a1310">
            <mixed-citation id="d632e654" publication-type="other">
Cheung, Steven N. S. 1969. The Theory of Share Tenancy. Chicago: University of Chicago Press.</mixed-citation>
         </ref>
         <ref id="d632e661a1310">
            <mixed-citation id="d632e665" publication-type="other">
Coate, Stephen, and Martin Ravallion. 1993. "Reciprocity without commitment: Characterization and
performance of informal insurance arrangements." Journal of Development Economics 40 (1): 1-24.</mixed-citation>
         </ref>
         <ref id="d632e675a1310">
            <mixed-citation id="d632e679" publication-type="other">
Conley, Timothy G., and Christopher R. Udry. 2010. "Learning about a New Technology: Pineapple
in Ghana." American Economic Review 100 (1): 35-69.</mixed-citation>
         </ref>
         <ref id="d632e689a1310">
            <mixed-citation id="d632e693" publication-type="other">
Cox, Donald, and Marcel Fafchamps. 2008. "Extended Family and Kinship Networks: Economic
Insights and Evolutionary Directions." In Handbook of Development Economics, Vol. 4, edited by
T. Paul Schultz and John Strauss, 3711-86. Amsterdam: North-Holland.</mixed-citation>
         </ref>
         <ref id="d632e706a1310">
            <mixed-citation id="d632e710" publication-type="other">
Cox, Donald, Emanuela Galasso, and Emmanuel Jimenez. 2006. "Private Transfers in a Cross Section of
Developing Countries." Boston College Center for Retirement Research (CRR) Working Paper 2006-1.</mixed-citation>
         </ref>
         <ref id="d632e720a1310">
            <mixed-citation id="d632e724" publication-type="other">
Cox, Donald, and Emmanuel Jimenez. 1998. "Risk Sharing and Private Transfers: What about Urban
Households?" Economic Development and Cultural Change 46 (3): 621-37.</mixed-citation>
         </ref>
         <ref id="d632e735a1310">
            <mixed-citation id="d632e739" publication-type="other">
Crainich, David, and Louis Eeckhoudt. 2008. "On the intensity of downside risk aversion." Journal of
Risk and Uncertainty 36 (3): 267-76.</mixed-citation>
         </ref>
         <ref id="d632e749a1310">
            <mixed-citation id="d632e753" publication-type="other">
Dubois, Pierre, Bruno Jullien, and Thierry Magnac. 2008. "Formal and Informal Risk Sharing in
LDCs: Theory and Empirical Evidence." Econometrica 76 (4): 679-725.</mixed-citation>
         </ref>
         <ref id="d632e763a1310">
            <mixed-citation id="d632e767" publication-type="other">
Eeckhoudt, Louis, Christian Gollier, and Harris Schlessinger. 1996. "Changes in Background Risk and
Risk Taking Behavior." Econometrica 64 (3): 683-89.</mixed-citation>
         </ref>
         <ref id="d632e777a1310">
            <mixed-citation id="d632e781" publication-type="other">
Eswaran, Mukesh, and Ashok Kotwal. 1985. "A Theory of Contractual Structure in Agriculture."
American Economic Review 75 (3): 352-67.</mixed-citation>
         </ref>
         <ref id="d632e791a1310">
            <mixed-citation id="d632e795" publication-type="other">
Evenson, Robert E., and Douglas Gollin. 2003. "Assessing the Impact of the Green Revolution, 1960
to 2000." Science 300 (5620): 758-62.</mixed-citation>
         </ref>
         <ref id="d632e805a1310">
            <mixed-citation id="d632e809" publication-type="other">
Fafchamps, Marcel. 2010. "Vulnerability, risk management, and agricultural development." African
Journal of Agricultural Economics 5 (1): 243-60.</mixed-citation>
         </ref>
         <ref id="d632e820a1310">
            <mixed-citation id="d632e824" publication-type="other">
Fafchamps, Marcel. 2011. "Risk Sharing Between Households." In Handbook of Social Economics,
Vol. 1A, edited by Jess Benhabib, Alberto Bisin, and Matthew O. Jackson, 1255-80. Amsterdam:
North-Holland.</mixed-citation>
         </ref>
         <ref id="d632e837a1310">
            <mixed-citation id="d632e841" publication-type="other">
Fafchamps, Marcel, and Flore Gubert. 2007. "The formation of risk sharing networks." Journal of
Development Economics 83 (2): 326-50.</mixed-citation>
         </ref>
         <ref id="d632e851a1310">
            <mixed-citation id="d632e855" publication-type="other">
Fafchamps, Marcel, and Susan Lund. 2003. "Risk-sharing networks in rural Philippines." Journal of
Development Economics 71 (2): 261-87.</mixed-citation>
         </ref>
         <ref id="d632e865a1310">
            <mixed-citation id="d632e869" publication-type="other">
Fishburn, Peter C., and R. Burr Porter. 1976. "Optimal portfolios with one safe and one risky asset:
effects of changes in the rate of return and risk." Management Science 22 (10): 1064-73.</mixed-citation>
         </ref>
         <ref id="d632e879a1310">
            <mixed-citation id="d632e883" publication-type="other">
Ghatak, Maitreesh. 1999. "Group lending, local information and peer selection." Journal of Develop-
ment Economics 60 (1): 27-50.</mixed-citation>
         </ref>
         <ref id="d632e893a1310">
            <mixed-citation id="d632e897" publication-type="other">
Ghatak, Maitreesh. 2000. "Screening by the Company You Keep: Joint Liability Lending and the Peer
Selection Effect." Economic Journal 110 (465): 601-31.</mixed-citation>
         </ref>
         <ref id="d632e908a1310">
            <mixed-citation id="d632e912" publication-type="other">
Ghatak, Maitreesh, and Alexander Karavainov. Forthcoming. "Contractual Structure in Agriculture
with Endogenous Matching." Journal of Development Economics.</mixed-citation>
         </ref>
         <ref id="d632e922a1310">
            <mixed-citation id="d632e926" publication-type="other">
Gollier, Christian, and John W. Pratt. 1996. "Risk Vulnerability and the Tempering Effect of Back-
ground Risk." Econometrica 64 (5): 1109-23.</mixed-citation>
         </ref>
         <ref id="d632e936a1310">
            <mixed-citation id="d632e940" publication-type="other">
Hadar, Josef, and Tae Kun Seo. 1990. "The Effects of Shifts in a Return Distribution on Optimal Port-
folios." International Economic Review 31 (3): 721-36.</mixed-citation>
         </ref>
         <ref id="d632e950a1310">
            <mixed-citation id="d632e954" publication-type="other">
Holmström, Bengt. 1982. "Moral Hazard in Teams." Bell Journal of Economics 13 (2): 324-40.</mixed-citation>
         </ref>
         <ref id="d632e961a1310">
            <mixed-citation id="d632e965" publication-type="other">
Jindapon, Paan, and William S. Neilson. 2007. "Higher-order generalizations of Arrow-Pratt and Ross
risk aversion: A comparative statics approach." Journal of Economic Theory 136 (1): 719-28.</mixed-citation>
         </ref>
         <ref id="d632e975a1310">
            <mixed-citation id="d632e979" publication-type="other">
Lafontaine, Francine. 1992. "Agency theory and franchising: some empirical results." Rand Journal
of Economics 23 (2): 263-83.</mixed-citation>
         </ref>
         <ref id="d632e990a1310">
            <mixed-citation id="d632e994" publication-type="other">
Legros, Patrick, and Andrew F. Newman. 2002. "Monotone Matching in Perfect and Imperfect
Worlds." Review of Economic Studies 69 (4): 925-42.</mixed-citation>
         </ref>
         <ref id="d632e1004a1310">
            <mixed-citation id="d632e1008" publication-type="other">
Ligon, Ethan, Jonathan P. Thomas, and Tim Worrall. 2001. "Informal Insurance Arrangements in
Limited Commitment: Theory and Evidence from Village Economies." Review of Economic Stud-
ies 69 (1): 209-44.</mixed-citation>
         </ref>
         <ref id="d632e1021a1310">
            <mixed-citation id="d632e1025" publication-type="other">
Modica, Salvatore, and Marco Scarsini. 2005. "A note on comparative downside risk aversion." Jour-
nal of Economic Theory 122 (2): 267-71.</mixed-citation>
         </ref>
         <ref id="d632e1035a1310">
            <mixed-citation id="d632e1039" publication-type="other">
Morris, Michael, Valerie A. Kelly, Ron J. Kopicki, and Derek Byerlee. 2007. Fertilizer Use in African
Agriculture , Lessons Learned and Good Practice Guidelines. Washington, DC: World Bank.</mixed-citation>
         </ref>
         <ref id="d632e1049a1310">
            <mixed-citation id="d632e1053" publication-type="other">
Prescot, Edward S., and Robert M. Townsend. 2006. "Theory of the Firm: Applied Mechanism
Design." Federal Reserve Bank of Richmond Working Paper 96-2.</mixed-citation>
         </ref>
         <ref id="d632e1063a1310">
            <mixed-citation id="d632e1067" publication-type="other">
Reid, Joseph D., Jr. 1976. "Sharecropping and Agricultural Uncertainty." Economic Development and
Cultural Change 24 (3): 549-76.</mixed-citation>
         </ref>
         <ref id="d632e1078a1310">
            <mixed-citation id="d632e1082" publication-type="other">
Reid, Joseph D., Jr. 1977. "The Theory of Share Tenancy Revisited—Again." Journal of Political
Economy 85 (2): 403-07.</mixed-citation>
         </ref>
         <ref id="d632e1092a1310">
            <mixed-citation id="d632e1096" publication-type="other">
Series, Konstantīnos. 2005. "Risk sharing vs. incentives: Contract design under two-sided heterogene-
ity." Economics Letters 88 (3): 343-49.</mixed-citation>
         </ref>
         <ref id="d632e1106a1310">
            <mixed-citation id="d632e1110" publication-type="other">
Simtowe, Franklin, and Manfred Zeller. 2007. "Determinants of Moral Hazard in Microfinance:
Empirical Evidence from Joint Liability Lending Programs in Malawi." Munich Personal RePEc
Archive (MPRA) Paper 461.</mixed-citation>
         </ref>
         <ref id="d632e1123a1310">
            <mixed-citation id="d632e1127" publication-type="other">
Suri, Tavneet. 2011. "Selection and Comparative Advantage in Technology Adoption." Econometrica
79 (1): 159-209.</mixed-citation>
         </ref>
         <ref id="d632e1137a1310">
            <mixed-citation id="d632e1141" publication-type="other">
Stiglitz, Joseph E. 1974. "Incentives and Risk Sharing in Sharecropping." Review of Economic Stud-
ies 41 (2): 219-55.</mixed-citation>
         </ref>
         <ref id="d632e1151a1310">
            <mixed-citation id="d632e1155" publication-type="other">
Stracca, Livio. 2006. "Delegated Portfolio Management: A Survey of the Theoretical Literature" Jour-
nal of Economic Surveys 20 (5): 823-48.</mixed-citation>
         </ref>
         <ref id="d632e1166a1310">
            <mixed-citation id="d632e1170" publication-type="other">
Townsend, Robert M. 1994. "Risk and Insurance in Village India." Econometrica 62 (3): 539-91.</mixed-citation>
         </ref>
         <ref id="d632e1177a1310">
            <mixed-citation id="d632e1181" publication-type="other">
Vereshchagina, Galina, and Hugo A. Hopenhayn. 2009. "Risk Taking by Entrepreneurs." American
Economic Review 99 (5): 1808-30.</mixed-citation>
         </ref>
         <ref id="d632e1191a1310">
            <mixed-citation id="d632e1195" publication-type="other">
Viswanath, P. V. 2000. "Risk sharing, diversification and moral hazard in Roman Palestine: evidence
from agricultural contract law." International Review of Law and Economics 20 (3): 353-69.</mixed-citation>
         </ref>
         <ref id="d632e1205a1310">
            <mixed-citation id="d632e1209" publication-type="other">
Windram, Richard. 2005. "Risk-Taking Incentives: A Review of the Literature." Journal of Economic
Surveys 19(1): 65-90.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
