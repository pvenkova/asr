<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt7t4v9</book-id>
      <subj-group>
         <subject content-type="call-number">HC79.I5M55 2005</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Income distribution</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Economic development</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Globalization</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Equality</subject>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Business</subject>
         <subject>Sociology</subject>
      </subj-group>
      <book-title-group>
         <book-title>Worlds Apart</book-title>
         <subtitle>Measuring International and Global Inequality</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Milanovic</surname>
               <given-names>Branko</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>27</day>
         <month>06</month>
         <year>2011</year>
      </pub-date>
      <isbn content-type="ppub">9780691130514</isbn>
      <isbn content-type="ppub">0691130515</isbn>
      <isbn content-type="epub">9781400840816</isbn>
      <publisher>
         <publisher-name>Princeton University Press</publisher-name>
         <publisher-loc>PRINCETON; OXFORD</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2005</copyright-year>
         <copyright-holder>Princeton University Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt7t4v9"/>
      <abstract abstract-type="short">
         <p>We are used to thinking about inequality within countries--about rich Americans versus poor Americans, for instance. But what about inequality between all citizens of the world?<italic>Worlds Apart</italic>addresses just how to measure global inequality among individuals, and shows that inequality is shaped by complex forces often working in different directions. Branko Milanovic, a top World Bank economist, analyzes income distribution worldwide using, for the first time, household survey data from more than 100 countries. He evenhandedly explains the main approaches to the problem, offers a more accurate way of measuring inequality among individuals, and discusses the relevant policies of first-world countries and nongovernmental organizations.</p>
         <p>Inequality has increased between nations over the last half century (richer countries have generally grown faster than poorer countries). And yet the two most populous nations, China and India, have also grown fast. But over the past two decades inequality within countries has increased. As complex as reconciling these three data trends may be, it is clear: the inequality between the world's individuals is staggering. At the turn of the twenty-first century, the richest 5 percent of people receive one-third of total global income, as much as the poorest 80 percent. While a few poor countries are catching up with the rich world, the differences between the richest and poorest individuals around the globe are huge and likely growing.</p>
      </abstract>
      <counts>
         <page-count count="240"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7t4v9.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7t4v9.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7t4v9.3</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7t4v9.4</book-part-id>
                  <title-group>
                     <title>Prologue</title>
                     <subtitle>The Promise of the Twentieth Century</subtitle>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>In 1963 French economist Jean Fourastié published the book<italic>Le grand espoir du XX siècle</italic>. I read the book in 1973 or 1974, after the first oil shock, and around the time Fourastié’s “thirty glorious years” (les trente glorieuses) were coming to a close. “Thirty glorious years” was Fourastié’s name for the period of sustained and high economic growth that, from the end of World War II to the mid-1970s, transformed the economies of Western Europe. Many of them were within a generation catapulted from predominantly agrarian to modern postindustrial societies. When I read the book, despite the gloom brought</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7t4v9.5</book-part-id>
                  <title-group>
                     <title>Introduction</title>
                     <subtitle>A Topic Whose Time Has Come</subtitle>
                  </title-group>
                  <fpage>3</fpage>
                  <abstract>
                     <p>World inequality is a topic whose time has come. There is more talk and writing than ever on globalization. The post–Cold War world is truly a globalized world. And with globalization on the agenda, our view as to what is the proper object of study changes too. Topics of interest are now global in their scope: global public goods, difficulty of pursuing national macro policies in a world of globalized capital flows, global environment issues, and then, of course, global inequality. One may conjecture whether in some not-too-distant future we would reach the situation where we would be interested</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7t4v9.6</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>The Three Concepts of Inequality Defined</title>
                  </title-group>
                  <fpage>7</fpage>
                  <abstract>
                     <p>There are three concepts of world inequality that need to be sharply distinguished. Yet, they are often confounded; even the terminology is unclear. So, we shall now first define them and give them their proper names.</p>
                     <p>The first (Concept 1) is unweighted international inequality. This concept takes country as the unit of observation, uses its income (or GDP) per capita, disregards its population, and thus compares, as it were, representative individuals from all the countries in the world. It is a kind of UN General Assembly where each country, small or large, counts the same. Imagine a world populated with</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7t4v9.7</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>Other Differences between the Concepts</title>
                  </title-group>
                  <fpage>12</fpage>
                  <abstract>
                     <p>Do different studies of world or international inequality differ only by the concept they use? Unfortunately not. Other differences also complicate comparisons. If readers or even researchers are not aware of these differences, comparing the results is difficult. And even when these differences are taken into account, comparisons remain difficult because the relationship among different variables (e.g., between GDP per capita and mean income or expenditures from household surveys) is not clear or obvious.</p>
                     <p>First, when we compare incomes of individuals who live in different nations, we need to express them in a common currency. Some studies use the simple</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7t4v9.8</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>International and World Inequality Compared</title>
                  </title-group>
                  <fpage>20</fpage>
                  <abstract>
                     <p>To see what exactly the differences among the three concepts are we shall write out the Gini formula (since we would be using mostly the Gini coefficient to estimate inequality).¹⁰ The Gini coefficient ranges from 0 (all recipients have the same income: full equality) to 100 (all income is received by one recipient only: maximum inequality).¹¹ One of the reasons for Gini’s popularity is that it can be easily represented in graphical terms. It is equal to twice the area lying between the line of perfect equality (the 45° line) and the Lorenz curve (see figure 3.1). The Lorenz curve</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7t4v9.9</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Rising Differences in Per Capita Incomes</title>
                  </title-group>
                  <fpage>31</fpage>
                  <abstract>
                     <p>We shall consider first the easiest concept—unweighted international inequality. In the analysis in this chapter, we shall never refer to the population. We shall ignore it altogether as if the growth rate of a tiny country had the same importance for the world as the growth rate of China. This is an approach that makes sense, first, for the reasons of economic policy-making, because we can regard each country’s experience as an observation on what works and why (and for that approach, the size of the country clearly does not matter), and second, because our view of the world</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7t4v9.10</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Regional Convergence, Divergence, or … “Vergence”</title>
                  </title-group>
                  <fpage>45</fpage>
                  <abstract>
                     <p>In the movie<italic>Mr. Smith Goes to Washington</italic>, the future Senator Smith is asked whether he is in favor of deflation or inflation. Aware that an affirmative answer to either is going to lead him into trouble, he declares himself to be in favor of ... “flation.”³³ Our analysis of unweighted international inequality has a direct bearing on two topics that have recently been very much present in economic literature: the issue of GDP per capita convergence or divergence (or lack of either: “vergence”), and the bimodality of international income distribution (the “twin peaks”).</p>
                     <p>The idea of convergence derives from</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7t4v9.11</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>The Shape of International GDP Per Capita Distribution</title>
                  </title-group>
                  <fpage>51</fpage>
                  <abstract>
                     <p>Is the effect of pulling apart the countries also visible in the shape of the international distribution of GDPs per capita? Figure 6.1 gives a nonparametric estimate of the distribution of countries’ GDPs per capita in 1960, 1978, and 2000. The values are normalized by the unweighted 1960 mean world GDP per capita so as to better capture growth of real incomes. Over both periods there is an “emptying out” of the poorest countries, as they move up in terms of income. The shaded part in 1960 moves up (is distributed) among different income levels, and the entire distribution to</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7t4v9.12</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Winners and Losers:</title>
                     <subtitle>Increasing Dominance of the West</subtitle>
                  </title-group>
                  <fpage>61</fpage>
                  <abstract>
                     <p>The emptying out of the middle of income distribution had the following two consequences. It reinforced the already strong domination of Western countries at the very top of the income distribution, and it reduced the number of possible contenders for positions in the top of income distribution. In other words, Western countries have pulled ahead of the rest of the world, and in only a few exceptional cases have non-Western countries been able to catch up.⁴⁸</p>
                     <p>We define GDP per capita of the poorest WENAO country (excluding Turkey) as the cut-off point between the rich and those immediately behind them,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7t4v9.13</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>Concept 2 Inequality:</title>
                     <subtitle>Decreasing in the Past Twenty Years</subtitle>
                  </title-group>
                  <fpage>85</fpage>
                  <abstract>
                     <p>Figure 8.1 shows population-weighted international inequality calculated for the same period and same countries as before (see chapter 4 on unweighted international inequality). After a significant jump in 1952 when China was added to the sample, and then in 1960 when African countries were included, the weighted international inequality almost constantly slides down, and the decline accelerates in the decade of the 1990s. Between 1965 and 2000, across a practically constant sample of countries, the weighted international inequality decreased from a Gini of 55.7 to 50.5—a 10 percent drop. The decline of the Theil index was even steeper.</p>
                     <p>What</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7t4v9.14</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>High Global Inequality:</title>
                     <subtitle>No Trend?</subtitle>
                  </title-group>
                  <fpage>101</fpage>
                  <abstract>
                     <p>World or global inequality treats, in principle, all individuals in the world the same. It is concerned with their individual incomes and ranks them from the poorest to the richest regardless of the country where they live. Such world inequality can still, of course, be disaggregated into that part of inequality that is due to the differences in mean countries’ incomes in the same way as overall inequality in a given country can be decomposed into a part of inequality due to differences in mean regional incomes.</p>
                     <p>The source of data for world income distribution are household surveys (HS). Since</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7t4v9.15</book-part-id>
                  <title-group>
                     <label>10</label>
                     <title>A World without a Middle Class</title>
                  </title-group>
                  <fpage>128</fpage>
                  <abstract>
                     <p>What is the world’s middle class? Does the question make sense? No: if we believe that the concept has a meaning only if there is a community with similar customs, language, and history, and with a government ruling over a precisely delimited territory. Yes: if treat the world as a single entity as we have done throughout. We shall now turn to this question, which in the context of a single country is, since Aristotle, linked with the question of social stability. One could argue that—even in the world context—it may be reasonable to ask whether a small</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7t4v9.16</book-part-id>
                  <title-group>
                     <label>11</label>
                     <title>The Three Concepts of Inequality in Historical Perspective</title>
                  </title-group>
                  <fpage>139</fpage>
                  <abstract>
                     <p>Thanks to recent historical research in countries’ income levels (Maddison 2001) and inequality (among others by Morrisson 2000), we are able to make reasonably informed estimates of concepts 1, 2, and 3 inequality going all the way back to 1820, that is, to the beginning of the modern times. In principle, calculations of concepts 1 and 2 should be easier and more reliable because for them we need only two pieces of information: GDP per capita and population (both provided by Maddison). However, the problem—even if this information were fully correct—is that for large parts of the world</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7t4v9.17</book-part-id>
                  <title-group>
                     <label>12</label>
                     <title>Why Does Global Inequality Matter and What to Do about It?</title>
                  </title-group>
                  <fpage>149</fpage>
                  <abstract>
                     <p>The arguments against global inequality and in favor of some redistribution or help for the world’s poorest are inextricably linked with the argument about the need for emocratization at the global level. This is a carbon copy of the argument, made in the nineteenth century at the national level, linking the spread of franchise and the empowerment of the poor. Note however, that, at the global level we cannot even speak of “democracy,” but in an almost Soviet-speak, the best we can do is to argue for “democratization,” that is, for the introduction of some elements of democratic (one person,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7t4v9.18</book-part-id>
                  <title-group>
                     <title>Appendixes 1–7</title>
                  </title-group>
                  <fpage>163</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7t4v9.19</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>195</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7t4v9.20</book-part-id>
                  <title-group>
                     <title>References</title>
                  </title-group>
                  <fpage>213</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7t4v9.21</book-part-id>
                  <title-group>
                     <title>Index of Authors</title>
                  </title-group>
                  <fpage>223</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7t4v9.22</book-part-id>
                  <title-group>
                     <title>Index of Subjects</title>
                  </title-group>
                  <fpage>225</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
