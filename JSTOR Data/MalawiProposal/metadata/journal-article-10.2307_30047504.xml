<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">procbiolscie</journal-id>
         <journal-id journal-id-type="jstor">j100837</journal-id>
         <journal-title-group>
            <journal-title>Proceedings: Biological Sciences</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>The Royal Society</publisher-name>
         </publisher>
         <issn pub-type="ppub">09628452</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">30047504</article-id>
         <article-id pub-id-type="pub-doi">10.1098/rspb.2004.2903</article-id>
         <title-group>
            <article-title>Inbreeding Uncovers Fundamental Differences in the Genetic Load Affecting Male and Female Fertility in a Butterfly</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Ilik J.</given-names>
                  <surname>Saccheri</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Hywel D.</given-names>
                  <surname>Lloyd</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Sarah J.</given-names>
                  <surname>Helyar</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Paul M.</given-names>
                  <surname>Brakefield</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>7</day>
            <month>1</month>
            <year>2005</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">272</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1558</issue>
         <issue-id>i30047497</issue-id>
         <fpage>39</fpage>
         <lpage>46</lpage>
         <permissions>
            <copyright-statement>Copyright 2005 The Royal Society</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/30047504"/>
         <abstract>
            <p>Inbreeding depression is most pronounced for traits closely associated with fitness. The traditional explanation is that natural selection eliminates deleterious mutations with additive or dominant effects more effectively than recessive mutations, leading to directional dominance for traits subject to strong directional selection. Here we report the unexpected finding that, in the butterfly Bicyclus anynana, male sterility contributes disproportionately to inbreeding depression for fitness (complete sterility in about half the sons from brother-sister matings), while female fertility is insensitive to inbreeding. The contrast between the sexes for functionally equivalent traits is inconsistent with standard selection arguments, and suggests that trait-specific developmental properties and cryptic selection play crucial roles in shaping genetic architecture. There is evidence that spermatogenesis is less developmentally stable than oogenesis, though the unusually high male fertility load in B. anynana additionally suggests the operation of complex selection maintaining male sterility recessives. Analysis of the precise causes of inbreeding depression will be needed to generate a model that reliably explains variation in directional dominance and reconciles the gap between observed and expected genetic loads carried by populations. This challenging evolutionary puzzle should stimulate work on the occurrence and causes of sex differences in fertility load.</p>
         </abstract>
         <kwd-group>
            <kwd>inbreeding depression</kwd>
            <kwd>genetic load</kwd>
            <kwd>fertility</kwd>
            <kwd>egg hatching</kwd>
            <kwd>lepidoptera</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d837e259a1310">
            <mixed-citation id="d837e263" publication-type="other">
Brakefield, P. M. &amp; Reitsma, N. 1991 Phenotypic plasticity,
seasonal climate and the population biology of Bicyclus but-
terflies (Satyridae) in Malawi. Ecol. Entomol. 16, 291-303.</mixed-citation>
         </ref>
         <ref id="d837e276a1310">
            <mixed-citation id="d837e280" publication-type="other">
Brakefield, P. M., El Filali, E., Van der Laan, R., Breuker, C. J.,
Saccheri, I. J. &amp; Zwaan, B. 2001 Effective population
size, reproductive success and sperm precedence in the
butterfly, Bicyclus anynana, in captivity. J. Evol. Biol. 14,
148-156.</mixed-citation>
         </ref>
         <ref id="d837e299a1310">
            <mixed-citation id="d837e303" publication-type="other">
Charlesworth, B. 1994 The effect of background selection
against deleterious mutations on weakly selected, linked
variants. Genet. Res. 63, 213-227.</mixed-citation>
         </ref>
         <ref id="d837e316a1310">
            <mixed-citation id="d837e320" publication-type="other">
Charlesworth, B. &amp; Charlesworth, D. 1999 The genetic basis
of inbreeding depression. Genet. Res. 74, 329-340.</mixed-citation>
         </ref>
         <ref id="d837e331a1310">
            <mixed-citation id="d837e335" publication-type="other">
Chung, C. S. 1962 Relative genetic loads due to lethal and
detrimental genes in irriadiated populations of Drosophila
melanogaster. Genetics 47, 1489-1504.</mixed-citation>
         </ref>
         <ref id="d837e348a1310">
            <mixed-citation id="d837e352" publication-type="other">
Crnokrak, P. &amp; Barrett, S. C. H. 2002 Purging the genetic
load: a review of the experimental evidence. Evolution 56,
2347-2358.</mixed-citation>
         </ref>
         <ref id="d837e365a1310">
            <mixed-citation id="d837e369" publication-type="other">
Crnokrak, P. &amp; Roff, D. A. 1995 Dominance variance: asso-
ciations with selection and fitness. Heredity 75, 530-540.</mixed-citation>
         </ref>
         <ref id="d837e379a1310">
            <mixed-citation id="d837e383" publication-type="other">
Crow, J. F. 1993 Mutation, mean fitness, and genetic load.
Oxf. Surv. Evol. Biol. 9, 3-42.</mixed-citation>
         </ref>
         <ref id="d837e393a1310">
            <mixed-citation id="d837e397" publication-type="other">
Dobzhansky, T., Spassky, B. &amp; Tidwell, T. 1963 Genetics of
natural populations. XXXII. Inbreeding and the mutational
and balanced genetic loads in natural populations of
Drosophila pseudoobscura. Genetics 48, 361-373.</mixed-citation>
         </ref>
         <ref id="d837e413a1310">
            <mixed-citation id="d837e417" publication-type="other">
Drummond, B. A. 1984 Multiple mating and sperm compe-
tition in the Lepidoptera. In Sperm competition and the
evolution of animal mating systems (ed. R. L. Smith), pp.
291-370. London: Academic.</mixed-citation>
         </ref>
         <ref id="d837e434a1310">
            <mixed-citation id="d837e438" publication-type="other">
Gage, M. J. G. &amp; Cook, P. A. 1994 Sperm size or numbers?
Effects of nutritional stress upon eupyrene and apyrene
sperm production strategies in the moth Plodia interpunctella
(Lepidoptera: Pyralidae). Funct. Ecol. 8, 594-599.</mixed-citation>
         </ref>
         <ref id="d837e454a1310">
            <mixed-citation id="d837e458" publication-type="other">
Garcia, N., López-Fanjul, C. &amp; García-Dorado, A. 1994 The
genetics of viability in Drosophila melanogaster: effects of
inbreeding and artificial selection. Evolution 48, 1277-1285.</mixed-citation>
         </ref>
         <ref id="d837e471a1310">
            <mixed-citation id="d837e475" publication-type="other">
Greenberg, R. &amp; Crow, J. F. 1960 A comparison of the effect
of lethal and detrimental chromosomes from Drosophila
populations. Genetics 45, 1153-1168.</mixed-citation>
         </ref>
         <ref id="d837e488a1310">
            <mixed-citation id="d837e492" publication-type="other">
Hedrick, P. W. 1994 Purging inbreeding depression and
the probability of extinction: full-sib mating. Heredity 73,
363-372.</mixed-citation>
         </ref>
         <ref id="d837e505a1310">
            <mixed-citation id="d837e509" publication-type="other">
Higashiura, Y., Ishihara, M. &amp; Schaefer, P. W. 1999 Sex-ratio
distortion and severe inbreeding depression in the gypsy
moth Lymantria dispar L. in Hokkaido, Japan. Heredity 83,
290-297.</mixed-citation>
         </ref>
         <ref id="d837e525a1310">
            <mixed-citation id="d837e529" publication-type="other">
Ingvarsson, P. K. &amp; Whitlock, M. C. 2000 Heterosis increases
the effective migration rate. Proc. R. Soc. B 267, 1321-1326.
(doi: 10.1098/rspb.2000.1145)</mixed-citation>
         </ref>
         <ref id="d837e543a1310">
            <mixed-citation id="d837e547" publication-type="other">
Joron, M. &amp; Brakefield, P. M. 2003 Captivity masks inbreed-
ing effects on male mating success in butterflies. Nature 424,
191-194.</mixed-citation>
         </ref>
         <ref id="d837e560a1310">
            <mixed-citation id="d837e564" publication-type="other">
Keightley, P. D. &amp; Lynch, M. 2003 Toward a realistic model
of mutations affecting fitness. Evolution 57, 683-685.</mixed-citation>
         </ref>
         <ref id="d837e574a1310">
            <mixed-citation id="d837e578" publication-type="other">
Keller, L. F. &amp; Waller, D. M. 2002 Inbreeding effects in wild
populations. Trends Ecol. Evol. 17, 230-241.</mixed-citation>
         </ref>
         <ref id="d837e588a1310">
            <mixed-citation id="d837e592" publication-type="other">
Lindsley, D. L. &amp; Lifschytz, E. 1972 The genetic control of
spermatogenesis. In Proc. Int. Symp. The Genetics of the Sper-
matozoon (ed. R. A. Beatty &amp; S. Gluecksohn-Waelsch), pp.
203-222. Copenhagen: Bogtrykkeriet Forum.</mixed-citation>
         </ref>
         <ref id="d837e608a1310">
            <mixed-citation id="d837e612" publication-type="other">
Lindsley, D. L. &amp; Tokuyasu, K. T. 1980 Spermatogenesis. In
The genetics and biology of Drosophila, vol. 2a (ed. M.
Ashburner &amp; T. R. F. Wright), pp. 225-294. New York:
Academic.</mixed-citation>
         </ref>
         <ref id="d837e628a1310">
            <mixed-citation id="d837e632" publication-type="other">
Lynch, M. &amp; Walsh, J. B. 1998 Genetics and analysis of quanti-
tative traits. Sunderland, MA: Sinauer.</mixed-citation>
         </ref>
         <ref id="d837e643a1310">
            <mixed-citation id="d837e647" publication-type="other">
Meves, F. 1902 über oligopyrene und apyrene Spermien und
über ihre Entstehung, nach Beobachtungen an Paludina
und Pygaera. Archiv. Mikrosk. Anat. 61, 1-84.</mixed-citation>
         </ref>
         <ref id="d837e660a1310">
            <mixed-citation id="d837e664" publication-type="other">
Monteiro, A. &amp; Pierce, N. E. 2000 Phylogeny of Bicyclus
(Lepidoptera: Nymphalidae) inferred from COI, COII and
EF-la gene sequences. Mol. Phylogenet. Evol. 18, 264-281.</mixed-citation>
         </ref>
         <ref id="d837e677a1310">
            <mixed-citation id="d837e681" publication-type="other">
Morton, N. E., Crow, J. F. &amp; Muller, H. J. 1956 An estimate of
the mutational damage in man from data on consaguineous
marriages. Proc. Natl Acad. Sci. USA 42, 855-863.</mixed-citation>
         </ref>
         <ref id="d837e694a1310">
            <mixed-citation id="d837e698" publication-type="other">
Parker, G. A. 1983 Mate quality and mating decisions. In
Mate choice (ed. P. P. G. Bateson), pp. 141-166. Cambridge
University Press.</mixed-citation>
         </ref>
         <ref id="d837e711a1310">
            <mixed-citation id="d837e715" publication-type="other">
Pavan, C., Cordiero, R., Dobzhansky, N., Dobzhansky, T.,
Malogolowkin, C., Spassky, B. &amp; Wedel, M. 1951 Con-
cealed genetic variability in Brazilian populations of
Drosophila willinstoni. Genetics 36, 13-30.</mixed-citation>
         </ref>
         <ref id="d837e731a1310">
            <mixed-citation id="d837e735" publication-type="other">
Pusey, A. &amp; Wolf, M. 1996 Inbreeding avoidance in animals.
Trends Ecol. Evol. 11, 201-206.</mixed-citation>
         </ref>
         <ref id="d837e746a1310">
            <mixed-citation id="d837e750" publication-type="other">
Ralls, K., Ballou, J. D. &amp; Templeton, A. 1988 Estimates of
lethal equivalents and the cost of inbreeding in mammals.
Conserv. Biol. 2, 185-193.</mixed-citation>
         </ref>
         <ref id="d837e763a1310">
            <mixed-citation id="d837e767" publication-type="other">
Saccheri, I. J. &amp; Brakefield, P. M. 2002 Rapid spread of immi-
grant genomes into inbred populations. Proc. R. Soc. B 269,
1073-1078. (doi: 10.1098/rspb.2002.1963)</mixed-citation>
         </ref>
         <ref id="d837e780a1310">
            <mixed-citation id="d837e784" publication-type="other">
Saccheri, I. J., Brakefield, P. M. &amp; Nichols, R. A. 1996 Severe
inbreeding depression and rapid fitness rebound in the but-
terfly Bicyclus anynana (Satyridae). Evolution 50, 2000-
2013.</mixed-citation>
         </ref>
         <ref id="d837e800a1310">
            <mixed-citation id="d837e804" publication-type="other">
Saccheri, I. J., Kuussaari, M., Kankare, M., Vikman, P.,
Fortelius, W. &amp; Hanski, I. 1998 Inbreeding and extinction
in a butterfly metapopulation. Nature 392, 491-494.</mixed-citation>
         </ref>
         <ref id="d837e817a1310">
            <mixed-citation id="d837e821" publication-type="other">
Saccheri, I. J., Wilson, I. J., Nichols, R. A., Bruford, M. W. &amp;
Brakefield, P. M. 1999 Inbreeding of bottlenecked butterfly
populations: estimation using the likelihood of changes in
marker allele frequencies. Genetics 151, 1053-1063.</mixed-citation>
         </ref>
         <ref id="d837e837a1310">
            <mixed-citation id="d837e841" publication-type="other">
Saccheri, I. J., Nichols, R. A. &amp; Brakefield, P. M. 2001 Effects
of bottlenecks on quantitative genetic variation in the but-
terfly Bicyclus anynana. Genet. Res. 77, 167-181.</mixed-citation>
         </ref>
         <ref id="d837e855a1310">
            <mixed-citation id="d837e859" publication-type="other">
Silberglied, R. E., Sheperd, J. G. &amp; Dickinson, J. L. 1984
Eunuchs: the role of apyrene sperm in Lepidoptera? Am.
Nat. 123, 255-265.</mixed-citation>
         </ref>
         <ref id="d837e872a1310">
            <mixed-citation id="d837e876" publication-type="other">
Simmons, M. J. &amp; Crow, J. F. 1977 Mutations affecting fitness
in Drosophila populations. A. Rev. Genet. 11, 49-78.</mixed-citation>
         </ref>
         <ref id="d837e886a1310">
            <mixed-citation id="d837e890" publication-type="other">
Temin, R. G. 1966 Homozygous viability and fertility loads in
Drosphila melanogaster. Genetics 53, 27-46.</mixed-citation>
         </ref>
         <ref id="d837e900a1310">
            <mixed-citation id="d837e904" publication-type="other">
Tregenza, T. &amp; Wedell, N. 2002 Polyandrous females avoid
costs of inbreeding. Nature 415, 71-73.</mixed-citation>
         </ref>
         <ref id="d837e914a1310">
            <mixed-citation id="d837e918" publication-type="other">
van Oosterhout, C., Zijlstra, W. G., Heuven, M. K. V. &amp;
Brakefield, P. M. 2000 Inbreeding depression and genetic
load in laboratory metapopulations of the butterfly Bicyclus
anynana. Evolution 54, 218-225.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
