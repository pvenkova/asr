<?xml version="1.0" encoding="UTF-8"?>
<article article-type="research-article" dtd-version="1.0" xml:lang="en">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="publisher-id">afrdevafrdev</journal-id>
         <journal-title-group>
            <journal-title content-type="full">Africa Development / Afrique et Développement</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>CODESRIA</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">08503907</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>jstor_product</meta-name>
               <meta-value>archive_collection_journals</meta-value>
            </custom-meta>
         </custom-meta-group>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="publisher-id">afrdevafrdev.37.4.141</article-id>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">afrdevafrdev.37.4.141</article-id>
         <title-group>
            <article-title>Africa's Growth and Development Strategies: A Critical Review</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Babatunde</surname>
                  <given-names>Musibau Adetunji</given-names>
               </string-name>
               <aff>Department of Economics, University of Ibadan, Ibadan, Nigeria. Email:<email xmlns:xlink="http://www.w3.org/1999/xlink"
                         xlink:href="tunjiyusuf19@yahoo.com">tunjiyusuf19@yahoo.com</email>
               </aff>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <year>2012</year>
            <string-date>2012</string-date>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink">37</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink">4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">afrdevafrdev.37.issue-4</issue-id>
         <fpage>141</fpage>
         <lpage>178</lpage>
         <permissions>
            <copyright-statement>© 2012 Council for the Development of Social Science Research in Africa</copyright-statement>
            <copyright-year>2012</copyright-year>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/afrdevafrdev.37.4.141"/>
         <abstract>
            <label>
               <bold>Abstract</bold>
            </label>
            <p>At independence in the late 1950s and early 1960s, there were high hopes about the growth prospects of the new politically independent African states. Economic conditions, such as per capita real income, were comparable to other developing countries like South Korea and Taiwan. By the mid-1970s, the growth profile of most African countries had started to decline and by the mid-1980s, it became obvious that the African continent needed rescue packages which came in the form of Structural Adjustment Programmes. However, countries like Taiwan and South Korea had made tremendous progress such that their per capita real incomes had grown more than tenfold while those of most African countries had declined considerably compared to the immediate postindependence era. What role did the growth strategies adopted by African countries play in this tragedy? How do we rethink Africa's growth strategy? What were the lessons learned and which way forward? These and other related issues are what this article sets out to address. The article identifies three distinct growth phases for the economies of Africa and analyzes critically the various models embedded in those phases. Among other things, the article strongly canvasses for the deepening of regional integration, enhancing productivity and competitiveness through investment in technology and education, and the reinventing of African labour markets to promote productivity and good labour relations.</p>
         </abstract>
         <trans-abstract xml:lang="fr">
            <label>
               <bold>Résumé</bold>
            </label>
            <p>Au moment des indépendances de la fin des années 1950 au début des années 1960, le continent plaçait de grands espoirs sur les perspectives de croissance des Etats africains nouvellement indépendants. Les conditions économiques telles que le revenu réel par habitant, étaient comparables à celles d'autres pays en développement comme la Corée du Sud et Taiwan. Dès le milieu des années 1970, le profil de croissance de la plupart des pays africains avait commencé à se dégrader et au milieu des années 1980, il était devenu évident que le continent africain avait besoin de programmes d'aide dont il a effectivement bénéficié sous forme de programmes d'ajustement structurel. Toutefois, des pays comme Taiwan et la Corée du Sud avaient accompli un progrès tel que leurs revenus réels par habitant ont été multipliés tandis que ceux de la plupart des pays africains baissaient sensiblement par rapport aux toutes premières années après les indépendances. Quel rôle ont joué les stratégies de croissance adoptées par les pays africains dans cette tragédie? Comment allons-nous repenser la stratégie de croissance de l'Afrique? Quelles sont les leçons apprises et quelle est la prochaine étape? Cet article se propose donc de s'attaquer à ces questions et à celles qui y sont liées. Cet article identifie trois phases distinctes de croissance pour les économies africaines et procède à une analyse critique des différents modèles inhérents à ces phases. Cet article explore également les voies et moyens de renforcer l'intégration régionale, relever la productivité et la compétition à travers l'investissement dans la technologie et l'éducation et réinventer les marchés de main d’œuvre en Afrique dans le but de promouvoir la productivité et de bonnes relations de travail.</p>
         </trans-abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>en</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="parsed-citations">
         <title>Bibliography</title>
         <ref id="ref1">
            <mixed-citation publication-type="book">
               <person-group person-group-type="editor">
                  <string-name>Adepoju, A.</string-name>
               </person-group>ed.,<year>1993</year>,<source>
                  <italic>The Impact of Structural Adjustment on the Population of Africa</italic>
               </source>,:<publisher-name>United Nations Population Fund</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref2">
            <mixed-citation publication-type="web">
               <collab>African Development Bank (AfDB)</collab>,<year>2002</year>,<article-title>‘Achieving MDGs in Africa - Progress, Prospect and Policy Implications’</article-title>,<uri xmlns:xlink="http://www.w3.org/1999/xlink"
                    xlink:href="http://www.lnweb18.worldbank.org/afr/afr.nsf/dfb6add124112b9c852567cf004c6ef1/aa2093d2b04ddbd885256be30058f71c/&amp;#x00024;FILE/mdg-africa.pdf">http://lnweb18.worldbank.org/afr/afr.nsf/dfb6add124112b9c852567cf004c6ef1/aa2093d2b04ddbd885256be30058f71c/$FILE/mdg-africa.pdf</uri>
            </mixed-citation>
         </ref>
         <ref id="ref3">
            <mixed-citation publication-type="other">
               <collab>African Development Bank, OECD and UNECA</collab>,<year>2009</year>,<source>
                  <italic>African Economic Outlook</italic>
               </source>.</mixed-citation>
         </ref>
         <ref id="ref4">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Ake, C.</string-name>
               </person-group>and<person-group person-group-type="author">
                  <string-name>Armacost, M.</string-name>
               </person-group>,<year>1995</year>,<source>
                  <italic>Democracy and Development in Africa</italic>
               </source>,:<publisher-name>Brookings Institution Press</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref5">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Amin, S.</string-name>
               </person-group>,<year>1976</year>,<source>
                  <italic>Unequal Development</italic>
               </source>,:<publisher-name>Monthly Review Press</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref6">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Amin, S.</string-name>
               </person-group>,<year>1979</year>,<source>
                  <italic>Imperialism and Unequal Development</italic>
               </source>,:<publisher-name>Monthly Review Press</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref7">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Artadi, E.V.</string-name>
               </person-group>and<person-group person-group-type="author">
                  <string-name>Sala-i-Martín, X.</string-name>
               </person-group>,<year>2003</year>,<article-title>The Economic Tragedy of the XXth Century: Growth in Africa</article-title>, Economics Working Papers, 684,<publisher-name>Department of Economics and Business, Universitat Pompeu Fabra</publisher-name>
            </mixed-citation>
         </ref>
         <ref id="ref8">
            <mixed-citation publication-type="other">
               <person-group person-group-type="author">
                  <string-name>Ayittey, G.B.N.</string-name>
               </person-group>,<year>1989</year>,<article-title>‘Why Structural Adjustment Failed in Africa’</article-title>, in<person-group person-group-type="editor">
                  <string-name>D. M. Schydlowsky</string-name>
               </person-group>, ed.,<source>
                  <italic>Structural Adjustment: Retrospect and Prospect</italic>
               </source>.</mixed-citation>
         </ref>
         <ref id="ref9">
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Babatunde, M. A.</string-name>
               </person-group>and<person-group person-group-type="author">
                  <string-name>Busari, T. D.</string-name>
               </person-group>,<year>2009</year>,<article-title>‘Global Economic Slowdown and the African Continent: Rethinking Export-Led Growth’</article-title>,<source>
                  <italic>International Journal of African Studies</italic>
               </source>,<volume>2</volume>,<fpage>47</fpage>-<lpage>73</lpage>.</mixed-citation>
         </ref>
         <ref id="ref10">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Bannock, G.</string-name>
               </person-group>,<person-group person-group-type="author">
                  <string-name>Baxter, R.E.</string-name>
               </person-group>and<person-group person-group-type="author">
                  <string-name>Rees, R.</string-name>
               </person-group>,<year>1984</year>,<source>
                  <italic>The Penguin Dictionary of Economics</italic>
               </source>.:<publisher-name>Penguin</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref11">
            <mixed-citation publication-type="web">
               <person-group person-group-type="author">
                  <string-name>Beri, R.</string-name>
               </person-group>,<year>2009</year>,<article-title>‘India's Economic Engagement in Africa: Partnering Africa’</article-title>,<source>
                  <italic>Institute for Defence Studies and Analyses</italic>
               </source>.<series>Weekly Fellow Seminar Series</series>.<uri xmlns:xlink="http://www.w3.org/1999/xlink"
                    xlink:href="http://www.idsa.in/reports/IndiaEconomicEngagementAfrica060109.htm">http://www.idsa.in/reports/IndiaEconomicEngagementAfrica060109.htm</uri>. Retrieved 17 May 2010.</mixed-citation>
         </ref>
         <ref id="ref12">
            <mixed-citation publication-type="confproc">
               <person-group person-group-type="author">
                  <string-name>Blecker, R.A.</string-name>
               </person-group>,<year>2000</year>,<article-title>‘The Diminishing Returns to Export-Led Growth’</article-title>,<conf-name>Paper prepared for the Council of Foreign Relations Working Group on Development</conf-name>,<conf-loc>New York</conf-loc>,</mixed-citation>
         </ref>
         <ref id="ref13">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Booth, D.</string-name>
               </person-group>,<year>2001</year>,<article-title>‘PRSP Processes in 8 African Countries—Initial Impacts and Potential for Institutionalization’</article-title>,:<publisher-name>Overseas Development Institute</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref14">
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Booth, D.</string-name>
               </person-group>,<year>2003</year>,<article-title>‘Are PRSPs making a difference? The African experience’</article-title>,<source>
                  <italic>Development Policy Review</italic>
               </source>,<volume>21</volume>(<issue>2</issue>),<fpage>131</fpage>-<lpage>287</lpage>.</mixed-citation>
         </ref>
         <ref id="ref15">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Brown, M.B.</string-name>
               </person-group>and<person-group person-group-type="author">
                  <string-name>Tiffen, P.</string-name>
               </person-group>,<year>1992</year>,<source>
                  <italic>ShortChanged: Africa and World Trade</italic>
               </source>,,<publisher-name>Pluto Press</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref16">
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Bruton, M.</string-name>
               </person-group>,<year>1998</year>,<article-title>‘A Reconsideration of import substitution’</article-title>,<source>
                  <italic>Journal of Economic Literature</italic>
               </source>.<publisher-name>American Economic Association</publisher-name>,<volume>36</volume>(<issue>2</issue>),<fpage>903</fpage>-<lpage>936</lpage>.</mixed-citation>
         </ref>
         <ref id="ref17">
            <mixed-citation publication-type="confproc">
               <person-group person-group-type="author">
                  <string-name>Cling, J-P</string-name>
               </person-group>,<person-group person-group-type="author">
                  <string-name>Razafrindrakoto, M.</string-name>
               </person-group>and<person-group person-group-type="author">
                  <string-name>Rouband, F.</string-name>
               </person-group>,<year>2002</year>,<article-title>‘New Poverty Reduction Strategies. Old Wine in New Bottles. Towards Pro-Poor Policies. Aids Institutions and Globalization’</article-title>,<conf-name>Annual World Bank Conference on Development Economics</conf-name>.</mixed-citation>
         </ref>
         <ref id="ref18">
            <mixed-citation publication-type="other">
               <collab>Commonwealth Secretariat</collab>.<year>2003</year>,<article-title>PRSP Learning Event</article-title>,<day>9</day>
               <month>July</month>.</mixed-citation>
         </ref>
         <ref id="ref19">
            <mixed-citation publication-type="web">
               <person-group person-group-type="author">
                  <string-name>Dembele, D.M</string-name>
               </person-group>,<year>2003</year>,<article-title>‘The Myths and Dangers of PRSPs’</article-title>, Bretton Woods Project<day>8</day>
               <month>September</month>,<uri xmlns:xlink="http://www.w3.org/1999/xlink"
                    xlink:href="http://www.globalpolicy.org/component/content/article/177/31539.html">http://www.globalpolicy.org/component/content/article/177/31539.html</uri>
            </mixed-citation>
         </ref>
         <ref id="ref20">
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Dijkstra, A. G.</string-name>
               </person-group>,<year>1996</year>,<article-title>‘The impact of structural adjustment programs on manufacturing: Lessons from Nicaragua’</article-title>
               <source>
                  <italic>World Development</italic>
               </source>,<volume>24</volume>(<issue>3</issue>),<fpage>535</fpage>-<lpage>547</lpage>.</mixed-citation>
         </ref>
         <ref id="ref21">
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Domar, E.D.</string-name>
               </person-group>,<year>1946</year>,<article-title>‘Capital Expansion, rate of Growth and Employment’</article-title>.,<source>
                  <italic>Econometrica</italic>
               </source>,<volume>14</volume>,<fpage>137</fpage>-<lpage>147</lpage>.</mixed-citation>
         </ref>
         <ref id="ref22">
            <mixed-citation publication-type="book">
               <collab>Donors Working Group on Public Sector Reform</collab>,<year>2002</year>,<article-title>‘Improving service delivery through Public Service Reform: Lessons from Experience from Select Sub-Saharan Countries’</article-title>,<publisher-name>OECD DAC</publisher-name>,.</mixed-citation>
         </ref>
         <ref id="ref23">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Easterly, W.</string-name>
               </person-group>,<year>2007</year>,<article-title>‘Are the Millennium Development Goals Unfair to Africa?’</article-title>
               <publisher-name>The Brookings Institution</publisher-name>,<uri xmlns:xlink="http://www.w3.org/1999/xlink"
                    xlink:href="http://www.brookings.edu/papers/2007/11_poverty_easterly.aspx">http://www.brookings.edu/papers/2007/11_poverty_easterly.aspx</uri>
            </mixed-citation>
         </ref>
         <ref id="ref24">
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Easterly, W.</string-name>
               </person-group>and<person-group person-group-type="author">
                  <string-name>Levine, R.</string-name>
               </person-group>,<year>1997</year>,<article-title>‘Africa's Growth Tragedy: Policies and Ethnic Divisions’</article-title>,<source>
                  <italic>Quarterly Journal of Economics</italic>
               </source>, Vol.<volume>112</volume>, Issue<issue>4</issue>,<month>November</month>.</mixed-citation>
         </ref>
         <ref id="ref25">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Gelb, A.</string-name>
               </person-group>,<year>2000</year>,<source>
                  <italic>Can Africa Claim the 21st Century?</italic>
               </source>
               <publisher-name>The World Bank</publisher-name>,.</mixed-citation>
         </ref>
         <ref id="ref26">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Grindle, M.</string-name>
               </person-group>,<year>2003</year>,<source>
                  <italic>Good enough governance: Poverty reduction and reform in developing countries</italic>
               </source>,<publisher-name>Harvard University</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref27">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Hammouda, B.H</string-name>
               </person-group>,<year>2004</year>,<article-title>‘Trade liberalization and development: lessons for Africa. Africa Trade Policy Centre Work in Progress’</article-title>,<publisher-name>Economic Commission for Africa</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref28">
            <mixed-citation publication-type="web">
               <person-group person-group-type="author">
                  <string-name>Hanson, S.</string-name>
               </person-group>,<year>2008</year>,<article-title>‘China, Africa, and Oil’</article-title>,<source>Council on Foreign Relations</source>
               <uri xmlns:xlink="http://www.w3.org/1999/xlink"
                    xlink:href="http://www.cfr.org/publication/9557/">http://www.cfr.org/publication/9557/</uri>. Retrieved 25 September 2009.</mixed-citation>
         </ref>
         <ref id="ref29">
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Harrod, R.</string-name>
               </person-group>,<year>1939</year>,<article-title>‘An Essay in Dynamic Theory’</article-title>,<source>
                  <italic>Economic Journal</italic>
               </source>, Vol.<volume>49</volume>,<fpage>14</fpage>-<lpage>33</lpage>.</mixed-citation>
         </ref>
         <ref id="ref30">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Hicks, R.</string-name>
               </person-group>and<person-group person-group-type="author">
                  <string-name>Brekk, O. P.</string-name>
               </person-group>,<year>1991</year>,<article-title>‘Assessing the Impact of Structural Adjustment on the Poor: The Case of Malawi’</article-title>,<publisher-name>IMF Working Paper No. 91/112</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref31">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Hussain, I.</string-name>
               </person-group>,<year>1994</year>,<article-title>‘Structural Adjustment and Long-term Development in Sub-Saharan Africa</article-title>, in<person-group person-group-type="editor">
                  <string-name>van de Hooven, R.</string-name>
               </person-group>and<person-group person-group-type="editor">
                  <string-name>van de Kraaj, F.</string-name>
               </person-group>,eds,<source>
                  <italic>Structural Adjustment and Beyond in Sub-Saharan Africa</italic>
               </source>,:<publisher-name>James Currey</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref32">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Irmen, A.</string-name>
               </person-group>and<person-group person-group-type="author">
                  <string-name>Wigger, B.U.</string-name>
               </person-group>,<year>2000</year>,<article-title>‘Trade Union Objectives and Economic Growth’</article-title>,<publisher-name>Department of Economics, University of Mannheim</publisher-name>,.</mixed-citation>
         </ref>
         <ref id="ref33">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Jadhav, N.</string-name>
               </person-group>,<year>2002</year>,<article-title>‘Synopses of External Comments and Contributions to the Joint IMF/World Bank Staff Review of the PRSP Approach’</article-title>,:<publisher-name>IMF/World Bank</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref34">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Lewis, W. A.</string-name>
               </person-group>,<year>1955</year>,<source>
                  <italic>The Theory of Economic Growth</italic>
               </source>.:<publisher-name>Irwin</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref35">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Malaluan, J.C.</string-name>
               </person-group>and<person-group person-group-type="author">
                  <string-name>Guttal, S.</string-name>
               </person-group>,<year>2002</year>,<source>
                  <italic>Structural Adjustment in the Name of the Poor: The PRSP Experience in the Lao PDR, Cambodia and Vietnam</italic>
               </source>,:<publisher-name>Focus on the Global South</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref36">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Mayer, J.</string-name>
               </person-group>,<year>1996</year>,<article-title>‘Implications of new trade and endogenous growth theories for diversification policies of commodity-dependent countries’</article-title>,<publisher-name>UNCTAD</publisher-name>Discussion Paper No 122.</mixed-citation>
         </ref>
         <ref id="ref37">
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Muscatelli, V.A.</string-name>
               </person-group>,<person-group person-group-type="author">
                  <string-name>Stevenson, A.A.</string-name>
               </person-group>, and<person-group person-group-type="author">
                  <string-name>Montagna, C.</string-name>
               </person-group>,<year>1994</year>,<article-title>‘Intra-NIE Competition in Exports of Manufactures’</article-title>,<source>
                  <italic>Journal of International Economics</italic>
               </source>,<volume>37</volume>,<fpage>29</fpage>-<lpage>47</lpage>.</mixed-citation>
         </ref>
         <ref id="ref38">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Nurkse, R.</string-name>
               </person-group>,<year>1959</year>,<source>
                  <italic>Patterns of Trade and Development</italic>
               </source>,<publisher-name>Wicksell Lectures</publisher-name>,.</mixed-citation>
         </ref>
         <ref id="ref39">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Onimode, B.</string-name>
               </person-group>,<year>2006</year>,<source>
                  <italic>Africa and the Globalised Economy of the 21<sup>st</sup>Century</italic>
               </source>.:<publisher-name>Zed Books</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref40">
            <mixed-citation publication-type="journal">
               <person-group person-group-type="editor">
                  <string-name>Oyejide, T.A.</string-name>
               </person-group>,<person-group person-group-type="editor">
                  <string-name>Ndulu, B</string-name>
               </person-group>and<person-group person-group-type="editor">
                  <string-name>Gunning, J.W.</string-name>
               </person-group>eds,<year>1999</year>,<article-title>‘Introduction and overview’</article-title>,<source>
                  <italic>Regional Integration and Trade Liberalization in Sub-saharan Africa</italic>
               </source>, Vol.<volume>2</volume>,<publisher-name>Country Case Studies</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref41">
            <mixed-citation publication-type="web">
               <person-group person-group-type="author">
                  <string-name>Oyejide, T.A.</string-name>
               </person-group>,<year>2004</year>,<article-title>‘Trade liberalization, regional integration, and African development in the context of structural adjustment’</article-title>, Retrieved 10 January 2005,<italic>
                  <uri xmlns:xlink="http://www.w3.org/1999/xlink"
                       xlink:href="http://www.idrc.ca/en/ev-56333-201-1-DO_TOPIC.html">http://www.idrc.ca/en/ev-56333-201-1-DO_TOPIC.html</uri>
               </italic>
            </mixed-citation>
         </ref>
         <ref id="ref42">
            <mixed-citation publication-type="confproc">
               <person-group person-group-type="author">
                  <string-name>Palley, T.</string-name>
               </person-group>,<year>2002</year>,<article-title>‘Domestic Demand-Led Growth: A New Paradigm For Development’</article-title>,<conf-name>Paper presented at the Alternatives to Neoliberalism Conference sponsored by the New Rules for Global Finance Coalition</conf-name>,<conf-date>23–24 May</conf-date>
               <uri xmlns:xlink="http://www.w3.org/1999/xlink"
                    xlink:href="http://www.economicswebinstitute.org/essays/domesticpulledgrowth.pdf">http://www.economicswebinstitute.org/essays/domesticpulledgrowth.pdf</uri>, Accessed 2 June 2009.</mixed-citation>
         </ref>
         <ref id="ref43">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Palley, T.I.</string-name>
               </person-group>,<year>2001</year>,<article-title>‘Is There a Relationship Between the Quality of Governance and Financial Crises? Evidence from the Crises of 1997’</article-title>,<publisher-name>AFL-CIO Public Policy Department</publisher-name>, Unpublished Paper.</mixed-citation>
         </ref>
         <ref id="ref44">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Prebisch, R.</string-name>
               </person-group>,<year>1950</year>,<source>
                  <italic>The Economic Development of Latin America and its Principle Problem</italic>
               </source>,:<publisher-name>UNECLA</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref45">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Prebisch, R.</string-name>
               </person-group>,<year>1954</year>,<article-title>‘Five Stages in My Thinking about Development’</article-title>, in<person-group person-group-type="editor">
                  <string-name>P. Bauer</string-name>
               </person-group>,<person-group person-group-type="editor">
                  <string-name>G. Meier</string-name>
               </person-group>, and<person-group person-group-type="editor">
                  <string-name>D. Seers</string-name>
               </person-group>, eds,<source>
                  <italic>Pioneers in Development</italic>
               </source>,:<publisher-name>Oxford University Press</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref46">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Sala-i-Martin, X.</string-name>
               </person-group>and<person-group person-group-type="author">
                  <string-name>Subramanian, A.</string-name>
               </person-group>,<year>2003</year>,<article-title>‘Addressing the Natural Resource Curse: Nigeria’</article-title>,,<publisher-name>Columbia University</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref47">
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Sarkar, P.</string-name>
               </person-group>, and<person-group person-group-type="author">
                  <string-name>Singer, H.</string-name>
               </person-group>,<year>1991</year>,<article-title>‘Manufactured Exports of Developing Countries and their Terms of Trade since 1965’</article-title>,<source>
                  <italic>World Development</italic>
               </source>,<volume>19</volume>,<fpage>333</fpage>-<lpage>40</lpage>.</mixed-citation>
         </ref>
         <ref id="ref48">
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Singer, H.</string-name>
               </person-group>,<year>1950</year>,<article-title>‘The Distribution of Gains Between Investing and Borrowing Countries’</article-title>,<source>
                  <italic>American Economic Review</italic>
               </source>,<publisher-name>Papers and Proceedings</publisher-name>,<volume>40</volume>,<fpage>473</fpage>-<lpage>85</lpage>.</mixed-citation>
         </ref>
         <ref id="ref49">
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Solow, R.</string-name>
               </person-group>,<year>1956</year>,<article-title>‘A Contribution to the Theory of Economic Growth’</article-title>,<source>
                  <italic>Quarterly Journal of Economics</italic>
               </source>, Vol.<volume>70</volume>,<fpage>65</fpage>-<lpage>94</lpage>.</mixed-citation>
         </ref>
         <ref id="ref50">
            <mixed-citation publication-type="confproc">
               <collab>UNCTAD</collab>,<year>2002</year>,<article-title>‘Economic Development in Africa: From Adjustment to Poverty Reduction:What is New?’</article-title>
               <conf-name>United Nations Conference on Trade and Development</conf-name>,<conf-loc>Geneva</conf-loc>.</mixed-citation>
         </ref>
         <ref id="ref51">
            <mixed-citation publication-type="confproc">
               <collab>UNCTAD</collab>,<year>2008</year>,<source>
                  <italic>Handbook of Statistics</italic>
               </source>,<conf-name>United Nations Conference on Trade and Development</conf-name>,<conf-loc>Geneva</conf-loc>.</mixed-citation>
         </ref>
         <ref id="ref52">
            <mixed-citation publication-type="book">
               <collab>UNDP</collab>,<year>2001</year>,<article-title>‘UNDP Review of the Poverty Reduction Strategy Paper (PRSP) ’</article-title>,:<publisher-name>UNDP</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref53">
            <mixed-citation publication-type="journal">
               <collab>UNDP</collab>,<year>2003</year>,<article-title>‘Evaluation of UNDP's Role in the PRSP Process’</article-title>, Volume<volume>1</volume>, Main Report,:<publisher-name>UNDP Evaluation Office</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref54">
            <mixed-citation publication-type="confproc">
               <collab>UNECA/UNESCO</collab>,<year>2002</year>,<article-title>‘Re-thinking the brain drain in Africa in light of recent events and findings’</article-title>,<conf-name>Report prepared for the Conference of Ministers of Education of African Member States</conf-name>,<conf-loc>Dar es Salaam</conf-loc>.</mixed-citation>
         </ref>
         <ref id="ref55">
            <mixed-citation publication-type="book">
               <collab>World Bank</collab>,<year>2004</year>,<article-title>‘The Poverty Reduction Strategy Initiative: An Independent Evaluation of the World Bank's Support Through 2003’</article-title>,:<publisher-name>World Bank Operations Evaluation Department</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref56">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Zuckerman, E.</string-name>
               </person-group>and<person-group person-group-type="author">
                  <string-name>Garrett, A.</string-name>
               </person-group>
               <year>2003</year>,<article-title>‘Do Poverty Reduction Strategy Papers (PRSPs) address Gender? A Gender Audit of 2002 PRSPs’</article-title>,:<publisher-name>Gender Action Publications</publisher-name>.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
