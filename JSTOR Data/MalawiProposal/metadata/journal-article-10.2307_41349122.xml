<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">revieconstat</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100341</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>The Review of Economics and Statistics</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>MIT Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00346535</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15309142</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">41349122</article-id>
         <title-group>
            <article-title>INTERNATIONAL CONVERGENCE OF COPYRIGHT PRODUCTION</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jin-Hyuk</given-names>
                  <surname>Kim</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>11</month>
            <year>2011</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">93</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40064284</issue-id>
         <fpage>1432</fpage>
         <lpage>1439</lpage>
         <permissions>
            <copyright-statement>©2011 THE PRESIDENT AND FELLOWS OF HARVARD COLLEGE</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:href="http://dx.doi.org/10.1162/REST_a_00144"
                   xlink:title="an external site"/>
         <abstract>
            <p>The production of copyrighted materials varies widely across countries, and how it evolves over time has important policy implications. I propose a simple dynamic model of copyrights and the public domain, which predicts conditional convergence of per capita copyright production among countries. Using book and film production data for the period 1975 to 1995, I test and confirm the model's prediction that copyright-poor countries tend to grow faster than copyright-rich countries in terms of per capita copyright production.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d817e189a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d817e196" publication-type="other">
(TRIPS, 1994)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d817e202" publication-type="other">
World Intellectual Property Organization Copyright Treaty (1996)</mixed-citation>
            </p>
         </fn>
         <fn id="d817e209a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d817e216" publication-type="other">
Peitz and Waelbroeck (2006)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d817e222" publication-type="other">
Towse, Handke, and
Stepan (2008)</mixed-citation>
            </p>
         </fn>
         <fn id="d817e232a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d817e239" publication-type="other">
David and Rubin (2008).</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>REFERENCES</title>
         <ref id="d817e255a1310">
            <mixed-citation id="d817e259" publication-type="other">
Barro, Robert, "Economic Growth in a Cross Section of Countries,"
Quarterly Journal of Economics 106 (1991), 407-443.</mixed-citation>
         </ref>
         <ref id="d817e269a1310">
            <mixed-citation id="d817e273" publication-type="other">
Barro, Robert, and Jong-Wha Lee, "International Data on Educational
Attainment: Updates and Implications," Oxford Economic Papers
53 (2001), 541-563.</mixed-citation>
         </ref>
         <ref id="d817e286a1310">
            <mixed-citation id="d817e290" publication-type="other">
Baumol, William, "Productivity Growth, Convergence, and Welfare: What
the Long-Run Data Show," American Economic Review 76 (1986),
1072-1085.</mixed-citation>
         </ref>
         <ref id="d817e303a1310">
            <mixed-citation id="d817e307" publication-type="other">
Boldrin, Michele, and David Levine, "Market Size and Intellectual Property
Protection," International Economic Review 50 (2009), 855-881.</mixed-citation>
         </ref>
         <ref id="d817e318a1310">
            <mixed-citation id="d817e322" publication-type="other">
David, Paul, and Jared Rubin, "Restricting Access to Books on the Internet:
Some Unanticipated Effects of U.S. Copyright Legislation," Review
of Economic Research on Copyright Issues 5 (2008), 23-53.</mixed-citation>
         </ref>
         <ref id="d817e335a1310">
            <mixed-citation id="d817e339" publication-type="other">
Hui, Kai-Lung, and Ivan Png, "On the Supply of Creative Work: Evi-
dence from the Movies," American Economic Review Papers and
Proceedings 92 (2002), 217-220.</mixed-citation>
         </ref>
         <ref id="d817e352a1310">
            <mixed-citation id="d817e356" publication-type="other">
Khan, Zorina, "Does Copyright Piracy Pay? The Effects of U.S. Interna-
tional Copyright Laws on the Market for Books, 1790-1920," NBER
working paper no. 10271 (2004).</mixed-citation>
         </ref>
         <ref id="d817e369a1310">
            <mixed-citation id="d817e373" publication-type="other">
Landes, William, and Richard Posner, "An Economic Analysis of Copyright
Law," Journal of Legal Studies 18 (1989), 325-363.</mixed-citation>
         </ref>
         <ref id="d817e383a1310">
            <mixed-citation id="d817e387" publication-type="other">
--- The Economic Structure of Intellectual Property Law (Cambridge,
MA: Belknap Press of Harvard University Press, 2003).</mixed-citation>
         </ref>
         <ref id="d817e397a1310">
            <mixed-citation id="d817e401" publication-type="other">
Liebowitz, Stanley, and Stephen Margolis, "Seventeen Famous Economists
Weigh in on Copyright: The Role of Theory, Empirics, and Network
Effects," Harvard Journal of Law and Technology 18 (2005), 435-
457.</mixed-citation>
         </ref>
         <ref id="d817e418a1310">
            <mixed-citation id="d817e422" publication-type="other">
Peitz, Martin, and Patrick Waelbroeck, "Piracy of Digital Products: A Crit-
ical Review of the Theoretical Literature," Information Economics
and Policy 18 (2006), 449-476.</mixed-citation>
         </ref>
         <ref id="d817e435a1310">
            <mixed-citation id="d817e439" publication-type="other">
Png, Ivan, and Qiu-hong Wang, "Copyright Duration and the Supply of
Creative Work: Evidence from the Movies," National University of
Singapore working paper (2006).</mixed-citation>
         </ref>
         <ref id="d817e452a1310">
            <mixed-citation id="d817e456" publication-type="other">
Scotchmer, Suzanne, "The Political Economy of Intellectual Property
Treaties," Journal of Law, Economics, and Organization 20 (2004),
415-437.</mixed-citation>
         </ref>
         <ref id="d817e469a1310">
            <mixed-citation id="d817e473" publication-type="other">
Towse, Ruth, Christian Handke, and Paul Stepan, "The Economics of Copy-
right Law: A Stocktake of the Literature," Review of Economic
Research on Copyright Issues 5 (2008), 1-22.</mixed-citation>
         </ref>
         <ref id="d817e486a1310">
            <mixed-citation id="d817e490" publication-type="other">
Whitney, Gretchen, "International Book Production Statistics," in Philip
Altbach and Edith Hoshino (Eds.), International Book Publishing:
An Encyclopedia (New York: Garland, 1995).</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
