<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt6wpdfs</book-id>
      <subj-group>
         <subject content-type="call-number">HV6322.7.M374 2010</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Genocide</subject>
         <subj-group>
            <subject content-type="lcsh">Prevention</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Crimes against humanity</subject>
         <subj-group>
            <subject content-type="lcsh">Prevention</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>Mass Atrocity Crimes</book-title>
         <subtitle>Preventing Future Outrages</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="editor" id="contrib1">
            <name name-style="western">
               <surname>Rotberg</surname>
               <given-names>Robert I.</given-names>
            </name>
            <role>Editor</role>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>01</day>
         <month>09</month>
         <year>2010</year>
      </pub-date>
      <isbn content-type="epub">9780815704867</isbn>
      <isbn content-type="epub">0815704860</isbn>
      <publisher>
         <publisher-name>Brookings Institution Press, World Peace Foundation, and the Harvard Kennedy School Program on Intrastate Conflict</publisher-name>
         <publisher-loc>Washington, D.C.</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2010</copyright-year>
         <copyright-holder>WORLD PEACE FOUNDATION</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7864/j.ctt6wpdfs"/>
      <abstract abstract-type="short">
         <p>What can be done to combat genocide, ethnic cleansing, and other crimes against humanity? Why aren't current measures more effective? Is there hope for the future? These and other pressing questions surrounding human security are addressed head-on in this provocative and all-too-timely book.</p>
         <p>Millions of people, particularly in Africa, face daily the prospect of death at the hands of state or state-linked forces. Although officially both the United Nations and the African Union have adopted "Responsibility to Protect" (R2P) principles, atrocities continue. The tenets of R2P, recently cited in a UN Outcomes Document, make it clear that states have a primary responsibility to protect their citizens from genocide, war crimes, ethnic cleansing, and crimes against humanity. When states cannot -or will not -protect their citizens, however, the international community must step into the breach.</p>
         <p>Why have efforts to stop horrific state-sanctioned crimes seen only limited success, despite widespread support of R2P? As this enlightening volume explains and illustrates, converting a norm into effective preventive measures remains difficult. The contributors examine the legal framework to inhibit war crimes, use of the emerging R2P norm, the role of the International Criminal Court, and new technologically sophisticated methods to gather early warnings of likely atrocity outbreaks. Together they show how mass atrocities may be anticipated, how they may be prevented, and when necessary, how they may be prosecuted.</p>
         <p>Contributors include Claire Applegarth (Harvard Kennedy School), Andrew Block (Harvard Kennedy School), Frank Chalk (Montreal Institute for Genocide and Human Rights Studies, Concordia University), David M. Crane (Syracuse University College of Law), Richard J. Goldstone (Constitutional Court of South Africa; UN International Criminal Tribunals for the former Yugoslavia and Rwanda), Don Hubert (University of Ottawa; Global Center for the Responsibility to Protect, City University of New York), Sarah Kreps (Cornell University), Dan Kuwali (Malawi Defence Force), Jennifer Leaning (Harvard Francois Xavier Bagnoud Center for Health and Human Rights), Edward C. Luck (Columbia University; International Peace Institute), Sarah Sewall (Harvard Kennedy School)</p>
      </abstract>
      <counts>
         <page-count count="255"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpdfs.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpdfs.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpdfs.3</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Rotberg</surname>
                           <given-names>Robert I.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpdfs.4</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Deterring Mass Atrocity Crimes:</title>
                     <subtitle>The Cause of Our Era</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>ROTBERG</surname>
                           <given-names>ROBERT I.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>Nation-states persist in killing their own citizens. In 2010, Congolese in their millions were still facing death in the cross-fire of continuing civil warfare between the national army and diverse rebel militias, from starvation and disease, and because of violence against women and children. Zimbabwe’s ruling Zimbabwe African National Union–Patriotic Front (ZANU-PF) Party and the military and police apparatus of the state were continuing to attack, maim, and kill, in hundreds, members of the Movement for Democratic Change (MDC), their supposed partners in a year-old joint government. The strong arm of the Sudanese state continued to foster inter-ethnic violence</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpdfs.5</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>Old Crimes, New Paradigms:</title>
                     <subtitle>Preventing Mass Atrocity Crimes</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>KUWALI</surname>
                           <given-names>DAN</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>25</fpage>
                  <abstract>
                     <p>The thresholds for intervention under the responsibility to protect (R2P) norm, as endorsed in the 2005 World Summit Outcome Document, are serious human rights violations in the form of genocide, war crimes, crimes against humanity, and ethnic cleansing. The same thresholds apply for intervention under Article 4(h) of the Constitutive Act of the African Union (AU Act). However, acts that shock the conscience and elicit a basic humanitarian impulse remain politically elusive. As these thresholds have no precise contours, it is not clear what mechanisms are to be used to determine the preconditions for intervention. For example, the mass atrocity</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpdfs.6</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>The Role of the International Criminal Court</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>GOLDSTONE</surname>
                           <given-names>RICHARD J.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>55</fpage>
                  <abstract>
                     <p>Other chapters in this collection consider the nature, identification, and prevention of the commission of crimes against humanity. This chapter considers the prosecution of persons alleged to have committed crimes against humanity. Thus, it deals with events that, by their nature, come after such egregious crimes have been committed.</p>
                     <p>As in the case of all prosecutions, for what are increasingly being called “atrocity crimes,” there are substantial benefits. I discuss six of them, namely:</p>
                     <p>Bringing an end to impunity for war criminals;</p>
                     <p>Providing justice to the victims;</p>
                     <p>Ending fabricated denials;</p>
                     <p>Deterring potential criminals;</p>
                     <p>Advancing international humanitarian law; and</p>
                     <p>Increasing the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpdfs.7</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Understanding Crimes against Humanity in West Africa:</title>
                     <subtitle>Giving the People What They Want</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>CRANE</surname>
                           <given-names>DAVID M.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>69</fpage>
                  <abstract>
                     <p>In March 1991, the tragedy of the civil war in Sierra Leone began with an invasion from Liberia into the eastern diamond fields of the country. The invading forces consisted of various units and criminal elements from across West Africa, including the Revolutionary United Front (RUF), led by Foday Sankoh; Guineans; Burkinabe; Liberians; and Special Forces from Libya.</p>
                     <p>For more than ten years, a joint criminal enterprise, led by then President Charles Taylor on behalf of Muammar Qaddafi, murdered, raped, maimed, and mutilated close to 1.2 million human beings to further its own criminal purposes, trading diamonds for guns and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpdfs.8</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>The Responsibility to Protect:</title>
                     <subtitle>Preventing and Halting Crimes against Humanity</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>HUBERT</surname>
                           <given-names>DON</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>89</fpage>
                  <abstract>
                     <p>At the September 2005 United Nations (UN) World Summit in New York, the world’s leaders formally accepted an international responsibility to protect civilian populations from genocide, war crimes, ethnic cleansing, and crimes against humanity. Widely hailed as one of the few successes of the World Summit, the commitment was, in many ways, a restatement of “never again,” the phrase frequently invoked in the wake of genocide. The “responsibility to protect” is rapidly becoming part of the international lexicon, but what was actually agreed upon at the summit and will it ever have an impact?</p>
                     <p>In this chapter, I trace the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpdfs.9</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Building a Norm:</title>
                     <subtitle>The Responsibility to Protect Experience</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>LUCK</surname>
                           <given-names>EDWARD C.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>108</fpage>
                  <abstract>
                     <p>The responsibility to protect (R2P) is a big idea, no doubt about it.¹ But it is also an evolving and still contentious one, despite pledges that the heads of state and government made at the 2005 World Summit. They affirmed, unanimously, that they would protect their populations by preventing genocide, war crimes, ethnic cleansing, and crimes against humanity, as well as the incitement of such acts. Further, they agreed that the international community should assist and support states in exercising that responsibility and in building their domestic protection capacities. When national authorities are nevertheless “manifestly failing” to protect their populations</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpdfs.10</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Acting against Atrocities:</title>
                     <subtitle>A Strategy for Supporters of R2P</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>APPLEGARTH</surname>
                           <given-names>CLAIRE</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>BLOCK</surname>
                           <given-names>ANDREW</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>128</fpage>
                  <abstract>
                     <p>The advent of the “responsibility to protect” (R2P) was thought to signal the international community’s commitment to ending genocide, war crimes, ethnic cleansing, and crimes against humanity once and for all. The emerging norm embodies the belief that all nations should prevent, react to, and assist in recovery from mass atrocity crimes wherever they may occur and by whomever they may be committed. Adopted by consensus at the United Nations’ (UN) momentous World Summit in 2005, R2P served as a firm declaration that state sovereignty would no longer provide a shield behind which perpetrators of mass atrocities could hide, and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpdfs.11</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>From Prevention to Response:</title>
                     <subtitle>Using Military Force to Oppose Mass Atrocities</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>SEWALL</surname>
                           <given-names>SARAH</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>159</fpage>
                  <abstract>
                     <p>Prevention is a compelling concept. As it pertains to the subject of this volume, the prevention of mass atrocities or crimes against humanity, it appears to be an unalloyed good. Individuals, states, non-governmental organizations (NGOs), and international and regional institutions that advocate against genocide and in support of the “Responsibility to Protect” (R2P) increasingly cast their arguments and craft their solutions as contributing to prevention. This approach presents as sensible and prudent, non-violent and economical. Focusing on prevention allows humanitarian advocates to skirt key controversies and political pitfalls and garners widespread support.</p>
                     <p>This chapter, however, questions the seductiveness of prevention</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpdfs.12</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>Social Networks and Technology in the Prevention of Crimes against Humanity</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>KREPS</surname>
                           <given-names>SARAH E.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>175</fpage>
                  <abstract>
                     <p>In a 2004 speech, then United Nations (UN) Secretary-General Kofi Annan lamented the “conspicuous gaps in our capacity to give early warning of genocide or comparable crimes, and to analyze or manage the information that we do receive.”¹ Several years later, the early warning gap remains, but less conspicuously so. Improvements in technology have given the illusion that crimes against humanity might now be anticipated and prevented, but, in fact, most of these technologies—mobile phones, satellites, and unmanned aerial vehicles—are still better suited for identification and documentation than for early warning and prevention.</p>
                     <p>This chapter takes stock of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpdfs.13</book-part-id>
                  <title-group>
                     <label>10</label>
                     <title>The Use of Patterns in Crisis Mapping to Combat Mass Atrocity Crimes</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>LEANING</surname>
                           <given-names>JENNIFER</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>192</fpage>
                  <abstract>
                     <p>For most of this decade a terrible conflict has played out in Darfur, yet the international community still has not managed to acquire a reliable estimate of how many people have been killed and under what circumstances. The government of the Sudan successfully blocked all usual means and methods of gathering population-based data on war-related morbidity and mortality in the region. Yet this decade has also seen an explosion in capacities and creative uses of technology to amass information from a variety of indirect sources. The potential now exists for mass atrocity crimes to be identified and tracked through the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpdfs.14</book-part-id>
                  <title-group>
                     <label>11</label>
                     <title>Monitoring African Governments’ Domestic Media to Predict and Prevent Mass Atrocities:</title>
                     <subtitle>Opportunities and Obstacles</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>CHALK</surname>
                           <given-names>FRANK</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>220</fpage>
                  <abstract>
                     <p>Let me begin by posing an axiom: “Monitoring media can predict mass atrocities.” While the axiom will not hold in every case, my experience confirms the usefulness of media monitoring for early warning. Governments and their agents usually organize mass atrocities, and one need not be a systems analyst to recognize that even dictators try to enlist the sympathy of their people with persuasive propaganda in order to commit crimes against humanity.</p>
                     <p>The story of Radio Télévision Libre des Mille Collines (RTLM)’s role in the Rwandan genocide of 1994 is among the best known cases in the sorry history of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpdfs.15</book-part-id>
                  <title-group>
                     <title>Contributors</title>
                  </title-group>
                  <fpage>239</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpdfs.16</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>245</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpdfs.17</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>256</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
