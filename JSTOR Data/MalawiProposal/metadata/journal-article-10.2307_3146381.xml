<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">landeconomics</journal-id>
         <journal-id journal-id-type="jstor">j100261</journal-id>
         <journal-title-group>
            <journal-title>Land Economics</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Wisconsin Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">00237639</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="doi">10.2307/3146381</article-id>
         <title-group>
            <article-title>Managing Tropical Forests: Reflections on the Rent Distribution Discussion</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>William F.</given-names>
                  <surname>Hyde</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Roger A.</given-names>
                  <surname>Sedjo</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>8</month>
            <year>1992</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">68</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id>i357979</issue-id>
         <fpage>343</fpage>
         <lpage>350</lpage>
         <page-range>343-350</page-range>
         <permissions>
            <copyright-statement>Copyright 1992 Board of Regents of the University of Wisconsin System</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/3146381"/>
         <abstract>
            <p>Commercial forest extraction is one component of the tropical deforestation issue. Discussions about commercial extraction focus on choices among alternate rents and royalties. These discussions often confuse measures of short- and long-run efficiency and equate distributive issues with efficiency. They also overlook (1) potential lump sum charges, (2) administrative costs, and (3) previous public forestry agency experience. This paper develops a simple analytical construct for clarifying these issues and examines each in turn.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d43e150a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d43e157" publication-type="book">
Sedjo and Clawson (1984)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d43e165" publication-type="journal">
Pe-
ters, Gentry, and Mendelsohn (1989)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d43e176" publication-type="journal">
Sedjo (1992)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d43e185" publication-type="book">
Mahar (1988)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d43e193" publication-type="book">
Binswanger (1989)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d43e201" publication-type="journal">
Weimer (1990)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d43e209" publication-type="journal">
Ehui and Hertel (1989)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d43e217" publication-type="book">
Southgate (1990a  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d43e225" publication-type="journal">
1990b)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d43e234" publication-type="book">
Brazee and Southgate (1991)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d43e242" publication-type="journal">
Hyde, Mendelsohn, and Sedjo
(1991)  </mixed-citation>
            </p>
         </fn>
         <fn id="d43e254a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d43e261" publication-type="book">
Hyde (1980, ch. 2)  </mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d43e279a1310">
            <mixed-citation id="d43e283" publication-type="book">
Binswanger, Hans. 1989. "Brazilian Policies
that Encourage Deforestation in the Ama-
zon." Environment Department working pa-
per 16, The World Bank.<person-group>
                  <string-name>
                     <surname>Binswanger</surname>
                  </string-name>
               </person-group>
               <source>Brazilian Policies that Encourage Deforestation in the Amazon</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d43e315a1310">
            <mixed-citation id="d43e319" publication-type="book">
Boyd, Roy, and William F. Hyde. 1989. For-
estry Sector Intervention. Ames: Iowa State
University Press.<person-group>
                  <string-name>
                     <surname>Boyd</surname>
                  </string-name>
               </person-group>
               <source>Forestry Sector Intervention</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d43e348a1310">
            <mixed-citation id="d43e352" publication-type="book">
Brazee, Richard J., and Douglas Southgate.
1991. "Development of Ethno-Biologically
Diverse Tropical Forests." Draft manuscript,
Department of Agricultural Economics, Ohio
State University.<person-group>
                  <string-name>
                     <surname>Brazee</surname>
                  </string-name>
               </person-group>
               <source>Development of Ethno-Biologically Diverse Tropical Forests</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d43e387a1310">
            <mixed-citation id="d43e391" publication-type="book">
Cardellichio, P., Y. Youn, C. Binkley, J. Vin-
cent, and D. Adams. 1988. "An Economic
Analysis of Short-Run Timber Supply
Around the Globe." CINTAFOR working
paper no. 18, College of Forest Resources,
University of Washington.<person-group>
                  <string-name>
                     <surname>Cardellichio</surname>
                  </string-name>
               </person-group>
               <source>An Economic Analysis of Short-Run Timber Supply Around the Globe</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d43e430a1310">
            <mixed-citation id="d43e434" publication-type="journal">
Ehui, S., and Thomas Hertel. 1989. "Deforesta-
tion and Agricultural Productivity in the Cote
d'Ivoire." American Journal of Agricultural
Economics 71 (Aug.):703-11.<object-id pub-id-type="doi">10.2307/1242026</object-id>
               <fpage>703</fpage>
            </mixed-citation>
         </ref>
         <ref id="d43e457a1310">
            <mixed-citation id="d43e461" publication-type="book">
Gillis, Malcomb. 1980. "Fiscal and Financial Is-
sues in Tropical Hardwood Concessions."
Discussion paper no. 110, Harvard Institute
for International Development.<person-group>
                  <string-name>
                     <surname>Gillis</surname>
                  </string-name>
               </person-group>
               <source>Fiscal and Financial Issues in Tropical Hardwood Concessions</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
         <ref id="d43e493a1310">
            <mixed-citation id="d43e499" publication-type="book">
---. 1988. "Indonesia: Public Policies, Re-
source Management and the Tropical For-
est." In Public Policy and the Misuse of For-
est Resources, eds. R. Repetto and M. Gillis.
Cambridge: Cambridge University Press.<person-group>
                  <string-name>
                     <surname>Gillis</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Indonesia: Public Policies, Resource Management and the Tropical Forest</comment>
               <source>Public Policy and the Misuse of Forest Resources</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d43e537a1310">
            <mixed-citation id="d43e541" publication-type="book">
Gray, John. 1983. "Forest Revenue Systems in
Developing Countries." FAO/UN forestry
paper 43.<person-group>
                  <string-name>
                     <surname>Gray</surname>
                  </string-name>
               </person-group>
               <source>Forest Revenue Systems in Developing Countries</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d43e570a1310">
            <mixed-citation id="d43e574" publication-type="other">
Grut, Michael, N. Egli, and John Gray. 1990.
"Forest Revenue Systems and Concession
Management Policies for West and Central
African Moist Forests." Unpublished manu-
script, The World Bank.</mixed-citation>
         </ref>
         <ref id="d43e593a1310">
            <mixed-citation id="d43e597" publication-type="book">
Hyde, William F. 1980. Timber Supply, Land
Allocation and Economic Efficiency. Balti-
more: Johns Hopkins University Press for
Resources for the Future.<person-group>
                  <string-name>
                     <surname>Hyde</surname>
                  </string-name>
               </person-group>
               <source>Timber Supply, Land Allocation and Economic Efficiency</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
         <ref id="d43e630a1310">
            <mixed-citation id="d43e634" publication-type="journal">
Hyde, W., R. Mendelsohn, and R. Sedjo. 1991.
"The Applied Economics of Tropical Defor-
estation." Association of Environmental and
Resource Economists Newsletter 11 (May):
6-12.<person-group>
                  <string-name>
                     <surname>Hyde</surname>
                  </string-name>
               </person-group>
               <issue>May</issue>
               <fpage>6</fpage>
               <volume>11</volume>
               <source>Association of Environmental and Resource Economists Newsletter</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d43e678a1310">
            <mixed-citation id="d43e682" publication-type="journal">
Krueger, Anne. 1990. "Government Failures in
Development." Journal of Economic Per-
spectives 9 (Spring):9-23.<object-id pub-id-type="jstor">10.2307/1942926</object-id>
               <fpage>9</fpage>
            </mixed-citation>
         </ref>
         <ref id="d43e701a1310">
            <mixed-citation id="d43e705" publication-type="book">
Mahar, Dennis. 1988. "Government Policies
and Deforestation in Brazil's Amazon Re-
gion." Environment Department working pa-
per no. 7, The World Bank.<person-group>
                  <string-name>
                     <surname>Mahar</surname>
                  </string-name>
               </person-group>
               <source>Government Policies and Deforestation in Brazil's Amazon Region</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d43e737a1310">
            <mixed-citation id="d43e741" publication-type="journal">
Peters, Charles, Alwyn Gentry, and Robert
Mendelsohn. 1989. "Valuation of an Amazo-
nian Rainforest." Nature 339:655-56.<person-group>
                  <string-name>
                     <surname>Peters</surname>
                  </string-name>
               </person-group>
               <fpage>655</fpage>
               <volume>339</volume>
               <source>Nature</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d43e776a1310">
            <mixed-citation id="d43e780" publication-type="book">
Repetto, Robert. 1988a. "Overview." In Public
Policies and the Misuse of Forest Resources,
eds. R. Repetto and M. Gillis. Cambridge:
Cambridge University Press.<person-group>
                  <string-name>
                     <surname>Repetto</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Overview</comment>
               <source>Public Policies and the Misuse of Forest Resources</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d43e815a1310">
            <mixed-citation id="d43e821" publication-type="book">
---. 1988b. "Subsidized Timber Sales from
National Forest Lands in the U.S." In Public
Policies and the Misuse of Forest Resources,
eds. R. Repetto and M. Gillis. Cambridge:
Cambridge University Press.<person-group>
                  <string-name>
                     <surname>Repetto</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Subsidized Timber Sales from National Forest Lands in the U.S.</comment>
               <source>Public Policies and the Misuse of Forest Resources</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d43e860a1310">
            <mixed-citation id="d43e864" publication-type="journal">
Rucker, Randal, and Keith B. Leffler. 1991.
"Transaction Costs and the Efficient Organi-
zation of Production: A Study of Timber-
Harvesting Contracts." Journal of Political
Economy 99 (Oct.):1060-87.<object-id pub-id-type="jstor">10.2307/2937658</object-id>
               <fpage>1060</fpage>
            </mixed-citation>
         </ref>
         <ref id="d43e890a1310">
            <mixed-citation id="d43e894" publication-type="journal">
Sedjo, Roger. 1992. "Can Tropical Forest Man-
agement be Economic?" Journal of Business
Administration (forthcoming).<person-group>
                  <string-name>
                     <surname>Sedjo</surname>
                  </string-name>
               </person-group>
               <source>Journal of Business Administration</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d43e923a1310">
            <mixed-citation id="d43e927" publication-type="book">
Sedjo, Roger, and Marion Clawson. 1984.
"Global Forests." In The Resourceful Earth,
eds. Julien Simon and Herman Kahn. Ox-
ford: Basil Blackwell.<person-group>
                  <string-name>
                     <surname>Sedjo</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Global Forests</comment>
               <source>The Resourceful Earth</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d43e962a1310">
            <mixed-citation id="d43e966" publication-type="book">
Southgate, Douglas. 1990a. "Tropical Defores-
tation and Agricultural Development in Latin
America." Environment Department work-
ing paper no. 1991-20, The World Bank.<person-group>
                  <string-name>
                     <surname>Southgate</surname>
                  </string-name>
               </person-group>
               <source>Tropical Deforestation and Agricultural Development in Latin America</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d43e998a1310">
            <mixed-citation id="d43e1002" publication-type="journal">
---. 1990b. "The Causes of Land Degrada-
tion along 'Spontaneously' Expanding Ag-
ricultural Frontiers in the Third World."
Land Economics 66 (Feb.):93-101.<object-id pub-id-type="doi">10.2307/3146686</object-id>
               <fpage>93</fpage>
            </mixed-citation>
         </ref>
         <ref id="d43e1025a1310">
            <mixed-citation id="d43e1029" publication-type="journal">
Srinivasan, T. 1985. "Neoclassical Political
Economy, the State and Economic Develop-
ment." Asian Development Review (Aug.):
38-58.<person-group>
                  <string-name>
                     <surname>Srinivasan</surname>
                  </string-name>
               </person-group>
               <issue>Aug.</issue>
               <fpage>38</fpage>
               <source>Asian Development Review</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d43e1068a1310">
            <mixed-citation id="d43e1072" publication-type="journal">
Vincent, Jeffrey. 1990. "Rent Capture and the
Feasibility of Tropical Forest Management."
Land Economics 66 (May):212-23.<object-id pub-id-type="doi">10.2307/3146370</object-id>
               <fpage>212</fpage>
            </mixed-citation>
         </ref>
         <ref id="d43e1091a1310">
            <mixed-citation id="d43e1095" publication-type="journal">
Weimer, D. 1990. "An Earmarked Fossil Fuels
Tax to Save the Rainforest." Journal of Pol-
icy Analysis and Management 9 (Spring):
249-54.<person-group>
                  <string-name>
                     <surname>Weimer</surname>
                  </string-name>
               </person-group>
               <issue>Spring</issue>
               <fpage>249</fpage>
               <volume>9</volume>
               <source>Journal of Policy Analysis and Management</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d43e1136a1310">
            <mixed-citation id="d43e1140" publication-type="book">
World Bank. 1983. "Accelerated Development
in Sub-Saharan Africa." Washington, DC.<person-group>
                  <string-name>
                     <surname>World Bank</surname>
                  </string-name>
               </person-group>
               <source>Accelerated Development in Sub-Saharan Africa</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
