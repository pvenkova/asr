<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">amernatu</journal-id>
         <journal-id journal-id-type="jstor">j100074</journal-id>
         <journal-title-group>
            <journal-title>The American Naturalist</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Chicago Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">00030147</issn>
         <issn pub-type="epub">15375323</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">2461172</article-id>
         <title-group>
            <article-title>Allomeric Variation. 2. Developmental Instability of Extreme Phenotypes</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Michael E.</given-names>
                  <surname>Soule</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Janine</given-names>
                  <surname>Cuzin-Roudy</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>12</month>
            <year>1982</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">120</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">6</issue>
         <issue-id>i320713</issue-id>
         <fpage>765</fpage>
         <lpage>786</lpage>
         <page-range>765-786</page-range>
         <permissions>
            <copyright-statement>Copyright 1982 The University of Chicago</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/2461172"/>
         <abstract>
            <p>A new pattern in the variation of morphological characters is described in insects, birds, and mammals. This pattern, called the IG-OG effect for convenience, depends for its detection on distinguishing among three operational levels of variation: (1) within-individual variation in paired or homologous, segmental structures (asymmetry of bilateral organs or traits is employed here); (2) within-population variation of a trait, usually estimated by the standard deviation or coefficient of variation (CV); (3) differences in variability (CV) between different traits. The IG-OG effect (increased asymmetry in extreme phenotypes) is apparently most strongly expressed in the least variable traits (level 3), those with CVs of 2.5 or less. When the frequency distribution of such a character (level 2) is divided into three parts, the two tails (out-groups, OG) and the central part (in-group, IG), and the amount of within-individual (asymmetry) variation (level 1) is compared among them, it was found that OG individuals were more asymmetrical than IG individuals. This is the same as discovering a correlation between asymmetry and absolute distance from the mean. The IG-OG effect, including its negative correlation with the coefficient of variation, was predicted from a general theory of morphometric variation. The theory, allomeric variation (Soule 1982), assumes that, within a set of homologous or nested characters, the contribution of developmental noise (random molecular motion and interactions) to the variability of characters decreases as the size of the characters increases. As a consequence, the additive genetic contribution to the phenotype should be greatest in those traits that are the largest or have the most parts. In turn, the correlation between the genotypic homozygosity and phenotypic distance from the mean should be most pronounced in the least variable traits, except in traits, such as skull or wing measurements, encompassing two or more elements, in which integration, compensatory effects or other extrinsic processes may obscure the expression of the intrinsic genotype. If, finally, it is assumed that developmental stability is correlated with the degree of genotypic heterozygosity, then it follows that OG individuals will be less stable than IG individuals. An alternative explanation for the IG-OG effect, based on the concept of balance (coadaptation) is considered. Available information is insufficient to weigh the relative importance of these hypotheses.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
</article>
