<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">philtranmathphys</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100915</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Philosophical Transactions: Mathematical, Physical and Engineering Sciences</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>The Royal Society</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">1364503X</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">41739926</article-id>
         <title-group>
            <article-title>Oceans of opportunity or rough seas? What does the future hold for developments in European marine policy?</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Tavis</given-names>
                  <surname>Potts</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Tim</given-names>
                  <surname>O'Higgins</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Emily</given-names>
                  <surname>Hastings</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>13</day>
            <month>12</month>
            <year>2012</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">370</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1980</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40081537</issue-id>
         <fpage>5682</fpage>
         <lpage>5700</lpage>
         <permissions>
            <copyright-statement>COPYRIGHT © 2012 The Royal Society</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/41739926"/>
         <abstract>
            <p>The management of European seas is undergoing a process of major reform. In the past, oceans and coastal policy has traditionally evolved in a fragmented and uncoordinated manner, developed by different sector-based agencies and arms of government with competing aims and objectives. Recently, the call for integrated and ecosystem-based approaches has driven the conceptualization of a new approach. At the scale of Europe through the Integrated Maritime Policy and Marine Strategy Framework Directive and in national jurisdictions such as the Marine and Coastal Access Act in the United Kingdom, ecosystem-based planning is becoming the norm. There are major challenges to this process and this paper explores, in particular, the opportunities inherent in building truly integrated approaches that cross different sectors of activity, integrate across scales, incorporate public involvement and build a sense of oceans citizenship.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d1230e190a1310">
            <label>1</label>
            <mixed-citation id="d1230e197" publication-type="other">
International Ocean Institute. 2012 The IOI story: International Ocean Institute. See
http://www.ioinst.org/.</mixed-citation>
         </ref>
         <ref id="d1230e207a1310">
            <label>2</label>
            <mixed-citation id="d1230e214" publication-type="other">
United Nations Convention of the Law off the Sea (UNCLOS). 1982 Section XI Article 136. Full
treaty text. See http://www.un.org/depts/los/convention_agreements/texts/unclos/closindx.
htm.</mixed-citation>
         </ref>
         <ref id="d1230e227a1310">
            <label>3</label>
            <mixed-citation id="d1230e234" publication-type="other">
Borgese, E. M. &amp; Krieger, D. (eds) 1975 Tides of change: peace, pollution and potential of the
oceans. New York, NY: Mason/Charter.</mixed-citation>
         </ref>
         <ref id="d1230e244a1310">
            <label>4</label>
            <mixed-citation id="d1230e251" publication-type="other">
Pierre, J. &amp; Peters, B. G. 2005 Governing complex societies: trajectories and scenarios.
Basingstoke, UK: Palgrave Macmillan.</mixed-citation>
         </ref>
         <ref id="d1230e262a1310">
            <label>5</label>
            <mixed-citation id="d1230e269" publication-type="other">
Turner, R. K. 2000 Integrating natural and socio-economic science in coastal management.
J. Mar. Systems 25, 447-460. (doi:10.1016/S0924-7963(00)00033-6)</mixed-citation>
         </ref>
         <ref id="d1230e279a1310">
            <label>6</label>
            <mixed-citation id="d1230e286" publication-type="other">
Olsen, S., Tobey, J. &amp; Kerr, M. 1997 A common framework for learning from ICM experience.
Ocean Coast. Manage. 37, 155-174. (doi:10.1016/S0964-5691(97)90105-8)</mixed-citation>
         </ref>
         <ref id="d1230e296a1310">
            <label>7</label>
            <mixed-citation id="d1230e303" publication-type="other">
Boesch, D. F. 1999 The role of science in ocean management. Ecol. Econ. 31, 189-198.
(doi:10.1016/S0921-8009(99)00078-6)</mixed-citation>
         </ref>
         <ref id="d1230e313a1310">
            <label>8</label>
            <mixed-citation id="d1230e320" publication-type="other">
Haward, M. &amp; Vince, J. 2008 Oceans governance in the twenty-first century: managing the blue
planet, pp. 21-25. Cheltenham, UK: Edward Elgar.</mixed-citation>
         </ref>
         <ref id="d1230e330a1310">
            <label>9</label>
            <mixed-citation id="d1230e337" publication-type="other">
Agardy, T. 2010 Ocean zoning: making marine management more effective. London, UK:
Earthscan.</mixed-citation>
         </ref>
         <ref id="d1230e347a1310">
            <label>10</label>
            <mixed-citation id="d1230e354" publication-type="other">
OSPAR and NEAFC. 2008 Memorandum of Understanding. See http://www.ospar.org/
html_documents/ospar/html/mou_neafc_ospar.pdf.</mixed-citation>
         </ref>
         <ref id="d1230e365a1310">
            <label>11</label>
            <mixed-citation id="d1230e372" publication-type="other">
Costanza, R. et al 1998 Principles for sustainable governance of the oceans. Science 281, 198-
199. (doi:10.1126/science.281.5374.198)</mixed-citation>
         </ref>
         <ref id="d1230e382a1310">
            <label>12</label>
            <mixed-citation id="d1230e389" publication-type="other">
Millennium Ecosystem Assessment. 2005 Millennium Ecosystem Assessment full report. See
http: //www.millenniumassessment.org/en/index.aspx.</mixed-citation>
         </ref>
         <ref id="d1230e399a1310">
            <label>13</label>
            <mixed-citation id="d1230e406" publication-type="other">
Garcia, S. M., Zerbi, A., Aliaume, C., Do Chi, T. &amp;; Lasserre, G. 2003 The ecosystem approach to
fisheries. Issues, terminology, principles, institutional foundations, implementation and outlook.
Fisheries technical paper no. 443, FAO, Rome, Italy.</mixed-citation>
         </ref>
         <ref id="d1230e419a1310">
            <label>14</label>
            <mixed-citation id="d1230e426" publication-type="other">
O'Higgins, T. &amp; Roth, E. 2011 Integrating the common fisheries policy and the marine strategy
for the Baltic. Discussion of spatial and temporal scales in the management and adaptation
to climate change. In Climate change and Baltic coasts (eds G. Schernewski, J. Hofstede &amp;
T. Neumann), pp. 275-294. Berlin, Germany: Springer.</mixed-citation>
         </ref>
         <ref id="d1230e442a1310">
            <label>15</label>
            <mixed-citation id="d1230e449" publication-type="other">
Farmer, A., Mee, L., Langmead, O., Cooper, P., Kannen, A., Kershaw, P. &amp; Cherrier, V.
2012 The ecosystem approach in marine management. Research report, EU FP7 Knowledge-
based Sustainable Management for Europe's Seas (KnowSeas). See http://www.knowseas.com/
links-and-data/policy-briefs.</mixed-citation>
         </ref>
         <ref id="d1230e465a1310">
            <label>16</label>
            <mixed-citation id="d1230e472" publication-type="other">
Tallis, H., Levin, S. P., Ruckelshaus, M., Lester, S. E., McLeod, K. L. Fluharty, D. D. L. &amp;
Halpern, B. S. 2010 The many faces of ecosystem-based management: making the process work
in real places. Mar. Policy 34, 340-348. (doi:10.1016/j.marpol.2009.08.003)</mixed-citation>
         </ref>
         <ref id="d1230e486a1310">
            <label>17</label>
            <mixed-citation id="d1230e493" publication-type="other">
Convention on Biological Diversity. 2007 Principles for the ecosystem approach. See
http: //www.cbd.int/ecosystem/principles.shtml.</mixed-citation>
         </ref>
         <ref id="d1230e503a1310">
            <label>18</label>
            <mixed-citation id="d1230e510" publication-type="other">
Commission of the European Communities. 2007 Communication from the Commission
to the European Parliament, the Council, the European Economic and Social Committee
and the Committee of the Regions: an Integrated Maritime Policy for the European
Union, Brussels, 10.10.2007 COM(2007) 575. See http://eur-lex.europa.eu/LexUriServ/
LexUriServ.do?uri=COM:2007:0575:FIN:EN:PDF.</mixed-citation>
         </ref>
         <ref id="d1230e529a1310">
            <label>19</label>
            <mixed-citation id="d1230e536" publication-type="other">
European Parliament and the Council. 2000 Establishing a framework for Community action
in the field of water policy (the Water Framework Directive) Directive 2000/60/EC. See
http://ec.europa.eu/environment/water/water-framework/index_en.html.</mixed-citation>
         </ref>
         <ref id="d1230e549a1310">
            <label>20</label>
            <mixed-citation id="d1230e556" publication-type="other">
Turner, R. K., Hadley, D., Luisetti, T., Lam, V. &amp; Cheung, W. 2010 An introduction to socio-
economic assessment within a marine strategy framework. London, UK: Defra.</mixed-citation>
         </ref>
         <ref id="d1230e566a1310">
            <label>21</label>
            <mixed-citation id="d1230e573" publication-type="other">
Pendleton, L., Atiyah, P. &amp; Moorthy, A. 2007 Is the non-market literature adequate
to support coastal and marine management? Ocean Coast. Manage. 50, 363-378.
(doi:10.1016/j.ocecoaman.2006.11.004)</mixed-citation>
         </ref>
         <ref id="d1230e586a1310">
            <label>22</label>
            <mixed-citation id="d1230e593" publication-type="other">
Potts, T., O'Higgins, T., Mee, L. &amp; Pita, C. 2011 Public perceptions of Europe's seas—a policy
brief. Research report, EU FP7 Knowledge-based Sustainable Management for Europe's Seas
(KnowSeas). See http://www.knowseas.com/links-and-data/policy-briefs.</mixed-citation>
         </ref>
         <ref id="d1230e607a1310">
            <label>23</label>
            <mixed-citation id="d1230e614" publication-type="other">
Jentoft, S. &amp; Chuenpagdee, R. 2009 Fisheries and coastal governance as a wicked problem.
Mar. Policy 33, 553-560. (doi:10.1016/j.marpol.2008.12.002)</mixed-citation>
         </ref>
         <ref id="d1230e624a1310">
            <label>24</label>
            <mixed-citation id="d1230e631" publication-type="other">
Helsinki Commission. 2010 Ecosystem health of the Baltic Sea 2003-2007: HELCOM initial
holistic assessment. Baltic Sea Environmental Proceedings, no. 122.</mixed-citation>
         </ref>
         <ref id="d1230e641a1310">
            <label>25</label>
            <mixed-citation id="d1230e648" publication-type="other">
Rätz, H. J., Dörner, H., Scott, R. &amp; Barbas, T. 2010 Complementary roles of European and
national institutions under the Common Fisheries Policy and the Marine Strategy Framework
Directive. Mar. Policy 34. 1028-1035. (doi:10.1016/i.marDol.2010.03.001)</mixed-citation>
         </ref>
         <ref id="d1230e661a1310">
            <label>26</label>
            <mixed-citation id="d1230e668" publication-type="other">
BSC. 2009 Strategic action plan for the environmental protection and rehabilitation of the Black
Sea (adopted in Sofia, Bulgaria, 17 April 2009). See http://www.blacksea-commission.org/_
bssap2009.asp.</mixed-citation>
         </ref>
         <ref id="d1230e681a1310">
            <label>27</label>
            <mixed-citation id="d1230e688" publication-type="other">
EU Commission. 2001 Communication from the Commission. Environmental cooperation in
the Danube-Black Sea Region (COM/2001/0615). See http://eur-lex.europa.eu/LexUriServ/
LexUriServ.do?uri=CELEX:52001DC0615:EN:HTML.</mixed-citation>
         </ref>
         <ref id="d1230e701a1310">
            <label>28</label>
            <mixed-citation id="d1230e708" publication-type="other">
Suarez de Vivero, J. L. &amp; Maetos, J. C. R. 2006 Maritime Europe and EU enlargement. A
geopolitical perspective. Mar. Policy 30, 167-172. (doi:10.1016/j.marpol.2004.11.002)</mixed-citation>
         </ref>
         <ref id="d1230e719a1310">
            <label>29</label>
            <mixed-citation id="d1230e726" publication-type="other">
UK Government and Devolved Administrations. 2009. Our seas : a shared resource. High
level marine objectives. London, UK: Defra. See http://archive.defra.gov.uk/environment/
marine/documents/ourseas-2009update.pdf.</mixed-citation>
         </ref>
         <ref id="d1230e739a1310">
            <label>30</label>
            <mixed-citation id="d1230e746" publication-type="other">
Department for Environment, Food and Rural Affairs. 2009 Marine and Coastal Access Act.
See http: //www.defra.gov.uk/environment /marine/mca/.</mixed-citation>
         </ref>
         <ref id="d1230e756a1310">
            <label>31</label>
            <mixed-citation id="d1230e763" publication-type="other">
Department for Environment, Food and Rural Affairs. 2002 Safeguarding our seas: a
strategy for the conservation and sustainable development of our marine environment. See
http://ec.euroDa.eu/environment/iczmdownloads/uk2002.Ddf.</mixed-citation>
         </ref>
         <ref id="d1230e776a1310">
            <label>32</label>
            <mixed-citation id="d1230e783" publication-type="other">
HM Government, Northern Ireland Executive, Scottish Government, Welsh Assembly
Government. 2011 UK Marine Policy Statement. London, UK: The Stationery Office.</mixed-citation>
         </ref>
         <ref id="d1230e793a1310">
            <label>33</label>
            <mixed-citation id="d1230e800" publication-type="other">
Jones, P. J. S. 2006 The Marine Bill: cornucopia or Pandora's Box? ECOS: Rev. Conserv. 27,
1-6.</mixed-citation>
         </ref>
         <ref id="d1230e810a1310">
            <label>34</label>
            <mixed-citation id="d1230e817" publication-type="other">
Appleby, T. &amp; Jones, P. J. S. 2012 The marine and coastal access act—a hornets nest? Mar.
Policy 36, 73-77. (doi:10.1016/j.marpol.2011.03.009)</mixed-citation>
         </ref>
         <ref id="d1230e828a1310">
            <label>35</label>
            <mixed-citation id="d1230e835" publication-type="other">
MMO. 2011 East Inshore and East Offshore marine plan areas. Statement of
public participation. See http://www.marinemanagement.org.uk/marineplanning/documents/
final spp.pdf.</mixed-citation>
         </ref>
         <ref id="d1230e848a1310">
            <label>36</label>
            <mixed-citation id="d1230e855" publication-type="other">
Fleming, D. M . &amp; Jones, P. J. S. 2012 Challenges to achieving greater and fairer stakeholder
involvement in marine spatial planning as illustrated by the Lyme Bay scallop dredging closure.
Mar. Policy 36, 370-377. (doi: 10. 1016/j.marpol.2011.07.006)</mixed-citation>
         </ref>
         <ref id="d1230e868a1310">
            <label>37</label>
            <mixed-citation id="d1230e875" publication-type="other">
Stevens, T. F. 2006 Independent scoping study: options for spatial management of scallop
dredging impacts on hard substrates in Lyme Bay. Report for the South West Inshore Scallopers
Association, Marine Institute, University of Plymouth, UK.</mixed-citation>
         </ref>
         <ref id="d1230e888a1310">
            <label>38</label>
            <mixed-citation id="d1230e895" publication-type="other">
Suarez de Vivero, J. L., Rodriguez Mateos, J. C. &amp; Florido del Corral, D. 2008 The paradox
of public participation in fisheries governance: the rising number of actors and the devolution
process. Mar. Policy 32, 319-325. (doi:10.1016/j.marpol.2007.06.005)</mixed-citation>
         </ref>
         <ref id="d1230e908a1310">
            <label>39</label>
            <mixed-citation id="d1230e915" publication-type="other">
Baxter, J. M., Boyd, I. L., Cox, M., Donald, A. E., Malcolm, S. J., Miles, H., Miller, B. &amp; Moffat,
C. F. 2011 Scotland's marine atlas: information for the national marine plan. Edinburgh, UK:
Marine Scotland.</mixed-citation>
         </ref>
         <ref id="d1230e928a1310">
            <label>40</label>
            <mixed-citation id="d1230e935" publication-type="other">
Scottish Government. 2010 Marine (Scotland) Act (2010). See http://www.legislation.gov.uk/
asp/2010/5/pdfs/asp_20100005_en.pdf.</mixed-citation>
         </ref>
         <ref id="d1230e946a1310">
            <label>41</label>
            <mixed-citation id="d1230e953" publication-type="other">
Pomeroy, R. &amp; Douvere, F. 2008 The engagement of stakeholders in the marine spatial planning
process. Mar. Policy 32, 816-822. (doi:10.1016/j.marpol.2008.03.017)</mixed-citation>
         </ref>
         <ref id="d1230e963a1310">
            <label>42</label>
            <mixed-citation id="d1230e970" publication-type="other">
Pound, D. 2008 Better management of shared natural resources: the need for good
practice stakeholder participation—a practitioner's perspective. Proc. 12th Biennial IASC
Conf, Cheltenham, UK, H~18 July 2008. See http://iasc2008.glos.ac.uk/conference%
20papers/papers/P/Pound 124601 pdf.</mixed-citation>
         </ref>
         <ref id="d1230e986a1310">
            <label>43</label>
            <mixed-citation id="d1230e993" publication-type="other">
McKinley, E. &amp; Fletcher, S. 2012 Improving marine environmental health through marine
citizenship: a call for debate. Mar. Policy 36, 839-843. (doi:10.1016/j.marpol.2011.11.001)</mixed-citation>
         </ref>
         <ref id="d1230e1003a1310">
            <label>44</label>
            <mixed-citation id="d1230e1010" publication-type="other">
McKinley, E. &amp; Fletcher, S. 2010 Individual responsibility for the oceans? An evaluation
of marine citizenship by UK marine practitioners. Ocean Coast. Manag. 53, 379-384.
(doi:10.1016/j.ocecoaman.2010.04.012)</mixed-citation>
         </ref>
         <ref id="d1230e1023a1310">
            <label>45</label>
            <mixed-citation id="d1230e1030" publication-type="other">
Williams, L. 2008 Public attitudes towards the maritime environment around the Menai Strait
and Conwy Bay, CCW policy research report no. 08/16, Countryside Council for Wales, UK.</mixed-citation>
         </ref>
         <ref id="d1230e1040a1310">
            <label>46</label>
            <mixed-citation id="d1230e1047" publication-type="other">
Mee, L. D., Jefferson, R. L., Laffoley, D. D. &amp; Elliott, M. 2008 How good is good?
Human values and Europe's proposed marine strategy directive. Mar. Poll. Bull. 56, 187-204.
(doi:10.1016/j.marpolbul.2007.09.038)</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
