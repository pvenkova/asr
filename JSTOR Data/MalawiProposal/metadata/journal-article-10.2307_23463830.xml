<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">evolution</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100004</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Evolution</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Wiley Subscription Services, Inc.</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00143820</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15585646</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">23463830</article-id>
         <title-group>
            <article-title>ALTITUDINAL CLINAL VARIATION IN WING SIZE AND SHAPE IN AFRICAN DROSOPHILA MELANOGASTER: ONE CLINE OR MANY?</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>William</given-names>
                  <surname>Pitchers</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>John E.</given-names>
                  <surname>Pool</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Ian</given-names>
                  <surname>Dworkin</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>2</month>
            <year>2013</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">67</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i23463700</issue-id>
         <fpage>438</fpage>
         <lpage>452</lpage>
         <permissions>
            <copyright-statement>Copyright © 2013 Society for the Study of Evolution</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:href="http://dx.doi.org/10.1111/j.1558-5646.2012.01774.x"
                   xlink:title="an external site"/>
         <abstract>
            <p>Geographical patterns of morphological variation have been useful in addressing hypotheses about environmental adaptation. In particular, latitudinal clines in phenotypes have been studied in a number of Drosophila species. Some environmental conditions along latitudinal clines—for example, temperature—also vary along altitudinal clines, but these have been studied infrequently and it remains unclear whether these environmental factors are similar enough for convergence or parallel evolution. Most clinal studies in Drosophila have dealt exclusively with univariate phenotypes, allowing for the detection of clinal relationships, but not for estimating the directions of covariation between them. We measured variation in wing shape and size in D. melanogaster derived from populations at varying altitudes and latitudes across sub-Saharan Africa. Geometric morphometrics allows us to compare shape changes associated with latitude and altitude, and manipulating rearing temperature allows us to quantify the extent to which thermal plasticity recapitulates clinal effects. Comparing effect vectors demonstrates that altitude, latitude, and temperature are only partly associated, and that the altitudinal shape effect may differ between Eastern and Western Africa. Our results suggest that selection responsible for these phenotypic clines may be more complex than just thermal adaptation.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>LITERATURE CITED</title>
         <ref id="d1024e238a1310">
            <mixed-citation id="d1024e242" publication-type="other">
Adams, D., F. Rohlf, and D. Slice. 2004. Geometric morphometries: ten years
of progress following the "revolution." Ital. J. Zool. 71(1):5–16.</mixed-citation>
         </ref>
         <ref id="d1024e252a1310">
            <mixed-citation id="d1024e256" publication-type="other">
Azevedo, R., V. French, and L. Partridge. 1996. Thermal evolution of egg size
in Drosophila melanogaster. Evolution 50(6):2338–2345.</mixed-citation>
         </ref>
         <ref id="d1024e266a1310">
            <mixed-citation id="d1024e270" publication-type="other">
Bitner-Mathe, B„ and L. Klaczko. 1999. Size and shape heritability in natu-
ral populations of Drosophila mediopunctata: temporal and microgeo-
graphical variation. Genetica 105(1):35–42.</mixed-citation>
         </ref>
         <ref id="d1024e283a1310">
            <mixed-citation id="d1024e287" publication-type="other">
Breuker, C. J., J. S. Patterson, and C. P. Klingenberg. 2006. A single basis for
developmental buffering of Drosophila wing shape. PLoS One l(l):e7.
doi: 10.1371 /journal.pone.0000007</mixed-citation>
         </ref>
         <ref id="d1024e301a1310">
            <mixed-citation id="d1024e305" publication-type="other">
Bridle, J. R., S. Gavaz, and W. J. Kennington. 2009. Testing limits to adaptation
along altitudinal gradients in rainforest Drosophila. Proc. R. Soc. Lond.
Β 276(1661): 1507–1515.</mixed-citation>
         </ref>
         <ref id="d1024e318a1310">
            <mixed-citation id="d1024e322" publication-type="other">
Bubliy, O., and V. Loeschcke. 2000. High stressful temperature and genetic
variation of five quantitative traits in Drosophila melanogaster. Genetica
110(l):79–85.</mixed-citation>
         </ref>
         <ref id="d1024e335a1310">
            <mixed-citation id="d1024e339" publication-type="other">
—. 2002. Effect of low stressful temperature on genetic variation of five
quantitative traits in Drosophila melanogaster. Heredity 89(l):70–75.</mixed-citation>
         </ref>
         <ref id="d1024e349a1310">
            <mixed-citation id="d1024e353" publication-type="other">
—. 2005. Variation of life-history and morphometrical traits in
Drosophila buzzatii and Drosophila simulons collected along an alti-
tudinal gradient from a Canary island. Biol. J. Linn. Soc. 84(1): 119–
136.</mixed-citation>
         </ref>
         <ref id="d1024e369a1310">
            <mixed-citation id="d1024e373" publication-type="other">
Capy, P., E. Pla, and J. David. 1993. Phenotypic and genetic variability of
morphometrical traits in natural populations of Drosophila melanogaster
and D simulans. I. Geographic variations. Genet. Sel. Evol. 25:517–536.</mixed-citation>
         </ref>
         <ref id="d1024e386a1310">
            <mixed-citation id="d1024e390" publication-type="other">
Claude, J. 2008. Morphometries with R. Springer Verlag, New York, USA.</mixed-citation>
         </ref>
         <ref id="d1024e398a1310">
            <mixed-citation id="d1024e402" publication-type="other">
Collinge, J., A. Hoffmann, and S. McKechnie. 2006. Altitudinal patterns
for latitudinally varying traits and polymorphic markers in Drosophila
melanogaster from eastern Australia. J. Evol. Biol. 19(2):473–482.</mixed-citation>
         </ref>
         <ref id="d1024e415a1310">
            <mixed-citation id="d1024e419" publication-type="other">
Dahlgaard, J., E. Hasson, and V. Loeschcke. 2001. Behavioral differentiation
in oviposition activity in Drosophila buzzatii from highland and lowland
populations in Argentina: plasticity or thermal adaptation? Evolution
55(4):738–747.</mixed-citation>
         </ref>
         <ref id="d1024e435a1310">
            <mixed-citation id="d1024e439" publication-type="other">
Débat, V., M. Bégin, H. Legout, and J. R. David. 2003. Allometric and non-
allometric components of Drosophila wing shape respond differently to
developmental temperature. Evolution 57(12):2773–2784.</mixed-citation>
         </ref>
         <ref id="d1024e452a1310">
            <mixed-citation id="d1024e456" publication-type="other">
Débat, V., A. Debelle, and I. Dworkin. 2009. Plasticity, canalization, and de-
velopmental stability of the Drosophila wing: joint effects of mutations
and developmental temperature. Evolution 63(11):2864–2876.</mixed-citation>
         </ref>
         <ref id="d1024e469a1310">
            <mixed-citation id="d1024e473" publication-type="other">
Dillon, M., and M. Frazier. 2006. Drosophila melanogaster locomotion in
cold thin air. J. Exp. Biol. 209(2):364–371.</mixed-citation>
         </ref>
         <ref id="d1024e483a1310">
            <mixed-citation id="d1024e487" publication-type="other">
Dillon, M., M. Frazier, and R. Dudley. 2006. Into thin air: physiology and
evolution of alpine insects. Integr. Comp. Biol. 46(1):49–61.</mixed-citation>
         </ref>
         <ref id="d1024e498a1310">
            <mixed-citation id="d1024e502" publication-type="other">
Drake, A. G., and C. P. Klingenberg. 2008. The pace of morphological change:
historical transformation of skull shape in St Bernard dogs. Proc. R. Soc.
Lond. Β 275(1630):71–76.</mixed-citation>
         </ref>
         <ref id="d1024e515a1310">
            <mixed-citation id="d1024e519" publication-type="other">
Eanes, W. F. 2010. Molecular population genetics and selection in the gly-
colytic pathway. J. Exp. Biol. 214(2): 165–171.</mixed-citation>
         </ref>
         <ref id="d1024e529a1310">
            <mixed-citation id="d1024e533" publication-type="other">
Gibert, P., and R. Huey. 2001. Chill-coma temperature in Drosophila: effects of
developmental temperature, latitude, and phylogeny. Physiol. Biochem.
Zool. 74(3):429–434.</mixed-citation>
         </ref>
         <ref id="d1024e546a1310">
            <mixed-citation id="d1024e550" publication-type="other">
Gidaszewski, Ν. Α., M. Baylac, and C. P. Klingenberg. 2009. Evolution of
sexual dimorphism of wing shape in the Drosophila melanogaster sub-
group. BMC Evol. Biol. 9:110.</mixed-citation>
         </ref>
         <ref id="d1024e563a1310">
            <mixed-citation id="d1024e567" publication-type="other">
Gilchrist, Α., and L. Partridge. 1999. A comparison of the genetic basis of
wing size divergence in three parallel body size clines of Drosophila
melanogaster. Genetics 153(4): 1775–1787.</mixed-citation>
         </ref>
         <ref id="d1024e580a1310">
            <mixed-citation id="d1024e584" publication-type="other">
Gilchrist, G., R. Huey, and L. Serra. 2001. Rapid evolution of wing size clines
in Drosophila subobscura. Genetica 112:273–286.</mixed-citation>
         </ref>
         <ref id="d1024e595a1310">
            <mixed-citation id="d1024e599" publication-type="other">
Hadfield, J. D. 2010. MCMC methods for multi-response generalized linear
mixed models: the MCMCglmm R Package. J. Stat. Softw. 33(2): 1–2.
Available at http://www.jstatsoft.org/v33/i02/.</mixed-citation>
         </ref>
         <ref id="d1024e612a1310">
            <mixed-citation id="d1024e616" publication-type="other">
Hallas, R., M. Schiffer, and A. Hoffmann. 2002. Clinal variation in Drosophila
serrata for stress resistance and body size. Genet. Res. 79(2): 141–148.</mixed-citation>
         </ref>
         <ref id="d1024e626a1310">
            <mixed-citation id="d1024e630" publication-type="other">
Hodkinson, I. D. 2005. Terrestrial insects along elevation gradients: species
and community responses to altitude. Biol. Rev. Camb. Philos. Soc.
80(3):489.</mixed-citation>
         </ref>
         <ref id="d1024e643a1310">
            <mixed-citation id="d1024e647" publication-type="other">
Hoffmann, Α., A. Anderson, and R. Hallas. 2002. Opposing clines for high
and low temperature resistance in Drosophila melanogaster. Ecol. Lett.
5(5):614–618.</mixed-citation>
         </ref>
         <ref id="d1024e660a1310">
            <mixed-citation id="d1024e664" publication-type="other">
Houle, D. et al. 2003. Automated measurement of Drosophila wings. BMC
Evol. Biol. 3(1), 25.</mixed-citation>
         </ref>
         <ref id="d1024e674a1310">
            <mixed-citation id="d1024e678" publication-type="other">
Huey, R.B., G. W. Gilchrist, M. L. Carlson, D. Berrigan, and L. Serra. 2000.
Rapid evolution of a geographic cline in size in an introduced fly. Science
287(5451):308–309.</mixed-citation>
         </ref>
         <ref id="d1024e692a1310">
            <mixed-citation id="d1024e696" publication-type="other">
Imasheva, A. G., and O. A. Bubliy. 2003. Quantitative variation of four mor-
phological traits in Drosophila melanogaster under larval crowding.
Hereditas 138(3): 193–199.</mixed-citation>
         </ref>
         <ref id="d1024e709a1310">
            <mixed-citation id="d1024e713" publication-type="other">
Imasheva, A. G., O. A. Bubli, and Ο. E. Lazebny. 1994. Variation in wing
length in Eurasian natural populations of Drosophila melanogaster.
Heredity 72(Pt 5):508–514.</mixed-citation>
         </ref>
         <ref id="d1024e726a1310">
            <mixed-citation id="d1024e730" publication-type="other">
James, Α., and L. Partridge. 1998. Geographic variation in competitive ability
in Drosophila melanogaster. Am. Nat. 151(6):530–537.</mixed-citation>
         </ref>
         <ref id="d1024e740a1310">
            <mixed-citation id="d1024e744" publication-type="other">
James, A. C., R. B. R. Azevedo, and L. Partridge. 1997. Genetic and envi-
ronmental responses to temperature of Drosophila melanogaster from a
latitudinal cline. Genetics 146(3):881.</mixed-citation>
         </ref>
         <ref id="d1024e757a1310">
            <mixed-citation id="d1024e761" publication-type="other">
Kutner, M. H., C. J. Nachtsheim, J. Neter, and W. Li. 2004. Applied linear
statistical models. McGraw-Hill/lrwin, New York, USA.</mixed-citation>
         </ref>
         <ref id="d1024e771a1310">
            <mixed-citation id="d1024e775" publication-type="other">
Lee, S. F., L. Rako, and A. A. Hoffmann. 2011. Molecular basis of adaptive
shift in body size in Drosophila melanogaster. functional and sequence
analyses of the Dca gene. Mol. Biol. Evol. 28(8):2393–2402.</mixed-citation>
         </ref>
         <ref id="d1024e789a1310">
            <mixed-citation id="d1024e793" publication-type="other">
Lencioni, V. 2004. Survival strategies of freshwater insects in cold environ-
ments. J. Limnol. 63(Suppl. l):45–55.</mixed-citation>
         </ref>
         <ref id="d1024e803a1310">
            <mixed-citation id="d1024e807" publication-type="other">
McKechnie, S. W., M. J. Blacket, S. V. Song, L. Rako, X. Carroll, T. K.
Johnson, L. T. Jensen, S. F. Lee, C. W. Wee, and A. A. Hoffmann. 2010.
A clinally varying promoter polymorphism associated with adaptive
variation in wing size in Drosophila. Mol. Ecol. 19(4):775–784.</mixed-citation>
         </ref>
         <ref id="d1024e823a1310">
            <mixed-citation id="d1024e827" publication-type="other">
Morin, P. B. Moreteau, G. Pétavy, and J. R. David. 1999. Divergence of
reaction norms of size characters between tropical and temperate pop-
ulations of Drosophila melanogaster and D. simulans. J. Evol. Biol.
12(2):329–339.</mixed-citation>
         </ref>
         <ref id="d1024e843a1310">
            <mixed-citation id="d1024e847" publication-type="other">
Norry, F., O. Bubliy, and V. Loeschcke. 2001. Developmental time, body size
and wing loading in Drosophila buzzatii from lowland and highland
populations in Argentina. Hereditas 135(1):35–40.</mixed-citation>
         </ref>
         <ref id="d1024e860a1310">
            <mixed-citation id="d1024e864" publication-type="other">
Paaby, Α. Β., Μ. J. Blacket, A. A. Hoffmann, and P. S. Schmidt. 2010.
Identification of a candidate adaptive polymorphism for Drosophila life
history by parallel independent clines on two continents. Mol. Ecol.
19(4):760–774.</mixed-citation>
         </ref>
         <ref id="d1024e880a1310">
            <mixed-citation id="d1024e884" publication-type="other">
Parkash, R., P. K. Tyagi, I. Sharma, and S. Rajpurohit. 2005. Adaptations
to environmental stress in altitudinal populations of two Drosophila
species. Physiol. Entomol. 30(4):353–361.</mixed-citation>
         </ref>
         <ref id="d1024e898a1310">
            <mixed-citation id="d1024e902" publication-type="other">
Pool, J. E.. and C. F. Aquadro. 2006. History and structure of sub-Saharan
populations of Drosophila melanogaster. Genetics 174:915–929.</mixed-citation>
         </ref>
         <ref id="d1024e912a1310">
            <mixed-citation id="d1024e916" publication-type="other">
Rako, L., P. J. Shirriffs, and A. A. Hoffmann. 2009. Clinal variation in post-
winter male fertility retention; an adaptive overwintering strategy in
Drosophila melanogaster. J. Evol. Biol. 22(12):2438–2444.</mixed-citation>
         </ref>
         <ref id="d1024e929a1310">
            <mixed-citation id="d1024e933" publication-type="other">
Reeve, M. W., K. Fowler, and L. Partridge. 2000. Increased body size confers
greater fitness at lower experimental temperature in male Drosophila
melanogaster. J. Evol. Biol. 13(5):836–844.</mixed-citation>
         </ref>
         <ref id="d1024e946a1310">
            <mixed-citation id="d1024e950" publication-type="other">
Rohlf, F.J. 2010. TpsDig2, digitize landmarks and outlines. Department of
Ecology and Evolution, State Univ. of New York at Stony Brook, Stony
Brook, NY.</mixed-citation>
         </ref>
         <ref id="d1024e963a1310">
            <mixed-citation id="d1024e967" publication-type="other">
Sambucetti, P., V. Loeschcke, and F. M. Norry. 2006. Developmental time and
size-related traits in Drosophila buzzatii along an altitudinal gradient
from Argentina. Hereditas 143(2006):77–83.</mixed-citation>
         </ref>
         <ref id="d1024e980a1310">
            <mixed-citation id="d1024e984" publication-type="other">
Santos, M., P. F. Iriarte, W. Cespedes, J. Balanya, A. Fontdevila, and L. Serra.
2004. Swift laboratory thermal evolution of wing shape (but not size) in
Drosophila subobscura and its relationship with chromosomal inversion
polymorphism. J. Evol. Biol. 17: 841–855.</mixed-citation>
         </ref>
         <ref id="d1024e1001a1310">
            <mixed-citation id="d1024e1005" publication-type="other">
Santos, M., W. Cespedes, J. Balanyà, V. Trotta, F. C. F. Calboli, A. Font-
devila, and L. Serra. 2005. Temperature-related genetic changes in
laboratory populations of Drosophila subobscura: evidence against sim-
ple climatic-based explanations for latitudinal clines. Am. Nat. 165:258–
273.</mixed-citation>
         </ref>
         <ref id="d1024e1024a1310">
            <mixed-citation id="d1024e1028" publication-type="other">
Santos, M., D. Brites, and H. Laayouni. 2006. Thermal evolution of pre-adult
life history traits, geometric size and shape, and developmental stability
in Drosophila subobscura. J. Evol. Biol. 19:2006–2021.</mixed-citation>
         </ref>
         <ref id="d1024e1041a1310">
            <mixed-citation id="d1024e1045" publication-type="other">
Schluter, D. 1996. Adaptive radiation along genetic lines of least resistance.
Evolution 50:1766–1774.</mixed-citation>
         </ref>
         <ref id="d1024e1055a1310">
            <mixed-citation id="d1024e1059" publication-type="other">
Schmidt, P. S., Α. Β. Paaby, M. S. Heschel. 2005. Geographic variation in dia-
pause incidence, life-history traits, and climatic adaptation in Drosophila
melanogaster. Evolution 59(8): 1721–1732.</mixed-citation>
         </ref>
         <ref id="d1024e1072a1310">
            <mixed-citation id="d1024e1076" publication-type="other">
Schmidt, P. S., C-.T. Zhu, J. Das, M. Batavia, L. Yang, and W. E. Eanes, 2008.
An amino acid polymorphism in the couch potato gene forms the basis
for climatic adaptation in Drosophila melanogaster. Proc. Natl. Acad.
Sci.USA 105(42): 16207–16211.</mixed-citation>
         </ref>
         <ref id="d1024e1092a1310">
            <mixed-citation id="d1024e1096" publication-type="other">
Somme, L. 1989. Adaptations of terrestrial arthropods to the alpine environ-
ment. Biol. Rev. 64(4):367–407.</mixed-citation>
         </ref>
         <ref id="d1024e1107a1310">
            <mixed-citation id="d1024e1111" publication-type="other">
Sorensen, J., J. Dahlgaard, and V. Loeschcke. 2001. Genetic variation in ther-
mal tolerance among natural populations of Drosophila buzzatii: down
regulation of Hsp70 expression and variation in heat stress resistance
traits. Funct. Ecol. 15(3):289–296.</mixed-citation>
         </ref>
         <ref id="d1024e1127a1310">
            <mixed-citation id="d1024e1131" publication-type="other">
Starmer, W., and L. L. Wolf. 1997. Reproductive characteristics of the flower
breeding Drosophila hibisci Bock (Drosophilidae) along a latitudinal
gradient in eastern Australia: relation to flower and habitat features.
Biol. J. Linn. Soc. 62(3):459–473.</mixed-citation>
         </ref>
         <ref id="d1024e1147a1310">
            <mixed-citation id="d1024e1151" publication-type="other">
R Core Development Team. 2011. R: alanguageand environment for statistical
computing. R Foundation for Statistical Computing, Vienna, Austria.</mixed-citation>
         </ref>
         <ref id="d1024e1161a1310">
            <mixed-citation id="d1024e1165" publication-type="other">
Telonis-Scott, M., A. A. Hoffmann, and C. M. Sgro. 2011. The molecular
genetics of clinal variation: a case study of ebony and thoracic trident
pigmentation in Drosophila melanogaster from eastern Australia. Mol.
Ecol. 20(10):2100–2110.</mixed-citation>
         </ref>
         <ref id="d1024e1181a1310">
            <mixed-citation id="d1024e1185" publication-type="other">
Turner, T. L., M. T. Levine, M. L. Eckert, and D. J. Begun. 2008. Genomic
analysis of adaptive differentiation in Drosophila melanogaster. Genet-
ics 179(l):455–473.</mixed-citation>
         </ref>
         <ref id="d1024e1198a1310">
            <mixed-citation id="d1024e1202" publication-type="other">
Zelditch, M., D. Swiderski, and H. Sheets. 2004. Geometric morphometries
for biologists: a primer. Elsevier, San Diego, USA.</mixed-citation>
         </ref>
         <ref id="d1024e1213a1310">
            <mixed-citation id="d1024e1217" publication-type="other">
Zwaan, B. J., R. B. R. Azvedo, A. C. James, J. Van T'Land, and L. Partridge.
2000. Cellular basis of wing size variation in Drosophila melanogaster.
a comparison of latitudinal clines on two continents. Heredity 84
(Pt 3):338–347.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
