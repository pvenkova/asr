<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt13x1sx3</book-id>
      <subj-group>
         <subject content-type="call-number">P115.5.I75H35 2014</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Multilingualism</subject>
         <subj-group>
            <subject content-type="lcsh">Palestine</subject>
            <subj-group>
               <subject content-type="lcsh">History</subject>
               <subj-group>
                  <subject content-type="lcsh">20th century</subject>
               </subj-group>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Jews</subject>
         <subj-group>
            <subject content-type="lcsh">Palestine</subject>
            <subj-group>
               <subject content-type="lcsh">Languages</subject>
               <subj-group>
                  <subject content-type="lcsh">History</subject>
                  <subj-group>
                     <subject content-type="lcsh">20th century</subject>
                  </subj-group>
               </subj-group>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Hebrew language</subject>
         <subj-group>
            <subject content-type="lcsh">Palestine</subject>
            <subj-group>
               <subject content-type="lcsh">History</subject>
               <subj-group>
                  <subject content-type="lcsh">20th century</subject>
               </subj-group>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Jews</subject>
         <subj-group>
            <subject content-type="lcsh">Identity</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>History</subject>
      </subj-group>
      <book-title-group>
         <book-title>Babel in Zion</book-title>
         <subtitle>Jews, Nationalism, and Language Diversity in Palestine, 1920-1948</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Halperin</surname>
               <given-names>Liora R.</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>25</day>
         <month>11</month>
         <year>2014</year>
      </pub-date>
      <isbn content-type="ppub">9780300197488</isbn>
      <isbn content-type="ppub">0300197489</isbn>
      <isbn content-type="epub">9780300210200</isbn>
      <isbn content-type="epub">0300210205</isbn>
      <publisher>
         <publisher-name>Yale University Press</publisher-name>
         <publisher-loc>New Haven; London</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2015</copyright-year>
         <copyright-holder>Yale University</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt13x1sx3"/>
      <abstract abstract-type="short">
         <p>The promotion and vernacularization of Hebrew, traditionally a language of Jewish liturgy and study, was a central accomplishment of the Zionist movement in Palestine in the years following World War I. Viewing twentieth-century history through the lens of language, author Liora Halperin questions the accepted scholarly narrative of a Zionist move away from multilingualism, demonstrating how Jews in Palestine remained connected linguistically by both preference and necessity to a world outside the boundaries of the pro-Hebrew community even as it promoted Hebrew and achieved that language's dominance. The story of language encounters in Jewish Palestine is a fascinating tale of shifting power relationships, both locally and globally. Halperin's absorbing study explores how a young national community was compelled to modify the dictates of Hebrew exclusivity as it negotiated its relationships with its Jewish population, Palestinian Arabs, the British, and others outside the margins of the national project and ultimately came to terms with the limitations of its hegemony in an interconnected world.</p>
      </abstract>
      <counts>
         <page-count count="328"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1sx3.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1sx3.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1sx3.3</book-part-id>
                  <title-group>
                     <title>NOTE ON TRANSLITERATION AND TRANSLATION</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1sx3.4</book-part-id>
                  <title-group>
                     <title>ACKNOWLEDGMENTS</title>
                  </title-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1sx3.5</book-part-id>
                  <title-group>
                     <title>INTRODUCTION</title>
                     <subtitle>BABEL IN ZION</subtitle>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>On the occasion of the one-hundredth anniversary of the city of Tel Aviv, in 2009, the Tel Aviv Municipality collected family photographs from longtime residents of the city and displayed them prominently in its streets. A certain photo caught my eye. It depicts a woman in her twenties, dressed in an unusual and elaborate costume constructed of national flags labeled with names of world languages: German, Romanian, Polish, Russian, Yiddish, Greek, and English. She holds a sign in her hand in the shape of the Star of David that reads: “I am a Hebrew woman and Hebrew is my language.”</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1sx3.6</book-part-id>
                  <title-group>
                     <label>CHAPTER ONE</label>
                     <title>LANGUAGES OF LEISURE IN THE HOME, THE COFFEEHOUSE, AND THE CINEMA</title>
                  </title-group>
                  <fpage>26</fpage>
                  <abstract>
                     <p>Labor, exertion, and sacrifice stood at the center of the Zionist movement’s self-conception, and linguistic sacrifice in particular proved a potent symbol of nationalist commitments. To cast off the detritus of the Diaspora, according to mainstream ideology, Zionists were to engage in pioneering (<italic>halutziyut</italic>), a task defined both by cultivating the land and by building institutions to support the national project. The ideal of the pioneer was developed in the period of the Third Aliyah (wave of Zionist immigration to Palestine) in the years between 1919 and 1923 and was also applied retroactively to members of the Second Aliyah, who</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1sx3.7</book-part-id>
                  <title-group>
                     <label>CHAPTER TWO</label>
                     <title>PEDDLERS, TRADERS, AND THE LANGUAGES OF COMMERCE</title>
                  </title-group>
                  <fpage>62</fpage>
                  <abstract>
                     <p>The cultural venues discussed in the last chapter constituted one small segment of the broader consumer market in the Yishuv; the far larger part of the market pertained to the buying and selling of goods and services, including foreign, or “non-Hebrew,” ones. These products, too, were marked not only by their place of provenance but also by the multilingual culture that surrounded their sale and served merchants and customers as they engaged in transactions that transcended the cultural or physical boundaries of the Hebrew nation.</p>
                     <p>The journalist and educator Abraham Ludvipol, writing in the newspaper<italic>Haaretz</italic>in 1919, referred to</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1sx3.8</book-part-id>
                  <title-group>
                     <label>CHAPTER THREE</label>
                     <title>CLERKS, TRANSLATORS, AND THE LANGUAGES OF BUREAUCRACY</title>
                  </title-group>
                  <fpage>99</fpage>
                  <abstract>
                     <p>The importance of languages other than Hebrew, it might have seemed, would wane with the assimilation of new immigrants to Hebrew culture or the hypothetical creation of an all-Hebrew marketplace. But the political reality of Palestine, so long as British rule persisted, ensured interlingual contact and compelled a series of accommodations to English in the bureaucratic realm.</p>
                     <p>Soon after conquering Palestine, the British took the monumental step of making Hebrew an official language of Palestine, seemingly freeing Jews to use only their national language even in official business. On 1 October 1920, a notice appeared in the<italic>Official Gazette of</italic>
                     </p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1sx3.9</book-part-id>
                  <title-group>
                     <label>CHAPTER FOUR</label>
                     <title>ZION IN BABEL:</title>
                     <subtitle>The Yishuv in Its Arabic-Speaking Context</subtitle>
                  </title-group>
                  <fpage>142</fpage>
                  <abstract>
                     <p>While negotiating both the proximate power of the British Empire and the growing global power of English, Jews in Palestine found themselves early on reflecting on another linguistic force, one that was native and thus inextricable from the landscape: Palestine’s majority Arabic-speaking population. Recounting his 1914 visit to the new Zionist agricultural colonies of Palestine, the Yiddish writer Yehoash (Solomon Blumgarten) called the lack of Arabic knowledge he had found among immigrant Jews “one of the weakest points of the settlement.” Arabic was a language that Jews used to address employees, he had found, not a language of widespread daily</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1sx3.10</book-part-id>
                  <title-group>
                     <label>CHAPTER FIVE</label>
                     <title>HEBREW EDUCATION BETWEEN EAST AND WEST:</title>
                     <subtitle>Foreign-Language Instruction in Zionist Schools</subtitle>
                  </title-group>
                  <fpage>181</fpage>
                  <abstract>
                     <p>The Yishuv was awash in linguistic contacts, some unwelcome, some sought out. But one space seemed to sit apart from this mixing and to provide a laboratory for the growth and cultivation of Hebrew: the school. Zionist schools in British mandate Palestine were, in reality as much as in the popular imagination, places where Hebrew reigned even when it did not permeate other settings. Attention to the ways in which reflections on multilingualism occurred in this space, however, suggests that no setting in the Yishuv was fully removed from outside pressures.</p>
                     <p>The Hebrew pedagogical sanctum had been built with great</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1sx3.11</book-part-id>
                  <title-group>
                     <title>CONCLUSION</title>
                     <subtitle>THE PERSISTENCE OF BABEL</subtitle>
                  </title-group>
                  <fpage>222</fpage>
                  <abstract>
                     <p>“The threat of a Babel of languages … is menacing the young state of Israel as it takes its first steps,” warned Aharon Shekhtman in Davar in March 1949, less than a year after Israel declared its independence.¹ A report prepared by the Israel National Commission for UNESCO in 1953 spoke of the fear that, with continuing immigration, “immigrants would divide themselves into their own communities according to the language they spoke, that a Babel of languages and idioms would destroy originality and creativeness in the country.”² The Tower of Babel metaphor, used in the mandate period, continued to surface</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1sx3.12</book-part-id>
                  <title-group>
                     <title>NOTES</title>
                  </title-group>
                  <fpage>231</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1sx3.13</book-part-id>
                  <title-group>
                     <title>BIBLIOGRAPHY</title>
                  </title-group>
                  <fpage>275</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1sx3.14</book-part-id>
                  <title-group>
                     <title>INDEX</title>
                  </title-group>
                  <fpage>303</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
