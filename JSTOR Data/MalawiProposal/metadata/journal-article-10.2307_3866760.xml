<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">stafpapeintemone</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100190</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Staff Papers (International Monetary Fund)</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>International Monetary Fund</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00208027</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="doi">10.2307/3866760</article-id>
         <title-group>
            <article-title> Multipurpose Banking: Its Nature, Scope, and Relevance for Less Developed Countries (Les banques à vocation générale: leur nature, leur importance et leur applicabilité aux pays en développement) (La banca múltiple: Carácter, ámbito y pertinencia para los países menos desarrollados) </article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Deena R.</given-names>
                  <surname>Khatkhate</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Klaus-Walter</given-names>
                  <surname>Riechel</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>9</month>
            <year>1980</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">27</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i294955</issue-id>
         <fpage>478</fpage>
         <lpage>516</lpage>
         <page-range>478-516</page-range>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/3866760"/>
         <abstract>
            <p>The purpose of the paper is to delineate the role, nature, and scope of multipurpose banking and to point out its relevance for less developed countries (LDCs) that are undertaking a deliberate program to develop their financial systems. After referring to the historical evolution of multipurpose banking and surveying the financial systems in some leading industrial countries, it is shown that multipurpose banking is being adopted in most industrial countries. An attempt is then made in the paper to develop criteria to evaluate multipurpose banking so as to help policymakers in developing countries with emerging financial systems to choose between alternative models of financial systems. Of the many possible evaluation criteria, five have been selected as especially important: economic efficiency, mobilization of savings, promotion of entrepreneurial skills, financial stability, and conflicts of interest. The conclusion reached is that the final judgment about the appropriateness of a given financial system should be made on the basis of a comprehensive evaluation of the effectiveness of the system in the particular context of a given country. It is found, on the basis of the above evaluation criteria, that acceptance of the multipurpose banking approach may benefit LDCs, with their rapidly changing economic structures and patterns of demand, without generating the inefficiency that often ensues from an increase in the degree of concentration. A multipurpose banking system implies a more liberal approach to banking legislation, thereby avoiding the inefficiencies of fragmentation in the financial system that usually accompany a rigid legislated specialization. /// Cette étude a pour objet de préciser le rôle, la nature et l'objet des banques à vocation générale ainsi que de faire ressortir leur applicabilité aux pays en voie de développement (PVD) qui mettent en oeuvre des programmes visant à développer leurs systèmes financiers. Après avoir fait l'historique des banques à vocation générale et examiné les systèmes financiers de certains grands pays industrialisés, les auteurs de l'étude montrent que les banques à vocation générale font leur apparition dans la plupart des pays industrialisés. Ils s'efforcent ensuite de déterminer des critères permettant d'évaluer les banques à vocation générale afin d'aider les pouvoirs publics des pays en voie de développement qui se dotent de systèmes financiers à faire un choix entre différents modèles de systèmes financiers. Parmi de nombreux critères d'évaluation possibles, les auteurs en retiennent cinq qu'ils estiment particulièrement importants: l'efficience économique, la mobilisation de l'épargne, le renforcement des qualifications des dirigeants d'entreprise, la stabilité financière et les conflits d'intérêts. L'étude conclut que c'est sur la base d'une évaluation exhaustive de l'efficacité d'un système financier dans le contexte propre à un pays donné qu'il convient de formuler un jugement définitif quant à la pertinence dudit système. On montre, sur la base des critères d'évaluation mentionnés ci-dessus, que le concept de banque à vocation générale, laquelle présente une faculté d'adaptation rapide aux besoins nouveaux, paraît mieux adapté pour les pays en voie de développement -- dont les structures économiques et la configuration de la demande sont en mutation rapide -- que des systèmes plus spécialisés, notamment en ce qui concerne l'utilisation des compétences économiques et sans l'inefficacité que provoque souvent un renforcement de la concentration. Un système bancaire à vocation générale doit s'accompagner d'un assouplissement de la législation bancaire, ce qui permettra d'éviter les insuffisances résultant de la fragmentation du système financier, qui vont souvent de pair avec une spécialisation rigide imposée par la loi. /// El presente trabajo se propone describir en términos generales la función, carácter y ámbito de la banca múltiple, indicando su pertinencia para los países en desarrollo que han iniciado un programa deliberado para desarrollar sus sistemas financieros. Tras referirse a la evolución histórica de la banca múltiple y pasar revista a los sistemas financieros de algunos de los principales países industrializados, se indica que en la mayoría de dichos países se está adoptando la modalidad de banca múltiple. Se trata, luego, de formular criterios para evaluar esta modalidad de banca, con el fin de ayudar a quienes tienen a su cargo la formulación de la política en los países en desarrollo con sistemas financieros incipientes, para que puedan escoger entre distintos modelos de sistemas financieros. De los diversos posibles criterios de evaluación, se han seleccionado cinco como especialmente importantes, a saber: eficiencia económica, movilización del ahorro, fomento de la capacidad empresarial, estabilidad financiera y conflictos de interés. La conclusión a que se llega es que el juicio definitivo sobre la adecuación de un determinado sistema financiero debe basarse en una evaluación completa de la eficacia del sistema en el marco concreto de un determinado país. Basándose en dichos criterios de evaluación, se llega a la conclusión de que la aceptación del concepto de banca múltiple puede beneficiar a los países en desarrollo, dadas su estructura económica y configuración de la demanda en rápida evolución, sin originar la ineficiencia que suele producirse tras un aumento del grado de concentración. Un sistema bancario múltiple implica un enfoque más liberal de la legislación bancaria, evitándose así en el sistema financiero las ineficiencias de la fragmentación que suelen acompañar a una rígida especialización legislada.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d1065e153a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1065e160" publication-type="other">
Adelman (1969)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1065e166" publication-type="other">
Short (1979)</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>Bibliography</title>
         <ref id="d1065e182a1310">
            <mixed-citation id="d1065e186" publication-type="journal">
Adelman, M. A., "Comment on the *H' Concentration Measure as a Numbers
Equivalent," Review of Economics and Statistics, Vol. 61 (February
1969), pp. 99-101<object-id pub-id-type="doi">10.2307/1926955</object-id>
               <fpage>99</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1065e205a1310">
            <mixed-citation id="d1065e209" publication-type="journal">
Awad, Mohamed Hashim, "The Supply of Risk Bearers in the Underdevel¬
oped Countries," Economic Development and Cultural Change, Vol. 19
(April1971), pp. 461-68<object-id pub-id-type="jstor">10.2307/1152929</object-id>
               <fpage>461</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1065e228a1310">
            <mixed-citation id="d1065e232" publication-type="book">
Bell, Frederick W., and Neil B. Murphy, Economies of Scale in Commercial
Banking (Federal Reserve Bank of Boston, 1967)<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Bell</surname>
                  </string-name>
               </person-group>
               <source>Economies of Scale in Commercial Banking</source>
               <year>1967</year>
            </mixed-citation>
         </ref>
         <ref id="d1065e257a1310">
            <mixed-citation id="d1065e261" publication-type="journal">
-, "Impact of Market Structure on the Price of a Commercial Banking
Service," Review of Economics and Statistics, Vol. 61 (February1969),
pp. 210-13<object-id pub-id-type="doi">10.2307/1926732</object-id>
               <fpage>210</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1065e281a1310">
            <mixed-citation id="d1065e285" publication-type="journal">
Bhatia, Rattan J., and Deena R. Khatkhate, "Financial Intermediation, Sav¬
ings Mobilization, and Entrepreneurial Development: The African Experi¬
ence," Staff Papers, Vol. 22 (March1975), pp. 132-58<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Bhatia</surname>
                  </string-name>
               </person-group>
               <issue>March</issue>
               <fpage>132</fpage>
               <volume>22</volume>
               <source>Staff Papers</source>
               <year>1975</year>
            </mixed-citation>
         </ref>
         <ref id="d1065e323a1310">
            <mixed-citation id="d1065e327" publication-type="book">
Bhatt, Vinayak V., Structure of Financial Institutions (Bombay, 1972)<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Bhatt</surname>
                  </string-name>
               </person-group>
               <source>Structure of Financial Institutions</source>
               <year>1972</year>
            </mixed-citation>
         </ref>
         <ref id="d1065e349a1310">
            <mixed-citation id="d1065e353" publication-type="book">
Bloomfield, Arthur I., "Monetary Policy in Underdeveloped Countries," in
Public Policy, ed. by Carl J. Friedrich and Seymour E. Harris (Harvard
University Press, 1956), pp. 232-74<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Bloomfield</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Monetary Policy in Underdeveloped Countries</comment>
               <fpage>232</fpage>
               <source>Public Policy</source>
               <year>1956</year>
            </mixed-citation>
         </ref>
         <ref id="d1065e388a1310">
            <mixed-citation id="d1065e392" publication-type="book">
Cameron, Rondo E., ed., Banking and Economic Development: Some Lessons
of History (Oxford University Press, 1972)<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Cameron</surname>
                  </string-name>
               </person-group>
               <source>Banking and Economic Development: Some Lessons of History</source>
               <year>1972</year>
            </mixed-citation>
         </ref>
         <ref id="d1065e417a1310">
            <mixed-citation id="d1065e421" publication-type="book">
—, and others, eds., Banking in the Early Stages of Industrialization: A
Study in Comparative Economic History (Oxford University Press, 1967)<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Cameron</surname>
                  </string-name>
               </person-group>
               <source>Banking in the Early Stages of Industrialization: A Study in Comparative Economic History</source>
               <year>1967</year>
            </mixed-citation>
         </ref>
         <ref id="d1065e446a1310">
            <mixed-citation id="d1065e450" publication-type="journal">
Chelliah, Raja J., Hessel J. Baas, and Margaret R. Kelly, "Tax Ratios and Tax
Effort in Developing Countries, 1969-71," Staff Papers, Vol. 22 (March
1975), pp. 187-205<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Chelliah</surname>
                  </string-name>
               </person-group>
               <issue>March</issue>
               <fpage>187</fpage>
               <volume>22</volume>
               <source>Staff Papers</source>
               <year>1975</year>
            </mixed-citation>
         </ref>
         <ref id="d1065e489a1310">
            <mixed-citation id="d1065e493" publication-type="journal">
Christians, F. Wilhelm, "Why the Universal Bank Works," The Banker,
Vol. 127 (October1977), pp. 104-106<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Christians</surname>
                  </string-name>
               </person-group>
               <issue>October</issue>
               <fpage>104</fpage>
               <volume>127</volume>
               <source>The Banker</source>
               <year>1977</year>
            </mixed-citation>
         </ref>
         <ref id="d1065e528a1310">
            <mixed-citation id="d1065e532" publication-type="book">
Crick, Wilfred F., ed., Commonwealth Banking Systems (Oxford University
Press, 1965)<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Crick</surname>
                  </string-name>
               </person-group>
               <source>Commonwealth Banking Systems</source>
               <year>1965</year>
            </mixed-citation>
         </ref>
         <ref id="d1065e557a1310">
            <mixed-citation id="d1065e561" publication-type="journal">
Deane, Phyllis, "Capital Formation in Britain Before the Railway Age," Eco-
nomic Development and Cultural Change, Vol. 9 (April1961), pp. 352-68<object-id pub-id-type="jstor">10.2307/1151804</object-id>
               <fpage>352</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1065e577a1310">
            <mixed-citation id="d1065e581" publication-type="book">
— , and W. A. Cole, British Economic Growth 1688-1959: Trends and
Structure (Cambridge University Press, 1967)<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Deane</surname>
                  </string-name>
               </person-group>
               <source>British Economic Growth 1688-1959: Trends and Structure</source>
               <year>1967</year>
            </mixed-citation>
         </ref>
         <ref id="d1065e606a1310">
            <mixed-citation id="d1065e610" publication-type="journal">
Federal Reserve Bank of New York, "A New Supervisory Approach to For¬
eign Lending," Quarterly Review, Vol. 3 (Spring1978), pp. 1-6<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Federal Reserve Bank of New York</surname>
                  </string-name>
               </person-group>
               <issue>Spring</issue>
               <fpage>1</fpage>
               <volume>3</volume>
               <source>Quarterly Review</source>
               <year>1978</year>
            </mixed-citation>
         </ref>
         <ref id="d1065e645a1310">
            <mixed-citation id="d1065e649" publication-type="book">
Gerschenkron, Alexander, Economic Backwardness in Historical Perspective:
A Book of Essays (Harvard University Press, 1962)<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Gerschenkron</surname>
                  </string-name>
               </person-group>
               <source>Economic Backwardness in Historical Perspective: A Book of Essays</source>
               <year>1962</year>
            </mixed-citation>
         </ref>
         <ref id="d1065e675a1310">
            <mixed-citation id="d1065e679" publication-type="book">
—, Continuity in History, and Other Essays (Harvard University Press,
1968)<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Gerschenkron</surname>
                  </string-name>
               </person-group>
               <source>Continuity in History, and Other Essays</source>
               <year>1968</year>
            </mixed-citation>
         </ref>
         <ref id="d1065e704a1310">
            <mixed-citation id="d1065e708" publication-type="book">
Goldsmith, Raymond W., Financial Structure and Development (Yale Univer¬
sity Press, 1969)<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Goldsmith</surname>
                  </string-name>
               </person-group>
               <source>Financial Structure and Development</source>
               <year>1969</year>
            </mixed-citation>
         </ref>
         <ref id="d1065e733a1310">
            <mixed-citation id="d1065e737" publication-type="journal">
Greenbaum, Stuart I., "Competition and Efficiency in the Banking System—
Empirical Research and Its Policy Implications," Journal of Political
Economy, Vol. 75 (August1967), pp. 461-79<object-id pub-id-type="jstor">10.2307/1832151</object-id>
               <fpage>461</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1065e756a1310">
            <mixed-citation id="d1065e760" publication-type="journal">
Heggestad, Arnold A., and John J. Mingo, "Prices, Nonprices, and Concentra¬
tion in Commercial Banking," Journal of Money, Credit and Banking,
Vol. 8 (February1976), pp. 107-17<object-id pub-id-type="doi">10.2307/1991923</object-id>
               <fpage>107</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1065e779a1310">
            <mixed-citation id="d1065e783" publication-type="book">
Immenga, Ulrich, Participation by Banks in Other Branches of the Economy
(Commission of the European Communities, February 1975)<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Immenga</surname>
                  </string-name>
               </person-group>
               <source>Participation by Banks in Other Branches of the Economy</source>
               <year>1975</year>
            </mixed-citation>
         </ref>
         <ref id="d1065e808a1310">
            <mixed-citation id="d1065e812" publication-type="book">
Inter-Bank Research Organisation, Banking Systems Abroad (London, 1978)<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Inter-Bank Research Organisation</surname>
                  </string-name>
               </person-group>
               <source>Banking Systems Abroad</source>
               <year>1978</year>
            </mixed-citation>
         </ref>
         <ref id="d1065e835a1310">
            <mixed-citation id="d1065e839" publication-type="journal">
Kaufman, George G., "Bank Market Structure and Performance: The Evi¬
dence from Iowa," Southern Economic Journal, Vol. 32 (April1966),
pp. 429-39<object-id pub-id-type="doi">10.2307/1055897</object-id>
               <fpage>429</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1065e858a1310">
            <mixed-citation id="d1065e862" publication-type="journal">
Khanna, Sushil, "Capital and Finance in the Industrial Revolution: Lessons
for the Third World," Economic and Political Weekly, Vol. 13 (Novem-
ber 18, 1978), pp. 1889-98<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Khanna</surname>
                  </string-name>
               </person-group>
               <issue>November 18</issue>
               <fpage>1889</fpage>
               <volume>13</volume>
               <source>Economic and Political Weekly</source>
               <year>1978</year>
            </mixed-citation>
         </ref>
         <ref id="d1065e900a1310">
            <mixed-citation id="d1065e904" publication-type="journal">
Khatkhate, Deena R., and Delano P. Villanueva, "Deposit Substitutes and
their Monetary Policy Significance in Developing Countries," Oxford Bul-
letin of Economics and Statistics, Vol. 41 (February1979), pp. 37-50<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Khatkhate</surname>
                  </string-name>
               </person-group>
               <issue>February</issue>
               <fpage>37</fpage>
               <volume>41</volume>
               <source>Oxford Bulletin of Economics and Statistics</source>
               <year>1979</year>
            </mixed-citation>
         </ref>
         <ref id="d1065e942a1310">
            <mixed-citation id="d1065e946" publication-type="book">
Madan, B. K. (1964 a), Aspects of Economic Development and Policy: Essays
(Bombay, 1964)<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Madan</surname>
                  </string-name>
               </person-group>
               <source>Aspects of Economic Development and Policy: Essays</source>
               <year>1964</year>
            </mixed-citation>
         </ref>
         <ref id="d1065e971a1310">
            <mixed-citation id="d1065e975" publication-type="journal">
—(1964 b), "Role of Commercial Banks in Developing Countries and
Measures for Improving the Adequacy of Banking Facilities," Reserve
Bank of India, Bulletin, Vol. 18 (June1964), pp. 756-64<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Madan</surname>
                  </string-name>
               </person-group>
               <issue>June</issue>
               <fpage>756</fpage>
               <volume>18</volume>
               <source>Bulletin</source>
               <year>1964</year>
            </mixed-citation>
         </ref>
         <ref id="d1065e1013a1310">
            <mixed-citation id="d1065e1017" publication-type="journal">
Masson, Robert T., "The Creation of Risk Aversion by Imperfect Capital Mar¬
kets," American Economic Review, Vol. 62 (March1972), pp. 77-86<object-id pub-id-type="jstor">10.2307/1821475</object-id>
               <fpage>77</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1065e1034a1310">
            <mixed-citation id="d1065e1038" publication-type="journal">
Mülhaupt, L., "In Defence of Universal Banks," The Banker, Vol. 126 (July
1976), pp. 775-82<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Mülhaupt</surname>
                  </string-name>
               </person-group>
               <issue>July</issue>
               <fpage>775</fpage>
               <volume>126</volume>
               <source>The Banker</source>
               <year>1976</year>
            </mixed-citation>
         </ref>
         <ref id="d1065e1073a1310">
            <mixed-citation id="d1065e1077" publication-type="journal">
Patrick, Hugh T., "Financial Development and Economic Growth in Underde¬
veloped Countries," Economic Development and Cultural Change,
Vol. 14 (January1966), pp. 174-89<object-id pub-id-type="jstor">10.2307/1152568</object-id>
               <fpage>174</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1065e1096a1310">
            <mixed-citation id="d1065e1100" publication-type="journal">
Pollard, Sidney, "Fixed Capital in the Industrial Revolution in Britain," Jour-
nal of Economic History, Vol. 24 (September1964), pp. 229-314<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Pollard</surname>
                  </string-name>
               </person-group>
               <issue>September</issue>
               <fpage>229</fpage>
               <volume>24</volume>
               <source>Journal of Economic History</source>
               <year>1964</year>
            </mixed-citation>
         </ref>
         <ref id="d1065e1135a1310">
            <mixed-citation id="d1065e1139" publication-type="journal">
Richard, Denis M., and Dan P. Villanueva, "Relative Economic Efficiency of
Banking Systems in a Developing Country," forthcoming in Vol. 4, No. 4
(1980) oi Journal of Banking and Finance<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Richard</surname>
                  </string-name>
               </person-group>
               <issue>4</issue>
               <volume>4</volume>
               <source>Journal of Banking and Finance</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
         <ref id="d1065e1174a1310">
            <mixed-citation id="d1065e1178" publication-type="book">
Riechel, Klaus-Walter, "Functional and Structural Aspects of the German
Universal Banking System" (unpublished, International Monetary Fund,
March 27, 1980)<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Riechel</surname>
                  </string-name>
               </person-group>
               <source>Functional and Structural Aspects of the German Universal Banking System</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
         <ref id="d1065e1207a1310">
            <mixed-citation id="d1065e1211" publication-type="journal">
Schatz, Sayre P., "Aiding Nigerian Business: The Yaba Industrial Estate,"
Nigerian Journal of Economic and Social Studies, Vol. 6 (July1964),
pp. 199-219<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Schatz</surname>
                  </string-name>
               </person-group>
               <issue>July</issue>
               <fpage>199</fpage>
               <volume>6</volume>
               <source>Nigerian Journal of Economic and Social Studies</source>
               <year>1964</year>
            </mixed-citation>
         </ref>
         <ref id="d1065e1250a1310">
            <mixed-citation id="d1065e1254" publication-type="book">
Schumpeter, Joseph A., The Theory of Economic Development: An Inquiry
into Profits, Capital, Credit, Interest, and the Business Cycle (Harvard
University Press, 1936)<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Schumpeter</surname>
                  </string-name>
               </person-group>
               <source>The Theory of Economic Development: An Inquiry into Profits, Capital, Credit, Interest, and the Business Cycle</source>
               <year>1936</year>
            </mixed-citation>
         </ref>
         <ref id="d1065e1283a1310">
            <mixed-citation id="d1065e1287" publication-type="journal">
Short, Brock K., "Capital Requirements for Commercial Banks: A Survey of
the Issues," Staff Papers, Vol. 25 (September1978), pp. 528-63<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Short</surname>
                  </string-name>
               </person-group>
               <issue>September</issue>
               <fpage>528</fpage>
               <volume>25</volume>
               <source>Staff Papers</source>
               <year>1978</year>
            </mixed-citation>
         </ref>
         <ref id="d1065e1322a1310">
            <mixed-citation id="d1065e1326" publication-type="journal">
—, "The Relation Between Commercial Bank Profit Rates and Banking
Concentration in Canada, Western Europe, and Japan," Journal of Bank-
ing and Finance, Vol. 3 (October1979), pp. 209-19<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Short</surname>
                  </string-name>
               </person-group>
               <issue>October</issue>
               <fpage>209</fpage>
               <volume>3</volume>
               <source>Journal of Banking and Finance</source>
               <year>1979</year>
            </mixed-citation>
         </ref>
         <ref id="d1065e1364a1310">
            <mixed-citation id="d1065e1368" publication-type="journal">
Smith, Paul W., "Measures of Banking Structure and Competition," Federal
Reserve Bulletin, Vol. 51 (September1965), pp. 1212-22<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Smith</surname>
                  </string-name>
               </person-group>
               <issue>September</issue>
               <fpage>1212</fpage>
               <volume>51</volume>
               <source>Federal Reserve Bulletin</source>
               <year>1965</year>
            </mixed-citation>
         </ref>
         <ref id="d1065e1403a1310">
            <mixed-citation id="d1065e1407" publication-type="journal">
Srivastava, U. K., and Nikhil M. Oza, "Stipulation and Exercise of Convert¬
ibility Options by Financial Institutions," Economic and Political Weekly,
Vol. 13 (November 25, 1978), pp. M133-56<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Srivastava</surname>
                  </string-name>
               </person-group>
               <issue>November 25</issue>
               <fpage>M133</fpage>
               <volume>13</volume>
               <source>Economic and Political Weekly</source>
               <year>1978</year>
            </mixed-citation>
         </ref>
         <ref id="d1065e1445a1310">
            <mixed-citation id="d1065e1449" publication-type="journal">
Stillson, Richard T., "An Analysis of Information and Transaction Services in
Financial Institutions," Journal of Money, Credit and Banking, Vol. 6
(November1974), pp. 517-35<object-id pub-id-type="doi">10.2307/1991461</object-id>
               <fpage>517</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1065e1469a1310">
            <mixed-citation id="d1065e1473" publication-type="book">
Tun Wai, U., Financial Intermediaries and National Savings in Developing
Countries (New York, 1972)<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Tun Wai</surname>
                  </string-name>
               </person-group>
               <source>Financial Intermediaries and National Savings in Developing Countries</source>
               <year>1972</year>
            </mixed-citation>
         </ref>
         <ref id="d1065e1498a1310">
            <mixed-citation id="d1065e1502" publication-type="journal">
—, and Hugh T. Patrick, "Stock and Bond Issues and Capital Markets in
Less Developed Countries," Staff Papers, Vol. 20 (July1973), pp. 253-
305<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Tun Wai</surname>
                  </string-name>
               </person-group>
               <issue>July</issue>
               <fpage>253</fpage>
               <volume>20</volume>
               <source>Staff Papers</source>
               <year>1973</year>
            </mixed-citation>
         </ref>
         <ref id="d1065e1540a1310">
            <mixed-citation id="d1065e1544" publication-type="book">
United States, President's Commission on Financial Structure and Regulation,
Report, December 1971 (Washington, 1972)<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>United States</surname>
                  </string-name>
               </person-group>
               <source>Report</source>
               <year>1972</year>
            </mixed-citation>
         </ref>
         <ref id="d1065e1569a1310">
            <mixed-citation id="d1065e1573" publication-type="book">
Whale, P. Barrett, Joint Stock Banking in Germany: A Study of the German
Creditbanks Before and After the War (London, 1930)<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Whale</surname>
                  </string-name>
               </person-group>
               <source>Joint Stock Banking in Germany: A Study of the German Creditbanks Before and After the War</source>
               <year>1930</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
