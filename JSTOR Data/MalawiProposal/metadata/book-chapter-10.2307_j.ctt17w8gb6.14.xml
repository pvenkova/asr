<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt16f89fk</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt17w8gb6</book-id>
      <subj-group>
         <subject content-type="call-number">ML3917.Z55C55 2015</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Popular music</subject>
         <subj-group>
            <subject content-type="lcsh">Social aspects</subject>
            <subj-group>
               <subject content-type="lcsh">Zimbabwe</subject>
               <subj-group>
                  <subject content-type="lcsh">History</subject>
                  <subj-group>
                     <subject content-type="lcsh">20th century</subject>
                  </subj-group>
               </subj-group>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Popular music</subject>
         <subj-group>
            <subject content-type="lcsh">Political aspects</subject>
            <subj-group>
               <subject content-type="lcsh">Zimbabwe</subject>
               <subj-group>
                  <subject content-type="lcsh">History</subject>
                  <subj-group>
                     <subject content-type="lcsh">20th century</subject>
                  </subj-group>
               </subj-group>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Missions</subject>
         <subj-group>
            <subject content-type="lcsh">Zimbabwe</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Zimbabwe</subject>
         <subj-group>
            <subject content-type="lcsh">Social conditions</subject>
            <subj-group>
               <subject content-type="lcsh">20th century</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Zimbabwe</subject>
         <subj-group>
            <subject content-type="lcsh">Colonial influence</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Music</subject>
      </subj-group>
      <book-title-group>
         <book-title>African Music, Power, and Being in Colonial Zimbabwe</book-title>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Chikowero</surname>
               <given-names>Mhoze</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>13</day>
         <month>10</month>
         <year>2015</year>
      </pub-date>
      <isbn content-type="ppub">9780253017680</isbn>
      <isbn content-type="epub">9780253018090</isbn>
      <isbn content-type="epub">0253018099</isbn>
      <publisher>
         <publisher-name>Indiana University Press</publisher-name>
         <publisher-loc>Bloomington; Indianapolis</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2015</copyright-year>
         <copyright-holder>Mhoze Chikowero</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt17w8gb6"/>
      <abstract abstract-type="short">
         <p>In this new history of music in Zimbabwe, Mhoze Chikowero deftly uses African sources to interrogate the copious colonial archive, reading it as a confessional voice along and against the grain to write a complex history of music, colonialism, and African self-liberation. Chikowero's book begins in the 1890s with missionary crusades against African performative cultures and African students being inducted into mission bands, which contextualize the music of segregated urban and mining company dance halls in the 1930s, and he builds genealogies of the Chimurenga music later popularized by guerrilla artists like Dorothy Masuku, Zexie Manatsa, Thomas Mapfumo, and others in the 1970s. Chikowero shows how Africans deployed their music and indigenous knowledge systems to fight for their freedom from British colonial domination and to assert their cultural sovereignty.</p>
      </abstract>
      <counts>
         <page-count count="364"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17w8gb6.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17w8gb6.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17w8gb6.3</book-part-id>
                  <title-group>
                     <title>Kupa Kutenda / Acknowledgments</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17w8gb6.4</book-part-id>
                  <title-group>
                     <title>Introduction:</title>
                     <subtitle>Cross-Cultural Encounters: Song, Power, and Being</subtitle>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>Writing about her childhood in 1960s Buhera, in rural colonial Zimbabwe, Sekai Nzenza (<italic>Herald</italic>, December 11, 2012) reminisced about how, one Christmas Eve, her mother instructed her and her siblings to look out for a local Anglican priest, Baba Mutemarari. Once they spotted him coming, she instructed them to hide “everything that was unChristian around the village compound. We covered two big pots of the highly potent mhanga beer under sacks and blankets then closed the kitchen hut. My brother Charles dragged our famous drum [ngoma] called ‘Zino irema’ and hid that in the granary. My father reluctantly switched off</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17w8gb6.5</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Missionary Witchcrafting African Being:</title>
                     <subtitle>Cultural Disarmament</subtitle>
                  </title-group>
                  <fpage>19</fpage>
                  <abstract>
                     <p>In a paper that he read at the University College of Rhodesia and Nyasaland in 1961, W. F. Rea argued that European missionaries should be judged as individuals who obeyed Jesus’ command to set out and teach the Christian gospel to all nations, not as people whose purpose was to further any political ideology, including the imperialism of the late nineteenth century. He contended that their work will certainly be judged, “but it is only in the Kingdom of heaven that the verdicts are published” (2). Rea’s work represents missionary self-writing, one of whose tenets is self-praise for “helping poor</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17w8gb6.6</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>Purging the “Heathen” Song, Mis/Grafting the Missionary Hymn</title>
                  </title-group>
                  <fpage>56</fpage>
                  <abstract>
                     <p>I have read the missionary effort to redesign African being through schooling and the lens of the brass band. The school brass band is, however, only part of the story of the evangelical musical odyssey. The first part of this chapter explores the mission’s efforts to graft the more conventional missionary idiom, the hymn, onto the African musical psyche. The mission employed the now familiar<italic>modus operandi:</italic>assaulting the hymn’s imagined antithesis, African song. The second part examines how this missionary ethnocentric cultural policy threw the mission church into crisis by midcentury, when some second-generation African Christian converts and a</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17w8gb6.7</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>“Too Many Don’ts”:</title>
                     <subtitle>Reinforcing, Disrupting the Criminalization of African Musical Cultures</subtitle>
                  </title-group>
                  <fpage>80</fpage>
                  <abstract>
                     <p>That night I sang and danced as I had never done before. I just let myself go and really had a wonderful time. I was surprised when it was dawn…. The concert had ended and we walked back home, tired, sleepy and happy.” These are the words of Stanlake Samkange (1975, 13), son of Rev. Thompson Samkange, reminiscing about his first scintillating experience of a<italic>konzati</italic>(concert, pl.<italic>makonzati</italic>) at Madzima School in Zvimba sometime in the early 1930s. It was in response to a<italic>konzati</italic>like this one and related “night dances” that, in June 1930, the Southern Rhodesia</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17w8gb6.8</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Architectures of Control:</title>
                     <subtitle>African Urban Re/Creation</subtitle>
                  </title-group>
                  <fpage>112</fpage>
                  <abstract>
                     <p>The wisdom that Reverend Phillips cites in the epigraph above summarizes the history of colonial state and capitalist investment of money, time, and energy in African urban entertainment in white settler Southern Rhodesia and most of Southern Africa. By the 1930s, the weight of colonial expropriation and enclosure had begun to squeeze and dismember the African family in the rural reserves, further dislocating young men into “native locations”—<italic>marukesheni</italic>(sing.<italic>rukesheni</italic>)—the sequestered racial ghettoes where they lived as seekers of alternative subsistence in the urban economy. From the mission schools, “mission boys” also took their education, adopted European cultural</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17w8gb6.9</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>The “Tribal Dance” as a Colonial Alibi:</title>
                     <subtitle>Ethnomusicology and the Tribalization of African Being</subtitle>
                  </title-group>
                  <fpage>131</fpage>
                  <abstract>
                     <p>On April 19, 1944, the<italic>African Weekly</italic>reported the prevalence of African weekend musical drumming and dancing in open spaces in the Salisbury Location:</p>
                     <p>It is interesting to visit the Native Location on Sunday afternoon. Sunday appears to have become the day of tribal activities. One finds almost every tribe busy organising itself. One hears drums beating everywhere in the Location. It is pleasing to watch these tribal dances and, no doubt, from the point of view of physical training, to those who take part, they must be beneficial. Apart from this point, these dances keep the Bantu public occupied</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17w8gb6.10</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Chimanjemanje:</title>
                     <subtitle>Performing and Contesting Colonial Modernity</subtitle>
                  </title-group>
                  <fpage>154</fpage>
                  <abstract>
                     <p>Not very long ago, wrote Jean-Paul Sartre (in his preface to Fanon 1968, 7), “the Earth counted two billion inhabitants, that is, five hundred million men, and one billion five hundred million natives. The former possessed the [v]erb, the latter borrowed it.” This was the “Golden Age” of empire, which, however, “came to an end: the mouths opened, unassisted.” European colonizing discourses justified the despoliation of Africans on the grounds of cultural difference, namely, that Africans were illiterate, precapitalist “heathens.” Among other responses, Africans selectively appropriated this colonizing discourse, and repurposed and redeployed it to unmake their marginalization. Here, complicating</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17w8gb6.11</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>The Many Moods of “Skokiaan”:</title>
                     <subtitle>Criminalized Leisure, Underclass Defiance, and Self-Narration</subtitle>
                  </title-group>
                  <fpage>187</fpage>
                  <abstract>
                     <p>In the 1940s, Zimbabwean musician August Machona Musarurwa composed and subsequently recorded a saxophone instrumental, “Skokiaan,” which quickly became an anthem in the country’s teeming “native locations,”<italic>marukesheni</italic>. Over the next two decades, dozens of western and regional musicians performed and created their own versions of the song. This appropriation of “Skokiaan” was part of a broader creative reading and misreading that helped to romanticize the song as a vernacular affirmation of the exoticized images of Africa prevalent in the western world. The romanticization was made possible by a decontextualization of the song which, in its original context of production</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17w8gb6.12</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>Usable Pasts:</title>
                     <subtitle>Crafting Madzimbabwe through Memory, Tradition, Song</subtitle>
                  </title-group>
                  <fpage>213</fpage>
                  <abstract>
                     <p>In the book that earned him acclaim as a theorist of revolutionary self-liberation, Frantz Fanon (1967b, 45) visualized the “native” at the outbreak of the armed struggle against colonialism:</p>
                     <p>At long last the native, gun in hand, stands face to face with … the forces of colonialism. And the youth of a colonized country, growing up in an atmosphere of shot and fire, may well make a mock of, and does not hesitate to pour scorn upon the zombies of his ancestors, the horses with two heads, the dead who rise again, and the djinns who rush into your body</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17w8gb6.13</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>Cultures of Resistance:</title>
                     <subtitle>Genealogies of Chimurenga Song</subtitle>
                  </title-group>
                  <fpage>239</fpage>
                  <abstract>
                     <p>Zimbabwe’s Chimurenga music has drawn much scholarly attention, partly because of the genre’s imbrication with the Second Chimurenga, the 1960s–70s liberation war that finally dislodged Rhodesian settler rule in 1980. In a book dedicated to this huge ouevre, A. J. C. Pongweni (1982) hailed Chimurenga as the “songs that won the liberation war.” What is remarkable is that, in spite of the acknowledged long history of Zimbabwe’s cultures of anticolonial resistance, analysis of Chimurenga music tends to limit its purview to the liberation war. This raises two problems. Firstly, the scholarship wittingly or unwittingly gives credence to a self-congratulatory,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17w8gb6.14</book-part-id>
                  <title-group>
                     <label>10</label>
                     <title>Jane Lungile Ngwenya:</title>
                     <subtitle>A Transgenerational Conversation</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <string-name>MC</string-name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Ngwenya</surname>
                           <given-names>Jane Lungile</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>274</fpage>
                  <abstract>
                     <p>Gogo (Grandmother) Jane Lungile Ngwenya’s life story and sociopolitical striving inject a vivid personal perspective into the multivalent story of African being, song, and power in colonial Zimbabwe. Ngwenya was born at the crest of the Rhodesian settler system, when the impact of missionary education and colonial policies had drastically reshaped African life. The trajectory of her life, from young girl growing up in the “native reserve,” to student, teacher, mother, politician, and, ultimately, a guerrilla who questioned and stood up to fight the colonial system, is instructive about the resilience of African consciousness under the assaults of colonial epistemes</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17w8gb6.15</book-part-id>
                  <title-group>
                     <title>Epilogue:</title>
                     <subtitle>Postcolonial Legacies: Song, Power, and Knowledge Production</subtitle>
                  </title-group>
                  <fpage>293</fpage>
                  <abstract>
                     <p>In his preface to Fanon’s influential<italic>The Wretched of the Earth</italic>(1968, 20), Jean-Paul Sartre aptly captured the psychological impact of colonialism when he wrote that the condition of the colonized is a nervous condition. Colonists sought to subjugate Africans both through their own European cultures and also through subverted African cultures, and Africans responded variously through assimilation, inculturation, accommodation, and resistance. All of these responses reinforced the cultural front as a creative site for a dialectical fashioning of the colonizing self, the colonized subject, and the self-liberating being. African engagement with colonial epistemicide was invariably aporic, producing new orders</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17w8gb6.16</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>305</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17w8gb6.17</book-part-id>
                  <title-group>
                     <title>Selected Bibliography and Discography</title>
                  </title-group>
                  <fpage>311</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17w8gb6.18</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>327</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17w8gb6.19</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>347</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
