<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">envideveecon</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50012637</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Environment and Development Economics</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Cambridage University Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">1355770X</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">14694395</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">44379229</article-id>
         <article-categories>
            <subj-group>
               <subject>Policy Options</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Tradable emission quotas, technical progress and climate change</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>I.G.</given-names>
                  <surname>BERTRAM</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>GEOFFREY</given-names>
                  <surname>BERTRAM</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>10</month>
            <year>1996</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40183250</issue-id>
         <fpage>465</fpage>
         <lpage>487</lpage>
         <permissions>
            <copyright-statement>Copyright © 1996 Cambridge University Press</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/44379229"/>
         <abstract>
            <p>The paper reviews two alternative rules for allocation of property rights in a global greenhouse-gas emissions budget, assuming implementation of a tradablequota arrangement. These are the per capita rule and no-regrets-for-the-South (NRFTS) rule. The operation of a quota market under these alternative regimes is simulated on a spreadsheet, using 1990-1 data from 125 countries. A significant result is that once the South has secured a quota allocation based on the per capita principle, it stands collectively to lose from progress in abatement technology because of the strong link from technical progress to the world market price of quota. The more restricted NRFTS rule gives the South smaller gains from the quota system, but enables it to retain some of the rents from its own technical progress. Some implications for the South's position in future negotiations are noted.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d568e251a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d568e258" publication-type="other">
Grubb et al. (1993).</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d568e274a1310">
            <mixed-citation id="d568e278" publication-type="other">
Barrett, S. (1992), ' "Acceptable" allocations of tradeable carbon emissions entitle-
ments in a global warming treaty', in United Nations Conference on Trade and
Development, Combating Global Warming: Study on a Global System of Tradeable
Carbon Emission Entitlements , New York: United Nations, ch. 6.</mixed-citation>
         </ref>
         <ref id="d568e294a1310">
            <mixed-citation id="d568e298" publication-type="other">
Barro, R.J. and X. Sala-i-Martin (1995), Economic Growth, New York: McGraw-Hill.</mixed-citation>
         </ref>
         <ref id="d568e305a1310">
            <mixed-citation id="d568e309" publication-type="other">
Bertram, G. (1992a), 'Latin America in a World Greenhouse Convention', Victoria
Economic Commentaries 9(2): 27-34.</mixed-citation>
         </ref>
         <ref id="d568e319a1310">
            <mixed-citation id="d568e323" publication-type="other">
Bertram, G. (1992b), 'Tradeable emission permits and the control of greenhouse
gases', Journal of Development Studies 28(3): 423-446.</mixed-citation>
         </ref>
         <ref id="d568e334a1310">
            <mixed-citation id="d568e338" publication-type="other">
Böhm, P. (1992), 'Distributional implications of allowing international trade in C02
emission quotas', The World Economy 15(1): 107-114.</mixed-citation>
         </ref>
         <ref id="d568e348a1310">
            <mixed-citation id="d568e352" publication-type="other">
Dean, A. and P. Hoeller (1992), 'Costs of reducing C02 emissions: evidence from six
global models', OECD Economic Studies 19, OECD Paris.</mixed-citation>
         </ref>
         <ref id="d568e362a1310">
            <mixed-citation id="d568e366" publication-type="other">
Edmonds, J. A., D.W. Barnes and M. Ton (1993), 'Carbon coalitions -the cost and
effectiveness of energy agreement to alter trajectories of atmospheric carbon
dioxide emissions', mimeo, Pacific Northwest Laboratories, Washington, DC.</mixed-citation>
         </ref>
         <ref id="d568e379a1310">
            <mixed-citation id="d568e383" publication-type="other">
Grubb, M. (1989), The Greenhouse Effect: Negotiating Targets. London: Royal Institute
of International Affairs.</mixed-citation>
         </ref>
         <ref id="d568e393a1310">
            <mixed-citation id="d568e397" publication-type="other">
Grubb, M., J. Edmonds, P. Ten Brink and M. Morrison (1993), 'The costs of limiting
fossil-fuel C02 emissions: a survey and analysis', Annual Review of Energy and the
Environment 18: 397-478.</mixed-citation>
         </ref>
         <ref id="d568e410a1310">
            <mixed-citation id="d568e414" publication-type="other">
Hourcade, J.C., K. Halsnaes, M. Jaccard, D. Montgomery, R. Richels, J. Robinson,
P.R. Shukla and P. Sturm (1996), 'A review of mitigation cost studies',
in IPCC, Climate Change 1995: Economic and Social Dimensions of Climate Change -
Contribution of Working Group III to the Second Assessment Report of the
Intergovernmental Panel on Climate Change, Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d568e434a1310">
            <mixed-citation id="d568e438" publication-type="other">
Kosobud, R.F., T.A. Daly, D.W. South and K.G. Quinn (1994), 'Tradable cumulative
C02 permits and global warming control', The Energy Journal 15(2): 213-232.</mixed-citation>
         </ref>
         <ref id="d568e448a1310">
            <mixed-citation id="d568e452" publication-type="other">
Larsen, B. and A. Shah (1994), 'Global tradeable carbon permits, participation in-
centives and transfers', Oxford Economic Papers 46: 841-856.</mixed-citation>
         </ref>
         <ref id="d568e462a1310">
            <mixed-citation id="d568e466" publication-type="other">
Pindyck, R. and A. Dixit (1994), Investment Under Uncertainty, Princeton University
Press.</mixed-citation>
         </ref>
         <ref id="d568e476a1310">
            <mixed-citation id="d568e480" publication-type="other">
Sandler, T. and K. Sargent (1995), 'Management of transnational commons: coordi-
nation, publicness, and treaty formation', Land Economics 71(2): 145-162.</mixed-citation>
         </ref>
         <ref id="d568e490a1310">
            <mixed-citation id="d568e494" publication-type="other">
Schumpeter, J.A. (1934), The Theory of Economic Development, Cambridge, MA:
Harvard University Press.</mixed-citation>
         </ref>
         <ref id="d568e504a1310">
            <mixed-citation id="d568e508" publication-type="other">
Sinclair, P.N. (1994), 'On the optimum trend of fossil fuel taxation', Oxford Economic
Papers 46: 869-877.</mixed-citation>
         </ref>
         <ref id="d568e519a1310">
            <mixed-citation id="d568e523" publication-type="other">
Stavins, R.N. (1995), 'Transaction costs and tradeable permits', Journal of
Environment Economics and Management 29(2): 133-148.</mixed-citation>
         </ref>
         <ref id="d568e533a1310">
            <mixed-citation id="d568e537" publication-type="other">
Ulph, A. and D. Ulph (1994), 'The optimal time path of a carbon tax', Oxford
Economic Papers 46: 857-868.</mixed-citation>
         </ref>
         <ref id="d568e547a1310">
            <mixed-citation id="d568e551" publication-type="other">
World Resources Institute (1994), World Resources 1994r-95, Oxford University Press.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
