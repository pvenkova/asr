<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1hd18zq</book-id>
      <subj-group>
         <subject content-type="call-number">PN3377.5.R45H38 2016</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Reportage literature</subject>
         <subj-group>
            <subject content-type="lcsh">History and criticism</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Journalism and literature</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Creative nonfiction</subject>
         <subj-group>
            <subject content-type="lcsh">History and criticism</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Reportage literature, American</subject>
         <subj-group>
            <subject content-type="lcsh">History and criticism</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Language &amp; Literature</subject>
         <subject>Sociology</subject>
      </subj-group>
      <book-title-group>
         <book-title>Literary Journalism and the Aesthetics of Experience</book-title>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Hartsock</surname>
               <given-names>John C.</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>15</day>
         <month>11</month>
         <year>2016</year>
      </pub-date>
      <isbn content-type="ppub">9781625341730</isbn>
      <isbn content-type="epub">9781613763728</isbn>
      <isbn content-type="epub">1613763727</isbn>
      <publisher>
         <publisher-name>University of Massachusetts Press</publisher-name>
         <publisher-loc>Amherst; Boston</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2016</copyright-year>
         <copyright-holder>University of Massachusetts Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt1hd18zq"/>
      <abstract abstract-type="short">
         <p>Proponents and practitioners of narrative literary journalism have sought to assert its distinctiveness as both a literary form and a type of journalism. In Literary Journalism and the Aesthetics of Experience, John C. Hartsock argues that this often neglected kind of journalism -- exemplified by such renowned works as John Hersey's Hiroshima, James Agee's Let Us Now Praise Famous Men, and Joan Didion's Slouching Towards Bethlehem -- has emerged as an important genre of its own, not just a hybrid of the techniques of fiction and the conventions of traditional journalism.</p>
         <p>Hartsock situates narrative literary journalism within the broader histories of the American tradition of "objective" journalism and the standard novel. While all embrace the value of narrative, or storytelling, literary journalism offers a particular "aesthetics of experience" lacking in both the others. Not only does literary journalism disrupt the myths sustained by conventional journalism and the novel, but its rich details and attention to everyday life question readers' cultural assumptions. Drawing on the critical theories of Nietzsche, Bakhtin, Benjamin, and others, Hartsock argues that the aesthetics of experience challenge the shibboleths that often obscure the realities the other two forms seek to convey.</p>
         <p>At a time when print media appear in decline, Hartsock offers a thoughtful response to those who ask, "What place if any is there for a narrative literary journalism in a rapidly changing media world?"</p>
      </abstract>
      <counts>
         <page-count count="208"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hd18zq.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hd18zq.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hd18zq.3</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hd18zq.4</book-part-id>
                  <title-group>
                     <title>Introduction</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>One cannot help but be struck by the magnitude of the grisly destruction at Verdun in 1916. What comes to my mind are the black-and-white photos in the aftermath of the battle, photos of collections of bones—heavy femurs like kettledrum mallets, skulls laughing with a few teeth, one skull still topped with thick, tousled hair, as well as shards of bones unaccountable in their provenance. and they remind me of the idealized ambitions humans construct to impose on their lives, no matter the era: to have one’s place in the sun, for example, as the Kaiser so earnestly desired</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hd18zq.5</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Telling News Naturally</title>
                  </title-group>
                  <fpage>9</fpage>
                  <abstract>
                     <p>In the opening to Jon Franklin’s pulitzer prize–winning 1978 article about death on an operating room table—an article that has taken on all but canonical status in discussions of narrative literary journalism in newspapers—Franklin selects his details with a discriminating and revealing precision: “in the cold hours of a winter morning, Dr. Thomas Barbee Ducker, university hospital’s senior brain surgeon, rises before dawn. his wife serves him waffles but no coffee. coffee makes his hands shake.”¹</p>
                     <p>Clearly the “lead,” as it is conventionally called in journalism, reveals the inauguration of a narrative story in the traditional sense</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hd18zq.6</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>Telling the Leaves from the Forest</title>
                  </title-group>
                  <fpage>24</fpage>
                  <abstract>
                     <p>In 1927 Ernest Hemingway wrote a passage describing a motoring tour he took in italy, this as part of an article that was to appear in the<italic>New Republic</italic>later in the year:</p>
                     <p>A big car passed us, going fast, and a sheet of muddy water rose up and over our windshield and radiator. The automatic windshield cleaner moved back and forth, spreading the film over the glass. We stopped and ate lunch at sestri. There was no heat in the restaurant and we kept our hats and coats on. We could see the car outside, through the window. It</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hd18zq.7</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>The Death of the Dream of Paradise</title>
                  </title-group>
                  <fpage>60</fpage>
                  <abstract>
                     <p>To what end, then, a narrative literary journalism, narra-descriptive in its modalities? in the last chapter James agee provided an intimation when he focused on what existed outside the boundaries of “myth.” similarly, consider the following: in the opening chapter of tom Wolfe’s<italic>The Right Stuff</italic>, an account of the origins of america’s space program, there is a scene in which a pilot bails out of his malfunctioning jet and his parachute fails to open. as onlookers watch from the ground, the world rises up to “smash him.”</p>
                     <p>“When they lifted his body up off the concrete, it was like</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hd18zq.8</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>The “Elasticity” of Literary Reportage</title>
                  </title-group>
                  <fpage>82</fpage>
                  <abstract>
                     <p>As journalists, Svetlana Alexievich and Anna Politkovskaya have long puzzled me. Both have been described as writers of “literary reportage” and its variant “reportage literature.” Yet they pose a riddle in genre studies because their journalistic work is strikingly different. Alexievich’s work is one, I would suggest, in which the narrative and descriptive modalities predominate, more in keeping with the american tradition of a narrative literary journalism with which i am familiar (which is not to say that somehow she is more American, or influenced by the american form; I have no reason to believe so). Indeed, one American anthology</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hd18zq.9</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Negotiating Cultural and Personal Revelation</title>
                  </title-group>
                  <fpage>124</fpage>
                  <abstract>
                     <p>Alexandra Fuller’s<italic>Scribbling the Cat: Travels with an African Soldier</italic>is a slender and ambitious volume, but at the same time a troubling one as the author attempts to navigate back and forth between a narrative literary journalism and what conventionally we would call memoir.¹ Thus she challenges our preconceptions of what constitutes genre. and one can legitimately ask the question: is there any real difference between narrative literary journalism and memoir if both are fundamentally narra-descriptive, or chronotopic, in their referentiality to a phenomenal world? moreover, one of the hallmarks of both a narrative literary journalism and memoir is</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hd18zq.10</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>When Nature Abhors a Vacuum</title>
                  </title-group>
                  <fpage>137</fpage>
                  <abstract>
                     <p>There is a descriptive passage in Adrian Nicole<italic>LeBlanc’s Random Family: Love, Drugs, Trouble, and Coming of Age in the Bronx</italic>in which Coco and Iris, two poor Latinas, “skimmed the tattoo sketchbooks like two girls flipping through fashion magazines.”¹ Thus the brief descriptive gestures of language reflect through irony the limited desires and ambitions of these urban, marginalized poor. While high fashion for some might be the couturier culture of Paris or Milan, here it is a tattoo. in the open-ended present, we can detect the familiar made unfamiliar.</p>
                     <p>And yet the passage also poses a problem precisely because</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hd18zq.11</book-part-id>
                  <title-group>
                     <title>Conclusion</title>
                  </title-group>
                  <fpage>149</fpage>
                  <abstract>
                     <p>This volume began as a series of independent queries regarding the nature and identity of what is often called literary journalism, narrative in disposition, which I prefer to characterize as a narrative literary journalism, or a narra-descriptive journalism. For that matter, it could be characterized as a chronotopic journalism. To conclude, I will provide a summary of the issues involved. But, I also want to consider what my findings could mean, very tentatively, for the future. This is because no one would dispute the changing nature of our media as they have moved toward convergence in cyberspace. Moreover, what are</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hd18zq.12</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>159</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hd18zq.13</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>183</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hd18zq.14</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>196</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
