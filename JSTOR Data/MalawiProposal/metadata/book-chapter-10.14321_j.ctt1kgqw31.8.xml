<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt17mcrp8</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1kgqw31</book-id>
      <subj-group>
         <subject content-type="call-number">BV3530.P75 2017</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Universities’ Mission to Central Africa</subject>
         <subj-group>
            <subject content-type="lcsh">History</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Anglican Communion</subject>
         <subj-group>
            <subject content-type="lcsh">Missions</subject>
            <subj-group>
               <subject content-type="lcsh">Africa, East</subject>
               <subj-group>
                  <subject content-type="lcsh">History</subject>
               </subj-group>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Christian women</subject>
         <subj-group>
            <subject content-type="lcsh">Africa, East</subject>
            <subj-group>
               <subject content-type="lcsh">Social conditions</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Christian women</subject>
         <subj-group>
            <subject content-type="lcsh">Religious life</subject>
            <subj-group>
               <subject content-type="lcsh">Africa, East</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>History</subject>
         <subject>Religion</subject>
      </subj-group>
      <book-title-group>
         <book-title>Sisters in Spirit</book-title>
         <subtitle>Christianity, Affect, and Community Building in East Africa, 1860–1970</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Prichard</surname>
               <given-names>Andreana C.</given-names>
            </name>
         </contrib>
         <contrib contrib-type="series-editor" id="contrib2">
            <role>SERIES EDITOR</role>
            <name name-style="western">
               <surname>Alegi</surname>
               <given-names>Peter</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>01</day>
         <month>05</month>
         <year>2017</year>
      </pub-date>
      <isbn content-type="ppub">9781611862409</isbn>
      <isbn content-type="epub">9781609175221</isbn>
      <publisher>
         <publisher-name>Michigan State University Press</publisher-name>
         <publisher-loc>East Lansing</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2017</copyright-year>
         <copyright-holder>Andreana C. Prichard</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.14321/j.ctt1kgqw31"/>
      <abstract abstract-type="short">
         <p>In this pioneering study, historian Andreana Prichard presents an intimate history of a single mission organization, the Universities' Mission to Central Africa (UMCA), told through the rich personal stories of a group of female African lay evangelists. Founded by British Anglican missionaries in the 1860s, the UMCA worked among refugees from the Indian Ocean slave trade on Zanzibar and among disparate communities on the adjacent Tanzanian mainland. Prichard illustrates how the mission's unique theology and the demographics of its adherents produced cohorts of African Christian women who, in the face of linguistic and cultural dissimilarity, used the daily performance of a certain set of "civilized" Christian values and affective relationships to evangelize to new inquirers. The UMCA's "sisters in spirit" ultimately forged a united spiritual community that spanned discontiguous mission stations across Tanzania and Zanzibar, incorporated diverse ethnolinguistic communities, and transcended generations. Focusing on the emotional and personal dimensions of their lives and on the relationships of affective spirituality that grew up among them, Prichard tells stories that are vital to our understanding of Tanzanian history, the history of religion and Christian missions in Africa, the development of cultural nationalisms, and the intellectual histories of African women.</p>
      </abstract>
      <counts>
         <page-count count="360"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1kgqw31.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1kgqw31.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1kgqw31.3</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1kgqw31.4</book-part-id>
                  <title-group>
                     <title>Introduction</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>In January 1898, Beatrice Muyororo and Petro Kilekwa were married in a small but festive ceremony on the grounds of the Universities’ Mission to Central Africa’s (UMCA) Mbweni station on the East African archipelago of Zanzibar. The wedding took place in St. Mary’s Chapel, a quaint building with a rounded, beehive-like brick nave and a three-story bell tower. Covered in a fragrant bramble of wisteria, bougainvillea, and honeysuckle vines, perpetually in bloom thanks to the tropical climate, the chapel was but several hundred meters from the rugged white coral coastline—this made it a breathtaking setting for the young couple’s</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1kgqw31.5</book-part-id>
                  <title-group>
                     <label>CHAPTER ONE</label>
                     <title>Tractarian Beginnings:</title>
                     <subtitle>Theology and Society in Britain and East Africa, 1830–1865</subtitle>
                  </title-group>
                  <fpage>29</fpage>
                  <abstract>
                     <p>At forty-four years old, Dr. David Livingstone had spent the past fifteen years traversing nearly one-third of Africa and working to combat the slave trade by bringing news of the Gospel to the “dark continent,” all while documenting its people, flora, fauna, and geography. He wrote home about his adventures, and British newspapers and church organizations published his edited field notes to rave reviews. Britain in the 1850s was a milieu given over to exploration and improvement, and Africa, seen by many as exotic, pagan, and deadly, as well as debased by slavery, Islam, and a host of other troubling</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1kgqw31.6</book-part-id>
                  <title-group>
                     <label>CHAPTER TWO</label>
                     <title>From Slaves to Christian Mothers:</title>
                     <subtitle>Developing a Doctrine of Female Evangelism, 1863–1877</subtitle>
                  </title-group>
                  <fpage>57</fpage>
                  <abstract>
                     <p>When the HMS<italic>Wasp</italic>docked in the late Seychellois spring of 1865, the young Kate Kadamweli was among the nine girls who disembarked into the waiting arms of the UMCA’s new British lady missionaries. Kate’s disembarkation from the<italic>Wasp</italic>was the start of fifty-three years as a congregant with the UMCA, during which time she worked as an assistant teacher, trained as a nurse and tended to patients in the mission’s hospital at Mkunazini, married, bore children, and mentored countless inquirers. Over her long career with the mission, Kate turned to the church to weather abandonment by her husband (also</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1kgqw31.7</book-part-id>
                  <title-group>
                     <label>CHAPTER THREE</label>
                     <title>Industrials and Schoolgirls:</title>
                     <subtitle>Bonds of Personal Dependency and the Mbweni Girls’ School, 1877–1890</subtitle>
                  </title-group>
                  <fpage>81</fpage>
                  <abstract>
                     <p>The “asylum” for freed slaves at the UMCA’s Mbweni<italic>shamba</italic>grew much more quickly than missionaries had either imagined or intended. Between 1875 and 1880, the population of the<italic>shamba</italic>nearly doubled in size from 150 people to 260—the latter a number that did not even include schoolchildren. In 1881 alone, the mission received more than 155 slaves from British anti-slave cruisers.¹ These numbers, which were far higher than missionaries felt they were equipped to handle, were due in large part to the change in the East African export market, and to the ironic results of the string of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1kgqw31.8</book-part-id>
                  <title-group>
                     <label>CHAPTER FOUR</label>
                     <title>Networks of Affective Spirituality:</title>
                     <subtitle>Evangelism and Expansion, 1890–1930</subtitle>
                  </title-group>
                  <fpage>115</fpage>
                  <abstract>
                     <p>In August 1894 Mwalimu Blandina Mwanbwanaa and her new husband, the Reverend Petro “Peter” Limo, departed Zanzibar for their mainland posting. In lieu of a honeymoon, the Limos headed to Peter’s childhood home of Magila, in a region of northeastern Tanzania that lies just east of the Usambara mountains and close to, but not on, the coast.¹ The son of a local Kilindi leader, Peter lost his father during an uprising when he was just a boy. The elder Limo’s death and the complexities of local politics spurred Peter to affiliate with the church, and allowed him to ascend quickly</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1kgqw31.9</book-part-id>
                  <title-group>
                     <label>CHAPTER FIVE</label>
                     <title>Of Marriages and Mimbas:</title>
                     <subtitle>Minding the Borders of the Christian Community, 1910–1930</subtitle>
                  </title-group>
                  <fpage>151</fpage>
                  <abstract>
                     <p>By 1910 a multigenerational network of Mbweni graduates, their daughters, their female protégées, and mainland-trained preachers’ wives and teachers connected UMCA stations in Tanganyika, Zanzibar, and Malawi. Despite missionaries’ repeated declarations that adopting Christianity would not require the inquirer to make “any very great outward change in his daily life,” the examples of the female lay evangelists suggested to new congregants that there were certain habits and behaviors that would define them as Christians and distinguish them from their “heathen” neighbors.¹ Thus even in the most rural of these communities, it was becoming relatively easy to spot a Christian. African</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1kgqw31.10</book-part-id>
                  <title-group>
                     <label>CHAPTER SIX</label>
                     <title>I Am a Spiritual Mother:</title>
                     <subtitle>Affect, Celibacy, and Social Reproduction, 1920–1970</subtitle>
                  </title-group>
                  <fpage>181</fpage>
                  <abstract>
                     <p>In 1926, Stella Mwenyipembe declared her intention to join six British nuns as the first “little sister” of the UMCA’s religious order for women. As a novice at the Community of the Sacred Passion (CSP) convent in Magila, a central station in northeastern Tanzania, Stella would have been tasked—in addition to studying for Holy Orders—with working with some of the 180 pupils in the girls’ school, in one of the local hospitals, or with local women in the district.¹ This work would have been something with which Stella was well acquainted, having grown up around the CSP Sisters</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1kgqw31.11</book-part-id>
                  <title-group>
                     <label>CHAPTER SEVEN</label>
                     <title>The Intimacies of National Belonging:</title>
                     <subtitle>Community Building in Post-Independence Tanzania, 1960–1970</subtitle>
                  </title-group>
                  <fpage>211</fpage>
                  <abstract>
                     <p>After independence, questions of community building and concerns about the nature and boundaries of the spiritual community remained an important consideration in the self-identification of many UMCA-educated individuals. But like many of their compatriots, they were also engaged in the project of defining how best to move forward as an independent nation, and of how best to knit together the various strands of anticolonial nationalism, ideas about national belonging, and values of the affective spiritual community. Deviating from the ideals of racial purity and virtue that underwrote TANU in the 1950s, President Nyerere brought with him to office a vision</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1kgqw31.12</book-part-id>
                  <title-group>
                     <title>Epilogue</title>
                  </title-group>
                  <fpage>235</fpage>
                  <abstract>
                     <p>On a dry day at the start of the long rains of 2008, just before the road from Tanga became impassible, my research assistant Tom and I set out to make what was becoming a familiar drive to the village of Maramba. Together with the neighboring villages of Kigongoi and Magila, Maramba hosted bustling UMCA mission stations—which included churches, schools, a convent, and health centers—for approximately a century, until the formal UMCA mission organization pulled out of Tanzania in the 1960s. The history of the UMCA in the area dates back to the Reverend C. A. Arlington’s first</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1kgqw31.13</book-part-id>
                  <title-group>
                     <title>A Note on Sources</title>
                  </title-group>
                  <fpage>245</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1kgqw31.14</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>251</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1kgqw31.15</book-part-id>
                  <title-group>
                     <title>Bibliography</title>
                  </title-group>
                  <fpage>305</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1kgqw31.16</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>331</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
