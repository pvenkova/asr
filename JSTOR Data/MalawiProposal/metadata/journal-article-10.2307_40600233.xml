<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">jsoutafristud</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100641</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Journal of Southern African Studies</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Routledge, Taylor &amp; Francis Group</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">03057070</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">14653893</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">40600233</article-id>
         <article-categories>
            <subj-group>
               <subject>The Limits of Colonial Control and Postcolonial Success in Botswana</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Accounting for the African Growth Miracle: The Official Evidence - Botswana 1965-1995</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Morten</given-names>
                  <surname>Jerven</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>3</month>
            <year>2010</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">36</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40026229</issue-id>
         <fpage>73</fpage>
         <lpage>94</lpage>
         <permissions>
            <copyright-statement>© 2010 The Editorial Board of the Journal of Southern African Studies</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:href="http://dx.doi.org/10.1080/03057071003607337"
                   xlink:title="an external site"/>
         <abstract>
            <p>Botswana has figured widely as the exceptional African growth success story and has been frequently cited in scholarship that supports the view that African and other less developed economies are capable of rapid economic growth as long as the internal institutional framework and development policies are right. A shortcoming of the literature on African economic performance to date is that it has focused on the aggregate average growth rate, and has not taken the quality of the growth evidence into consideration. This article makes use of the official growth evidence taken from the published national accounts in Botswana to establish that there is reason to doubt the accuracy of the growth evidence on Botswana. It shows how the first decade of growth in particular is seriously biased upwards. After the official evidence and the national accounting methodologies have been analysed, several revisions of the African growth miracle become necessary. The ' policy' accounts have largely been informed by observing the recorded aggregate growth rate and have attributed the rapid growth to stylised facts about policies and institutions. A consideration of disaggregated growth rates allows a discussion of the causal coherence of the dominant explanations of rapid growth in Botswana, in particular, and in the divergent fortunes in the developing world, in general. This article argues that the growth miracle can only directly be attributed to economic policy if (good policy f is defined as the absence of very bad policies.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d633e217a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d633e224" publication-type="other">
W. Easterly and R. Levine, 'Africa's Growth Tragedy: Policies and Ethnic Divisions', Quarterly Journal of
Economics, 112, 4 (1997), pp. 1,203-50.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e234a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d633e241" publication-type="other">
A.I. Samatar, An African Miracle: State and Class Leadership and Colonial Legacy in Botswana (Portsmouth,
NH, Heinemann, 1999).</mixed-citation>
            </p>
         </fn>
         <fn id="d633e251a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d633e258" publication-type="other">
B.J. Ndulu, S.A. O'Connell, J.P. Azam, R.H. Bates, A.K. Fosu, J.W. Gunning and D. Njinkeu (eds), The Political
Economic of Growth in Africa 1960-2000 (Cambridge, Cambridge University Press, 2008).</mixed-citation>
            </p>
         </fn>
         <fn id="d633e268a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d633e275" publication-type="other">
P. Collier, The Bottom Billion: Why the Poorest Countries Are Failing and What Can Be Done About It (New
York, Oxford University Press, 2007), p. 50.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e286a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d633e293" publication-type="other">
D. Acemoglu, S. Johnson and J.A. Robinson, 'An African Success Story: Botswana', in D. Rodrik (ed.), In
Search of Prosperity: Analytic Narratives on Economic Growth (Princeton NJ, Princeton University Press, 2003),
p. 83.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e306a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d633e313" publication-type="other">
E. Hillbom, 'Diamonds or Development?
A Structural Assessment of Botswana's Forty Years of Success', Journal of Modern African Studies, 46, 2
(2008), pp. 191-214.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e326a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d633e333" publication-type="other">
National Accounts 1964 to 1968, p. 1.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e340a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d633e347" publication-type="other">
National Accounts 1967/68, p. 1.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e354a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d633e361" publication-type="other">
National Accounts 1968/69, d. 1.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e368a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d633e375" publication-type="other">
National Accounts of Botswana 1971/72, p. 4-11.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e383a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d633e390" publication-type="other">
National Accounts of Botswana 1971/72, pp. 1-2.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e397a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d633e404" publication-type="other">
Ibid., pp. 1-3.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e411a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d633e418" publication-type="other">
Ibid., p. 5-1.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e425a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d633e432" publication-type="other">
Ibid., p. 7-1.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e439a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d633e446" publication-type="other">
Ibid., p. 6-2.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e453a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d633e460" publication-type="other">
National Accounts of Botswana 1973/74, pp. 1-4.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e468a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d633e475" publication-type="other">
National Accounts of Botswana 1971/72, pp. 1-3.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e482a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d633e489" publication-type="other">
National Accounts of Botswana 1973/74, pp. 1-4.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e496a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d633e503" publication-type="other">
C. Harvey and S.R. Lewis, Policy Choice and Development Performance in Botswana (London, Macmillan,
1989), p. 110.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e513a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d633e520" publication-type="other">
National Accounts of Botswana 1975/76: 2-1-5.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e527a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d633e534" publication-type="other">
National Accounts of Botswana 1986/87: 2.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e541a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d633e548" publication-type="other">
Harvey and Lewis, Policy Choice and Development Performance in Botswana, p. 110.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e556a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d633e563" publication-type="other">
Acemoslu et al.. 'An African Success Storv' d. 83.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e570a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d633e577" publication-type="other">
G.S. Maipose and T.C. Matsheka, 'The Indigenous Development State and Growth in Botswana', in Ndulu, et al.
(eds), The Political Economic of Growth in Africa 1960-2000, p. 511.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e587a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d633e594" publication-type="other">
Ibid., p. 535.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e601a1310">
            <label>31</label>
            <p>
               <mixed-citation id="d633e608" publication-type="other">
W. Easterly and R. Levine, 'Africa's Growth Tragedy: Policies and Ethnic Divisions', Quarterly Journal of
Economics, 112, 4 (1997), pp. 1,203-50.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e618a1310">
            <label>32</label>
            <p>
               <mixed-citation id="d633e625" publication-type="other">
J.C. Leith, Why Botswana Prospered? (Montreal, McGill-Queen's University Press, 2005), pp. 29-30.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e632a1310">
            <label>33</label>
            <p>
               <mixed-citation id="d633e639" publication-type="other">
H. Stein, Beyond the World
Bank Agenda: An Institutional Approach to Development (Chicago, IL, University of Chicago Press, 2008),
pp. 76-81.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e653a1310">
            <label>34</label>
            <p>
               <mixed-citation id="d633e660" publication-type="other">
K. Good, Diamonds, Dispossession &amp; Democracy in Botswana (Oxford, James Currey, 2008).</mixed-citation>
            </p>
         </fn>
         <fn id="d633e667a1310">
            <label>35</label>
            <p>
               <mixed-citation id="d633e674" publication-type="other">
D. Acemoglu, S.H. Johnson, and J.A. Robinson, 'The Colonial Origins of Comparative Development: An
Empirical Investigation', American Economic Review, 91, 5 (2001), pp. 1,369-401.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e684a1310">
            <label>38</label>
            <p>
               <mixed-citation id="d633e691" publication-type="other">
Acemoglu et al, 'An African Success Story'.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e698a1310">
            <label>39</label>
            <p>
               <mixed-citation id="d633e705" publication-type="other">
Ibid., p. 113.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e712a1310">
            <label>40</label>
            <p>
               <mixed-citation id="d633e719" publication-type="other">
G. Austin, 'The "Reversal of Fortune" Thesis and
the Compression of History: Perspectives from African and Comparative Economic History', Journal of
International Development, 20 (2008), pp. 1-32.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e732a1310">
            <label>41</label>
            <p>
               <mixed-citation id="d633e739" publication-type="other">
P. Peters, Dividing the
Commons: Politics, Policy and Culture in Botswana (Charlottesville, University Press of Virginia, 1994), p. 218.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e750a1310">
            <label>42</label>
            <p>
               <mixed-citation id="d633e757" publication-type="other">
Ibid., p. 223.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e764a1310">
            <label>43</label>
            <p>
               <mixed-citation id="d633e771" publication-type="other">
D. Rodrik, 'Why is Trade Reform so Difficult in Africa?', Journal of African Economies, 7, 1 (1998), pp. 43-69,</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d633e777" publication-type="other">
R.H. Bates, 'Agricultural Policy and the Study of Politics in Post-Independence Africa', pp. 115-29, in
D. Rimmer (ed.), Africa 30 Years On (London, James Currey, 1991).</mixed-citation>
            </p>
         </fn>
         <fn id="d633e787a1310">
            <label>44</label>
            <p>
               <mixed-citation id="d633e794" publication-type="other">
Maipose and Matsheka, 'The Indigenous Development State', p. 518.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e801a1310">
            <label>45</label>
            <p>
               <mixed-citation id="d633e808" publication-type="other">
H.J. Cooke, 'The Problem of Drought in Botswana', in M.T. Hinchey (ed.), Proceedings of the Symposium on
Drought in Botswana (Gaborone, The Botswana Society, 1979), p. 8.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e818a1310">
            <label>46</label>
            <p>
               <mixed-citation id="d633e825" publication-type="other">
D. Jones, 'Arable Agriculture in Botswana: A Case for Subsidies', in D. Harvey (ed.), Papers on the Economy of
Botswana (London, Heinemann, 1981), p. 33.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e835a1310">
            <label>48</label>
            <p>
               <mixed-citation id="d633e842" publication-type="other">
D.B. Jones, 'Drought and Arable Farming', in M.T. Hinchey (ed.), Proceedings of the Symposium on Drought in
Botswana (Gaborone, The Botswana Society, 1979), p. 235.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e853a1310">
            <label>49</label>
            <p>
               <mixed-citation id="d633e860" publication-type="other">
United Nations, A Study of Constraints on Agricultural Development
in the Republic of Botswana (including an assessment of the role of food aid), (Rome, FAO, 1974).</mixed-citation>
            </p>
         </fn>
         <fn id="d633e870a1310">
            <label>50</label>
            <p>
               <mixed-citation id="d633e877" publication-type="other">
Republic of Botswana, Ministry of Agriculture, 1993 Botswana Agricultural Census (Gaborone, Government
Printer, 1995).</mixed-citation>
            </p>
         </fn>
         <fn id="d633e887a1310">
            <label>51</label>
            <p>
               <mixed-citation id="d633e894" publication-type="other">
Peters, Dividing the Commons, p. 218.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e901a1310">
            <label>52</label>
            <p>
               <mixed-citation id="d633e908" publication-type="other">
M. Hubbard, Agricultural Exports and Economic Growth: A Study of the Botswana Beef Industry (London, KPI,
1987V nn. 158-66.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e918a1310">
            <label>53</label>
            <p>
               <mixed-citation id="d633e925" publication-type="other">
Jones, 'Arable Agriculture in Botswana', Papers on the Economy of Botswana p. 34.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e932a1310">
            <label>54</label>
            <p>
               <mixed-citation id="d633e939" publication-type="other">
Maipose and Matsheka, 'The Indigenous Development State', p. 530.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e947a1310">
            <label>55</label>
            <p>
               <mixed-citation id="d633e954" publication-type="other">
K. Jefferis, 'Botswana and Diamond-Dependent Development', in W.A. Edge and M.H. Lekorwe (eds),
Botswana: Politics and Society (Pretoria, J.L. van Schaik, 2008), p. 304.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e964a1310">
            <label>56</label>
            <p>
               <mixed-citation id="d633e971" publication-type="other">
C. Harvey and S.R. Lewis, Policy Choice and Development Performance in Botswana (London, Macmillan,
1989), p. 138.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e981a1310">
            <label>57</label>
            <p>
               <mixed-citation id="d633e988" publication-type="other">
Ibid., p. 138.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e995a1310">
            <label>58</label>
            <p>
               <mixed-citation id="d633e1002" publication-type="other">
Acemoglu et al, 'An African Success Story', p. 113.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e1009a1310">
            <label>59</label>
            <p>
               <mixed-citation id="d633e1016" publication-type="other">
Maipose and Matsheka, 'The Indigenous Development State', p. 536.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e1023a1310">
            <label>60</label>
            <p>
               <mixed-citation id="d633e1030" publication-type="other">
Harvey and Lewis, Policy Choice and Development Performance, p. 221.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e1038a1310">
            <label>61</label>
            <p>
               <mixed-citation id="d633e1045" publication-type="other">
Ibid.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e1052a1310">
            <label>62</label>
            <p>
               <mixed-citation id="d633e1059" publication-type="other">
Maipose and Matsheka, 'The Indigenous Development State', p. 518</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d633e1065" publication-type="other">
D. Rodrik, 'Why is Trade Reform so
Difficult in Africa?', Journal of African Economies, 7, 1 (1998), pp. 43-69.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e1075a1310">
            <label>63</label>
            <p>
               <mixed-citation id="d633e1082" publication-type="other">
Maipose and Matsheka, 'The Indigenous Development State', p. 535.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e1089a1310">
            <label>64</label>
            <p>
               <mixed-citation id="d633e1096" publication-type="other">
Harvey and Lewis, Policy Choice and Development Performance, pp. 166-8.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e1103a1310">
            <label>65</label>
            <p>
               <mixed-citation id="d633e1110" publication-type="other">
B. Tsie, The Political Economy of Botswana in SADCC (Harare, SAPES, 1995), p. 127.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e1117a1310">
            <label>66</label>
            <p>
               <mixed-citation id="d633e1124" publication-type="other">
Harvey and Lewis, Policy Choice and Development Performance, p. 176-7.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e1132a1310">
            <label>67</label>
            <p>
               <mixed-citation id="d633e1139" publication-type="other">
Tsie, The Political Economy of Botswana, p. 124.</mixed-citation>
            </p>
         </fn>
         <fn id="d633e1146a1310">
            <label>68</label>
            <p>
               <mixed-citation id="d633e1153" publication-type="other">
J. Otto et al, Mining Royalties: A Global Study of their Impact on Investors, Government, and Civil Society
(Washington, DC, World Bank, 2006), pp. 82-4 and A1-4.</mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
