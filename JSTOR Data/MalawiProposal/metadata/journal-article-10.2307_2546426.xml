<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">intemigrrevi</journal-id>
         <journal-id journal-id-type="jstor">j100586</journal-id>
         <journal-title-group>
            <journal-title>The International Migration Review</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Center for Migration Studies of New York, Inc.</publisher-name>
         </publisher>
         <issn pub-type="ppub">01979183</issn>
         <issn pub-type="epub">17477379</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="doi">10.2307/2546426</article-id>
         <title-group>
            <article-title>Remittances from Labor Migration: Evaluations, Performance and Implications</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Charles B.</given-names>
                  <surname>Keely</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Bao Nga</given-names>
                  <surname>Tran</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>10</month>
            <year>1989</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">23</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id>i323481</issue-id>
         <fpage>500</fpage>
         <lpage>525</lpage>
         <page-range>500-525</page-range>
         <permissions>
            <copyright-statement>Copyright 1989 Center for Migration Studies of New York, Inc.</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/2546426"/>
         <abstract>
            <p> Two evaluative views of worker remittances draw opposite conclusions. The negative one posits that remittances increase dependency, contribute to economic and political instability and development distortion, and lead to economic decline that over-shadows a temporary advantage for a fortunate few. The positive view sees remittances as an effective response to market forces, providing a transition to an otherwise unsustainable development. They improve income distribution and quality of life beyond what other available development approaches could deliver. The implications are tested for labor supply countries to Europe and to the Middle East. The implications of the negative view are not supported. Although the dire predictions of the pessimistic view have not materialized, the converse -- contributions of remittances to economic performance -- should not be overstated due to lack of data. </p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d1107e184a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1107e191" publication-type="other">
Mahmud and Osmani, 1980.</mixed-citation>
            </p>
         </fn>
         <fn id="d1107e198a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1107e205" publication-type="other">
Jonas Widgren (1988)</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d1107e221a1310">
            <mixed-citation id="d1107e225" publication-type="book">
Ali,SA.
1979 "Home Remittances by Bangladesh Nationals Working Abroad". Dacca (Dakha):
Bangladesh Bank.<person-group>
                  <string-name>
                     <surname>Ali</surname>
                  </string-name>
               </person-group>
               <source>Home Remittances by Bangladesh Nationals Working Abroad</source>
               <year>1979</year>
            </mixed-citation>
         </ref>
         <ref id="d1107e254a1310">
            <mixed-citation id="d1107e258" publication-type="book">
Bach, R.L.
1985 "International Relations, Economic Development and Migration". Prepared for Inter¬
national Migration Workshop, Population Council, NY. Mimeo.<person-group>
                  <string-name>
                     <surname>Bach</surname>
                  </string-name>
               </person-group>
               <source>International Relations, Economic Development and Migration</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d1107e287a1310">
            <mixed-citation id="d1107e291" publication-type="book">
Birks, J.S. and CA. Sinclair
1980International Migration and Development in the Arab Region. Geneva: ILO.<person-group>
                  <string-name>
                     <surname>Birks</surname>
                  </string-name>
               </person-group>
               <source>International Migration and Development in the Arab Region</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
         <ref id="d1107e316a1310">
            <mixed-citation id="d1107e320" publication-type="book">
Chilivumbo, A.
1985Migration and Uneven Rural Development in Africa: The Case of Zambia. Lanham, MD
and London: University Press of America.<person-group>
                  <string-name>
                     <surname>Chilivumbo</surname>
                  </string-name>
               </person-group>
               <source>Migration and Uneven Rural Development in Africa: The Case of Zambia</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d1107e350a1310">
            <mixed-citation id="d1107e354" publication-type="book">
Finkle, J.L. and CA. Mclntosh
1982 "The Consequences of International Migration for Sending Countries in the Third
World". Report for the Bureau of Policy and Program Coordination, USAID,
Washington, D.C.<person-group>
                  <string-name>
                     <surname>Finkle</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The Consequences of International Migration for Sending Countries in the Third World</comment>
               <source>Report for the Bureau of Policy and Program Coordination</source>
               <year>1982</year>
            </mixed-citation>
         </ref>
         <ref id="d1107e389a1310">
            <mixed-citation id="d1107e393" publication-type="book">
Gilani, I., M.F. Khan and M. Iqbal
1981 "Labour Migration from Pakistan to the Middle East and Its Impact on the Domestic
Economy". Final Report. Research Project on Export of Manpower from Pakistan to
the Middle East. Washington, D.C: World Bank.<person-group>
                  <string-name>
                     <surname>Gilani</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Labour Migration from Pakistan to the Middle East and Its Impact on the Domestic Economy</comment>
               <source>Final Report. Research Project on Export of Manpower from Pakistan to the Middle East</source>
               <year>1981</year>
            </mixed-citation>
         </ref>
         <ref id="d1107e428a1310">
            <mixed-citation id="d1107e432" publication-type="book">
International Monetary Fund
1987International Financial Statistics: 1987 Yearbook. Washington, D.C: IMF.<person-group>
                  <string-name>
                     <surname>International Monetary Fund</surname>
                  </string-name>
               </person-group>
               <source>International Financial Statistics: 1987 Yearbook</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d1107e457a1310">
            <mixed-citation id="d1107e461" publication-type="journal">
Keely, C.B. and B. Saket
1984 "Jordanian Migrant Workers in the Arab Region: A Case Study of Consequences for
Labor Supplying Countries", The Middle East Journal, 38(4): 685-698. Autumn.<person-group>
                  <string-name>
                     <surname>Keely</surname>
                  </string-name>
               </person-group>
               <issue>4</issue>
               <fpage>685</fpage>
               <volume>38</volume>
               <source>The Middle East Journal</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d1107e499a1310">
            <mixed-citation id="d1107e503" publication-type="book">
Kritz, M.M. and C.B. Keely
1981 "Introduction". In Global Trends in Migration. Edited by M.M. Kritz, C.B. Keely and
S.M. Tomasi. Staten Island: Center for Migration Studies. Pp. xiii-xxxi.<person-group>
                  <string-name>
                     <surname>Kritz</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Introduction</comment>
               <fpage>xiii</fpage>
               <source>Global Trends in Migration</source>
               <year>1981</year>
            </mixed-citation>
         </ref>
         <ref id="d1107e538a1310">
            <mixed-citation id="d1107e542" publication-type="journal">
Mahmud, H. and S.R. Osmani
1980 "Impact of Emigrant Workers' Remittances on the Bangladesh Economy", The
Bangladesh Development Studies, 8(3): 1-28. Monsoon.<person-group>
                  <string-name>
                     <surname>Mahmud</surname>
                  </string-name>
               </person-group>
               <issue>3</issue>
               <fpage>1</fpage>
               <volume>8</volume>
               <source>The Bangladesh Development Studies</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
         <ref id="d1107e581a1310">
            <mixed-citation id="d1107e585" publication-type="book">
Rhoda, R., S. Binnendijk, with CK. Levinson
1979 "Near East Labor Migration: Implications and Policy". Conference Proceedings.
Washington, D.C: USAID.<person-group>
                  <string-name>
                     <surname>Rhoda</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Near East Labor Migration: Implications and Policy</comment>
               <source>Conference Proceedings</source>
               <year>1979</year>
            </mixed-citation>
         </ref>
         <ref id="d1107e617a1310">
            <mixed-citation id="d1107e621" publication-type="journal">
Russell, S.S.
1986 "Remittances from International Migration: A Review in Perspective", World
Development, 14(6):677-696.<person-group>
                  <string-name>
                     <surname>Russell</surname>
                  </string-name>
               </person-group>
               <issue>6</issue>
               <fpage>677</fpage>
               <volume>14</volume>
               <source>World Development</source>
               <year>1986</year>
            </mixed-citation>
         </ref>
         <ref id="d1107e659a1310">
            <mixed-citation id="d1107e663" publication-type="book">
Stahl, C.W.
1982 "International Labour Migration and International Development". International
Migration for Employment Branch Working Paper. Geneva: ILO.<person-group>
                  <string-name>
                     <surname>Stahl</surname>
                  </string-name>
               </person-group>
               <source>International Labour Migration and International Development</source>
               <year>1982</year>
            </mixed-citation>
         </ref>
         <ref id="d1107e692a1310">
            <mixed-citation id="d1107e696" publication-type="book">
Swamy, G.
1981 "International Migrant Workers' Remittances: Issues and Prospects". World Bank
Staff Working Paper No. 481. Washington, D.C: World Bank.<person-group>
                  <string-name>
                     <surname>Swamy</surname>
                  </string-name>
               </person-group>
               <source>International Migrant Workers' Remittances: Issues and Prospects</source>
               <year>1981</year>
            </mixed-citation>
         </ref>
         <ref id="d1107e725a1310">
            <mixed-citation id="d1107e729" publication-type="other">
Widgren, J.
1988 Reaction to "Remittances from Labor Migration".Unpublished comments at the
Bellagio Conference on International Migration.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
