<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">revinterstud</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000149</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Review of International Studies</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Cambridge University Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">02602105</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">14699044</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">41308461</article-id>
         <article-categories>
            <subj-group>
               <subject>Special section on "Uncertainty and responsibility"</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>The economic-institutional construction of regions: conceptualisation and operationalisation</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>KATHY</given-names>
                  <surname>POWERS</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>GARY</given-names>
                  <surname>GOERTZ</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>12</month>
            <year>2011</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">37</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">5</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40059146</issue-id>
         <fpage>2387</fpage>
         <lpage>2416</lpage>
         <permissions>
            <copyright-statement>© British International Studies Association 2011</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:href="http://dx.doi.org/10.1017/S0260210510001762"
                   xlink:title="an external site"/>
         <abstract>
            <p>The international relations literature on regionalism, both in economic and security issues, has grown dramatically over the last 15 years. One of the ongoing issues discussed in most articles and books is the conceptualisation of 'region'. Instead of thinking about regions using notions of interdependence and interaction we take a social constructivist approach, whereby states themselves define regions via the construction of regional economic institutions (REI). We explore how a conceptualisation of region based on REIs contrasts with various related concepts such as regional system, and regional IGO. Empirically, we show that most all countries belong to at least one important regional economic institution, REI, (for example, EU, Mercosur, ASEAN, etc). In short, the world is dividing itself into regions by the creation of regional economic institutions. We contrast our economic-institutional approach to regions with Buzan and Wæver's 'regional security complexes' which is based on security dependence. There are interesting agreements and disagreements between their approach and our economic-institutional approach to defining regions. It is perhaps not surprising that many REIs have taken on security roles, which we briefly show by looking at military alliances embedded in REIs. This suggests that policymakers are creating regions through institutional innovations that link economic and security issues.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d646e192a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d646e199" publication-type="other">
William R. Thompson, The Regional Subsystem: A Conceptual Explication and a Propositional
Inventory', International Studies Quarterly , 17:1 (1973), pp. 89-117</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d646e208" publication-type="other">
Edward D. Mansfield and Etel Solingen, 'Regionalism', Annual
Review of Political Science, 13 (2010), pp. 145-63.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e218a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d646e225" publication-type="other">
Peter Katzenstein, 'Regionalism in Comparative Perspective', Cooperation and Conflict , 31:2 (1996),
pp. 123-60;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d646e234" publication-type="other">
Peter Katzenstein, A World of Regions: Asia and Europe in the American Imperium
(Ithaca: Cornell University Press, 2005).</mixed-citation>
            </p>
         </fn>
         <fn id="d646e244a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d646e251" publication-type="other">
Jeffrey Frankel, Regional Trading Blocs in the World Economic System (Washington, DC: Institute
for International Economics 1997).</mixed-citation>
            </p>
         </fn>
         <fn id="d646e261a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d646e268" publication-type="other">
Karl Deutsch, et al., Political Community and the North Atlantic Area: International Organization in
the Light of Historical Experience (Princeton: Princeton University Press, 1969).</mixed-citation>
            </p>
         </fn>
         <fn id="d646e279a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d646e286" publication-type="other">
Bruce Russett, International Regions and the International System (Chicago: Rand McNally, 1967).</mixed-citation>
            </p>
         </fn>
         <fn id="d646e293a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d646e300" publication-type="other">
Emanuel Adler and Michael Barnett, (eds), Security Communities (Cambridge: Cambridge University
Press, 1998).</mixed-citation>
            </p>
         </fn>
         <fn id="d646e310a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d646e317" publication-type="other">
Rick Fawn (ed.), Globalising the Regional: Regionising the Global (Cambridge: Cambridge University
Press, 2009).</mixed-citation>
            </p>
         </fn>
         <fn id="d646e327a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d646e334" publication-type="other">
Bruce Russett, 'Delineating International Regions', in J. David Singer (ed.), Quantitative Inter-
national Politics: Insights and Evidence (New York: Free Press, 1968), p. 312.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e344a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d646e351" publication-type="other">
Gary Goertz, Social Science Concepts: A User's Guide (Princeton: Princeton University Press, 2005);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d646e357" publication-type="other">
Robert Adcock and David Collier, 'Measurement Validity: A Shared Standard for Qualitative and
Quantitative Research', American Political Science Review, 95:3 (2001), pp. 529-46.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e367a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d646e374" publication-type="other">
David A. Lake, 'Regional Hierarchy: Authority the Local International Order', Review of
International Studies , 35: SI (2009), pp. 35-58.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e385a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d646e392" publication-type="other">
Deutsch, et al., Political Community and the North Atlantic Area.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e399a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d646e406" publication-type="other">
Barry Buzan and Ole Waever, Regions and Powers: The Structure of International Security
(Cambridge: Cambridge University Press, 2004).</mixed-citation>
            </p>
         </fn>
         <fn id="d646e416a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d646e423" publication-type="other">
Gerardo L. Munck and Jay Verkuilen, 'Conceptualizing and Measuring Democracy: Evaluating
Alternative Indices', Comparative Political Studies , 35:1 (2002), pp. 5-34,</mixed-citation>
            </p>
         </fn>
         <fn id="d646e433a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d646e440" publication-type="other">
Lake, 'Regional Hierarchy'.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e447a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d646e454" publication-type="other">
Buzan and Waever, Regions and Powers.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e461a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d646e468" publication-type="other">
David Collier, Jody LaPorte, and Jason Seawright, 'Typologies: Forming Concepts and Creating
Categorical Variables', in Janet M. Box-Steffensmier, Henry Brady and David Collier (eds), The
Oxford Handbook of Political Methodolosv (Oxford: Oxford Universitv Press. 2008). dd. 152-73.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e482a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d646e489" publication-type="other">
Gary Goertz and James. Mahoney, 'Concept Asymmetry and Non-Mutually Exclusive Typologies',
in A Tale of Two Cultures : Contrasting Qualitative and Quantitative Paradigms (Princeton University
Press, forthcoming).</mixed-citation>
            </p>
         </fn>
         <fn id="d646e502a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d646e509" publication-type="other">
Fawn, Globalising the Regional ; David A. Lake and Patrick M. Morgan (eds), Regional Orders:
Building Security in a New World (University Park: Pennsylvania State University Press, 1997);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d646e518" publication-type="other">
Amitav Acharya and Alastair Iain Johnston (eds), Crafting Cooperation: Regional International
Institutions in Comparative Perspective (Cambridge: Cambridge University Press, 2007).</mixed-citation>
            </p>
         </fn>
         <fn id="d646e528a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d646e535" publication-type="other">
D. Scott Bennett and Allan C. Stam, 'EUGene: A Conceptual Manual', International Interactions ,
26:2 (2000), pp. 179-204.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e545a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d646e552" publication-type="other">
Keith Jaggers and Ted R. Gurr, 'Tracking Democracy's Third Wave with the Polity III Data',
Journal of Peace Research , 32:4 (1995), pp. 469-82.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e562a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d646e569" publication-type="other">
Adcock and Collier, 'Measurement Validity', p. 531.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e576a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d646e583" publication-type="other">
Joseph S. Nye, 'Comparative Regional Integration: Concept and Measurement', International
Organization , 22:4 (1968), p. xii.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e594a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d646e601" publication-type="other">
Thompson, 'The Regional Subsystem', p. 101.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e608a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d646e615" publication-type="other">
Russett, 'Delineating International Regions', p. 318.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e622a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d646e629" publication-type="other">
Russett, International Regions</mixed-citation>
            </p>
         </fn>
         <fn id="d646e636a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d646e643" publication-type="other">
Katzenstein, 'Regionalism in Comparative Perspective', p. 130.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e650a1310">
            <label>31</label>
            <p>
               <mixed-citation id="d646e657" publication-type="other">
David A. Lake, 'Regional Security Complexes: A Systems Approach', in David Lake and Patrick
Morgan (eds), Regional Orders : Building Security in A New World (University Park: Pennsylvania
State University Press, 1997);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d646e669" publication-type="other">
Buzan and Waever, Regions and Powers.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e676a1310">
            <label>32</label>
            <p>
               <mixed-citation id="d646e683" publication-type="other">
Lake, 'Regional Security Complexes'.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e691a1310">
            <label>33</label>
            <p>
               <mixed-citation id="d646e698" publication-type="other">
Frankel, Regional Trading Blocs.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e705a1310">
            <label>34</label>
            <p>
               <mixed-citation id="d646e712" publication-type="other">
Beth V. Yarbrough and Robert M. Yarbrough, Cooperation and Governance in International Trade
(Princeton: Princeton University Press. 1992V</mixed-citation>
            </p>
         </fn>
         <fn id="d646e722a1310">
            <label>35</label>
            <p>
               <mixed-citation id="d646e729" publication-type="other">
Edward D. Mansfield, 'The Proliferation of Preferential Trading Agreements', Journal of Conflict
Resolution , 42:5 (1998), pp. 523-43;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d646e738" publication-type="other">
Carlo Perroni and John Whalley, 'The New Regionalism: Trade
Liberalization or Insurance', National Bureau of Economic Research (Working Paper No. 4626,
Cambridge, MA 1994);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d646e750" publication-type="other">
John E. Whalley, 'Why Do Countries Seek Regional Trade Agreements', in
Jeffrey Frankel, (ed.), The Regionalization of the World Economy (Chicago: University of Chicago
Press, 1998);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d646e763" publication-type="other">
Yarbrough and Yarbrough, Cooperation and Governance in International Trade.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e770a1310">
            <label>36</label>
            <p>
               <mixed-citation id="d646e777" publication-type="other">
Frankel, Regional Trading Blocs.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e784a1310">
            <label>37</label>
            <p>
               <mixed-citation id="d646e791" publication-type="other">
Andrew G. Long and Brett Ashley Leeds, 'Trading Alliances for Security: Military Alliances and
Economic agreements', Journal of Peace Research, 43:4 (2006), pp. 433-51.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e801a1310">
            <label>38</label>
            <p>
               <mixed-citation id="d646e808" publication-type="other">
Goertz, Social Science Concepts .</mixed-citation>
            </p>
         </fn>
         <fn id="d646e816a1310">
            <label>40</label>
            <p>
               <mixed-citation id="d646e823" publication-type="other">
Jon C. Pevehouse, Timothy Nordstrom and Kevin Warnke, 'The Correlates of War 2 International
Governmental Organizations Data Version 2.0', Conflict Management and Peace Science , 21:2 (2004),
pp. 101-20.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e836a1310">
            <label>41</label>
            <p>
               <mixed-citation id="d646e843" publication-type="other">
Kenneth Abbott and Duncan Snidal, 'Why States Act through Formal International Organizations',
Journal of Conflict Resolution , 42:1 CI 998), pp. 3-32.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e853a1310">
            <label>42</label>
            <p>
               <mixed-citation id="d646e860" publication-type="other">
Adcock and Collier, 'Measurement Validity'.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e867a1310">
            <label>43</label>
            <p>
               <mixed-citation id="d646e874" publication-type="other">
Arthur S. Banks and Thomas Mueller, Political Handbook of the World (Binghamton: CSA
Publications, 1988).</mixed-citation>
            </p>
         </fn>
         <fn id="d646e884a1310">
            <label>44</label>
            <p>
               <mixed-citation id="d646e891" publication-type="other">
Jon C. Pevehouse, Democracy from Above: Regional Organizations and Democratization (Cambridge:
Cambridge University Press, 2005).</mixed-citation>
            </p>
         </fn>
         <fn id="d646e901a1310">
            <label>45</label>
            <p>
               <mixed-citation id="d646e908" publication-type="other">
Beckfield, 'The Dual World Polity'.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e916a1310">
            <label>46</label>
            <p>
               <mixed-citation id="d646e923" publication-type="other">
Thomas Volgy, Elizabeth Fausett, Keith A. Grant and Stuart Rodgers, 'Identifying Formal
Intergovermental Organizations', Journal of Peace Research, 45:6 (2008), pp. 849-62,</mixed-citation>
            </p>
         </fn>
         <fn id="d646e933a1310">
            <label>47</label>
            <p>
               <mixed-citation id="d646e940" publication-type="other">
John P. Willerton, et al., A Methodology for Nested Treaties and Institutions: Nested Bilateralism in
the Commonwealth of Independent States (Manuscript, University of Arizona 2009).</mixed-citation>
            </p>
         </fn>
         <fn id="d646e950a1310">
            <label>48</label>
            <p>
               <mixed-citation id="d646e957" publication-type="other">
Mark W. Janis and John E. Noyes, International Law: Cases and Commentary (St. Paul:
Thomson/West, 2006).</mixed-citation>
            </p>
         </fn>
         <fn id="d646e967a1310">
            <label>49</label>
            <p>
               <mixed-citation id="d646e974" publication-type="other">
Kathy Powers, Scraps of paper or signs of 10 autonomy? 10 treaties and international legal personality
(Manuscript. University of New Mexico, 2011).</mixed-citation>
            </p>
         </fn>
         <fn id="d646e984a1310">
            <label>50</label>
            <p>
               <mixed-citation id="d646e991" publication-type="other">
Buzan and Waever, Regions and Powers.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e998a1310">
            <label>51</label>
            <p>
               <mixed-citation id="d646e1005" publication-type="other">
Banks and Mueller. Political Handbook of the World .</mixed-citation>
            </p>
         </fn>
         <fn id="d646e1013a1310">
            <label>52</label>
            <p>
               <mixed-citation id="d646e1020" publication-type="other">
Liesbet Hooghe and Gary Marks, 'Does Efficiency Shape the Territorial Structure of Government?',
Annual Review of Political Science , 12 (2009), pp. 225-41,</mixed-citation>
            </p>
         </fn>
         <fn id="d646e1030a1310">
            <label>53</label>
            <p>
               <mixed-citation id="d646e1037" publication-type="other">
Correlates of War Project (2008), 'State System Membership List, v2008.1', available at:
{http://correlatesofwar.org} .</mixed-citation>
            </p>
         </fn>
         <fn id="d646e1047a1310">
            <label>54</label>
            <p>
               <mixed-citation id="d646e1054" publication-type="other">
Hooghe and Marks, 'Does Efficiency Shape the Territorial Structure of Government? , p. 236.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e1061a1310">
            <label>55</label>
            <p>
               <mixed-citation id="d646e1068" publication-type="other">
Patrick M. Morgan, 'Regional Security Complexes and Regional Orders', in David A. Lake and
Patrick M. Morgan (eds), Regional Orders: Building Security in a New World (University Park:
Pennsylvania State University Press, 1997).</mixed-citation>
            </p>
         </fn>
         <fn id="d646e1081a1310">
            <label>56</label>
            <p>
               <mixed-citation id="d646e1088" publication-type="other">
Thompson, 'The Regional Subsystem'.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e1095a1310">
            <label>57</label>
            <p>
               <mixed-citation id="d646e1102" publication-type="other">
Russett. international Regions ana the international system.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e1110a1310">
            <label>58</label>
            <p>
               <mixed-citation id="d646e1117" publication-type="other">
Emanuel Adler and Patricia Greve, 'When Security Community Meets Balance of Power:
Overlapping Regional Mechanisms of Security Governance', Review of International Studies , 35
(2009), p. 59,</mixed-citation>
            </p>
         </fn>
         <fn id="d646e1130a1310">
            <label>59</label>
            <p>
               <mixed-citation id="d646e1137" publication-type="other">
Solomon Polachek, 'Conflict and Trade', Journal of Conflict Resolution , 24:1 (1980), pp. 55-78.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e1144a1310">
            <label>60</label>
            <p>
               <mixed-citation id="d646e1151" publication-type="other">
Willerton et al.. A Methodolovv for Nested Treaties and Institutions (2009)</mixed-citation>
            </p>
         </fn>
         <fn id="d646e1158a1310">
            <label>61</label>
            <p>
               <mixed-citation id="d646e1165" publication-type="other">
Buzan and Waever, Regions and Powers.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e1172a1310">
            <label>62</label>
            <p>
               <mixed-citation id="d646e1179" publication-type="other">
Douglas Lemke, Regions of War and Peace (Cambridge: Cambridge University Press, 2002).</mixed-citation>
            </p>
         </fn>
         <fn id="d646e1186a1310">
            <label>63</label>
            <p>
               <mixed-citation id="d646e1193" publication-type="other">
Barry Buzan, People, States, and Fear: The National Security Problem in International Relations
(Brighton: Wheatsheaf Books, 1983);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d646e1202" publication-type="other">
Barry Buzan, Ole Waever, and Jaap De Wilde, Security: A New
Framework for Analysis (Boulder: Lvnne Rienner, 1997);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d646e1211" publication-type="other">
Buzan and Waever, Regions and Powers.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e1219a1310">
            <label>64</label>
            <p>
               <mixed-citation id="d646e1226" publication-type="other">
Buzan, People, States, and Fear , p. 106.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e1233a1310">
            <label>65</label>
            <p>
               <mixed-citation id="d646e1240" publication-type="other">
Buzan and Waever, Regions and Powers , p. 44.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e1247a1310">
            <label>66</label>
            <p>
               <mixed-citation id="d646e1254" publication-type="other">
Ibid., p. 45.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e1261a1310">
            <label>67</label>
            <p>
               <mixed-citation id="d646e1268" publication-type="other">
Ibid., p. 46.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e1275a1310">
            <label>68</label>
            <p>
               <mixed-citation id="d646e1282" publication-type="other">
Lake, 'Regional Security Complexes', p. 48.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e1289a1310">
            <label>69</label>
            <p>
               <mixed-citation id="d646e1296" publication-type="other">
Lemke, Regions of War and Peace.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e1304a1310">
            <label>70</label>
            <p>
               <mixed-citation id="d646e1311" publication-type="other">
A. F. K. Organski and Jacek Kugler, The War Ledger f Chicago: University of Chicago Press. 1981)</mixed-citation>
            </p>
         </fn>
         <fn id="d646e1318a1310">
            <label>71</label>
            <p>
               <mixed-citation id="d646e1325" publication-type="other">
Lemke, Regions of War and Peace , p. 69.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e1332a1310">
            <label>73</label>
            <p>
               <mixed-citation id="d646e1339" publication-type="other">
Buzan and Waever, Regions and Powers.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e1346a1310">
            <label>75</label>
            <p>
               <mixed-citation id="d646e1353" publication-type="other">
Brett Ashley Leeds, Jeffrey M. Ritter, Sara McLaughlin Mitchell and Andrew G. Long, 'Alliance
Treaty Obligations and Provisions, 1815-1944', International Interactions , 28:3 (2002), pp. 237-60.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e1363a1310">
            <label>76</label>
            <p>
               <mixed-citation id="d646e1370" publication-type="other">
Powers, 'Dispute Initiation and Alliance Obligations in Regional Economic Institutions'.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e1377a1310">
            <label>78</label>
            <p>
               <mixed-citation id="d646e1384" publication-type="other">
Adler and Barnett, Security Communities.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e1392a1310">
            <label>79</label>
            <p>
               <mixed-citation id="d646e1399" publication-type="other">
James Mahoney and Dietrich Rueschemeyer (eds), Comparative Historical Analysis in the Social
Sciences (Cambridge: Cambridge Universitv Press. 2003).</mixed-citation>
            </p>
         </fn>
         <fn id="d646e1409a1310">
            <label>80</label>
            <p>
               <mixed-citation id="d646e1416" publication-type="other">
Max Weber, The Theory of Social and Economic Organizations (New York: Free Press, 1947).</mixed-citation>
            </p>
         </fn>
         <fn id="d646e1423a1310">
            <label>81</label>
            <p>
               <mixed-citation id="d646e1430" publication-type="other">
Barrington Moore, The Social Origins of Dictatorship and Democracy: Lord and Peasant in the
Making of the Modern World (Boston: Beacon Press, 1966);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d646e1439" publication-type="other">
Charles Tilly (ed.), Building States in
Western Europe (Princeton: University of Princeton Press, 1974);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d646e1448" publication-type="other">
Theda Skocpol, States and Social
Revolutions: A Comparative Analysis of France, Russia, and China (Cambridge: Cambridge University
Press, 1979).</mixed-citation>
            </p>
         </fn>
         <fn id="d646e1461a1310">
            <label>82</label>
            <p>
               <mixed-citation id="d646e1468" publication-type="other">
Hooghe and Gary Marks, 'Does Efficiency Shape the Territorial Structure of Government?'.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e1475a1310">
            <label>83</label>
            <p>
               <mixed-citation id="d646e1482" publication-type="other">
Etel Solingen, Regional Orders at Center's dawn: Global and Domestic Influences on Grand Strategy
(Princeton: Princeton University Press, 1998);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d646e1491" publication-type="other">
Etel Solingen, 'Mapping Internationalization:
Domestic and Regional Impacts', International Studies Quarterly , 45:4 (2001), pp. 305-37;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d646e1500" publication-type="other">
Etel
Solingen, 'The Genesis, Design and Effects of Regional Institutions: Lessons from East Asia and the
Middle East', International Studies Quarterly , 52:2 (2008), pp. 261-94.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e1513a1310">
            <label>84</label>
            <p>
               <mixed-citation id="d646e1520" publication-type="other">
John W. Meyer, John Boli, George M. Thomas and Francisco O. Ramirez, 'World Society and the
Nation-State', American Journal of Sociology , 103:1 (1997), pp. 144-81.</mixed-citation>
            </p>
         </fn>
         <fn id="d646e1531a1310">
            <label>85</label>
            <p>
               <mixed-citation id="d646e1538" publication-type="other">
Frank Schimmelfennig, The EU, NATO, and the Integration of Europe: Rules and Rhetoric
(Cambridge: Cambridge University Press, 2003).</mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
