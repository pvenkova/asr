<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">socisciequar</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50010312</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Social Science Quarterly</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Wiley Subscription Services, Inc.</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00384941</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15406237</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">42940584</article-id>
         <article-categories>
            <subj-group>
               <subject>Of General Interest</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Standardizing the World Income Inequality Database</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Frederick</given-names>
                  <surname>Solt</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>6</month>
            <year>2009</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">90</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40113217</issue-id>
         <fpage>231</fpage>
         <lpage>242</lpage>
         <permissions>
            <copyright-statement>© 2009 Southwestern Social Science Association</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/42940584"/>
         <abstract>
            <p>Objective. Cross-national research on the causes and consequences of income inequality has been hindered by the limitations of existing inequality data sets: greater coverage across countries and over time is available from these sources only at the cost of significantly reduced comparability across observations. The goal of the Standardized World Income Inequality Database (SWIID) is to overcome these limitations. Methods. A custom missing-data algorithm was used to standardize the U.N. University's World Income Inequality Database; data collected by the Luxembourg Income Study served as the standard. Results. The SWIID provides comparable Gini indices of gross and net income inequality for 153 countries for as many years as possible from 1960 to the present, along with estimates of uncertainty in these statistics. Conclusions. By maximizing comparability for the largest possible sample of countries and years, the SWIID is better suited to broad cross-national research on income inequality than previously available sources.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d1091e141a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1091e148" publication-type="other">
Kuznets (1955),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1091e154" publication-type="other">
Lenski (1966),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1091e160" publication-type="other">
Meitzer and Richard (1981).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1091e167" publication-type="other">
Huber et al. (2006),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1091e173" publication-type="other">
Lee, Nielsen, and Alderson
(2007),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1091e182" publication-type="other">
Bergh and Fink (2008).</mixed-citation>
            </p>
         </fn>
         <fn id="d1091e189a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1091e196" publication-type="other">
Russett (1964)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1091e202" publication-type="other">
Dahl (1971).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1091e208" publication-type="other">
Reenock, Bernhard, and Sobek (2007).</mixed-citation>
            </p>
         </fn>
         <fn id="d1091e215a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1091e222" publication-type="other">
http://www.lisproject.org</mixed-citation>
            </p>
         </fn>
         <fn id="d1091e229a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1091e236" publication-type="other">
King et al., 2001;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1091e242" publication-type="other">
Gelman and Hill,
2007:529-43</mixed-citation>
            </p>
         </fn>
         <fn id="d1091e253a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d1091e260" publication-type="other">
http://www.lisproject.org/keyfigures/standarderrors.htm</mixed-citation>
            </p>
         </fn>
         <fn id="d1091e267a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d1091e274" publication-type="other">
Moore (2006);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1091e280" publication-type="other">
Mayer and Sarin
(2005).</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>REFERENCES</title>
         <ref id="d1091e299a1310">
            <mixed-citation id="d1091e303" publication-type="other">
APSA Task Force on Inequality and American Democracy. 2004. "American Democracy in
an Age of Rising Inequality." Perspectives on Politics 2(4):651-70.</mixed-citation>
         </ref>
         <ref id="d1091e313a1310">
            <mixed-citation id="d1091e317" publication-type="other">
Atkinson, Anthony B., and Andrea Brandolini. 2001. "Promise and Pitfalls in the Use of
'Secondary' Data-Sets: Income Inequality in OECD Countries as a Case Study." Journal of
Economic Literature 39(3):771-99.</mixed-citation>
         </ref>
         <ref id="d1091e330a1310">
            <mixed-citation id="d1091e334" publication-type="other">
Babones, Salvatore J., and Maria José Alvarez-Rivadulla. 2007. "Standardized Income In-
equality Data for Use in Cross-National Research." Sociological Inquiry 77(1):3-22.</mixed-citation>
         </ref>
         <ref id="d1091e344a1310">
            <mixed-citation id="d1091e348" publication-type="other">
Bergh, Andreas. 2005. "On the Counterfactual Problem of Welfare State Research: How
Can We Measure Redistribution?" European Sociological Review 21(4):345-57.</mixed-citation>
         </ref>
         <ref id="d1091e359a1310">
            <mixed-citation id="d1091e363" publication-type="other">
Bergh, Andreas, and Günther Fink. 2008. "Higher Education Policy, Enrollment, and In-
come Inequality." Social Science Quarterly 89(1):217-35.</mixed-citation>
         </ref>
         <ref id="d1091e373a1310">
            <mixed-citation id="d1091e377" publication-type="other">
Bradley, David, Evelyne Huber, Stephanie Moller, François Nielsen, and John D. Stephens.
2003. "Distribution and Redistribution in Postindustrial Democracies." World Politics
55(2):193-228.</mixed-citation>
         </ref>
         <ref id="d1091e390a1310">
            <mixed-citation id="d1091e394" publication-type="other">
Dahl, Robert A. 1971. Polyarchy: Participation and Opposition. New Haven, CT: Yale Uni-
versity Press.</mixed-citation>
         </ref>
         <ref id="d1091e404a1310">
            <mixed-citation id="d1091e408" publication-type="other">
Deininger, Klaus, and Lyn Squire. 1996. "A New Data Set Measuring Income Inequality."
World Bank Economic Review 10(3):565-91.</mixed-citation>
         </ref>
         <ref id="d1091e418a1310">
            <mixed-citation id="d1091e422" publication-type="other">
Gelman, Andrew, and Jennifer Hill. 2007. Data Analysis Using Regression and Multilevel/
Hierarchical Models. Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d1091e432a1310">
            <mixed-citation id="d1091e436" publication-type="other">
Huber, Evelyne, François Nielsen, Jenny Pribble, and John D. Stephens. 2006. "Politics and
Inequality in Latin America and the Caribbean." American Sociological Review 71(6):943-63.</mixed-citation>
         </ref>
         <ref id="d1091e447a1310">
            <mixed-citation id="d1091e451" publication-type="other">
King, Gary, James Honaker, Anne Joseph, and Kenneth Scheve. 2001. "Analyzing Incom-
plete Political Science Data: An Alternative Algorithm for Multiple Imputation." American
Political Science Review 95(1):49-69.</mixed-citation>
         </ref>
         <ref id="d1091e464a1310">
            <mixed-citation id="d1091e468" publication-type="other">
Kuznets, Simon. 1955. "Economic Growth and Income Inequality." American Economic
Review 45(1):1-28.</mixed-citation>
         </ref>
         <ref id="d1091e478a1310">
            <mixed-citation id="d1091e482" publication-type="other">
Lee, Cheol-Sung, François Nielsen, and Arthur S. Alderson. 2007. "Income Inequality,
Global Economy, and the State." Social Forces 86(1):77-111.</mixed-citation>
         </ref>
         <ref id="d1091e492a1310">
            <mixed-citation id="d1091e496" publication-type="other">
Lenski, Gerhard E. 1966. Power and Privilege: A Theory of Social Stratification. New York:
McGraw-Hill.</mixed-citation>
         </ref>
         <ref id="d1091e506a1310">
            <mixed-citation id="d1091e510" publication-type="other">
Luxembourg Income Study (LIS). 2008a. Database. Available at (http://www.lisproject.org/
techdoc.htm) (multiple countries).</mixed-citation>
         </ref>
         <ref id="d1091e520a1310">
            <mixed-citation id="d1091e524" publication-type="other">
---. 2008b. Key Figures. Available at (http://www.lisproject.org/keyfigures.htm).</mixed-citation>
         </ref>
         <ref id="d1091e532a1310">
            <mixed-citation id="d1091e536" publication-type="other">
Mayer, Susan E., and Ankur Sarin. 2005. "An Assessment of Some Mechanisms Linking
Economic Inequality and Infant Mortality." Social Science &amp; Medicine 60(3):439-455.</mixed-citation>
         </ref>
         <ref id="d1091e546a1310">
            <mixed-citation id="d1091e550" publication-type="other">
Meitzer, Allan H., and Scott F. Richard. 1981. "A Rational Theory of the Size of Gov-
ernment." Journal of Political Economy 89(5):914-27.</mixed-citation>
         </ref>
         <ref id="d1091e560a1310">
            <mixed-citation id="d1091e564" publication-type="other">
Moore, Spencer. 2006. "Peripherality, Income Inequality, and Life Expectancy: Revisiting
the Income Inequality Hypothesis." International Journal of Epidemiology 35(3):623-32.</mixed-citation>
         </ref>
         <ref id="d1091e574a1310">
            <mixed-citation id="d1091e578" publication-type="other">
Neckerman, Kathryn M., and Florencia Torche. 2007. "Inequality: Causes and Conse-
quences." Annual Review of Sociology 33:335-37.</mixed-citation>
         </ref>
         <ref id="d1091e588a1310">
            <mixed-citation id="d1091e592" publication-type="other">
Petrova, Maria. 2008. "Inequality and Media Capture." Journal of Public Economics 92(1-2):
183-212.</mixed-citation>
         </ref>
         <ref id="d1091e602a1310">
            <mixed-citation id="d1091e606" publication-type="other">
Reenock, Christopher, Michael Bernhard, and David Sobek. 2007. "Regressive Socioeco-
nomic Distribution and Democratic Survival." International Studies Quarterly 51(3):677-99.</mixed-citation>
         </ref>
         <ref id="d1091e617a1310">
            <mixed-citation id="d1091e621" publication-type="other">
Russett, Bruce M. 1964. "Inequality and Instability: The Relation of Land Tenure to Pol-
itics." World Politics 16(3):442-54.</mixed-citation>
         </ref>
         <ref id="d1091e631a1310">
            <mixed-citation id="d1091e635" publication-type="other">
Smeeding, Timothy M. 2005. "Public Policy, Economic Inequality, and Poverty: The
United States in Comparative Perspective." Social Science Quarterly 86(S1):955 -83.</mixed-citation>
         </ref>
         <ref id="d1091e645a1310">
            <mixed-citation id="d1091e649" publication-type="other">
Solt, Frederick. 2008. "Economic Inequality and Democratic Political Engagement." Amer-
ican Journal of Political Science 52(1):48-60.</mixed-citation>
         </ref>
         <ref id="d1091e659a1310">
            <mixed-citation id="d1091e663" publication-type="other">
UNU-WIDER. 2008. World Income Inequality Database, Version 2.0c, May 2008. Available
at (http://www.wider.unu.edu/research/Database/en_GB/database/).</mixed-citation>
         </ref>
         <ref id="d1091e673a1310">
            <mixed-citation id="d1091e677" publication-type="other">
U.S. Census Bureau. 2007. International Data Base. Available at (http://www.census.gov/ipc/
www/idb/idbprint.html).</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
