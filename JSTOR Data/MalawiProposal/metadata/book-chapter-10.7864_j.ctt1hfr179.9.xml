<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1hfr179</book-id>
      <subj-group subj-group-type="discipline">
         <subject>Economics</subject>
         <subject>Ecology &amp; Evolutionary Biology</subject>
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>Why Forests? Why Now?</book-title>
         <subtitle>The Science, Economics, and Politics of Tropical Forests and Climate Change</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Seymour</surname>
               <given-names>Frances</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib2">
            <name name-style="western">
               <surname>Busch</surname>
               <given-names>Jonah</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>25</day>
         <month>10</month>
         <year>2016</year>
      </pub-date>
      <isbn content-type="ppub">9781933286853</isbn>
      <isbn content-type="epub">9781933286860</isbn>
      <publisher>
         <publisher-name>Brookings Institution Press</publisher-name>
      </publisher>
      <permissions>
         <copyright-year>2016</copyright-year>
         <copyright-holder>CENTER FOR GLOBAL DEVELOPMENT</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7864/j.ctt1hfr179"/>
      <abstract abstract-type="short">
         <p>Tropical forests are an undervalued asset in meeting the greatest global challenges of our time-averting climate change and promoting development. Despite their importance, tropical forests and their ecosystems are being destroyed at a high and increasing rate in most forest-rich countries. The good news is that science, economics, and politics are aligned to support a major international effort over the next five years to reverse tropical deforestation.<italic>Why Forests? Why Now?</italic>synthesizes the latest evidence on the importance of tropical forests in a way that is accessible to anyone interested in climate change and development and to readers already familiar with the problem of deforestation. It makes the case to decision makers in rich countries that rewarding developing countries for protecting their forests is urgent, affordable, and achievable.</p>
      </abstract>
      <counts>
         <page-count count="250"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hfr179.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hfr179.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hfr179.3</book-part-id>
                  <title-group>
                     <title>Foreword</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Baldwin</surname>
                           <given-names>Alec</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>vii</fpage>
                  <abstract>
                     <p>In 1995, while shooting a documentary in the Peruvian rainforest about the illegal importation of exotic birds, I first became truly interested in the desecration of the Amazon rainforest. In recent years, that concern has grown through my work with the United Nations Development Program and their mission regarding the rights of indigenous peoples. As someone who cares about the threat of climate change, it has become impossible for me to decouple these issues.</p>
                     <p>It is clear from this remarkable book that we simply cannot meet global climate goals without greater investment in forest protection and restoration. When you look</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hfr179.4</book-part-id>
                  <title-group>
                     <title>Foreword</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Stern</surname>
                           <given-names>Lord Nicholas</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>ix</fpage>
                  <abstract>
                     <p>Poor people are hit earliest and hardest by the effects of climate change, including, for example, floods, droughts, extreme weather events, transformations in the monsoons, changing growing conditions, and heat stress. They also have so much to gain directly by more sustainable ways of organizing our economic lives, including, for example, efficiency in the management of water and energy or decentralized solar power. And, in particular, and the subject of this study, poor people stand to benefit from the better management of our forests, grasslands and other ecosystems. Climate change makes the climb out of poverty much more difficult for</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hfr179.5</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Birdsall</surname>
                           <given-names>Nancy</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hfr179.6</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Seymour</surname>
                           <given-names>Frances</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Busch</surname>
                           <given-names>Jonah</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>xiii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hfr179.7</book-part-id>
                  <title-group>
                     <label>CHAPTER 1</label>
                     <title>Introduction</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>For six long days at the end of October 1998, Hurricane Mitch lashed Central America with gusts of wind over three hundred kilometers per hour. Waves as high as thirteen meters crashed onto the shore.¹ Dubbed a “Category 5 Monster,” the worst Atlantic storm in more than two hundred years spread death and destruction across the region. Millions of people were displaced, and eleven thousand died. In countries where two-thirds of the population lived on less than four U.S. dollars a day,² the economic damage was estimated to be at least $5 billion.³</p>
                     <p>While El Salvador, Guatemala, and Nicaragua suffered</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hfr179.8</book-part-id>
                  <title-group>
                     <label>CHAPTER 2</label>
                     <title>Tropical Forests</title>
                     <subtitle>A Large Share of Climate Emissions; an Even Larger Share of Potential Emission Reductions</subtitle>
                  </title-group>
                  <fpage>27</fpage>
                  <abstract>
                     <p>
                        <italic>Indonesia, 2015</italic>. Between January and October 2015, more than 127,000 fires broke out across Indonesia,¹ burning an area larger than the state of New Jersey.² On many days, the fires generated more greenhouse gas emissions than the whole economy of the United States.³ Why was the world’s third largest expanse of tropical rainforest going up in flames?</p>
                     <p>Large-scale fires in 1982–83 in East Kalimantan—a province in the Indonesian portion of Borneo—provided an early clue. A weather pattern called<italic>El Niño</italic>(Spanish for “the boy,” a reference to the Christ child, due to its tendency to arrive on the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hfr179.9</book-part-id>
                  <title-group>
                     <label>CHAPTER 3</label>
                     <title>Tropical Forests and Development</title>
                     <subtitle>Contributions to Water, Energy, Agriculture, Health, Safety, and Adaptation</subtitle>
                  </title-group>
                  <fpage>59</fpage>
                  <abstract>
                     <p>
                        <italic>Hispaniola</italic>. Three thousand meters above the Caribbean, a raindrop is streaking toward Earth at terminal velocity. It’s heading for the Cordillera Central, the mountainous spine of the island of Hispaniola, and a small puff of wind could blow it in any direction. A slight draft to the east, and the raindrop will fall in the Dominican Republic. A gust to the west, and it will fall in Haiti. Whether this tiny drop of water will ultimately flow east or west will be determined by topography, wind, and chance; but whether it will bring comfort or provoke suffering will be determined</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hfr179.10</book-part-id>
                  <title-group>
                     <label>CHAPTER 4</label>
                     <title>Monitoring Tropical Forests</title>
                     <subtitle>Advances in Tracking Emissions, Sequestration, and Safeguards</subtitle>
                  </title-group>
                  <fpage>89</fpage>
                  <abstract>
                     <p>
                        <italic>Bonn, Germany, July 2001</italic>. It was late in the day. Behind a closed ballroom door in the faded Hotel Maritim, United Nations climate diplomats negotiated the fate of tropical forests. Specifically, they were deciding whether the Kyoto Protocol to the United Nations Framework Convention on Climate Change (UNFCCC) would include provisions for avoiding tropical deforestation.</p>
                     <p>The Kyoto Protocol had been adopted four years earlier, but many issues remained unresolved. One related to its Clean Development Mechanism (CDM), which let developed countries meet their climate targets more cheaply by purchasing offset credits from projects in developing countries. Projects related to energy</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hfr179.11</book-part-id>
                  <title-group>
                     <label>CHAPTER 5</label>
                     <title>Cheaper, Cooler, Faster</title>
                     <subtitle>Reducing Tropical Deforestation for a More Cost-Effective Global Response to Climate Change</subtitle>
                  </title-group>
                  <fpage>121</fpage>
                  <abstract>
                     <p>
                        <italic>Kemper County, Mississippi</italic>. Beneath the rolling pastures and yellow pines of eastern Mississippi lies a reservoir of coal. Soft, wet, crumbly, and low-grade, the brown coal of Mississippi has been described as “a step above dirt.”¹ Compared to the older, denser, blacker bituminous coal that is mined in West Virginia and Wyoming, this brown coal when burned produces less energy, more air pollution, and more carbon dioxide. Since 2006, the Southern Company has had plans to strip-mine brown coal from up to 125 square kilometers of Kemper County to feed a 500-megawatt power plant.</p>
                     <p>The power plant planned for Kemper</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hfr179.12</book-part-id>
                  <title-group>
                     <label>CHAPTER 6</label>
                     <title>Making Forests Worth More Alive than Dead</title>
                     <subtitle>Carbon May Succeed Where Other Values Haven’t</subtitle>
                  </title-group>
                  <fpage>149</fpage>
                  <abstract>
                     <p>
                        <italic>Makira National Park, Madagascar</italic>. The island nation of Madagascar is home to so many plants and animals that live nowhere else on Earth that biologists have dubbed it the Eighth Continent.¹ In addition to the lemurs and baobab trees for which the world’s fourth largest island is famous, Madagascar hosts colorful chameleons, bizarre see-through frogs, and a fierce overgrown mongoose relative called a fossa. Madagascar sits off the east coast of southern Africa, but it actually split off from what is now India nearly one hundred million years ago. Since then, its flora and fauna have evolved in nearly complete</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hfr179.13</book-part-id>
                  <title-group>
                     <label>CHAPTER 7</label>
                     <title>How to Stop Deforestation</title>
                     <subtitle>Experience from Brazil and Beyond</subtitle>
                  </title-group>
                  <fpage>185</fpage>
                  <abstract>
                     <p>
                        <italic>Porto do Moz, Brazil, 2002</italic>. The hot season was approaching in Porto do Moz, a rainforest town of a few thousand people in the Brazilian state of Pará, where the mighty Xingu River empties into the even mightier Amazon, as shown in figure 7.1. Inside a tin-roofed shed, Tarcísio Feitosa da Silva gathered with two dozen men and women he knew well: fishermen, farmers, members of a women’s cooperative, three members of the clergy.¹ For decades their community had relied on the surrounding forest and the rivers flowing through it for a modest living. Feitosa’s grandfather was a rubber tapper;</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hfr179.14</book-part-id>
                  <title-group>
                     <label>CHAPTER 8</label>
                     <title>Global Consumer Demand</title>
                     <subtitle>A Big Footprint on Tropical Forests</subtitle>
                  </title-group>
                  <fpage>219</fpage>
                  <abstract>
                     <p>
                        <italic>Cyberspace, March 17, 2010</italic>. A Greenpeace video on the Internet portrays a bored office worker opening up a Nestlé Kit Kat candy bar, tearing into its distinctive red wrapper. He fails to notice that instead of chocolate, he is biting into the severed finger of an organgutan, to the horror of his officemates. Blood drips down his chin and splatters onto his computer keyboard. After cutting to scenes of an orangutan surveying a devastated forest to the sound of revving chainsaws, the video presents its message against a bright red background: “Stop Nestlé buying palm oil from companies that destroy</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hfr179.15</book-part-id>
                  <title-group>
                     <label>CHAPTER 9</label>
                     <title>The International Politics of Deforestation and Climate Change</title>
                     <subtitle>Two Problems with a Common Solution</subtitle>
                  </title-group>
                  <fpage>249</fpage>
                  <abstract>
                     <p>
                        <italic>Great Britain, 1850</italic>. Amid alarm about a “denudation crisis,” the British Association for the Advancement of Science commissioned a study on the impacts of tropical deforestation, selecting Dr. Hugh Cleghorn, a Scottish surgeon, to lead a team of scientists in conducting it (see figure 9.1).¹ The son of a colonial administrator in Madras and himself a member of the Indian Medical Service, Cleghorn had witnessed and expressed concern about deforestation in southern India. Like many medical professionals of his day, he was also interested in botany and had cultivated expertise in medicinal and economically valuable plants.</p>
                     <p>The resulting<italic>Report of</italic>
                     </p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hfr179.16</book-part-id>
                  <title-group>
                     <label>CHAPTER 10</label>
                     <title>Forest Politics in Developing Countries</title>
                     <subtitle>Tipping the Balance Away from Deforestation as Usual</subtitle>
                  </title-group>
                  <fpage>287</fpage>
                  <abstract>
                     <p>
                        <italic>Brasília, Brazil, 2009</italic>. On Wednesday, November 12, a tense meeting took place in the office of the president of Brazil.¹ In a few hours, President Luiz Inácio Lula da Silva would board a plane bound for Paris to work with French president Nicolas Sarkozy on a joint statement for release at the Copenhagen climate negotiations, just a few weeks away. He had gathered top officials in a final attempt to achieve consensus on whether or not Brazil should announce quantitative targets for reducing emissions.</p>
                     <p>President Lula was impatient. He had already made important decisions to reduce deforestation. He had appointed</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hfr179.17</book-part-id>
                  <title-group>
                     <label>CHAPTER 11</label>
                     <title>The Politics of REDD+ in Rich Countries</title>
                     <subtitle>Broad Constituencies in Favor, Small but Vocal Opposition</subtitle>
                  </title-group>
                  <fpage>325</fpage>
                  <abstract>
                     <p>
                        <italic>Oslo, Norway, 2007</italic>. On September 27, 2007, the leaders of two environmental NGOs in Norway—both named Lars—sent a letter to their government that would have global ramifications.¹ Lars Løvold, head of Rainforest Foundation Norway, and Lars Haltbrekken, head of the Norwegian Society for the Conservation of Nature/Friends of the Earth Norway, had discerned a window of opportunity to take advantage of the domestic politics of climate change in a way that would result in a windfall for tropical forests under the emerging rubric of REDD+.²</p>
                     <p>A long-simmering political dilemma in Norwegian climate policy was becoming acute. Aside from</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hfr179.18</book-part-id>
                  <title-group>
                     <label>CHAPTER 12</label>
                     <title>Finance for Tropical Forests</title>
                     <subtitle>Too Low, Too Slow, Too Constrained as Aid</subtitle>
                  </title-group>
                  <fpage>359</fpage>
                  <abstract>
                     <p>
                        <italic>UN Headquarters, New York, 1989</italic>. A few years after tropical deforestation was placed on the international agenda through the Tropical Forestry Action Plan, described in chapter 9, UK prime minister Margaret Thatcher delivered a prescient speech to the United Nations General Assembly in anticipation of the 1992 UN Conference on Environment and Development. Long before the importance of forests to climate change mitigation gained traction more widely, she observed,</p>
                     <p>We are seeing a vast increase in the amount of carbon dioxide reaching the atmosphere … At the same time as this is happening, we are seeing the destruction on a</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hfr179.19</book-part-id>
                  <title-group>
                     <label>CHAPTER 13</label>
                     <title>Conclusion</title>
                     <subtitle>A Closing Window of Opportunity</subtitle>
                  </title-group>
                  <fpage>401</fpage>
                  <abstract>
                     <p>In the preceding chapters of this book, we have presented many answers to the questions embedded in the title,<italic>Why Forests? Why Now?</italic>In short, conservation of tropical forests is critical to achieving both climate and development objectives. The science, the economics, and the politics are aligned to support ambitious international cooperation to capitalize on the potential contribution of tropical forests to meeting those objectives. Paying developing countries for their performance in forest conservation is among the most promising approaches to climate change mitigation and development alike. But the big money is missing, and the window of opportunity is closing.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hfr179.20</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>415</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hfr179.21</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>431</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
