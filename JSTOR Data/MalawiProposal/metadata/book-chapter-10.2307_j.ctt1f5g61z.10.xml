<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt13kh398</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1f5g61z</book-id>
      <subj-group subj-group-type="discipline">
         <subject>History</subject>
         <subject>Business</subject>
         <subject>Environmental Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>The Green State in Africa</book-title>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Death</surname>
               <given-names>Carl</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>27</day>
         <month>09</month>
         <year>2016</year>
      </pub-date>
      <isbn content-type="ppub">9780300215830</isbn>
      <isbn content-type="epub">9780300224894</isbn>
      <publisher>
         <publisher-name>Yale University Press</publisher-name>
         <publisher-loc>New Haven; London</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2016</copyright-year>
         <copyright-holder>Carl Death</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt1f5g61z"/>
      <abstract abstract-type="short">
         <p>A provocative reassessment of the relationship between states and environmental politics in AfricaFrom climate-related risks such as crop failure and famine to longer-term concerns about sustainable urbanization, environmental justice, and biodiversity conservation, African states face a range of environmental issues. As Carl Death demonstrates, the ways in which they are addressing them have important political ramifications, and challenge current understandings of green politics. Death draws on almost a decade of research to reveal how central African environmental politics are to the transformation of African states.</p>
      </abstract>
      <counts>
         <page-count count="360"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1f5g61z.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1f5g61z.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1f5g61z.3</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1f5g61z.4</book-part-id>
                  <title-group>
                     <title>List of Abbreviations</title>
                  </title-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1f5g61z.5</book-part-id>
                  <title-group>
                     <title>Introduction</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>Environmental politics is right at the beating green heart of the contemporary African condition. Instead of seeing the environment as a side issue or a concern which arises once poverty has been addressed, in this book I argue that we cannot understand African politics without understanding the political governance and contestation of environmental issues: of land, peoples, animals, plants, forests, natural resources, energy supplies, water, and crops. How to live with and within our nonhuman surroundings is a shared global concern. Yet despite this, environmental politics is sometimes regarded as a peripheral concern within the fields of both international politics</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1f5g61z.6</book-part-id>
                  <title-group>
                     <label>CHAPTER 1</label>
                     <title>Global Environmental Governance and the Green State</title>
                  </title-group>
                  <fpage>19</fpage>
                  <abstract>
                     <p>Whose questions count, and whose answers have authority? Before we can begin to explore Africa’s green states, we must reflect both upon the positions from which questions are asked and answers delivered and upon the interlocutors with whom the conversation is conducted. Knowledge production is not neutral or objective, and as my intent in this book is to theorise environmental politics in Africa, it is important to acknowledge at the outset Robert Cox’s well-known dictum that “theory is always<italic>for</italic>someone and<italic>for</italic>some purpose.”¹ In order to clarify and explain the relationship between African states and environmental politics, it</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1f5g61z.7</book-part-id>
                  <title-group>
                     <label>CHAPTER 2</label>
                     <title>Postcolonial Theory and the Green State in Africa</title>
                  </title-group>
                  <fpage>44</fpage>
                  <abstract>
                     <p>The first chapter began with objections to focusing on the state in environmental politics and posed the green state as a response. However, theories of ecological modernisation are ill-suited to understanding and explaining the state in Africa. Thus we require an alternative theoretical conception of the green state. But what is the state? Deliberations on the concept of the state dominate both political theory and strategies of political activism: many environmental activists frame their role in terms of applying pressure to states in order to persuade them to regulate, legislate, and enforce environmental protections. This view of the state as</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1f5g61z.8</book-part-id>
                  <title-group>
                     <label>CHAPTER 3</label>
                     <title>Green Land and State Territory</title>
                  </title-group>
                  <fpage>69</fpage>
                  <abstract>
                     <p>In July 2010 the Royal Society for the Protection of Birds, one of the oldest conservation organisations in the United Kingdom, began a letter-writing campaign to dissuade the government of Tanzania from building a 171.5-km road through the Serengeti National Park. The road, it was claimed, would “critically affect the mammals that inhabit the park and could potentially disrupt the renowned wildebeest migration between Serengeti and Maasai Mara in Kenya.”¹ The Serengeti is a United Nations Educational, Scientific and Cultural Organisation (UNESCO) World Heritage Site and one of the most famous landscapes in East Africa, an iconic vista of grassland</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1f5g61z.9</book-part-id>
                  <title-group>
                     <label>CHAPTER 4</label>
                     <title>Green Citizens and Problematic Populations</title>
                  </title-group>
                  <fpage>108</fpage>
                  <abstract>
                     <p>In 2004 the Kenyan politician and international environmental activist Wangari Maathai was awarded the Nobel Peace Prize “for her contribution to sustainable development, democracy and peace.”¹ She founded the Green Belt Movement, which helped women to restore degraded landscapes through tree planting across Africa, as well as leading high-profile international programmes like the Billion Trees Campaign in 2007. The World Bank estimates that trees planted by the Green Belt Movement in Kenya will capture 375,000 tonnes of carbon dioxide by 2017. Her story is well known, and indeed she is probably the world’s most famous African environmentalist. She was honoured</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1f5g61z.10</book-part-id>
                  <title-group>
                     <label>CHAPTER 5</label>
                     <title>Green Economies and Environmental Markets</title>
                  </title-group>
                  <fpage>152</fpage>
                  <abstract>
                     <p>Near the end of Charles van Onselen’s magisterial social history of Kas Maine, a South African sharecropper, we find lines which seem to encapsulate Maine’s worldview: “As you know, everyone, black or white, has a gift. Some men are gifted livestock farmers. In my case it is tilling the land. My survival depends on that. . . . If you plant beans in October, you could be harvesting them by February.”¹ Van Onselen reflects on these words, spoken by a patriarch in his nineties after a lifetime in the fields, with barely concealed awe: “Even at ninety-one there was always</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1f5g61z.11</book-part-id>
                  <title-group>
                     <label>CHAPTER 6</label>
                     <title>Green African States and International Relations</title>
                  </title-group>
                  <fpage>193</fpage>
                  <abstract>
                     <p>On 5 August 2013 Friends of the Earth International began an international campaign calling upon Shell to clean up its operations in the heavily polluted Niger Delta region of Nigeria. The date is significant: it was two years to the day since UNEP had issued a report on the region which found environmental issues ranging from “serious threats to human health from contaminated drinking water to concerns over the viability and productivity of ecosystems.”¹ The UNEP review followed decades of activism by groups in the Niger Delta and by international advocacy coalitions against the devastation caused by Shell, most famously</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1f5g61z.12</book-part-id>
                  <title-group>
                     <label>CHAPTER 7</label>
                     <title>Afro-Ecologism</title>
                  </title-group>
                  <fpage>233</fpage>
                  <abstract>
                     <p>In a book of this breadth and scope there is a constant risk of generalisation and overabstraction. In struggling against this, I have emphasised the importance of seeing the green state in Africa as an effect of micropractices and discourses of environmental governance and contestation. In turn, this approach sometimes risks missing the big picture: the state is not just an assemblage of practices and discourses like any other. It has a mythical and symbolic enframing character which constitutes a landscape and context for much of political life. Similarly, the focus on specific national and local African examples can sometimes</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1f5g61z.13</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>247</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1f5g61z.14</book-part-id>
                  <title-group>
                     <title>Bibliography</title>
                  </title-group>
                  <fpage>285</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1f5g61z.15</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>337</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
