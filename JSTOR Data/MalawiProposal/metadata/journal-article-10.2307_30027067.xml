<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">publchoi</journal-id>
         <journal-id journal-id-type="jstor">j50000024</journal-id>
         <journal-title-group>
            <journal-title>Public Choice</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Kluwer Academic Publishers</publisher-name>
         </publisher>
         <issn pub-type="ppub">00485829</issn>
         <issn pub-type="epub">15737101</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">30027067</article-id>
         <title-group>
            <article-title>The Stability of International Coalitions in United Nations Voting from 1946 to 1973</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Randall G.</given-names>
                  <surname>Holcombe</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Russell S.</given-names>
                  <surname>Sobel</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>1</month>
            <year>1996</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">86</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1/2</issue>
         <issue-id>i30027064</issue-id>
         <fpage>17</fpage>
         <lpage>34</lpage>
         <permissions>
            <copyright-statement>Copyright 1996 Kluwer Academic Publishers</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/30027067"/>
         <abstract>
            <p>The stability of outcomes under democratic decision-making is a significant issue in public choice. Several factors might make U.N. voting blocs less stable than blocs in national legislatures. Nevertheless, the data suggest that from 1946 to 1973 United Nations voting blocs were relatively stable. Nations that leave their blocs tend to vote with nearby blocs rather than making large ideological shifts, and tend to return to their old blocs. There does not appear to be cycles in United Nations voting blocs. Furthermore, the blocs can be ranked on a stable single-dimensioned continuum, lending further evidence that United Nations voting blocs are stable.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Notes</title>
         <ref id="d1772e199a1310">
            <label>1</label>
            <mixed-citation id="d1772e206" publication-type="other">
Downs (1957)</mixed-citation>
            <mixed-citation id="d1772e212" publication-type="other">
Bo-
wen (1943)</mixed-citation>
         </ref>
         <ref id="d1772e222a1310">
            <label>2</label>
            <mixed-citation id="d1772e229" publication-type="other">
Tullock (1967)</mixed-citation>
            <mixed-citation id="d1772e235" publication-type="other">
Plott (1967)</mixed-citation>
         </ref>
         <ref id="d1772e242a1310">
            <label>3</label>
            <mixed-citation id="d1772e249" publication-type="other">
Koford (1982)</mixed-citation>
            <mixed-citation id="d1772e255" publication-type="other">
Holcombe (1985: Ch. 9)</mixed-citation>
         </ref>
         <ref id="d1772e262a1310">
            <label>4</label>
            <mixed-citation id="d1772e269" publication-type="other">
Buchanan (1954)</mixed-citation>
         </ref>
         <ref id="d1772e277a1310">
            <label>5</label>
            <mixed-citation id="d1772e284" publication-type="other">
Weingast, Shepsle, and Johnson (1981)</mixed-citation>
            <mixed-citation id="d1772e290" publication-type="other">
Holcombe (1985)</mixed-citation>
            <mixed-citation id="d1772e296" publication-type="other">
Riker's (1962)</mixed-citation>
            <mixed-citation id="d1772e303" publication-type="other">
Holcombe (1986)</mixed-citation>
            <mixed-citation id="d1772e309" publication-type="other">
Lee (1989)</mixed-citation>
         </ref>
         <ref id="d1772e316a1310">
            <label>6</label>
            <mixed-citation id="d1772e323" publication-type="other">
Tullock (1982)</mixed-citation>
         </ref>
         <ref id="d1772e330a1310">
            <label>7</label>
            <mixed-citation id="d1772e337" publication-type="other">
Alker and Russett (1965: 276)</mixed-citation>
            <mixed-citation id="d1772e343" publication-type="other">
1965: 12</mixed-citation>
         </ref>
         <ref id="d1772e350a1310">
            <label>8</label>
            <mixed-citation id="d1772e357" publication-type="other">
Marín-Bosch (1987)</mixed-citation>
         </ref>
         <ref id="d1772e364a1310">
            <label>16</label>
            <mixed-citation id="d1772e371" publication-type="other">
Poole and Rosenthal (1985</mixed-citation>
            <mixed-citation id="d1772e377" publication-type="other">
1987</mixed-citation>
            <mixed-citation id="d1772e383" publication-type="other">
1991)</mixed-citation>
            <mixed-citation id="d1772e390" publication-type="other">
Koford (1990)</mixed-citation>
            <mixed-citation id="d1772e396" publication-type="other">
Hoolloway (1990)</mixed-citation>
            <mixed-citation id="d1772e402" publication-type="other">
Newcombe, Ross and Newcombe (1970)</mixed-citation>
         </ref>
         <ref id="d1772e409a1310">
            <label>17</label>
            <mixed-citation id="d1772e416" publication-type="other">
Hagan (1989)</mixed-citation>
         </ref>
         <ref id="d1772e424a1310">
            <label>18</label>
            <mixed-citation id="d1772e431" publication-type="other">
Poole and Rosenthal (1991)</mixed-citation>
            <mixed-citation id="d1772e437" publication-type="other">
Koford (1990)</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>References</title>
         <ref id="d1772e453a1310">
            <mixed-citation id="d1772e457" publication-type="other">
Alker, H.R., Jr. and Russett, B.M. (1965). World politics in the GeneralAssembly. New Haven:
Yale University Press.</mixed-citation>
         </ref>
         <ref id="d1772e467a1310">
            <mixed-citation id="d1772e471" publication-type="other">
Arrow, K.J. (1951). Social choice and individual values. New Haven: Yale University Press.</mixed-citation>
         </ref>
         <ref id="d1772e478a1310">
            <mixed-citation id="d1772e482" publication-type="other">
Black, D. (1958). The theory of committees and elections. Cambridge: Cambridge University
Press.</mixed-citation>
         </ref>
         <ref id="d1772e492a1310">
            <mixed-citation id="d1772e496" publication-type="other">
Bowen, H.R. (1943). The interpretation of voting in the allocation of economic resources. Quar-
terly Journal of Economics 58 (November): 891-901.</mixed-citation>
         </ref>
         <ref id="d1772e507a1310">
            <mixed-citation id="d1772e511" publication-type="other">
Buchanan, J.M. (1954). Social choice, democracy, and free markets. Journal of Political Econo-
my 62: 114-123.</mixed-citation>
         </ref>
         <ref id="d1772e521a1310">
            <mixed-citation id="d1772e525" publication-type="other">
Downs, A. (1957). An economic theory of democracy. New York: Harper &amp; Row.</mixed-citation>
         </ref>
         <ref id="d1772e532a1310">
            <mixed-citation id="d1772e536" publication-type="other">
Hagan, J.D. (1989). Domestic political regime changes and third world voting realignments in the
United Nations, 1946-84. International Organization 43.3 (Summer): 505-541.</mixed-citation>
         </ref>
         <ref id="d1772e546a1310">
            <mixed-citation id="d1772e550" publication-type="other">
Holcombe, R.G. (1985). An economic analysis of democracy. Carbondale: Southern Illinois
University Press.</mixed-citation>
         </ref>
         <ref id="d1772e560a1310">
            <mixed-citation id="d1772e564" publication-type="other">
Holcombe, R.G. (1986). Non-optimal unanimous agreement. Public Choice 48(3): 229-244.</mixed-citation>
         </ref>
         <ref id="d1772e571a1310">
            <mixed-citation id="d1772e575" publication-type="other">
Holloway, S. (1990). Forty years of United Nations General Assembly voting. Canadian Journal
of Political Science 23.2 (June): 279-296.</mixed-citation>
         </ref>
         <ref id="d1772e586a1310">
            <mixed-citation id="d1772e590" publication-type="other">
Koford, K.J. (1982). Centralized vote trading. Public Choice 39: 245-268.</mixed-citation>
         </ref>
         <ref id="d1772e597a1310">
            <mixed-citation id="d1772e601" publication-type="other">
Koford, K.J. (1990). Dimensions, transactions costs and coalitions in legislative voting. Econom-
ics and Politics 2.1 (March): 59-82.</mixed-citation>
         </ref>
         <ref id="d1772e611a1310">
            <mixed-citation id="d1772e615" publication-type="other">
Lee, D.R. (1989). Less than unanimous agreement on the reason for unanimous agreement: Com-
ment. Public Choice 62.1 (July): 83-87.</mixed-citation>
         </ref>
         <ref id="d1772e625a1310">
            <mixed-citation id="d1772e629" publication-type="other">
Maríed;n-Bosch, M. (1987). How nations vote in the General Assembly of the United Nations. Inter-
national Organization 41.4 (Autumn): 705-724.</mixed-citation>
         </ref>
         <ref id="d1772e639a1310">
            <mixed-citation id="d1772e643" publication-type="other">
McKelvey, R.D. (1976). Intransitivities in multi dimensional voting models and some implications
for agenda control. Journal of Economic Theory 12.3 (June): 472-482.</mixed-citation>
         </ref>
         <ref id="d1772e653a1310">
            <mixed-citation id="d1772e657" publication-type="other">
Newcombe, H. and Allett, J. (1981). Nations in groups (2): Typal analyses of roll-call votes in the
U.N. General Assembly (1946-1973). Dundas, Ontario, Canada: The Peace Research Institute.</mixed-citation>
         </ref>
         <ref id="d1772e668a1310">
            <mixed-citation id="d1772e672" publication-type="other">
Newcombe, H., Ross, M. and Newcombe, A.G. (1970). United Nations voting patterns. Interna-
tional Organization 24.1 (Winter): 100-121.</mixed-citation>
         </ref>
         <ref id="d1772e682a1310">
            <mixed-citation id="d1772e686" publication-type="other">
Plott, C.R. (1967). A notion of equilibrium and its possibility under majority rule. American Eco-
nomic Review 62.4 (September): 787-806.</mixed-citation>
         </ref>
         <ref id="d1772e696a1310">
            <mixed-citation id="d1772e700" publication-type="other">
Poole, K.T. and Rosenthal, H. (1985). A spatial model for legislative roll-call analysis. American
Journal of Political Science 29.2 (May): 357-384.</mixed-citation>
         </ref>
         <ref id="d1772e710a1310">
            <mixed-citation id="d1772e714" publication-type="other">
Poole, K.T. and Rosenthal, H. (1987). Analysis of congressional coalition patterns: A unidimen-
sional spatial model. Legislative Studies Quarterly 12.1 (February): 55-75.</mixed-citation>
         </ref>
         <ref id="d1772e724a1310">
            <mixed-citation id="d1772e728" publication-type="other">
Poole, K.T. and Rosenthal, H. (1991). Patterns of congressional voting. American Journal of Po-
litical Science 35.1 (February): 228-278.</mixed-citation>
         </ref>
         <ref id="d1772e738a1310">
            <mixed-citation id="d1772e742" publication-type="other">
Riker, W.H. (1962). The theory of political coalitions. New Haven: Yale University Press.</mixed-citation>
         </ref>
         <ref id="d1772e750a1310">
            <mixed-citation id="d1772e754" publication-type="other">
Riker, W.H. (1980). Implications from the disequilibrium of majority rule for the study of institu-
tions. American Political Science Review 74.2 (June): 432-446.</mixed-citation>
         </ref>
         <ref id="d1772e764a1310">
            <mixed-citation id="d1772e768" publication-type="other">
Tullock, G. (1967). The general irrelevance of the general impossibility theorem. Quarterly Jour-
nal of Economics 81.2 (May): 256-270.</mixed-citation>
         </ref>
         <ref id="d1772e778a1310">
            <mixed-citation id="d1772e782" publication-type="other">
Tullock, G. (1982). Why so much stability. Public Choice 37(2): 189-202.</mixed-citation>
         </ref>
         <ref id="d1772e789a1310">
            <mixed-citation id="d1772e793" publication-type="other">
Weingast, B.R., Shepsle, K.A. and Johnsen, C. (1981). The political economy of benefits and
costs: A neoclassical approach to distributive politics. Journal of Political Economy 89.4 (Au-
gust): 642-664.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
