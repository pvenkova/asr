<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">epidinfe</journal-id>
         <journal-id journal-id-type="jstor">j100820</journal-id>
         <journal-title-group>
            <journal-title>Epidemiology and Infection</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Cambridge University Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">09502688</issn>
         <issn pub-type="epub">14694409</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">3864936</article-id>
         <title-group>
            <article-title>Environmental Mycobacteria in Northern Malawi: Implications for the Epidemiology of Tuberculosis and Leprosy</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>P. E. M.</given-names>
                  <surname>Fine</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>S.</given-names>
                  <surname>Floyd</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>J. L.</given-names>
                  <surname>Stanford</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>P.</given-names>
                  <surname>Nkhosa</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>A.</given-names>
                  <surname>Kasunga</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>S.</given-names>
                  <surname>Chaguluka</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>D. K.</given-names>
                  <surname>Warndorff</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>P. A.</given-names>
                  <surname>Jenkins</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>M.</given-names>
                  <surname>Yates</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>J. M.</given-names>
                  <surname>Ponnighaus</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>6</month>
            <year>2001</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">126</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id>i294841</issue-id>
         <fpage>379</fpage>
         <lpage>387</lpage>
         <page-range>379-387</page-range>
         <permissions>
            <copyright-statement>Copyright 2001 Cambridge University Press</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/3864936"/>
         <abstract>
            <p>More than 36000 individuals living in rural Malawi were skin tested with antigens derived from 12 different species of environmental mycobacteria. Most were simultaneously tested with RT23 tuberculin, and all were followed up for both tuberculosis and leprosy incidence. Skin test results indicated widespread sensitivity to the environmental antigens, in particular to Mycobacterium scrofulaceum, M. intracellulare and one strain of M. fortuitum. Individuals with evidence of exposure to 'fast growers' (i.e. with induration to antigens from fast growers which exceeded their sensitivity to tuberculin), but not those exposed to 'slow growers', were at reduced risk of contracting both tuberculosis and leprosy, compared to individuals whose indurations to the environmental antigen were less than that to tuberculin. This evidence for cross protection from natural exposure to certain environmental mycobacteria may explain geographic distributions of mycobacterial disease and has important implications for the mechanisms and measurement of protection by mycobacterial vaccines.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d701e229a1310">
            <label>1</label>
            <mixed-citation id="d701e236" publication-type="journal">
Romanus V. Mycobacterial infections in Sweden. Scand
J Infect Dis Suppl1995; 98: 15-6.<person-group>
                  <string-name>
                     <surname>Romanus</surname>
                  </string-name>
               </person-group>
               <fpage>15</fpage>
               <volume>98</volume>
               <source>Scand J Infect Dis Suppl</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d701e268a1310">
            <label>2</label>
            <mixed-citation id="d701e275" publication-type="journal">
Horsburgh CR, Selik RM. The epidemiology of
disseminated nontuberculous mycobacterial infection
in the acquired immunodeficiency syndrome (AIDS).
Am Rev Respir Dis1989; 139: 4-7.<person-group>
                  <string-name>
                     <surname>Horsburgh</surname>
                  </string-name>
               </person-group>
               <fpage>4</fpage>
               <volume>139</volume>
               <source>Am Rev Respir Dis</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d701e313a1310">
            <label>3</label>
            <mixed-citation id="d701e320" publication-type="journal">
van der Werf TS, van der Graaf WTA, Tappero JW,
Asiedu K. Mycobacterium ulcerans infection. Lancet
1999; 354: 1013-8.<person-group>
                  <string-name>
                     <surname>van der Werf</surname>
                  </string-name>
               </person-group>
               <fpage>1013</fpage>
               <volume>354</volume>
               <source>Lancet</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d701e355a1310">
            <label>4</label>
            <mixed-citation id="d701e362" publication-type="journal">
Wright EP, Collins CH, Yates MD. Mycobacterium
xenopi and Mycobacterium kansasii in a hospital water
supply. J Hosp Infect1985; 6: 175-8.<person-group>
                  <string-name>
                     <surname>Wright</surname>
                  </string-name>
               </person-group>
               <fpage>175</fpage>
               <volume>6</volume>
               <source>J Hosp Infect</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d701e398a1310">
            <label>5</label>
            <mixed-citation id="d701e405" publication-type="journal">
Fine PEM. Variation in protection by BCG: impli¬
cations of and for heterologous immunity. Lancet1995;
346: 1339-45.<person-group>
                  <string-name>
                     <surname>Fine</surname>
                  </string-name>
               </person-group>
               <fpage>1339</fpage>
               <volume>346</volume>
               <source>Lancet</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d701e440a1310">
            <label>6</label>
            <mixed-citation id="d701e447" publication-type="journal">
McManus IC, Lockwood DNJ, Stanford JL, Shaaban
MA, Abdul Ati M, Bahr GM. Recognition of a
category of responders to group ii, slow-grower
associated antigens amongst Kuwaiti senior school
children. Tubercle1988; 69: 275-81.<person-group>
                  <string-name>
                     <surname>McManus</surname>
                  </string-name>
               </person-group>
               <fpage>275</fpage>
               <volume>69</volume>
               <source>Tubercle</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d701e488a1310">
            <label>7</label>
            <mixed-citation id="d701e495" publication-type="journal">
Palmer CE, Long MW. Effects of infection with atypical
mycobacteria on BCG vaccination and tuberculosis.
Am Rev Respir Dis1966; 94: 553-68.<person-group>
                  <string-name>
                     <surname>Palmer</surname>
                  </string-name>
               </person-group>
               <fpage>553</fpage>
               <volume>94</volume>
               <source>Am Rev Respir Dis</source>
               <year>1966</year>
            </mixed-citation>
         </ref>
         <ref id="d701e530a1310">
            <label>8</label>
            <mixed-citation id="d701e537" publication-type="journal">
Edwards LB, Acquaviva FA, Livesay VT, Cross FW,
Palmer CE. An atlas of sensitivity to tuberculin, PPD-
B, and histoplasmin in the United States. Am Rev
Respir Dis1969; 99: 1-132.<person-group>
                  <string-name>
                     <surname>Edwards</surname>
                  </string-name>
               </person-group>
               <fpage>1</fpage>
               <volume>99</volume>
               <source>Am Rev Respir Dis</source>
               <year>1969</year>
            </mixed-citation>
         </ref>
         <ref id="d701e575a1310">
            <label>9</label>
            <mixed-citation id="d701e582" publication-type="journal">
Bechelli LM, Barrai I, Gallego Garbajosa P, Uemura
K, Gyi MM, Tamondong C. Correlation between
leprosy rates in villages different distances apart. Bull
WHO1973; 48: 257-60.<person-group>
                  <string-name>
                     <surname>Bechelli</surname>
                  </string-name>
               </person-group>
               <fpage>257</fpage>
               <volume>48</volume>
               <source>Bull WHO</source>
               <year>1973</year>
            </mixed-citation>
         </ref>
         <ref id="d701e620a1310">
            <label>10</label>
            <mixed-citation id="d701e627" publication-type="journal">
Ponnighaus JM, Fine PEM, Bliss L, Sliney IJ, Bradley
DJ, Rees RJW. The Lepra Evaluation Project (LEP),
an epidemiological study of leprosy in Northern
Malawi. I. Methods. Lepr Rev1987; 58: 359-75.<person-group>
                  <string-name>
                     <surname>Ponnighaus</surname>
                  </string-name>
               </person-group>
               <fpage>359</fpage>
               <volume>58</volume>
               <source>Lepr Rev</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d701e666a1310">
            <label>11</label>
            <mixed-citation id="d701e673" publication-type="journal">
Ponnighaus JM, Fine PEM, Gruer PJK, et al. The
Karonga Prevention Trial: a leprosy and tuberculosis
vaccine trial in Nothern Malawi. I. Methods of the
vaccination phase. Lepr Rev1993; 64: 338-56.<person-group>
                  <string-name>
                     <surname>Ponnighaus</surname>
                  </string-name>
               </person-group>
               <fpage>338</fpage>
               <volume>64</volume>
               <source>Lepr Rev</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d701e711a1310">
            <label>12</label>
            <mixed-citation id="d701e718" publication-type="journal">
Shield MJ, Stanford JL, Paul RC, Carswell JW.
Multiple skin testing of tuberculosis patients with a
range of new tuberculins, and a comparison with
leprosy and Mycobacterium ulcerans infection. J Hyg
1977; 78: 331^18.<person-group>
                  <string-name>
                     <surname>Shield</surname>
                  </string-name>
               </person-group>
               <fpage>331</fpage>
               <volume>78</volume>
               <source>J Hyg</source>
               <year>1977</year>
            </mixed-citation>
         </ref>
         <ref id="d701e759a1310">
            <label>13</label>
            <mixed-citation id="d701e766" publication-type="journal">
Ponnighaus JM, Fine PEM, Sterne JAC, Bliss L,
Wilson RJ, Malema SS, Kileta S. Incidence rates of
leprosy in Karonga District, Northern Malawi: patterns
by age, sex, BCG status and classification. Int J Lepr
Other Mycobact Dis1994; 62: 10-23.<person-group>
                  <string-name>
                     <surname>Ponnighaus</surname>
                  </string-name>
               </person-group>
               <fpage>10</fpage>
               <volume>62</volume>
               <source>Int J Lepr Other Mycobact Dis</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d701e807a1310">
            <label>14</label>
            <mixed-citation id="d701e814" publication-type="journal">
Glynn JR, Jenkins PA, Fine PEM, et al. Patterns of
initial and acquired antituberculous drug resistance in
Karonga District, Malawi. Lancet1995; 345: 907-10.<person-group>
                  <string-name>
                     <surname>Glynn</surname>
                  </string-name>
               </person-group>
               <fpage>907</fpage>
               <volume>345</volume>
               <source>Lancet</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d701e849a1310">
            <label>15</label>
            <mixed-citation id="d701e856" publication-type="journal">
Ponnighaus JM, Fine PEM, Bliss L. Certainty levels in
the diagnosis of leprosy. Int J Lepr Other Mycobact Dis
1987; 55: 454-62.<person-group>
                  <string-name>
                     <surname>Ponnighaus</surname>
                  </string-name>
               </person-group>
               <fpage>454</fpage>
               <volume>55</volume>
               <source>Int J Lepr Other Mycobact Dis</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d701e891a1310">
            <label>16</label>
            <mixed-citation id="d701e898" publication-type="journal">
Fine PEM, Bruce J, Ponnighaus JM, Nkhosa P,
Harawa A, Vynnycky E. Tuberculin sensitivity: con-
versions and reversions in a rural African population.
Int J Tub Lung Dis1999; 3: 962-75.<person-group>
                  <string-name>
                     <surname>Fine</surname>
                  </string-name>
               </person-group>
               <fpage>962</fpage>
               <volume>3</volume>
               <source>Int J Tub Lung Dis</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d701e937a1310">
            <label>17</label>
            <mixed-citation id="d701e944" publication-type="journal">
McIntyre G, Belsey E, Stanford JL. Taxonomic
differences between Mycobacterium avium and Myco¬
bacterium intracellulare elucidated in man by skin tests
with three new tuberculins. Eur J Resp Dis1986; 69:
146-52.<person-group>
                  <string-name>
                     <surname>McIntyre</surname>
                  </string-name>
               </person-group>
               <fpage>146</fpage>
               <volume>69</volume>
               <source>Eur J Resp Dis</source>
               <year>1986</year>
            </mixed-citation>
         </ref>
         <ref id="d701e985a1310">
            <label>18</label>
            <mixed-citation id="d701e992" publication-type="journal">
Sutherland I. Recent studies in the epidemiology of
tuberculosis, based on the risk of being infected with
tubercle bacilli. Adv Tuberc Res1976; 19: 1-63.<person-group>
                  <string-name>
                     <surname>Sutherland</surname>
                  </string-name>
               </person-group>
               <fpage>1</fpage>
               <volume>19</volume>
               <source>Adv Tuberc Res</source>
               <year>1976</year>
            </mixed-citation>
         </ref>
         <ref id="d701e1027a1310">
            <label>19</label>
            <mixed-citation id="d701e1034" publication-type="journal">
Fine PEM, Sterne JAC, Ponnighaus JM, Rees RJW.
Delayed-type hypersensitivity, mycobacterial vaccines
and protective immunity. Lancet1994; 344: 1245-9.<person-group>
                  <string-name>
                     <surname>Fine</surname>
                  </string-name>
               </person-group>
               <fpage>1245</fpage>
               <volume>344</volume>
               <source>Lancet</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d701e1069a1310">
            <label>20</label>
            <mixed-citation id="d701e1076" publication-type="journal">
Edwards LB, Acquaviva FA, Livesay VT. Identification
of tuberculous infected: dual tests and density of
reaction. Am Rev Resp Dis1973; 108: 1334-9.<person-group>
                  <string-name>
                     <surname>Edwards</surname>
                  </string-name>
               </person-group>
               <fpage>1334</fpage>
               <volume>108</volume>
               <source>Am Rev Resp Dis</source>
               <year>1973</year>
            </mixed-citation>
         </ref>
         <ref id="d701e1111a1310">
            <label>21</label>
            <mixed-citation id="d701e1118" publication-type="journal">
Abou-zeid C, Gares M-P, Inwald J, et al. Induction of
a type 1 immune response to a recombinant antigen
from Mycobacterium tuberculosis expressed in Myco¬
bacterium vaccae. Infect Immun1997; 65: 1856-62.<person-group>
                  <string-name>
                     <surname>Abou-zeid</surname>
                  </string-name>
               </person-group>
               <fpage>1856</fpage>
               <volume>65</volume>
               <source>Infect Immun</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d701e1156a1310">
            <label>22</label>
            <mixed-citation id="d701e1163" publication-type="journal">
Skinner MA, Yuan S, Prestidge R, Chuk D, Watson
JD, Tan PLJ. Immunization with heat-killed Myco¬
bacterium vaccae stimulates CD8+ cytotoxic T cells
specific for macrophages infected with Mycobacterium
tuberculosis. Infect Immun1997; 65: 4525-30.<person-group>
                  <string-name>
                     <surname>Skinner</surname>
                  </string-name>
               </person-group>
               <fpage>4525</fpage>
               <volume>65</volume>
               <source>Infect Immun</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d701e1205a1310">
            <label>23</label>
            <mixed-citation id="d701e1212" publication-type="journal">
Seah GT, Scott GM, Rook GAW. Type 2 cytokine gene
activation and its relationship to extent of disease in
patients with tuberculosis. J Infect Dis2000; 181:
385-9.<person-group>
                  <string-name>
                     <surname>Seah</surname>
                  </string-name>
               </person-group>
               <fpage>385</fpage>
               <volume>181</volume>
               <source>J Infect Dis</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d701e1250a1310">
            <label>24</label>
            <mixed-citation id="d701e1257" publication-type="book">
McKinney JD, Jacobs WR, Bloom BR. Persisting
problems in tuberculosis. In: Krause RM, ed. Emerging
infections, Academic Press, 1998: 51-146.<person-group>
                  <string-name>
                     <surname>McKinney</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Persisting problems in tuberculosis</comment>
               <fpage>51</fpage>
               <source>Emerging infections</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
