<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt7mqxz</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt7zch7</book-id>
      <subj-group>
         <subject content-type="call-number">HG3550.M37 2009</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Banks and banking, Foreign</subject>
         <subj-group>
            <subject content-type="lcsh">Developing countries</subject>
            <subj-group>
               <subject content-type="lcsh">Case studies</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Banks and banking, International</subject>
         <subj-group>
            <subject content-type="lcsh">Developing countries</subject>
            <subj-group>
               <subject content-type="lcsh">Case studies</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Banks and banking</subject>
         <subj-group>
            <subject content-type="lcsh">Deregulation</subject>
            <subj-group>
               <subject content-type="lcsh">Developing countries</subject>
               <subj-group>
                  <subject content-type="lcsh">Case studies</subject>
               </subj-group>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Financial crises</subject>
         <subj-group>
            <subject content-type="lcsh">Developing countries</subject>
            <subj-group>
               <subject content-type="lcsh">Case studies</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Globalization</subject>
         <subj-group>
            <subject content-type="lcsh">Economic aspects</subject>
            <subj-group>
               <subject content-type="lcsh">Developing countries</subject>
               <subj-group>
                  <subject content-type="lcsh">Case studies</subject>
               </subj-group>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>Globalizing in Hard Times</book-title>
         <subtitle>The Politics of Banking-Sector Opening in the Emerging World</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Martinez-Diaz</surname>
               <given-names>Leonardo</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>22</day>
         <month>10</month>
         <year>2009</year>
      </pub-date>
      <isbn content-type="ppub">9780801447556</isbn>
      <isbn content-type="epub">9780801459849</isbn>
      <publisher>
         <publisher-name>Cornell University Press</publisher-name>
         <publisher-loc>ITHACA; LONDON</publisher-loc>
      </publisher>
      <edition>1</edition>
      <permissions>
         <copyright-year>2009</copyright-year>
         <copyright-holder>Cornell University</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7591/j.ctt7zch7"/>
      <abstract abstract-type="short">
         <p>In<italic>Globalizing in Hard Times</italic>, Leonardo Martinez-Diaz examines the sudden and substantial increase in cross-border ownership of commercial banks in countries where bank ownership had long been restricted by local rules. Many parties-the World Bank and the IMF, the world's largest commercial banks, their home governments, and their negotiators-had been pushing for a relaxation of ownership rules since the early 1980s and into the 1990s, when bank profitability levels in advanced industrial societies went flat. In their hunt for higher returns on assets, the major banks looked to expand business overseas, but through the mid-1990s their efforts to impose more liberal ownership regimes in nationalist countries proved largely unsuccessful.</p>
         <p>Martinez-Diaz illustrates the ongoing political resistance to liberalized ownership rules in Mexico, Indonesia, Brazil, and South Korea. He then demonstrates the importance of a series of events-the Mexican crisis and the Brazilian banking shock in 1994-1995 and the Asian crisis of 1997-1998 among them-in finally knocking down barriers to foreign ownership of banks. After these upheavals, policymakers who were worried about their political survival-and who were sometimes pressed by the IMF and foreign governments-reshaped the regulatory environment in key emerging markets. Self-proclaimed global banks eagerly grasped the opportunity to expand their operations worldwide, but after the initial shock, domestic politics reasserted themselves, often diluting the new, liberal rules.</p>
      </abstract>
      <counts>
         <page-count count="256"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zch7.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zch7.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zch7.3</book-part-id>
                  <title-group>
                     <title>List of Figures and Tables</title>
                  </title-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zch7.4</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <fpage>xiii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zch7.5</book-part-id>
                  <title-group>
                     <title>List of Acronyms</title>
                  </title-group>
                  <fpage>xvii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zch7.6</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>THE POLITICS OF BANKING-SECTOR OPENING IN THE EMERGING WORLD</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract abstract-type="extract">
                     <p>Financial globalization—the reduction of regulatory and technological barriers that makes possible the unfettered movement of capital across national borders—is one of the most powerful, transformative forces of our time. It has made the global economy more dynamic and more productive, but also more unstable, than in any period since the late nineteenth century. Financial globalization has also challenged the capacity of governments to control financial and monetary variables, throwing into question what it means to be a sovereign state in a world where market participants have vastly enhanced options for circumventing and exploiting gaps in national regulatory frameworks.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zch7.7</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>THE FRUSTRATED QUEST TO GLOBALIZE BANKING</title>
                  </title-group>
                  <fpage>23</fpage>
                  <abstract abstract-type="extract">
                     <p>During the late 1980s and 1990s, a diverse group of actors began to work toward a common goal: the elimination of barriers to foreign participation in the banking sectors of large emerging economies. The group included the Bretton Woods institutions (the World Bank and the International Monetary Fund), some of the largest American and European financial institutions, their home governments, and their countries’ trade negotiators. Their motivations were different; the Bretton Woods institutions peddled liberalization in the name of sound financial policy and economic growth, foreign banks did so with an eye to raising profits through overseas expansion, and their</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zch7.8</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>MEXICO:</title>
                     <subtitle>Liberal Ideas in a Crisis Environment</subtitle>
                  </title-group>
                  <fpage>43</fpage>
                  <abstract abstract-type="extract">
                     <p>Of the four countries in this book, Mexico undertook the most radical exercise in banking-sector opening. Compared with Brazil, South Korea, and Indonesia, Mexico had the longest and most consistent tradition of banking-sector protectionism: from the end of liberal finance in the 1930s to the eve of liberal resurgence in the late 1980s, its banking sector was one of the most closed in the emerging world. Yet in the course of the decade running from 1992 to 2002, Mexico’s banking regime shifted from highly protectionist to totally open, and its banking sector moved from one held entirely by private domestic</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zch7.9</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>BRAZIL:</title>
                     <subtitle>Pragmatism and the Titration of Foreign Entry</subtitle>
                  </title-group>
                  <fpage>78</fpage>
                  <abstract abstract-type="extract">
                     <p>Brazil’s approach to banking-sector opening was different from that adopted by the authorities in Mexico, Indonesia, and South Korea. The Brazilian government implemented opening through a legally opaque method that allowed the government to approve the inflow of foreign capital into the banking system without actually removing a constitutional ban on foreign participation. In Brazil, de facto banking-sector opening took place without de jure opening. As a result, Brazilian policymakers enjoyed more control over the timing and extent of banking-sector opening than did decision makers in other countries. Like chemists regulating the flow of a liquid with the turn of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zch7.10</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>INDONESIA:</title>
                     <subtitle>External Pressure and Ethnic Politics</subtitle>
                  </title-group>
                  <fpage>109</fpage>
                  <abstract abstract-type="extract">
                     <p>At first glance, the many similarities between Indonesia and Mexico would suggest that the drivers and process of banking-sector opening were similar in both countries. In the early 1990s, both countries had stable, corporatist, “soft authoritarian” regimes. A single party had a monopoly on power, political repression was rare but targeted, and a degree of consent from the electorate was purchased with robust, if uneven, economic growth. In both countries, economic and financial policy was in the hands of U.S.-educated technocrats with strong orthodox leanings, a deep faith in integration with the global economy, and robust relationships with the Bretton</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zch7.11</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>SOUTH KOREA:</title>
                     <subtitle>Rescuing Gradualism from the Imperatives of Crisis</subtitle>
                  </title-group>
                  <fpage>140</fpage>
                  <abstract abstract-type="extract">
                     <p>Of the four case studies, South Korea stands out as the country in which the state exercised the most extensive and prolonged control over the commercial banking system. Mexico’s banking system was controlled by the private sector. In Brazil and Indonesia, a class of private bankers took root and eventually flourished in the shadow of large state-owned banks, but in Korea, no discrete, politically independent class of bankers emerged at all. Korean commercial banks were owned and controlled by the government throughout the 1960s and 1970s, and even after they were privatized in the 1980s, government influence remained strong. For</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zch7.12</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>CONCLUSION:</title>
                     <subtitle>Learning to Live with Barbarians</subtitle>
                  </title-group>
                  <fpage>172</fpage>
                  <abstract abstract-type="extract">
                     <p>This book has provided an account of how an important dimension of financial globalization was constructed during the late twentieth and early twenty-first centuries in four different corners of the emerging world. That dimension is the opening of the domestic commercial banking sector to foreign investment, competition, and ownership. For each of the four countries studied here, I performed an autopsy of the liberalization process by dissecting its many policy dilemmas, negotiations, and political battles. Above all, the four narratives are about the struggle of different groups to control the financial pipelines of an economy and to capture the riches</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zch7.13</book-part-id>
                  <title-group>
                     <title>Bibliography</title>
                  </title-group>
                  <fpage>195</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zch7.14</book-part-id>
                  <title-group>
                     <title>Appendix 1.</title>
                     <subtitle>LIST OF INTERVIEWEES</subtitle>
                  </title-group>
                  <fpage>215</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zch7.15</book-part-id>
                  <title-group>
                     <title>Appendix 2.</title>
                     <subtitle>GATS COMMITMENTS</subtitle>
                  </title-group>
                  <fpage>221</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zch7.16</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>225</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
