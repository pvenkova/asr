<?xml version="1.0" encoding="UTF-8"?>
<article article-type="research-article" dtd-version="1.0" xml:lang="eng">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="publisher-id">ecologysociety</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50015787</journal-id>
         <journal-title-group>
            <journal-title>Ecology and Society</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Resilience Alliance</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">17083087</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">26269267</article-id>
         <article-categories>
            <subj-group>
               <subject>Research</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Ex Ante Scale Dynamics Analysis in the Policy Debate on Sustainable Biofuels in Mozambique</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Schut</surname>
                  <given-names>Marc</given-names>
               </string-name>
               <xref ref-type="aff" rid="af1">¹</xref>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Leeuwis</surname>
                  <given-names>Cees</given-names>
               </string-name>
               <xref ref-type="aff" rid="af1">¹</xref>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>van Paassen</surname>
                  <given-names>Annemarie</given-names>
               </string-name>
               <xref ref-type="aff" rid="af1">¹</xref>
            </contrib>
            <aff id="af1">
               <label>¹</label>Knowledge, Technology and Innovation Group, Wageningen University and Research Centre</aff>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>3</month>
            <year>2013</year>
            <string-date>Mar 2013</string-date>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink">18</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">e26269247</issue-id>
         <permissions>
            <copyright-statement>Copyright © 2013 by the author(s)</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/26269267"/>
         <abstract>
            <label>ABSTRACT.</label>
            <p>In this paper, we explore how ex ante scale dynamics analysis can contribute to better understanding of interactions between scales and levels, and how these interactions influence solution space in policy processes. In so doing, we address opportunities and challenges of conducting ex ante scale dynamics analysis as part of an action-oriented social science research approach that seeks to enhance its contribution to more scale-sensitive policy development. The policy debate on sustainable biofuels in Mozambique provides the empirical context in which we analyze interactions across administrative, institutional, and economic scales and levels, and how these interactions influence the space in which policy solutions can be explored and designed. On the basis of the analysis, we conclude that ex ante scale dynamics analysis can contribute to: (1) increasing awareness of interactions between scales and levels, and their implications for policy, (2) identifying immediate and potential matches and mismatches between scales and levels, and developing (adaptive) capacity to address them, and (3) identifying stakeholders and their scale- and level-related interests that can provide the basis for collaborative multi-stakeholder learning. Consequently, ex ante scale dynamics analysis can provide an important contribution to balancing and harmonizing interactions across different scales and levels, from which innovative and scale-sensitive policy responses can emerge. As part of an action-oriented, social science research approach, careful attention needs to be paid to processes of scale and level inclusion and exclusion when conducting scale dynamics analysis.</p>
         </abstract>
         <kwd-group>
            <label>Key Words:</label>
            <kwd>action-oriented research</kwd>
            <kwd>biofuels</kwd>
            <kwd>ex ante scale dynamics analysis</kwd>
            <kwd>Mozambique</kwd>
            <kwd>policy processes</kwd>
            <kwd>scale and level</kwd>
            <kwd>sustainability</kwd>
         </kwd-group>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="unparsed-citations">
         <title>LITERATURE CITED</title>
         <ref id="ref1">
            <mixed-citation publication-type="other">Allen, C. R., and C. S. Holling. 2010. Novelty, adaptive capacity, and resilience. Ecology and Society 15(3): 15.[online] URL: http://www.ecologyandsociety.org/vol15/iss13/art24/</mixed-citation>
         </ref>
         <ref id="ref2">
            <mixed-citation publication-type="other">Bauen, A., J. Howes, A. Chase, R. Tipper, A. Inkinen, J. Lovell, and J. Woods. 2005. Feasibility study on certification or a Renewable Transport Fuel Obligation (RTFO). E4tech, ECCM and Imperial College London, London, UK.</mixed-citation>
         </ref>
         <ref id="ref3">
            <mixed-citation publication-type="other">Buizer, M., B. Arts, and K. Kok. 2011. Governance, scale, and the environment: the importance of recognizing knowledge claims in transdisciplinary arenas. Ecology and Society 16(1): 21. [online] URL: http://www.ecologyandsociety.org/vol16/iss21/art21/</mixed-citation>
         </ref>
         <ref id="ref4">
            <mixed-citation publication-type="other">Bunce, M., K. Brown, and S. Rosendo. 2010. Policy misfits, climate change and cross-scale vulnerability in coastal Africa: how development projects undermine resilience. Environmental Science and Policy 13(6):485–497. http://dx.doi.org/10.1016/j.envsci.2010.06.003</mixed-citation>
         </ref>
         <ref id="ref5">
            <mixed-citation publication-type="other">Cash, D. W., and S. C. Moser. 2000. Linking global and local scales: designing dynamic assessment and management processes. Global Environmental Change 10(2): 109–120. http://dx.doi.org/10.1016/S0959-3780(00)00017-0</mixed-citation>
         </ref>
         <ref id="ref6">
            <mixed-citation publication-type="other">Cash, D. W., W. N. Adger, F. Berkes, P. Garden, L. Lebel, P. Olsson, L. Pritchard, and O. Young. 2006. Scale and crossscale dynamics: governance and information in a multilevel world. Ecology and Society 11(2): 8. [online] URL: http://www.ecologyandsociety.org/vol11/iss12/art18/</mixed-citation>
         </ref>
         <ref id="ref7">
            <mixed-citation publication-type="other">CEPAGRI. 2008. Project application and land acquisition process. Mozambican Ministry of Agriculture, Maputo, Mozambique. [online] URL: http://siteresources.worldbank.org/EXTARD/Resources/336681-1236436879081/5893311-- 1271205116054/AlbinoPresentation.pdf</mixed-citation>
         </ref>
         <ref id="ref8">
            <mixed-citation publication-type="other">Coulibaly, A. L., and P. Liu. 2006. Regulations, standards and certification for agricultural exports: a practical manual for producers and exporters in East Africa. Food and Agriculture Organization (FAO), Rome, Italy. [online] URL: http://www.fao.org/docrep/010/a0791e/a0791e00.HTM</mixed-citation>
         </ref>
         <ref id="ref9">
            <mixed-citation publication-type="other">Council of the European Union. 2008. Proposal for a Directive of the European Parliament and of the Council on the promotion of the use of energy from renewable sources (17086/08). Brussels, Belgium.</mixed-citation>
         </ref>
         <ref id="ref10">
            <mixed-citation publication-type="other">Cumming, G. S., D. H. M. Cumming, and C. L. Redman. 2006.Scale mismatches in social-ecological systems: causes, consequences, and solutions. Ecology and Society 11(1): 14.[online] URL: http://www.ecologyandsociety.org/vol11/iss11/art14/</mixed-citation>
         </ref>
         <ref id="ref11">
            <mixed-citation publication-type="other">Dehue, B., C. Hamelinck, S.d. Lint, R. Archer, E. Garcia, and E.v.d. Heuvel. 2008. Sustainability reporting within the RTFO: framework report. Ecofys bv, Utrecht, The Netherlands. [online] URL: http://biomass.ucdavis.edu/secure/materials/sustainability%20committee/Ecofys% 20sustainabilityreporting%20May07.pdf</mixed-citation>
         </ref>
         <ref id="ref12">
            <mixed-citation publication-type="other">Di Lucia, L. 2010. External governance and the EU policy for sustainable biofuels, the case of Mozambique. Energy Policy 38(11):7395–7403. http://dx.doi.org/10.1016/j.enpol.2010.08.015</mixed-citation>
         </ref>
         <ref id="ref13">
            <mixed-citation publication-type="other">European Union (EU). 2009. Directive 2009/28/EC of the European Parliament and of the Council on the promotion of biofuels and other renewable fuels for transport. Official Journal of the European Union L 140/16. European Commission, Brussels, Belgium.</mixed-citation>
         </ref>
         <ref id="ref14">
            <mixed-citation publication-type="other">Gibbons, M., C. Limoges, H. Nowotny, S. Schwartzman, P. Scott, and M. Trow. 1994. The new production of knowledge: the dynamics of science and research in contemporary societies. Sage, London, UK.</mixed-citation>
         </ref>
         <ref id="ref15">
            <mixed-citation publication-type="other">Gibson, C. C., E. Ostrom, and T. K. Ahn. 2000. The concept of scale and the human dimensions of global change: a survey. Ecological Economics 32(2):217–239. http://dx.doi.org/10.1016/S0921-8009(99)00092-0</mixed-citation>
         </ref>
         <ref id="ref16">
            <mixed-citation publication-type="other">Giller, K. E., C. Leeuwis, J. A. Andersson, W. Andriesse, A. Brouwer, P. Frost, P. Hebinck, I. Heitkönig, M. K. van Ittersum, N. Koning, R. Ruben, M. Slingerland, H. Udo, T. Veldkamp, C. van de Vijver, M. T. van Wijk, and P. Windmeijer. 2008. Competing claims on natural resources: what role for science? Ecology and Society 13(2): 34. [online] URL: http://www.ecologyandsociety.org/vol13/iss32/art34/</mixed-citation>
         </ref>
         <ref id="ref17">
            <mixed-citation publication-type="other">Government of Mozambique. 2009. Política e estratégia de biocombustíveis. Resolução 22/2009 [Biofuel policy and strategy. Resolution 22/2009], Maputo, Mozambique.</mixed-citation>
         </ref>
         <ref id="ref18">
            <mixed-citation publication-type="other">Gunderson, L. H., and C. S. Holling, editors. 2002. Panarchy: understanding transformations in human and natural systems. Island Press, Washington D.C., USA.</mixed-citation>
         </ref>
         <ref id="ref19">
            <mixed-citation publication-type="other">Hoppe, R. 2005. Rethinking the science-policy nexus: from knowledge utilization and science technology studies to types of boundary arrangements. Poiesis und Praxis 3(3):199–215. http://dx.doi.org/10.1007/s10202-005-0074-0</mixed-citation>
         </ref>
         <ref id="ref20">
            <mixed-citation publication-type="other">Jasanoff, S. 1990. The fifth branch: science advisors as policymakers.. Harvard University Press, Cambridge, Massachusetts, USA. http://dx.doi.org/10.1063/1.2810251</mixed-citation>
         </ref>
         <ref id="ref21">
            <mixed-citation publication-type="other">Keeney, D., and C. Nanninga. 2008. Biofuel and global biodiversity. Institute for Agriculture and Trade Policy (IATP). Minneapolis, Minnesota, USA.</mixed-citation>
         </ref>
         <ref id="ref22">
            <mixed-citation publication-type="other">Kok, K., and T. Veldkamp (editors). 2011a. Scale and Governance. Ecology and Society [online] URL: http://www.ecologyandsociety.org/issues/view.php?sf=57</mixed-citation>
         </ref>
         <ref id="ref23">
            <mixed-citation publication-type="other">Kok, K., and T. Veldkamp. 2011b. Scale and governance: conceptual considerations and practical implications. Ecology and Society 16(2): 23. [online] URL: http://www.ecologyandsociety.org/vol16/iss22/art23/</mixed-citation>
         </ref>
         <ref id="ref24">
            <mixed-citation publication-type="other">Mandemaker, M., M. Bakker, and J. Stoorvogel. 2011. The role of governance in agricultural expansion and intensification: a global study of arable agriculture. Ecology and Society 16(2): 8. http://dx.doi.org/10.5751/ES-04142-160208</mixed-citation>
         </ref>
         <ref id="ref25">
            <mixed-citation publication-type="other">Manson, S. M. 2008. Does scale exist? An epistemological scale continuum for complex human–environment systems. Geoforum 39(2):776–788. http://dx.doi.org/10.1016/j.geoforum.2006.09.010</mixed-citation>
         </ref>
         <ref id="ref26">
            <mixed-citation publication-type="other">McNie, E. C. 2007. Reconciling the supply of scientific information with user demands: an analysis of the problem and review of the literature. Environmental Science and Policy 10(1):17–18. http://dx.doi.org/10.1016/j.envsci.2006.10.004</mixed-citation>
         </ref>
         <ref id="ref27">
            <mixed-citation publication-type="other">Olsson, P., C. Folke, V. Galaz, T. Hahn, and L. Schultz. 2007.Enhancing the fit through adaptive co-management: creating and maintaining bridging functions for matching scales in the Kristianstads Vattenrike Biosphere Reserve, Sweden. Ecology and Society 12(1): 28. [online] URL: http://www.ecologyandsociety.org/vol12/iss21/art28/</mixed-citation>
         </ref>
         <ref id="ref28">
            <mixed-citation publication-type="other">Project Group Sustainable Production of Biomass. 2007.Testing framework for sustainable biomass. Interdepartmental Programme Management (IPM) Energy Transition, Government of The Netherlands, The Hague, The Netherlands. [online] URL: http://www.globalbioenergy.org/uploads/media/0703_Sustainable_Production_of__Sustainable_production_of_biomass__Cramer__-_Testing_framework_for_sustainable_biomass_01.pdf</mixed-citation>
         </ref>
         <ref id="ref29">
            <mixed-citation publication-type="other">Roundtable on Sustainable Biofuels (RSB). 2008. Global principles and criteria for sustainable biofuels production. Version 0. Ecole Polytechnique Federale de Lausanne (EPFL) Energy Centre, Lausanne, Switzerland. [online] URL: http://rsb.org/pdfs/standards/Version-Zero_RSB_Std_en.pdf</mixed-citation>
         </ref>
         <ref id="ref30">
            <mixed-citation publication-type="other">Schut, M., S. Bos, L. Machuama, and M. Slingerland. 2010a. Working towards sustainability. Learning experiences for sustainable biofuel strategies in Mozambique. Wageningen University and Research Centre, Wageningen, The Netherlands; Mozambique Centre for Agricultural Promotion (CEPAGRI), Maputo, Mozambique.</mixed-citation>
         </ref>
         <ref id="ref31">
            <mixed-citation publication-type="other">Schut, M., M. Slingerland, and A. Locke. 2010b. Biofuel developments in Mozambique. Update and analysis of policy, potential and reality. Energy Policy 38(9): 5151–5165. http://dx.doi.org/http://dx.doi.org/10.1016/j.enpol.2010.04.048</mixed-citation>
         </ref>
         <ref id="ref32">
            <mixed-citation publication-type="other">Schut, M., C. Leeuwis, A. van Paassen, and A. Lerner. 2011a. Knowledge and innovation management in the policy debate on biofuel sustainability in Mozambique: what roles for researchers? Knowledge Management for Development Journal 7(1):45–64. http://dx.doi.org/http://dx.doi.org/10.1080/19474199.2011.593874</mixed-citation>
         </ref>
         <ref id="ref33">
            <mixed-citation publication-type="other">Schut, M., A. van Paassen, C. Leeuwis, S. Bos, W. Leonardo and A. Lerner. 2011b. Space for innovation for sustainable community-based biofuel production and use: lessons learned for policy from Nhambita community, Mozambique. Energy Policy 39(9):5116–5128. http://dx.doi.org/http://dx.doi.org/10.1016/j.enpol.2011.05.053</mixed-citation>
         </ref>
         <ref id="ref34">
            <mixed-citation publication-type="other">Schut, M., A. Van Paassen, and C. Leeuwis. 2013. Beyond the research-policy interface. Boundary arrangements at research-stakeholder interfaces in the policy debate on biofuel sustainability in Mozambique. Environmental Science and Policy 27:91–102. http://dx.doi.org/10.1016/j.envsci.2012.10.007</mixed-citation>
         </ref>
         <ref id="ref35">
            <mixed-citation publication-type="other">Smeets, E., M. Junginger, A. Faaij, A. Walter, P. Dolzan, and W. Turkenburg. 2008. The sustainability of Brazilian ethanol: an assessment of the possibilities of certified production. Biomass and Bioenergy 32(8):781–813. http://dx.doi.org/10.1016/j.biombioe.2008.01.005</mixed-citation>
         </ref>
         <ref id="ref36">
            <mixed-citation publication-type="other">Smith, A., J.-P. Voß and J. Grin. 2010. Innovation studies and sustainability transitions: the allure of the multi-level perspective and its challenges. Research Policy 39(4):435–448. http://dx.doi.org/10.1016/j.respol.2010.01.023</mixed-citation>
         </ref>
         <ref id="ref37">
            <mixed-citation publication-type="other">Termeer, C. J .A. M., A. Dewulf, and M. van Lieshout. 2010.Disentangling scale approaches in governance research: comparing monocentric, multilevel, and adapative governance. Ecology and Society 15(4): 29. [online] URL: http://www.ecologyandsociety.org/vol15/iss24/art29/</mixed-citation>
         </ref>
         <ref id="ref38">
            <mixed-citation publication-type="other">Turnhout, E., and S. Boonman-Berson. 2011. Databases, scaling practices, and the globalization of biodiversity. Ecology and Society 16(1): 35. [online] URL: http://www.ecologyandsociety.org/vol16/iss31/art35/</mixed-citation>
         </ref>
         <ref id="ref39">
            <mixed-citation publication-type="other">van Dam, J., M. Junginger, A. Faaij, I. Jürgens, G. Best, and U. Fritsche. 2008. Overview of recent developments in sustainable biomass certification. Biomass and Bioenergy 32(8):749–780. http://dx.doi.org/10.1016/j.biombioe.2008.01.018</mixed-citation>
         </ref>
         <ref id="ref40">
            <mixed-citation publication-type="other">van der Veen, A., and G. Tagel. 2011. Effect of policy interventions on food security in Tigray, northern Ethiopia. Ecology and Society 16(1): 18. [online] URL: http://www.ecologyandsociety.org/vol16/iss11/art18/</mixed-citation>
         </ref>
         <ref id="ref41">
            <mixed-citation publication-type="other">van Lieshout, M., A. Dewulf, N. Aarts, and C. J. A. M. Termeer. 2011. Do scale frames matter? Scale frame mismatches in the decision making process about a “mega farm” in a small Dutch village. Ecology and Society 16(1): 38.[online] URL: http://www.ecologyandsociety.org/vol16/iss31/art38/</mixed-citation>
         </ref>
         <ref id="ref42">
            <mixed-citation publication-type="other">Veldkamp, T., N. Polman, S. Reinhard, and M. Slingerland. 2011. From scaling to governance of the land system: bridging ecological and economic perspectives. Ecology and Society 16(1): 18. [online] URL: http://www.ecologyandsociety.org/vol16/iss11/art11/</mixed-citation>
         </ref>
         <ref id="ref43">
            <mixed-citation publication-type="other">Woods, J., and R. Diaz-Chavez. 2007. The environmental certification of biofuels. Pages 189–209 in Biofuels: linking support to performance. Round table 138. Organisation for Economic Co-operation and Development (OECD), International Transport Forum and Faculty of Natural Sciences, Imperial College London, London, UK. http://dx.doi.org/10.1787/9789282101803-7-en</mixed-citation>
         </ref>
         <ref id="ref44">
            <mixed-citation publication-type="other">World Bank. 2009. Mozambique—country brief. [online] URL: http://go.worldbank.org/70UK6S1X30</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
