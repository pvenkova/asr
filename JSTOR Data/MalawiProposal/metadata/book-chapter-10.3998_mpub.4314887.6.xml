<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="doi">10.3998/mpub.4314887</book-id>
      <subj-group>
         <subject content-type="call-number">JZ1320.7.H34 2012</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Interregionalism</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">International relations</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">International economic integration</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Regional economics</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">International organizations</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Peace</subject>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>Regional Economic Institutions and Conflict Mitigation</book-title>
         <subtitle>Design, Implementation, and the Promise of Peace</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>HAFTEL</surname>
               <given-names>YORAM Z.</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>21</day>
         <month>05</month>
         <year>2012</year>
      </pub-date>
      <isbn content-type="ppub">9780472118342</isbn>
      <isbn content-type="epub">9780472028443</isbn>
      <publisher>
         <publisher-name>University of Michigan Press</publisher-name>
         <publisher-loc>Ann Arbor</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2012</copyright-year>
         <copyright-holder>the University of Michigan</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.3998/mpub.4314887"/>
      <abstract abstract-type="short">
         <p>In addition to the explicit goal of advancing mutual economic interests, regional economic organizations (REOs) are intended to foster regional cohesion and peace. Drawing on a data set detailing the institutional features of 25 REOs established during the 1980s and 1990s, complemented by a case study of ASEAN, Yoram Z. Haftel investigates the factors that affect REOs' ability to mitigate interstate military conflict. He finds fewer interstate conflicts among REO members who have developed high levels of economic integration and who cultivate regular interaction among member-states' representatives. Haftel concludes that, with an appropriate institutional design and fully implemented agreements, an REO can indeed play a role in mitigating interstate conflict and make a meaningful contribution to regional peace.</p>
      </abstract>
      <counts>
         <page-count count="262"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.4314887.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.4314887.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.4314887.3</book-part-id>
                  <title-group>
                     <title>ACKNOWLEDGMENTS</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.4314887.4</book-part-id>
                  <title-group>
                     <label>CHAPTER 1</label>
                     <title>Introduction:</title>
                     <subtitle>How Regional Institutions Promote Peace</subtitle>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>Regional conflict and war continue to plague our world. While the total number of interstate wars worldwide has fallen and while great-power war is seemingly obsolete, regional tensions remain high in many parts of the world, including sub-Saharan Africa, Southeastern Europe, and the Indian subcontinent. The wars that revolved around the Democratic Republic of Congo, recurrent violence between Israel and its Arab neighbors, and ongoing clashes between India and Pakistan illustrate this troubling development. Compounding these growing regional frictions, diminished global rivalries have reduced the willingness and ability of great powers to intervene and manage disputes that have no direct</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.4314887.5</book-part-id>
                  <title-group>
                     <label>CHAPTER 2</label>
                     <title>Theorizing Institutional Design and Conflict</title>
                  </title-group>
                  <fpage>18</fpage>
                  <abstract>
                     <p>Geographical proximity commonly gives rise to recurrent interaction that can be either amicable or antagonistic. Neighboring countries have an interest in mutually beneficial cooperation, and in an anarchical international system, they also have to worry about the potential harmful implications of disagreement and conflict. What can adjacent countries do to reduce the risk of violent conflict and war? This chapter advances the argument that economic cooperation through regional institutions can facilitate peaceful coexistence in several ways: it increases the opportunity cost of violent conflict, reduces uncertainty with respect to intentions and resolve, builds trust between governmental officials, and offers a</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.4314887.6</book-part-id>
                  <title-group>
                     <label>CHAPTER 3</label>
                     <title>The Landscape of Regional Economic Organizations</title>
                  </title-group>
                  <fpage>59</fpage>
                  <abstract>
                     <p>A glance at the landscape of REOs reveals a great deal of variation in the objectives and structures of these institutions. The previous chapter drew attention to the significant consequences of these differences to the ability of these organizations to promote peace among their members. This chapter takes a closer look at the functions performed by and the institutional features contained in a large number of regional economic organizations. It first describes the type of institutions considered in this study. It then discusses the design features purported to affect violent conflict. It provides a definition for each organizational characteristic and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.4314887.7</book-part-id>
                  <title-group>
                     <label>CHAPTER 4</label>
                     <title>The Effect of Regional Institutionalization on Violent Conflict</title>
                  </title-group>
                  <fpage>91</fpage>
                  <abstract>
                     <p>The theoretical framework presented in chapter 2 indicates that regional economic organizations can be instrumental in reducing interstate violent conflict, and proposes several causal mechanisms by which they may do so. As discussed in that chapter, the effects of these institutional features on conflict depend, in important ways, on their level of institutionalization. More institutionalized REOs, I argued, increase the opportunity cost of conflict, build trust among their members, and serve as effective conflict managers. In addition, bringing a number of specific features together creates synergies by facilitating issue linkage, side payments, and socialization. Using the data set described in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.4314887.8</book-part-id>
                  <title-group>
                     <label>CHAPTER 5</label>
                     <title>Institutional Design, Violent Conflict, and Reversed Causality</title>
                  </title-group>
                  <fpage>121</fpage>
                  <abstract>
                     <p>Chapter 4 examined the effect of regional institutionalization on conflict. Quantitative and qualitative evidence corroborated the claim made in this book that higher levels of institutionalization reduce the number of militarized disputes. It also identified some conditions under which REOs promote peace. Most notably, it pointed to the need to implement signed agreements. In addition, it suggested that the pacifying effect of regional institutions is likely to be muted in the most war-prone regions. Extending the quantitative analysis presented in chapter 4, this chapter probes further into the specific manners by which regional institutions are linked to armed conflict. It</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.4314887.9</book-part-id>
                  <title-group>
                     <label>CHAPTER 6</label>
                     <title>Regional Economic Organizations and Conflict in Southeast Asia</title>
                  </title-group>
                  <fpage>155</fpage>
                  <abstract>
                     <p>The quantitative analysis presented in previous chapters indicates that some institutional features reduce conflict and that others do not. This chapter complements the quantitative analysis with an in-depth case study. The first section discusses the main objectives of this chapter and elaborates on the selection of the ASEAN region for this qualitative analysis.¹ The second section examines the effect of regional institutionalization and institutional design on intraregional conflict. It largely corroborates the statistical findings: economic scope and regular meetings of high-level officials made a significant contribution to intraregional peace. Greater institutional independence, as well as security cooperation, did not have</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.4314887.10</book-part-id>
                  <title-group>
                     <label>CHAPTER 7</label>
                     <title>Conclusion:</title>
                     <subtitle>Regional Organizations and Orders into the Twenty-first Century</subtitle>
                  </title-group>
                  <fpage>193</fpage>
                  <abstract>
                     <p>The new wave of economic regionalism is central to the contemporary global economy. Today, most of the industrial and less developed states in the world are members of at least one regional economic organization. Governments that formed these institutions envisioned them as vehicles not only of economic prosperity but also of regional cohesion and peace. This aspiration has become ever more pronounced with the demise of Cold War rivalries and the growing autonomy of regions around the world. In some parts of the world, such as Europe and Southeast Asia, these hopes have come to fruition as REOs proved effective</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.4314887.11</book-part-id>
                  <title-group>
                     <title>NOTES</title>
                  </title-group>
                  <fpage>211</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.4314887.12</book-part-id>
                  <title-group>
                     <title>REFERENCES</title>
                  </title-group>
                  <fpage>223</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.4314887.13</book-part-id>
                  <title-group>
                     <title>INDEX</title>
                  </title-group>
                  <fpage>247</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
