<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">americanjbotany</journal-id>
         <journal-id journal-id-type="jstor">j100059</journal-id>
         <journal-title-group>
            <journal-title>American Journal of Botany</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>American Botanical Society</publisher-name>
         </publisher>
         <issn pub-type="ppub">00029122</issn>
         <issn pub-type="epub">15372197</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">2446461</article-id>
         <article-categories>
            <subj-group>
               <subject>Anatomy and Morphology</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Anatomy and Distribution of Foliar Idioblasts in Scrophularia and Verbascum (Scrophulariaceae)</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Nels R.</given-names>
                  <surname>Lersten</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>John D.</given-names>
                  <surname>Curtis</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>12</month>
            <year>1997</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">84</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">12</issue>
         <issue-id>i319685</issue-id>
         <fpage>1638</fpage>
         <lpage>1645</lpage>
         <page-range>1638-1645</page-range>
         <permissions>
            <copyright-statement>Copyright 1997 Botanical Society of America, Inc.</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/2446461"/>
         <abstract>
            <p>Internal secretory structures have rarely been reported from Scrophulariaceae, and foliar idioblasts only once before, in 1887. Presented here are the first unambiguous descriptions of subepidermal foliar idioblasts in the family, from Scrophularia and Verbascum, genera regarded as closely allied on other grounds. Leaf samples from 183 mostly herbarium specimens (128 species, with 55 replicates) were cleared and stained, which revealed idioblasts in 62 (69.7%) of 89 Scrophularia species and 13 (33.3%) of 39 Verbascum species. We then chose 14 representative species to examine by resin sectioning and scanning electron microscopy. Idioblasts occurred both adaxially and abaxially. Most were conspicuous, in some species penetrating to vasculature level. Idioblasts had a thin primary wall and were empty at maturity. Verbascum and Scrophularia species with and without idioblasts were scattered among the subgeneric taxa without taxonomic clustering; likewise, both types occurred in approximately proportionate numbers throughout the geographic range, except that 14 of 15 North American Scrophularia species had idioblasts. Petals of two species had abundant idioblasts. The 1887 report illustrated a huge idioblast in S. deserti and we also found the largest in either genus in this species. We also noted trichome and stomata types and report that paraveinal mesophyll and foliar endodermis with casparian strip were both absent.</p>
         </abstract>
         <kwd-group>
            <kwd>Idioblasts</kwd>
            <kwd>Leaf Anatomy</kwd>
            <kwd>Scrophularia</kwd>
            <kwd>Scrophulariaceae</kwd>
            <kwd>Stomata</kwd>
            <kwd>Trichomes</kwd>
            <kwd>Verbascum</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Literature Cited</title>
         <ref id="d386e710a1310">
            <mixed-citation id="d386e714" publication-type="journal">
BAAS, P., AND M. GREGORY. 1985. A survey of oil cells in the dicot-
yledons with comments on their replacement by and joint occur-
rence with mucilage cells. Israel Journal of Botany34: 167-186.<person-group>
                  <string-name>
                     <surname>Baas</surname>
                  </string-name>
               </person-group>
               <fpage>167</fpage>
               <volume>34</volume>
               <source>Israel Journal of Botany</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d386e749a1310">
            <mixed-citation id="d386e753" publication-type="journal">
BARRINGER, K.1993. Five new tribes in the Scrophulariaceae. Novon
3: 15-17.<object-id pub-id-type="doi">10.2307/3391410</object-id>
               <fpage>15</fpage>
            </mixed-citation>
         </ref>
         <ref id="d386e769a1310">
            <mixed-citation id="d386e773" publication-type="journal">
BIGAZZI, M.1993. A survey on the intranuclear inclusions in the
Scrophulariaceae and their systematic significance. Nordic Journal
of Botany13: 19-31.<person-group>
                  <string-name>
                     <surname>Bigazzi</surname>
                  </string-name>
               </person-group>
               <fpage>19</fpage>
               <volume>13</volume>
               <source>Nordic Journal of Botany</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d386e808a1310">
            <mixed-citation id="d386e812" publication-type="journal">
BRUBAKER, C. L., AND N. R. LERSTEN. 1995. Paraveinal mesophyll:
review and survey of the subtribe Erythrininae (Phaseoleae, Papil-
ionoideae, Leguminosae). Plant Systematics and Evolution196:
31-62.<person-group>
                  <string-name>
                     <surname>Brubaker</surname>
                  </string-name>
               </person-group>
               <fpage>31</fpage>
               <volume>196</volume>
               <source>Plant Systematics and Evolution</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d386e851a1310">
            <mixed-citation id="d386e855" publication-type="book">
CRONQUIST, A.1981. An integrated system of classification of flower-
ing plants. Columbia University Press, New York, NY.<person-group>
                  <string-name>
                     <surname>Cronquist</surname>
                  </string-name>
               </person-group>
               <source>An integrated system of classification of flowering plants</source>
               <year>1981</year>
            </mixed-citation>
         </ref>
         <ref id="d386e880a1310">
            <mixed-citation id="d386e884" publication-type="journal">
DICKISON, W. C., AND A. L. WETrzMAN. 1996. Comparative anatomy
of the young stem, node, and leaf of Bonnetiaceae, including ob-
servations on a foliar endodermis. American Journal of Botany83:
405-418.<object-id pub-id-type="doi">10.2307/2446210</object-id>
               <fpage>405</fpage>
            </mixed-citation>
         </ref>
         <ref id="d386e907a1310">
            <mixed-citation id="d386e911" publication-type="book">
FAHN, A.1979. Secretory tissues in plants. Academic Press, London.<person-group>
                  <string-name>
                     <surname>Fahn</surname>
                  </string-name>
               </person-group>
               <source>Secretory tissues in plants</source>
               <year>1979</year>
            </mixed-citation>
         </ref>
         <ref id="d386e933a1310">
            <mixed-citation id="d386e937" publication-type="book">
HILLIARD, 0. M. 1994. The Manuleae—a tribe of Scrophulariaceae.
Edinburgh University Press, Edinburgh.<person-group>
                  <string-name>
                     <surname>Hilliard</surname>
                  </string-name>
               </person-group>
               <source>The Manuleae—a tribe of Scrophulariaceae</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d386e962a1310">
            <mixed-citation id="d386e966" publication-type="journal">
HUFFORD, L.1992. Leaf structure of Besseya and Synthris (Scrophu-
lariaceae). Canadian Journal of Botany70: 921-932.<person-group>
                  <string-name>
                     <surname>Hufford</surname>
                  </string-name>
               </person-group>
               <fpage>921</fpage>
               <volume>70</volume>
               <source>Canadian Journal of Botany</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d386e998a1310">
            <mixed-citation id="d386e1002" publication-type="journal">
KARRFALT, E. E., AND A. S. TOMB. 1983. Air spaces, secretory cavities,
and the relationship between Leucophylleae (Scrophulariaceae) and
Myoporaceae. Systematic Botany8: 29-32.<object-id pub-id-type="doi">10.2307/2418560</object-id>
               <fpage>29</fpage>
            </mixed-citation>
         </ref>
         <ref id="d386e1022a1310">
            <mixed-citation id="d386e1026" publication-type="journal">
LERSTEN, N. R.1986. Modified clearing technique to show sieve tubes
in minor veins of leaves. Stain Technology61: 231-234.<person-group>
                  <string-name>
                     <surname>Lersten</surname>
                  </string-name>
               </person-group>
               <fpage>231</fpage>
               <volume>61</volume>
               <source>Stain Technology</source>
               <year>1986</year>
            </mixed-citation>
         </ref>
         <ref id="d386e1058a1310">
            <mixed-citation id="d386e1062" publication-type="journal">
AND J. D. CURTIS. 1994. Leaf anatomy in Caesalpinia and
Hoffinannseggia (Leguminosae, Caesalpinioideae) with emphasis
on secretory structures. Plant Systematics and Evolution192: 231-
255.<person-group>
                  <string-name>
                     <surname>Lersten</surname>
                  </string-name>
               </person-group>
               <fpage>231</fpage>
               <volume>192</volume>
               <source>Plant Systematics and Evolution</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d386e1100a1310">
            <mixed-citation id="d386e1104" publication-type="book">
METCALFE, C. R., AND L. CHALK. 1950. Anatomy of the dicotyledons.
Clarendon Press, Oxford.<person-group>
                  <string-name>
                     <surname>Metcalfe</surname>
                  </string-name>
               </person-group>
               <source>Anatomy of the dicotyledons</source>
               <year>1950</year>
            </mixed-citation>
         </ref>
         <ref id="d386e1129a1310">
            <mixed-citation id="d386e1133" publication-type="book">
MURBECK, S.1933. Monographie der Gattung Verbascum. Lunds Univ-
ersitets Arsskrift, N.E Avd. 2, Bd. 29, Nr. 2, pp. 1-630 plus 31
plates. Hakan Ohlsson, Lund.<person-group>
                  <string-name>
                     <surname>Murbeck</surname>
                  </string-name>
               </person-group>
               <fpage>1</fpage>
               <source>Monographie der Gattung Verbascum</source>
               <year>1933</year>
            </mixed-citation>
         </ref>
         <ref id="d386e1165a1310">
            <mixed-citation id="d386e1169" publication-type="book">
NAPP-ZINN, K.1973-1974. Anatomie des Blattes II. Blattanatomie der
Angiospermen. Handbuch der Pflanzenanatomie, Band VIII, Teil
2A, Lieferung 1 and 2. Gebruder Borntraeger, Berlin.<person-group>
                  <string-name>
                     <surname>Napp-zinn</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Anatomie des Blattes II</comment>
               <volume>VIII</volume>
               <source>Blattanatomie der Angiospermen</source>
               <year>1973</year>
            </mixed-citation>
         </ref>
         <ref id="d386e1204a1310">
            <mixed-citation id="d386e1208" publication-type="journal">
OLMSTEAD, R. G., AND P. A. REEVES. 1995. Evidence for the polyphylly
of the Scrophulariaceae based on chloroplast rbcL and ndhF se-
quences. Annals of the Missouri Botanical Garden82: 176-193.<object-id pub-id-type="doi">10.2307/2399876</object-id>
               <fpage>176</fpage>
            </mixed-citation>
         </ref>
         <ref id="d386e1228a1310">
            <mixed-citation id="d386e1232" publication-type="journal">
RENAUDIN, S., AND M. CAPDEPON. 1977. Sur la structure des parois des
glandes pedicellees et des glandes en bouclier de Tozzia alpina L.
Bulletin de la Societe Botanique de France124: 29-43.<person-group>
                  <string-name>
                     <surname>Renaudin</surname>
                  </string-name>
               </person-group>
               <fpage>29</fpage>
               <volume>124</volume>
               <source>Bulletin de la Societe Botanique de France</source>
               <year>1977</year>
            </mixed-citation>
         </ref>
         <ref id="d386e1267a1310">
            <mixed-citation id="d386e1271" publication-type="book">
RICHARDSON, I. B. K.1993. Scrophulariaceae. In V. H. Heywood [ed.],
Flowering plants of the world, 243-245. Oxford University Press,
New York, NY.<person-group>
                  <string-name>
                     <surname>Richardson</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Scrophulariaceae</comment>
               <fpage>243</fpage>
               <source>Flowering plants of the world</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d386e1306a1310">
            <mixed-citation id="d386e1310" publication-type="book">
SOLEREDER, H.1908. Systematic anatomy of the dicotyledons. Trans-
lated from German by L. A. Boodle and D. H. Scott and revised
from the 1899 edition by D. H. Scott. Clarendon Press, Oxford.<person-group>
                  <string-name>
                     <surname>Solereder</surname>
                  </string-name>
               </person-group>
               <source>Systematic anatomy of the dicotyledons</source>
               <year>1908</year>
            </mixed-citation>
         </ref>
         <ref id="d386e1339a1310">
            <mixed-citation id="d386e1343" publication-type="journal">
STIEFELHAGEN, H.1910. Systematische und pflanzengeographische Stu-
dien zur Kenntnis der Gattung Scrophularia. Botanische Jahrbüch-
er44: 406-496.<person-group>
                  <string-name>
                     <surname>Stiefelhagen</surname>
                  </string-name>
               </person-group>
               <fpage>406</fpage>
               <volume>44</volume>
               <source>Botanische Jahrbücher</source>
               <year>1910</year>
            </mixed-citation>
         </ref>
         <ref id="d386e1378a1310">
            <mixed-citation id="d386e1382" publication-type="journal">
THIERET, J. W.1967. Supraspecific classification in the Scrophularia-
ceae: a review. Sida3: 87-106.<person-group>
                  <string-name>
                     <surname>Thieret</surname>
                  </string-name>
               </person-group>
               <fpage>87</fpage>
               <volume>3</volume>
               <source>Sida</source>
               <year>1967</year>
            </mixed-citation>
         </ref>
         <ref id="d386e1414a1310">
            <mixed-citation id="d386e1418" publication-type="book">
VOLKENS, G.1887. Die Flora der aegyptisch-arabischen Wüste, auf
Grundlage anatomisch-physiologischer Forschungen dargestellt.
Gebriuder Borntraeger, Berlin.<person-group>
                  <string-name>
                     <surname>Volkens</surname>
                  </string-name>
               </person-group>
               <source>Die Flora der aegyptisch-arabischen Wüste, auf Grundlage anatomisch-physiologischer Forschungen dargestellt</source>
               <year>1887</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
