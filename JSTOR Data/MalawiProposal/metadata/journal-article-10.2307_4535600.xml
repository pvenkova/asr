<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">behaviour</journal-id>
         <journal-id journal-id-type="jstor">j101444</journal-id>
         <journal-title-group>
            <journal-title>Behaviour</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Brill</publisher-name>
         </publisher>
         <issn pub-type="ppub">00057959</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">4535600</article-id>
         <title-group>
            <article-title>Nest Provisioning in the Mud-Dauber Wasp Sceliphron laetum (F. Smith): Body Mass and Taxa Specific Prey Selection</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Mark A.</given-names>
                  <surname>Elgar</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Matthew</given-names>
                  <surname>Jebb</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>3</month>
            <year>1999</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">136</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id>i407364</issue-id>
         <fpage>147</fpage>
         <lpage>159</lpage>
         <page-range>147-159</page-range>
         <permissions>
            <copyright-statement>Copyright 1999 Koninklijke Brill NV</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/4535600"/>
         <abstract>
            <p>The mud dauber wasp Sceliphron laetum (F. Smith) lays a single egg in a mud chamber that is provisioned almost exclusively with orb-weaving spiders. In Madang, Papua New Guinea, the wasps provision their chambers with between three and nine spiders that weigh between 0.01 and 0.28 g and are from at least twelve species. The number of spiders placed in each chamber is negatively correlated with the mean mass of each spider. A field experiment revealed that females cease provisioning after capturing a certain mass of spiders, rather than simply filling each chamber to its volumetric capacity. Furthermore, the wasps select different spider species according to the provisioning sequence. In general, wasps avoid provisioning the early larval instar with species of Gasteracantha, perhaps because the newly emerged wasp larvae cannot penetrate the hard integuments of these spiders.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d448e150a1310">
            <mixed-citation id="d448e154" publication-type="journal">
Adato-Barrion, A.M. &amp; Barrion, A.T. (1981). Spider prey of Sceliphron madraspatanum
conspicillatus (Hymenoptera: Specidae). - Kalikasan10, p. 122-125.<person-group>
                  <string-name>
                     <surname>Adato-Barrion</surname>
                  </string-name>
               </person-group>
               <fpage>122</fpage>
               <volume>10</volume>
               <source>Kalikasan</source>
               <year>1981</year>
            </mixed-citation>
         </ref>
         <ref id="d448e186a1310">
            <mixed-citation id="d448e190" publication-type="journal">
Callan, E.McC. (1988). Biological observations on the mud-dauber wasp Sceliphronformo-
sum (F. Smith) (Hymenoptera: Sphecidae). - Aust. ent. Mag.14, p. 78-82.<person-group>
                  <string-name>
                     <surname>Callan</surname>
                  </string-name>
               </person-group>
               <fpage>78</fpage>
               <volume>14</volume>
               <source>Aust. ent. Mag.</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d448e222a1310">
            <mixed-citation id="d448e226" publication-type="book">
Clutton-Brock, T.H. (1991). The evolution of parental care. - Princeton University Press,
Princeton.<person-group>
                  <string-name>
                     <surname>Clutton-Brock</surname>
                  </string-name>
               </person-group>
               <source>The evolution of parental care</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d448e251a1310">
            <mixed-citation id="d448e255" publication-type="book">
Coville, R.E. (1987). Spider-hunting sphecid wasps. - In: Ecophysiology of spiders
(W. Nentwig, ed.). Springer-Verlag, Heidelberg, p. 309-318.<person-group>
                  <string-name>
                     <surname>Coville</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Spider-hunting sphecid wasps</comment>
               <fpage>309</fpage>
               <source>Ecophysiology of spiders</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d448e288a1310">
            <mixed-citation id="d448e292" publication-type="journal">
&amp; Griswold, C. (1983). Nesting biology of Trypoxulon xanthandrum in Costa Rica with
observations on its spider prey (Hymenoptera: Sphecidae; Araneae: Senoculidae).
J. Kansas Ent. Soc.56, p. 205-216.<person-group>
                  <string-name>
                     <surname>Coville</surname>
                  </string-name>
               </person-group>
               <fpage>205</fpage>
               <volume>56</volume>
               <source>J. Kansas Ent. Soc.</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d448e327a1310">
            <mixed-citation id="d448e331" publication-type="journal">
Dean, D.A., Nyffeler, M. &amp; Sterling, W.L. (1988). Natural enemies of spiders: mud-
dauber wasps in East Texas (Hymenoptera, Schecidae). - Southwestern Entomol.13,
p. 283-290.<person-group>
                  <string-name>
                     <surname>Dean</surname>
                  </string-name>
               </person-group>
               <fpage>283</fpage>
               <volume>13</volume>
               <source>Southwestern Entomol.</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d448e366a1310">
            <mixed-citation id="d448e370" publication-type="journal">
Eberhard, W. (1970). The predatory behaviour of two wasps Agenoideus humilis (Pompilidae)
and Sceliphron caementarium (Sphecidae) on the orb weaving spider Araneus cornutus
(Araneidae). -Psyche77, p. 243-251.<person-group>
                  <string-name>
                     <surname>Eberhard</surname>
                  </string-name>
               </person-group>
               <fpage>243</fpage>
               <volume>77</volume>
               <source>Psyche</source>
               <year>1970</year>
            </mixed-citation>
         </ref>
         <ref id="d448e405a1310">
            <mixed-citation id="d448e409" publication-type="book">
Evans, H.E. &amp; West-Eberhard, M.J. (1970). The wasps. - Redwood Press, England.<person-group>
                  <string-name>
                     <surname>Evans</surname>
                  </string-name>
               </person-group>
               <source>The wasps</source>
               <year>1970</year>
            </mixed-citation>
         </ref>
         <ref id="d448e431a1310">
            <mixed-citation id="d448e435" publication-type="journal">
Ferguson, C.S. &amp; Hunt, J.H. (1989). Near-nest behavior of a solitary mud-daubing
wasp, Sceliphron caementarium (Hymenoptera: Sphecidae). - J. Insect Behav.2,
p. 315-323.<person-group>
                  <string-name>
                     <surname>Ferguson</surname>
                  </string-name>
               </person-group>
               <fpage>315</fpage>
               <volume>2</volume>
               <source>J. Insect Behav.</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d448e470a1310">
            <mixed-citation id="d448e474" publication-type="journal">
Field, J. (1992). Patterns of nest provisioning and parental investment in the solitary digger
wasp Ammophila sabulosa. - Ecol. Entomol.17, p. 43-5 1.<person-group>
                  <string-name>
                     <surname>Field</surname>
                  </string-name>
               </person-group>
               <fpage>43</fpage>
               <volume>17</volume>
               <source>Ecol. Entomol.</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d448e507a1310">
            <mixed-citation id="d448e511" publication-type="journal">
Fowler, H.G. (1987). Life table and behavior of Sceliphron asiaticum (Hymenoptera: Sphe-
cidae). - Bolm. Zool.11, p. 40-45.<person-group>
                  <string-name>
                     <surname>Fowler</surname>
                  </string-name>
               </person-group>
               <fpage>40</fpage>
               <volume>11</volume>
               <source>Bolm. Zool.</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d448e543a1310">
            <mixed-citation id="d448e547" publication-type="journal">
Freeman, B.E. (1980). A population study in Jamaica on adult Sceliphron assimile (Dahlbom)
(Hymenoptera: Sphecidae). - Ecol. Entomol.5, p. 19-30.<person-group>
                  <string-name>
                     <surname>Freeman</surname>
                  </string-name>
               </person-group>
               <fpage>19</fpage>
               <volume>5</volume>
               <source>Ecol. Entomol.</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
         <ref id="d448e579a1310">
            <mixed-citation id="d448e583" publication-type="journal">
-- (1981a). Parental investment and its ecological consequences in the solitary
wasp Sceliphron assimile (Dahlbom) (Sphecidae). - Behav. Ecol. Sociobiol.9,
p. 261-268.<person-group>
                  <string-name>
                     <surname>Freeman</surname>
                  </string-name>
               </person-group>
               <fpage>261</fpage>
               <volume>9</volume>
               <source>Behav. Ecol. Sociobiol.</source>
               <year>1981</year>
            </mixed-citation>
         </ref>
         <ref id="d448e618a1310">
            <mixed-citation id="d448e622" publication-type="journal">
- (1981b). Parental investment, maternal size and population dynamics of a solitary
wasp. - Am. Nat.117, p. 357-362.<object-id pub-id-type="jstor">10.2307/2460534</object-id>
               <fpage>357</fpage>
            </mixed-citation>
         </ref>
         <ref id="d448e638a1310">
            <mixed-citation id="d448e642" publication-type="journal">
- (1982). The comparative distribution and population dynamics in Trinidad of
Sceliphron fistularium (Dahlbom) and S. asiaticum (L.) (Hymenoptera: Sphecidae).-
Biol. J. Linn. Soc.17, p. 343-360.<person-group>
                  <string-name>
                     <surname>Freeman</surname>
                  </string-name>
               </person-group>
               <fpage>343</fpage>
               <volume>17</volume>
               <source>Biol. J. Linn. Soc.</source>
               <year>1982</year>
            </mixed-citation>
         </ref>
         <ref id="d448e677a1310">
            <mixed-citation id="d448e681" publication-type="journal">
- &amp; Johnston, B. (1978). The biology in Jamaica of the adults of the sphecid wasp
Sceliphron assimile Dahlbom. - Ecol. Entomol.3, p. 39-52.<person-group>
                  <string-name>
                     <surname>Freeman</surname>
                  </string-name>
               </person-group>
               <fpage>39</fpage>
               <volume>3</volume>
               <source>Ecol. Entomol.</source>
               <year>1978</year>
            </mixed-citation>
         </ref>
         <ref id="d448e714a1310">
            <mixed-citation id="d448e718" publication-type="journal">
Horner, N.V. &amp; Klein, J.H. (1979). Spider prey of two mud dauber wasp species in Comanche
County, Oklahoma (Hymenoptera: Sphecidae). - Environ. Entomol.8, p. 30-31.<person-group>
                  <string-name>
                     <surname>Horner</surname>
                  </string-name>
               </person-group>
               <fpage>30</fpage>
               <volume>8</volume>
               <source>Environ. Entomol.</source>
               <year>1979</year>
            </mixed-citation>
         </ref>
         <ref id="d448e750a1310">
            <mixed-citation id="d448e754" publication-type="journal">
Hunt, J.H. (1993). Survivorship, fecundity and recruitment in a mud dauber wasp, Sceliphron
assimile (Hymenoptera: Sphecidae). - Ann. Ent. Soc. Am.86, p. 51-59.<person-group>
                  <string-name>
                     <surname>Hunt</surname>
                  </string-name>
               </person-group>
               <fpage>51</fpage>
               <volume>86</volume>
               <source>Ann. Ent. Soc. Am.</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d448e786a1310">
            <mixed-citation id="d448e790" publication-type="journal">
Jiménez, M.L., Servin, R., Tejas, A. &amp; Aguilar, R. (1992). La composicion de preseas de
la avispa lodera Sceliphron jamaicense lucae en la Region Del Cabo, Mexico.-
Southwestern Entomol.17, p. 169-180.<person-group>
                  <string-name>
                     <surname>Jiménez</surname>
                  </string-name>
               </person-group>
               <fpage>169</fpage>
               <volume>17</volume>
               <source>Southwestern Entomol.</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d448e825a1310">
            <mixed-citation id="d448e829" publication-type="journal">
Jocqué, R. (1988). The prey of the mud-dauber wasp, Sceliphron spirifex (Linnaeus), in
Central Africa. - Newsl. Br. arachnol. Soc.51, p. 7.<person-group>
                  <string-name>
                     <surname>Jocqué</surname>
                  </string-name>
               </person-group>
               <fpage>7</fpage>
               <volume>51</volume>
               <source>Newsl. Br. arachnol. Soc.</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d448e861a1310">
            <mixed-citation id="d448e865" publication-type="journal">
Kurczewski, F.E. &amp; Miller, R.C. (1984). Observations of the nesting of three species of
Cerceris (Hymenoptera: Sphecidae). - Florida Entomol.119, p. 189-194.<person-group>
                  <string-name>
                     <surname>Kurczewski</surname>
                  </string-name>
               </person-group>
               <fpage>189</fpage>
               <volume>119</volume>
               <source>Florida Entomol.</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d448e897a1310">
            <mixed-citation id="d448e901" publication-type="journal">
Landes, D.A., Obin, M.S., Cady, A.B. &amp; Hunt, J.H. (1987). Seasonal and latitudinal variation
in spider prey of the mud dauber Chalybion californicum (Hymenoptera: Sphecidae).-
J. Arachnol.15, p. 249-256.<object-id pub-id-type="jstor">10.2307/3705735</object-id>
               <fpage>249</fpage>
            </mixed-citation>
         </ref>
         <ref id="d448e921a1310">
            <mixed-citation id="d448e925" publication-type="journal">
Muma, M.H. &amp; Jeffers, W.F. (1945). Studies of the spider prey of several mud-dauber
wasps. - Ann. Entomol. Soc. Am.38, p. 245-255.<person-group>
                  <string-name>
                     <surname>Muma</surname>
                  </string-name>
               </person-group>
               <fpage>245</fpage>
               <volume>38</volume>
               <source>Ann. Entomol. Soc. Am.</source>
               <year>1945</year>
            </mixed-citation>
         </ref>
         <ref id="d448e957a1310">
            <mixed-citation id="d448e961" publication-type="journal">
Obin, M.S. (1982). Spiders living at wasp nesting sites: what constrains predation by mud-
daubers? - Psyche89, p. 321-335.<person-group>
                  <string-name>
                     <surname>Obin</surname>
                  </string-name>
               </person-group>
               <fpage>321</fpage>
               <volume>89</volume>
               <source>Psyche</source>
               <year>1982</year>
            </mixed-citation>
         </ref>
         <ref id="d448e993a1310">
            <mixed-citation id="d448e997" publication-type="journal">
Rehnberg, B.G. (1987). Selection of spider prey by Trypozylon politum (Say) (Hymenoptera:
Sphecidae). - Can. Entomol.20, p. 121-125.<person-group>
                  <string-name>
                     <surname>Rehnberg</surname>
                  </string-name>
               </person-group>
               <fpage>121</fpage>
               <volume>20</volume>
               <source>Can. Entomol.</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d448e1029a1310">
            <mixed-citation id="d448e1033" publication-type="journal">
Smith, A. (1979). Life strategy and mortality factors of Sceliphron laetum (Smith) (Hy-
menoptera: Sphecidae) in Australia. - Aust. J. Ecol.4, p. 181-186.<person-group>
                  <string-name>
                     <surname>Smith</surname>
                  </string-name>
               </person-group>
               <fpage>181</fpage>
               <volume>4</volume>
               <source>Aust. J. Ecol.</source>
               <year>1979</year>
            </mixed-citation>
         </ref>
         <ref id="d448e1065a1310">
            <mixed-citation id="d448e1069" publication-type="journal">
White, E. (1962). Nest-building and provisioning in relation to sex in Sceliphron spirifex L.
(Sphecidae). - J. Anim. Ecol.31, p. 317-329.<object-id pub-id-type="doi">10.2307/2144</object-id>
               <fpage>317</fpage>
            </mixed-citation>
         </ref>
         <ref id="d448e1085a1310">
            <mixed-citation id="d448e1089" publication-type="book">
Wilkinson, L. (1992). SYSTAT: statistics, version 5.2 edition. - SYSTAT, Inc., Evanston, Il.<person-group>
                  <string-name>
                     <surname>Wilkinson</surname>
                  </string-name>
               </person-group>
               <source>SYSTAT: statistics, version 5.2 edition</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
