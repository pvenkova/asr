<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">jecongrowth</journal-id>
         <journal-id journal-id-type="jstor">j50000470</journal-id>
         <journal-title-group>
            <journal-title>Journal of Economic Growth</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Springer</publisher-name>
         </publisher>
         <issn pub-type="ppub">13814338</issn>
         <issn pub-type="epub">15737020</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">40216118</article-id>
         <article-id pub-id-type="pub-doi">10.1007/s10887-007-9013-3</article-id>
         <title-group>
            <article-title>Conditional Convergence and the Dynamics of the Capital-Output Ratio</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Kieran</given-names>
                  <surname>McQuinn</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Karl</given-names>
                  <surname>Whelan</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>6</month>
            <year>2007</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">12</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id>i40007524</issue-id>
         <fpage>159</fpage>
         <lpage>184</lpage>
         <self-uri xlink:href="https://www.jstor.org/stable/40216118"/>
         <abstract>
            <p>Output per worker can be expressed as a function of technological efficiency and of the capital-output ratio. Because technology is exogenous in the Solow model, all of the endogenous convergence dynamics take place through the adjustment of the capitaloutput ratio. This paper uses the empirical behavior of the capital-output ratio to estimate the speed of conditional convergence of economies towards their steady-State paths. We find that the conditional convergence speed is about seven percent per year. This is somewhat faster than predicted by the Solow model and is significantly higher than reported in most previous studies based on Output per worker regressions. We show that, once there are stochastic shocks to technology, Standard panel econometric techniques produce downward-biased estimates of convergence speeds, while our approach does not.</p>
         </abstract>
         <kwd-group>
            <kwd>Convergence</kwd>
            <kwd>Solow Model</kwd>
            <kwd>Panel Data</kwd>
            <kwd>O41</kwd>
            <kwd>O30</kwd>
            <kwd>C23</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d10e156a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d10e163" publication-type="other">
Islam (1995)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d10e169" publication-type="other">
Caselii et al. (1996).</mixed-citation>
            </p>
         </fn>
         <fn id="d10e176a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d10e183" publication-type="other">
Bond et al. (2001)</mixed-citation>
            </p>
         </fn>
         <fn id="d10e190a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d10e197" publication-type="other">
Chap. 4 of DeLong's (2003)</mixed-citation>
            </p>
         </fn>
         <fn id="d10e204a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d10e211" publication-type="other">
Jones and Scrimgeour (2005)</mixed-citation>
            </p>
         </fn>
         <fn id="d10e219a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d10e226" publication-type="other">
Barro and Sala-i-Martin (1995)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d10e232" publication-type="other">
Romer (2001).</mixed-citation>
            </p>
         </fn>
         <fn id="d10e239a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d10e246" publication-type="other">
Fraumeni (1997)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d10e252" publication-type="other">
www.bea.doc.gov/bea/dn/home/fixedassets.htm.</mixed-citation>
            </p>
         </fn>
         <fn id="d10e259a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d10e266" publication-type="other">
Greenwood, Hercowitz, and Krusell (1997)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d10e272" publication-type="other">
Whelan (2003)</mixed-citation>
            </p>
         </fn>
         <fn id="d10e279a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d10e286" publication-type="other">
Bernanke and Gurkaynak (2002),</mixed-citation>
            </p>
         </fn>
         <fn id="d10e293a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d10e300" publication-type="other">
Durlauf and Johnson (1995)</mixed-citation>
            </p>
         </fn>
         <fn id="d10e307a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d10e314" publication-type="other">
Bond (2002)</mixed-citation>
            </p>
         </fn>
         <fn id="d10e322a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d10e329" publication-type="other">
Caselli et al. (1996)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d10e335" publication-type="other">
Bond et al. (2001)</mixed-citation>
            </p>
         </fn>
         <fn id="d10e342a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d10e349" publication-type="other">
McQuinn and Whelan (2007).</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d10e365a1310">
            <mixed-citation id="d10e369" publication-type="other">
Barro, R., &amp; Sala-i -Martin, X. (1992). Convergence. Journal of Political Economy, 100, 223-251.</mixed-citation>
         </ref>
         <ref id="d10e376a1310">
            <mixed-citation id="d10e380" publication-type="other">
Barro, R., &amp; Sala-i-Martin, X. (1995). Economic Growth. New York: McGraw-Hill.</mixed-citation>
         </ref>
         <ref id="d10e387a1310">
            <mixed-citation id="d10e391" publication-type="other">
Barro, R., Mankiw, N. G., &amp; Sala-i-Martin, X. (1995). Capital mobility in neoclassical models of growth.
American Economic Review, 85, 103-115.</mixed-citation>
         </ref>
         <ref id="d10e401a1310">
            <mixed-citation id="d10e405" publication-type="other">
Bernanke, B., &amp; Gurkaynak, R. (2002). Is growth exogenous? Taking Mankiw, Romer and Weil Seriously.
NBER Macroeconomics Annual 16, 11-57.</mixed-citation>
         </ref>
         <ref id="d10e416a1310">
            <mixed-citation id="d10e420" publication-type="other">
Bond, S., Hoeffler, A., &amp; Temple, J. (2001). GMM estimation of empirical growth models. CEPR Discussion
Paper No. 3048.</mixed-citation>
         </ref>
         <ref id="d10e430a1310">
            <mixed-citation id="d10e434" publication-type="other">
Bond, S. (2002). Dynamic panel data models: A guide to micro data methods and practice. UCL Working
paper CWP09/02.</mixed-citation>
         </ref>
         <ref id="d10e444a1310">
            <mixed-citation id="d10e448" publication-type="other">
Bradford, D. J. (2003). Macroeconomics. New York: McGraw-Hill.</mixed-citation>
         </ref>
         <ref id="d10e455a1310">
            <mixed-citation id="d10e459" publication-type="other">
Caselh, F., Esquivel, G., &amp; Lefort, F. (1996). Reopening the convergence debate: A new look at cross-country
growth empirics. Journal of Economic Growth, 1, 363-389.</mixed-citation>
         </ref>
         <ref id="d10e469a1310">
            <mixed-citation id="d10e473" publication-type="other">
Durlauf, S., &amp; Johnson, P. (1995). Multiple regimes and cross-country growth behaviour. Journal of Applied
Econometrics, 10, 365-384.</mixed-citation>
         </ref>
         <ref id="d10e483a1310">
            <mixed-citation id="d10e487" publication-type="other">
Fraumeni, B. (1997). The measurement of depreciation in the US national income and product Accounts.
Survey of Current Business, July, 7-23.</mixed-citation>
         </ref>
         <ref id="d10e498a1310">
            <mixed-citation id="d10e502" publication-type="other">
Galor, O. (1996). Convergence? Inferences from theoretical models. Economic Journal, 106, 1056-1069.</mixed-citation>
         </ref>
         <ref id="d10e509a1310">
            <mixed-citation id="d10e513" publication-type="other">
Galor, O., &amp; Weil, D. (2000). Population, technology, and growth: From malthusian stagnation to the demo-
graphic transition and beyond. American Economic Review, 90, 806-828.</mixed-citation>
         </ref>
         <ref id="d10e523a1310">
            <mixed-citation id="d10e527" publication-type="other">
Greenwood, J., Hercowitz, Z., &amp; Krussell, P. (1997). Long-run implications of investment-specific technolog-
ical change. American Economic Review, 87, 342-362.</mixed-citation>
         </ref>
         <ref id="d10e537a1310">
            <mixed-citation id="d10e541" publication-type="other">
Hall, R., &amp; Jones, C. I. (1997). Why do some countries produce so much more per worker than others?
Quarterly Journal of Economics, 114, 83-116.</mixed-citation>
         </ref>
         <ref id="d10e551a1310">
            <mixed-citation id="d10e555" publication-type="other">
Heston, A., Summers, R., &amp; Aten, B. (2002). Penn World Table Version 6.1. Pennsylvania: Center for Inter-
national Comparisons at the University of Pennsylvania.</mixed-citation>
         </ref>
         <ref id="d10e565a1310">
            <mixed-citation id="d10e569" publication-type="other">
Islam, N. (1995). Growth empirics: A panel data approach. Quarterly Journal of Economics, 110, 1127-1170.</mixed-citation>
         </ref>
         <ref id="d10e577a1310">
            <mixed-citation id="d10e581" publication-type="other">
Jones, C. I. (2000). A Note on the Closed-Form Solution of the Solow Model. Mimeo: Berkeley.</mixed-citation>
         </ref>
         <ref id="d10e588a1310">
            <mixed-citation id="d10e592" publication-type="other">
Jones, C. I., &amp; Scrimgeour, D. (2005). The steady-state growth theorem: Understanding uzawa (1961). Mimeo:
Berkeley.</mixed-citation>
         </ref>
         <ref id="d10e602a1310">
            <mixed-citation id="d10e606" publication-type="other">
Mankiw, N. G., David, R., &amp; Weil, D. (1992). A contribution to the empirics of economic growth. Quarterly
Journal of Economics, 107, 407-437.</mixed-citation>
         </ref>
         <ref id="d10e616a1310">
            <mixed-citation id="d10e620" publication-type="other">
Mankiw, N. G. (1995). The Growth of Nations. Brookings Papers on Economic Activity, 1, 275-326.</mixed-citation>
         </ref>
         <ref id="d10e627a1310">
            <mixed-citation id="d10e631" publication-type="other">
McQuinn, K., &amp; Whelan, K. (2007) Solow (1956) as a model of cross-country growth dynamics. Oxford
Review of Economic Policy, 23, 1st ed.</mixed-citation>
         </ref>
         <ref id="d10e641a1310">
            <mixed-citation id="d10e645" publication-type="other">
Nickell, S. (1981). Biases in dynamic models with fixed effects. Econometrics 49, 1417-1426.</mixed-citation>
         </ref>
         <ref id="d10e653a1310">
            <mixed-citation id="d10e657" publication-type="other">
Romer, D. (2001). Advanced Macroeconomics. New York: McGraw-Hill.</mixed-citation>
         </ref>
         <ref id="d10e664a1310">
            <mixed-citation id="d10e668" publication-type="other">
Solow, R. (1956). A contribution to the theory of economic growth. Quarterly Journal of Economics, 70,
65-94.</mixed-citation>
         </ref>
         <ref id="d10e678a1310">
            <mixed-citation id="d10e682" publication-type="other">
Whelan, K. (2003). A two-sector approach to modeling US NIPA Data. Journal of Money, Credit, and Banking,
55, 627-656.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
