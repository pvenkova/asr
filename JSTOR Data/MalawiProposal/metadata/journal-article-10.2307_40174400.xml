<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">africaspec</journal-id>
         <journal-id journal-id-type="jstor">j50000361</journal-id>
         <journal-title-group>
            <journal-title>Africa Spectrum</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>German Institute of Global and Area Studies</publisher-name>
         </publisher>
         <issn pub-type="ppub">00020397</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">40174400</article-id>
         <title-group>
            <article-title>Afrika nach dem Ende des Ost-West-Konflikts: Die Notwendigkeit zu einem "Neuen Realismus"</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Winrich</given-names>
                  <surname>Kühne</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>1</month>
            <year>1990</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">25</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id>i40005026</issue-id>
         <fpage>139</fpage>
         <lpage>166</lpage>
         <self-uri xlink:href="https://www.jstor.org/stable/40174400"/>
         <abstract>
            <p>In the 1990ies African politics will have to operate in a dramatically changed international environment. The ideological and military East-West rivalry in the Third World has come to an end. Furthermore, the quick erosion of Marxist-Leninist regimes has heavily discredited socialist approaches, which in the past were of prime importance in Africa. Regional conflicts, like those in Southern Africa and the Horn of Africa, have turned from fields of competition into nuisance-factors for Moscow's and Washington's rapprochement. It may even be that, due to centrifugal national and ethnic forces inside the Soviet Union, Soviet Africa and Third World policy will come to an end altogether. After sketching these far-reaching changes, the author analyses some of their implications for Africa. He upholds that there is little reason to speak of a "victory of capitalism", although the failure of Marxist models is obvious. The abstract and rigid controversy between capitalism and socialism is less relevant to African development in practice than the numerous books and articles written thereon suggest. The analysis of the failure of African economic and political development has to go beyond this outdated controversy and look at the deeper reasons for the failure of centralistic, authoritarian approaches on the one hand and the role of market forces for development on the other. With respect to the future, the author points out that the ideological discussion, i. e. the debate on economic and social justice, ist not closed. Simplistic "free-marketeering" will not end Africa's economic and social plight. There is also the danger of a one-sided debate on the withdrawal of the African State instead of discussing the direction of its transformation into a viable framework for market forces in a "mixed economy". This debate on the economic role of the State has to be integrated with the debate on its democratic character, now sweeping through Africa. In the author's view, the reconciliation between ethno-regionalism and the modern territorial State is the most urgent requirement. The multi- versus one-party rule debate has to be seen in this perspective. Finally, the study concludes that Africa needs a "New Realism" for solving its problems through its own efforts, without ideological dreams. The continent cannot any longer hope to be saved by assistance from outside. There will be even no Marshal-Plans for war-torn regions like Southern Africa and the Horn. Appeals for more economic aid will fall on deaf ears. Past results are seen as to be disappointing. And, more important, billions of dollars of economic and humanitarian aid will be needed to restructure the eastern parts of Europe. /// Dans les années 90, la politique africaine devra opérer dans un contexte international qui aura changé de façon dramatique. La rivalité au sein du Tiers Monde entre l'Ouest et l'Est sur les plans idéologique et militaire est arrivé à sa fin. En outre, l'érosion rapide des régimes marxistes-léninistes a discrédité sévèrement toute forme de socialisme, c'est-à-dire des orientations qui ont eu une importance primordiale dans le passé. Les conflits régionaux, comme ceux de l'Afrique du Sud et de la Corne d'Afrique, ont cessé de représenter des champs de compétition; actuellement ils constituent plutôt des facteurs nuisibles au rapprochement entre Moscou et Washington. Tenant compte des forces centrifuges nationales et ethniques à l'intérieur de l'Union Soviétique, il n'est même pas à exclure que la politique soviétique vis-à-vis de l'Afrique et du Tiers-Monde s'arrêtera complètement. Après avoir esquissé ces changements de grande portée, l'auteur analyse quelques unes de leurs conséquences pour l'Afrique. Il maintient qu'il est peu approprié de parler d'une "victoire du capitalisme", même si la faillite des modèles marxistes est incontestable. Pour le développement africain, l'opposition abstraite et rigide entre socialisme et capitalisme est de moindre portée que ne le suggèrent la multitude de livres et d'articles écrits à ce sujet. L'analyse de l'échec politique et économique du développement africain devra outrepasser cette controverse surannée pour mettre en lumière les conditions plus profondes de l'échec des tentatives centralistes et autoritaires d'une part, et du rôle des forces du marché pour le développement, de l'autre. En ce qui concerne le futur, l'auteur souligne que la discussion idéologique, c'est-à-dire la discussion sur la justice économique et sociale, n'est pas terminée. Une orientation simpliste vers le libre marché n'arrivera pas à mettre fin au désastre économique et social en Afrique. Il y a aussi le danger d'un débat biaisé sur le repli de l'Etat en Afrique au lieu d'une discussion sur sa transformation nécessaire afin de créer un cadre approprié aux forces du marché dans une "économie mixte". Cette discussion sur la rôle économique de l'Etat devra étre liée à celle en train de se répandre à travers toute l'Afrique sur son caractère démocratique. L'auteur constate que la reconciliation entre l'ethno-régionalisme d'une part et l'Etat territorial moderne de l'autre est de première urgence. C'est dans ce contexte qu'il faut situer les controverses sur la question du système multipartite ou unipartite. Finalement, l'article arrive à la conclusion que l'Afrique a besoin d'un "nouveau réalisme" pour résoudre ses problèmes par ses propres forces en renonçant aux rêves idéologiques. Le continent ne peut plus espérer d'être sauvé par des aides extérieures. Il n'y aura même pas de "plans Marshal" pour les régions secouées par la guerre comme l'Afrique du Sud et la Corne d'Afrique. Les appels pour une augmentation de l'aide économique tomberont sur des oreilles sourdes. Les résultats du passé son perçus comme décevant. Et, chose plus importante, il va falloir des milliards de dollars en aide économique et humanitaire pour restructurer les régions orientales de l'Europe.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>ger</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d1333e107a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1333e114" publication-type="other">
Gorbatschow, Michail: Die sozialistische Idee und die revolutionäre Umgestaltung. - In: Der
Spiegel, Dokument (Jan. 1990), S. 1 ff. (2).</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e124a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1333e131" publication-type="other">
dazu Krumwiede, Heinrich W., Det-
lef Nolte: Chile: Auf dem Rückweg zur Demo-
kratie? Baden-Baden 1988</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e144a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1333e153" publication-type="other">
Kühne, Winrich: Sowjetische Afrikapolitik
in der "Ära Gorbatschow". Ebenhausen, Mai
1986, S. 329</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1333e165" publication-type="other">
Kridl-Valkenier, Elisabeth: The Soviet
Union and the Third World - An Economic
Bind. New York 1983;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1333e178" publication-type="other">
Hough, Jerry E.: The
Struggle for the Third World. Soviet Debates
and American Options, Washington 1986</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e191a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1333e200" publication-type="other">
Front File, Southern Africa Brief 2 (Aug. 1988)
12, S. 1 ff.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e211a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d1333e218" publication-type="other">
Adamishin, Anatoli: Zum Zerwürfnis Ver-
urteilt? - In: Neue Zeit (1989) 12, S. 1416.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1333e227" publication-type="other">
Gromyko, Anatoli: Adopt New Poli-
tical Thinking in the Practice of International
Relations. - In: Asia and Africa Today (1989)
4, S. 51 -54(52).</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e243a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d1333e250" publication-type="other">
Polyakov, Grigori: Economic Coopera-
tion from the Viewpoint of Perestroika. - In:
Asia and Africa Today (1988) 5, S. 8 - 11 (10).</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e263a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d1333e270" publication-type="other">
Volkov, Nikolai, Vladimir Popov: Has an
Era of Neocolonialism Materialized? - In: In-
ternational Affairs 64 (1988) 11, S. 107 - 117
(109).</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e286a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d1333e293" publication-type="other">
Zitiert in: Financial Times 10.4.1990.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e300a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d1333e307" publication-type="other">
dazu unter anderem Kühne, Winrich :
"Neuer Realismus" in Moskaus Afrika-Poli-
tik? - In: Aus Politik und Zeitgeschichte (Bei-
lage zu Das Parlament) 12.2.1988, S. 31 - 41.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e323a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d1333e330" publication-type="other">
Financial Mail (Johannesburg) 3.8.
1990 und BBC-SWB, Middle East/W0142, 21.8.
1990.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e344a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d1333e351" publication-type="other">
Davies, R. W.: Gorbachev's Social-
ism in Historical Perspective. - In: New Left
Review (Jan./Feb. 1990) 179, S. 5 - 27.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e364a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d1333e371" publication-type="other">
Halliday, Fred: The Ends of Cold War. -
In: New Left Review (March/Apr. 1990) 180,
S. 5 - 24 (17).</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e384a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d1333e391" publication-type="other">
Mirsky, Georgy: Newly-Independent
States: Ways of Development. - In: Asia and
Africa Today (Sept./Okt. 1987) 5, S. 53 - 59
(53).</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e407a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d1333e414" publication-type="other">
Shejnis, W.: Besonderheiten und Proble-
me des Kapitalismus in den Entwicklungslän-
dern. - In: Sowjetwissenschaft, Gesellschafts-
wissenschaftliche Beiträge (Juli/ August 1987)
4, S. 396 - 412 (398).</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e433a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d1333e442" publication-type="other">
Kiva, A.: Socialist-oriented Countries.
Some Development Problems. - In: Interna-
tional Affairs (Moskau) (Okt. 1984) 10, S. 22 -
29 (27).</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e458a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d1333e465" publication-type="other">
Ders.: Developing Countries, Socialism,
Capitalism. - In: ebd. (1989) 3, S. 54 - 63
(59).</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e479a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d1333e486" publication-type="other">
Ebd., S. 57.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e493a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d1333e500" publication-type="other">
den
gleichnamigen Beitrag von Peter Meyns in
Veroffentlichungen des Fachbereichs 1 der Uni-
versitat-Gesamthochschule-Duisburg, Duisburg
1990, Heft 4.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e519a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d1333e526" publication-type="other">
Slovo, Joe: Has Socialism Failed? - In:
The South African Communist (2nd Quarter,
1990) 121, S. 5 -51.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e539a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d1333e546" publication-type="other">
Ebd., S. 32 und 34.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e553a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d1333e560" publication-type="other">
Jordan, Pallo: Crisis of Conscience in the
SACP. - In: SAPEM (June 1990), S. 28 - 34.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e570a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d1333e577" publication-type="other">
Ebd., d. 15.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e585a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d1333e592" publication-type="other">
Ebd., S. 59.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e599a1310">
            <label>32</label>
            <p>
               <mixed-citation id="d1333e608" publication-type="other">
Front File, IAIS Con-
ference Special 3 (Dez. 1989) 17, S. 6 f.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e618a1310">
            <label>33</label>
            <p>
               <mixed-citation id="d1333e625" publication-type="other">
ausführlich zu Botswana den Sam-
melband von Hasse, Rolf, Elisabeth Zeil-Fahl-
busch (Hrsg.): Botswana, Entwicklung am Ran-
de der Apartheid. Hamburg 1989</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1333e640" publication-type="other">
Afrikanisches Wirtschaftswunder
oder abhängiges Quasi-Homeland Südafrikas?
S. 119 - 154.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e653a1310">
            <label>34</label>
            <p>
               <mixed-citation id="d1333e660" publication-type="other">
Crowder, Michael: Whose Dream Was it
Anyway? Twenty-five Years of African Inde-
pendence. - In: African Affairs 86 (Jan. 1987)
342, S. 7-24 (13).</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e676a1310">
            <label>35</label>
            <p>
               <mixed-citation id="d1333e685" publication-type="other">
Ebenso Ansprenger, rranz: Afrikanische
Politik vor neuen Orientierungen? Die Last der
Privilegien. - In: Der Überbück 26 (1990) 1,
S. 5-7.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e701a1310">
            <label>36</label>
            <p>
               <mixed-citation id="d1333e708" publication-type="other">
Africa Research Bulletin (Economic
Series) 27 (28.2.1990) 1, S. 9823.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e719a1310">
            <label>38</label>
            <p>
               <mixed-citation id="d1333e728" publication-type="other">
Senghaas, Dieter: Europa 2000. Ein Frie-
densplan. Frankfurt 1990, S. 71.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e738a1310">
            <label>40</label>
            <p>
               <mixed-citation id="d1333e747" publication-type="other">
Lofchie, Michael, Stephen K. Com-
mins: Food Deficits and Agricultural Policies in
Tropical Africa. - In: The Journal of Modern
African Studies 20 (März 1982) 1, S. 7 - 25.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e763a1310">
            <label>41</label>
            <p>
               <mixed-citation id="d1333e770" publication-type="other">
Amin, S.: The
Class Struggle in Africa. - In: Revolution 1
(1964)l,S.43 ff.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e783a1310">
            <label>42</label>
            <p>
               <mixed-citation id="d1333e790" publication-type="other">
z. B. Andreyew, I.: The Non-Capi-
talist Way - Socialism and the Developing
Countries. Moskau 1977.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e803a1310">
            <label>43</label>
            <p>
               <mixed-citation id="d1333e810" publication-type="other">
zur Reform-
politik Mosambiks Adam, Erfried: Mozambi-
que: Reform Policy - A Way out of the Cri-
sis? - In: Außenpolitik 39 (1988) 2, S. 182 -
196.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e829a1310">
            <label>44</label>
            <p>
               <mixed-citation id="d1333e836" publication-type="other">
MacGaffey, Janet: Perestroika without
Glasnost: the Need for a New Approach to the
Real Economics of African Countries. - In:
The Carter Center of Emory University (Hrsg.):
Beyond Autocracy in Africa. Atlanta, GA
1989, S. 132 ff.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e860a1310">
            <label>45</label>
            <p>
               <mixed-citation id="d1333e867" publication-type="other">
Miegel, Meinhard: Die Zeit 4.5.1990,
S.41.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e877a1310">
            <label>46</label>
            <p>
               <mixed-citation id="d1333e884" publication-type="other">
Laidi, Zaki: Le déclassement interna-
tional de l'Afrique. - In: Politique Etrangère
53 (1988) 3, S. 667 - 675 (667).</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e897a1310">
            <label>47</label>
            <p>
               <mixed-citation id="d1333e904" publication-type="other">
Peltzer, Roger: Befreiungsmythen. Plä-
doyer für die Revision einiger Leitvorstellungen
der Dritte-Welt-Bewegung. - In: Blätter für
deutsche und internationale Politik 34 (Apr.
1989) 4, S. 446-461 (450).</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e923a1310">
            <label>48</label>
            <p>
               <mixed-citation id="d1333e932" publication-type="other">
auch Lancaster, Carol: The World
Bank's Africa Update: Something Old, Some-
thing New, Something Borrowed, Something
Blue. - In: CSIS Africa Notes (Febr. 1990)
108, S. 1 -4.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e951a1310">
            <label>49</label>
            <p>
               <mixed-citation id="d1333e958" publication-type="other">
Zitiert nach Africa Research Bulletin
(Economic Series) 27 (31.8.1990) 7, S. 10035.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e968a1310">
            <label>50</label>
            <p>
               <mixed-citation id="d1333e975" publication-type="other">
Tetzlaff, Rainer: Daten des Elends in
Afrika. Wege aus der Krise? - In: Der Über-
blick 26 (1990) 1,S. 8- 12(12).</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e989a1310">
            <label>51</label>
            <p>
               <mixed-citation id="d1333e998" publication-type="other">
Schließer, Waldfried: Plan und
Markt als Triebkräfte in der sozialistischen
Produktionsweise. - In: Wirtschaftswissen-
schaften 38 (1990) 2, S. 184 - 197.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e1014a1310">
            <label>53</label>
            <p>
               <mixed-citation id="d1333e1021" publication-type="other">
Alajew, Leonid: Den Blick nach Osten
richten. Wie die Entwicklungsländer Probleme
lösen, die unseren ähnlich sind. - In: Neue
Zeit (1989) 26, S. 13 f.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e1037a1310">
            <label>55</label>
            <p>
               <mixed-citation id="d1333e1044" publication-type="other">
Brandt, Hartmut, Wolfgang Zehender:
Das Debakel Afrikas und mögliche Auswege. -
In: Vierteljahresberichte (Friedrich-Ebert-Stif-
tung) (Sept. 1986) 105, S. 243 - 250 (249).</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e1060a1310">
            <label>56</label>
            <p>
               <mixed-citation id="d1333e1069" publication-type="other">
unter anderem Erwin, Alec: South
Africa's Postapartheid Economy: Planning for
Prosperity. - In: South Africa International
20 (1990) 11, S. 205-210.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e1085a1310">
            <label>57</label>
            <p>
               <mixed-citation id="d1333e1092" publication-type="other">
Das Dokument dieser Beratung, "The
Economy beyond Apartheid", wurde in der
New Nation, 15. - 21.6.1990,</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e1105a1310">
            <label>58</label>
            <p>
               <mixed-citation id="d1333e1112" publication-type="other">
Sklar, Richard L.: Beyond Capitalism
and Socialism in Africa. - In: The Journal of
Modern African Studies 26 (1988) 1, S. 1 - 21
(9).</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e1129a1310">
            <label>59</label>
            <p>
               <mixed-citation id="d1333e1138" publication-type="other">
Zitiert in: International Herald Tribüne
28.2.1990, S. 8.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1333e1147" publication-type="other">
Daily News 24.3.1990.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e1154a1310">
            <label>60</label>
            <p>
               <mixed-citation id="d1333e1161" publication-type="other">
Monitor-Dienst (Afrika) 23.8.1990.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e1168a1310">
            <label>61</label>
            <p>
               <mixed-citation id="d1333e1175" publication-type="other">
Monitor-Dienst (Afrika) 4.1.1990.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e1182a1310">
            <label>63</label>
            <p>
               <mixed-citation id="d1333e1189" publication-type="other">
The
Sunday Mail 23.9.1990;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1333e1198" publication-type="other">
Africa Confidential 31
(12.10.1990) 20, S. 8.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e1208a1310">
            <label>64</label>
            <p>
               <mixed-citation id="d1333e1215" publication-type="other">
Front File, Southern Africa Brief 4
(Febr. 1990) 2, S. 4.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e1225a1310">
            <label>65</label>
            <p>
               <mixed-citation id="d1333e1232" publication-type="other">
Moto (Okt. 1984) 28.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e1240a1310">
            <label>66</label>
            <p>
               <mixed-citation id="d1333e1247" publication-type="other">
Ki-Zerbo, Joseph: Die Geschichte
Schwarz-Afrikas. Wuppertal 1979, S. 677.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e1257a1310">
            <label>67</label>
            <p>
               <mixed-citation id="d1333e1264" publication-type="other">
Weiland, Heribert: Namibia auf dem
Weg zur Unabhängigkeit. - In: Europa-Archiv
44 (1989) 23, S. 711 - 718 (714).</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e1277a1310">
            <label>68</label>
            <p>
               <mixed-citation id="d1333e1286" publication-type="other">
zur Rechtfertigung des Einparteien-
systems in Afrika unter anderem den informati-
ven Aufsatz von Meyns, Peter: The Road to
One-Party Rule in Zambia and Zimbabwe: A
Comparative View. - In. Meyns, P., D.W.
Nabudere (Hrsg.): Democracy and the One-
Party State in Africa. Hamburg 1989, S. 181 -
202.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e1315a1310">
            <label>69</label>
            <p>
               <mixed-citation id="d1333e1322" publication-type="other">
The Carter Center: Beyond
Autocracy in Africa, S. 21 ff.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e1332a1310">
            <label>70</label>
            <p>
               <mixed-citation id="d1333e1341" publication-type="other">
Nzongola-Ntalaja: The Second In-
dependence Movement in Congo-Kinshasa, und
Wamba-di-Wamba, E.: The Experience of
Struggle in the People's Republic of Congo,
beide in: Nyong'o, Peter Anyang (Hrsg.):
Popular Struggles for Democracy in Africa.
London 1987.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e1367a1310">
            <label>71</label>
            <p>
               <mixed-citation id="d1333e1374" publication-type="other">
Anm. 65).</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e1382a1310">
            <label>72</label>
            <p>
               <mixed-citation id="d1333e1389" publication-type="other">
Wamba-di-Wamba, E.: Das Palaver als
Praxis von Kritik und Selbstkritik. - In: Peri-
pherie (1989) 39, S. 21 - 42 (24).</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e1402a1310">
            <label>73</label>
            <p>
               <mixed-citation id="d1333e1409" publication-type="other">
Ki-Zerbo, Die Geschichte Schwarz-Afri-
kas, S. 675</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1333e1418" publication-type="other">
Anm. 65).</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e1425a1310">
            <label>74</label>
            <p>
               <mixed-citation id="d1333e1434" publication-type="other">
dazu die interessante
Kontroverse zwischen den afrikanischen Wis-
senschaftlern Nyongo, Peter Anyang: Political
Instability and the Prospects for Democracy in
Africa. - In: Africa Development (1988) 1 und
Mkandawire, Thandika: Comments on Democ-
racy and Political Instability. - In: Africa
Development 13 (1988) 3, S. 77 - 82.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e1463a1310">
            <label>76</label>
            <p>
               <mixed-citation id="d1333e1470" publication-type="other">
Die Zeit 24.8.1990.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e1477a1310">
            <label>77</label>
            <p>
               <mixed-citation id="d1333e1486" publication-type="other">
The Courier (Nov./Dez. 1989)
118, S. 2-5.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e1496a1310">
            <label>78</label>
            <p>
               <mixed-citation id="d1333e1503" publication-type="other">
Africa Research Bulletin (Economic
Series) 27 (28.2.1990) 1, S. 9824.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e1514a1310">
            <label>79</label>
            <p>
               <mixed-citation id="d1333e1521" publication-type="other">
Handelsblatt 31.9.1990.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e1528a1310">
            <label>80</label>
            <p>
               <mixed-citation id="d1333e1535" publication-type="other">
Ebd.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e1542a1310">
            <label>81</label>
            <p>
               <mixed-citation id="d1333e1551" publication-type="other">
Zitiert nach Entwicklung und Zusammen-
arbeit (E + Z) (1990) 1, S. 21.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e1561a1310">
            <label>82</label>
            <p>
               <mixed-citation id="d1333e1568" publication-type="other">
Zitiert in Monitor-Dienst (Afrika) 3.1.
1990.</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e1578a1310">
            <label>83</label>
            <p>
               <mixed-citation id="d1333e1585" publication-type="other">
Dauderstädt, Michael: Entwicklungspoli-
tik '92: Abkehr von der Dritten Welt. Bonn
1990, S. 1 - 8 (5)</mixed-citation>
            </p>
         </fn>
         <fn id="d1333e1598a1310">
            <label>84</label>
            <p>
               <mixed-citation id="d1333e1607" publication-type="other">
Siehe z. B. die in Arusha/Tansania im
Februar 1990 veranstaltete Konferenz Popular
Participation in the Recovery and Development
Process in Africa;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1333e1622" publication-type="other">
The Weekly Review (Nai-
robi) 2.3.1990.</mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
