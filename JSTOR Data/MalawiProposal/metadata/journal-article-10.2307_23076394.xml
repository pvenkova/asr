<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">socitheo</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100735</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Sociological Theory</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Wiley-Blackwell</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">07352751</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">23076394</article-id>
         <title-group>
            <article-title>The Question of Moral Action: A Formalist Position</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Iddo</given-names>
                  <surname>Tavory</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>12</month>
            <year>2011</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">29</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i23076390</issue-id>
         <fpage>272</fpage>
         <lpage>293</lpage>
         <permissions>
            <copyright-statement>COPYRIGHT ©2011 American Sociological Association</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/23076394"/>
         <abstract>
            <p>This article develops a research position that allows cultural sociologists to compare morality across sociohistorical cases. In order to do so, the article suggests focusing analytic attention on actions that fulfill the following criteria: (a) actions that define the actor as a certain kind of socially recognized person, both within and across fields; (b) actions that actors experience—or that they expect others to perceive—as defining the actor both intersituationally and to a greater extent than other available definitions of self; and (c) actions to which actors either have themselves, or expect others to have, a predictable emotional reaction. Such a position avoids both a realist moral sociology and descriptive-relativism, and provides sociologists with criteria for comparing moral action in different cases while staying attuned to social and historical specificity.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d295e116a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d295e123" publication-type="other">
Abend unpublished</mixed-citation>
            </p>
         </fn>
         <fn id="d295e130a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d295e137" publication-type="other">
Skeptics such as Mackie (1977)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d295e143" publication-type="other">
Abend 2008</mixed-citation>
            </p>
         </fn>
         <fn id="d295e150a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d295e157" publication-type="other">
Smith's (2010)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d295e163" publication-type="other">
Habermas (1985)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d295e169" publication-type="other">
Honneth (1996)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d295e176" publication-type="other">
Irons 1991</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d295e182" publication-type="other">
Turiel 1983</mixed-citation>
            </p>
         </fn>
         <fn id="d295e189a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d295e196" publication-type="other">
Shklovsky ([1917] 1965)</mixed-citation>
            </p>
         </fn>
         <fn id="d295e204a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d295e211" publication-type="other">
Sewell (1996)</mixed-citation>
            </p>
         </fn>
         <fn id="d295e218a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d295e225" publication-type="other">
Hughes (1945)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d295e231" publication-type="other">
Stryker 1968</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d295e237" publication-type="other">
Stryker and
Burke 2000</mixed-citation>
            </p>
         </fn>
         <fn id="d295e247a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d295e254" publication-type="other">
Strawson (1962)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d295e260" publication-type="other">
Foucault (1964)</mixed-citation>
            </p>
         </fn>
         <fn id="d295e267a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d295e274" publication-type="other">
Proudfoot's (1985)</mixed-citation>
            </p>
         </fn>
         <fn id="d295e281a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d295e288" publication-type="other">
Smith's ([1759] 2010)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d295e294" publication-type="other">
Mead [1934]</mixed-citation>
            </p>
         </fn>
         <fn id="d295e301a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d295e308" publication-type="other">
Martineau (1838</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d295e314" publication-type="other">
Abend 2010)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d295e320" publication-type="other">
Abend (2010:561)</mixed-citation>
            </p>
         </fn>
         <fn id="d295e328a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d295e335" publication-type="other">
Rawls's (1987</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d295e341" publication-type="other">
1989)</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>REFERENCES</title>
         <ref id="d295e357a1310">
            <mixed-citation id="d295e361" publication-type="other">
Abend, Gabriel. 2008. "Two Main Problems in the Sociology of Morality." Theory and Society 37:87-125.</mixed-citation>
         </ref>
         <ref id="d295e368a1310">
            <mixed-citation id="d295e372" publication-type="other">
. 2010. "What's Old and What's New About the New Sociology of Morality." Pp. 561-85 in Handbook
of the Sociology of Morality, edited by Steven Hitlin and Stephen Vaisey. New York: Springer.</mixed-citation>
         </ref>
         <ref id="d295e382a1310">
            <mixed-citation id="d295e386" publication-type="other">
. Unpublished. "What the Science of Morality Doesn't Say About Morality."</mixed-citation>
         </ref>
         <ref id="d295e393a1310">
            <mixed-citation id="d295e397" publication-type="other">
Alexander, Jeffrey. 2003. The Meanings of Social Life: A Cultural Sociology. Oxford, UK: Oxford Uni-
versity Press.</mixed-citation>
         </ref>
         <ref id="d295e408a1310">
            <mixed-citation id="d295e412" publication-type="other">
Alexander, Jeffrey and Phillip Smith. 1993. "The Discourse of American Civil Society: A New Proposal
for Cultural Studies." Theory and Society 22:151-207.</mixed-citation>
         </ref>
         <ref id="d295e422a1310">
            <mixed-citation id="d295e426" publication-type="other">
Anscombe, Gertrude E. M. 1958. "Modern Moral Philosophy." Philosophy 33:1-39.</mixed-citation>
         </ref>
         <ref id="d295e433a1310">
            <mixed-citation id="d295e437" publication-type="other">
Asad, Talal. 2003. Formations of the Secular: Christianity, Islam, Modernity. Stanford, CA: Stanford
University Press.</mixed-citation>
         </ref>
         <ref id="d295e447a1310">
            <mixed-citation id="d295e451" publication-type="other">
Barth, Frederik. 1969. Ethnic Groups and Boundaries: The Social Organization of Cultural Difference. Oslo:
Universitetsforlaget.</mixed-citation>
         </ref>
         <ref id="d295e461a1310">
            <mixed-citation id="d295e465" publication-type="other">
Bauman, Zygmunt. 1989. Modernity and the Holocaust. Ithaca, NY: Cornell University Press.</mixed-citation>
         </ref>
         <ref id="d295e472a1310">
            <mixed-citation id="d295e476" publication-type="other">
Beck, Ulrich and Elisabeth Beck-Gernsheim. 1995. The Normal Chaos of Love. Cambridge, UK: Polity.</mixed-citation>
         </ref>
         <ref id="d295e484a1310">
            <mixed-citation id="d295e488" publication-type="other">
Beck-Gernsheim, Elisabeth. 1999. "On the Way to a Post-Familial Family: From a Community of Needs
to Elective Affinity." Pp. 53-71 in Love and Eroticism, edited by Mike Featherstone. London: Sage.</mixed-citation>
         </ref>
         <ref id="d295e498a1310">
            <mixed-citation id="d295e502" publication-type="other">
Becker, Howard S. 1998. Tricks of the Trade: How to Think About Your Research While You're Doing It.
Chicago: University of Chicago Press.</mixed-citation>
         </ref>
         <ref id="d295e512a1310">
            <mixed-citation id="d295e516" publication-type="other">
Beisel, Nicola. 1990. "Class, Culture, and Campaigns Against Vice in Three American Cities, 1872-1892."
American Sociological Review 55:44-62.</mixed-citation>
         </ref>
         <ref id="d295e526a1310">
            <mixed-citation id="d295e530" publication-type="other">
Berger, Peter L. 1967. The Sacred Canopy: Elements of a Sociological Theory of Religion. New York:
Doubleday.</mixed-citation>
         </ref>
         <ref id="d295e540a1310">
            <mixed-citation id="d295e544" publication-type="other">
Boltanski, Luc and Laurent Thévenot. 2006. On Justification: Economies of Worth. Princeton, NJ:
Princeton University Press.</mixed-citation>
         </ref>
         <ref id="d295e554a1310">
            <mixed-citation id="d295e558" publication-type="other">
Bourdieu, Pierre. 1984. Distinction: A Social Critique of the Judgment of Taste. Cambridge, MA: Harvard
University Press.</mixed-citation>
         </ref>
         <ref id="d295e569a1310">
            <mixed-citation id="d295e573" publication-type="other">
. 1990. In Other Words: Essays Towards a Reflexive Sociology. Stanford, CA: Stanford University
Press.</mixed-citation>
         </ref>
         <ref id="d295e583a1310">
            <mixed-citation id="d295e587" publication-type="other">
. 1996. The Rules of Art: Genesis and Structure of the Literary Field. Stanford, CA: Stanford Univer-
sity Press.</mixed-citation>
         </ref>
         <ref id="d295e597a1310">
            <mixed-citation id="d295e601" publication-type="other">
. 2000. Pascalian Meditations. Stanford, CA: Stanford University Press.</mixed-citation>
         </ref>
         <ref id="d295e608a1310">
            <mixed-citation id="d295e612" publication-type="other">
Brubaker, Rogers and Frederik Cooper. 2000. "Beyond 'Identity'." Theory and Society 29:1-47.</mixed-citation>
         </ref>
         <ref id="d295e619a1310">
            <mixed-citation id="d295e623" publication-type="other">
Caplow, Theodore. 1984. "Rule Enforcement Without Visible Means: Christmas Gift Giving in Middle-
town." American Journal of Sociology 89:1306-23.</mixed-citation>
         </ref>
         <ref id="d295e633a1310">
            <mixed-citation id="d295e637" publication-type="other">
Crane, Andrew. 2001. "Unpacking the Ethical Product." Journal of Business Ethics 30:361-73.</mixed-citation>
         </ref>
         <ref id="d295e645a1310">
            <mixed-citation id="d295e649" publication-type="other">
Dimaggio, Paul. 1982. "Cultural Entrepreneurship in Nineteenth-Century Boston, Part I: The Creation
of an Organizational Base for High Culture in America." Media, Culture and Society 4:33-50.</mixed-citation>
         </ref>
         <ref id="d295e659a1310">
            <mixed-citation id="d295e663" publication-type="other">
. 1987. "Classification in Art." American Sociological Review 52:440-55.</mixed-citation>
         </ref>
         <ref id="d295e670a1310">
            <mixed-citation id="d295e674" publication-type="other">
Douglas, Mary. 1966. Purity and Danger: An Analysis of Concepts of Pollution and Taboo. London:
Routledge and Kegan Paul.</mixed-citation>
         </ref>
         <ref id="d295e684a1310">
            <mixed-citation id="d295e688" publication-type="other">
Duneier, Mitchell. 1999. Sidewalk. New York: Farrar Straus and Giroux.</mixed-citation>
         </ref>
         <ref id="d295e695a1310">
            <mixed-citation id="d295e699" publication-type="other">
Durkheim, Emile. [1893] 1984. The Division of Labor in Society. New York: Macmillan.</mixed-citation>
         </ref>
         <ref id="d295e706a1310">
            <mixed-citation id="d295e710" publication-type="other">
. [1902] 1964. Suicide. New York: Free Press of Glencoe.</mixed-citation>
         </ref>
         <ref id="d295e718a1310">
            <mixed-citation id="d295e722" publication-type="other">
. [1912] 1995. The Elementary Forms of Religious Life. New York: Free Press.</mixed-citation>
         </ref>
         <ref id="d295e729a1310">
            <mixed-citation id="d295e733" publication-type="other">
Elias, Norbert. [1939] 1994. The Civilizing Process. Oxford: Blackwell.</mixed-citation>
         </ref>
         <ref id="d295e740a1310">
            <mixed-citation id="d295e744" publication-type="other">
Foot, Philippa. 2001. Natural Goodness. Oxford: Clarendon.</mixed-citation>
         </ref>
         <ref id="d295e751a1310">
            <mixed-citation id="d295e755" publication-type="other">
Foucault, Michel. 1964. Madness and Civilization: A History of Insanity in the Age of Reason. New York:
Pantheon.</mixed-citation>
         </ref>
         <ref id="d295e765a1310">
            <mixed-citation id="d295e769" publication-type="other">
. 1970. The Order of Things: An Archaeology of the Human Sciences. New York: Random House.</mixed-citation>
         </ref>
         <ref id="d295e776a1310">
            <mixed-citation id="d295e780" publication-type="other">
. 1977. Discipline and Punish: The Birth of the Prison. New York: Random House.</mixed-citation>
         </ref>
         <ref id="d295e788a1310">
            <mixed-citation id="d295e792" publication-type="other">
. 1984. "Nietzsche, Genealogy, History." Pp. 76-100 in The Foucault Reader, edited by Paul Rabinow.
New York: Pantheon.</mixed-citation>
         </ref>
         <ref id="d295e802a1310">
            <mixed-citation id="d295e806" publication-type="other">
Garfinkel, Harold. 1967. Studies in Ethnomethodology. Oxford: Blackwell.</mixed-citation>
         </ref>
         <ref id="d295e813a1310">
            <mixed-citation id="d295e817" publication-type="other">
Gecas, Viktor and Peter Burke. 1995. "Self and Identity." Pp. 41-67 in Sociological Perspectives on Social
Psychology, edited by Karen S. Cook, Gary A. Fine, and James S. House. Boston: Allyn and Bacon.</mixed-citation>
         </ref>
         <ref id="d295e827a1310">
            <mixed-citation id="d295e831" publication-type="other">
Geertz, Clifford. 1973. The Interpretation of Cultures. New York: Basic Books.</mixed-citation>
         </ref>
         <ref id="d295e838a1310">
            <mixed-citation id="d295e842" publication-type="other">
Gelerenter, Lior. 2005. The Field of Science Fiction Fandom in Israel: A Bourdieusian Approach. Unpub-
lished Master's Thesis, Tel Aviv University, Israel.</mixed-citation>
         </ref>
         <ref id="d295e852a1310">
            <mixed-citation id="d295e856" publication-type="other">
Goflman, Erving. 1959. The Presentation of Self in Everyday Life. New York: Doubleday.</mixed-citation>
         </ref>
         <ref id="d295e864a1310">
            <mixed-citation id="d295e868" publication-type="other">
. 1963. Stigma: Notes on the Management of Spoiled Identity. New York: Prentice Hall.</mixed-citation>
         </ref>
         <ref id="d295e875a1310">
            <mixed-citation id="d295e879" publication-type="other">
Habermas, Jürgen. 1985. The Theory of Communicative Action (Two Volumes). New York: Beacon Press.</mixed-citation>
         </ref>
         <ref id="d295e886a1310">
            <mixed-citation id="d295e890" publication-type="other">
Haidt, Jonathan. 2001. "The Emotional Dog and its Rational Tail: A Social Intuitionist Approach to
Moral Judgment." Psychological Review 108:814-34.</mixed-citation>
         </ref>
         <ref id="d295e900a1310">
            <mixed-citation id="d295e904" publication-type="other">
Hilton, James L. and William von Hippel. 1996. "Stereotypes." Annual Review of Psychology 47:237-71.</mixed-citation>
         </ref>
         <ref id="d295e911a1310">
            <mixed-citation id="d295e915" publication-type="other">
Hitlin, Steven. 2003. "Values as the Core of Personal Identity: Drawing Links Between Two Theories of
the Self." Social Psychology Quarterly 66:118-37.</mixed-citation>
         </ref>
         <ref id="d295e925a1310">
            <mixed-citation id="d295e929" publication-type="other">
. 2008. Moral Selves, Evil Selves: The Social Psychology of Conscience. New York: Palgrave
MacMilllan.</mixed-citation>
         </ref>
         <ref id="d295e940a1310">
            <mixed-citation id="d295e944" publication-type="other">
Hitlin, Steven and Jane A. Piliavin. 2004. "Values: Reviving a Dormant Concept." Annual Review of
Sociology 30:359-93.</mixed-citation>
         </ref>
         <ref id="d295e954a1310">
            <mixed-citation id="d295e958" publication-type="other">
Hitlin, Steven and Stephen Vaisey. 2010. Handbook of the Sociology of Morality. New York: Springer.</mixed-citation>
         </ref>
         <ref id="d295e965a1310">
            <mixed-citation id="d295e969" publication-type="other">
Honneth, Axel. 1996. The Struggle for Recognition: The Moral Grammar of Social Conflicts. New York:
Polity Press.</mixed-citation>
         </ref>
         <ref id="d295e979a1310">
            <mixed-citation id="d295e983" publication-type="other">
Hughes, Everett C. 1945. "Dilemmas and Contradictions of Status." American Journal of Sociology
50:353-59.</mixed-citation>
         </ref>
         <ref id="d295e993a1310">
            <mixed-citation id="d295e997" publication-type="other">
Hursthouse, Rosalind. 1999. On Virtue Ethics. Oxford: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d295e1004a1310">
            <mixed-citation id="d295e1008" publication-type="other">
Ignatow, Gabriel. 2007. "Theories of Embodied Knowledge: New Directions for Cultural and Cognitive
Sociology?" Journal for the Theory of Social Behaviour 37:115-35.</mixed-citation>
         </ref>
         <ref id="d295e1019a1310">
            <mixed-citation id="d295e1023" publication-type="other">
Irons, William. 1991. "How Did Morality Evolve?" Zygon: Journal of Religion and Science 26:49-89.</mixed-citation>
         </ref>
         <ref id="d295e1030a1310">
            <mixed-citation id="d295e1034" publication-type="other">
James, William. [1907] 1981. Pragmatism. Cambridge, MA: Hackett Publishing Company.</mixed-citation>
         </ref>
         <ref id="d295e1041a1310">
            <mixed-citation id="d295e1045" publication-type="other">
Katz, Jack. 1975. "Essences as Moral Identities: On Verifiability and Responsibility in the Imputations of
Deviance." American Journal of Sociology 80:1369-90.</mixed-citation>
         </ref>
         <ref id="d295e1055a1310">
            <mixed-citation id="d295e1059" publication-type="other">
. 1996. "The Social Psychology of Adam and Eve." Theory and Society 25:1194-1237.</mixed-citation>
         </ref>
         <ref id="d295e1066a1310">
            <mixed-citation id="d295e1070" publication-type="other">
Kierkegaard, Seren. [1843] 1992. Either/Or: A Fragment of Life. London: Penguin.</mixed-citation>
         </ref>
         <ref id="d295e1077a1310">
            <mixed-citation id="d295e1081" publication-type="other">
Krause, Monika. 2010. "Theory as the Practice of Chasing Variables." Paper presented at the Annual
Meeting of the American Sociological Association, Atlanta.</mixed-citation>
         </ref>
         <ref id="d295e1092a1310">
            <mixed-citation id="d295e1096" publication-type="other">
Lamont, Michele. 2000. The Dignity of Working Men: Morality and the Boundaries of Race, Class, and
Immigration. Cambridge, MA: Harvard University Press.</mixed-citation>
         </ref>
         <ref id="d295e1106a1310">
            <mixed-citation id="d295e1110" publication-type="other">
Lamont, Michele and Virag Molnár. 2002. "The Study of Boundaries in the Social Sciences." Annual
Review of Sociology 28:167-95.</mixed-citation>
         </ref>
         <ref id="d295e1120a1310">
            <mixed-citation id="d295e1124" publication-type="other">
Lévinas, Immanuel. 1998. Otherwise than Being or Beyond Essence. Dordrecht, NL: Kluwer Academic
Publishers.</mixed-citation>
         </ref>
         <ref id="d295e1134a1310">
            <mixed-citation id="d295e1138" publication-type="other">
Liebow, Elliot. 1967. Tally's Corner: A Study of Negro Streetcorner Men. Boston: Little, Brown.</mixed-citation>
         </ref>
         <ref id="d295e1145a1310">
            <mixed-citation id="d295e1149" publication-type="other">
Lukes, Steven. 2008. Moral Relativism. New York: Picador.</mixed-citation>
         </ref>
         <ref id="d295e1156a1310">
            <mixed-citation id="d295e1160" publication-type="other">
Maclntyre, Alasdaire. 1984. After Virtue. South Bend, IN: Notre Dame University Press.</mixed-citation>
         </ref>
         <ref id="d295e1168a1310">
            <mixed-citation id="d295e1172" publication-type="other">
Mackie, John L. 1977. Ethics: Inventing Right and Wrong. Harmondsworth, UK: Penguin.</mixed-citation>
         </ref>
         <ref id="d295e1179a1310">
            <mixed-citation id="d295e1183" publication-type="other">
Martin, John Levi. 2003. "What is Field Theory?" American Journal of Sociology 109:1^19.</mixed-citation>
         </ref>
         <ref id="d295e1190a1310">
            <mixed-citation id="d295e1194" publication-type="other">
Marx, Karl. [1844] 1959. The Economic and Philosophical Manuscripts of 1844. Moscow: Progress Pub-
lishers.</mixed-citation>
         </ref>
         <ref id="d295e1204a1310">
            <mixed-citation id="d295e1208" publication-type="other">
May, Elaine T. 1980. Great Expectations: Marriage and Divorce in Post-Victorian America. Chicago:
University of Chicago Press.</mixed-citation>
         </ref>
         <ref id="d295e1218a1310">
            <mixed-citation id="d295e1222" publication-type="other">
Mead, George H. 1934. Mind, Self and Society: From the Standpoint of a Social Behaviorist. Chicago:
Chicago University Press.</mixed-citation>
         </ref>
         <ref id="d295e1232a1310">
            <mixed-citation id="d295e1236" publication-type="other">
Mills, C. Wright. 1940. "Situated Actions and Vocabularies of Motive."American Sociological Review
5:904-13.</mixed-citation>
         </ref>
         <ref id="d295e1247a1310">
            <mixed-citation id="d295e1251" publication-type="other">
Oakley, Justin. 1996. "Varieties of Virtue Ethics." Ratio 9:128-52.</mixed-citation>
         </ref>
         <ref id="d295e1258a1310">
            <mixed-citation id="d295e1262" publication-type="other">
Pachuki, Mark Α., Sabrina Pendergrass, and Michele Lamont. 2007. "Boundary Processes: Recent De-
velopments and New Contributions." Poetics 35:331-51.</mixed-citation>
         </ref>
         <ref id="d295e1272a1310">
            <mixed-citation id="d295e1276" publication-type="other">
Pagis, Michal. 2010. "Producing Intersubjectivity in Silence: An Ethnography of Meditation Practices."
Ethnography 11:309-28.</mixed-citation>
         </ref>
         <ref id="d295e1286a1310">
            <mixed-citation id="d295e1290" publication-type="other">
Pessah, Tom. 2008. "Violent Representations: Hostile Indians and Civilized Wars in the
Nineteenth-Century U.S." Unpublished Master's Thesis: University of California, Berkeley.</mixed-citation>
         </ref>
         <ref id="d295e1300a1310">
            <mixed-citation id="d295e1304" publication-type="other">
Proudfoot, Wayne. 1985. Religious Experience. Berkeley: University of California Press.</mixed-citation>
         </ref>
         <ref id="d295e1311a1310">
            <mixed-citation id="d295e1315" publication-type="other">
Quine, William V. 1960. Word and Object. Cambridge, MA: MIT Press.</mixed-citation>
         </ref>
         <ref id="d295e1323a1310">
            <mixed-citation id="d295e1327" publication-type="other">
. 1987. "Indeterminacy of Translation Again." Journal of Philosophy 84:5-10.</mixed-citation>
         </ref>
         <ref id="d295e1334a1310">
            <mixed-citation id="d295e1338" publication-type="other">
Rawls, Ann W. 1987. "The Interaction Order Sui Generis: Goffman's Contribution to Social Theory."
Sociological Theory 5:136-49.</mixed-citation>
         </ref>
         <ref id="d295e1348a1310">
            <mixed-citation id="d295e1352" publication-type="other">
. 1989. "Language, Self, and Social Order: A Re-evaluation of Goffman and Sacks." Human Studies
12:147-72.</mixed-citation>
         </ref>
         <ref id="d295e1362a1310">
            <mixed-citation id="d295e1366" publication-type="other">
Saussure de, Ferdinand. [1916] 1986. Course in General Linguistics. New York: Open Court.</mixed-citation>
         </ref>
         <ref id="d295e1373a1310">
            <mixed-citation id="d295e1377" publication-type="other">
Schutz, Alfred. 1967. The Phenomenology of the Social World. Evanstor, IL: Northwestern University
Press.</mixed-citation>
         </ref>
         <ref id="d295e1387a1310">
            <mixed-citation id="d295e1391" publication-type="other">
Sewell, William H., Jr. 1992. "A Theory of Structure: Duality, Agency, and Transformation." American
Journal of Sociology 98:1-29.</mixed-citation>
         </ref>
         <ref id="d295e1402a1310">
            <mixed-citation id="d295e1406" publication-type="other">
. 1996. "Historical Events as Transformations of Structures: Inventing Revolution at the Bastille."
Theory and Society 25:841-81.</mixed-citation>
         </ref>
         <ref id="d295e1416a1310">
            <mixed-citation id="d295e1420" publication-type="other">
Shklovsky, Victor. [1917] 1965. "Art as Technique." Pp. 3-25 in Russian Formalist Criticism, edited by
L. T. Lemon and M. Reiss. Lincoln: University of Nebraska Press.</mixed-citation>
         </ref>
         <ref id="d295e1430a1310">
            <mixed-citation id="d295e1434" publication-type="other">
Silber, llana F. 2003. "Pragmatic Sociology as Cultural Sociology: Beyond Repertoire Theory?" European
Journal of Social Theory 6:427-49.</mixed-citation>
         </ref>
         <ref id="d295e1444a1310">
            <mixed-citation id="d295e1448" publication-type="other">
Simmel, Georg. 1950. "The Stranger." Pp. 402-08 in The Sociology of Georg Simmel, translated by
Kurt Wolff. New York: Free Press.</mixed-citation>
         </ref>
         <ref id="d295e1458a1310">
            <mixed-citation id="d295e1462" publication-type="other">
Slote, Michael. 1992. From Morality to Virtue. Oxford: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d295e1469a1310">
            <mixed-citation id="d295e1473" publication-type="other">
Smith, Adam. [1759] 2010. The Theory of Moral Sentiments. London: Penguin.</mixed-citation>
         </ref>
         <ref id="d295e1481a1310">
            <mixed-citation id="d295e1485" publication-type="other">
Smith, Christian. 1998. American Evangelicalism: Embattled and Thriving. Chicago: University of Chicago
Press.</mixed-citation>
         </ref>
         <ref id="d295e1495a1310">
            <mixed-citation id="d295e1499" publication-type="other">
. 2003. Moral, Believing Animals: Human Personhood and Culture. New York: Oxford University
Press.</mixed-citation>
         </ref>
         <ref id="d295e1509a1310">
            <mixed-citation id="d295e1513" publication-type="other">
. 2010. What Is a Person?: Rethinking Humanity, Social Life, and the Moral Good from the Person
Up. Chicago: University of Chicago Press.</mixed-citation>
         </ref>
         <ref id="d295e1523a1310">
            <mixed-citation id="d295e1527" publication-type="other">
Spring, Kimberly. 2010. "The Moral Performance: Reconceptualizing the Sociological Approach to Moral-
ity." Paper presented at the Junior Theorists Symposium, Atlanta.</mixed-citation>
         </ref>
         <ref id="d295e1537a1310">
            <mixed-citation id="d295e1541" publication-type="other">
Steinmetz, George. 2008. "The Colonial State as a Social Field." American Sociological Review 73:
589-612.</mixed-citation>
         </ref>
         <ref id="d295e1551a1310">
            <mixed-citation id="d295e1555" publication-type="other">
Stevenson, Charles L. 1944. Ethics and Language. New Haven, CT: Yale University Press.</mixed-citation>
         </ref>
         <ref id="d295e1563a1310">
            <mixed-citation id="d295e1567" publication-type="other">
Strawson, Peter F. 1962. Freedom and Resentment and Other Essays. London: Methuen.</mixed-citation>
         </ref>
         <ref id="d295e1574a1310">
            <mixed-citation id="d295e1578" publication-type="other">
Stryker, Sheldon. 1968. "Identity Salience and Role Performance." Journal of Marriage and the Family
4:558-64.</mixed-citation>
         </ref>
         <ref id="d295e1588a1310">
            <mixed-citation id="d295e1592" publication-type="other">
Stryker, Sheldon and Peter J. Burke. 2000. "The Past, Present and Future of Identity Theory." Social
Psychology Quarterly 63:284-97.</mixed-citation>
         </ref>
         <ref id="d295e1602a1310">
            <mixed-citation id="d295e1606" publication-type="other">
Swidler, Ann. 1986. "Culture in Action: Symbols and Strategies." American Sociological Review 51:273-86.</mixed-citation>
         </ref>
         <ref id="d295e1613a1310">
            <mixed-citation id="d295e1617" publication-type="other">
Tangney, June P., Jeff Stuewig, and Debra J. Mashek. 2007. "Moral Emotions and Moral Behavior."
Annual Review of Psychology 58:345-72.</mixed-citation>
         </ref>
         <ref id="d295e1627a1310">
            <mixed-citation id="d295e1631" publication-type="other">
Tavory, Iddo. 2010a. Off Melrose: Sustaining Orthodox Jewish Religious Life in a Secular Space. Unpub-
lished Ph.D. Dissertation, University of California, Los Angeles.</mixed-citation>
         </ref>
         <ref id="d295e1642a1310">
            <mixed-citation id="d295e1646" publication-type="other">
. 2010b. "Of Yarmulkes and Categories: Delegating Boundaries and the Phenomenology of Interac-
tional Expectation." Theory and Society 39:49-68.</mixed-citation>
         </ref>
         <ref id="d295e1656a1310">
            <mixed-citation id="d295e1660" publication-type="other">
Tavory, Iddo and Ann Swidler. 2009. "Condom Semiotics: Meaning and Condom Use in Rural Malawi."
American Sociological Review 74:171-89.</mixed-citation>
         </ref>
         <ref id="d295e1670a1310">
            <mixed-citation id="d295e1674" publication-type="other">
Taylor, Charles. 1989. Sources of the Self. Cambridge, MA: Harvard University Press.</mixed-citation>
         </ref>
         <ref id="d295e1681a1310">
            <mixed-citation id="d295e1685" publication-type="other">
Trianosky, Gregory. 1990. "What is Virtue Ethics All About?" American Philosophical Quarterly 27:
335-44.</mixed-citation>
         </ref>
         <ref id="d295e1695a1310">
            <mixed-citation id="d295e1699" publication-type="other">
Turiel, Elliot. 1983. The Development of Social Knowledge: Morality and Convention. Cambridge, UK:
Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d295e1709a1310">
            <mixed-citation id="d295e1713" publication-type="other">
Vaisey, Stephen. 2009. "Motivation and Justification: A Dual-Process Model of Culture in Action."
American Journal of Sociology 114:1675-1715.</mixed-citation>
         </ref>
         <ref id="d295e1724a1310">
            <mixed-citation id="d295e1728" publication-type="other">
Wacquant, Loie. 2002. "Scrutinizing the Street: Poverty, Morality, and the Pitfalls of Urban Ethnography."
American Journal of Sociology 107:1468-1532.</mixed-citation>
         </ref>
         <ref id="d295e1738a1310">
            <mixed-citation id="d295e1742" publication-type="other">
Weber, Klaus, Kathryn L. Heinze, and Michaela DeSoucey. 2008. "Forage for Thought: Mobilizing Codes
in the Movement for Grass-Fed Meat and Dairy Products." Administrative Science Quarterly 53:529-67.</mixed-citation>
         </ref>
         <ref id="d295e1752a1310">
            <mixed-citation id="d295e1756" publication-type="other">
Weber, Max. 1978. Economy and Society: An Outline of Interpretive Sociology. Berkeley: University of
California Press.</mixed-citation>
         </ref>
         <ref id="d295e1766a1310">
            <mixed-citation id="d295e1770" publication-type="other">
Winchester, Daniel. 2008. "Embodying the Faith: Religious Practice and the Making of a Muslim Moral
Habitus." Social Forces 86:1753-80.</mixed-citation>
         </ref>
         <ref id="d295e1780a1310">
            <mixed-citation id="d295e1784" publication-type="other">
Winston, Hella. 2005. Unchosen: The Hidden Lives of Hasidic Rebels. Boston: Beacon Press.</mixed-citation>
         </ref>
         <ref id="d295e1791a1310">
            <mixed-citation id="d295e1795" publication-type="other">
Wuthnow, Robert. 1987. Meaning and Moral Order. Berkeley: University of California Press.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
