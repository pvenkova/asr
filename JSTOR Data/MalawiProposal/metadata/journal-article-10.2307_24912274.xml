<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">diplhist</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50011703</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Diplomatic History</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Scholarly Resources Inc.</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">01452096</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">14677709</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">24912274</article-id>
         <title-group>
            <article-title>The Tuskegee Model of Development in Africa: Another Dimension of the African/African-American Connection</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>MICHAEL O.</given-names>
                  <surname>WEST</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>7</month>
            <year>1992</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">16</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i24912270</issue-id>
         <fpage>371</fpage>
         <lpage>387</lpage>
         <permissions>
            <copyright-statement>©The Society for Historians of American Foreign Relations (SHAFR) 1992</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/24912274"/>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d679e107a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d679e114" publication-type="other">
Holiis R. Lynch, Edward Wilmot Blyden: Pan-Negro Patriot, 1832-1912 (New York,
1967); Wilson Jeremiah Moses, Alexander Crummell: A Study of Civilization and Discontent
(New York, 1989).</mixed-citation>
            </p>
         </fn>
         <fn id="d679e127a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d679e134" publication-type="other">
Louis R. Harlan, Booker T. Washington: The Making of a Black Leader, 1856-1901 (New
York, 1972); idem, Booker T. Washington: The Wizard of Tuskegee, 1901-1915 (New York,
1983).</mixed-citation>
            </p>
         </fn>
         <fn id="d679e147a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d679e154" publication-type="other">
The veteran pan-Africanist scholar George Shepperson has made the very useful distinction
between "Pan-Africanism" with an uppercase "P" and "pan-Africanism" with a lowercase one.
The first is used to describe the series of conferences held between 1900 and 1945 and, after
1945, the Garvey Movement, while the second refers to the more general phenomenon of contacts
and interconnections between continental Africans and diaspora blacks. See Shepperson, "Pan-
Africanism and 'Pan-Africanism': Some Historical Notes," Phyton 23 (December 1962): 346-58.
See also George Padmore, Pan-Africanism or Communism? The Coming Struggle for Africa
(London, 1956); Colin Legum, Pan-Africanism: A Short Political Guide (London, 1962); J.
Ayodele Langley, Pan-Africanism and Nationalism in West Africa: A Study in Ideology and
Social Classes (Oxford, 1973); Imanuel Geiss, The Pan-African Movement: A History of Pan-
Africanism in America, Europe and Africa, trans. Ann Keep (New York, 1974); and P.
Olisanwuche Esedebe, Pan-Africanism: The Idea and Movement, 1776-1963 (Washington,
1982). Milfred C. Fierce, "African-American Interest in Africa and Interaction with West Africa:
The Origins of the Pan-African Idea in the United States, 1900-1919" (Ph.D. diss., Columbia
University, 1976), is one of the few full-length studies to give serious treatment to Washington
and Tuskegee within a larger pan-African context.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e208a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d679e215" publication-type="other">
For a major statement on the genesis and maturation of American overseas expansion see
Emily S. Rosenberg, Spreading the American Dream: American Economic and Cultural
Expansion, 1890-1945 (New York, 1982).</mixed-citation>
            </p>
         </fn>
         <fn id="d679e229a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d679e236" publication-type="other">
La mont D. Thomas, Paul Cuffe: Black Entrepreneur and Pan-Africanist (Urbana, 1988).</mixed-citation>
            </p>
         </fn>
         <fn id="d679e243a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d679e250" publication-type="other">
Miles Mark Fisher, "Lott Carey: The Colonizing Missionary," Journal of Negro History 7
(October 1922): 380-418; Sandy Dwayne Martin, "Black Baptists, Foreign Missions, and African
Colonization, 1814-1882," in Black Americans and the Missionary Movement in Africa, ed.
Sylvia Jacobs (Westport, CT, 1982), 63-76.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e266a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d679e273" publication-type="other">
St. Clair Drake, The Redemption of Africa and Black Religion (Chicago, 1970).</mixed-citation>
            </p>
         </fn>
         <fn id="d679e280a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d679e287" publication-type="other">
See especially Edward W. Blyden, Christianity, Islam and the Negro Race (1887; reprint
ed., Edinburgh, 1967).</mixed-citation>
            </p>
         </fn>
         <fn id="d679e297a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d679e304" publication-type="other">
Appointed consul general to Haiti in 1869, Ebenezer D. Basset! was the first African-
American to serve the United States in a diplomatic capacity. Subsequently a number of African-
Americans were given diplomatic assignments, mostly to Liberia and Haiti. See St. Gair Drake,
"Negro Americans and the Africa Interest," in The American Negro Reference Book, ed. John P.
Davis (Englewood Qiffs, 1966), 664, n. 5.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e323a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d679e330" publication-type="other">
Randall B. Woods, "Blackamerica's Giallenge to European Colonialism: The Waller
Affair, 1891-1895," Journal of Black Studies! (September 1976): 57-77 (quotation on 65).</mixed-citation>
            </p>
         </fn>
         <fn id="d679e341a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d679e348" publication-type="other">
Ibid.; Sylvia M. Jacobs, The African Nexus: Black American Perspectives on the
European Partitioning of Africa, 1880-1920 (Westport, CT, 1981), 127-31.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e358a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d679e365" publication-type="other">
Floyd J. Miller, The Search for a Black Nationality: Black Emigration and Colonization,
1787-1863 (Urbana, 1975); M. R. Delany and Robert Campbell, Search for a Place: Black
Separatism and Africa, 1860 (Ann Arbor, 1969).</mixed-citation>
            </p>
         </fn>
         <fn id="d679e378a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d679e385" publication-type="other">
Edwin S. Redkey, Black Exodus: Black Nationalist and Back-to-Africa Movements, 1890-
1910 (New Haven, 1969); William E. Bittle and Gilbert Geis, The Longest Way Home: Chief
Alfred C. Sam's Back-to-Africa Movement (Detroit, 1964); Willard B. Gatewood, Jr., "Black
Americans and the Quest for Empire, 1898-1903," Journal of Southern History 38 (November
1972): 545-66.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e404a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d679e413" publication-type="other">
Louis R. Harlan, "Booker T. Washington and the White Man's Burden," American
Historical Review 71 (January 1966): 441-67; Edward H. Berman, "Tuskegee in Africa," Journal
of Negro Education 41 (Spring 1972): 99-112; W. Manning Marable, "Booker T. Washington
and African Nationalism," Phyton 35 (December 1974): 398-406; idem, "A Black School in
South Africa," Negro History Bulletin 37 (June-July 1974): 258-61; idem, "Black Skin,
Bourgeois Masks," in Profiles of Self-Determination: African Responses to European
Colonialism in Southern Africa, 1652-Present, ed. David Chanaiwa (Northridge, CA, 1976),
320-45; idem, "John L. Dube and the Politics of Segregated Education in South Africa," in
Independence without Freedom: The Political Economy of Colonial Education in Southern
Africa, ed. Agrippah T. Mugomba and Mougo Nyaggah (Santa Barbara, 1980), 113-28; idem,
"Ambiguous Legacy: Tuskegee's 'Missionary' Impulse and Africa during the Moton
Administration, 1915-1935," in Jacobs, ed., Black Americans, 77-93; Donald Spivey, "The
African Crusade for Black Industrial Schooling," Journal of Negro History 63 (January 1978): 1-
17; R. Hunt Davis, Jr., "The Black American Education Component in African Responses to
Colonialism in South Africa: (ca. 1890-1914)," Journal of Southern African Affairs 3 (January
1978): 65-83.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e467a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d679e474" publication-type="other">
Harlan, "Booker T. Washington"; Berman, "Tuskegee." Liberia, of course, had been
founded in 1822 as a settlement for free black Americans. In 1847 it became an independent state,
governed by the tightly knit Americo-Liberian elite, which discriminated against and often
connived in the brutal exploitation of the indigenous Africans. See, for example, I. K. Sundiata,
Black Scandal: America and the Liberian Labor Crisis, 1929-1936 (Philadelphia, 1980); and
Charles S. Johnson, Bitter Canaan: The Story of the Negro Republic (New Brunswick, 1987).</mixed-citation>
            </p>
         </fn>
         <fn id="d679e497a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d679e504" publication-type="other">
Williams to Washington, 27 September 1898, in The Booker T. Washington Papers, ed.
Louis R. Harlan, 14 vols. (Urbana, 1975), 4:475.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e515a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d679e522" publication-type="other">
Williams to Washington, 8 June 1899, in Washington Papers 5:134.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e529a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d679e536" publication-type="other">
Washington to the editor of the Indianapolis Freeman, 15 July 1899, ibid., 154-57.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e543a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d679e550" publication-type="other">
Williams to Washington, ca. July 1899, ibid., 166-67; Garence G. Contee, "The
Emergence of Du Bois as an African Nationalist," Journal of Negro History 54 (January 1969):
48-63.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e563a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d679e570" publication-type="other">
"The Opening Address of the International Conference on the Negro," 17 April 1912, in
Washington Papers 11:520-22.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e580a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d679e587" publication-type="other">
An Announcement of a Conference at Tuskegee Institute," ca. March 1911, ibid., 72-73.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e594a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d679e601" publication-type="other">
Robert E. Park, "The International Conference on the Negro," Southern Workman 41:6
(1912): 347-52.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e612a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d679e621" publication-type="other">
Ibid. The campaign to export the Tuskegee model to Africa was not without its African-
American critics, chief among whom were Du Bois and Carter G. Woodson, founder of the
Association for Study of Negro (now Afro-American) Life and History and the Journal of Negro
History. See, for example, W. E. Burghardt Du Bois, "Education in Africa: A Review of the
Recommendations of the African Education Committee," Crisis 32:2 (1926): 86-89; and C. G.
Woodson, "Thomas Jesse Jones," Journal of Negro History 35 (January 1950): 107-9. For a
fuller discussion of the African-American response to the colonization of Africa see Jacobs, The
African Nexus.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e650a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d679e657" publication-type="other">
Marable, "Ambiguous Legacy"; Berman, "Tuskegee"; lan Duffield, "Some American
Influences on Dusé Mohamed Ali," in Pan-African Biography, ed. Robert A. Hill (Los Angeles,
1987), 36-38.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e670a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d679e677" publication-type="other">
Fierce, "African-American Interest in Africa," 275.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e684a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d679e691" publication-type="other">
Carol Page, "Conrad A. Rideout, Afro-American Advisor to the Chiefs of Lesotho and
Pondoland, 1899-1903," in Hill, ed., Pan-African Biography, 1-10.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e701a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d679e708" publication-type="other">
Shula Marks, "The Ambiguities of Dependence: John L. Dube of Natal," Journal of
Southern African Studies 1 (April 1975): 162-80.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e718a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d679e725" publication-type="other">
John Dube, "Need of Industrial Education in Africa," Southern Workman 27:7 (1897):
141-42.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e736a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d679e743" publication-type="other">
Marable, "A Black School," 258.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e750a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d679e757" publication-type="other">
Quoted in R. Hunt Davis, Jr., "John L. Dube: A South African Exponent of Booker T.
Washington," Journal of African Studies 2:4 (1975/76): 497-98.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e767a1310">
            <label>31</label>
            <p>
               <mixed-citation id="d679e774" publication-type="other">
Peter Walshe, The Rise of African Nationalism in South Africa: The African National
Congress, 1912-1952 (Berkeley, 1971), 147; Davis, "John L. Dube," 512-13, n. 59. (The
quotation is from Walshe.)</mixed-citation>
            </p>
         </fn>
         <fn id="d679e787a1310">
            <label>32</label>
            <p>
               <mixed-citation id="d679e794" publication-type="other">
Spivey, "The African Crusade," 5.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e801a1310">
            <label>33</label>
            <p>
               <mixed-citation id="d679e808" publication-type="other">
Edwin W. Smith, Aggrey of Africa: A Study in Black and White (London, 1929); Kenneth
King, "James E. K. Aggrey: Collaborator, Nationalist, Pan-African," Canadian Journal of African
Studies 3:3 (1970): 511-30; Thomas C. Howard, "West Africa and the American South: Notes on
James E. K. Aggrey and the Idea of a University of West Africa," Journal of African Studies 2:4
(1975/76): 445-65.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e827a1310">
            <label>34</label>
            <p>
               <mixed-citation id="d679e834" publication-type="other">
One of Washington's most famous parodies of educated blacks was a story supposedly
told him by a friend who had visited Liberia. An African, it was said, had shut himself up in a hut
reading Cicero. "But the trouble with the colored man was that he had on no pants. I want a tailor
shop first so that the negro can sit down and read Cicero's orations like a gentleman with his pants
on." See Harlan, "Booker T. Washington," 442.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e854a1310">
            <label>35</label>
            <p>
               <mixed-citation id="d679e861" publication-type="other">
On Garveyism in South Africa see Robert A. Hill and Gregory A. Pirio, " 'Africa for the
Africans'. The Garvey Movement in South Africa, 1920-1940," in The Politics of Race, Class
and Nationalism in Twentieth Century South Africa, ed. Shula Marks and Stanley Trapido (New
York, 1987), 209-53.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e877a1310">
            <label>36</label>
            <p>
               <mixed-citation id="d679e884" publication-type="other">
Among the missionaries who "founded" Aggrey were African-Americans from the
African Methodist Episcopal Zion Church. It was often assumed, both in Africa and the United
States, that Aggrey was an African-American. Smith, Aggrey, 176.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e897a1310">
            <label>37</label>
            <p>
               <mixed-citation id="d679e904" publication-type="other">
Lawrence Vambe, From Rhodesia to Zimbabwe (Pittsburgh, 1976), 83-84.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e911a1310">
            <label>38</label>
            <p>
               <mixed-citation id="d679e918" publication-type="other">
%t. Clair Drake, "Mbuyi Koinange and the Pan-African Movement," in Hill, ed., Pan-
African Biography, 164.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e928a1310">
            <label>39</label>
            <p>
               <mixed-citation id="d679e935" publication-type="other">
Nnamdi Azikiwe, My Odyssey: An Autobiography (New York, 1970), 36-37.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e942a1310">
            <label>40</label>
            <p>
               <mixed-citation id="d679e949" publication-type="other">
Azikiwe, needless to say, recognized the limitations of Aggrey's views in the context of an
anticolonial struggle. Thus, while acknowledging Aggrey's pivotal role in sparking his
"intellectual curiosity," Azikiwe attributes his "ambitions to be of service for the redemption of
Africa" to Garvey. See Azikiwe, My Odyssey, 66.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e966a1310">
            <label>41</label>
            <p>
               <mixed-citation id="d679e973" publication-type="other">
Southern Rhodesia Native Association, Minutes.... 16 August 1929, S138/41, National
Archives of Zimbabwe.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e983a1310">
            <label>42</label>
            <p>
               <mixed-citation id="d679e990" publication-type="other">
Samkange to Barnett, 22 January 1954, Claude Barnett Papers, box 174, folder: Rhodesia,
North and South 1951-65, Correspondence, Chicago Historical Society, Chicago, Illinois;
Michael Oliver West, "African Middle-Class Formation in Colonial Zitnbabwe, 1890-1965"
(Ph.D. diss., Harvard University, 1990), 65-70.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e1006a1310">
            <label>43</label>
            <p>
               <mixed-citation id="d679e1013" publication-type="other">
Bantu Mirror (Bulawayo, Southern Rhodesia), 2 July 1955.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e1020a1310">
            <label>44</label>
            <p>
               <mixed-citation id="d679e1027" publication-type="other">
West, "African Middle-Class," 278-85.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e1034a1310">
            <label>45</label>
            <p>
               <mixed-citation id="d679e1041" publication-type="other">
Bantu Mirror, 6 February 1937.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e1048a1310">
            <label>46</label>
            <p>
               <mixed-citation id="d679e1055" publication-type="other">
Booker T. Washington, Up from Slavery: An Autobiography (New York, 1901), 223.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e1063a1310">
            <label>47</label>
            <p>
               <mixed-citation id="d679e1070" publication-type="other">
Ian Duffield, "The Business Activities of Dusé Mohammed Ali: An Example of the
Economic Dimension of Pan-Africanism, 1912-1945," Journal of the Historical Society of
Nigeria 4:4 (1969): 571-600; idem, "Pan-Africanism, Rational and Irrational," Journal of African
History 18:4 (1977): 597-620; and idem, "Some American Influences," in Hill, ed., Pan-African
Biography, 11-56.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e1089a1310">
            <label>48</label>
            <p>
               <mixed-citation id="d679e1096" publication-type="other">
Harlan, "Booker T. Washington."</mixed-citation>
            </p>
         </fn>
         <fn id="d679e1103a1310">
            <label>49</label>
            <p>
               <mixed-citation id="d679e1110" publication-type="other">
South African Native Affairs Commission: Evidence, 103, N3/6/3, National Archives of
Zimbabwe.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e1120a1310">
            <label>50</label>
            <p>
               <mixed-citation id="d679e1127" publication-type="other">
Meacham to Reuling, 27 December 1946, ABC 15.6, v. 11, 4:2, Houghton Library,
Harvard University.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e1137a1310">
            <label>51</label>
            <p>
               <mixed-citation id="d679e1144" publication-type="other">
For a general overview of the development in Africa of the Christian missionary
enterprise, including its educational component, see C. P. Groves, The Planting of Christianity in
Africa, 4 vols. (London, 1948-1958).</mixed-citation>
            </p>
         </fn>
         <fn id="d679e1157a1310">
            <label>52</label>
            <p>
               <mixed-citation id="d679e1164" publication-type="other">
Thomas Jesse Jones, Education in Africa: A Study of West, South and Equatorial Africa
by the African Education Commission (New York, 1922); idem, Education in East Africa: A
Study of East, Central and South Africa by the Second African Education Commission (New
York, 1925).</mixed-citation>
            </p>
         </fn>
         <fn id="d679e1181a1310">
            <label>53</label>
            <p>
               <mixed-citation id="d679e1188" publication-type="other">
Edward H. Berman, "American Influence on African Education: The Role of the Phelps-
Stokes Fund's Education Commissions," Comparative Education Review 15:2 (1971): 132-45.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e1198a1310">
            <label>54</label>
            <p>
               <mixed-citation id="d679e1205" publication-type="other">
Kenneth J. King, "Africa and the Southern States of the U.S.A.: Notes on J. H. Oldham
and American Negro Education for Africans," Journal of African History 10:4 (1969): 659-77;
idem, "African Students in Negro American Colleges: Notes on the Good African," Phylon 31
(October 1970): 16-30; George Bennett, "Paramountcy to Partnership: J. H. Oldham and Africa,"
Africa 30:1 (1960): 356-61. In this connection see also Kenneth James King, Pan-Africanism and
Education: A Study of Race, Philanthropy and Education in the Southern States of America and
East Africa (Oxford, 1971); and Fred H. Matthews, "Robert Park, Congo Reform and Tuskegee:
The Molding of a Race Relations Expert, 1905-1913," Canadian Journal of History 8 (March
1973): 37-65.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e1237a1310">
            <label>55</label>
            <p>
               <mixed-citation id="d679e1244" publication-type="other">
Education Policy in British Tropical Africa, Cmd. 2374 (London, 1925), 7.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e1251a1310">
            <label>56</label>
            <p>
               <mixed-citation id="d679e1258" publication-type="other">
Jones, Education in Africa, 17.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e1265a1310">
            <label>57</label>
            <p>
               <mixed-citation id="d679e1272" publication-type="other">
King, "Africa and the Southern States"; idem, "African Students"; R. Hunt Davis, Jr.,
"Producing the 'Good African': South Carolina's Penn School as a Guide for African Education
in South Africa," in Mugomba and Nyaggah, eds., Independence without Freedom, 83-112.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e1285a1310">
            <label>58</label>
            <p>
               <mixed-citation id="d679e1292" publication-type="other">
R. Hunt Davis, Jr., "Charles T. Lorain and an American Model for African Education in
South Adrian" African Studies Review 19 (September 1976): 87-99; Berman, "Tuskegee."</mixed-citation>
            </p>
         </fn>
         <fn id="d679e1303a1310">
            <label>59</label>
            <p>
               <mixed-citation id="d679e1310" publication-type="other">
Charles T. Loram, The Education of the South African Native (1917; reprint ed., New
York, 1969). On this head see also R. H. W. Shepherd, Literature for the South African Bantu: A
Comparative Study in Negro Achievement (Pretoria, 1936).</mixed-citation>
            </p>
         </fn>
         <fn id="d679e1323a1310">
            <label>60</label>
            <p>
               <mixed-citation id="d679e1330" publication-type="other">
Loram, Education.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e1337a1310">
            <label>61</label>
            <p>
               <mixed-citation id="d679e1344" publication-type="other">
Yet, interestingly enough, the apartheid regime's notorious "Bantu" education policy,
which was promulgated in the 1950s, had much in common with Loram's industrial educational
program. The former, like the latter, was based on the assumption that Africans should receive
only such training as would equip them for their assigned subservient role in the South African
economy and society.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e1363a1310">
            <label>62</label>
            <p>
               <mixed-citation id="d679e1370" publication-type="other">
Davis, "Charles T. Loram"; Berman, "Tuskegee."</mixed-citation>
            </p>
         </fn>
         <fn id="d679e1377a1310">
            <label>63</label>
            <p>
               <mixed-citation id="d679e1386" publication-type="other">
0n this point see John W. Cell, The Highest Stage of White Supremacy: The Origins of
Segregation in South Africa and the American South (Cambridge, 1982); George M. Fredrickson,
White Supremacy: A Comparative Study in American and South African History (New York,
1981); and Stanley B. Greenberg, Race and State in Capitalist Development: Comparative
Perspectives (New Haven, 1980).</mixed-citation>
            </p>
         </fn>
         <fn id="d679e1405a1310">
            <label>64</label>
            <p>
               <mixed-citation id="d679e1412" publication-type="other">
Brian Willan, Sol Plaatje: South African Nationalist, 1876-1932 (Berkeley, 1984), 110—
11.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e1423a1310">
            <label>65</label>
            <p>
               <mixed-citation id="d679e1430" publication-type="other">
As what appears to be the beginning of the end of white rule in South Africa approaches,
the African-American example seems to have lost none of its luster for some black South
Africans. Thus, recently a large delegation of black South African businessmen, anticipating the
unfettering of African entrepreneurial talent in a post-apartheid society, visited the United States
to learn from the experiences of African-American capitalists. Asked about the warm reception
accorded the members of the delegation by their African-American hosts, one of the visiting
South Africans explained: "It's because of the identity we have. They see themselves in the mirror
that's me." See Chicago Tribune, 10 September and 18 October 1990.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e1459a1310">
            <label>66</label>
            <p>
               <mixed-citation id="d679e1466" publication-type="other">
"Work" over "agitation" is how a white newspaper editor in Southern Rhodesia
formulated it in a heading he gave to a letter from an African admirer (and plagiarizer) of
Washington. See Native Mirror (Bulawayo), April 1932.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e1479a1310">
            <label>67</label>
            <p>
               <mixed-citation id="d679e1486" publication-type="other">
Rayford W. Logan, The Negro in American Life and Thought: The Nadir, 1877-1901
(New York, 1954).</mixed-citation>
            </p>
         </fn>
         <fn id="d679e1496a1310">
            <label>68</label>
            <p>
               <mixed-citation id="d679e1503" publication-type="other">
The term "cultural expansionists" is Emily Rosenberg's. See Rosenberg, Spreading the
American Dream.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e1513a1310">
            <label>69</label>
            <p>
               <mixed-citation id="d679e1520" publication-type="other">
Blyden's reputation as a pan-African hero is somewhat ironic, given the zeal with which in
the late nineteenth century he promoted British imperialism in West Africa, even going as far as
faulting the British for not being vigorous enough in their advance into the interior, especially in
view of growing French influence there. This reputation, no doubt, is based on Blyden's
"Afrocentric" cultural nationalism. He is, for instance, often credited with coining the term
"African personality." By contrast, many of his contemporaries, like Alexander Crummell, were
much more "Eurocentric" in their cultural outlook.</mixed-citation>
            </p>
         </fn>
         <fn id="d679e1546a1310">
            <label>70</label>
            <p>
               <mixed-citation id="d679e1553" publication-type="other">
Frantz Fanon, The Wretched of the Earth, trans. Constance Farrington (New York, 1965),
esp. the chap, entitled, "Pitfalls of National Consciousness."</mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
