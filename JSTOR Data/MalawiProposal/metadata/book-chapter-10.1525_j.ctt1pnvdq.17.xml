<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt14w16w</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1pnvdq</book-id>
      <subj-group>
         <subject content-type="call-number">RA644.T7P28 1989</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Tuberculosis</subject>
         <subj-group>
            <subject content-type="lcsh">South Africa</subject>
            <subj-group>
               <subject content-type="lcsh">History</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Anthropology</subject>
      </subj-group>
      <book-title-group>
         <book-title>White Plague, Black Labor</book-title>
         <subtitle>Tuberculosis and the Political Economy of Health and Disease in South Africa</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Packard</surname>
               <given-names>Randall M.</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>07</day>
         <month>10</month>
         <year>1989</year>
      </pub-date>
      <isbn content-type="ppub">9780520065741</isbn>
      <isbn content-type="epub">9780520909120</isbn>
      <publisher>
         <publisher-name>University of California Press</publisher-name>
         <publisher-loc>Berkeley; Los Angeles</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>1989</copyright-year>
         <copyright-holder>The Regents of the University of California</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.1525/j.ctt1pnvdq"/>
      <abstract abstract-type="short">
         <p>Why does tuberculosis, a disease which is both curable and preventable, continue to produce over 50,000 new cases a year in South Africa, primarily among blacks? In answering this question Randall Packard traces the history of one of the most devastating diseases in twentieth-century Africa, against the background of the changing political and economic forces that have shaped South African society from the end of the nineteenth century to the present. These forces have generated a growing backlog of disease among black workers and their families and at the same time have prevented the development of effective public health measures for controlling it. Packard's rich and nuanced analysis is a significant contribution to the growing body of literature on South Africa's social history as well as to the history of medicine and the political economy of health.</p>
      </abstract>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pnvdq.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pnvdq.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pnvdq.3</book-part-id>
                  <title-group>
                     <title>List of Tables and Graphs</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pnvdq.4</book-part-id>
                  <title-group>
                     <title>Abbreviations</title>
                  </title-group>
                  <fpage>xii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pnvdq.5</book-part-id>
                  <title-group>
                     <title>[Map]</title>
                  </title-group>
                  <fpage>xiv</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pnvdq.6</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <fpage>xv</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pnvdq.7</book-part-id>
                  <title-group>
                     <title>Introduction:</title>
                     <subtitle>Industrialization and the Political Economy of Tuberculosis</subtitle>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>Tuberculosis was the number one cause of death in Europe and America from the late eighteenth to the early twentieth century.¹ Although the disease was by no means unknown before this time, its impact on human populations increased tremendously during the early years of the industrial revolution. The rise of industrial development and the growth of cities in both Europe and America produced ideal conditions for the spread of the disease. The men and women who flocked from rural communities to find employment in the factories and mills of Manchester and Birmingham, Lowell and Fall River had little prior contact</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pnvdq.8</book-part-id>
                  <title-group>
                     <label>CHAPTER I</label>
                     <title>Preindustrial South Africa:</title>
                     <subtitle>A Virgin Soil for Tuberculosis?</subtitle>
                  </title-group>
                  <fpage>22</fpage>
                  <abstract>
                     <p>Western-trained medical authorities who have studied the epidemiology of tuberculosis in South Africa have been nearly unanimous in asserting that the African populations of the region were, for all practical purposes, free from the disease prior to their contact with Europeans. Africans in effect represented, like island populations in the Pacific, a virgin soil for TB.</p>
                     <p>The absence of tuberculosis among African peoples was noted in a number of medical writings and travelers’ accounts from the eighteenth and early nineteenth centuries. In the latter half of the nineteenth century, as TB began to appear more frequently among Africans, it was</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pnvdq.9</book-part-id>
                  <title-group>
                     <label>CHAPTER II</label>
                     <title>Urban Growth, “Consumption,” and the “Dressed Native” 1870–1914</title>
                  </title-group>
                  <fpage>33</fpage>
                  <abstract>
                     <p>Until the 1870s the South African economy was dominated by herding and agriculture. Its African population was primarily rural, living on farms and in kraals and small towns scattered over the African veld. Few Africans, other than those residing in royal homesteads, lived in large concentrated settlements and only a small number of European towns, such as Grahamstown (1856) and Uitenhage (1836) in the eastern Cape, possessed a sufficiently large African population to warrant the establishment of an African location. In the Transvaal, the Volksraad went so far as to decree in 1844 that no African could settle near a</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pnvdq.10</book-part-id>
                  <title-group>
                     <label>CHAPTER III</label>
                     <title>Black Mineworkers and the Production of Tuberculosis, 1870–1914</title>
                  </title-group>
                  <fpage>67</fpage>
                  <abstract>
                     <p>Although the major urban centers of South Africa provided important foci for the initial spread of tuberculosis in southern Africa, it was the mines in Kimberley and especially on the Rand that were the major producers of tuberculosis prior to World War I.¹ The immense size of the mine labor force, over 200,000 on the Rand alone by 1910, together with the appalling health conditions that existed on the mines, ensured that they would play a central role in the early development of TB in southern Africa. Though African mortality rates from TB were generally lower on the mines than</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pnvdq.11</book-part-id>
                  <title-group>
                     <label>CHAPTER IV</label>
                     <title>Migrant Labor and the Rural Expansion of Tuberculosis, 1870–1938</title>
                  </title-group>
                  <fpage>92</fpage>
                  <abstract>
                     <p>Just prior to World War I, a settled urban African population was beginning to emerge in the towns and cities of South Africa. Yet most Africans who lived and worked in urban centers were temporary sojourners, moving back and forth between the towns and the countryside. Within the gold-mining industry, where temporary employment for periods of six to nine months was the norm, the turnover of employees was nearly 100 percent within any given calendar year. By 1910 this meant that several hundred thousand men a year moved back and forth between the mines and their rural homes. This general</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pnvdq.12</book-part-id>
                  <title-group>
                     <label>CHAPTER V</label>
                     <title>Slumyards and the Rising Tide of Tuberculosis, 1914–1938</title>
                  </title-group>
                  <fpage>126</fpage>
                  <abstract>
                     <p>World War I marked the beginning of a period of rapid industrial growth in South Africa centered on the development of secondary industries and accompanied by a dramatic expansion of urban populations. These changes were paralleled by a steady rise in TB mortality. African TB mortality rates peaked during World War I and then, following a brief period of remission after the war, rose again through the mid-1930s. The increase in postwar TB mortality indicates that the conditions under which Africans lived and worked in the urban centers of South Africa became progressively worse during the 1920s and 1930s. This</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pnvdq.13</book-part-id>
                  <title-group>
                     <label>CHAPTER VI</label>
                     <title>Labor Supplies and Tuberculosis on the Witwatersrand 1913–1938</title>
                  </title-group>
                  <fpage>159</fpage>
                  <abstract>
                     <p>Although tuberculosis morbidity and mortality rates rose steadily for blacks living within both the urban and rural areas of South Africa from World War I through the mid-1930s, they dropped dramatically within the country’s gold-mining industry. TB rates on the Rand, in fact, mirrored the mortality rates of the major urban centers, excluding those of Johannesburg, where TB rates were shaped by the experience of the mining industry. Whereas urban TB mortality rates rose during World War I and then again from the mid-1920s to the mid-1930s, TB incidence on the mines fell during both of these periods. Overall between</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pnvdq.14</book-part-id>
                  <title-group>
                     <label>CHAPTER VII</label>
                     <title>Segregation and Racial Susceptibility:</title>
                     <subtitle>The Ideological Foundations of Tuberculosis Control, 1913–1938</subtitle>
                  </title-group>
                  <fpage>194</fpage>
                  <abstract>
                     <p>For medical officers working on the Rand and in other urban and industrial centers of South Africa, the struggle against TB in the 1920s and 1930s was an exercise in holding back the tide. TB control was based on a policy of exclusion rather than amelioration. Sanitary segregation, slum clearance, and medical screening were instruments for keeping TB off the Rand and away from white urban populations, and had little or nothing to do with improving African health.</p>
                     <p>The response of white medical authorities to the rising tide of TB during this period represented an extension of health established at</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pnvdq.15</book-part-id>
                  <title-group>
                     <label>CHAPTER VIII</label>
                     <title>Industrial Expansion, Squatters, and the Second Tuberculosis Epidemic, 1938–1948</title>
                  </title-group>
                  <fpage>211</fpage>
                  <abstract>
                     <p>If efforts to control tuberculosis in South Africa during the 1920s and 1930s were an exercise in building social barriers against the rising tide of tubercular infection, the late 1930s and 1940s must be seen as a period in which these barriers were swept aside by a crashing new wave of disease. The tendency for urban authorities to deal with TB by applying exclusionary social control measures, rather than by dealing directly with the conditions under which Africans lived and worked, had left a legacy of overcrowding, malnutrition, and disease within the African townships, locations, and peri-urban slums surrounding the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pnvdq.16</book-part-id>
                  <title-group>
                     <label>CHAPTER IX</label>
                     <title>Tuberculosis and Apartheid:</title>
                     <subtitle>The Great Disappearing Act, 1948–1980</subtitle>
                  </title-group>
                  <fpage>249</fpage>
                  <abstract>
                     <p>The victory of the Nationalist party in 1948 brought to power a government whose political agenda was markedly different from that of its predecessor. The Nationalists represented a constellation of interest groups who were diametrically opposed to the pattern of social and economic development that had shaped South African society over the previous twenty years. They rejected out of hand the view of the Fagan and Smit Commissions that the development of a permanent urbanized African population was both inevitable and essential for the economic development of the country. In its place they reasserted the Stallardist doctrine that the African’s</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pnvdq.17</book-part-id>
                  <title-group>
                     <title>Epilogue:</title>
                     <subtitle>The Present and Future of Tuberculosis in South Africa</subtitle>
                  </title-group>
                  <fpage>299</fpage>
                  <abstract>
                     <p>A central argument of this study has been that TB control measures in South Africa from the beginning of this century up to the present time have involved the application of exclusionary policies designed to keep the disease out of the social and economic centers of white society. Despite shifts in medical ideas concerning the causes and prevention of TB among blacks; developments in the area of treatment, including the use of chemotherapy and BCG; and occasional efforts to provide blacks with better housing and diets, South African local and state authorities have relied primarily on legislative instruments to deal</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pnvdq.18</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>321</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pnvdq.19</book-part-id>
                  <title-group>
                     <title>Select Bibliography</title>
                  </title-group>
                  <fpage>367</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pnvdq.20</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>379</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pnvdq.21</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>391</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
