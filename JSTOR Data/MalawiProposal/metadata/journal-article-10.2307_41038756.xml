<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">amereconrevi</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100009</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>The American Economic Review</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>American Economic Association</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00028282</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">41038756</article-id>
         <title-group>
            <article-title>International Trade and Income Differences</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Michael E.</given-names>
                  <surname>Waugh</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>12</month>
            <year>2010</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">100</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">5</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40046434</issue-id>
         <fpage>2093</fpage>
         <lpage>2124</lpage>
         <permissions>
            <copyright-statement>Copyright © 2010 American Economic Association</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:title="an external site"
                   xlink:href="http://dx.doi.org/10.1257/aer.100.5.2093"/>
         <abstract>
            <p>I develop a novel view of the trade frictions between rich and poor countries by arguing that to reconcile bilateral trade volumes and price data within a standard gravity model, the trade frictions between rich and poor countries must be systematically asymmetric, with poor countries facing higher costs to export relative to rich countries. I provide a method to model these asymmetries and demonstrate the merits of my approach relative to alternatives in the trade literature. I then argue that these trade frictions are quantitatively important to understanding the large differences in standards of living and total factor productivity across countries.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d1612e226a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1612e233" publication-type="other">
James E. Anderson and Eric van Wincoop (2004)</mixed-citation>
            </p>
         </fn>
         <fn id="d1612e240a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1612e247" publication-type="other">
Eaton and
Kortum (2002)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1612e256" publication-type="other">
ECD countries and Fernando Alvarez and Robert E. Lucas Jr. (2007)</mixed-citation>
            </p>
         </fn>
         <fn id="d1612e263a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1612e270" publication-type="other">
Ricardo Lagos (2006)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1612e276" publication-type="other">
Chang-Tai Hsieh and Peter J. Klenow (2007a)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1612e282" publication-type="other">
Diego
Restuccia and Richard Rogerson (2008),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1612e292" publication-type="other">
Nezih
Guner, Gustavo Ventura, and Yi Xu (2008)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1612e301" publication-type="other">
Naranjo (2009)</mixed-citation>
            </p>
         </fn>
         <fn id="d1612e308a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1612e315" publication-type="other">
Kei-Mu Yi (2003)</mixed-citation>
            </p>
         </fn>
         <fn id="d1612e323a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1612e330" publication-type="other">
Martin Neil Baily and Hans Gersbach (1995)</mixed-citation>
            </p>
         </fn>
         <fn id="d1612e337a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d1612e344" publication-type="other">
Kortum (1997)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1612e350" publication-type="other">
Waugh (2009)</mixed-citation>
            </p>
         </fn>
         <fn id="d1612e357a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d1612e364" publication-type="other">
Eaton and Kortum (2002)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1612e370" publication-type="other">
Appendix and in Waugh (2009),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1612e376" publication-type="other">
Anderson and van Wincoop (2003)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1612e383" publication-type="other">
Melitz (2003)</mixed-citation>
            </p>
         </fn>
         <fn id="d1612e390a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d1612e397" publication-type="other">
Joao M.C. Santos-
Silva and Silvana Tenreyro (2006)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1612e406" publication-type="other">
Waugh (2009)</mixed-citation>
            </p>
         </fn>
         <fn id="d1612e413a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d1612e420" publication-type="other">
Eaton and Kortum (2002)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1612e426" publication-type="other">
Anderson and van Wincoop (2004)</mixed-citation>
            </p>
         </fn>
         <fn id="d1612e433a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d1612e440" publication-type="other">
Eaton and Kortum (2002)</mixed-citation>
            </p>
         </fn>
         <fn id="d1612e448a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d1612e455" publication-type="other">
Cecilia Fieler (2009)</mixed-citation>
            </p>
         </fn>
         <fn id="d1612e462a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d1612e469" publication-type="other">
Abhijit Banerjee and Esther Duflo
(2005)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1612e478" publication-type="other">
William W. Lewis (2004)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1612e484" publication-type="other">
David Lagakos (2009)</mixed-citation>
            </p>
         </fn>
         <fn id="d1612e491a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d1612e498" publication-type="other">
Anderson and van Wincoop (2004)</mixed-citation>
            </p>
         </fn>
         <fn id="d1612e505a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d1612e512" publication-type="other">
Eaton and Kortum's (2001)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1612e518" publication-type="other">
Edward J. Balistreri and Russell H. Hillberry
(2006)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1612e527" publication-type="other">
Anderson and van Wincoop's (2003)</mixed-citation>
            </p>
         </fn>
         <fn id="d1612e534a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d1612e541" publication-type="other">
Andrea Finicelli, Patrizio Pagano, and Massimo Sbracia (2009)</mixed-citation>
            </p>
         </fn>
         <fn id="d1612e548a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d1612e555" publication-type="other">
Jones (2008),</mixed-citation>
            </p>
         </fn>
         <fn id="d1612e563a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d1612e570" publication-type="other">
Burstein,
Joao C. Neves, and Sergio Rebelo (2003)</mixed-citation>
            </p>
         </fn>
         <fn id="d1612e580a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d1612e587" publication-type="other">
Nuno M. Limao and Venables (2001)</mixed-citation>
            </p>
         </fn>
         <fn id="d1612e594a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d1612e601" publication-type="other">
David Hummels and Volodymyr Lugovskyy (2006)</mixed-citation>
            </p>
         </fn>
         <fn id="d1612e608a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d1612e615" publication-type="other">
Vittorio Corbo (1997)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1612e621" publication-type="other">
Sebastian Edwards and Daniel Lederman (1998)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1612e627" publication-type="other">
World Bank's World Trade Indicators (www.worldbank.org/wti2008)</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>REFERENCES</title>
         <ref id="d1612e643a1310">
            <mixed-citation id="d1612e647" publication-type="other">
Alvarez, Fernando, and Robert E. Lucas, Jr. 2007. "General Equilibrium Analysis of the Eaton-Kortum
Model of International Trade." Journal of Monetary Economics, 54(6): 1726-68.</mixed-citation>
         </ref>
         <ref id="d1612e657a1310">
            <mixed-citation id="d1612e661" publication-type="other">
Anderson, James E., and Eric van Wincoop. 2003. "Gravity with Gravitas: A Solution to the Border Puz-
zle." American Economic Review, 93(1): 170-92.</mixed-citation>
         </ref>
         <ref id="d1612e671a1310">
            <mixed-citation id="d1612e675" publication-type="other">
Anderson, James E., and Eric van Wincoop. 2004. "Trade Costs." Journal of Economic Literature, 42(3):
691-751.</mixed-citation>
         </ref>
         <ref id="d1612e685a1310">
            <mixed-citation id="d1612e689" publication-type="other">
Baily, Martin Neil, and Hans Gersbach. 1995. "Efficiency in Manufacturing and the Need for Global Com-
petition." Brookings Papers on Economic Activity, Microeconomics, 1995: 307-58.</mixed-citation>
         </ref>
         <ref id="d1612e700a1310">
            <mixed-citation id="d1612e704" publication-type="other">
Balistreri, Edward J., and Russell H. Hillberry. 2006. "Trade Frictions and Welfare in the Gravity Model:
How Much of the Iceberg Melts?" Canadian Journal of Economics, 39(1): 247-65.</mixed-citation>
         </ref>
         <ref id="d1612e714a1310">
            <mixed-citation id="d1612e718" publication-type="other">
Banerjee, Abhijit, and Esther Duflo. 2005. "Growth Theory through the Lens of Development Econom-
ics." In Handbook of Economic Growth, Vol. 1, ed. Philippe Aghion and Steven Durlauf, 473-552.
Amsterdam: Elsevier.</mixed-citation>
         </ref>
         <ref id="d1612e731a1310">
            <mixed-citation id="d1612e735" publication-type="other">
Bernard, Andrew B., Jonathan Eaton, J. Bradford Jensen, and Samuel Kortum. 2003. "Plants and Pro-
ductivity in International Trade." American Economic Review, 93(4): 1268-90.</mixed-citation>
         </ref>
         <ref id="d1612e745a1310">
            <mixed-citation id="d1612e749" publication-type="other">
Burstein, Ariel T., and Alexander Monge-Naranjo. 2009. "Foreign Know-How, Firm Control, and the
Income of Developing Countries." Quarterly Journal of Economics, 124(1): 149-95.</mixed-citation>
         </ref>
         <ref id="d1612e759a1310">
            <mixed-citation id="d1612e763" publication-type="other">
Burstein, Ariel T., Joao C. Neves, and Sergio Rebelo. 2003. "Distribution Costs and Real Exchange Rate
Dynamics During Exchange-Rate-Based Stabilizations." Journal of Monetary Economics, 50(6): 1189-
1214.</mixed-citation>
         </ref>
         <ref id="d1612e776a1310">
            <mixed-citation id="d1612e780" publication-type="other">
Caselli, Francesco. 2005. "Accounting for Cross-Country Income Differences." In Handbook of Economic
Growth, Vol. 1, ed. Philippe Aghion and Steven Durlauf, 679-741. Amsterdam: Elsevier.</mixed-citation>
         </ref>
         <ref id="d1612e791a1310">
            <mixed-citation id="d1612e795" publication-type="other">
Chari, Varadarajan V., Patrick J. Kehoe, and Ellen R. McGrattan. 2007. "Business Cycle Accounting."
Econometrica, 75(3): 781-836.</mixed-citation>
         </ref>
         <ref id="d1612e805a1310">
            <mixed-citation id="d1612e809" publication-type="other">
Corbo, Vittorio. 1997. "Trade Reform and Uniform Import Tariffs: The Chilean Experience." American
Economic Review, 87(2): 73-77.</mixed-citation>
         </ref>
         <ref id="d1612e819a1310">
            <mixed-citation id="d1612e823" publication-type="other">
Dornbusch, Rudiger, Stanley Fischer, and Paul A. Samuelson. 1977. "Comparative Advantage, Trade,
and Payments in a Ricardian Model with a Continuum of Goods." American Economic Review, 67(5):
823-39.</mixed-citation>
         </ref>
         <ref id="d1612e836a1310">
            <mixed-citation id="d1612e840" publication-type="other">
Eaton, Jonathan, and Samuel Kortum. 2001. "Trade in Capital Goods." European Economic Review,
45(7): 1195-1235.</mixed-citation>
         </ref>
         <ref id="d1612e850a1310">
            <mixed-citation id="d1612e854" publication-type="other">
Eaton, Jonathan, and Samuel Kortum. 2002. "Technology, Geography, and Trade." Econometrica, 70(5):
1741-79.</mixed-citation>
         </ref>
         <ref id="d1612e864a1310">
            <mixed-citation id="d1612e868" publication-type="other">
Edwards, Sebastian, and Daniel Lederman. 1998. "The Political Economy of Unilateral Trade Liberaliza-
tion: The Case of Chile." National Bureau of Economic Research Working Paper 6510.</mixed-citation>
         </ref>
         <ref id="d1612e879a1310">
            <mixed-citation id="d1612e883" publication-type="other">
Feenstra, Robert C, Robert E. Lipsey, and Harry P. Bowen. 1997. "World Trade Flows, 1970-1992, with
Production and Tariff Data." National Bureau of Economic Research Working Paper 5910.</mixed-citation>
         </ref>
         <ref id="d1612e893a1310">
            <mixed-citation id="d1612e897" publication-type="other">
Fieler, Ana Cecilia. 2009. "Non-Homotheticity and Bilateral Trade: Evidence and a Quantitative Explana-
tion." http://www.sas.upenn.edu/~afieler/Fieler_Jmp.pdf.</mixed-citation>
         </ref>
         <ref id="d1612e907a1310">
            <mixed-citation id="d1612e911" publication-type="other">
Finicelli, Andrea, Patrizio Pagano, and Massimo Sbracia. 2009. "Trade-Revealed TFP." Bank of Italy
Working Paper 729.</mixed-citation>
         </ref>
         <ref id="d1612e921a1310">
            <mixed-citation id="d1612e925" publication-type="other">
Gollin, Douglas. 2002. "Getting Income Shares Right." Journal of Political Economy, 110(2): 458-74.</mixed-citation>
         </ref>
         <ref id="d1612e932a1310">
            <mixed-citation id="d1612e936" publication-type="other">
Guner, Nezih, Gustavo Ventura, and Yi Xu. 2008. "Macroeconomic Implications of Size-Dependent Poli-
cies." Review of Economic Dynamics, 11(4): 721-44.</mixed-citation>
         </ref>
         <ref id="d1612e946a1310">
            <mixed-citation id="d1612e950" publication-type="other">
Hall, Robert E., and Charles I. Jones. 1999. "Why Do Some Countries Produce So Much More Output Per
Worker Than Others?" Quarterly Journal of Economics, 114(1): 83-116.</mixed-citation>
         </ref>
         <ref id="d1612e961a1310">
            <mixed-citation id="d1612e965" publication-type="other">
Heston, Alan, Robert Summers, and Bettina Aten. 2002. "Penn World Table Version 6.1." Center for Inter-
national Comparison at the University of Pennsylvania (CICUP). httpV/datacentre.chass.utoronto.ca/
pwt61/.</mixed-citation>
         </ref>
         <ref id="d1612e978a1310">
            <mixed-citation id="d1612e982" publication-type="other">
Hsieh, Chang-Tai, and Peter J. Klenow. 2007a. "Misallocation and Manufacturing TFP in China and
India." National Bureau of Economic Research Working Paper 13290.</mixed-citation>
         </ref>
         <ref id="d1612e992a1310">
            <mixed-citation id="d1612e996" publication-type="other">
Hsieh, Chang-Tai, and Peter J. Klenow. 2007b. "Relative Prices and Relative Prosperity." American Eco-
nomic Review, 97(3): 562-85.</mixed-citation>
         </ref>
         <ref id="d1612e1006a1310">
            <mixed-citation id="d1612e1010" publication-type="other">
Hummels, David, and Volodymyr Lugovskyy. 2006. "Are Matched Partner Trade Statistics a Usable Mea-
sure of Transportation Costs?" Review of International Economics, 14(1): 69-86.</mixed-citation>
         </ref>
         <ref id="d1612e1020a1310">
            <mixed-citation id="d1612e1024" publication-type="other">
Jones, Charles I. 2008. "Intermediate Goods, Weak Links, and Superstars: A Theory of Economic Devel-
opment." National Bureau of Economic Research Working Paper 13834.</mixed-citation>
         </ref>
         <ref id="d1612e1034a1310">
            <mixed-citation id="d1612e1038" publication-type="other">
Klenow, Peter J., and Andres Rodríguez-Clare. 1997. "The Neoclassical Revival in Growth Econom-
ics: Has It Gone Too Far?" In NBER Macroeconomics Annual 1997, ed. Ben S. Bernanke and Julio J.
Rotemberg, 73-103. Cambridge, MA: MIT Press.</mixed-citation>
         </ref>
         <ref id="d1612e1052a1310">
            <mixed-citation id="d1612e1056" publication-type="other">
Kortum, Samuel. 1997. "Research, Patenting, and Technological Change." Econometrica, 65(6): 1389-
1419.</mixed-citation>
         </ref>
         <ref id="d1612e1066a1310">
            <mixed-citation id="d1612e1070" publication-type="other">
Kravis, Irving B., and Robert E. Lipsey. 1988. "National Price Levels and the Prices of Tradables and Non-
tradables." American Economic Review, 78(2): 474-78.</mixed-citation>
         </ref>
         <ref id="d1612e1080a1310">
            <mixed-citation id="d1612e1084" publication-type="other">
Krueger, Anne O., Maurice Schiff, and Alberto Valdes. 1988. "Agricultural Incentives in Develop-
ing Countries: Measuring the Effect of Sectoral and Economywide Policies." World Bank Economic
Review, 2(3): 255-71.</mixed-citation>
         </ref>
         <ref id="d1612e1097a1310">
            <mixed-citation id="d1612e1101" publication-type="other">
Lagakos, David. 2009. "Superstores or Mom and Pops? Market Size, Technology Adoption and Productiv-
ity Differences in Retail Trade." Federal Reserve Bank of Minneapolis Staff Report 428-2009.</mixed-citation>
         </ref>
         <ref id="d1612e1111a1310">
            <mixed-citation id="d1612e1115" publication-type="other">
Lagos, Ricardo. 2006. "A Model of TFP." Review of Economic Studies, 73(4): 983-1007.</mixed-citation>
         </ref>
         <ref id="d1612e1122a1310">
            <mixed-citation id="d1612e1126" publication-type="other">
Lewis, William W. 2004. lhe Power of Productivity: Wealth, Poverty, and the Threat to Global Stability.
Chicago: University of Chicago Press.</mixed-citation>
         </ref>
         <ref id="d1612e1137a1310">
            <mixed-citation id="d1612e1141" publication-type="other">
Limao, Nuno, and Anthony J. Venables. 2001. "Infrastructure, Geographical Disadvantage, Transport
Costs, and Trade." World Bank Economic Review, 15(3): 451-79.</mixed-citation>
         </ref>
         <ref id="d1612e1151a1310">
            <mixed-citation id="d1612e1155" publication-type="other">
Melitz, Marc J. 2003. "The Impact of Trade on Intra-Industry Reallocations and Aggregate Industry Pro-
ductivity." Econometrica, 71(6): 1695-1725.</mixed-citation>
         </ref>
         <ref id="d1612e1165a1310">
            <mixed-citation id="d1612e1169" publication-type="other">
Moreira, Maurício M., Christian Volpe, and Juan S. Blyde. 2008. "Unclogging the Arteries: The Impact
of Transport Costs on Latin American and Caribbean Trade." Special Report on Integration and Trade.
Washington, DC: Inter-American Development Bank.</mixed-citation>
         </ref>
         <ref id="d1612e1182a1310">
            <mixed-citation id="d1612e1186" publication-type="other">
Olarreaga, Marcelo, Alessandro Nicita, and Hiau Looi Kee. 2006. "Estimating Trade Restrictiveness Indi-
ces." World Bank Policy Research Working Paper 3840.</mixed-citation>
         </ref>
         <ref id="d1612e1196a1310">
            <mixed-citation id="d1612e1200" publication-type="other">
Parente, Stephen L., and Edward C. Prescott. 2000. Barriers to Riches. Walras-Pareto Lectures, vol. 3.
Cambridge, MA: MIT Press.</mixed-citation>
         </ref>
         <ref id="d1612e1210a1310">
            <mixed-citation id="d1612e1214" publication-type="other">
Redding, Stephen, and Anthony J. Venables. 2004. Economic Geography and International Inequality.
Journal of International Economics, 62(1): 53-82.</mixed-citation>
         </ref>
         <ref id="d1612e1225a1310">
            <mixed-citation id="d1612e1229" publication-type="other">
Restuccia, Diego, and Richard Rogerson. 2008. "Policy Distortions and Aggregate Productivity with Het-
erogeneous Establishments." Review of Economic Dynamics, 11(4): 707-20.</mixed-citation>
         </ref>
         <ref id="d1612e1239a1310">
            <mixed-citation id="d1612e1243" publication-type="other">
Santos Silva, Joao M. C, and Silvana Tenreyro. 2006. "The Log of Gravity." Review of Economics and
Statistics, 88(4): 641-58.</mixed-citation>
         </ref>
         <ref id="d1612e1253a1310">
            <mixed-citation id="d1612e1257" publication-type="other">
United Nations Industrial Development Organization (UNIDO). 1996. International Yearbook of Indus-
trial Statistics. Surrey, UK: Edward Elgar Publishing.</mixed-citation>
         </ref>
         <ref id="d1612e1267a1310">
            <mixed-citation id="d1612e1271" publication-type="other">
Waugh, Michael E. 2009. "International Trade and Income Differences." Federal Reserve Bank of Min-
neapolis Staff Report, 435.</mixed-citation>
         </ref>
         <ref id="d1612e1281a1310">
            <mixed-citation id="d1612e1285" publication-type="other">
Yi, Kei-Mu. 2003. "Can Vertical Specialization Explain the Growth of World Trade?" Journal of Political
Economy, 111(1): 52-102.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
