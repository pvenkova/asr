<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">landeconomics</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100261</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Land Economics</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Wisconsin Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00237639</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">41307685</article-id>
         <title-group>
            <article-title>Insecure Land Rights and Share Tenancy: Evidence from Madagascar</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Marc F.</given-names>
                  <surname>Bellemare</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>2</month>
            <year>2012</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">88</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40059102</issue-id>
         <fpage>155</fpage>
         <lpage>180</lpage>
         <permissions>
            <copyright-statement>Copyright © 2012 Board of Regents of the University of Wisconsin System</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/41307685"/>
         <abstract>
            <p>Most studies oftenurial insecurity focus on its effects on investment. This paper studies the hitherto unexplored relationship between tenurial insecurity and land tenancy contracts. Based on distinct features of formal law and customary rights in Madagascar, this paper augments the canonical model of sharecropping by making the strength of the landlord's property right increasing in the amount of risk she bears within the contract. Using data on landlords' subjective perceptions in rural Madagascar, empirical tests support the hypothesis that insecure property rights drive contract choice but offer little support in favor of the canonical risk sharing hypothesis.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d190e187a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d190e194" publication-type="other">
Shaban (1987)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d190e200" publication-type="other">
Arcand, Ai, and Ethier (2007)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d190e206" publication-type="other">
Sadoulet, de Janvry, and Fukui (1997),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d190e213" publication-type="other">
Kassie and
Holden (2007)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d190e222" publication-type="other">
Braido (2008)</mixed-citation>
            </p>
         </fn>
         <fn id="d190e229a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d190e236" publication-type="other">
Bellemare (2009)</mixed-citation>
            </p>
         </fn>
         <fn id="d190e243a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d190e250" publication-type="other">
(Ellickson 1989)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d190e256" publication-type="other">
(Ellickson 1994).</mixed-citation>
            </p>
         </fn>
         <fn id="d190e263a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d190e270" publication-type="other">
(Tversky and Kahneman 1982).</mixed-citation>
            </p>
         </fn>
         <fn id="d190e278a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d190e285" publication-type="other">
Charmes (1975)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d190e291" publication-type="other">
Jarosz (1990, 1991).</mixed-citation>
            </p>
         </fn>
         <fn id="d190e298a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d190e305" publication-type="other">
(Man-
ski and Lerman 1977).</mixed-citation>
            </p>
         </fn>
         <fn id="d190e315a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d190e322" publication-type="other">
Minten and Razafindraibe (2003)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d190e328" publication-type="other">
(Minten and Razafindraibe
2003).</mixed-citation>
            </p>
         </fn>
         <fn id="d190e338a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d190e345" publication-type="other">
Dubois (2002),</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d190e361a1310">
            <mixed-citation id="d190e365" publication-type="other">
Ackerberg, Daniel A., and Maristella Botticini. 2002.
"Endogenous Matching and the Empirical Deter-
minants of Contract Form." Journal of Political
Economy 110(3): 564-91.</mixed-citation>
         </ref>
         <ref id="d190e381a1310">
            <mixed-citation id="d190e385" publication-type="other">
Allen, Douglas W., and Dean Lueck. 2002. I he Na-
ture of the Farm. Cambridge, MA: MIT Press.</mixed-citation>
         </ref>
         <ref id="d190e395a1310">
            <mixed-citation id="d190e399" publication-type="other">
Arcand, Jean-Louis, Chunrong Ai, and Francis
Ethier. 2007. "Moral Hazard and Marshallian In-
efficiency: Evidence from Tunisia." Journal of
Development Economics 83 (2): 411-45.</mixed-citation>
         </ref>
         <ref id="d190e415a1310">
            <mixed-citation id="d190e419" publication-type="other">
Baker, Matthew, Thomas Miceli, C. F. Sirmans, and
Geoffrey K. Turnbull. 2001. "Property Rights by
Squatting: Land Ownership Risk and Adverse
Possession Statutes." Land Economics 11 (3):
360-70.</mixed-citation>
         </ref>
         <ref id="d190e439a1310">
            <mixed-citation id="d190e443" publication-type="other">
Baneijee, Abhijit V., and Maitreesh Ghatak. 2004.
"Eviction Threats and Investment Incentives."
Journal of Development Economics 74 (2): 469-
88.</mixed-citation>
         </ref>
         <ref id="d190e459a1310">
            <mixed-citation id="d190e463" publication-type="other">
Bellemare, Marc F. 2009. "Sharecropping, Insecure
Land Rights, and Land Titling Policies: A Case
Study of Lac Alaotra, Madagascar." Development
Policy Review 21 (1): 87-106.</mixed-citation>
         </ref>
         <ref id="d190e479a1310">
            <mixed-citation id="d190e483" publication-type="other">
--- . 2009. "When Perception Is Reality: Subjec-
tive Expectations and Contracting." American
Journal of Agricultural Economics 91 (5): 1377-
81.</mixed-citation>
         </ref>
         <ref id="d190e499a1310">
            <mixed-citation id="d190e503" publication-type="other">
--- . 2011. "The Productivity Impacts of Formal
and Informal Land Rights." Working paper, Duke
University.</mixed-citation>
         </ref>
         <ref id="d190e516a1310">
            <mixed-citation id="d190e520" publication-type="other">
Bellemare, Marc F., and Zachary S. Brown. 2010.
"On the (Mis)Use of Wealth as a Proxy for Risk
Aversion." American Journal of Agricultural Eco-
nomics 92 (1): 273-82.</mixed-citation>
         </ref>
         <ref id="d190e536a1310">
            <mixed-citation id="d190e540" publication-type="other">
Besley, Timothy. 1995. "Property Rights and Invest-
ment Incentives: Theory and Evidence from
Ghana." Journal of Political Economy 103 (5):
903-37.</mixed-citation>
         </ref>
         <ref id="d190e557a1310">
            <mixed-citation id="d190e561" publication-type="other">
Bezabih, Mintewab. 2007. "Essays on Land Lease
Markets, Productivity, Biodiversity, and Environ-
mental Variability." Ph.D. dissertation, Goteborg
University.</mixed-citation>
         </ref>
         <ref id="d190e577a1310">
            <mixed-citation id="d190e581" publication-type="other">
Braido, Luis H. B. 2008. "Evidence on the Incentive
Properties of Share Contracts." Journal of Law
and Economics 51 (2): 327-49.</mixed-citation>
         </ref>
         <ref id="d190e594a1310">
            <mixed-citation id="d190e598" publication-type="other">
Brasselle, Anne-Sophie, Fr6d£ric Gaspart, and Jean-
Philippe Platteau. 2002. "Land Tenure Security
and Investment Incentives: Puzzling Evidence
from Burkina Faso." Journal of Development Eco-
nomics 67 (2): 373-418.</mixed-citation>
         </ref>
         <ref id="d190e617a1310">
            <mixed-citation id="d190e621" publication-type="other">
Cardenas, Juan-Camilo, and Jeffrey Carpenter. 2008.
"Behavioral Development Economics: Lessons
from Field Labs in the Developing World." Jour-
nal of Development Studies 44 (3): 337-64.</mixed-citation>
         </ref>
         <ref id="d190e637a1310">
            <mixed-citation id="d190e641" publication-type="other">
Carter, Michael R., and Pedro Olinto. 2003. "Getting
Institutions 'Right' for Whom? Credit Constraints
and the Impact of Property Rights on the Quantity
and Composition of Investment." American Jour-
nal of Agricultural Economics 85 (1): 173-86.</mixed-citation>
         </ref>
         <ref id="d190e660a1310">
            <mixed-citation id="d190e664" publication-type="other">
Charmes, Jacques. 1975. "M6tayage et capitalisme
agraire sur les p&amp;imfctres nord de la SOMALAC."
Cahiers de I'ORSTOM, sirie Sciences Humaines
12 (2): 259-82.</mixed-citation>
         </ref>
         <ref id="d190e681a1310">
            <mixed-citation id="d190e685" publication-type="other">
Cheung, Steven N. S. 1968. "Private Property Rights
and Sharecropping." Journal of Political Economy
76 (6): 1107-22.</mixed-citation>
         </ref>
         <ref id="d190e698a1310">
            <mixed-citation id="d190e702" publication-type="other">
Conning, Jonathan H., and James A. Robinson. 2007.
"Property Rights and the Political Organization of
Agriculture." Journal of Development Economics
82 (2): 416-47.</mixed-citation>
         </ref>
         <ref id="d190e718a1310">
            <mixed-citation id="d190e722" publication-type="other">
Deininger, Klaus, Daniel Ayalew Ali, and Tekie
Alemu. 2009. "Land Rental Markets: Transaction
Costs and Tenure Insecurity in Rural Ethiopia." In
The Emergence of Land Markets in Africa, ed.
Stein T. Holden, Keijiro Otsuka, and Frank M.
Place. Washington, DC: Resources for the Future.</mixed-citation>
         </ref>
         <ref id="d190e745a1310">
            <mixed-citation id="d190e749" publication-type="other">
Deininger, Klaus, and Songqing Jin. 2003. 'The Im-
pact of Property Rights on Households' Invest-
ment, Risk Coping, and Policy Preferences:
Evidence from China." Economic Development
and Cultural Change 51 (4): 851-82.</mixed-citation>
         </ref>
         <ref id="d190e768a1310">
            <mixed-citation id="d190e772" publication-type="other">
--- . 2006. "Tenure Security and Land-Related In-
vestment: Evidence from Ethiopia." European
Economic Review 50 (5): 1245-77.</mixed-citation>
         </ref>
         <ref id="d190e785a1310">
            <mixed-citation id="d190e789" publication-type="other">
Delavande, Adeline, Xavier Gin6, and David Mc-
Kenzie. 2011. "Measuring Subjective Expecta-
tions in Developing Countries: A Critical Review
and New Evidence." Journal of Development Eco-
nomics 94 (2): 151-63.</mixed-citation>
         </ref>
         <ref id="d190e809a1310">
            <mixed-citation id="d190e813" publication-type="other">
Doss, Cheryl, John McPeak, and Christopher B. Bar-
rett. 2006. "Interpersonal, Intertemporal and Spa-
tial Variation in Risk Perceptions: Evidence from
East Africa." Working paper, Yale University.</mixed-citation>
         </ref>
         <ref id="d190e829a1310">
            <mixed-citation id="d190e833" publication-type="other">
Dubois, Pierre. 2002. "Moral Hazard, Land Fertility
and Sharecropping in a Rural Area of the Philip-
pines." Journal of Development Economics 68 (1):
35-64.</mixed-citation>
         </ref>
         <ref id="d190e849a1310">
            <mixed-citation id="d190e853" publication-type="other">
Ellickson, Robert C. 1989. "A Hypothesis of Wealth-
Maximizing Norms: Evidence from the Whaling
Industry." Journal of Law, Economics, and Or-
ganization 5 (1): 83-97.</mixed-citation>
         </ref>
         <ref id="d190e869a1310">
            <mixed-citation id="d190e873" publication-type="other">
--- . 1994. Order without Law: How Neighbors
Settle Disputes. Cambridge, MA: Harvard Uni-
versity Press.</mixed-citation>
         </ref>
         <ref id="d190e886a1310">
            <mixed-citation id="d190e890" publication-type="other">
Fafchamps, Marcel, and Bart Minten. 2001 . "Property
Rights in a Flea Market Economy." Economic De-
velopment and Cultural Change 49 (2): 229-67.</mixed-citation>
         </ref>
         <ref id="d190e903a1310">
            <mixed-citation id="d190e907" publication-type="other">
Fenske, James. 2010. "L'Etranger: Status, Property
Rights, and Investment Incentives in Cote
d' I voire." Land Economics 86 (4): 621-44.</mixed-citation>
         </ref>
         <ref id="d190e921a1310">
            <mixed-citation id="d190e925" publication-type="other">
Fukunaga, K., and Wallace E. Huffman. 2009. "The
Role of Risk and Transaction Costs in Contract
Design: Evidence from Farmland Lease Contracts
in U.S. Agriculture." American Journal of Agri-
cultural Economics 91 (1): 237-49.</mixed-citation>
         </ref>
         <ref id="d190e944a1310">
            <mixed-citation id="d190e948" publication-type="other">
Goldstein, Markus, and Christopher Udry. 2008. "The
Profits of Power: Land Rights and Agricultural In-
vestment in Ghana." Journal of Political Economy
116(6): 981-1022.</mixed-citation>
         </ref>
         <ref id="d190e964a1310">
            <mixed-citation id="d190e968" publication-type="other">
Jarosz, Lucy A. 1990. "Rice on Shares: Agrarian
Change and the Development of Sharecropping in
Lac Alaotra, Madagascar." Ph.D. diss., University
of California, Berkeley.</mixed-citation>
         </ref>
         <ref id="d190e984a1310">
            <mixed-citation id="d190e988" publication-type="other">
--- . 1991. "Women as Rice Sharecroppers in
Madagascar." Society and Natural Resources 4
(1): 53-63.</mixed-citation>
         </ref>
         <ref id="d190e1001a1310">
            <mixed-citation id="d190e1005" publication-type="other">
Jewitt, Ian. 1987. "Justifying the First-Order Ap-
proach to Principal Agent Problems." Econome-
trica 56 (5): 1177-90.</mixed-citation>
         </ref>
         <ref id="d190e1018a1310">
            <mixed-citation id="d190e1022" publication-type="other">
Johnson, D. Gale. 1950. "Resource Allocation under
Share Contracts." Journal of Political Economy 58
(2): 111-23.</mixed-citation>
         </ref>
         <ref id="d190e1036a1310">
            <mixed-citation id="d190e1040" publication-type="other">
Kahneman, Daniel, and Amos Tversky. 1979. "Pros-
pect Theory: An Analysis of Decision under
Risk." Econometrica 47 (2): 263-91.</mixed-citation>
         </ref>
         <ref id="d190e1053a1310">
            <mixed-citation id="d190e1057" publication-type="other">
Karlan, Dean, and Jonathan Zinman. 2009. Observ-
ing Unobservables: Identifying Information
Asymmetries with a Consumer Credit Field Ex-
periment." Econometrica 11 (6): 1993-2008.</mixed-citation>
         </ref>
         <ref id="d190e1073a1310">
            <mixed-citation id="d190e1077" publication-type="other">
Karsenty, Alain, and Etienne Le Roy. 1996. "Revue
du metayage et du fermage des terres et de leur
implication legale et de leur enregistrement." Re-
port to the Madagascar Ministry of Agriculture
and Rural Development. Paris: CIRAD.</mixed-citation>
         </ref>
         <ref id="d190e1096a1310">
            <mixed-citation id="d190e1100" publication-type="other">
Kassie, Menale, and Stein T. Holden. 2007. "Share-
cropping Efficiency in Ethiopia: Threats of Evic-
tion and Kinship." Agricultural Economics 37 (2-
3): 179-88.</mixed-citation>
         </ref>
         <ref id="d190e1116a1310">
            <mixed-citation id="d190e1120" publication-type="other">
--- . 2009. "Kinship, Tenure Insecurity, Input
Use, and Land Productivity: The Case of Share-
cropping in Ethiopia." In The Emergence of Land
Markets in Africa , ed. Stein T. Holden, Keijiro Ot-
suka, and Frank M. Place. Washington, DC: Re-
sources for the Future.</mixed-citation>
         </ref>
         <ref id="d190e1143a1310">
            <mixed-citation id="d190e1147" publication-type="other">
Keck, Andrew, Narendra P. Sharma, and Gershon
Feder. 1994. "Population Growth, Shifting Culti-
vation, and Unsustainable Agricultural Develop-
ment: A Case Study in Madagascar." Africa
Technical Department Series Discussion Paper
#234. Washington, DC: World Bank.</mixed-citation>
         </ref>
         <ref id="d190e1171a1310">
            <mixed-citation id="d190e1175" publication-type="other">
Laffont, Jean-Jacques, and Mohammed S. Matoussi.
1995. "Moral Hazard, Financial Constraints and
Sharecropping in El Oulja." Review of Economic
Studies 62 (3): 381-99.</mixed-citation>
         </ref>
         <ref id="d190e1191a1310">
            <mixed-citation id="d190e1195" publication-type="other">
Lawry, Steven. 1993. "Transactions in Cropland Held
under Customary Tenure in Lesotho." In Land in
African Agrarian Systems , ed. Thomas J. Bassett
and Donald E. Crummey. Madison: University of
Wisconsin Press.</mixed-citation>
         </ref>
         <ref id="d190e1214a1310">
            <mixed-citation id="d190e1218" publication-type="other">
Liu, Elaine. 2011. "Time to Change What to Sow:
Risk Preferences and Technology Adoption De-
cisions of Cotton Farmers in China." Working pa-
per, Department of Economics, University of
Houston.</mixed-citation>
         </ref>
         <ref id="d190e1237a1310">
            <mixed-citation id="d190e1241" publication-type="other">
Lunduka, Rodney, Stein T. Holden, and Ragnar
0ygard. 2009. "Land Rental Market Participation
and Tenure Security in Malawi." In The Emer-
gence of Land Markets in Africa , ed. Stein T.
Holden, Keijiro Otsuka, and Frank M. Place.
Washington, DC: Resources for the Future.</mixed-citation>
         </ref>
         <ref id="d190e1264a1310">
            <mixed-citation id="d190e1268" publication-type="other">
Luseno, Winnie K., John G. McPeak, Christopher B.
Barrett, Peter D. Little, and Getachew Gebru.
2003. "Assessing the Value of Climate Forecast
Information for Pastoralists: Evidence from
Southern Ethiopia and Northern Kenya." World
Developmental (9): 1477-94.</mixed-citation>
         </ref>
         <ref id="d190e1291a1310">
            <mixed-citation id="d190e1295" publication-type="other">
Lybbert, Travis J., Christopher B. Barrett, John G.
McPeak, and Winnie K. Luseno. 2007. "Bayesian
Herders: Updating of Rainfall Beliefs in Response
to External Forecasts." World Development 35 (3):
480-97.</mixed-citation>
         </ref>
         <ref id="d190e1315a1310">
            <mixed-citation id="d190e1319" publication-type="other">
Lybbert, Travis J., and David R. Just. 2007. "Is Risk
Aversion Really Correlated with Wealth? How Es-
timated Probabilities Introduce Spurious Correla-
tion." American Journal of Agricultural
Economics 89 (4): 964-79.</mixed-citation>
         </ref>
         <ref id="d190e1338a1310">
            <mixed-citation id="d190e1342" publication-type="other">
Lyne, Michael C., and David N. Thomson. 1995.
"Creating Opportunities for Farmers in Communal
Areas: Adapting Institutions to Promote an Effi-
cient Rental Market in Arable Land." Ph.D. diss.,
University of Natal.</mixed-citation>
         </ref>
         <ref id="d190e1361a1310">
            <mixed-citation id="d190e1365" publication-type="other">
Macours, Karen, Alain de Janvry, and Elisabeth Sa-
doulet. 2010. "Insecurity of Property Rights and
Social Matching in the Tenancy Market." Euro-
pean Economic Review 54 (7): 880-99.</mixed-citation>
         </ref>
         <ref id="d190e1381a1310">
            <mixed-citation id="d190e1385" publication-type="other">
Manski, Charles F., and Steven R. Lerman. 1977.
"The Estimation of Choice Probabilities from
Choice-Based Samples." Econometrica 45 (8):
1977-88.</mixed-citation>
         </ref>
         <ref id="d190e1401a1310">
            <mixed-citation id="d190e1405" publication-type="other">
Minten, Bart, and Rolland Razafindraibe. 2003. Re-
lations terres agricoles -pauvrete a Madagascar.
Policy brief. Antananarivo: FOFIFA.</mixed-citation>
         </ref>
         <ref id="d190e1418a1310">
            <mixed-citation id="d190e1422" publication-type="other">
Nagin, Daniel. S., James B. Rebitzer, Seth Sanders,
and Lowell J. Taylor. 2002. "Monitoring, Moti-
vation, and Management: The Determinants of
Opportunistic Behavior in a Field Experiment."
American Economic Review 92 (4): 850-73.</mixed-citation>
         </ref>
         <ref id="d190e1442a1310">
            <mixed-citation id="d190e1446" publication-type="other">
Pearce, Richard. 1983. "Sharecropping: Towards a
Marxist View." In Sharecropping and Sharecrop-
pers , ed. T. J. Byres. London: Frank Cass.</mixed-citation>
         </ref>
         <ref id="d190e1459a1310">
            <mixed-citation id="d190e1463" publication-type="other">
Platteau, Jean-Philippe. 1994. "Behind the Market
Stage Where Real Societies Exist. Part I: The Role
of Public and Private Order Institutions." Journal
of Development Studies 30 (3): 533-77.</mixed-citation>
         </ref>
         <ref id="d190e1479a1310">
            <mixed-citation id="d190e1483" publication-type="other">
--- . 2000. Institutions, Social Norms , and Eco-
nomic Development. London: Routledge.</mixed-citation>
         </ref>
         <ref id="d190e1493a1310">
            <mixed-citation id="d190e1497" publication-type="other">
Posner, Richard A. 2007. Economic Analysis of Law.
New York: Aspen Publishers.</mixed-citation>
         </ref>
         <ref id="d190e1507a1310">
            <mixed-citation id="d190e1511" publication-type="other">
Randrianarisoa, Jean Claude, and Bart Minten. 2001.
"Agricultural Production, Agricultural Land and
Rural Poverty in Madagascar." Working paper,
Cornell University.</mixed-citation>
         </ref>
         <ref id="d190e1527a1310">
            <mixed-citation id="d190e1531" publication-type="other">
Rogerson, William P. 1985. "The First-Order Ap-
proach to Principal-Agent Problems." Econome-
trica 53 (6): 1357-68.</mixed-citation>
         </ref>
         <ref id="d190e1545a1310">
            <mixed-citation id="d190e1549" publication-type="other">
Roumasset, James A. 2002. "The Microeconomics of
Agricultural Development in the Philippines."
Working paper, University of Hawaii.</mixed-citation>
         </ref>
         <ref id="d190e1562a1310">
            <mixed-citation id="d190e1566" publication-type="other">
Sadoulet, Elisabeth, Alain de Janvry, and Seiichi Fu-
kui. 1997. "The Meaning of Kinship in Share-
cropping." American Journal of Agricultural
Economics 79 (2): 394-406.</mixed-citation>
         </ref>
         <ref id="d190e1582a1310">
            <mixed-citation id="d190e1586" publication-type="other">
Shaban, Radwan A. 1987. testing between Com-
peting Models of Sharecropping." Journal of Po-
litical Economy 95 (5): 893-920.</mixed-citation>
         </ref>
         <ref id="d190e1599a1310">
            <mixed-citation id="d190e1603" publication-type="other">
Singh, Indeijit. 1989. "Reverse Tenancy in Punjab
Agriculture: Impact of Technological Change."
Economic and Political Weekly 24 (25).</mixed-citation>
         </ref>
         <ref id="d190e1616a1310">
            <mixed-citation id="d190e1620" publication-type="other">
Smith, Adam. 1776/1976. An Inquiry into the Nature
and Causes of the Wealth of Nations. Chicago:
University of Chicago Press.</mixed-citation>
         </ref>
         <ref id="d190e1633a1310">
            <mixed-citation id="d190e1637" publication-type="other">
Stiglitz, Joseph E. 1974. "Incentives and Risk Sharing
in Sharecropping." Review of Economic Studies 41
(2): 219-55.</mixed-citation>
         </ref>
         <ref id="d190e1651a1310">
            <mixed-citation id="d190e1655" publication-type="other">
Stokey, Nancy L., and Robert E. Lucas Jr. 1989. Re-
cursive Methods in Economic Dynamics. Cam-
bridge, MA: Harvard University Press.</mixed-citation>
         </ref>
         <ref id="d190e1668a1310">
            <mixed-citation id="d190e1672" publication-type="other">
Teyssier, Andr£. 1998. "Front pionnier et s6curisation
fonci&amp;re k Madagascar: Le n£cessaire recours k
l'Etat." In Quelles politiques fonciires pour
VAfrique rurale?, ed. Philippe Lavigne Delville.
Paris: Karthala.</mixed-citation>
         </ref>
         <ref id="d190e1691a1310">
            <mixed-citation id="d190e1695" publication-type="other">
Tikabo, Mahari O. 2003. "Land Contract Choice:
Poor Landlords and Rich Tenants -Evidence from
the Highlands of Eritrea." Ph.D. dissertation, Ag-
ricultural University of Norway.</mixed-citation>
         </ref>
         <ref id="d190e1711a1310">
            <mixed-citation id="d190e1715" publication-type="other">
Turcotte, Louis. 2006. "De l'6conomie informelle k
la morphologie sociale de l'economie: L'Stude de
cas des Mafy ady &amp; Tananarive (Madagascar)."
Ph.D. dissertation, University de Montreal.</mixed-citation>
         </ref>
         <ref id="d190e1731a1310">
            <mixed-citation id="d190e1735" publication-type="other">
Tversky, Amos, and Daniel Kahneman. 1982. "Judg-
ment under Uncertainty: Heuristics and Biases."
In Judgment under Uncertainty : Heuristics and
Biases , ed. Daniel Kahneman, Paul Slovic, and
Amos Tversky. Cambridge, UK: Cambridge Uni-
versity Press.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
