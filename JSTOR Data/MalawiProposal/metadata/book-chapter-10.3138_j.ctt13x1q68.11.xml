<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt13x1q68</book-id>
      <subj-group>
         <subject content-type="call-number">CR6257.M323 2005</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Order of Canada</subject>
         <subj-group>
            <subject content-type="lcsh">History</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Decorations of honor</subject>
         <subj-group>
            <subject content-type="lcsh">Canada</subject>
            <subj-group>
               <subject content-type="lcsh">History</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>History</subject>
      </subj-group>
      <book-title-group>
         <book-title>The Order of Canada</book-title>
         <subtitle>Its Origins, History, and Developments</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>McCreery</surname>
               <given-names>Christopher</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>01</day>
         <month>02</month>
         <year>2015</year>
      </pub-date>
      <isbn content-type="ppub">9780802039408</isbn>
      <isbn content-type="epub">9781442627963</isbn>
      <publisher>
         <publisher-name>University of Toronto Press</publisher-name>
         <publisher-loc>Toronto; Buffalo; London</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2005</copyright-year>
         <copyright-holder>University of Toronto Press Incorporated</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.3138/j.ctt13x1q68"/>
      <abstract abstract-type="short">
         <p>Extensively illustrated with never-before-published photographs,<italic>The Order of Canada: Its Origins, History, and Developments</italic>pays tribute to the individuals who felt the need for a system of recognition for Canadians.</p>
         <p>Disclaimer: Images removed at the request of the rights holder.</p>
      </abstract>
      <counts>
         <page-count count="350"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1q68.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1q68.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1q68.3</book-part-id>
                  <title-group>
                     <title>Message from the Queen</title>
                  </title-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1q68.4</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <fpage>xiii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1q68.5</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <fpage>xv</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1q68.6</book-part-id>
                  <title-group>
                     <title>Abbreviations</title>
                  </title-group>
                  <fpage>xvii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1q68.7</book-part-id>
                  <title-group>
                     <label>CHAPTER ONE</label>
                     <title>False Starts and Honours in Canada, 1867–1917:</title>
                     <subtitle>The Creation of a Canadian Policy</subtitle>
                  </title-group>
                  <fpage>3</fpage>
                  <abstract>
                     <p>At a number of times in Canadian history and for a complex variety of reasons, the fount of honour has been turned off. The history of honours in Canada is as complex as our development as a nation. Indeed, in many respects the development of honours and honours policy in Canada has mirrored the growth of Canadian autonomy and independence – growth that ultimately led to the emergence of a Canadian identity. This national identity has realized its full potential with the creation of the Order of Canada. The Order of Canada is a fellowship of service that embodies more than</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1q68.8</book-part-id>
                  <title-group>
                     <label>CHAPTER TWO</label>
                     <title>Controversy and Discontent, 1917–1935:</title>
                     <subtitle>The Decline and Brief Revival of British Honours in Canada</subtitle>
                  </title-group>
                  <fpage>24</fpage>
                  <abstract>
                     <p>The Great War served as a catalyst for profound change in Canada and for the evolution from colony to nation.¹ As reflected in the display of military valour at Vimy Ridge and in the adoption of Resolution IX of the Imperial War Conference,² Canada was coming of age both in action and in law. Among these defining events we must include the Nickle Resolution, which essentially ended the practice of Canadians being knighted, and which also restricted the bestowal of British honours on Canadian residents. Unfortunately, the Nickle Resolution was only a half-measure; although it sought to restrict the bestowal</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1q68.9</book-part-id>
                  <title-group>
                     <label>CHAPTER THREE</label>
                     <title>The Honours Quagmire, 1935–1948:</title>
                     <subtitle>The Ubiquitous Canada Medal</subtitle>
                  </title-group>
                  <fpage>57</fpage>
                  <abstract>
                     <p>When William Lyon Mackenzie King returned to power in 1935, there was little reason to believe that honours of any type would be reintroduced or continued in Canada, yet during his wartime tenure the country came within centimetres of creating a national order. This brush with creating a Canadian order was largely precipitated by the Second World War.</p>
                     <p>The history of attempts to create a Canadian honours system and Canadian order is not dominated entirely by well-meaning civil servants; a large role was also played by soldiers eager to see their comrades – and themselves – recognized. Wars always generate a demand</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1q68.10</book-part-id>
                  <title-group>
                     <label>CHAPTER FOUR</label>
                     <title>More False Starts:</title>
                     <subtitle>Massey Presses Forward</subtitle>
                  </title-group>
                  <fpage>92</fpage>
                  <abstract>
                     <p>Few Canadians knew about the many attempts to create a national honour during and immediately after the Second World War. Many knew about the Canada Medal, which had already been announced,¹ yet that award had never been given. In the other British Dominions, there was no discussion of creating honours systems separate from Britain. Australia and New Zealand were comfortable with the British system. By war’s end, Canada had essentially removed this vestige of Imperial furniture, and the South Africans were beginning to consider a few national awards.</p>
                     <p>Strong interest in honours was found only at the most senior levels</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1q68.11</book-part-id>
                  <title-group>
                     <label>CHAPTER FIVE</label>
                     <title>First Successful Steps:</title>
                     <subtitle>Founding the Order of Canada</subtitle>
                  </title-group>
                  <fpage>107</fpage>
                  <abstract>
                     <p>With the approaching Centennial, the Canadian lexicon of symbols still lacked a key element. The Beaver had been used as a national symbol in 1851 with Sir Sandford Fleming’s design for Canada’s first postage stamp. In 1921, Canada had obtained a grant of Arms from George V, along with national colours chosen by the Sovereign himself. This was followed in 1965 by the adoption of the Maple Leaf flag. Yet Canada still lacked a national system of honours. Even newly independent countries such as Malawi and Ghana had instituted national honours; yet Canada, the first ‘Dominion,’ was still without. Although</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1q68.12</book-part-id>
                  <title-group>
                     <label>CHAPTER SIX</label>
                     <title>Selection and Reaction:</title>
                     <subtitle>The First Honours List</subtitle>
                  </title-group>
                  <fpage>132</fpage>
                  <abstract>
                     <p>Institutions take time to develop and gain acceptance, and this was true of the Order of Canada. The order, established in 1967, has changed in structure over the years. It is difficult today to imagine that at one time the order was not universally recognized in Canada. In 1967 the Canadian public was quite naive when it came to honours systems.² The Governor General, the Duke of Devonshire, had observed this in 1917, and it was still true fifty years later.</p>
                     <p>Parts of the Order of Canada were drawn from the various French and British orders; however, one tradition was</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1q68.13</book-part-id>
                  <title-group>
                     <label>CHAPTER SEVEN</label>
                     <title>Transition to a Complete Honours System</title>
                  </title-group>
                  <fpage>148</fpage>
                  <abstract>
                     <p>The decade that followed the creation of the Order of Canada and the appointment of its first members saw significant structural changes to the original institution. By 1972, British honours had been designated as Commonwealth awards and no longer considered Canadian. Then, with the addition of bravery awards and a special order for the military, the Canadian honours system came into being, and the Dominion–Commonwealth system came to a complete end. Between 1967 and 1977, the mechanics of the order’s administration and structure were formalized. This was part of a broader pattern of change within the new institution to</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1q68.14</book-part-id>
                  <title-group>
                     <label>CHAPTER EIGHT</label>
                     <title>Enlargement and Acceptance</title>
                  </title-group>
                  <fpage>168</fpage>
                  <abstract>
                     <p>As the Order of Canada acquired the patina of age, the Canadian public came to recognize it more, and even those who were initially sceptical began to accept it. In some cases, the sceptical were converted and eventually even received the accolade. An example is the case of Joseph Smallwood, Premier of Newfoundland and the last living Father of Confederation. Smallwood retired as premier in January 1972 and was subsequently nominated by the Advisory Council to be made a Companion. The Governor General approved, and the standard letter of notification was sent out. Smallwood refused the nomination, contending that he</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1q68.15</book-part-id>
                  <title-group>
                     <title>Illustrations</title>
                  </title-group>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1q68.16</book-part-id>
                  <title-group>
                     <label>CHAPTER NINE</label>
                     <title>The Logistics of a National Order</title>
                  </title-group>
                  <fpage>183</fpage>
                  <abstract>
                     <p>The actual mechanics of the Order of Canada had been devised through its constitution; just as great an effort had to be invested in physically creating the order. Problems such as finding a name for the order, what the insignia would look like, who was to make it, and other logistical matters all had to be dealt with quickly. Considering that there were less than seven months between Cabinet’s approval of the order and the investiture of the first Companion, it is remarkable how well things turned out. Regarding some elements – the motto, for example – the decisions predated Cabinet’s approval.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1q68.17</book-part-id>
                  <title-group>
                     <label>CHAPTER TEN</label>
                     <title>Unity and Diversity:</title>
                     <subtitle>The Composition of the Order of Canada</subtitle>
                  </title-group>
                  <fpage>203</fpage>
                  <abstract>
                     <p>Lester Pearson intended the Order of Canada not only to recognize worthy citizens, but also to bolster the cause of national unity. This unity could only be achieved if people from all regions of the country and all disciplines were recognized equally. Government officials have long claimed that the order is representative of ‘Canada’s diversity and strengths.’² It is easy to look at the list of the more than four thousand Canadians who have received the honour and choose specific examples of outstanding service in a wide variety of fields. A more accurate statistical survey of the order is necessary</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1q68.18</book-part-id>
                  <title-group>
                     <label>CHAPTER ELEVEN</label>
                     <title>Honorary Membership:</title>
                     <subtitle>The Extended Project</subtitle>
                  </title-group>
                  <fpage>218</fpage>
                  <abstract>
                     <p>From a very early stage, there was a desire to recognize non-Canadian citizens with the Order of Canada. That the first ‘honorary’ appointment was not made until more than a decade after the order’s founding is more a sign of a lethargic bureaucracy than of intentional neglect. In essence, the Order of Canada has two divisions: a general division open to Canadian citizens and an honorary division populated by non-Canadians. This is quite a contrast from other national orders, which usually make no distinction between awards made to citizens and those made to non-citizens.¹ In most British orders of chivalry,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1q68.19</book-part-id>
                  <title-group>
                     <label>CHAPTER TWELVE</label>
                     <title>Centrepiece of a Modern Honours System:</title>
                     <subtitle>Recognition from Sea to Sea</subtitle>
                  </title-group>
                  <fpage>228</fpage>
                  <abstract>
                     <p>The establishment of the Order of Canada required that the Canadian honours system as a whole be expanded. This was understood as early as 1968, and first acted on in 1972 when the order was restructured into a three-level entity, and with the further addition of the Order of Military Merit, the Cross of Valour, the Star of Courage, and the Medal of Bravery.</p>
                     <p>The broader system has continued to grow, with the addition of the Meritorious Service Cross in 1984, the Meritorious Service Medal in 1991, the return of the Victoria Cross as Canada’s pre-eminent military valour decoration in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1q68.20</book-part-id>
                  <title-group>
                     <title>APPENDIX ONE:</title>
                     <subtitle>MEMBERS OF THE INTERDEPARTMENTAL COMMITTEE AND AWARDS COORDINATION COMMITTEE, 1942</subtitle>
                  </title-group>
                  <fpage>239</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1q68.21</book-part-id>
                  <title-group>
                     <title>APPENDIX TWO:</title>
                     <subtitle>ROYAL WARRANT CREATING THE CANADA MEDAL, 1943</subtitle>
                  </title-group>
                  <fpage>240</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1q68.22</book-part-id>
                  <title-group>
                     <title>APPENDIX THREE:</title>
                     <subtitle>PROPOSED ROYAL WARRANT CREATING THE CANADIAN AWARD OF HONOUR AND CANADIAN DECORATION OF HONOUR, 1943</subtitle>
                  </title-group>
                  <fpage>242</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1q68.23</book-part-id>
                  <title-group>
                     <title>APPENDIX FOUR:</title>
                     <subtitle>LETTERS PATENT FOUNDING THE ORDER OF CANADA, 1967</subtitle>
                  </title-group>
                  <fpage>244</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1q68.24</book-part-id>
                  <title-group>
                     <title>APPENDIX FIVE:</title>
                     <subtitle>CONSTITUTION OF THE ORDER OF CANADA, 1967</subtitle>
                  </title-group>
                  <fpage>245</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1q68.25</book-part-id>
                  <title-group>
                     <title>APPENDIX SIX:</title>
                     <subtitle>NOMINATION FORM FROM VINCENT MASSEY</subtitle>
                  </title-group>
                  <fpage>250</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1q68.26</book-part-id>
                  <title-group>
                     <title>APPENDIX SEVEN:</title>
                     <subtitle>DISTRIBUTION OF FIRST AWARDS OF THE ORDER OF CANADA, JULY 1967</subtitle>
                  </title-group>
                  <fpage>251</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1q68.27</book-part-id>
                  <title-group>
                     <title>APPENDIX EIGHT:</title>
                     <subtitle>RECIPIENTS OF THE ORDER OF CANADA, JULY 1967</subtitle>
                  </title-group>
                  <fpage>253</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1q68.28</book-part-id>
                  <title-group>
                     <title>APPENDIX NINE:</title>
                     <subtitle>CONSTITUTION OF THE ORDER OF CANADA, 1972</subtitle>
                  </title-group>
                  <fpage>261</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1q68.29</book-part-id>
                  <title-group>
                     <title>APPENDIX TEN:</title>
                     <subtitle>ORDER OF CANADA INSIGNIA MANUFACTURE, 1967–2004</subtitle>
                  </title-group>
                  <fpage>266</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1q68.30</book-part-id>
                  <title-group>
                     <title>APPENDIX ELEVEN:</title>
                     <subtitle>CHANCELLORS OF THE ORDER OF CANADA</subtitle>
                  </title-group>
                  <fpage>269</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1q68.31</book-part-id>
                  <title-group>
                     <title>APPENDIX TWELVE:</title>
                     <subtitle>OFFICIALS OF THE ORDER OF CANADA</subtitle>
                  </title-group>
                  <fpage>270</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1q68.32</book-part-id>
                  <title-group>
                     <title>APPENDIX THIRTEEN:</title>
                     <subtitle>MEMBERS OF THE ADVISORY COUNCIL OF THE ORDER OF CANADA</subtitle>
                  </title-group>
                  <fpage>271</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1q68.33</book-part-id>
                  <title-group>
                     <title>APPENDIX FOURTEEN:</title>
                     <subtitle>HONORARY RECIPIENTS OF THE ORDER OF CANADA</subtitle>
                  </title-group>
                  <fpage>275</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1q68.34</book-part-id>
                  <title-group>
                     <title>APPENDIX FIFTEEN:</title>
                     <subtitle>CONSTITUTION OF THE ORDER OF CANADA, 2001</subtitle>
                  </title-group>
                  <fpage>279</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1q68.35</book-part-id>
                  <title-group>
                     <title>APPENDIX SIXTEEN:</title>
                     <subtitle>AMENDMENTS TO THE CONSTITUTION OF THE ORDER OF CANADA</subtitle>
                  </title-group>
                  <fpage>283</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1q68.36</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>285</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1q68.37</book-part-id>
                  <title-group>
                     <title>Bibliography</title>
                  </title-group>
                  <fpage>345</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1q68.38</book-part-id>
                  <title-group>
                     <title>Illustration Credits</title>
                  </title-group>
                  <fpage>355</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1q68.39</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>357</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
