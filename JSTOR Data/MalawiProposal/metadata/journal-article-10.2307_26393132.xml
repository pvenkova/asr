<?xml version="1.0" encoding="UTF-8"?>
<article article-type="research-article" dtd-version="1.0" xml:lang="eng">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="publisher-id">conssoci</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50019729</journal-id>
         <journal-title-group>
            <journal-title>Conservation and Society</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>WOLTERS KLUWER INDIA PRIVATE LIMITED</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">09724923</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">09753133</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">26393132</article-id>
         <article-categories>
            <subj-group>
               <subject>Article</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Understanding the Social and Ecological Outcomes of PES Projects</article-title>
            <subtitle>A Review and an Analysis</subtitle>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Adhikari</surname>
                  <given-names>Bhim</given-names>
               </string-name>
               <xref ref-type="aff" rid="af1">
                  <sup>a</sup>
               </xref>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Agrawal</surname>
                  <given-names>Arun</given-names>
               </string-name>
               <xref ref-type="aff" rid="af2">
                  <sup>b</sup>
               </xref>
            </contrib>
            <aff id="af1">
               <label>
                  <sup>a</sup>
               </label>International Development Research Center (IDRC), Ottawa, Canada</aff>
            <aff id="af2">
               <label>
                  <sup>b</sup>
               </label>School of Natural Resources and Environment, University of Michigan, Ann Arbor, MI, USA</aff>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>1</month>
            <year>2013</year>
            <string-date>2013</string-date>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink">11</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink">4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">e26393128</issue-id>
         <fpage>359</fpage>
         <lpage>374</lpage>
         <permissions>
            <copyright-statement>Copyright: © Adhikari and Agrawal 2013</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/26393132"/>
         <abstract xml:lang="eng">
            <p>Market-based approaches to environmental management, such as payments for ecosystem services (PES), have attracted unprecedented attention during the past decade. In this article, we review 26 case studies on PES from 11 countries in Asia and Latin America to help improve the understanding of the factors affecting PES schemes at the local level. We assess outcomes of the PES interventions in relation to four outcomes: equity, participation, livelihood, and environmental sustainability. Although we consider economic efficiency of these schemes to be crucial for informing policy debates, assessing it was not under the scope of this review. Our analysis shows the importance of property rights and tenure security, transaction costs, household and community characteristics, effective communication about the intervention, and the availability of PES-related information with regard to the sustainability of ecosystem service markets. The review suggests that PES schemes could target improvements in more than one outcome dimension. Focusing on the above five areas can lead to the continued provision of ecosystem services and improvements of the well-being of local inhabitants.</p>
         </abstract>
         <kwd-group>
            <label>Keywords:</label>
            <kwd>ecosystem services</kwd>
            <kwd>markets</kwd>
            <kwd>payments for ecosystem services</kwd>
            <kwd>equity</kwd>
            <kwd>participation</kwd>
            <kwd>livelihood</kwd>
            <kwd>environmental sustainability</kwd>
         </kwd-group>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="unparsed-citations">
         <title>REFERENCES</title>
         <ref id="ref1">
            <mixed-citation publication-type="other">Adhikari, B. and J.C. Lovett. 2006. Transaction costs and community-based natural resource management in Nepal. Journal of Environmental Management 78(1): 5–15.</mixed-citation>
         </ref>
         <ref id="ref2">
            <mixed-citation publication-type="other">Agarwal, C., S. Tiwari, M. Borgoyary, A. Acharya, and E. Morrison. 2007. Fair deals for watershed services in India. Natural Resource Issues No. 10. London: International Institute for Environment and Development.</mixed-citation>
         </ref>
         <ref id="ref3">
            <mixed-citation publication-type="other">Arifin, B. 2005. Institutional constraints and opportunities in developing environmental service markets: lessons from institutional studies in Indonesia. Bogor: World Agroforestry Center, Southeast Asia Regional Office.</mixed-citation>
         </ref>
         <ref id="ref4">
            <mixed-citation publication-type="other">Arocena-Francisco, H. 2003. Environmental services “payments”: experiences, constraints, and potential in the Philippines. Bogor: World Agroforestry Center, Southeast Asia Regional Office.</mixed-citation>
         </ref>
         <ref id="ref5">
            <mixed-citation publication-type="other">Asquith, N.M., M.T. Vargas, and S. Wunder. 2008. Selling two environmental services: in-kind payments for bird habitat and watershed protection in Los Negros, Bolivia. Ecological Economics 65(4): 675–684.</mixed-citation>
         </ref>
         <ref id="ref6">
            <mixed-citation publication-type="other">Barrett, C.B and P. Arcese. 1995. Are integrated conservation development projects (ICDPs) sustainable? On the conservation of large mammals in Sub-Saharan Africa. World Development 23(7): 1073–1084.</mixed-citation>
         </ref>
         <ref id="ref7">
            <mixed-citation publication-type="other">Barrett, C.B. and P. Arcese. 1998. Wildlife harvest in integrated conservation and development projects: linking harvest to household demand, agricultural production, and environmental shocks in the Serengeti. Land Economics 74: 449–465.</mixed-citation>
         </ref>
         <ref id="ref8">
            <mixed-citation publication-type="other">Berkes, F. 2006. From community-based resource management to complex systems: the scale issue and marine commons. Ecology and Society 11(1): 45.</mixed-citation>
         </ref>
         <ref id="ref9">
            <mixed-citation publication-type="other">Brown, K. 2003. Integrating conservation and development: a case of institutional misfit. Frontiers in Ecology and the Environment 1(9): 479–487.</mixed-citation>
         </ref>
         <ref id="ref10">
            <mixed-citation publication-type="other">Corbera, E., N. Kosoy, and M.M. Tuna. 2007. Equity implications of marketing ecosystem services in protected areas and rural communities: case studies from Meso-America. Global Environmental Change 17: 365–380.</mixed-citation>
         </ref>
         <ref id="ref11">
            <mixed-citation publication-type="other">Engel, S., S. Pagiola, and S. Wunder. 2008. Designing payments for environmental services in theory and practice: an overview of the issues. Ecological Economics 65(4): 663–674.</mixed-citation>
         </ref>
         <ref id="ref12">
            <mixed-citation publication-type="other">Ferraro, P. and A. Kiss. 2002. Direct payments to conserve biodiversity. Science 298(5599): 1718–1719.</mixed-citation>
         </ref>
         <ref id="ref13">
            <mixed-citation publication-type="other">Fisher, R.J. 1989. Indigenous systems of common property forest management in Nepal. Environment and Policy Institute Working Paper No. 18. Honolulu: East West Center.</mixed-citation>
         </ref>
         <ref id="ref14">
            <mixed-citation publication-type="other">Flintan, F. 2003. ‘Engendering’ Eden Volume III: women, gender and ICDPs in South and South-East Asia: lessons learnt and experiences shared. London: IIED Wildlife and Development Series No. 18, June 2003.</mixed-citation>
         </ref>
         <ref id="ref15">
            <mixed-citation publication-type="other">Giné, X. and D. Yang 2009. Insurance, credit, and technology adoption: field experimental evidence from Malawi. Journal of Development Economics 89 (2009) 1–11.</mixed-citation>
         </ref>
         <ref id="ref16">
            <mixed-citation publication-type="other">Grieg-Gran, M., I. Porras, and S. Wunder. 2005. How can market mechanisms for forest environmental services help the poor? Preliminary lessons from Latin America. World Development 33(9): 1511–1527.</mixed-citation>
         </ref>
         <ref id="ref17">
            <mixed-citation publication-type="other">Grosjean, P. and A. Kontoleon. 2009. How sustainable are sustainable development programs? The case of the Sloping Land Conversion Program in China. World Development 37(1): 268–285.</mixed-citation>
         </ref>
         <ref id="ref18">
            <mixed-citation publication-type="other">Gunatilake, H.M. 1998. The role of rural development in protecting tropical rainforests: evidence from Sri Lanka. Journal of Environmental Management 53(3): 273–292.</mixed-citation>
         </ref>
         <ref id="ref19">
            <mixed-citation publication-type="other">Hoang, M.H., M. van Noordwijk, and P.T. Thuy. 2008. Payment for environmental services: experiences and lessons from Vietnam. Hanoi: World Agroforestry Center.</mixed-citation>
         </ref>
         <ref id="ref20">
            <mixed-citation publication-type="other">Holden, S., B. Shiferaw, and M. Wik. 1998. Poverty, market imperfections and time preferences: of relevance for environmental policy? Environment and Development Economics 3: 105–130.</mixed-citation>
         </ref>
         <ref id="ref21">
            <mixed-citation publication-type="other">Houghton, R.A., J. Unruh, and P.A. Lefebvre. 1991. Current land use in the tropics and its potential for sequestering carbon. In: Proceedings of the technical workshop to explore options for global forest management (eds. Howlett, D. and C. Sargent). Pp. 297–310. April 1991, Bangkok, Thailand. London: International Institute for Environment and Development (IIED).</mixed-citation>
         </ref>
         <ref id="ref22">
            <mixed-citation publication-type="other">Huang M. and S.K. Upadhaya. 2007. Watershed-based payment for environmental services in Asia. Working Paper No. 06–07. SAMREM/OIRED Virginia Tech/USAID. August 2007.</mixed-citation>
         </ref>
         <ref id="ref23">
            <mixed-citation publication-type="other">Huberman, D. and T. Leippraud. 2006. Developing international payments for ecosystem services: a technical discussion. Background Paper. Geneva: Division of Technology, Industry and Economics and Trade Branch, UNEP.</mixed-citation>
         </ref>
         <ref id="ref24">
            <mixed-citation publication-type="other">Hughes R. and F. Flintan. 2001. Integrating conservation and development experience. London: International Institute for Environment and Development (IIED). http://www.iied.org/pubs/pdfs/9080IIED.pdf. Accessed in January, 2009.</mixed-citation>
         </ref>
         <ref id="ref25">
            <mixed-citation publication-type="other">Jack, B.K., C. Kousky, and K.R.E. Sims. 2008. Designing payments for ecosystem services: lessons from previous experience with incentivebased mechanisms. Proceedings of the National Academy of Sciences 105(28): 9465–9470.</mixed-citation>
         </ref>
         <ref id="ref26">
            <mixed-citation publication-type="other">Kosoy, N., M. Martinez-Tuna, R. Muradianand, and J. Martínez-Alier. 2007. Payments for environmental services in watersheds: insights from a comparative study of three cases in Central America. Ecological Economics 61(2–3): 446–455.</mixed-citation>
         </ref>
         <ref id="ref27">
            <mixed-citation publication-type="other">Leimona, B. and E. Lee. 2008. Pro-poor payment for environmental services: some considerations. RUPES-RECOFTC Brief. January 2008.</mixed-citation>
         </ref>
         <ref id="ref28">
            <mixed-citation publication-type="other">López, J.J. 1992. Theory choice in comparative social inquiry. Polity 25(2): 267–282.</mixed-citation>
         </ref>
         <ref id="ref29">
            <mixed-citation publication-type="other">MA (Millennium Ecosystem Assessment), 2005. Ecosystems and human well-being: synthesis. Island Press: Washington, DC.</mixed-citation>
         </ref>
         <ref id="ref30">
            <mixed-citation publication-type="other">Malla, Y.B. and R.J. Fisher. 1987. Planting trees on private farmland in Nepal: the equity aspects. In: Workshop on multipurpose trees for small farm use. Organised by Winrock International-F/FRED. Thailand, November 1–5, 1987.</mixed-citation>
         </ref>
         <ref id="ref31">
            <mixed-citation publication-type="other">Mayrand, K. and M. Paquin. 2004. Payments for environmental services: a survey and assessment of current schemes. Montreal: Unisféra International Centre for the Commission for Environmental Cooperation of North America.</mixed-citation>
         </ref>
         <ref id="ref32">
            <mixed-citation publication-type="other">Mehta, L., M. Leach, O. Newell, I Scoones, K. Sivaramakrishnan, and S.N. Way. 1999. Exploring understandings of institutions and uncertainty: new directions in natural resource management. Institute for Development Studies Discussion Paper No. 372. Brighton: Institute of Development Studies.</mixed-citation>
         </ref>
         <ref id="ref33">
            <mixed-citation publication-type="other">Pagiola, S., A. Artcenas, and G. Platais. 2005. Can payment for environmental services help reduce poverty? An exploration of the issues and the evidence to date from Latin America. World Development 33(2): 237–253.</mixed-citation>
         </ref>
         <ref id="ref34">
            <mixed-citation publication-type="other">Pagiola, S., A.R. Rios, and A. Arcenas. 2008. Can the poor participate in payments for environmental services? Lessons from the Silvopastoral project in Nicaragua. Environment and Development Economics 13(3): 299–325.</mixed-citation>
         </ref>
         <ref id="ref35">
            <mixed-citation publication-type="other">Scoones, I. 1998. Sustainable rural livelihoods: a framework for analysis. Institute for Development Studies Working Paper No. 72. Brighton: Institute for Development Studies.</mixed-citation>
         </ref>
         <ref id="ref36">
            <mixed-citation publication-type="other">Sedjo, R.A. and A.J. Solomon. 1991. Climate and forests. In: Greenhouse warming: abatement and adaptation (eds. N.S. Rosenberg, .E. Easterling, P.R. Crosson, and J. Dormstadter). Workshop proceedings. Washington, DC: World Resources Institute.</mixed-citation>
         </ref>
         <ref id="ref37">
            <mixed-citation publication-type="other">Sureshwaran, S., S.R. Londhe, and P. Frazier.1996. A logit model for evaluating farmer participation in soil conservation programs: sloping agricultural land technology in upland farms in Philippines. Journal of Sustainable Agriculture 7(4): 57–69.</mixed-citation>
         </ref>
         <ref id="ref38">
            <mixed-citation publication-type="other">Swallow, B., R. Median-Dick, and M. van Noordwijk. 2005. Localizing demand and supply of environmental services: interactions with property rights, collective action and the welfare of the poor. CAPRi Working Paper No. 42. Washington, DC: Environment Protection and Technology Development, International Food Policy Research Institute.</mixed-citation>
         </ref>
         <ref id="ref39">
            <mixed-citation publication-type="other">TEEB (The Economics of Ecosystems and Biodiversity). 2009. The Economics of Ecosystems and Biodiversity for national and international policy makers. Brussels: United Nations Environment Programme.</mixed-citation>
         </ref>
         <ref id="ref40">
            <mixed-citation publication-type="other">Thatcher, T.A, D.R. Lee, and J. Schelhas. 1997. Farmer participation in government sponsored reforestation incentive programs in Costa Rica. Agro-forestry Systems 35: 269–289.</mixed-citation>
         </ref>
         <ref id="ref41">
            <mixed-citation publication-type="other">The, B.D. and H.B. Ngoc. 2006. Payment for environmental services in Vietnam: assessing an economic approach to sustainable forest management. Research Report No. 2006-RR3. Singapore: Economy and Environment Program for Southeast Asia.</mixed-citation>
         </ref>
         <ref id="ref42">
            <mixed-citation publication-type="other">Tu, Q., A.P.J. Mol, L. Zhang, and R. Ruben. 2011. How do trust and property security influence household contributions to public goods? The case of the Sloping Land Conversion Program in China. China Economic Review 22(4): 499–511.</mixed-citation>
         </ref>
         <ref id="ref43">
            <mixed-citation publication-type="other">UNEP/LSE (United Nations Environment Programme/London School of Economics). 2005. High-level brainstorming workshop on creating pro-poor markets for ecosystem services: identifying a mid-term strategy for a possible plan of action of cooperation. Division of Environmental Conventions, UNDP in cooperation with London School of Economics. October 11–12, 2005.</mixed-citation>
         </ref>
         <ref id="ref44">
            <mixed-citation publication-type="other">Upadhaya, S. 2005. Payments for environmental services: sharing hydropower benefits with upland communities. Rewarding Upland Poor for Environmental Services (RUPES) Working Paper No. 1. Kathmandu: Winrock International.</mixed-citation>
         </ref>
         <ref id="ref45">
            <mixed-citation publication-type="other">Wells, M.P., T.O. McShane, H.T. Dublin, S. O’Connor, and K.H. Redford. 2004. The future of integrated conservation and development projects: building on what works. In: Getting biodiversity projects to work: towards more effective conservation and development (eds. McShane, T.O. and M.P. Wells). Pp. 397–422. New York, NY: Columbia University Press.</mixed-citation>
         </ref>
         <ref id="ref46">
            <mixed-citation publication-type="other">Wunder, S. 2005. Payments for environmental services: some nuts and bolts. CIFOR Occasional Paper No. 2. Bogor: Center for International Forestry Research.</mixed-citation>
         </ref>
         <ref id="ref47">
            <mixed-citation publication-type="other">Zbinden, S. and D.R. Lee. 2005. Paying for environmental services: an analysis of participation in Costa Rica’s PSA program. World Development 33(2): 255–272.</mixed-citation>
         </ref>
         <ref id="ref48">
            <mixed-citation publication-type="other">Zhang, L., Q. Tu, and P.J. Mol. 2008. Payment for environmental services: the Sloping Land Conversion Program in Ningxia Autonomous Region of China. China and World Economy 16(2): 66–81.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
