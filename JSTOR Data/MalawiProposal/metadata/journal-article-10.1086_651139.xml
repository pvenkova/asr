<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         article-type="research-article"
         dtd-version="1.0"
         xml:lang="en">
   <front>
      <journal-meta>
         <journal-id journal-id-type="publisher-id">compeducrevi</journal-id>
         <journal-title-group>
            <journal-title>Comparative Education Review</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>The University of Chicago Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">00104086</issn>
         <issn pub-type="epub">1545701X</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="doi">10.1086/651139</article-id>
         <article-id pub-id-type="msid">CER1079</article-id>
         <title-group>
            <article-title>Waves of Educational Model Production: The Case of Higher Education Institutionalization in Malawi, 1964–2004</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author" xlink:type="simple">
               <string-name>
                  <given-names>Dana G.</given-names>
                  <x xml:space="preserve"> </x>
                  <surname>Holland</surname>
               </string-name>
               <xref ref-type="fn" rid="fn1"/>
            </contrib>
         </contrib-group>
         <pub-date pub-type="ppub">
            <month>05</month>
            <year>2010</year>
            <string-date>May 2010</string-date>
         </pub-date>
         <pub-date pub-type="ppub">
            <day>23</day>
            <month>02</month>
            <year>2010</year>
            <string-date>February 23, 2010</string-date>
         </pub-date>
         <volume>54</volume>
         <issue>2</issue>
         <issue-id>651219</issue-id>
         <fpage>199</fpage>
         <lpage>222</lpage>
         <permissions>
            <copyright-statement>© 2010 by the Comparative and International Education Society. All rights reserved.</copyright-statement>
            <copyright-year>2010</copyright-year>
            <copyright-holder>the Comparative and International Education Society</copyright-holder>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/10.1086/651139"/>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>en</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
      <notes notes-type="footnote">
         <fn-group>
            <fn id="fn1">
               <p>The research reported here was supported by an International Dissertation Field Research Fellowship awarded by the Social Science Research Council (SSRC) in 2004. In addition to financial assistance, the SSRC’s facilitation of an intellectual support network proved invaluable to the project. I benefited from the opportunity to present an early draft of the core theoretical ideas in this article at the 2005 Princeton Institute for International and Regional Studies Graduate Student Conference. Insightful feedback on the project was made by Miguel Centeno, David Grazian, Lewis Dzimbiri, Kathleen Hall, Henrika Kuklick, John W. Meyer, and Susan Cotts Watkins. The comments of four anonymous reviewers as well as the<italic>C</italic>
                  <italic>ER</italic>editors contributed significantly to this article’s theoretical argument.</p>
            </fn>
         </fn-group>
      </notes>
   </front>
   <back>
      <ref-list content-type="unparsed">
         <title>References</title>
         <ref id="rf1">
            <mixed-citation xlink:type="simple" publication-type="">ACE (American Council on Education). 1964.<italic>Education for Development: Report of the Survey Team on Education in Malawi</italic>. Washington, DC: American Council on Education.</mixed-citation>
         </ref>
         <ref id="rf2">
            <mixed-citation xlink:type="simple" publication-type="">Ajayi, J. F. Ade, L. K. G. Goma, and G. Ampah Johnson. 1996.<italic>The African Experience with Higher Education</italic>. Accra: Association of African Universities.</mixed-citation>
         </ref>
         <ref id="rf3">
            <mixed-citation xlink:type="simple" publication-type="">Altbach, P. G. 2004. “Globalization and the University: Myths and Realities in an Unequal World.”<italic>Tertiary Education and Management</italic>1:1–20.</mixed-citation>
         </ref>
         <ref id="rf4">
            <mixed-citation xlink:type="simple" publication-type="">Anderson‐Levitt, K. M. 2003. “A World Culture of Schooling?” In<italic>Local Meanings, Global Schooling: Anthropology and World Culture Theory</italic>, ed. K. M. Anderson‐Levitt. New York: Palgrave Macmillan.</mixed-citation>
         </ref>
         <ref id="rf5">
            <mixed-citation xlink:type="simple" publication-type="">Ashby, E. 1966.<italic>Universities: British, Indian, African; A Study in the Ecology of Higher Education</italic>. Cambridge, MA: Harvard University Press.</mixed-citation>
         </ref>
         <ref id="rf6">
            <mixed-citation xlink:type="simple" publication-type="">Bartlett, L. 2003. “World Culture or Transnational Project? Competing Educational Projects in Brazil.” In<italic>Local Meanings, Global Schooling: Anthropology and World Culture Theory</italic>, ed. K. M. Anderson‐Levitt. New York: Palgrave Macmillan.</mixed-citation>
         </ref>
         <ref id="rf7">
            <mixed-citation xlink:type="simple" publication-type="">Berman, E. 1980a. “Educational Colonialism in Africa: The Role of American Foundations, 1910–1945.” In<italic>Philanthropy and Cultural Imperialism: The Foundations at Home and Abroad</italic>, ed. R. F. Arnove. Boston: G. K. Hall.</mixed-citation>
         </ref>
         <ref id="rf8">
            <mixed-citation xlink:type="simple" publication-type="">Berman, E. 1980b. “The Foundations’ Role in American Foreign Policy: The Case of Africa, post 1945.” In<italic>Philanthropy and Cultural Imperialism: The Foundations at Home and Abroad</italic>, ed. R. F. Arnove. Boston: G. K. Hall.</mixed-citation>
         </ref>
         <ref id="rf9">
            <mixed-citation xlink:type="simple" publication-type="">Bloom, D., D. Canning, and K. Chan. 2006.<italic>Higher Education and Economic Development in Africa</italic>. Washington, DC: Human Development Sector Africa Region, World Bank.</mixed-citation>
         </ref>
         <ref id="rf10">
            <mixed-citation xlink:type="simple" publication-type="">Bockman, J., and G. Eyal. 2002. “Eastern Europe as a Laboratory for Economic Knowledge: The Transnational Roots of Neoliberalism.”<italic>American Journal of Sociology</italic>108 (2): 310–52.</mixed-citation>
         </ref>
         <ref id="rf11">
            <mixed-citation xlink:type="simple" publication-type="">Burawoy, M. 2001. “Manufacturing the Global.”<italic>Ethnography</italic>2 (2): 147–59.</mixed-citation>
         </ref>
         <ref id="rf12">
            <mixed-citation xlink:type="simple" publication-type="">Campbell, J. L. 2002. “Ideas, Politics, and Public Policy.”<italic>Annual Review of Sociology</italic>28:21–38.</mixed-citation>
         </ref>
         <ref id="rf13">
            <mixed-citation xlink:type="simple" publication-type="">Chancellor College. 1974.<italic>Chancellor College Inauguration Day, 2nd July 1974, Souvenir Brochure</italic>. Limbe: Montfort.</mixed-citation>
         </ref>
         <ref id="rf14">
            <mixed-citation xlink:type="simple" publication-type="">Chimombo, J. P. A. 2003. “Malawi: Country Higher Education Profiles.” Center for International Higher Education, Boston College.<ext-link ext-link-type="uri"
                         xlink:href="http://www.bc.edu/bc_org/avp/soe/cihe/inhea/profiles/Malawi.htm"
                         xlink:type="simple">http://www.bc.edu/bc_org/avp/soe/cihe/inhea/profiles/Malawi.htm</ext-link>.</mixed-citation>
         </ref>
         <ref id="rf15">
            <mixed-citation xlink:type="simple" publication-type="">Cooper, F., and R. Packard, eds. 1997.<italic>International Development and the Social Sciences: Essays on the History and Politics of Knowledge</italic>. Berkeley: University of California Press.</mixed-citation>
         </ref>
         <ref id="rf16">
            <mixed-citation xlink:type="simple" publication-type="">Cummings, G. D. 2001.<italic>Aid to Africa: French and British Policies from the Cold War to the New Millennium</italic>. Aldershot: Ashgate.</mixed-citation>
         </ref>
         <ref id="rf17">
            <mixed-citation xlink:type="simple" publication-type="">DiMaggio, P. J., and W. W. Powell. 1983. “The Iron Cage Revisited: Institutional Isomorphism and Collective Rationality in Organizational Fields.”<italic>American Sociological Review</italic>48:147–60.</mixed-citation>
         </ref>
         <ref id="rf18">
            <mixed-citation xlink:type="simple" publication-type="">Dubbey, J. M. 1989. “The University in a Developing Country.”<italic>Higher Education Policy</italic>2 (1): 41–43.</mixed-citation>
         </ref>
         <ref id="rf19">
            <mixed-citation xlink:type="simple" publication-type="">Dubbey, J. M. 1994.<italic>Warm Hearts, White Hopes</italic>. Pretoria West: Penrose.</mixed-citation>
         </ref>
         <ref id="rf20">
            <mixed-citation xlink:type="simple" publication-type="">Gibbons, M. 1998.<italic>Higher Education Relevance in the 21st Century</italic>. Washington, DC: Human Development Network, World Bank.</mixed-citation>
         </ref>
         <ref id="rf21">
            <mixed-citation xlink:type="simple" publication-type="">GOM (Government of Malawi). 2002.<italic>Basic Education Statistics</italic>. Lilongwe: Ministry of Education, Government Printer.</mixed-citation>
         </ref>
         <ref id="rf22">
            <mixed-citation xlink:type="simple" publication-type="">GOM (Government of Malawi). 2006.<italic>Education Statistics</italic>. Lilongwe: Ministry of Education, Government Printer.</mixed-citation>
         </ref>
         <ref id="rf23">
            <mixed-citation xlink:type="simple" publication-type="">GOM and EU (Government of Malawi and European Union). 2004.<italic>Proposal for the Establishment of a Public Policy Analysis and Research Organization in Malawi</italic>. Capacity Building Project for Economic Management and Policy Coordination (ECONMANCAP), Technical Report no. 26. Prepared by Arcadis Euroconsult for GOM and EU.</mixed-citation>
         </ref>
         <ref id="rf24">
            <mixed-citation xlink:type="simple" publication-type="">Guillen, M. F. 2001. “Is Globalization Civilizing, Destructive or Feeble? A Critique of Five Key Debates in the Social Science Literature.”<italic>Annual Review of Sociology</italic>27:235–60.</mixed-citation>
         </ref>
         <ref id="rf25">
            <mixed-citation xlink:type="simple" publication-type="">Harrigan, J. 2001.<italic>From Dictatorship to Democracy: Economic Policy in Malawi, 1964–2000</italic>. Aldershot: Ashgate.</mixed-citation>
         </ref>
         <ref id="rf26">
            <mixed-citation xlink:type="simple" publication-type="">Heyneman, S. P. 2003. “The History and Problems in the Making of Education Policy at the World Bank, 1960–2000.”<italic>International Journal of Educational Development</italic>23:315–37.</mixed-citation>
         </ref>
         <ref id="rf27">
            <mixed-citation xlink:type="simple" publication-type="">Holland, D. G. 2009. “Research Values in Context: The Influence of the Nation and the Market on Social Research Production in Malawi.”<italic>Science, Technology, and Human Values</italic>34 (5): 551–72.</mixed-citation>
         </ref>
         <ref id="rf28">
            <mixed-citation xlink:type="simple" publication-type="">Hugo, P. 1998. “Transformation: The Changing Context of Academia in Post‐apartheid South Africa.”<italic>African Affairs</italic>97:5–27.</mixed-citation>
         </ref>
         <ref id="rf29">
            <mixed-citation xlink:type="simple" publication-type="">Hunnings, G. 1975.<italic>The Realization of a Dream: The University of Malawi, 1964–1974</italic>. Zomba: Government of Malawi.</mixed-citation>
         </ref>
         <ref id="rf30">
            <mixed-citation xlink:type="simple" publication-type="">Jones, P. W. 2004. “Taking the Credit: Financing and Policy Linkages in the Education Portfolio of the World Bank.” In<italic>The Global Politics of Educational Borrowing and Lending</italic>, ed. Gita Steiner‐Khamsi. New York: Teachers College, Columbia University.</mixed-citation>
         </ref>
         <ref id="rf31">
            <mixed-citation xlink:type="simple" publication-type="">Kadzamira, Z. D. 1977. “Planning for Development in Malawi, 1954–1974.”<italic>Journal of Social Science</italic>6:60–82.</mixed-citation>
         </ref>
         <ref id="rf32">
            <mixed-citation xlink:type="simple" publication-type="">Kimble, D. 1978.<italic>The University and the Nation: Address to the Congregation Held in Zomba, 29 July 1978</italic>. Zomba: Government of Malawi.</mixed-citation>
         </ref>
         <ref id="rf33">
            <mixed-citation xlink:type="simple" publication-type="">King, K., and S. McGrath. 2004.<italic>Knowledge for Development? Comparing British, Japanese, Swedish and World Bank Aid</italic>. London: Zed.</mixed-citation>
         </ref>
         <ref id="rf34">
            <mixed-citation xlink:type="simple" publication-type="">Küster, S. 1999.<italic>African Education in Colonial Zimbabwe, Zambia and Malawi: Government Control, Settler Antagonism and African Agency, 1890–1964</italic>. Hamburg: Lit.</mixed-citation>
         </ref>
         <ref id="rf35">
            <mixed-citation xlink:type="simple" publication-type="">Lancaster, C. 1999.<italic>Aid to Africa: So Much to Do, So Little Done</italic>. Chicago: University of Chicago Press.</mixed-citation>
         </ref>
         <ref id="rf36">
            <mixed-citation xlink:type="simple" publication-type="">Latham, M. E. 2000.<italic>Modernization as Ideology: American Social Science and “Nation Building” in the Kennedy Era</italic>. Chapel Hill: University of North Carolina Press.</mixed-citation>
         </ref>
         <ref id="rf37">
            <mixed-citation xlink:type="simple" publication-type="">Latham, R., R. Kassimir, and T. M. Callaghy. 2001. “Introduction: Transboundary Formations, Intervention, Order, and Authority.” In<italic>Intervention and Transnationalism in Africa: Global‐Local Networks of Power</italic>, ed. T. M. Callaghy, R. Kassimir, and R. Latham. Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="rf38">
            <mixed-citation xlink:type="simple" publication-type="">Latour, B. 1987.<italic>Science in Action</italic>. Cambridge, MA: Harvard University Press.</mixed-citation>
         </ref>
         <ref id="rf39">
            <mixed-citation xlink:type="simple" publication-type="">Latour, B. 1988.<italic>The Pasteurization of France</italic>. Cambridge, MA: Harvard University Press.</mixed-citation>
         </ref>
         <ref id="rf40">
            <mixed-citation xlink:type="simple" publication-type="">Meyer, J. W. 1980. “The World Polity and the Authority of the Nation‐State.” In<italic>Studies of the Modern World‐System</italic>, ed. A. Bergesen. New York: Academic Press.</mixed-citation>
         </ref>
         <ref id="rf41">
            <mixed-citation xlink:type="simple" publication-type="">Meyer, J. W., J. Boli, G. M. Thomas, and F. O. Ramirez. 1997. “World Society and the Nation‐State.”<italic>American Journal of Sociology</italic>103 (1): 144–81.</mixed-citation>
         </ref>
         <ref id="rf42">
            <mixed-citation xlink:type="simple" publication-type="">Meyer, J. W., F. O. Ramirez, D. J. Frank, and E. Schofer. 2006. “Higher Education as an Institution.” Center on Democracy, Development, and the Rule of Law Working Paper, Stanford University.</mixed-citation>
         </ref>
         <ref id="rf43">
            <mixed-citation xlink:type="simple" publication-type="">Meyer, J. W., F. O. Ramirez, and Y. N. Soysal. 1992. “World Expansion of Mass Education, 1970–1980.”<italic>Sociology of Education</italic>65 (2): 128–49.</mixed-citation>
         </ref>
         <ref id="rf44">
            <mixed-citation xlink:type="simple" publication-type="">Meyer, J. W., and B. Rowan. 1977. “Institutionalized Organizations: Formal Structure as Myth and Ceremony.”<italic>American Journal of Sociology</italic>83 (2): 340–63.</mixed-citation>
         </ref>
         <ref id="rf45">
            <mixed-citation xlink:type="simple" publication-type="">Michael, I. 1978. “Academic Autonomy and Governmental Demands: The Case of Malawi.”<italic>Minerva</italic>16 (4): 465–79.</mixed-citation>
         </ref>
         <ref id="rf46">
            <mixed-citation xlink:type="simple" publication-type="">Mkandawire, N. A. 1986.<italic>Study on Small Countries Research</italic>. Ottawa: International Development Research Centre.</mixed-citation>
         </ref>
         <ref id="rf47">
            <mixed-citation xlink:type="simple" publication-type="">Morton, K. 1975.<italic>Aid and Dependence: British Aid to Malawi</italic>. London: Croom Helm.</mixed-citation>
         </ref>
         <ref id="rf48">
            <mixed-citation xlink:type="simple" publication-type="">Mosley, P., J. Harrigan, and J. F. J. Toye. 1991.<italic>Case Studies</italic>. Vol. 2 of<italic>Aid and Power: The World Bank and Policy‐Based Lending</italic>. London: Routledge.</mixed-citation>
         </ref>
         <ref id="rf49">
            <mixed-citation xlink:type="simple" publication-type="">Moyo, C. 1992. “Education Policy and Development Strategy in Malawi.” In<italic>Malawi at the Crossroads: The Post‐colonial Political Economy</italic>, ed. G. C. Z. Mhone. Harare: Sapes.</mixed-citation>
         </ref>
         <ref id="rf50">
            <mixed-citation xlink:type="simple" publication-type="">Norwegian Ministry of Foreign Affairs. 1999.<italic>Strategy for Strengthening Research and Higher Education in the Context of Norway’s Relation with Developing Countries</italic>. Oslo: Ministry of Foreign Affairs.<ext-link ext-link-type="uri"
                         xlink:href="http://www.norad.no/en/Tools+and+publications/Publications/Publication+Page?key=109530"
                         xlink:type="simple">http://www.norad.no/en/Tools+and+publications/Publications/Publication+Page?key=109530</ext-link>.</mixed-citation>
         </ref>
         <ref id="rf51">
            <mixed-citation xlink:type="simple" publication-type="">Pachai, B. 1967. “University Education in Malawi.”<italic>Africa Quarterly</italic>6 (4): 343–51.</mixed-citation>
         </ref>
         <ref id="rf52">
            <mixed-citation xlink:type="simple" publication-type="">Phillips, D. 2005. “Toward a Theory of Policy Attraction in Education.” In<italic>The Global Politics of Educational Borrowing and Lending</italic>, ed. G. Steiner‐Khamsi. New York: Teachers College, Columbia University.</mixed-citation>
         </ref>
         <ref id="rf53">
            <mixed-citation xlink:type="simple" publication-type="">Phillips, E. H. 1976. “H. B. M. Chipembere, 1930–75: Malawi Patriot.”<italic>Ufahamu</italic>7.</mixed-citation>
         </ref>
         <ref id="rf54">
            <mixed-citation xlink:type="simple" publication-type="">Porter, A. T. 1971. “University Development in English‐Speaking Africa: Problems and Opportunities.”<italic>African Affairs</italic>71 (282): 73–83.</mixed-citation>
         </ref>
         <ref id="rf55">
            <mixed-citation xlink:type="simple" publication-type="">President’s Task Force on Foreign Economic Assistance. 1961.<italic>The Act for International Development: A Program for the Decade of Development, Objectives, Concepts, and Proposed Program</italic>. Washington, DC: International Cooperation Administration.</mixed-citation>
         </ref>
         <ref id="rf56">
            <mixed-citation xlink:type="simple" publication-type="">Ramirez, F. O., and J. W. Meyer. 1980. “Comparative Education: The Social Construction of the Modern World System.”<italic>Annual Review of Sociology</italic>6:369–99.</mixed-citation>
         </ref>
         <ref id="rf57">
            <mixed-citation xlink:type="simple" publication-type="">Richards, A. I. 1965. “The Adaptation of Universities to the African Situation: Review Article.”<italic>Minerva</italic>3 (3): 336–42.</mixed-citation>
         </ref>
         <ref id="rf58">
            <mixed-citation xlink:type="simple" publication-type="">Roberts, B. 1970. “Malawi: Progress and Problems since Independence.”<italic>African Affairs</italic>69 (274): 60–64.</mixed-citation>
         </ref>
         <ref id="rf59">
            <mixed-citation xlink:type="simple" publication-type="">Rosen, L. 2003. “The Politics of Identity and the Marketization of U.S. Schools: How Local Meaning Mediates Global Struggles.” In<italic>Local Meanings, Global Schooling: Anthropology and World Culture Theory</italic>, ed. K. M. Anderson‐Levitt. New York: Palgrave Macmillan.</mixed-citation>
         </ref>
         <ref id="rf60">
            <mixed-citation xlink:type="simple" publication-type="">Sall, E. 2002.<italic>Working Draft: The Social Science in Sub‐Saharan Africa</italic>. Uppsala: Nordic Africa Institute and Social Science Research Council.</mixed-citation>
         </ref>
         <ref id="rf61">
            <mixed-citation xlink:type="simple" publication-type="">Samoff, J., ed. 1994.<italic>Coping with Crisis: Austerity, Adjustment, and Human Resources</italic>. London: Cassell.</mixed-citation>
         </ref>
         <ref id="rf62">
            <mixed-citation xlink:type="simple" publication-type="">Samoff, J. 2003. “Institutionalizing International Influence.” In<italic>Comparative Education: The Dialectic of the Global and the Local</italic>, ed. R. F. Arnove and C. A. Torres. 2nd ed. Boulder, CO: Rowman &amp; Littlefield.</mixed-citation>
         </ref>
         <ref id="rf63">
            <mixed-citation xlink:type="simple" publication-type="">Schofer, E., and J. W. Meyer. 2005. “The Worldwide Expansion of Higher Education in the Twentieth Century.”<italic>American Sociological Review</italic>70:898–920.</mixed-citation>
         </ref>
         <ref id="rf64">
            <mixed-citation xlink:type="simple" publication-type="">Schriewer, J. 2004. “Multiple Internationalities.” In<italic>Transnational Intellectual Networks</italic>, ed. C. Charle, J. Schriewer, and P. Wagner. Frankfurt: Campus.</mixed-citation>
         </ref>
         <ref id="rf65">
            <mixed-citation xlink:type="simple" publication-type="">Schriewer, J., and C. Martinez. 2004. “Constructions of Internationality in Education.” In<italic>The Global Politics of Educational Borrowing and Lending</italic>, ed. Gita Steiner‐Khamsi. New York: Teachers College, Columbia University.</mixed-citation>
         </ref>
         <ref id="rf66">
            <mixed-citation xlink:type="simple" publication-type="">Schultz, T. 1961. “Investment in Human Capital.”<italic>American Economic Review</italic>51:1–17.</mixed-citation>
         </ref>
         <ref id="rf67">
            <mixed-citation xlink:type="simple" publication-type="">Slaughter, S., and L. L. Leslie. 1997.<italic>Academic Capitalism: Politics, Policies and the Entrepreneurial University</italic>. Baltimore: Johns Hopkins University Press.</mixed-citation>
         </ref>
         <ref id="rf68">
            <mixed-citation xlink:type="simple" publication-type="">Stambach, A. 2003. “World‐Culture and Anthropological Interpretations of ‘Choice Programming’ in Tanzania.” In<italic>Local Meanings, Global Schooling: Anthropology and World Culture Theory</italic>, ed. K. M. Anderson‐Levitt. New York: Palgrave Macmillan.</mixed-citation>
         </ref>
         <ref id="rf69">
            <mixed-citation xlink:type="simple" publication-type="">Steiner‐Khamsi, G. 2004. “Globalization in Education: Real or Imagined.” In<italic>The Global Politics of Educational Borrowing and Lending</italic>, ed. G. Steiner‐Khamsi. New York: Teachers College, Columbia University.</mixed-citation>
         </ref>
         <ref id="rf70">
            <mixed-citation xlink:type="simple" publication-type="">Task Force on Higher Education in Developing Countries. 2000.<italic>Higher Education in Developing Countries: Peril and Promise</italic>. Washington, DC: World Bank.</mixed-citation>
         </ref>
         <ref id="rf71">
            <mixed-citation xlink:type="simple" publication-type="">Teferra, D., and P. G. Altbach. 2003. “Trends and Perspectives in African Higher Education.” In<italic>African Higher Education: An International Reference Handbook</italic>, ed. D. Teferra and P. G. Altbach. Bloomington: Indiana University Press.</mixed-citation>
         </ref>
         <ref id="rf72">
            <mixed-citation xlink:type="simple" publication-type="">University of Malawi. n.d.<italic>Academic Staff by Qualification/Faculty: 1996–2001</italic>. Zomba: University of Malawi.</mixed-citation>
         </ref>
         <ref id="rf73">
            <mixed-citation xlink:type="simple" publication-type="">USAID (United States Agency for International Development). 1964.<italic>Country Assistance Plan, Malawi</italic>. Washington, DC: USAID.</mixed-citation>
         </ref>
         <ref id="rf74">
            <mixed-citation xlink:type="simple" publication-type="">USAID (United States Agency for International Development). 1980.<italic>Malawi: Country Development Statement</italic>. Washington, DC: USAID.</mixed-citation>
         </ref>
         <ref id="rf75">
            <mixed-citation xlink:type="simple" publication-type="">USAID (United States Agency for International Development). 1981.<italic>Malawi: Country Development Strategy Statement</italic>. Washington, DC: USAID.</mixed-citation>
         </ref>
         <ref id="rf76">
            <mixed-citation xlink:type="simple" publication-type="">Vail, L., and L. White. 1989. “Tribalism in the Political History of Malawi.” In<italic>The Creation of Tribalism in Southern Africa</italic>, ed. L. Vail. Berkeley: University of California Press.</mixed-citation>
         </ref>
         <ref id="rf77">
            <mixed-citation xlink:type="simple" publication-type="">Williams, T. D. 1978.<italic>Malawi: The Politics of Despair</italic>. Ithaca, NY: Cornell University Press.</mixed-citation>
         </ref>
         <ref id="rf78">
            <mixed-citation xlink:type="simple" publication-type="">World Bank. 2002.<italic>Constructing Knowledge Societies: New Challenges for Tertiary Education</italic>. Washington, DC: World Bank.</mixed-citation>
         </ref>
         <ref id="rf79">
            <mixed-citation xlink:type="simple" publication-type="">Wykstra, R. A. 1971.<italic>Human Capital Formation and Manpower Development</italic>. New York: Free Press.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
