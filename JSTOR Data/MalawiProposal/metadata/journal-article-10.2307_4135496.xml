<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">taxon</journal-id>
         <journal-id journal-id-type="jstor">j100387</journal-id>
         <journal-title-group>
            <journal-title>Taxon</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>International Association for Plant Taxonomy</publisher-name>
         </publisher>
         <issn pub-type="ppub">00400262</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="doi">10.2307/4135496</article-id>
         <article-categories>
            <subj-group>
               <subject>Biodiversity and Conservation</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Lessons Learned from the SABONET Project While Building Capacity to Document the Botanical Diversity of Southern Africa</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>Stefan J. Siebert</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Gideon F.</given-names>
                  <surname>Smith</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>2</month>
            <year>2004</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">53</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id>i388571</issue-id>
         <fpage>119</fpage>
         <lpage>126</lpage>
         <page-range>119-126</page-range>
         <permissions>
            <copyright-statement>Copyright 2004 IAPT</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/4135496"/>
         <abstract>
            <p>The Southern African Botanical Diversity Network (SABONET) is a donor-funded capacity-building project supported by the GEF-UNDP and USAID/IUCN ROSA. The ten southern African countries that make up the network are Angola, Botswana, Lesotho, Malawi, Mozambique, Namibia, South Africa, Swaziland, Zambia and Zimbabwe. Participating institutions in these countries are involved in the compilation of national plant checklists, production of botanical inventories for centres of plant diversity, regional and national plant collecting expeditions, computerisation of herbarium specimens, support of postgraduate studies in systematics, biodiversity research projects, regional and national training courses and workshops, implementation of threatened plants programmes in botanical gardens and the compilation of Plant Red Data Lists. Since the inception of the Project in 1996, these efforts have contributed positively towards networking and capacity-building among botanists and providing baseline information for the in situ and ex situ conservation of the flora of the southern African region. This paper discusses the lessons learned during the implementation of the project up to December 2002 and outlines the contribution SABONET has made towards building the capacity of focal institutions in southern Africa. It also highlights the main strengths of the project and provides a 20-point plan for a successful regional network. Other issues that pertain to the SABONET Project are discussed under challenges and opportunities, constraints to improvement and complementing other networks. It is hoped that this paper will provide some insight into the experience of southern Africa in documenting biodiversity. By evaluating our successes and failures we hope the findings presented in this paper will serve as guidelines for other projects to accomplish even more than we had.</p>
         </abstract>
         <kwd-group>
            <kwd>Biodiversity</kwd>
            <kwd>Botany</kwd>
            <kwd>Capacity Building</kwd>
            <kwd>GEF-UNDP</kwd>
            <kwd>Network</kwd>
            <kwd>Southern Africa</kwd>
            <kwd>Taxonomy</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Literature Cited</title>
         <ref id="d939e160a1310">
            <mixed-citation id="d939e164" publication-type="journal">
Arnold, T. H. &amp;amp; Siebert, S. J. 2002. Computerisation of
Southern Africa Herbaria: Regional Update. SABONET
News 7: 92-96.<person-group>
                  <string-name>
                     <surname>Arnold</surname>
                  </string-name>
               </person-group>
               <fpage>92</fpage>
               <volume>7</volume>
               <source>SABONET News</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d939e199a1310">
            <mixed-citation id="d939e203" publication-type="journal">
BioNET-INTERNATIONAL. 2002. Third Global Taxonomy
Workshop agrees to partnership approach for Global
Taxonomy Initiative for CBD. BioNET-INTERNATIONAL
News 11: 1-5.<person-group>
                  <string-name>
                     <surname>BioNET-INTERNATIONAL</surname>
                  </string-name>
               </person-group>
               <fpage>1</fpage>
               <volume>11</volume>
               <source>BioNET-INTERNATIONAL News</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d939e241a1310">
            <mixed-citation id="d939e245" publication-type="book">
Chapano, C. 2002. A Checklist of Zimbabwean Grasses.
SABONET Report Series No. 16. SABONET, Pretoria.<person-group>
                  <string-name>
                     <surname>Chapano</surname>
                  </string-name>
               </person-group>
               <source>A Checklist of Zimbabwean Grasses</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d939e270a1310">
            <mixed-citation id="d939e274" publication-type="book">
Craven, P. (ed.). 1999. A checklist of Namibian plant species.
SABONET Report Series No. 7. SABONET, Windhoek.<person-group>
                  <string-name>
                     <surname>Craven</surname>
                  </string-name>
               </person-group>
               <source>A checklist of Namibian plant species</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d939e300a1310">
            <mixed-citation id="d939e304" publication-type="journal">
Golding, J. S. 2001. Southern African herbaria and Red Data
Lists. Taxon 50: 593-603.<object-id pub-id-type="jstor">10.2307/1223911</object-id>
               <fpage>593</fpage>
            </mixed-citation>
         </ref>
         <ref id="d939e320a1310">
            <mixed-citation id="d939e324" publication-type="journal">
Huntley, B. J. 2002. South Africa: the flowering of the
Rainbow Nation. Plant Talk 29: 34-40.<person-group>
                  <string-name>
                     <surname>Huntley</surname>
                  </string-name>
               </person-group>
               <fpage>34</fpage>
               <volume>29</volume>
               <source>Plant Talk</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d939e356a1310">
            <mixed-citation id="d939e360" publication-type="book">
Huntley, B. J., Matos, E. M., Aye, T. T., Nermark, U.,
Nagendran, C. R., Seyani, J. H., Da Silva, M. A. C.,
Izidine, S., Maggs, G. L., Mannheimer, C., Kubirske,
R., Smith, G. F., Koekemoer, M., Dlamini, G. M., Phiri,
P. S. M., Nobanda, N. &amp;amp; Willis, C. K. (eds.). 1998.
Inventory, Evaluation and Monitoring of Botanical
Diversity in Southern Africa: A Regional Capacity and
Institution Building Network (SABONET). SABONET
Report Series No. 4. SABONET, Pretoria.<person-group>
                  <string-name>
                     <surname>Huntley</surname>
                  </string-name>
               </person-group>
               <source>Inventory, Evaluation and Monitoring of Botanical Diversity in Southern Africa: A Regional Capacity and Institution Building Network (SABONET)</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d939e408a1310">
            <mixed-citation id="d939e412" publication-type="book">
Huntley, B. J., Willis, C. K., Smith, G. F. &amp;amp; Siebert, S. J.
2002. The history and success of SABONET in southern
Africa. Pp. 231-246 in: Baijnath, H. &amp;amp; Singh, Y. (eds.),
Rebirth of Science in Africa: A Shared Vision for Life and
Environmental Sciences. Umdaus Press, Hatfield.<person-group>
                  <string-name>
                     <surname>Huntley</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The history and success of SABONET in southern Africa</comment>
               <fpage>231</fpage>
               <source>Rebirth of Science in Africa: A Shared Vision for Life and Environmental Sciences</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d939e453a1310">
            <mixed-citation id="d939e457" publication-type="journal">
Klopper, R. R., Smith, G. F. &amp;amp; Chikuni, A. C. 2001. The
Global Taxonomy Initiative: Documenting the biodiversi-
ty of Africa. Strelitzia 12: 1-202.<person-group>
                  <string-name>
                     <surname>Klopper</surname>
                  </string-name>
               </person-group>
               <fpage>1</fpage>
               <volume>12</volume>
               <source>Strelitzia</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d939e492a1310">
            <mixed-citation id="d939e496" publication-type="book">
Klopper, R. R., Smith, G. F. &amp;amp; Van Rooy, J. 2002. The bio-
diversity of Africa. Pp. 60-86 in: Baijnath, H. &amp;amp; Singh, Y.
(eds.), Rebirth of Science in Africa: A Shared Vision for
Life and Environmental Sciences. Umdaus Press, Hatfield.<person-group>
                  <string-name>
                     <surname>Klopper</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The biodiversity of Africa</comment>
               <fpage>60</fpage>
               <source>Rebirth of Science in Africa: A Shared Vision for Life and Environmental Sciences</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d939e535a1310">
            <mixed-citation id="d939e539" publication-type="journal">
Siebert S. J., Bandeira S. O., Burrows, J. E. &amp;amp; Winter, P. J.
D. 2002. SABONET Southern Mozambique Expedition
2001. SABONET News 7: 6-18.<person-group>
                  <string-name>
                     <surname>Siebert</surname>
                  </string-name>
               </person-group>
               <fpage>6</fpage>
               <volume>7</volume>
               <source>SABONET News</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d939e574a1310">
            <mixed-citation id="d939e578" publication-type="journal">
Siebert, S. J., Mössmer, M., Rukazhanga-Noko, N. &amp;amp;
Haasbroek, C. 2001. Has SABONET developed the
regional botanical expertise it promised? SABONET News
6: 74-83.<person-group>
                  <string-name>
                     <surname>Siebert</surname>
                  </string-name>
               </person-group>
               <fpage>74</fpage>
               <volume>6</volume>
               <source>SABONET News</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d939e616a1310">
            <mixed-citation id="d939e620" publication-type="book">
Smith, G. F. &amp;amp; Willis, C. K. (eds). 1999. Index Herbariorum:
Southern African Supplement, ed. 2. SABONET Report
Series No. 8. SABONET, Pretoria.<person-group>
                  <string-name>
                     <surname>Smith</surname>
                  </string-name>
               </person-group>
               <edition>2</edition>
               <source>Index Herbariorum: Southern African Supplement</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d939e652a1310">
            <mixed-citation id="d939e656" publication-type="book">
Steenkamp, Y. &amp;amp; Smith, G. F. 2002. Addressing the Needs of
the Users of Botanical Information. SABONET Report
Series No. 15. SABONET, Pretoria.<person-group>
                  <string-name>
                     <surname>Steenkamp</surname>
                  </string-name>
               </person-group>
               <source>Addressing the Needs of the Users of Botanical Information</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d939e685a1310">
            <mixed-citation id="d939e689" publication-type="book">
The World Bank. 2002. The World Bank and Agenda 21. The
World Bank, Washington D.C.<person-group>
                  <string-name>
                     <surname>The World Bank</surname>
                  </string-name>
               </person-group>
               <source>The World Bank and Agenda 21</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d939e714a1310">
            <mixed-citation id="d939e718" publication-type="journal">
Timberlake, J. &amp;amp; Paton, A. 2001. SABONET Midterm
Review Report Back. SABONET News 6: 5-13.<person-group>
                  <string-name>
                     <surname>Timberlake</surname>
                  </string-name>
               </person-group>
               <fpage>5</fpage>
               <volume>6</volume>
               <source>SABONET News</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d939e751a1310">
            <mixed-citation id="d939e755" publication-type="journal">
Willis, C. K., Burrows, J. E. &amp;amp; Winter, P. J. D. 2000. SABO-
NET Nyika Expedition 2000. SABONET News 5: 5-14.<person-group>
                  <string-name>
                     <surname>Willis</surname>
                  </string-name>
               </person-group>
               <fpage>5</fpage>
               <volume>5</volume>
               <source>SABONET News</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d939e787a1310">
            <mixed-citation id="d939e791" publication-type="journal">
Willis, C. K. &amp;amp; Huntley, B. J. 2001. SABONET: Developing
capacity within southern Africa&amp;apos;s herbaria and botanical
gardens. Systematics and Geography of Plants 71:
247-258.<object-id pub-id-type="doi">10.2307/3668671</object-id>
               <fpage>247</fpage>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
