<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">jmammalogy</journal-id>
         <journal-id journal-id-type="jstor">j100007</journal-id>
         <journal-title-group>
            <journal-title>Journal of Mammalogy</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>American Society of Mammalogists</publisher-name>
         </publisher>
         <issn pub-type="ppub">00222372</issn>
         <issn pub-type="epub">15451542</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">1383871</article-id>
         <title-group>
            <article-title>Influence of Relatedness on Snowshoe Hare Spacing Behavior</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Cole</given-names>
                  <surname>Burton</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Charles J.</given-names>
                  <surname>Krebs</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>8</month>
            <year>2003</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">84</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id>i201338</issue-id>
         <fpage>1100</fpage>
         <lpage>1111</lpage>
         <page-range>1100-1111</page-range>
         <permissions>
            <copyright-statement>Copyright 2003 The American Society of Mammalogists</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/1383871"/>
         <abstract>
            <p> Predation and food are dominant forces regulating snowshoe hare (Lepus americanus) population density, yet manipulations of these factors have not proven sufficient to explain all aspects of hare population dynamics. Social interactions among hares have often been dismissed as unimportant in population regulation, but the mechanisms and consequences of such interactions have not been well studied. In this study, we examined one aspect of social behavior in snowshoe hares that has been hypothesized to be important in the spacing behavior of other species of small mammals: interactions among related individuals. We sampled 68 hares on two 7.3-ha grids in the southwest Yukon Territory during a cyclic peak phase of population density and used livetrapping and radiotelemetry to quantify spacing behavior. Hares were genotyped at 7 microsatellite DNA loci, and relatedness (r) among individuals was estimated and correlated with spacing. Average relatedness was low on both grids (≤0) because very few close kin were present. Hares were not more or less likely to associate with kin than they were with nonkin. The results were similar when males, females, adults, and juveniles were considered together or separately and are thus consistent with a lack of sex-biased dispersal in snowshoe hares. Kin are not clustered in snowshoe hare populations, thus interactions among kin do not likely have a strong influence on hare spacing behavior. This study supports the idea that spacing behavior has little influence on hare population dynamics, at least during the peak phase of the cycle. </p>
         </abstract>
         <kwd-group>
            <kwd>Home range</kwd>
            <kwd>Kin structure</kwd>
            <kwd>Lepus americanus</kwd>
            <kwd>Microsatellite DNA</kwd>
            <kwd>Radiotelemetry</kwd>
            <kwd>Relatedness</kwd>
            <kwd>Snowshoe hare</kwd>
            <kwd>Spacing behavior</kwd>
            <kwd>Yukon</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Literature cited</title>
         <ref id="d2259e179a1310">
            <mixed-citation id="d2259e183" publication-type="journal">
BALLOUX, F., J. GOUDET, AND N. PERRIN. 1998. Breed-
ing system and genetic variance in the monogamous,
semi-social shrew, Crocidura russula. Evolution 52:
1230-1235.<object-id pub-id-type="doi">10.2307/2411254</object-id>
               <fpage>1230</fpage>
            </mixed-citation>
         </ref>
         <ref id="d2259e206a1310">
            <mixed-citation id="d2259e210" publication-type="journal">
BLOUIN, M. S., M. PARSONS, V. LACAILLE, AND S. LOTz.
1996. Use of microsatellite loci to classify individ-
uals by relatedness. Molecular Ecology 5:393-401.<person-group>
                  <string-name>
                     <surname>Blouin</surname>
                  </string-name>
               </person-group>
               <fpage>393</fpage>
               <volume>5</volume>
               <source>Molecular Ecology</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e245a1310">
            <mixed-citation id="d2259e249" publication-type="journal">
BOONSTRA, R., D. HIK, G. R. SINGLETON, AND A. TIN-
NIKOV. 1998a. The impact of predator-induced stress
on the snowshoe hare cycle. Ecological Monographs
68:371-394.<object-id pub-id-type="doi">10.2307/2657244</object-id>
               <fpage>371</fpage>
            </mixed-citation>
         </ref>
         <ref id="d2259e272a1310">
            <mixed-citation id="d2259e276" publication-type="journal">
BOONSTRA, R., C. J. KREBS, AND N. C. STENSETH.
1998b. Population cycles in small mammals: the
problem of explaining the low phase. Ecology 79:
1479-1488.<object-id pub-id-type="doi">10.2307/176770</object-id>
               <fpage>1479</fpage>
            </mixed-citation>
         </ref>
         <ref id="d2259e300a1310">
            <mixed-citation id="d2259e304" publication-type="book">
BOUTIN, S. 1979. Spacing behavior of snowshoe hares
in relation to their population dynamics. M.S. thesis,
University of British Columbia, Vancouver, British
Columbia, Canada.<person-group>
                  <string-name>
                     <surname>Boutin</surname>
                  </string-name>
               </person-group>
               <source>Spacing behavior of snowshoe hares in relation to their population dynamics</source>
               <year>1979</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e336a1310">
            <mixed-citation id="d2259e340" publication-type="journal">
BOUTIN, S. 1980. Effect of spring removal experiments
on the spacing behavior of female snowshoe hares.
Canadian Journal of Zoology 58:2167-2174.<person-group>
                  <string-name>
                     <surname>Boutin</surname>
                  </string-name>
               </person-group>
               <fpage>2167</fpage>
               <volume>58</volume>
               <source>Canadian Journal of Zoology</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e375a1310">
            <mixed-citation id="d2259e379" publication-type="journal">
BOUTIN, S. A. 1984. The effect of conspecifics on ju-
venile survival and recruitment of snowshoe hares.
Journal of Animal Ecology 53:623-637.<object-id pub-id-type="doi">10.2307/4540</object-id>
               <fpage>623</fpage>
            </mixed-citation>
         </ref>
         <ref id="d2259e398a1310">
            <mixed-citation id="d2259e402" publication-type="journal">
BURTON, C. 2002. Microsatellite analysis of multiple
paternity and male reproductive success in the pro-
miscuous snowshoe hare. Canadian Journal of Zo-
ology 80:1948-1956.<person-group>
                  <string-name>
                     <surname>Burton</surname>
                  </string-name>
               </person-group>
               <fpage>1948</fpage>
               <volume>80</volume>
               <source>Canadian Journal of Zoology</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e440a1310">
            <mixed-citation id="d2259e444" publication-type="journal">
BURTON, C., C. J. KREBS, AND E. B. TAYLOR. 2002.
Population genetic structure of the cyclic snowshoe
hare (Lepus americanus) in southwestern Yukon,
Canada. Molecular Ecology 11:1689-1701.<person-group>
                  <string-name>
                     <surname>Burton</surname>
                  </string-name>
               </person-group>
               <fpage>1689</fpage>
               <volume>11</volume>
               <source>Molecular Ecology</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e482a1310">
            <mixed-citation id="d2259e486" publication-type="journal">
CHARNOV, E. L., AND J. P. FINERTY. 1980. Vole popu-
lation cycles: a case for kin-selection. Oecologia 45:
1-2.<person-group>
                  <string-name>
                     <surname>Charnov</surname>
                  </string-name>
               </person-group>
               <fpage>1</fpage>
               <volume>45</volume>
               <source>Oecologia</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e522a1310">
            <mixed-citation id="d2259e526" publication-type="journal">
DOUGLAS, G. W. 1974. Montane zone vegetation of the
Alsek River region, south-western Yukon. Canadian
Journal of Botany 52:2505-2532.<person-group>
                  <string-name>
                     <surname>Douglas</surname>
                  </string-name>
               </person-group>
               <fpage>2505</fpage>
               <volume>52</volume>
               <source>Canadian Journal of Botany</source>
               <year>1974</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e561a1310">
            <mixed-citation id="d2259e565" publication-type="journal">
FERRIERE, R., J. R. BELTHOFF, I. OLIVIERI, AND S. KRAC-
KOW. 2000. Evolving dispersal: where to go next?
Trends in Ecology and Evolution 15:5-7.<person-group>
                  <string-name>
                     <surname>Ferriere</surname>
                  </string-name>
               </person-group>
               <fpage>5</fpage>
               <volume>15</volume>
               <source>Trends in Ecology and Evolution</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e600a1310">
            <mixed-citation id="d2259e604" publication-type="journal">
FERRON, J. 1993. How do population density and food
supply influence social behaviour in the snowshoe
hare (Lepus americanus)? Canadian Journal of Zo-
ology 71:1084-1089.<person-group>
                  <string-name>
                     <surname>Ferron</surname>
                  </string-name>
               </person-group>
               <fpage>1084</fpage>
               <volume>71</volume>
               <source>Canadian Journal of Zoology</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e642a1310">
            <mixed-citation id="d2259e646" publication-type="journal">
GILLIS, E. A. 1998. Survival of juvenile snowshoe
hares during a cyclic population increase. Canadian
Journal of Zoology 76:1949-1956.<person-group>
                  <string-name>
                     <surname>Gillis</surname>
                  </string-name>
               </person-group>
               <fpage>1949</fpage>
               <volume>76</volume>
               <source>Canadian Journal of Zoology</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e681a1310">
            <mixed-citation id="d2259e687" publication-type="journal">
GILLIS, E. A., AND C. J. KREBS. 1999. Natal dispersal
of snowshoe hares during a cyclic population in-
crease. Journal of Mammalogy 80:933-939.<object-id pub-id-type="doi">10.2307/1383263</object-id>
               <fpage>933</fpage>
            </mixed-citation>
         </ref>
         <ref id="d2259e706a1310">
            <mixed-citation id="d2259e710" publication-type="journal">
GOODNIGHT, K. F., AND D. C. QUELLER. 1999. Com-
puter software for performing likelihood tests of
pedigree relationship using genetic markers. Molec-
ular Ecology 8:1231-1234.<person-group>
                  <string-name>
                     <surname>Goodnight</surname>
                  </string-name>
               </person-group>
               <fpage>1231</fpage>
               <volume>8</volume>
               <source>Molecular Ecology</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e749a1310">
            <mixed-citation id="d2259e753" publication-type="journal">
GRAF, R. P 1985. Social organization of snowshoe
hares. Canadian Journal of Zoology 63:468-474.<person-group>
                  <string-name>
                     <surname>Graf</surname>
                  </string-name>
               </person-group>
               <fpage>468</fpage>
               <volume>63</volume>
               <source>Canadian Journal of Zoology</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e785a1310">
            <mixed-citation id="d2259e789" publication-type="journal">
GRAF, R. P., AND A. R. E. SINCLAIR. 1987. Parental care
and adult aggression toward juvenile snowshoe
hares. Arctic 40:175-178.<person-group>
                  <string-name>
                     <surname>Graf</surname>
                  </string-name>
               </person-group>
               <fpage>175</fpage>
               <volume>40</volume>
               <source>Arctic</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e824a1310">
            <mixed-citation id="d2259e828" publication-type="journal">
GREENWOOD, P. J. 1980. Mating systems, philopatry
and dispersal in birds and mammals. Animal Behav-
iour 28:1140-1162.<person-group>
                  <string-name>
                     <surname>Greenwood</surname>
                  </string-name>
               </person-group>
               <fpage>1140</fpage>
               <volume>28</volume>
               <source>Animal Behaviour</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e863a1310">
            <mixed-citation id="d2259e867" publication-type="book">
HIK, D. S. 1994. Predation risk and the 10-year snow-
shoe hare cycle. Ph.D. dissertation, University of
British Columbia, Vancouver, British Columbia,
Canada.<person-group>
                  <string-name>
                     <surname>Hik</surname>
                  </string-name>
               </person-group>
               <source>Predation risk and the 10-year snowshoe hare cycle</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e899a1310">
            <mixed-citation id="d2259e903" publication-type="journal">
HIK, D. S. 1995. Does risk of predation influence pop-
ulation dynamics? Evidence from the cyclic decline
of snowshoe hares. Wildlife Research 22:115-129.<person-group>
                  <string-name>
                     <surname>Hik</surname>
                  </string-name>
               </person-group>
               <fpage>115</fpage>
               <volume>22</volume>
               <source>Wildlife Research</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e938a1310">
            <mixed-citation id="d2259e942" publication-type="book">
HODGES, K. E. 1998. Snowshoe hare demography and
behaviour during a cyclic population low phase.
Ph.D. dissertation, University of British Columbia,
Vancouver, British Columbia, Canada.<person-group>
                  <string-name>
                     <surname>Hodges</surname>
                  </string-name>
               </person-group>
               <source>Snowshoe hare demography and behaviour during a cyclic population low phase</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e975a1310">
            <mixed-citation id="d2259e979" publication-type="journal">
HODGES, K. E. 1999. Proximate factors affecting snow-
shoe hare movements during a cyclic population low
phase. Ecoscience 6:487-496.<person-group>
                  <string-name>
                     <surname>Hodges</surname>
                  </string-name>
               </person-group>
               <fpage>487</fpage>
               <volume>6</volume>
               <source>Ecoscience</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e1014a1310">
            <mixed-citation id="d2259e1018" publication-type="book">
HODGES, K. E. 2000. The ecology of snowshoe hares
in northern boreal forests. Pp. 117-161 in Ecology
and conservation of lynx in the United States (L. F.
Ruggiero et al., eds.). University Press of Colorado,
Boulder.<person-group>
                  <string-name>
                     <surname>Hodges</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The ecology of snowshoe hares in northern boreal forests</comment>
               <fpage>117</fpage>
               <source>Ecology and conservation of lynx in the United States</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e1059a1310">
            <mixed-citation id="d2259e1063" publication-type="journal">
HODGES, K. E., C. J. KREBS, AND A. R. E. SINCLAIR.
1999. Snowshoe hare demography during a cyclic
population low. Journal of Animal Ecology 68:581-
594.<object-id pub-id-type="jstor">10.2307/2647186</object-id>
               <fpage>581</fpage>
            </mixed-citation>
         </ref>
         <ref id="d2259e1086a1310">
            <mixed-citation id="d2259e1090" publication-type="journal">
ISHIBASHI, Y., T. SAITOH, S. ABE, AND C. YOSHIDA.
1997. Sex-related spatial kin structure in a spring
population of grey-sided voles Clethrionomys rufo-
canus as revealed by mitochondrial and microsatel-
lite DNA analyses. Molecular Ecology 6:63-71.<person-group>
                  <string-name>
                     <surname>Ishibashi</surname>
                  </string-name>
               </person-group>
               <fpage>63</fpage>
               <volume>6</volume>
               <source>Molecular Ecology</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e1131a1310">
            <mixed-citation id="d2259e1135" publication-type="journal">
JACOBS, J. 1974. Quantitative measurements of food
selection. Oecologia 14:413-417.<person-group>
                  <string-name>
                     <surname>Jacobs</surname>
                  </string-name>
               </person-group>
               <fpage>413</fpage>
               <volume>14</volume>
               <source>Oecologia</source>
               <year>1974</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e1167a1310">
            <mixed-citation id="d2259e1171" publication-type="book">
KEITH, L. B. 1990. Dynamics of snowshoe hare pop-
ulations. Pp. 119-195 in Current mammalogy (H.
H. Genoways, ed.). Plenum Press, New York.<person-group>
                  <string-name>
                     <surname>Keith</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Dynamics of snowshoe hare populations</comment>
               <fpage>119</fpage>
               <source>Current mammalogy</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e1207a1310">
            <mixed-citation id="d2259e1211" publication-type="book">
KENWARD, R. E., AND K. H. HODDER. 1996. Ranges V.
An analysis system for biological location data. In-
stitute of Terrestrial Ecology, Wareham, United
Kingdom.<person-group>
                  <string-name>
                     <surname>Kenward</surname>
                  </string-name>
               </person-group>
               <source>Ranges V. An analysis system for biological location data</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e1243a1310">
            <mixed-citation id="d2259e1247" publication-type="journal">
KENWARD, R. E., V. MARCSTROM, AND M. KARLBOM.
1993. Post-nestling behaviour in goshawks, Accipi-
ter gentilis: II. Sex differences in sociality and nest
switching. Animal Behaviour 46:371-378.<person-group>
                  <string-name>
                     <surname>Kenward</surname>
                  </string-name>
               </person-group>
               <fpage>371</fpage>
               <volume>46</volume>
               <source>Animal Behaviour</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e1285a1310">
            <mixed-citation id="d2259e1289" publication-type="journal">
KNIGHT, M. E., M. J. H. VAN OPPEN, H. L. SMITH, C.
RICO, G. M. HEWITT, AND G. F. TURNER. 1999. Evi-
dence for male-biased dispersal in Lake Malawi
cichlids from microsatellites. Molecular Ecology 8:
1521-1527.<person-group>
                  <string-name>
                     <surname>Knight</surname>
                  </string-name>
               </person-group>
               <fpage>1521</fpage>
               <volume>8</volume>
               <source>Molecular Ecology</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e1330a1310">
            <mixed-citation id="d2259e1334" publication-type="journal">
KREBS, C. J. 1996. Population cycles revisited. Journal
of Mammalogy 77:8-24.<object-id pub-id-type="doi">10.2307/1382705</object-id>
               <fpage>8</fpage>
            </mixed-citation>
         </ref>
         <ref id="d2259e1350a1310">
            <mixed-citation id="d2259e1354" publication-type="journal">
KREBS, C. J., ET AL. 1995. Impact of food and predation
on the snowshoe hare cycle. Science 269:1112-
1115.<object-id pub-id-type="jstor">10.2307/2888056</object-id>
               <fpage>1112</fpage>
            </mixed-citation>
         </ref>
         <ref id="d2259e1373a1310">
            <mixed-citation id="d2259e1377" publication-type="journal">
LAMBIN, X., AND C. J. KREBS. 1991. Can changes in
female relatedness influence microtine population
dynamics? Oikos 61:126-132.<person-group>
                  <string-name>
                     <surname>Lambin</surname>
                  </string-name>
               </person-group>
               <fpage>126</fpage>
               <volume>61</volume>
               <source>Oikos</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e1413a1310">
            <mixed-citation id="d2259e1417" publication-type="journal">
LAMBIN, X., AND C. J. KREBS. 1993. Influence of fe-
male relatedness on the demography of Townsend's
vole populations in spring. Journal of Animal Ecol-
ogy 62:536-550.<object-id pub-id-type="doi">10.2307/5203</object-id>
               <fpage>536</fpage>
            </mixed-citation>
         </ref>
         <ref id="d2259e1440a1310">
            <mixed-citation id="d2259e1444" publication-type="book">
MACDONALD, D. W., F. G. BALL, AND N. G. HOUGH.
1980. The evaluation of home range size and con-
figuration using radio tracking data. Pp. 405-424 in
A handbook on biotelemetry and radio tracking (C.
J. Amlaner and D. W. Macdonald, eds.). Pergamon
Press, Oxford, United Kingdom.<person-group>
                  <string-name>
                     <surname>Macdonald</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The evaluation of home range size and configuration using radio tracking data</comment>
               <fpage>405</fpage>
               <source>A handbook on biotelemetry and radio tracking</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e1488a1310">
            <mixed-citation id="d2259e1492" publication-type="book">
MANLY, B. F. J. 1997a. Randomization, bootstrap and
Monte Carlo methods in biology. Chapman &amp; Hall,
New York.<person-group>
                  <string-name>
                     <surname>Manly</surname>
                  </string-name>
               </person-group>
               <source>Randomization, bootstrap and Monte Carlo methods in biology</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e1521a1310">
            <mixed-citation id="d2259e1525" publication-type="book">
MANLY, B. F. J. 1997b. RT: a program for randomi-
zation testing. Version 2.1. Western Ecosystems
Technology, Inc., Cheyenne, Wyoming.<person-group>
                  <string-name>
                     <surname>Manly</surname>
                  </string-name>
               </person-group>
               <source>RT: a program for randomization testing</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e1554a1310">
            <mixed-citation id="d2259e1558" publication-type="journal">
MANTEL, N. 1967. The detection of disease clustering
and a generalized regression approach. Cancer Re-
search 27:209-220.<person-group>
                  <string-name>
                     <surname>Mantel</surname>
                  </string-name>
               </person-group>
               <fpage>209</fpage>
               <volume>27</volume>
               <source>Cancer Research</source>
               <year>1967</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e1593a1310">
            <mixed-citation id="d2259e1597" publication-type="journal">
MATTHIOPOULOS, J., R. MOSS, AND X. LAMBIN. 1998.
Models of red grouse cycles. A family affair? Oikos
82:574-590.<person-group>
                  <string-name>
                     <surname>Matthiopoulos</surname>
                  </string-name>
               </person-group>
               <fpage>574</fpage>
               <volume>82</volume>
               <source>Oikos</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e1633a1310">
            <mixed-citation id="d2259e1637" publication-type="journal">
MOUGEL, F., J. C. MOUNOLOU, AND M. MONNEROT.
1997. Nine polymorphic microsatellite loci in the
rabbit, Oryctolagus cuniculus. Animal Genetics 28:
58-59.<person-group>
                  <string-name>
                     <surname>Mougel</surname>
                  </string-name>
               </person-group>
               <fpage>58</fpage>
               <volume>28</volume>
               <source>Animal Genetics</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e1675a1310">
            <mixed-citation id="d2259e1679" publication-type="journal">
O'DONOGHUE, M. 1994. Early survival of juvenile
snowshoe hares. Ecology 75:1582-1592.<object-id pub-id-type="doi">10.2307/1939619</object-id>
               <fpage>1582</fpage>
            </mixed-citation>
         </ref>
         <ref id="d2259e1695a1310">
            <mixed-citation id="d2259e1699" publication-type="journal">
O'DONOGHUE, M., AND C. M. BERGMAN. 1992. Early
movements and dispersal of juvenile snowshoe
hares. Canadian Journal of Zoology 70:1787-1791.<person-group>
                  <string-name>
                     <surname>O'Donoghue</surname>
                  </string-name>
               </person-group>
               <fpage>1787</fpage>
               <volume>70</volume>
               <source>Canadian Journal of Zoology</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e1734a1310">
            <mixed-citation id="d2259e1738" publication-type="journal">
QUELLER, D. C., AND K. F. GOODNIGHT. 1989. Estimat-
ing relatedness using genetic markers. Evolution 43:
258-275.<object-id pub-id-type="doi">10.2307/2409206</object-id>
               <fpage>258</fpage>
            </mixed-citation>
         </ref>
         <ref id="d2259e1757a1310">
            <mixed-citation id="d2259e1761" publication-type="journal">
QUENETTE, P. Y., J. FERRON, AND L. SIROIS. 1997. Group
foraging in snowshoe hares (Lepus americanus): ag-
gregation or social group? Behavioural Processes
41:29-37.<person-group>
                  <string-name>
                     <surname>Quenette</surname>
                  </string-name>
               </person-group>
               <fpage>29</fpage>
               <volume>41</volume>
               <source>Behavioural Processes</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e1799a1310">
            <mixed-citation id="d2259e1803" publication-type="journal">
RAYMOND, M., AND F. ROUSSET. 1995. GENEPOP (ver-
sion 1.2): population genetics software for exact
tests and ecumenicism. Journal of Heredity 86:248-
249.<person-group>
                  <string-name>
                     <surname>Raymond</surname>
                  </string-name>
               </person-group>
               <fpage>248</fpage>
               <volume>86</volume>
               <source>Journal of Heredity</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e1842a1310">
            <mixed-citation id="d2259e1846" publication-type="journal">
RICE, W. R. 1989. Analyzing tables of statistical tests.
Evolution 43:223-225.<object-id pub-id-type="doi">10.2307/2409177</object-id>
               <fpage>223</fpage>
            </mixed-citation>
         </ref>
         <ref id="d2259e1862a1310">
            <mixed-citation id="d2259e1866" publication-type="journal">
RICO, C., I. RICO, N. J. WEBB, S. SMITH, D. J. BELL,
AND G. M. HEWITT. 1994. Four polymorphic micro-
satellite loci for the European wild rabbit, Orycto-
lagus cuniculus. Animal Genetics 25:367.<person-group>
                  <string-name>
                     <surname>Rico</surname>
                  </string-name>
               </person-group>
               <fpage>367</fpage>
               <volume>25</volume>
               <source>Animal Genetics</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e1904a1310">
            <mixed-citation id="d2259e1908" publication-type="journal">
SHERMAN, P. W. 1981. Kinship, demography, and Beld-
ing's ground squirrel nepotism. Behavioral Ecology
and Sociobiology 8:251-259.<person-group>
                  <string-name>
                     <surname>Sherman</surname>
                  </string-name>
               </person-group>
               <fpage>251</fpage>
               <volume>8</volume>
               <source>Behavioral Ecology and Sociobiology</source>
               <year>1981</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e1943a1310">
            <mixed-citation id="d2259e1947" publication-type="journal">
SINCLAIR, A. R. E. 1986. Testing multi-factor causes of
population limitation: an illustration using snowshoe
hares. Oikos 47:360-364.<person-group>
                  <string-name>
                     <surname>Sinclair</surname>
                  </string-name>
               </person-group>
               <fpage>360</fpage>
               <volume>47</volume>
               <source>Oikos</source>
               <year>1986</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e1982a1310">
            <mixed-citation id="d2259e1986" publication-type="book">
SOKAL, R. R., AND F. J. ROHLF. 1995. Biometry: the
principles and practice of statistics in biological re-
search. W. H. Freeman and Company, New York.<person-group>
                  <string-name>
                     <surname>Sokal</surname>
                  </string-name>
               </person-group>
               <source>Biometry: the principles and practice of statistics in biological research</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e2015a1310">
            <mixed-citation id="d2259e2019" publication-type="journal">
SURRIDGE, A. K., D. J. BELL, C. RICO, AND G. M. HEW-
ITT. 1997. Polymorphic microsatellite loci in the Eu-
ropean rabbit (Oryctolagus cuniculus) are also am-
plified in other lagomorph species. Animal Genetics
28:302-305.<person-group>
                  <string-name>
                     <surname>Surridge</surname>
                  </string-name>
               </person-group>
               <fpage>302</fpage>
               <volume>28</volume>
               <source>Animal Genetics</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e2061a1310">
            <mixed-citation id="d2259e2065" publication-type="journal">
SURRIDGE, A. K., K. M. IBRAHIM, D. J. BELL, N. J.
WEBB, C. RICO, AND G. M. HEWITT. 1999. Fine-scale
genetic structuring in a natural population of Euro-
pean wild rabbits (Oryctolagus cuniculus). Molecu-
lar Ecology 8:299-307.<person-group>
                  <string-name>
                     <surname>Surridge</surname>
                  </string-name>
               </person-group>
               <fpage>299</fpage>
               <volume>8</volume>
               <source>Molecular Ecology</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e2106a1310">
            <mixed-citation id="d2259e2110" publication-type="journal">
VAN STAADEN, M. J., G. R. MICHENER, AND R. K. CHES-
SER. 1996. Spatial analysis of microgeographic ge-
netic structure in Richardson's ground squirrels. Ca-
nadian Journal of Zoology 74:1187-1195.<person-group>
                  <string-name>
                     <surname>Van Staaden</surname>
                  </string-name>
               </person-group>
               <fpage>1187</fpage>
               <volume>74</volume>
               <source>Canadian Journal of Zoology</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d2259e2148a1310">
            <mixed-citation id="d2259e2152" publication-type="journal">
WORTON, B. J. 1989. Kernel methods for estimating
the utilization distribution in home-range studies.
Ecology 70:164-168.<object-id pub-id-type="doi">10.2307/1938423</object-id>
               <fpage>164</fpage>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
