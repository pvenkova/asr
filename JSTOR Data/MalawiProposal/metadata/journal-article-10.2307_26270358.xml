<?xml version="1.0" encoding="UTF-8"?>
<article article-type="research-article" dtd-version="1.0" xml:lang="eng">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="publisher-id">ecologysociety</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50015787</journal-id>
         <journal-title-group>
            <journal-title>Ecology and Society</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Resilience Alliance</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">17083087</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">26270358</article-id>
         <article-categories>
            <subj-group>
               <subject>Insight</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Africa’s game changers and the catalysts of social and system innovation</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Swilling</surname>
                  <given-names>Mark</given-names>
               </string-name>
               <xref ref-type="aff" rid="af1">¹</xref>
               <xref ref-type="aff" rid="af2">²</xref>
            </contrib>
            <aff id="af1">
               <label>¹</label>Stellenbosch Centre for Complex Systems in Transition,</aff>
            <aff id="af2">
               <label>²</label>Stellenbosch University</aff>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>3</month>
            <year>2016</year>
            <string-date>Mar 2016</string-date>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink">21</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">e26270314</issue-id>
         <permissions>
            <copyright-statement>Copyright © 2016 by the author(s)</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/26270358"/>
         <abstract>
            <label>ABSTRACT.</label>
            <p>It is widely recognized that many African economies are being transformed by rapid economic growth driven largely by rising demand for the abundant natural resources scattered across the African continent. I critically review the mainstream game-changing dynamics driving this process, with special reference to a set of influential policy-oriented documents. This is followed by an analysis of less-recognized game-changing dynamics that have, in turn, been affected by the mainstream game-changing dynamics. These less-recognized game-changing dynamics include energy infrastructure challenges in a context of climate change, securing access to water, access to arable soils, slum urbanism, and food security responses. These mainstream and less-recognized game-changing dynamics provide the context for analyzing a range of African actor networks engaged in social and system innovations. I use a transdisciplinary framework to discuss these actor networks and how they construct their understanding of the game changers affecting their programs and actions. Based on a case study of the iShack initiative in Stellenbosch, South Africa, I conclude that social and system innovations will need to be driven by transformation knowledge co-produced by researchers and social actors who can actively link game-changing dynamics that operate at multiple scales with local-level innovations with potential societal impacts.</p>
         </abstract>
         <kwd-group>
            <label>Key Words:</label>
            <kwd>African development</kwd>
            <kwd>social innovation</kwd>
            <kwd>structural transformation</kwd>
            <kwd>transdisciplinary research</kwd>
         </kwd-group>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="unparsed-citations">
         <title>LITERATURE CITED</title>
         <ref id="ref1">
            <mixed-citation publication-type="other">Africa Progress Panel. 2015. Power people planet: seizing Africa’s energy and climate opportunities. Africa Progress Panel, Geneva, Switzerland. [online] URL: http://www.africaprogresspanel.org/wp-content/uploads/2015/06/APP_REPORT_2015_FINAL_low1.pdf</mixed-citation>
         </ref>
         <ref id="ref2">
            <mixed-citation publication-type="other">African Center for Economic Transformation (ACET). 2014. 2014 African transformation report: growth with depth. African Center for Economic Transformation, Accra, Ghana. [online] URL: http://acetforafrica.org/wp-content/uploads/2014/03/2014- African-Transformation-Report.pdf</mixed-citation>
         </ref>
         <ref id="ref3">
            <mixed-citation publication-type="other">African Union. 2015. Africa renewable energy initiative. African Union, Addis Ababa, Ethiopia.</mixed-citation>
         </ref>
         <ref id="ref4">
            <mixed-citation publication-type="other">Anderson, T., A. Curtis, and C. Wittig. 2014. Definition and theory in social innovation. Thesis. Danube University, Krems, Austria. [online] URL: http://www.social-innovation-blog.com/wp-content/uploads/2014/05/Definitions-and-Theory-in-Social-Innovation-Final-1.pdf</mixed-citation>
         </ref>
         <ref id="ref5">
            <mixed-citation publication-type="other">Auerbach, R., G. Kundren, and N. E. Scialabba, editors. 2013. Organic agriculture: African experiences in resilience and sustainability. Food and Agriculture Organization, Rome, Italy. [online] URL: http://www.fao.org/docrep/018/i3294e/i3294e.pdf</mixed-citation>
         </ref>
         <ref id="ref6">
            <mixed-citation publication-type="other">Avelino, F., J. Wittmayer, A. Haxeltine, R. Kemp, T. O’Riordan, P. Weaver, D. Loorbach, and J. Rotmans. 2014. Game-changers and transformative social innovation: the case of the economic crisis and the new economy. TRANSIT working paper. EU SSH.2013.3.2-1, grant agreement 613169. DRIFT, Rotterdam, The Netherlands. [online] URL: http://www.transitsocialinnovation. eu/content/original/TRANSIT%20outputs/91%20Gamechangers_TSI_Avelino_etal_TRANSIT_workingpaper_2014.pdf</mixed-citation>
         </ref>
         <ref id="ref7">
            <mixed-citation publication-type="other">Battersby, J., and J. Crush. 2014. Africa’s urban food deserts. Urban Forum 25(2):143-151.</mixed-citation>
         </ref>
         <ref id="ref8">
            <mixed-citation publication-type="other">Bolton, P., F. Samana, and J. E. Stiglitz, editors. 2012. Sovereign wealth funds and long-term investing. Columbia University Press, New York, New York, USA.</mixed-citation>
         </ref>
         <ref id="ref9">
            <mixed-citation publication-type="other">Borlaug, N. E. 2000. Ending world hunger. The promise of biotechnology and the threat of antiscience zealotry. Plant Physiology 124(2):487-490. http://dx.doi.org/10.1104/pp.124.2.487</mixed-citation>
         </ref>
         <ref id="ref10">
            <mixed-citation publication-type="other">Branch, A., and Z. Mampilly. 2015. African uprising: popular protest and political change. Zed Books, London, UK.</mixed-citation>
         </ref>
         <ref id="ref11">
            <mixed-citation publication-type="other">Bringezu, S., H. Schütz, W. Pengue, M. O’Brien, F. Garcia, R. Sims, R. W. Howarth, L. Kauppi, M. Swilling, and J. Herrick. 2014. Assessing global land use: balancing consumption with sustainable supply. United Nations Environment Programme, Nairobi, Kenya. [online] URL: http://www.unep.org/resourcepanel/Portals/50244/publications/Full_Report-Assessing_Global_Land_UseEnglish_%28PDF%29.pdf</mixed-citation>
         </ref>
         <ref id="ref12">
            <mixed-citation publication-type="other">Buckley, R., and A. Kallergis. 2014. Does Africa urban policy provide a platform for sustained economic growth? Pages 173-190 in S. Parnell and S. Oldfield, editors. Routledge handbook on cities of the global south. Routledge, New York, New York, USA.</mixed-citation>
         </ref>
         <ref id="ref13">
            <mixed-citation publication-type="other">Casale, M., S. Drimie, T. Quinlan, and G. Ziervogel. 2010. Understanding vulnerability in southern Africa: comparative findings using a multiple-stressor approach in South Africa and Malawi. Regional Environmental Change 10(2):157-168. http://dx.doi.org/10.1007/s10113-009-0103-y</mixed-citation>
         </ref>
         <ref id="ref14">
            <mixed-citation publication-type="other">Chilisa, B. 2012. Indigenous research methodologies. Sage, Thousand Oaks, California, USA.</mixed-citation>
         </ref>
         <ref id="ref15">
            <mixed-citation publication-type="other">Collins, R. O. 1990. The waters of the Nile: hydropolitics and the Jonglei Canal. Clarendon Press, Oxford, UK.</mixed-citation>
         </ref>
         <ref id="ref16">
            <mixed-citation publication-type="other">Cotula, L., S. Vermeulen, R. Leonard, and J. Keeley. 2009. Land grab or development opportunity? Agricultural investment and international land deals in Africa. International Instituted for Environment and Development, Food and Agriculture Organization, and International Fund for Agricultural Development, London, UK, and Rome, Italy. [online] URL: http://www.ifad.org/pub/land/land_grab.pdf</mixed-citation>
         </ref>
         <ref id="ref17">
            <mixed-citation publication-type="other">Economic Commission for Latin America and the Caribbean. 2013. Natural resources: status and trends towards a regional development agenda in Latin America and the Caribbean. United Nations, Santiago, Chile. [online] URL: http://hdl.handle.net/11362/35892</mixed-citation>
         </ref>
         <ref id="ref18">
            <mixed-citation publication-type="other">Ernst &amp; Young. 2011. It’s time for Africa: Ernst &amp; Young’s 2011 Africa attractivness survey. Ernst &amp; Young, London, UK. [online] URL: http://www.ey.com/ZA/en/Issues/Business-environment/2011- Africa-attractiveness-survey</mixed-citation>
         </ref>
         <ref id="ref19">
            <mixed-citation publication-type="other">Evans, P. 2005. The challenges of the “institutional turn”: new interdisciplinary opportunities in development theory. Pages 90-116 in V. Nee and R. Swedberg, editors. The economic sociology of capitalism. Princeton University Press, Princeton, New Jersey, USA.</mixed-citation>
         </ref>
         <ref id="ref20">
            <mixed-citation publication-type="other">Fischer-Kowalski, M., and H. Haberl, editors. 2007. Socioecological transitions and global change: trajectories of social metabolism and land use. Edward Elgar, Cheltenham, UK. http://dx.doi.org/10.4337/9781847209436</mixed-citation>
         </ref>
         <ref id="ref21">
            <mixed-citation publication-type="other">Fischer-Kowalski, M., and M. Swilling. 2011. Decoupling natural resource use and environmental impacts from economic growth. United Nations Environment Programme, Paris, France. [online] URL: http://www.unep.org/resourcepanel/decoupling/files/pdf/Decoupling_Report_English.pdf</mixed-citation>
         </ref>
         <ref id="ref22">
            <mixed-citation publication-type="other">Food and Agriculture Organization. 2013. The state of food and agriculture 2013: better systems for better nutrition. Food and Agriculture Organization, Rome, Italy. [online] URL: http://www.fao.org/docrep/018/i3300e/i3300e00.htm</mixed-citation>
         </ref>
         <ref id="ref23">
            <mixed-citation publication-type="other">Franz, H.-W., J. Hochgerner, and J. Howaldt, editors. 2012. Challenge social innovation: potentials for business, social entrepreneurship, welfare and civil society. Springer, Berlin, Germany. http://dx.doi.org/10.1007/978-3-642-32879-4</mixed-citation>
         </ref>
         <ref id="ref24">
            <mixed-citation publication-type="other">Godfray, H. C. J., I. R. Crute, L. Haddad, D. Lawrence, J. F. Muir, N. Nisbett, J. Pretty, S. Robinson, C. Toulmin, and R. Whiteley. 2010. The future of the global food system. Philosophical Transactions of the Royal Society B 365(1554):2769-2777. http://dx.doi.org/10.1098/rstb.2010.0180</mixed-citation>
         </ref>
         <ref id="ref25">
            <mixed-citation publication-type="other">Grin, J., J. Rotmans, J. Schot, F. Geels, and D. Loorbach. 2010. Transitions to sustainable development: new directions in the study of long term transformative change. Routledge, New York, New York, USA.</mixed-citation>
         </ref>
         <ref id="ref26">
            <mixed-citation publication-type="other">Hajer, M., H. Westhoek, L. Ozay, J. Ingram, and S. van Berkum. 2016. Food systems and natural resources. Report for the International Resource Panel. United Nations Environment Programme, Paris, France. In press.</mixed-citation>
         </ref>
         <ref id="ref27">
            <mixed-citation publication-type="other">Haxeltine, A., F. Avelino, J. Wittmayer, R. Kemp, P. Weaver, J. Backhaus, and T. O’Riordan. 2013. Transformative social innovation: a sustainability transitions perspective on social innovation. Pages 2-19 in NESTA, editor. Social frontiers. NESTA, London, UK. http://www.scribd.com/doc/191799102/Transformative-social-innovations-A-sustainability-transition-perspectiveon- socialinnovation</mixed-citation>
         </ref>
         <ref id="ref28">
            <mixed-citation publication-type="other">Hirsch-Hadorn, G., D. Bradley, C. Pohl, S. Rist, and U. Wiesmann. 2006. Implications of transdisciplinarity for sustainability research. Ecological Economics 60(1):119-128. http://dx.doi.org/10.1016/j.ecolecon.2005.12.002</mixed-citation>
         </ref>
         <ref id="ref29">
            <mixed-citation publication-type="other">Hirsch-Hadorn, G., H. Hoffmann-Riem, S. Biber-Klemm, W. Grossenbacher-Mansuy, D. Joye, C. Pohl, U. Wiesmann, and E. Zemp, editors. 2008. Handbook of transdisciplinary research. Springer, Dordrecht, The Netherlands. http://dx.doi.org/10.1007/978-1-4020-6699-3</mixed-citation>
         </ref>
         <ref id="ref30">
            <mixed-citation publication-type="other">International Monetary Fund. 2011. Regional economic outlook: sub-Saharan Africa: recovery and new risks. International Monetary Fund, Washington, D.C., USA. [online] URL: https://www.imf.org/external/pubs/ft/reo/2011/afr/eng/sreo0411.</mixed-citation>
         </ref>
         <ref id="ref31">
            <mixed-citation publication-type="other">htmInternational Renewable Energy Agency. 2013. Africa’s renewable future: the path to sustainable growth. International Renewable Energy Agency, Abu Dhabi, United Arab Emirates. [online] URL: http://www.irena.org/documentdownloads/publications/africa_renewable_future.pdf</mixed-citation>
         </ref>
         <ref id="ref32">
            <mixed-citation publication-type="other">International Renewable Energy Agency. 2014. Africa clean energy corridor: analysis of infrastructure for renewable power in southern Africa. International Renewable Energy Agency, Abu Dhabi, United Arab Emirates. [online] URL: http://www.irena.org/DocumentDownloads/Publications/ACEC%20Document%20V19- For%20Web%20Viewing-Small.pdf</mixed-citation>
         </ref>
         <ref id="ref33">
            <mixed-citation publication-type="other">Keller, A. 2012. Conceptualising a sustainable energy solution for in situ informal settlement upgrading. Thesis. Stellenbosch University, Stellenbosch, South Africa. [online] URL: http://www.sustainabilityinstitute.net/si-library/4002-conceptualising-asustainable- energy-solution-for-in-situ-informal-settlement-upgrading</mixed-citation>
         </ref>
         <ref id="ref34">
            <mixed-citation publication-type="other">Khan, F. 2008. Political economy of housing policy in South Africa, 1994–2004. Dissertation. Stellenbosch University, Stellenbosch, South Africa.</mixed-citation>
         </ref>
         <ref id="ref35">
            <mixed-citation publication-type="other">Lang, D. J., A. Wiek, M. Bergmann, M. Stauffacher, P. Martens, P. Moll, M. Swilling, and C. J. Thomas. 2012. Transdisciplinary research in sustainability science: practice, principles, and challenges. Sustainability Science 7(S1):25-43. http://dx.doi.org/10.1007/s11625-011-0149-x</mixed-citation>
         </ref>
         <ref id="ref36">
            <mixed-citation publication-type="other">Latour, B. 2010. Where are the passions commensurate with the stakes? IDDRI SciencePo: Annual Report 2010. Institute for Sustainable Development and International Relasions, Paris, France.</mixed-citation>
         </ref>
         <ref id="ref37">
            <mixed-citation publication-type="other">Monitor Group. 2009. Africa: from the bottom up: cities, economic growth, and prosperity in sub-Saharan Africa. Monitor Group, Johannesburg, South Africa.</mixed-citation>
         </ref>
         <ref id="ref38">
            <mixed-citation publication-type="other">Moulaert, F., D. MacCullum, A. Mehmood, and A. Hamdouch, editors. 2013. International handbook on social innovation: collective action, social learning and transdisciplinary research. Edward Elgar, Cheltenham, UK. http://dx.doi.org/10.4337/9781849809993</mixed-citation>
         </ref>
         <ref id="ref39">
            <mixed-citation publication-type="other">Muhar, A., J. Visser, and J. van Breda. 2013. Experiences from establishing structured inter- and transdisciplinary doctoral programs in sustainability: a comparison of two cases in South Africa and Austria. Journal of Cleaner Production 61:122-129. http://dx.doi.org/10.1016/j.jclepro.2013.07.031</mixed-citation>
         </ref>
         <ref id="ref40">
            <mixed-citation publication-type="other">Mulgan, G. 2006. The process of social innovation. Innovations 1(2):145-162. http://dx.doi.org/10.1162/itgg.2006.1.2.145</mixed-citation>
         </ref>
         <ref id="ref41">
            <mixed-citation publication-type="other">Murray, R., J. Caulier-Grice, and G. Mulgan. 2010. Open book of social innovation. Young Foundation and NESTA, London, UK. [online] URL: http://youngfoundation.org/publications/theopen- book-of-social-innovation/</mixed-citation>
         </ref>
         <ref id="ref42">
            <mixed-citation publication-type="other">New Partnership for African Development (NEPAD). 2014. Agriculture in Africa: transformation and outlook. African Union, Addis Ababa, Ethiopia. [online] URL: http://www.un.org/en/africa/osaa/pdf/pubs/2013africanagricultures.pdf</mixed-citation>
         </ref>
         <ref id="ref43">
            <mixed-citation publication-type="other">Parnell, S., and E. Pieterse, editors. 2014. Africa’s urban revolution. Zed Books, London, UK.</mixed-citation>
         </ref>
         <ref id="ref44">
            <mixed-citation publication-type="other">Patel, R. 2008. Stuffed and starved. Melville House, New York, New York, USA.</mixed-citation>
         </ref>
         <ref id="ref45">
            <mixed-citation publication-type="other">Regeer, B. J., and J. F. G. Bunders. 2009. Knowledge co-creation: interaction between science and society: a transdisciplinary approach to complex societal issues. RMNO, Amsterdam, The Netherlands. [online] URL: http://www.managingforimpact.org/sites/default/files/resource/bunders_regeer_2009_knowledge_cocreation1_tcm19-355212.pdf</mixed-citation>
         </ref>
         <ref id="ref46">
            <mixed-citation publication-type="other">Roxburgh, C., N. Dörr, A. Leke, A. Tazi-Riffi, A. van Wamelen, S. Lund, M. Chironga, T. Alatovik, C. Atkins, N. Terfous, and T. Zeino-Mahmalat. 2010. Lions on the move: the progress and potential of African economies. McKinsey Global Institute, London, UK. [online] URL: http://http://www.mckinsey.com/insights/africa/lions_on_the_move</mixed-citation>
         </ref>
         <ref id="ref47">
            <mixed-citation publication-type="other">Scherr, S. 1999. Soil degradation: a threat to developing-country food security by 2020? Food, Agriculture and the Environment Discussion Paper 63. International Food Policy Research Institute, Washington, D.C., USA. [online] URL: http://www.ifpri.org/sites/default/files/publications/pubs_2020_dp_dp27.pdf</mixed-citation>
         </ref>
         <ref id="ref48">
            <mixed-citation publication-type="other">Scholz, R. W., and O. Tietje. 2002. Embedded case study methods: integrating quantitative and qualitative knowledge. Sage, Thousand Oaks, California, USA.</mixed-citation>
         </ref>
         <ref id="ref49">
            <mixed-citation publication-type="other">Sonderegger, T., S. Pfister, and S. Hellweg. 2015. Criticality of water: aligning water and mineral resources assessment. Environmental Science and Technology 49(20):12315-12323. http://dx.doi.org/10.1021/acs.est.5b02982</mixed-citation>
         </ref>
         <ref id="ref50">
            <mixed-citation publication-type="other">Swilling, M. 2014. Rethinking the science–policy interface in South Africa: experiments in knowledge co-production. South African Journal of Science 10(5/6):Art.#2013-0265. http://dx.doi.org/10.1590/sajs.2014/20130265</mixed-citation>
         </ref>
         <ref id="ref51">
            <mixed-citation publication-type="other">Swilling, M., L. Tavener-Smith, A. Keller, V. von der Heyde, and B. Wessels. 2016. Rethinking incremental urbanism: coproduction of incremental informal settlement upgrading strategies. In M. van Donk, T. Gorgens, and L. Cirolia, editors. Pursuing partnership-based approaches to incremental upgrading in South Africa. Jacana, Johannesburg, South Africa. In press.</mixed-citation>
         </ref>
         <ref id="ref52">
            <mixed-citation publication-type="other">Turok, I. 2014. Linking urbanisation and development in Africa’s economic revival. Pages 60-81 in S. Parnell and E. Pieterse, editors. Africa’s urban revolution. Zed Books, London, UK.</mixed-citation>
         </ref>
         <ref id="ref53">
            <mixed-citation publication-type="other">Turton, A. R., and P. J. Ashton. 2008. Basin closure and issues of scale: the southern African hydropolitical complex. International Journal of Water Resources Development 24(2):305-318. http://dx.doi.org/10.1080/07900620701723463</mixed-citation>
         </ref>
         <ref id="ref54">
            <mixed-citation publication-type="other">United Nations Centre for Human Settlements. 2003. The challenge of slums: global report on human settlements. Earthscan, London, UK.</mixed-citation>
         </ref>
         <ref id="ref55">
            <mixed-citation publication-type="other">United Nations Conference on Trade and Development (UNCTAD). 2012. Economic development in Africa: report 2012: structural transformation and sustainable development in Africa. United Nations Conference on Trade and Development, Geneva, Switzerland. [online] URL: http://unctad.org/en/PublicationsLibrary/aldcafrica2012_embargo_en.pdf</mixed-citation>
         </ref>
         <ref id="ref56">
            <mixed-citation publication-type="other">United Nations Department of Economic and Social Affairs. 2012. World urbanization prospects: the 2011 revision. UnitedNations, New York, New York, USA. [online] URL: http://www.un.org/en/development/desa/population/publications/pdf/urbanization/WUP2011_Report.pdf</mixed-citation>
         </ref>
         <ref id="ref57">
            <mixed-citation publication-type="other">United Nations Economic Commission for Africa (UNECA) and African Union. 2014. Dynamic industrial policy in Africa: economic report on Africa. United Nations Commission for Africa, Addis Ababa, Ethiopia. [online] URL: http://www.uneca.org/sites/default/files/PublicationFiles/final_era2014_march25_en.pdf</mixed-citation>
         </ref>
         <ref id="ref58">
            <mixed-citation publication-type="other">United Nations Habitat. 2008. The state of African cities 2008: a framework for addressing urban challenges in Africa. United Nations Human Settlements Programme (UN-Habitat), Nairobi, Kenya. [online] URL: http://mirror.unhabitat.org/pmss/listItemDetails.aspx?publicationID=2574</mixed-citation>
         </ref>
         <ref id="ref59">
            <mixed-citation publication-type="other">World Bank. 2007. World development report 2008: agriculture for development. World Bank, Washington, D.C., USA. [online] URL: http://siteresources.worldbank.org/INTWDRS/Resources/477365-1327599046334/8394679-1327614067045/WDROver2008-ENG.pdf</mixed-citation>
         </ref>
         <ref id="ref60">
            <mixed-citation publication-type="other">World Bank. 2009. World development report 2009: shaping economic geography. Oxford University Press, Oxford, UK. [online] URL: http://siteresources.worldbank.org/INTWDRS/Resources/477365-1327525347307/8392086-1327528510568/PartTwo_web_full.pdf</mixed-citation>
         </ref>
         <ref id="ref61">
            <mixed-citation publication-type="other">World Bank. 2011. Africa’s future and the World Bank’s support to it. World Bank, Washington, D.C., USA. [online] URL: http://siteresources.worldbank.org/INTAFRICA/Resources/AFR_Regional_Strategy_3-2-11.pdf</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
