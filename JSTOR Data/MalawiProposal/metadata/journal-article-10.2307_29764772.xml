<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">clininfedise</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j101405</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Clinical Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Chicago Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">10584838</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">29764772</article-id>
         <article-categories>
            <subj-group>
               <subject>ARTICLES AND COMMENTARIES</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>HIV and Placental Infection Modulate the Appearance of Drug-Resistant Plasmodium falciparum in Pregnant Women who Receive Intermittent Preventive Treatment</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Clara</given-names>
                  <surname>Menéndez</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Elisa</given-names>
                  <surname>Serra-Casas</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Michael D.</given-names>
                  <surname>Scahill</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Sergi</given-names>
                  <surname>Sanz</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Augusto</given-names>
                  <surname>Nhabomba</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Azucena</given-names>
                  <surname>Bardají</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Betuel</given-names>
                  <surname>Sigauque</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Pau</given-names>
                  <surname>Cisteró</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Inacio</given-names>
                  <surname>Mandomando</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Carlota</given-names>
                  <surname>Dobaño</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Pedro L.</given-names>
                  <surname>Alonso</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Alfredo</given-names>
                  <surname>Mayor</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>1</month>
            <year>2011</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">52</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i29764761</issue-id>
         <fpage>41</fpage>
         <lpage>48</lpage>
         <permissions>
            <copyright-statement>© 2011 The Infectious Diseases Society of America</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:href="http://dx.doi.org/10.1093/cid/ciq049"
                   xlink:title="an external site"/>
         <abstract>
            <p>Background. Factors involved in the development of resistance to sulphadoxine-pyrimethamine (SP) by Plasmodium falciparum, particularly in the context of intermittent preventive treatment during pregnancy (IPTp), are not well known. We aimed to determine the impact of IPTp and human immunodeficiency virus (HIV) infection on molecular markers of SP resistance and the clinical relevance of resistant infections. Methods. SP resistance alleles were determined in peripheral (n = 125) and placental (n = 145) P. falciparum isolates obtained from pregnant women enrolled in a randomized, placebo-controlled trial of IPTp in Manhiça, Mozambique. Results. Prevalence of quintuple mutant infections was 12% (23 of 185 isolates) in pregnant women who received placebo and 24% (20 of 85 isolates) in those who received SP (P = .031). When the last IPTp dose was administered at late pregnancy, mutant infections at delivery were more prevalent in placental samples (7 [23%] of 30, samples) than in peripheral blood samples (2 [7%] of 30 samples; P = .025), more prevalent in women who received IPTp-SP than in those who received placebo (odds ratio [OR], 8.13; 95% confidence interval [CI], 1.69-39.08), and more prevalent in HIV-positive women than in HIV-negative women (OR, 5.17; 95% CI, 1.23-21.66). No association was found between mutant infections and increased parasite density or malaria-related morbidity in mothers and children. Conclusions. IPTp with SP increases the prevalence of resistance markers in the placenta and in HIV-infected women at delivery, which suggests that host immunity is key for the clearance of drug-resistant infections. However, this effect of IPTp is limited to the period when blood levels of SP are likely to be significant and does not translate into more-severe infections or adverse clinical outcomes.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d485e298a1310">
            <label>1</label>
            <mixed-citation id="d485e305" publication-type="other">
Brabin BJ. An analysis of malaria in pregnancy in Africa. Bull World
Health Organ 1983; 61(6);1005-1016.</mixed-citation>
         </ref>
         <ref id="d485e315a1310">
            <label>2</label>
            <mixed-citation id="d485e322" publication-type="other">
White NJ, McGready RM, Nosten FH. New medicines for tropical
diseases in pregnancy: catch-22. PLoS Med 2008; 5(6);el33.</mixed-citation>
         </ref>
         <ref id="d485e332a1310">
            <label>3</label>
            <mixed-citation id="d485e339" publication-type="other">
Ward SA, Sevene EJ, Hastings IM, Nosten F, McGready R. Antimalarial
drugs and pregnancy: safety, pharmacokinetics, and pharmacovigi-
lance. Lancet Infect Dis 2007; 7(2);136-144.</mixed-citation>
         </ref>
         <ref id="d485e352a1310">
            <label>4</label>
            <mixed-citation id="d485e359" publication-type="other">
Yamada M, Steketee R, Abramowsky C, et al. Plasmodium falcipa-
rum associated placental pathology: a light and electron microscopic
and immunohistologic study. Am J Trop Med Hyg 1989;
41(2);161-168.</mixed-citation>
         </ref>
         <ref id="d485e376a1310">
            <label>5</label>
            <mixed-citation id="d485e383" publication-type="other">
Mount AM, Mwapasa V, Elliott SR, et al. Impairment of humoral
immunity to Plasmodium falciparum malaria in pregnancy by HIV
infection. Lancet 2004; 363(9424);1860-1867.</mixed-citation>
         </ref>
         <ref id="d485e396a1310">
            <label>6</label>
            <mixed-citation id="d485e403" publication-type="other">
Rogerson SJ, Wijesinghe RS, Meshnick SR. Host immunity as a de¬
terminant of treatment outcome in Plasmodium falciparum malaria.
Lancet Infect Dis 2010; 10(l);51-59.</mixed-citation>
         </ref>
         <ref id="d485e416a1310">
            <label>7</label>
            <mixed-citation id="d485e423" publication-type="other">
Menendez C, Bardaji A, Sigauque B, et al. Malaria prevention with IPTp
during pregnancy reduces neonatal mortality. PLoS One 2010;
5(2);e9438.</mixed-citation>
         </ref>
         <ref id="d485e436a1310">
            <label>8</label>
            <mixed-citation id="d485e443" publication-type="other">
World Health Organization (WHO). A strategic framework for malaria
prevention and control during pregnancy in the African region.
Brazzaville, Congo: WHO Regional Office for Africa, 2004.</mixed-citation>
         </ref>
         <ref id="d485e456a1310">
            <label>9</label>
            <mixed-citation id="d485e463" publication-type="other">
Karunajeewa HA, Salman S, Mueller I, et al. Pharmacokinetic prop¬
erties of sulfadoxine-pyrimethamine in pregnant women. Antimicrob
Agents Chemother 2009; 53(10);4368-4376.</mixed-citation>
         </ref>
         <ref id="d485e476a1310">
            <label>10</label>
            <mixed-citation id="d485e483" publication-type="other">
Sibley CH, Hyde IE, Sims PF, et al. Pyrimethamine-sulfadoxine re¬
sistance in Plasmodium falciparum: what next? Trends Parasitol 2001;
17(12);582-588.</mixed-citation>
         </ref>
         <ref id="d485e497a1310">
            <label>11</label>
            <mixed-citation id="d485e504" publication-type="other">
Kublin JG, Dzinjalamala FK, Kamwendo DD, et al. Molecular markers
for failure of sulfadoxine-pyrimethamine and chlorproguanil-dapsone
treatment of Plasmodium falciparum malaria. J Infect Dis 2002;
185(3);380-388.</mixed-citation>
         </ref>
         <ref id="d485e520a1310">
            <label>12</label>
            <mixed-citation id="d485e527" publication-type="other">
Mockenhaupt FP, Bedu-Addo G, Junge C, Hommerich L, Eggelte TA,
Bienzle U. Markers of sulfadoxine-pyrimethamine-resistant Plasmo¬
dium falciparum in placenta and circulation of pregnant women.
Antimicrob Agents Chemother 2007; 51(l);332-334.</mixed-citation>
         </ref>
         <ref id="d485e543a1310">
            <label>13</label>
            <mixed-citation id="d485e550" publication-type="other">
Mockenhaupt FP, Bedu-Addo G, Eggelte TA, et al. Rapid increase in
the prevalence of sulfadoxine-pyrimethamine resistance among Plas¬
modium falciparum isolated from pregnant women in Ghana. J Infect
Dis 2008; 198 (10); 1545-1549.</mixed-citation>
         </ref>
         <ref id="d485e566a1310">
            <label>14</label>
            <mixed-citation id="d485e573" publication-type="other">
Bouyou-Akotet MK, Mawili-Mboumba DP, Tchantchou Tde D,
Kombila M. High prevalence of sulfadoxine/pyrimethamine-resistant
alleles of Plasmodium falciparum isolates in pregnant women at the time
of introduction of intermittent preventive treatment with sulfadoxine/
pyrimethamine in Gabon. J Antimicrob Chemother 2010; 65(3);
438-^41.</mixed-citation>
         </ref>
         <ref id="d485e596a1310">
            <label>15</label>
            <mixed-citation id="d485e603" publication-type="other">
Harrington WE, Mutabingwa TK, Muehlenbachs A, et al. Competitive
facilitation of drug-resistant Plasmodium falciparum malaria parasites
in pregnant women who receive preventive treatment. Proc Natl Acad
Sei U S A 2009; 106(22);9027-9032.</mixed-citation>
         </ref>
         <ref id="d485e619a1310">
            <label>16</label>
            <mixed-citation id="d485e626" publication-type="other">
Alonso P, Saute F, Aponte JJ, et al. Manhica demographic surveillance
System, Mozambique. Population, Health and Survival at INDEPTH
Sites 2001; 1:189-195.</mixed-citation>
         </ref>
         <ref id="d485e640a1310">
            <label>17</label>
            <mixed-citation id="d485e647" publication-type="other">
Menendez C, Bardaji A, Sigauque B, et al. A randomized placebo-
controlled trial of intermittent preventive treatment in pregnant
women in the context of insecticide treated nets delivered through the
antenatal clinic. PLoS One 2008; 3 (4) ;e 1934.</mixed-citation>
         </ref>
         <ref id="d485e663a1310">
            <label>18</label>
            <mixed-citation id="d485e670" publication-type="other">
Alonso PL, Smith T, Schellenberg JR, et al. Randomised trial of efficacy
of SP666 vaccine against Plasmodium falciparum malaria in children in
southern Tanzania. Lancet 1994; 344(8931);1175-1181.</mixed-citation>
         </ref>
         <ref id="d485e683a1310">
            <label>19</label>
            <mixed-citation id="d485e690" publication-type="other">
Ismail MR, Ordi J, Menendez C, et al. Placental pathology in malaria:
a histological, immunohistochemical, and quantitative study. Hum
Pathol 2000; 31(l);85-93.</mixed-citation>
         </ref>
         <ref id="d485e703a1310">
            <label>20</label>
            <mixed-citation id="d485e710" publication-type="other">
Mayor A, Serra-Casas E, Bardaji A, et al. Sub-microscopic infections
and long-term recrudescence of Plasmodium falciparum in Mozambi-
can pregnant women. Malar J 2009; 8:9.</mixed-citation>
         </ref>
         <ref id="d485e723a1310">
            <label>21</label>
            <mixed-citation id="d485e730" publication-type="other">
Plowe CV, Cortese JF, Djimde A, et al. Mutations in Plasmodium
falciparum dihydrofolate reductase and dihydropteroate synthase
and epidemiologic patterns of pyrimethamine-sulfadoxine use and
resistance. J Infect Dis 1997; 176(6);1590-1596.</mixed-citation>
         </ref>
         <ref id="d485e746a1310">
            <label>22</label>
            <mixed-citation id="d485e753" publication-type="other">
Snounou G, Zhu X, Siripoon N, et al. Biased distribution of mspl
and msp2 allelic variants in Plasmodium falciparum populations in
Thailand. Trans R Soc Trop Med Hyg 1999; 93(4);369-374.</mixed-citation>
         </ref>
         <ref id="d485e767a1310">
            <label>23</label>
            <mixed-citation id="d485e774" publication-type="other">
White NJ, Pongtavornpinyo W. The de novo selection of drug-resistant
malaria parasites. Proc Biol Sei 2003; 270(1514);545-554.</mixed-citation>
         </ref>
         <ref id="d485e784a1310">
            <label>24</label>
            <mixed-citation id="d485e791" publication-type="other">
Jafari-Guemouri S, Ndam NT, Bertin G, et al. Demonstration of
a high level of parasite population homology by quantification of
Plasmodium falciparum alleles in matched peripheral, placental,
and umbilical cord blood samples. J Clin Microbiol 2005; 43(6);
2980-2983.</mixed-citation>
         </ref>
         <ref id="d485e810a1310">
            <label>25</label>
            <mixed-citation id="d485e817" publication-type="other">
Diouf I, Fievet N, Doucoure S, et al. IL-12 producing monocytes and
IFN-gamma and TNF-alpha producing T-lymphocytes are increased in
placentas infected by Plasmodium falciparum. J Reprod Immunol 2007;
74(1-2);152-162.</mixed-citation>
         </ref>
         <ref id="d485e833a1310">
            <label>26</label>
            <mixed-citation id="d485e840" publication-type="other">
Diouf I, Fievet N, Doucoure S, et al. Monocyte activation and T cell
inhibition in Plasmodium falciparum-miected placenta. J Infect Dis
2004; 189(12);2235-2242.</mixed-citation>
         </ref>
         <ref id="d485e853a1310">
            <label>27</label>
            <mixed-citation id="d485e860" publication-type="other">
Riley EM, Schneider G, Sambou I, Greenwood BM. Suppression of cell-
mediated immune responses to malaria antigens in pregnant Gambian
women. Am J Trop Med Hyg 1989; 40(2);141-144.</mixed-citation>
         </ref>
         <ref id="d485e873a1310">
            <label>28</label>
            <mixed-citation id="d485e880" publication-type="other">
Hastings IM, Donnelly MJ. The impact of antimalarial drug resistance
mutations on parasite fitness, and its implications for the evolution of
resistance. Drug Resist Updat 2005; 8(l-2);43-50.</mixed-citation>
         </ref>
         <ref id="d485e894a1310">
            <label>29</label>
            <mixed-citation id="d485e901" publication-type="other">
Carter JY, Loolpapit MP, Lema OE, Tome JL, Nagelkerke NJ, Watkins
WM. Reduction of the efficacy of antifolate antimalarial therapy by folk
acid supplementation. Am J Trop Med Hyg 2005; 73(1);166-170.</mixed-citation>
         </ref>
         <ref id="d485e914a1310">
            <label>30</label>
            <mixed-citation id="d485e921" publication-type="other">
Mayor A, Serra-Casas E, Sanz S, et al. Molecular markers of resistance
to sulfadoxine-pyrimethamine during intermittent preventive treat¬
ment for malaria in Mozambican infants. J Infect Dis 2008; 197(12);
1737-1742.</mixed-citation>
         </ref>
         <ref id="d485e937a1310">
            <label>31</label>
            <mixed-citation id="d485e944" publication-type="other">
Djimde AA, Doumbo OK, Traore O, et al. Clearance of drug-resistant
parasites as a model for protective immunity in Plasmodium falciparum
malaria. Am J Trop Med Hyg 2003; 69(5);558-563.</mixed-citation>
         </ref>
         <ref id="d485e957a1310">
            <label>32</label>
            <mixed-citation id="d485e964" publication-type="other">
Marks F, von Kalckreuth V, Kobbe R, et al. Parasitological rebound
effect and emergence of pyrimethamine resistance in Plasmodium fal¬
ciparum after single-dose sulfadoxine-pyrimethamine. J Infect Dis
2005; 192(11);1962-1965.</mixed-citation>
         </ref>
         <ref id="d485e980a1310">
            <label>33</label>
            <mixed-citation id="d485e987" publication-type="other">
Gesase S, Gosling RD, Hashim R, et al. High resistance of Plasmodium
falciparum to sulphadoxine/pyrimethamine in northern Tanzania and
the emergence of dhps resistance mutation at Codon 581. PLoS One
2009; 4(2);e4569.</mixed-citation>
         </ref>
         <ref id="d485e1003a1310">
            <label>34</label>
            <mixed-citation id="d485e1010" publication-type="other">
Chawira AN, Warhurst DC, Peters W. Qinghaosu resistance in rodent
malaria. Trans R Soc Trop Med Hyg 1986; 80(3);477-480.</mixed-citation>
         </ref>
         <ref id="d485e1021a1310">
            <label>35</label>
            <mixed-citation id="d485e1028" publication-type="other">
Nkhoma S, Molyneux M, Ward S. Molecular surveillance for drug-
resistant Plasmodium falciparum malaria in Malawi. Acta Trop 2007;
102(2);138-142.</mixed-citation>
         </ref>
         <ref id="d485e1041a1310">
            <label>36</label>
            <mixed-citation id="d485e1048" publication-type="other">
Wegmann TG, Lin H, Guilbert L, Mosmann TR. Bidirectional
cytokine interactions in the maternal-fetal relationship: is successful
pregnancy a TH2 phenomenon? Immunol Today 1993; 14(7);353-356.</mixed-citation>
         </ref>
         <ref id="d485e1061a1310">
            <label>37</label>
            <mixed-citation id="d485e1068" publication-type="other">
Steketee RW, Wirima JJ, Bioland PB, et al. Impairment of a pregnant
woman's acquired ability to limit Plasmodium falciparum by infection
with human immunodeficiency virus type-1. Am J Trop Med Hyg
1996; 55(suppl l);42-49.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
