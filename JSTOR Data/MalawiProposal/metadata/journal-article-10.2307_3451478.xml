<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">geographicalj</journal-id>
         <journal-id journal-id-type="jstor">j100008</journal-id>
         <journal-title-group>
            <journal-title>The Geographical Journal</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Blackwell Publishing</publisher-name>
         </publisher>
         <issn pub-type="ppub">00167398</issn>
         <issn pub-type="epub">14754959</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">3451478</article-id>
         <title-group>
            <article-title>Management of Transboundary Water Resources: Lessons from International Cooperation for Conflict Prevention</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Juha I.</given-names>
                  <surname>Uitto</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Alfred M.</given-names>
                  <surname>Duda</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>12</month>
            <year>2002</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">168</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id>i368330</issue-id>
         <fpage>365</fpage>
         <lpage>378</lpage>
         <page-range>365-378</page-range>
         <permissions>
            <copyright-statement>Copyright 2002 Royal Geographical Society</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/3451478"/>
         <abstract>
            <p>Tensions over freshwater resources may become more frequent as pressures on water resources grow due to increased demand and variability of rainfall. Conflicts may take place between or within countries or between competing sectoral users. This paper focuses on institutional approaches for enhancing cooperation between countries for sustainable development of transboundary freshwater bodies and contributing basins. It is assumed that instead of being zones of conflict, shared water resources can provide a basis for cooperation and benefit-sharing provided that threats to the international waters are recognized and collaborative structures are created. The paper draws upon experiences gained within the international waters focal area of the Global Environment Facility, the main funding mechanism for countries to support the environmental management of transboundary water resources. Lessons for promoting peaceful cooperation for environmental management, benefit-sharing and sustainable use of transboundary freshwater resources are highlighted through examples from Africa, Central Asia and Latin America. Experience shows the importance of processes that bring together all sectors and actors whose actions affect the transboundary waterbody at regional, national and local levels. The development of a science-based diagnostic analysis is essential to identify the threats to the transboundary ecosystem and to break down the issues into manageable parts with the aim of developing a strategic action programme. Ensuring political commitment that can result in institutional, policy and legal reforms in the countries concerned is the key to sustainable development of the transboundary resource.</p>
         </abstract>
         <kwd-group>
            <kwd>Transboundary Water Resources</kwd>
            <kwd>Aral Sea</kwd>
            <kwd>Bermejo River</kwd>
            <kwd>Lake Tanganyika</kwd>
            <kwd>Global Environment Facility</kwd>
            <kwd>Conflict Prevention</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d421e200a1310">
            <mixed-citation id="d421e204" publication-type="book">
Bewers J M and Uitto J I 2001 International waters program
study Evaluation Report #1-01 Global Environment Facility,
Washington DC<person-group>
                  <string-name>
                     <surname>Bewers</surname>
                  </string-name>
               </person-group>
               <fpage>1</fpage>
               <source>International waters program study Evaluation Report</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d421e236a1310">
            <mixed-citation id="d421e240" publication-type="book">
Biswas A K 2000 Water for urban areas of the developing
world in the twenty-first century in Uitto J I and Biswas
A K eds Water for urban areas: challenges and perspectives
United Nations University Press, Tokyo 1-23<person-group>
                  <string-name>
                     <surname>Biswas</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Water for urban areas of the developing world in the twenty-first century</comment>
               <fpage>1</fpage>
               <source>Water for urban areas: challenges and perspectives</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d421e278a1310">
            <mixed-citation id="d421e282" publication-type="journal">
Duda A M 1994 Achieving pollution prevention goals for
transboundary waters through international joint commis-
sion processes Water Science and Technology 30 223-31<person-group>
                  <string-name>
                     <surname>Duda</surname>
                  </string-name>
               </person-group>
               <fpage>223</fpage>
               <volume>30</volume>
               <source>Water Science and Technology</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d421e317a1310">
            <mixed-citation id="d421e321" publication-type="journal">
Duda A M and EI-Ashry M T 2000 Addressing the global
water and environment crises through integrated
approaches to the management of land, water and
ecological resources Water International 25 115-26<person-group>
                  <string-name>
                     <surname>Duda</surname>
                  </string-name>
               </person-group>
               <fpage>115</fpage>
               <volume>25</volume>
               <source>Water International</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d421e360a1310">
            <mixed-citation id="d421e364" publication-type="journal">
Duda A M and La Roche D 1997 Joint institutional arrange-
ments for addressing transboundary water resources issues
- lessons for the GEF Natural Resources Forum 21 127-37<person-group>
                  <string-name>
                     <surname>Duda</surname>
                  </string-name>
               </person-group>
               <fpage>127</fpage>
               <volume>21</volume>
               <source>Natural Resources Forum</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d421e399a1310">
            <mixed-citation id="d421e403" publication-type="book">
GEF 1996 Operational strategy Global Environment Facility,
Washington DC<person-group>
                  <string-name>
                     <surname>Gef</surname>
                  </string-name>
               </person-group>
               <source>Operational strategy</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d421e428a1310">
            <mixed-citation id="d421e432" publication-type="book">
Glantz M H 1998 Creeping environmental problems in the
Aral Sea basin in Kobori I and Glantz M H eds Central
Eurasian water crisis: Caspian, Aral, and Dead Seas United
Nations University Press, Tokyo 25-52<person-group>
                  <string-name>
                     <surname>Glantz</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Creeping environmental problems in the Aral Sea basin</comment>
               <fpage>25</fpage>
               <source>Central Eurasian water crisis: Caspian, Aral, and Dead Seas</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d421e470a1310">
            <mixed-citation id="d421e474" publication-type="book">
Glazovsky N F 1995 The Aral Sea basin in Kasperson J X,
Kasperson R E and Turner B L, II eds Regions at risk:
comparisons of threatened environments United Nations
University Press, Tokyo 92-139<person-group>
                  <string-name>
                     <surname>Glazovsky</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The Aral Sea basin</comment>
               <fpage>92</fpage>
               <source>Regions at risk: comparisons of threatened environments</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d421e512a1310">
            <mixed-citation id="d421e516" publication-type="book">
Homer-Dixon T 1999 The myth of global water wars Forum
International Committee of the Red Cross, Geneva 10-13<person-group>
                  <string-name>
                     <surname>Homer-Dixon</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The myth of global water wars</comment>
               <fpage>10</fpage>
               <source>Forum International Committee of the Red Cross</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d421e548a1310">
            <mixed-citation id="d421e552" publication-type="book">
IPCC 2001 The regional impacts of climate change: an
assessment of vulnerability Intergovernmental Panel on
Climate Change, Geneva<person-group>
                  <string-name>
                     <surname>Ipcc</surname>
                  </string-name>
               </person-group>
               <source>The regional impacts of climate change: an assessment of vulnerability</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d421e582a1310">
            <mixed-citation id="d421e586" publication-type="book">
Mageed Y A 1994 The Nile basin: lessons from the past in
Biswas A K ed International waters of the Middle East: from
Euphrates-Tigris to Nile Oxford University Press, Bombay
156-84<person-group>
                  <string-name>
                     <surname>Mageed</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The Nile basin: lessons from the past</comment>
               <fpage>156</fpage>
               <source>International waters of the Middle East: from Euphrates-Tigris to Nile</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d421e624a1310">
            <mixed-citation id="d421e628" publication-type="book">
Nile Basin Initiative 2001 Transboundary environmental
analysis Shared Vision Program, Nile Basin Initiative
Secretariat (http://www.nilebasin.org) Entebbe, Uganda<person-group>
                  <string-name>
                     <surname>Nile Basin Initiative</surname>
                  </string-name>
               </person-group>
               <source>Transboundary environmental analysis</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d421e657a1310">
            <mixed-citation id="d421e661" publication-type="book">
OAS 2000 Transboundary diagnostic analysis of the binational
basin of the Bermejo river Programa estrategico de accion
para la cuenca del Rio Bermejo, Organisation of American
States, Buenos Aires<person-group>
                  <string-name>
                     <surname>Oas</surname>
                  </string-name>
               </person-group>
               <source>Transboundary diagnostic analysis of the binational basin of the Bermejo river</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d421e693a1310">
            <mixed-citation id="d421e697" publication-type="book">
Ollila P, Uitto J I, Crepin C and Duda A M 2000 Multi-
country project arrangements: report of a thematic review
Monitoring and Evaluation Working Paper 3 Global
Environment Facility, Washington DC<person-group>
                  <string-name>
                     <surname>Ollila</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Multicountry project arrangements: report of a thematic review</comment>
               <source>Monitoring and Evaluation Working Paper 3 Global Environment Facility</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d421e732a1310">
            <mixed-citation id="d421e736" publication-type="book">
Rangeley R, Thiam B M, Andersen R A and Lyle C A 1994
International river basin organizations in sub-Saharan Africa
Technical paper no. 250 World Bank, Washington DC<person-group>
                  <string-name>
                     <surname>Rangeley</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">International river basin organizations in sub-Saharan Africa</comment>
               <source>Technical paper</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d421e768a1310">
            <mixed-citation id="d421e772" publication-type="book">
Renner M 1996 Fighting for survival: environmental decline,
social conflict, and the new age of insecurity W W Norton,
New York<person-group>
                  <string-name>
                     <surname>Renner</surname>
                  </string-name>
               </person-group>
               <source>Fighting for survival: environmental decline, social conflict, and the new age of insecurity</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d421e802a1310">
            <mixed-citation id="d421e806" publication-type="book">
Sjiberg H 1999 Restructuring the Global Environment
Facility Working Paper 13, Global Environment Facility,
Washington DC<person-group>
                  <string-name>
                     <surname>Sjiberg</surname>
                  </string-name>
               </person-group>
               <source>Restructuring the Global Environment Facility</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d421e835a1310">
            <mixed-citation id="d421e839" publication-type="book">
Uitto J I 2000 Population, food and water in the 21st century
in Rose J ed Population problems: topical issues Gordon
and Breach, Amsterdam 93-110<person-group>
                  <string-name>
                     <surname>Uitto</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Population, food and water in the 21st century</comment>
               <fpage>93</fpage>
               <source>Population problems: topical issues</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d421e874a1310">
            <mixed-citation id="d421e878" publication-type="book">
Uitto J I 2001 Global freshwater resources in Palo M,
Uusivuori J and Mery G eds World forests, markets and
policies Kluwer, Amsterdam 65-76<person-group>
                  <string-name>
                     <surname>Uitto</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Global freshwater resources</comment>
               <fpage>65</fpage>
               <source>World forests, markets and policies</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d421e913a1310">
            <mixed-citation id="d421e919" publication-type="book">
Uitto J I 2002a Multi-country co-operation for sustainable
development of international lake basins: lessons from the
Global Environment Facility in Jansky L, Nakayama M and
Uitto J I eds Lakes and reservoirs as international water
systems Towards a World Lake Vision United Nations
University, Tokyo 79-93<person-group>
                  <string-name>
                     <surname>Uitto</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Multi-country co-operation for sustainable development of international lake basins: lessons from the Global Environment Facility</comment>
               <fpage>79</fpage>
               <source>Lakes and reservoirs as international water systems</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d421e963a1310">
            <mixed-citation id="d421e967" publication-type="journal">
Uitto J I 2002b Environmental disaster feeds terrorism The
Japan Times 30 March<person-group>
                  <string-name>
                     <surname>Uitto</surname>
                  </string-name>
               </person-group>
               <issue>30 March</issue>
               <source>The Japan Times</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d421e996a1310">
            <mixed-citation id="d421e1000" publication-type="book">
UN 1997a World demographic trends Report of the
Secretary-General E/CN.9/1997/9 Commission on Popu-
lation and Development, thirtieth session 24-28 February
1997 United Nations Economic and Social Council, New
York<person-group>
                  <string-name>
                     <surname>Un</surname>
                  </string-name>
               </person-group>
               <source>World demographic trends</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d421e1036a1310">
            <mixed-citation id="d421e1040" publication-type="book">
UN 1997b Comprehensive assessment of the freshwater
resources of the world United Nations, New York<person-group>
                  <string-name>
                     <surname>Un</surname>
                  </string-name>
               </person-group>
               <source>Comprehensive assessment of the freshwater resources of the world</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d421e1065a1310">
            <mixed-citation id="d421e1069" publication-type="journal">
Wolf A T, Natharius J A, Danielson J ), Ward B S and Pender
j K 1999 International river basins of the world
International Journal of Water Resources Development 15
387-427<person-group>
                  <string-name>
                     <surname>Wolf</surname>
                  </string-name>
               </person-group>
               <fpage>387</fpage>
               <volume>15</volume>
               <source>International Journal of Water Resources Development</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d421e1107a1310">
            <mixed-citation id="d421e1111" publication-type="book">
World Commission on Dams 2000 Dams and development -
a new framework for decision-making Earthscan Publi-
cations, London<person-group>
                  <string-name>
                     <surname>World Commission On Dams</surname>
                  </string-name>
               </person-group>
               <source>Dams and development a new framework for decision-making</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d421e1140a1310">
            <mixed-citation id="d421e1144" publication-type="book">
World Water Commission 2000 A water secure world - vision
for water, life, and the environment Thanet Press, London<person-group>
                  <string-name>
                     <surname>World Water Commission</surname>
                  </string-name>
               </person-group>
               <source>A water secure world - vision for water, life, and the environment</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
