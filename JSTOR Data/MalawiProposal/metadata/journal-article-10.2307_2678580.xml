<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">japplecon</journal-id>
         <journal-id journal-id-type="jstor">j100764</journal-id>
         <journal-title-group>
            <journal-title>Journal of Applied Econometrics</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>John Wiley &amp; Sons</publisher-name>
         </publisher>
         <issn pub-type="ppub">08837252</issn>
         <issn pub-type="epub">10991255</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">2678580</article-id>
         <title-group>
            <article-title>Asymptotically Perfect and Relative Convergence of Productivity</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Bart</given-names>
                  <surname>Hobijn</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Philip Hans</given-names>
                  <surname>Franses</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>1</month>
            <year>2000</year>
         
            <day>1</day>
            <month>2</month>
            <year>2000</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">15</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id>i345935</issue-id>
         <fpage>59</fpage>
         <lpage>81</lpage>
         <page-range>59-81</page-range>
         <permissions>
            <copyright-statement>Copyright 2000 John Wiley &amp; Sons, Ltd.</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/2678580"/>
         <abstract>
            <p>In this paper we examine the extent to which countries are converging in per capita productivity levels. We propose to use cluster analysis in order to allow for the endogenous selection of converging countries. We formally define convergence in a time series analytical context, derive the necessary and sufficient conditions for convergence, and introduce a cluster analytical procedure that distinguishes several convergence clubs by testing for these conditions using a multivariate test for stationarity. We find a large number of relatively small convergence clubs, which suggests that convergence might not be such a widespread phenomenon.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d519e144a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d519e151" publication-type="book">
Hamilton (1994, p. 544).  <fpage>544</fpage>
               </mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d519e172a1310">
            <mixed-citation id="d519e176" publication-type="journal">
Barro, R. J. and X. Sala-i-Martin (1992), 'Convergence', Journal of Political Economy, 100, 223-25 1.<object-id pub-id-type="jstor">10.2307/2138606</object-id>
               <fpage>223</fpage>
            </mixed-citation>
         </ref>
         <ref id="d519e189a1310">
            <mixed-citation id="d519e193" publication-type="book">
Barro, R. J. and X. Sala-i-Martin (1995), 'Technological diffusion, convergence and growth', NBER
working paper 5151.<person-group>
                  <string-name>
                     <surname>Barro</surname>
                  </string-name>
               </person-group>
               <source>Technological diffusion, convergence and growth</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d519e218a1310">
            <mixed-citation id="d519e222" publication-type="journal">
Baumol, W. J. (1986), 'Productivity growth, convergence and welfare: what the long-run data show',
American Economic Review, 76, 1072-1085.<object-id pub-id-type="jstor">10.2307/1816469</object-id>
               <fpage>1072</fpage>
            </mixed-citation>
         </ref>
         <ref id="d519e238a1310">
            <mixed-citation id="d519e242" publication-type="book">
Baumol, W. J., R. R. Nelson and E. N. Wolff (eds) (1994), Convergence of Productivity: Cross National
Studies and Historical Evidence, Oxford University Press, New York.<person-group>
                  <string-name>
                     <surname>Baumol</surname>
                  </string-name>
               </person-group>
               <source>Convergence of Productivity: Cross National Studies and Historical Evidence</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d519e268a1310">
            <mixed-citation id="d519e272" publication-type="journal">
Ben-David, D. (1993), 'Equalizing exchange: trade liberalization and convergence', Quarterly Journal of
Economics, 108, 653-679.<object-id pub-id-type="doi">10.2307/2118404</object-id>
               <fpage>653</fpage>
            </mixed-citation>
         </ref>
         <ref id="d519e288a1310">
            <mixed-citation id="d519e292" publication-type="book">
Ben-David, D., R. L. Lumsdaine and D. H. Papell (1997), 'Unit roots, postwar slowdowns and long-run
growth: evidence from two structural breaks', working paper.<person-group>
                  <string-name>
                     <surname>Ben-David</surname>
                  </string-name>
               </person-group>
               <source>Unit roots, postwar slowdowns and long-run growth: evidence from two structural breaks</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d519e317a1310">
            <mixed-citation id="d519e321" publication-type="journal">
Bernard, A. B. and S. N. Durlauf (1995), 'Convergence in international output', Journal of Applied
Econometrics, 10, 161-173.<object-id pub-id-type="jstor">10.2307/2284967</object-id>
               <fpage>161</fpage>
            </mixed-citation>
         </ref>
         <ref id="d519e337a1310">
            <mixed-citation id="d519e341" publication-type="journal">
Bernard, A. B. and S. N. Durlauf (1996), 'Interpreting tests for the convergence hypothesis', Journal of
Econometrics, 91, 161-173.<person-group>
                  <string-name>
                     <surname>Bernard</surname>
                  </string-name>
               </person-group>
               <fpage>161</fpage>
               <volume>91</volume>
               <source>Journal of Econometrics</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d519e373a1310">
            <mixed-citation id="d519e377" publication-type="journal">
Bernard, A. B. and C. I. Jones (1996), 'Comparing apples and oranges: productivity convergence and
measurement across industries and countries', American Economic Review, 86, 1216-1238.<object-id pub-id-type="jstor">10.2307/2118287</object-id>
               <fpage>1216</fpage>
            </mixed-citation>
         </ref>
         <ref id="d519e393a1310">
            <mixed-citation id="d519e397" publication-type="journal">
Bianchi, M. (1997), 'Testing for convergence: evidence from non-parametric multimodality tests', Journal of
Applied Econometrics, 12, 393-409.<object-id pub-id-type="jstor">10.2307/2284960</object-id>
               <fpage>393</fpage>
            </mixed-citation>
         </ref>
         <ref id="d519e414a1310">
            <mixed-citation id="d519e418" publication-type="book">
Canova, F. and A. Marcet (1995), 'The poor stay poor: non-convergence across countries and regions',
CEPR Discussion Paper 1265.<person-group>
                  <string-name>
                     <surname>Canova</surname>
                  </string-name>
               </person-group>
               <source>The poor stay poor: non-convergence across countries and regions</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d519e443a1310">
            <mixed-citation id="d519e447" publication-type="journal">
Caselli, F., G. Esquivel and F. Lefort (1996), 'Reopening the convergence debate: a new look at cross-
country growth empirics', Journal of Economic Growth, 1, 363-389.<person-group>
                  <string-name>
                     <surname>Caselli</surname>
                  </string-name>
               </person-group>
               <fpage>363</fpage>
               <volume>1</volume>
               <source>Journal of Economic Growth</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d519e479a1310">
            <mixed-citation id="d519e483" publication-type="journal">
Daniel, B. C. (1997), 'International interdependence of national growth rates: a structural trends analysis',
Journal of Monetary Economics, 40, 73-96.<person-group>
                  <string-name>
                     <surname>Daniel</surname>
                  </string-name>
               </person-group>
               <fpage>73</fpage>
               <volume>40</volume>
               <source>Journal of Monetary Economics</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d519e515a1310">
            <mixed-citation id="d519e519" publication-type="journal">
Durlauf, S. N. and P. A. Johnson (1995), 'Multiple regimes and cross-country growth behavior', Journal of
Applied Econometrics, 10, 365-384.<object-id pub-id-type="jstor">10.2307/2285053</object-id>
               <fpage>365</fpage>
            </mixed-citation>
         </ref>
         <ref id="d519e535a1310">
            <mixed-citation id="d519e539" publication-type="book">
Durlauf, S. N. and D. T. Quah (1998), 'The new empirics of economic growth', NBER Working Paper Series
6422. To appear in The Handbook of Macroeconomics.<person-group>
                  <string-name>
                     <surname>Durlauf</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The new empirics of economic growth</comment>
               <source>The Handbook of Macroeconomics</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d519e568a1310">
            <mixed-citation id="d519e572" publication-type="book">
Hamilton, J. D. (1994), Time Series Analysis, Princeton University Press, Princeton, NJ.<person-group>
                  <string-name>
                     <surname>Hamilton</surname>
                  </string-name>
               </person-group>
               <source>Time Series Analysis</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d519e595a1310">
            <mixed-citation id="d519e599" publication-type="book">
Hobijn, B., P. H. Franses and M. Ooms (1998), 'Generalizations of the KPSS-test for stationarity'.
Econometric Institute Report 9802, Erasmus University Rotterdam.<person-group>
                  <string-name>
                     <surname>Hobijn</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Generalizations of the KPSS-test for stationarity</comment>
               <source>Econometric Institute Report 9802</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d519e628a1310">
            <mixed-citation id="d519e632" publication-type="journal">
Islam, N. (1995), 'Growth empirics: a panel data approach', Quarterly Journal of Economics, 110,
1127-1170.<object-id pub-id-type="doi">10.2307/2946651</object-id>
               <fpage>1127</fpage>
            </mixed-citation>
         </ref>
         <ref id="d519e648a1310">
            <mixed-citation id="d519e652" publication-type="journal">
Johansen, S. (1991), 'Estimation and hypothesis testing of cointegration vectors in Gaussian vector
autoregressive models', Econometrica, 55, 251-276.<object-id pub-id-type="doi">10.2307/2938278</object-id>
               <fpage>251</fpage>
            </mixed-citation>
         </ref>
         <ref id="d519e668a1310">
            <mixed-citation id="d519e672" publication-type="journal">
Jones, C. I. (1997), 'On the evolution of the world income distribution', Journal of Economic Perspectives,
11:3, 19-36.<object-id pub-id-type="jstor">10.2307/2138182</object-id>
               <fpage>19</fpage>
            </mixed-citation>
         </ref>
         <ref id="d519e688a1310">
            <mixed-citation id="d519e692" publication-type="journal">
Kwiatkowski, D., P. C. B. Phillips, P. Schmidt and Y. Shin (1992), 'Testing the null hypothesis of
stationarity against the alternative of a unit root', Journal of Econometrics, 54, 159-178.<person-group>
                  <string-name>
                     <surname>Kwiatkowski</surname>
                  </string-name>
               </person-group>
               <fpage>159</fpage>
               <volume>54</volume>
               <source>Journal of Econometrics</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d519e724a1310">
            <mixed-citation id="d519e728" publication-type="journal">
Mankiw, N. G., D. Romer and D. N. Weil (1992), 'A contribution to the empirics of economic growth',
Quarterly Journal of Economics, 107, 407-437.<object-id pub-id-type="doi">10.2307/2118477</object-id>
               <fpage>407</fpage>
            </mixed-citation>
         </ref>
         <ref id="d519e745a1310">
            <mixed-citation id="d519e749" publication-type="journal">
Newey, W. K. and K. D. West (1987), 'A simple, positive semi-definite, heteroskedasticity and auto-
correlation consistent covariance matrix', Econometrica, 55, 703-708.<object-id pub-id-type="doi">10.2307/1913610</object-id>
               <fpage>703</fpage>
            </mixed-citation>
         </ref>
         <ref id="d519e765a1310">
            <mixed-citation id="d519e769" publication-type="journal">
Pritchett, L. (1997), 'Divergence, big time', Journal of Economic Perspectives, 11(3), 3-17.<object-id pub-id-type="jstor">10.2307/2138181</object-id>
               <fpage>3</fpage>
            </mixed-citation>
         </ref>
         <ref id="d519e782a1310">
            <mixed-citation id="d519e786" publication-type="journal">
Quah, D. (1993), 'Galton's fallacy and tests for the convergence hypothesis', Scandinavian Journal of
Economics, 95, 427-443.<person-group>
                  <string-name>
                     <surname>Quah</surname>
                  </string-name>
               </person-group>
               <fpage>427</fpage>
               <volume>95</volume>
               <source>Scandinavian Journal of Economics</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d519e818a1310">
            <mixed-citation id="d519e822" publication-type="journal">
Quah, D. (1996a), 'Convergence empirics across economies with (some) capital mobility', Journal of
Economic Growth, 1, 95-124.<person-group>
                  <string-name>
                     <surname>Quah</surname>
                  </string-name>
               </person-group>
               <fpage>95</fpage>
               <volume>1</volume>
               <source>Journal of Economic Growth</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d519e854a1310">
            <mixed-citation id="d519e858" publication-type="journal">
Quah, D. (1996b), 'Empirics for economic growth and convergence', European Economic Review, 40, 1353-
1375.<person-group>
                  <string-name>
                     <surname>Quah</surname>
                  </string-name>
               </person-group>
               <fpage>1353</fpage>
               <volume>40</volume>
               <source>European Economic Review</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d519e890a1310">
            <mixed-citation id="d519e894" publication-type="journal">
Summers, R. and A. Heston (1991), 'The Penn World Table (mark 5): an expanded set of international
comparisons, 1950-1988', Quarterly Journal of Economics, 106, 327-368.<object-id pub-id-type="doi">10.2307/2937941</object-id>
               <fpage>327</fpage>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
