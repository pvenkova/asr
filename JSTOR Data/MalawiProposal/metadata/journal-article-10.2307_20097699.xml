<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">revinterstud</journal-id>
         <journal-id journal-id-type="jstor">j50000149</journal-id>
         <journal-title-group>
            <journal-title>Review of International Studies</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Cambridge University Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">02602105</issn>
         <issn pub-type="epub">14699044</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">20097699</article-id>
         <article-categories>
            <subj-group>
               <subject>Contradictions of Capitalism</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Aid and Sovereignty: Quasi-States and the International Financial Institutions</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>David</given-names>
                  <surname>Williams</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>10</month>
            <year>2000</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">26</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id>i20097693</issue-id>
         <fpage>557</fpage>
         <lpage>573</lpage>
         <permissions>
            <copyright-statement>Copyright 2000 British International Studies Association</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/20097699"/>
         <abstract>
            <p>This article examines the changing status of 'sovereignty' in the context of some of the world's poorest countries. An examination of the relationship between the International Financial Institutions (IFIs) and these countries suggests that the norm of sovereignty is increasingly being 'trumped' by the IFIs' commitment to the achievement of good political and social arrangements and economic development within these countries. The article explores the historical roots of this development by tracing the way sovereignty became bound up with the idea of self-determination, the achievement of the ideals of the Enlightenment, and the pursuit of a 'national economic project'.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d687e123a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d687e130" publication-type="other">
Thomas Biersteker and Cynthia
Weber, 'The Social Construction of State Sovereignty', in Thomas Biersteker and Cynthia Weber
(eds.), State Sovereignty as Social Construct (Cambridge: Cambridge University Press, 1996).</mixed-citation>
            </p>
         </fn>
         <fn id="d687e143a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d687e150" publication-type="other">
Robert Keohane and Joseph Nye (eds.), Transnational Relations and World Politics
(Cambridge, MA: Harvard University Press, 1972)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d687e159" publication-type="other">
Robert Gilpin, The Political Economy of
International Relations (Princeton, NJ: Princeton University Press, 1987)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d687e168" publication-type="other">
Mark Zacher, 'The
Decaying Pillars of the Westphalian Temple', in James Rosenau and Ernst-Otto Czempiel (eds.),
Governance Without Government: Order and Change in World Politics (Cambridge: Cambridge
University Press, 1991).</mixed-citation>
            </p>
         </fn>
         <fn id="d687e184a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d687e191" publication-type="other">
R. B. J Walker, Inside/Outside: International Relations as Political Theory
(Cambridge: Cambridge University Press, 1993)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d687e200" publication-type="other">
James Der Derian, On Diplomacy (Oxford:
Blackwell, 1987)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d687e209" publication-type="other">
Thomas Biersteker and Cynthia Weber (eds.), State Sovereignty as Social
Construct.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e219a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d687e226" publication-type="other">
J. Samuel Barkin and Bruce Cronin, 'The State and the Nation: Changing Norms and the Rules of
Sovereignty in International Relations', International Organization, 48 (1994), pp. 107-30.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e237a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d687e244" publication-type="other">
Walker, Inside/ Outside, p. 166</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d687e250" publication-type="other">
Jens Bartleson, A Geneaology of Sovereignty (Cambridge:
Cambridge University Press, 1995), p. 13.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e260a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d687e267" publication-type="other">
Janice Thomson, 'State Sovereignty in International Relations: Bridging the Gap Between Theory
and Empirical Research', International Studies Quarterly, 39 (1995), pp. 215-19.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e277a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d687e284" publication-type="other">
Martha Finnemore, 'Constructing Norms of Humanitarian Intervention', in Peter Katzenstein (ed.),
The Culture of National Security: Norms and Identity in World Politics (New York: Columbia
University Press, 1996), p. 161.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e297a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d687e304" publication-type="other">
Charles Taylor, 'To
Follow a Rule', in Philosophical Arguments (Cambridge, MA: Harvard University Press, 1995</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d687e313" publication-type="other">
Friedrich Kratochwil, Rules, Norms, and Decisions: On the Conditions of
Practical and Legal Reasoning in International Relations and Domestic Affairs (Cambridge:
Cambridge University Press, 1989)</mixed-citation>
            </p>
         </fn>
         <fn id="d687e326a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d687e333" publication-type="other">
John Boli and George M. Thomas (eds.), Constructing World Culture: International
Nongovernmental Organizations since 1875 (Stanford, CA: Stanford University Press, 1999).</mixed-citation>
            </p>
         </fn>
         <fn id="d687e343a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d687e350" publication-type="other">
Judith Goldstein and Robert Keohane (eds.), Ideas and Foreign Policy (Ithaca, NY: Cornell
University Press, 1992).</mixed-citation>
            </p>
         </fn>
         <fn id="d687e361a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d687e368" publication-type="other">
Charles Taylor, 'Social Theory as Practice', and 'Interpretation and the Sciences of
Man', in Philosophy and the Human Sciences: Philosophical Papers 2 (Cambridge: Cambridge
University Press, 1985).</mixed-citation>
            </p>
         </fn>
         <fn id="d687e381a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d687e388" publication-type="other">
Friedrich Kratochwil and John Gerard Ruggie, 'International Organization: A State of the Art on an
Art of the State', International Organization, 40 (1986), p. 753.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e398a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d687e405" publication-type="other">
Martha Finnemore, National Interests in International Society (Ithaca, NY: Cornell
University Press, 1996).</mixed-citation>
            </p>
         </fn>
         <fn id="d687e415a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d687e422" publication-type="other">
Nicholas Onuf, The Republican Legacy in International Thought (Cambridge: Cambridge University
Press, 1998), p. 148</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d687e431" publication-type="other">
Robert Cox,
'Social Forces, States, and World Orders: Beyond International Relations Theory', in Robert Keohane
(ed.), Neorealism and its Critics (New York: Columbia University Press, 1986).</mixed-citation>
            </p>
         </fn>
         <fn id="d687e444a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d687e451" publication-type="other">
Finnemore, 'Constructing Norms of Humanitarian Intervention', p. 185</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d687e457" publication-type="other">
Robert Keohane,
'International Institutions: Two Approaches', International Studies Quarterly, 32 (1988), p. 392</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d687e466" publication-type="other">
Finnemore, National Interests in International Society.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e473a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d687e480" publication-type="other">
Barkin and Cronin, 'The State and the Nation', p. 108.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e488a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d687e495" publication-type="other">
John Ruggie, 'Continuity and Transformation in the World Polity: Toward a Neorealist Synthesis', in
Keohane (ed.), Neorealism and its Critics, p. 143.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e505a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d687e512" publication-type="other">
Leo Gross, 'The Peace of Westphalia, 1648-1948', The American Journal of International Law, 42
(1948), pp. 20-41</mixed-citation>
            </p>
         </fn>
         <fn id="d687e522a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d687e529" publication-type="other">
Andreas Osiander, The States System of Europe, 1640-1990: Peacemaking and the Conditions of
International Stability (Oxford: Clarendon, 1994), ch. 2.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e539a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d687e546" publication-type="other">
Osiander, The States System of Europe, pp. 42-18.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e553a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d687e560" publication-type="other">
Ibid., p. 40</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d687e566" publication-type="other">
article XXVIII of the Treaty between the Holy Roman Emperor and France</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d687e572" publication-type="other">
Stephen Krasner, 'Westphalia and All That', in Goldstein and Keohane (eds.), Ideas and Foreign
Policy.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e582a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d687e589" publication-type="other">
Articles LXIX and LXX of the Treaty between the Holy Roman Emperor and France.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e597a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d687e604" publication-type="other">
Fred Halliday, 'State and Society in International Relations: A Second Agenda', Millennium, 16
(1987), pp. 215-29.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e614a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d687e621" publication-type="other">
Barkin and Cronin, 'The State and the Nation', p. 108.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e628a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d687e635" publication-type="other">
Charles Taylor, 'Invoking Civil Society', in Philosophical Arguments.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e642a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d687e649" publication-type="other">
Taylor, 'Invoking Civil Society', p. 231.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e656a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d687e663" publication-type="other">
Jurgen Habermas, The Structural Transformation of the Public Sphere: An Inquiry into a Category of
Bourgeois Society, trans. Thomas Berger (Cambridge, MA: MIT Press, 1989)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d687e672" publication-type="other">
Anthony
Volpa, 'Conceiving a Public: Ideas and Society in Eighteenth-Century Europe', The Journal of
Modern History, 64 (1992), pp. 79-116.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e685a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d687e692" publication-type="other">
Taylor, 'Invoking Civil Society', pp. 215-16.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e700a1310">
            <label>31</label>
            <p>
               <mixed-citation id="d687e707" publication-type="other">
Rosalyn Higgins, Problems and Processes: International Law and How We Use It (Oxford: Clarendon,
1994), ch. 7.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e717a1310">
            <label>32</label>
            <p>
               <mixed-citation id="d687e724" publication-type="other">
Higgins, Problems and Processes, ch. 7.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e731a1310">
            <label>33</label>
            <p>
               <mixed-citation id="d687e738" publication-type="other">
James Mayall, '1789 and the Liberal Theory of International Society', Review of International
Studies, 15 (1989), pp. 297-307.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e748a1310">
            <label>34</label>
            <p>
               <mixed-citation id="d687e755" publication-type="other">
Ruggie, 'Continuity and Transformation in the World Polity', p. 145.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e762a1310">
            <label>35</label>
            <p>
               <mixed-citation id="d687e769" publication-type="other">
Mayall, '1789 and the Liberal Theory of International Society'.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e776a1310">
            <label>36</label>
            <p>
               <mixed-citation id="d687e783" publication-type="other">
Edmund Burke, Reflections on the Revolution in France, quoted in Der Derian, On Diplomacy,
pp. 171-2.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e794a1310">
            <label>37</label>
            <p>
               <mixed-citation id="d687e801" publication-type="other">
Der Derian, On Diplomacy, p. 176.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e808a1310">
            <label>38</label>
            <p>
               <mixed-citation id="d687e815" publication-type="other">
Robert Jackson, Quasi-States: Sovereignty, International Relations and the Third World (Cambridge:
Cambridge University Press, 1990), pp. 71-4</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d687e824" publication-type="other">
Gerrit Gong, The Standard of 'Civilisation in
International Society (Oxford: Clarendon, 1984)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d687e833" publication-type="other">
David Strang, 'Contested Sovereignty: The
Social Construction of Colonial Imperialism', in Biersteker and Weber (eds.), State Sovereignty as
Social Construct, pp. 31-4.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e846a1310">
            <label>39</label>
            <p>
               <mixed-citation id="d687e853" publication-type="other">
John Stuart Mill, 'A Few Words on Non-Intervention', in Essays on Politics and Culture, (ed.),
Gertrude Himmelfarb (Gloucester, MA: Peter Smith, 1973), p. 337</mixed-citation>
            </p>
         </fn>
         <fn id="d687e863a1310">
            <label>40</label>
            <p>
               <mixed-citation id="d687e870" publication-type="other">
J. Samuel Barkin, 'The Evolution of the Constitution of Sovereignty and the Emergence of Human
Rights Norms', Millennium, 27 (1998), pp. 229-52</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d687e879" publication-type="other">
Jack Donnelly, 'The Social Construction of
International Human Rights', in Tim Dunne and Nicholas Wheeler (eds.), Human Rights in Global
Politics (Cambridge: Cambridge University Press, 1999).</mixed-citation>
            </p>
         </fn>
         <fn id="d687e892a1310">
            <label>41</label>
            <p>
               <mixed-citation id="d687e899" publication-type="other">
Gianfranco Poggi, The State: Its Nature, Development and Prospects (Stanford, CA: Stanford
University Press, 1990), ch. 7.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e909a1310">
            <label>42</label>
            <p>
               <mixed-citation id="d687e916" publication-type="other">
George Thomas and Pat Lauderdale, 'State Authority and National Welfare Programs in the World
System Context', Sociological Forum, 3 (1988), p. 388.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e927a1310">
            <label>43</label>
            <p>
               <mixed-citation id="d687e934" publication-type="other">
John Ruggie, 'International Regimes, Transactions and Change: Embedded Liberalism in the
Post-War Economic Order', in Stephen Krasner (ed.), International Regimes (Ithaca, NY: Cornell
University Press, 1983).</mixed-citation>
            </p>
         </fn>
         <fn id="d687e947a1310">
            <label>44</label>
            <p>
               <mixed-citation id="d687e954" publication-type="other">
Onuf, The Republican Legacy in International Thought, pp. 156-8.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e961a1310">
            <label>45</label>
            <p>
               <mixed-citation id="d687e968" publication-type="other">
Jackson, Quasi-States.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e975a1310">
            <label>46</label>
            <p>
               <mixed-citation id="d687e982" publication-type="other">
David Lumsdaine, The Moral Vision in International Politics: The Foreign Aid Regime, 1949-1989
(Princeton, NJ: Princeton University Press, 1993), ch. 2.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e992a1310">
            <label>47</label>
            <p>
               <mixed-citation id="d687e999" publication-type="other">
Eugene Black, The Diplomacy of Economic Development and Other Papers (Clinton, MA: Colonial
Press, 1963), p. 24.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e1009a1310">
            <label>48</label>
            <p>
               <mixed-citation id="d687e1016" publication-type="other">
Warren Baum, 'The Project Cycle', Finance and Development, 1 (1970), pp. 2-13.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e1024a1310">
            <label>49</label>
            <p>
               <mixed-citation id="d687e1031" publication-type="other">
Jackson, Quasi-States, ch. 5.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e1038a1310">
            <label>50</label>
            <p>
               <mixed-citation id="d687e1045" publication-type="other">
Ibid., ch. 6</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d687e1051" publication-type="other">
Donnelly, 'The Social Construction of Human Rights', pp. 85-8.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e1058a1310">
            <label>51</label>
            <p>
               <mixed-citation id="d687e1065" publication-type="other">
Bettina Hurni, The Lending Policies of the World Bank in the 1970s:
Analysis and Evaluation (Boulder, CO: Westview, 1980).</mixed-citation>
            </p>
         </fn>
         <fn id="d687e1075a1310">
            <label>53</label>
            <p>
               <mixed-citation id="d687e1082" publication-type="other">
World Bank, World Development Report 1997 (Washington, DC: 1997), pp. 218-19.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e1089a1310">
            <label>54</label>
            <p>
               <mixed-citation id="d687e1096" publication-type="other">
Lee Howard, 'An Assessment of Donor Coordination and External Financial Mobilization for
Health, Population and Nutrition in Sub-Saharan Africa', Falls Church, VA, The Pragma
Corporation, 1989.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e1109a1310">
            <label>55</label>
            <p>
               <mixed-citation id="d687e1116" publication-type="other">
Stephen Lister and Mike Stevens, 'Aid Coordination and Management', internal World Bank paper,
22 April 1992, pp. 8, 10.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e1127a1310">
            <label>56</label>
            <p>
               <mixed-citation id="d687e1134" publication-type="other">
World Bank, World Development Report 1997, p. 246.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e1141a1310">
            <label>57</label>
            <p>
               <mixed-citation id="d687e1148" publication-type="other">
Anne Kreuger, 'Changing
Perspectives on Development Economics and World Bank Research', Development Policy Review, 4
(1986), pp. 195-210.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e1161a1310">
            <label>58</label>
            <p>
               <mixed-citation id="d687e1168" publication-type="other">
Francis Deng, 'Reconciling Sovereignty with Responsibility: A Basis for International Humanitarian
Action', in John Harbeson and Donald Rothchild (eds.), Africa in World Politics: Post-Cold War
Challenges (Boulder, CO: Westview, 1995), p. 298</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d687e1180" publication-type="other">
Barkin, 'The Evolution of the
Constitution of Sovereignty', p. 149.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e1190a1310">
            <label>59</label>
            <p>
               <mixed-citation id="d687e1197" publication-type="other">
Tom Farer (ed.), Beyond Sovereignty: Collectively Defending Democracy in
the Americas (Baltimore, MD: Johns Hopkins University Press, 1996).</mixed-citation>
            </p>
         </fn>
         <fn id="d687e1207a1310">
            <label>60</label>
            <p>
               <mixed-citation id="d687e1214" publication-type="other">
Hurni, The Lending Policies of the World Bank in the 1970s.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e1221a1310">
            <label>61</label>
            <p>
               <mixed-citation id="d687e1228" publication-type="other">
Paul Mosley, Jane Harrigan and John Toye, Aid and Power: The World Bank and Policy-based
Lending, vol. 1 (London: Routledge, 1991), pp. 27-32.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e1239a1310">
            <label>62</label>
            <p>
               <mixed-citation id="d687e1246" publication-type="other">
World Bank, Governance and Development (Washington, DC: World Bank, 1992)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d687e1252" publication-type="other">
IMF, Good
Governance: The IMF's Role (Washington, DC: IMF, 1997)</mixed-citation>
            </p>
         </fn>
         <fn id="d687e1262a1310">
            <label>63</label>
            <p>
               <mixed-citation id="d687e1269" publication-type="other">
Buvan Bhatnagar and Aubrey Williams (eds.), Participatory Development and the World Bank:
Potential Directions for Change (Washington, DC: World Bank, 1992).</mixed-citation>
            </p>
         </fn>
         <fn id="d687e1279a1310">
            <label>64</label>
            <p>
               <mixed-citation id="d687e1286" publication-type="other">
Samuel Paul and Arturo Israel (eds.), Nongovernmental Organizations and the World Bank:
Cooperation for Development (Washington, DC: World Bank, 1991).</mixed-citation>
            </p>
         </fn>
         <fn id="d687e1296a1310">
            <label>65</label>
            <p>
               <mixed-citation id="d687e1303" publication-type="other">
Cheryl Gray, Lynn Khadiagala and Richard Moore, Institutional
Development Work at the World Bank: A Review of 84 Bank Projects (Washington, DC: World Bank,
1990).</mixed-citation>
            </p>
         </fn>
         <fn id="d687e1316a1310">
            <label>66</label>
            <p>
               <mixed-citation id="d687e1323" publication-type="other">
World Bank, Sub-Saharan Africa: From Crisis to Sustainable Growth (Washington, DC: World Bank,
1989), pp. 167-8</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d687e1332" publication-type="other">
World Bank, World Development Report 1991 (Washington, DC: World Bank,
1991), pp. 141-3.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e1342a1310">
            <label>67</label>
            <p>
               <mixed-citation id="d687e1349" publication-type="other">
World Bank, World Development Report 1991, p. 133.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e1357a1310">
            <label>68</label>
            <p>
               <mixed-citation id="d687e1364" publication-type="other">
World Bank, Assessing Development Effectiveness: Evaluation in the World Bank and the International
Finance Corporation, (Washington, DC: World Bank, 1994), p. 33.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e1374a1310">
            <label>69</label>
            <p>
               <mixed-citation id="d687e1381" publication-type="other">
David Plank, 'Aid, Debt and the End of Sovereignty: Mozambique and its
Donors', The Journal of Modern African Studies, 31 (1993), pp. 407-30.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e1391a1310">
            <label>70</label>
            <p>
               <mixed-citation id="d687e1398" publication-type="other">
Staff Appraisal Report, 'Republic of Ghana: Local Government Development Project', report
number 12332-GH, January 1994.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e1408a1310">
            <label>71</label>
            <p>
               <mixed-citation id="d687e1415" publication-type="other">
Richard Crook, 'Four Years of the Ghana District Assemblies in Operation: Decentralisation,
Democratisation, and Administrative Performance', Public Administration and Development, 14
(1994), pp. 339-64.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e1428a1310">
            <label>72</label>
            <p>
               <mixed-citation id="d687e1435" publication-type="other">
West Africa Department, 'Ghana—Strengthening Local Initiative and Building Local Capacity',
report number 11369-GH, February 1993</mixed-citation>
            </p>
         </fn>
         <fn id="d687e1445a1310">
            <label>73</label>
            <p>
               <mixed-citation id="d687e1452" publication-type="other">
'Local Government Development Project', pp. 110-11. This level of detail is entirely typical.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e1460a1310">
            <label>74</label>
            <p>
               <mixed-citation id="d687e1467" publication-type="other">
David Williams, 'Governance and the Discipline of Development', European Journal of Development
Research, 8 (1996), pp. 157-77.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e1477a1310">
            <label>75</label>
            <p>
               <mixed-citation id="d687e1484" publication-type="other">
Mosley et al., Aid and Power, vol. 1, p. 136</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d687e1490" publication-type="other">
Christopher Clapham, Africa and the International
System: The Politics of State Survival (Cambridge: Cambridge University Press, 1996), pp. 180-1,
pp. 201-7.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e1503a1310">
            <label>76</label>
            <p>
               <mixed-citation id="d687e1510" publication-type="other">
Clapham, Africa and the International System,
pt. III.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e1520a1310">
            <label>77</label>
            <p>
               <mixed-citation id="d687e1527" publication-type="other">
Lister and Stevens,' Aid Coordination and Management', p. 18.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e1534a1310">
            <label>78</label>
            <p>
               <mixed-citation id="d687e1541" publication-type="other">
R. Feinberg, 'The Changing Relationship Between the World Bank and the IMF', International
Organization, 42 (1988), pp. 545-60.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e1551a1310">
            <label>79</label>
            <p>
               <mixed-citation id="d687e1558" publication-type="other">
OECD, Aid Coordination and Aid Effectiveness: A Review of Country and Regional Experience (Paris:
OECD, 1988), pp. 13-29.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e1569a1310">
            <label>80</label>
            <p>
               <mixed-citation id="d687e1576" publication-type="other">
OECD, Aid Coordination and Aid
Effectiveness, p. 21.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e1586a1310">
            <label>81</label>
            <p>
               <mixed-citation id="d687e1593" publication-type="other">
Devendra Ray Panday and Maurice Williams, 'Aid Coordination and Effectiveness: Least Developed
Countries 1981-1989', study prepared for the Second United Nations Conference on the Least
Developed Countries, March 1990, p. 120.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e1606a1310">
            <label>82</label>
            <p>
               <mixed-citation id="d687e1613" publication-type="other">
OECD, Aid Coordination and Aid Effectiveness, p. 42.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e1620a1310">
            <label>83</label>
            <p>
               <mixed-citation id="d687e1627" publication-type="other">
Goran Hyden, No Shortcuts to Progress: African Development Management in Perspective (Berkeley,
CA: University of California Press, 1983)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d687e1636" publication-type="other">
Sudipta Kaviraj, 'On State, Society and Discourse in
India', in James Manor (ed.), Rethinking Third World Politics (London: Longman, 1991).</mixed-citation>
            </p>
         </fn>
         <fn id="d687e1646a1310">
            <label>84</label>
            <p>
               <mixed-citation id="d687e1653" publication-type="other">
Donal B. Cruise O'Brien, 'The Show of State in a Neo-Colonial Twilight: Francophone Africa', in
Manor (ed.), Rethinking Third World Politics, p. 146.</mixed-citation>
            </p>
         </fn>
         <fn id="d687e1663a1310">
            <label>85</label>
            <p>
               <mixed-citation id="d687e1670" publication-type="other">
Onuf, The Republican Legacy in International Thought, pp. 153-62.</mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
