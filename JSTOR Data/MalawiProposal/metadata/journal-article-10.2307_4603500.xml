<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">humanecology</journal-id>
         <journal-id journal-id-type="jstor">j101456</journal-id>
         <journal-title-group>
            <journal-title>Human Ecology</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Kluwer Academic/Plenum Publishers</publisher-name>
         </publisher>
         <issn pub-type="ppub">03007839</issn>
         <issn pub-type="epub">15729915</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">4603500</article-id>
         <title-group>
            <article-title>Families and Firewood: A Comparative Analysis of the Costs and Benefits of Children in Firewood Collection and Use in Two Rural Communities in Sub-Saharan Africa</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>Adam Biran</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>Joanne Abbot</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Ruth</given-names>
                  <surname>Mace</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>2</month>
            <year>2004</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">32</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id>i411994</issue-id>
         <fpage>1</fpage>
         <lpage>25</lpage>
         <page-range>1-25</page-range>
         <permissions>
            <copyright-statement>Copyright 2004 Plenum Publishing Corporation</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/4603500"/>
         <abstract>
            <p>Domestic firewood collection is compared across study sites in Malawi and Tanzania. The study focuses on accommodation of infant-care within wood collection, the influence of family size on firewood demand and the contribution of girls to firewood collection. Malawian women carry their infants on wood collection trips. The Tanzanian women leave their infants behind. The shorter trips of the Tanzanian women, and the ready availability of alloparental care may facilitate this. Mean per capita wood consumption was similar across the two sites. However, a marked economy of scale was evident in wood use at the Malawi site. Girls at both sites assist with wood collection. Their contribution appears more than sufficient to compensate for their own wood use. Having a daughter therefore need not represent a net energetic cost to a woman in terms of firewood acquisition and consumption. While family size and structure appear to influence firewood consumption and acquisition, differences in the environment between the two sites may underpin much of the variation. The longer journey times, heavier load, and less frequent journeys undertaken by Malawian women may reflect the steep terrain and risks associated with firewood collection within a national park.</p>
         </abstract>
         <kwd-group>
            <kwd>children's work</kwd>
            <kwd>family size</kwd>
            <kwd>child-care</kwd>
            <kwd>firewood</kwd>
            <kwd>Maasai</kwd>
            <kwd>Tanzania</kwd>
            <kwd>Malawi</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d546e157a1310">
            <mixed-citation id="d546e161" publication-type="other">
Abbot, J. I. O. (1996). Rural Subsistence and Protected Areas: Community Use of the Miombo
Woodlands of Lake Malawi National Park, Unpublished PhD Thesis, University College,
University of London.</mixed-citation>
         </ref>
         <ref id="d546e174a1310">
            <mixed-citation id="d546e178" publication-type="journal">
Abbot, J. I. O., and Homewood, K. (1999). A history of change: Causes of miombo woodland
decline in a protected area in Malawi. Journal of Applied Ecology36: 422-433.<object-id pub-id-type="jstor">10.2307/2655898</object-id>
               <fpage>422</fpage>
            </mixed-citation>
         </ref>
         <ref id="d546e194a1310">
            <mixed-citation id="d546e198" publication-type="journal">
Abbot, J. I. O., and Mace, R. (1999). Managing protected woodlands: Fuelwood collection and
law enforcement in lake Malawi National Park. Conservation Biology13(2): 418-421.<object-id pub-id-type="jstor">10.2307/2641484</object-id>
               <fpage>418</fpage>
            </mixed-citation>
         </ref>
         <ref id="d546e214a1310">
            <mixed-citation id="d546e218" publication-type="journal">
Altmann, J. (1974). Observational study of behavior: Sampling methods. BehaviorXLIX: 227-
265.<person-group>
                  <string-name>
                     <surname>Altmann</surname>
                  </string-name>
               </person-group>
               <fpage>227</fpage>
               <volume>XLIX</volume>
               <source>Behavior</source>
               <year>1974</year>
            </mixed-citation>
         </ref>
         <ref id="d546e251a1310">
            <mixed-citation id="d546e255" publication-type="book">
Barnes, C., Ensminger, J., and O'Keefe, P. (1984). Wood Energy and Households: Perspectives
on Rural Kenya, Beijer Institute and Scandinavian Institute of African Studies, Uppsala,
Sweden.<person-group>
                  <string-name>
                     <surname>Barnes</surname>
                  </string-name>
               </person-group>
               <source>Wood Energy and Households: Perspectives on Rural Kenya</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d546e284a1310">
            <mixed-citation id="d546e288" publication-type="book">
Becker, G. S. (1991). A Treatise on the Family, Harvard University Press, Cambridge.<person-group>
                  <string-name>
                     <surname>Becker</surname>
                  </string-name>
               </person-group>
               <source>A Treatise on the Family</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d546e310a1310">
            <mixed-citation id="d546e314" publication-type="journal">
Belanger, D. (2002). Son preference in a rural village in North Vietnam. Studies in Family
Planning33(4): 321-334.<object-id pub-id-type="jstor">10.2307/3181094</object-id>
               <fpage>321</fpage>
            </mixed-citation>
         </ref>
         <ref id="d546e330a1310">
            <mixed-citation id="d546e334" publication-type="journal">
Berio, A. (1984). The analysis of time allocation and activity patterns in nutrition and rural
development planning. Food and Nutrition Bulletin6(4): 53-68.<person-group>
                  <string-name>
                     <surname>Berio</surname>
                  </string-name>
               </person-group>
               <issue>4</issue>
               <fpage>53</fpage>
               <volume>6</volume>
               <source>Food and Nutrition Bulletin</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d546e369a1310">
            <mixed-citation id="d546e373" publication-type="book">
Berry, V., and Petty, C. (eds.) (1992). The Nyasaland Survey Papers 1938-1943 Agriculture, Food
and Health, Academy Books Limited, London.<person-group>
                  <string-name>
                     <surname>Berry</surname>
                  </string-name>
               </person-group>
               <source>The Nyasaland Survey Papers 1938-1943 Agriculture, Food and Health</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d546e398a1310">
            <mixed-citation id="d546e402" publication-type="other">
Biran, A. (1996). Time Allocation Among Maasai Women: An Evolutionary Approach, Unpub-
lished PhD thesis, University College, University of London.</mixed-citation>
         </ref>
         <ref id="d546e413a1310">
            <mixed-citation id="d546e417" publication-type="book">
Blurton Jones, N. (1972). Comparative aspects of mother-child contact. In Blurton Jones, N.
(eds.), Ethological Studies on Child Behavior, Cambridge University Press, Cambridge.<person-group>
                  <string-name>
                     <surname>Blurton Jones</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Comparative aspects of mother-child contact</comment>
               <source>Ethological Studies on Child Behavior</source>
               <year>1972</year>
            </mixed-citation>
         </ref>
         <ref id="d546e446a1310">
            <mixed-citation id="d546e450" publication-type="book">
Blurton Jones, N. G., Hawkes, K., and O'Connell, J. F (1989). Modelling and measuring costs
of children in two foraging societies. In Standen, V., and Foley, R. A. (eds.), Compara-
tive Socioecology: The Behavioural Ecology of Humans and Other Mammals, Blackwell
Scientific, Oxford.<person-group>
                  <string-name>
                     <surname>Blurton Jones</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Modelling and measuring costs of children in two foraging societies</comment>
               <source>Comparative Socioecology: The Behavioural Ecology of Humans and Other Mammals</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d546e485a1310">
            <mixed-citation id="d546e489" publication-type="book">
Blurton Jones, N. G., and Sibly, R. M. (1978). Testing adaptiveness of culturally determined
behaviour: Do bushman women maximise their reproductive success by spacing births
widely and foraging seldom. In Blurton Jones, N. G., and Reynolds, V. (eds.), Human
Behavior and Adaptation, Taylor and Francis, London.<person-group>
                  <string-name>
                     <surname>Blurton Jones</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Testing adaptiveness of culturally determined behaviour: Do bushman women maximise their reproductive success by spacing births widely and foraging seldom</comment>
               <source>Human Behavior and Adaptation</source>
               <year>1978</year>
            </mixed-citation>
         </ref>
         <ref id="d546e524a1310">
            <mixed-citation id="d546e528" publication-type="journal">
Borgerhoff Mulder, M., and Caro, T. M. (1985). The use of quantitative observational methods
in anthropology. Current Anthropology26(3): 323-332.<object-id pub-id-type="jstor">10.2307/2742731</object-id>
               <fpage>323</fpage>
            </mixed-citation>
         </ref>
         <ref id="d546e544a1310">
            <mixed-citation id="d546e548" publication-type="journal">
Borgerhoff Mulder, M., and Milton, M. (1985). Factors affecting infant care among the Kipsigis.
Journal of Anthropological Research41(3): 231-262.<object-id pub-id-type="jstor">10.2307/3630593</object-id>
               <fpage>231</fpage>
            </mixed-citation>
         </ref>
         <ref id="d546e564a1310">
            <mixed-citation id="d546e568" publication-type="book">
Boserup, E. (1989). Woman's Role in Economic Development, Earthscan Publications Limited,
London.<person-group>
                  <string-name>
                     <surname>Boserup</surname>
                  </string-name>
               </person-group>
               <source>Woman's Role in Economic Development</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d546e594a1310">
            <mixed-citation id="d546e598" publication-type="journal">
Brouwer, I. D., Nederveen, L. M., den Hartog, A. P., and Vlasveld, A. H. C. (1989). Nutritional
impacts of an increasing firewood shortage in rural households in developing countries.
Progress in Food and Nutrition Science13: 349-361.<person-group>
                  <string-name>
                     <surname>Brouwer</surname>
                  </string-name>
               </person-group>
               <fpage>349</fpage>
               <volume>13</volume>
               <source>Progress in Food and Nutrition Science</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d546e633a1310">
            <mixed-citation id="d546e637" publication-type="journal">
Bryceson, D. F, and Howe, J. (1993). Rural household transport in Africa: Reducing the burden
on women? World Development21(11): 1715-1728.<person-group>
                  <string-name>
                     <surname>Bryceson</surname>
                  </string-name>
               </person-group>
               <issue>11</issue>
               <fpage>1715</fpage>
               <volume>21</volume>
               <source>World Development</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d546e672a1310">
            <mixed-citation id="d546e676" publication-type="journal">
Caldwell, J. C. (1977). The economic rationality of high fertility: An investigation illustrated
with Nigerian Survey data. Population Studies31: 5-27.<object-id pub-id-type="doi">10.2307/2173485</object-id>
               <fpage>5</fpage>
            </mixed-citation>
         </ref>
         <ref id="d546e692a1310">
            <mixed-citation id="d546e696" publication-type="book">
Caldwell, J. C. (1983). Direct economic costs and benefits of children. In Bulatao, R. A., and
Lee, R. 0. (eds.), Determinants of Fertility in Developing Countries. Vol. 1. Supply and
Demand for Children, Academic Press, New York.<person-group>
                  <string-name>
                     <surname>Caldwell</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Direct economic costs and benefits of children</comment>
               <source>Determinants of Fertility in Developing Countries. Vol. 1. Supply and Demand for Children</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d546e728a1310">
            <mixed-citation id="d546e732" publication-type="journal">
Dunbar, R. I. M. (1976). Some aspects of research design and their implications in the obser-
vational study of behaviour. Behavior58: 78-98.<person-group>
                  <string-name>
                     <surname>Dunbar</surname>
                  </string-name>
               </person-group>
               <fpage>78</fpage>
               <volume>58</volume>
               <source>Behavior</source>
               <year>1976</year>
            </mixed-citation>
         </ref>
         <ref id="d546e764a1310">
            <mixed-citation id="d546e768" publication-type="book">
Dyson-Hudson, N. (1980). Strategies of resource exploitation among East African pastoralists.
In Harris, D. (ed.), Human Ecology in Savanna Environments, Academic Press, London.<person-group>
                  <string-name>
                     <surname>Dyson-Hudson</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Strategies of resource exploitation among East African pastoralists</comment>
               <source>Human Ecology in Savanna Environments</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
         <ref id="d546e798a1310">
            <mixed-citation id="d546e802" publication-type="journal">
Easterlin, R. A. (1975). An economic framework for fertility analysis. Studies in Family Planning
6: 54-63<object-id pub-id-type="doi">10.2307/1964934</object-id>
               <fpage>54</fpage>
            </mixed-citation>
         </ref>
         <ref id="d546e818a1310">
            <mixed-citation id="d546e822" publication-type="book">
Easterlin R. A. (ed.) (1990). Population and Economic Change in Developing Countries,
Chicago University Press, Chicago.<person-group>
                  <string-name>
                     <surname>Easterlin</surname>
                  </string-name>
               </person-group>
               <source>Population and Economic Change in Developing Countries</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d546e847a1310">
            <mixed-citation id="d546e851" publication-type="book">
Ellis, F (1988). Peasant Economies, Cambridge University Press, Cambridge.<person-group>
                  <string-name>
                     <surname>Ellis</surname>
                  </string-name>
               </person-group>
               <source>Peasant Economies</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d546e873a1310">
            <mixed-citation id="d546e877" publication-type="journal">
Fleuret, P. C., and Fleuret, A. K. (1978). Firewood Use in a Peasant Community: A Tanzanian
Case Study. The Journal of Developing Areas12: 315-322.<person-group>
                  <string-name>
                     <surname>Fleuret</surname>
                  </string-name>
               </person-group>
               <fpage>315</fpage>
               <volume>12</volume>
               <source>The Journal of Developing Areas</source>
               <year>1978</year>
            </mixed-citation>
         </ref>
         <ref id="d546e909a1310">
            <mixed-citation id="d546e913" publication-type="book">
Galaty, J. G. (1994). Rangeland tenure and pastoralism in Africa. In Fratkin, E., Galvin, K.
A., and Roth, E. A. (eds.), African Pastoralist Systems: An Integrated Approach, Lynne
Rienner Publishers, Boulder, CO.<person-group>
                  <string-name>
                     <surname>Galaty</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Rangeland tenure and pastoralism in Africa</comment>
               <source>African Pastoralist Systems: An Integrated Approach</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d546e945a1310">
            <mixed-citation id="d546e949" publication-type="journal">
Gross, D. R. (1984). Time allocation: A tool for the study of cultural behaviour. Annual Review
of Anthropology13: 519-558.<object-id pub-id-type="jstor">10.2307/2155680</object-id>
               <fpage>519</fpage>
            </mixed-citation>
         </ref>
         <ref id="d546e966a1310">
            <mixed-citation id="d546e970" publication-type="book">
Hames, R. (1988). The allocation of parental care among the Ye'kwana. In Betzig, L., Borgerhoff
Mulder, M., and Turke, P. (eds.), Human Reproductive Behavior, A Darwinian Perspective,
Cambridge University Press, Cambridge.<person-group>
                  <string-name>
                     <surname>Hames</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The allocation of parental care among the Ye'kwana</comment>
               <source>Human Reproductive Behavior, A Darwinian Perspective</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d546e1002a1310">
            <mixed-citation id="d546e1006" publication-type="book">
Homewood, K. M., and Rodgers, W. A. (1991). Maasailand Ecology: Pastoralist Develop-
ment and Wildlife Conservation in Ngorongoro, Tanzania, Cambridge University Press,
Cambridge.<person-group>
                  <string-name>
                     <surname>Homewood</surname>
                  </string-name>
               </person-group>
               <source>Maasailand Ecology: Pastoralist Development and Wildlife Conservation in Ngorongoro, Tanzania</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d546e1035a1310">
            <mixed-citation id="d546e1039" publication-type="book">
Hosier, R. (1984). Domestic energy consumption in rural Kenya: Results of a nationwide survey.
In Barnes, C., Ensminger, J., and Keefe, P. O. (eds.), Energy, Environment and Development
in Africa 6: Wood, Energy and Households, Perspectives on Rural Kenya, Beijer Institute,
Stockholm, Sweden.<person-group>
                  <string-name>
                     <surname>Hosier</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Domestic energy consumption in rural Kenya: Results of a nationwide survey</comment>
               <source>Energy, Environment and Development in Africa 6: Wood, Energy and Households, Perspectives on Rural Kenya</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d546e1074a1310">
            <mixed-citation id="d546e1078" publication-type="journal">
Hurtado, A. M., Hawkes, K., Hill, K., and Kaplan, H. (1985). Female subsistence strategies
among ache hunter-gatherers of Eastern Paraguay. Human Ecology13(1): 1-28.<person-group>
                  <string-name>
                     <surname>Hurtado</surname>
                  </string-name>
               </person-group>
               <issue>1</issue>
               <fpage>1</fpage>
               <volume>13</volume>
               <source>Human Ecology</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d546e1113a1310">
            <mixed-citation id="d546e1117" publication-type="journal">
Hurtado, A. M., Hill, K., Kaplan, H., and Hurtado, I. (1992). Trade-Offs between female food
acquisition and child care among Hiwi and Ache Foragers. Human Nature: An Interdisci-
plinary Biosocial Perspective3(3): 185-216.<person-group>
                  <string-name>
                     <surname>Hurtado</surname>
                  </string-name>
               </person-group>
               <issue>3</issue>
               <fpage>185</fpage>
               <volume>3</volume>
               <source>Human Nature: An Interdisciplinary Biosocial Perspective</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d546e1155a1310">
            <mixed-citation id="d546e1159" publication-type="journal">
Kaplan, H. (1994). Evolutionary and wealth flows theories of fertility: Empirical tests and new
models. Population and Development Review20(4): 753-791.<object-id pub-id-type="doi">10.2307/2137661</object-id>
               <fpage>753</fpage>
            </mixed-citation>
         </ref>
         <ref id="d546e1176a1310">
            <mixed-citation id="d546e1180" publication-type="journal">
Kramer, K. L. (2002). Variation in juvenile dependence: Helping behavior among maya children.
Human nature13(2): 299-325.<person-group>
                  <string-name>
                     <surname>Kramer</surname>
                  </string-name>
               </person-group>
               <issue>2</issue>
               <fpage>299</fpage>
               <volume>13</volume>
               <source>Human nature</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d546e1215a1310">
            <mixed-citation id="d546e1219" publication-type="book">
Lee, P. C. (1989). Family structure, communal care and female reproductive effort. In Standen,
V., and Foley, R. A. (eds.), Comparative Socioecology: The Behavioural Ecology of Humans
and Other Mammals. Blackwell Scientific, Oxford.<person-group>
                  <string-name>
                     <surname>Lee</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Family structure, communal care and female reproductive effort</comment>
               <source>Comparative Socioecology: The Behavioural Ecology of Humans and Other Mammals</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d546e1251a1310">
            <mixed-citation id="d546e1255" publication-type="journal">
Levine, N. E. (1988). Women's work and infant feeding: A case from rural Nepal. Ethnology
28(3): 231-251.<object-id pub-id-type="doi">10.2307/3773519</object-id>
               <fpage>231</fpage>
            </mixed-citation>
         </ref>
         <ref id="d546e1271a1310">
            <mixed-citation id="d546e1275" publication-type="journal">
Mace, R., and Sear R. (1997). The birth interval and sex of children in a traditional African
population: An evolutionary analysis. Journal of Biosocial Science29: 499-507.<person-group>
                  <string-name>
                     <surname>Mace</surname>
                  </string-name>
               </person-group>
               <fpage>499</fpage>
               <volume>29</volume>
               <source>Journal of Biosocial Science</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d546e1307a1310">
            <mixed-citation id="d546e1311" publication-type="book">
Martin, P., and Bateson, P. (1986). Measuring Behavior: An Introductory Guide. Cambridge
University Press, Cambridge.<person-group>
                  <string-name>
                     <surname>Martin</surname>
                  </string-name>
               </person-group>
               <source>Measuring Behavior: An Introductory Guide</source>
               <year>1986</year>
            </mixed-citation>
         </ref>
         <ref id="d546e1336a1310">
            <mixed-citation id="d546e1340" publication-type="journal">
Mehretu, A., and Mutambirwa, C. (1992). Gender differences in time and energy costs of
distance for regular domestic chores in rural Zimbabwe: A case study in the Chiduku
Communal Area. World Development20(11): 1675-1683.<person-group>
                  <string-name>
                     <surname>Mehretu</surname>
                  </string-name>
               </person-group>
               <issue>11</issue>
               <fpage>1675</fpage>
               <volume>20</volume>
               <source>World Development</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d546e1379a1310">
            <mixed-citation id="d546e1383" publication-type="journal">
Mwageni, E. A., Ankomah, A., and Powell, R. A. (2001). Sex preference and contraceptive
behaviour among men in Mbeya region, Tanzania. Journal of Family Planning and Repro-
ductive Health Care27(2): 85-89.<person-group>
                  <string-name>
                     <surname>Mwageni</surname>
                  </string-name>
               </person-group>
               <issue>2</issue>
               <fpage>85</fpage>
               <volume>27</volume>
               <source>Journal of Family Planning and Reproductive Health Care</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d546e1421a1310">
            <mixed-citation id="d546e1425" publication-type="journal">
Nag, M., White, B. N. F, and Peet, R. C. (1978). An anthropological approach to the study of
the economic value of children in Java and Nepal. Current Anthropology19(2): 293-306.<object-id pub-id-type="jstor">10.2307/2741995</object-id>
               <fpage>293</fpage>
            </mixed-citation>
         </ref>
         <ref id="d546e1441a1310">
            <mixed-citation id="d546e1445" publication-type="book">
Oppenheim, A. N. (1992). Questionnaire Design, Interviewing, and Attitude Measurement, Pin-
ter Publishers, London.<person-group>
                  <string-name>
                     <surname>Oppenheim</surname>
                  </string-name>
               </person-group>
               <source>Questionnaire Design, Interviewing, and Attitude Measurement</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d546e1470a1310">
            <mixed-citation id="d546e1474" publication-type="journal">
Panter-Brick, C. (1989). Motherhood and subsistence work: The Tamang of rural Nepal. Human
Ecology17(2): 205-228.<person-group>
                  <string-name>
                     <surname>Panter-Brick</surname>
                  </string-name>
               </person-group>
               <issue>2</issue>
               <fpage>205</fpage>
               <volume>17</volume>
               <source>Human Ecology</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d546e1509a1310">
            <mixed-citation id="d546e1513" publication-type="journal">
Popkin, B. M. (1980). Time allocation of the mother and child nutrition. Journal of Food and
Nutrition9: 1-14.<person-group>
                  <string-name>
                     <surname>Popkin</surname>
                  </string-name>
               </person-group>
               <fpage>1</fpage>
               <volume>9</volume>
               <source>Journal of Food and Nutrition</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
         <ref id="d546e1545a1310">
            <mixed-citation id="d546e1549" publication-type="journal">
Rahman, M., and DaVanzo, J. (1993). Gender preference and birth spacing in Matlab,
Bangladesh. Demography30(3): 315-332.<object-id pub-id-type="doi">10.2307/2061643</object-id>
               <fpage>315</fpage>
            </mixed-citation>
         </ref>
         <ref id="d546e1566a1310">
            <mixed-citation id="d546e1570" publication-type="book">
Saitoti, T. 0. (1986). The Worlds of a Maasai Warrior, Random House, New York.<person-group>
                  <string-name>
                     <surname>Saitoti</surname>
                  </string-name>
               </person-group>
               <source>The Worlds of a Maasai Warrior</source>
               <year>1986</year>
            </mixed-citation>
         </ref>
         <ref id="d546e1592a1310">
            <mixed-citation id="d546e1596" publication-type="journal">
Shackleton, C. M. (1993). Fuelwood harvesting and sustainable utilization in a communal graz-
ing land and protected area of the Eastern Transvaal Lowveld. Biological Conservation63:
247-254.<person-group>
                  <string-name>
                     <surname>Shackleton</surname>
                  </string-name>
               </person-group>
               <fpage>247</fpage>
               <volume>63</volume>
               <source>Biological Conservation</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d546e1631a1310">
            <mixed-citation id="d546e1635" publication-type="book">
Turke, P. W. (1988). Helpers at the nest: childcare networks on Ifaluk. In Betzig, L., Borgerhoff
Mulder, M., and Turke, P. (eds.), Human Reproductive Behavior, A Darwinian Perspective,
Cambridge University Press, Cambridge.<person-group>
                  <string-name>
                     <surname>Turke</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Helpers at the nest: childcare networks on Ifaluk</comment>
               <source>Human Reproductive Behavior, A Darwinian Perspective</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d546e1667a1310">
            <mixed-citation id="d546e1671" publication-type="journal">
Ulijaszek, S. J. (1993). Influence of birth interval and child labor on family energy require-
ments and dependency ratois in two traditional subsistence economies in Africa. Journal
of Biosocial Science25: 79-86.<person-group>
                  <string-name>
                     <surname>Ulijaszek</surname>
                  </string-name>
               </person-group>
               <fpage>79</fpage>
               <volume>25</volume>
               <source>Journal of Biosocial Science</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
