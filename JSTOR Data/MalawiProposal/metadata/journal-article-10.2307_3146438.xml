<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">landeconomics</journal-id>
         <journal-id journal-id-type="jstor">j100261</journal-id>
         <journal-title-group>
            <journal-title>Land Economics</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Wisconsin Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">00237639</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="doi">10.2307/3146438</article-id>
         <title-group>
            <article-title>Technical Change in Agriculture and Land Degradation in Developing Countries: A General Equilibrium Analysis</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Ian</given-names>
                  <surname>Coxhead</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Sisira</given-names>
                  <surname>Jayasuriya</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>2</month>
            <year>1994</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">70</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id>i357985</issue-id>
         <fpage>20</fpage>
         <lpage>37</lpage>
         <page-range>20-37</page-range>
         <permissions>
            <copyright-statement>Copyright 1994 Board of Regents of the University of Wisconsin System</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/3146438"/>
         <abstract>
            <p>This paper analyzes the role of economic linkages between upland agriculture, lowland agriculture and other sectors in developing economies, and the potential for welfare-enhancing shifts from more to less erosive upland land use patterns. Comparative statics results are obtained from analytical and numerical general equilibrium models. They indicate that the green revolution in lowland agriculture helped alleviate upland land degradation, and that policies aimed at slowing land degradation through technical progress in upland crops may have the opposite of their intended effects. The results highlight the need for integrated policy packages to reduce upland land degradation in developing countries.</p>
         </abstract>
         <kwd-group>
            <kwd>Q24</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d155e184a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d155e191" publication-type="book">
Barbier
1991)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d155e202" publication-type="book">
World Bank
1990)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d155e213" publication-type="book">
World Bank 1989)  </mixed-citation>
            </p>
         </fn>
         <fn id="d155e222a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d155e229" publication-type="book">
Coxhead and Jayasuriya
1993)  </mixed-citation>
            </p>
         </fn>
         <fn id="d155e241a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d155e248" publication-type="journal">
Bar-
rett 1991  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d155e259" publication-type="book">
Clarke 1991  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d155e267" publication-type="journal">
Lipton 1987  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d155e276" publication-type="book">
LaFrance 1990  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d155e284" publication-type="journal">
McConnell 1983  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d155e292" publication-type="book">
Repetto 1989  </mixed-citation>
            </p>
         </fn>
         <fn id="d155e301a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d155e308" publication-type="book">
Aminuddin, Chow, and Ng
1991  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d155e319" publication-type="book">
Gregerson, Draper and Elz 1989  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d155e327" publication-type="book">
Repetto 1989  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d155e336" publication-type="book">
Delos Angeles 1991  </mixed-citation>
            </p>
         </fn>
         <fn id="d155e346a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d155e353" publication-type="book">
Askari and Cummings 1976  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d155e361" publication-type="journal">
Bale and Lutz 1981  </mixed-citation>
            </p>
         </fn>
         <fn id="d155e370a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d155e377" publication-type="journal">
Coxhead and Warr (1991)  </mixed-citation>
            </p>
         </fn>
         <fn id="d155e386a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d155e393" publication-type="book">
Coxhead 1992  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d155e401" publication-type="journal">
Coxhead and Warr 1991  </mixed-citation>
            </p>
         </fn>
         <fn id="d155e410a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d155e417" publication-type="journal">
Dornbusch 1974  </mixed-citation>
            </p>
         </fn>
         <fn id="d155e426a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d155e433" publication-type="book">
Codsi and Pearson 1988  </mixed-citation>
            </p>
         </fn>
         <fn id="d155e442a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d155e449" publication-type="book">
Pagan and Shannon 1985  </mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d155e467a1310">
            <mixed-citation id="d155e471" publication-type="book">
Aminuddin, B. Y., W. T. Chow, and T. T. Ng.
1991. "Resources and Problems Associated
with Sustainable Development of Upland Ar-
eas in Malaysia." In Technologies for Sus-
tainable Agriculture on Marginal Uplands in
Southeast Asia, eds. Graeme Blair and Rod
Lefroy. Canberra: Australian Centre for In-
ternational Agricultural Research.<person-group>
                  <string-name>
                     <surname>Aminuddin</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Resources and Problems Associated with Sustainable Development of Upland Areas in Malaysia</comment>
               <source>Technologies for Sustainable Agriculture on Marginal Uplands in Southeast Asia</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d155e519a1310">
            <mixed-citation id="d155e523" publication-type="book">
Askari, H., and T. J. Cummings. 1976. Agricul-
tural Supply Response: A Survey of Econo-
metric Evidence. New York: Praeger.<person-group>
                  <string-name>
                     <surname>Askari</surname>
                  </string-name>
               </person-group>
               <source>Agricultural Supply Response: A Survey of Econometric Evidence</source>
               <year>1976</year>
            </mixed-citation>
         </ref>
         <ref id="d155e552a1310">
            <mixed-citation id="d155e556" publication-type="journal">
Bale, Malcolm D., and Ernst Lutz. 1981. "Price
Distortions in Agriculture and Their Effects:
An International Comparison." American
Journal of Agricultural Economics 63
(Feb.):8-22.<object-id pub-id-type="doi">10.2307/1239807</object-id>
               <fpage>8</fpage>
            </mixed-citation>
         </ref>
         <ref id="d155e582a1310">
            <mixed-citation id="d155e586" publication-type="book">
Barbier, Edward B. 1991. "Environmental Man-
agement and Development in the South: Pre-
requisites for Sustainable Development."
London: London Environmental Economics
Centre. Mimeo.<person-group>
                  <string-name>
                     <surname>Barbier</surname>
                  </string-name>
               </person-group>
               <source>Environmental Management and Development in the South: Prerequisites for Sustainable Development</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d155e622a1310">
            <mixed-citation id="d155e626" publication-type="book">
Barbier, Edward B. 1990. "Sustainable Devel-
opment in the Upper Watersheds of Java."
In Sustainable Development: Economics and
Environment in the Third World, eds. D.
Pearce, E. Barbier, and A. Markandya. Lon-
don: Edward Elgar.<person-group>
                  <string-name>
                     <surname>Barbier</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Sustainable Development in the Upper Watersheds of Java</comment>
               <source>Sustainable Development: Economics and Environment in the Third World</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d155e667a1310">
            <mixed-citation id="d155e671" publication-type="book">
Barlow, Colin. 1978. The Natural Rubber Indus-
try: Its Development, Technology, and Econ-
omy in Malaysia. Kuala Lumpur, New York:
Oxford University Press.<person-group>
                  <string-name>
                     <surname>Barlow</surname>
                  </string-name>
               </person-group>
               <source>The Natural Rubber Industry: Its Development, Technology, and Economy in Malaysia</source>
               <year>1978</year>
            </mixed-citation>
         </ref>
         <ref id="d155e703a1310">
            <mixed-citation id="d155e707" publication-type="journal">
Barlow, Colin, and Sisira Jayasuriya. 1984.
"Problems of Investment for Technological
Advance. The Case of Indonesian Small-
holder Rubber." Journal of Agricultural Eco-
nomics 35 (1):85-95.<person-group>
                  <string-name>
                     <surname>Barlow</surname>
                  </string-name>
               </person-group>
               <issue>1</issue>
               <fpage>85</fpage>
               <volume>35</volume>
               <source>Journal of Agricultural Economics</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d155e751a1310">
            <mixed-citation id="d155e755" publication-type="journal">
Barrett, Scott. 1991. "Optimal Soil Conserva-
tion and the Reform of Agricultural Pricing
Policies." Journal of Development Econom-
ics 36 (Oct.): 167-87.<person-group>
                  <string-name>
                     <surname>Barrett</surname>
                  </string-name>
               </person-group>
               <issue>Oct.</issue>
               <fpage>167</fpage>
               <volume>36</volume>
               <source>Journal of Development Economics</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d155e796a1310">
            <mixed-citation id="d155e800" publication-type="book">
Blaikie, Piers. 1985. The Political Economy of
Soil Erosion in Developing Countries. Lon-
don and New York: Longman.<person-group>
                  <string-name>
                     <surname>Blaikie</surname>
                  </string-name>
               </person-group>
               <source>The Political Economy of Soil Erosion in Developing Countries</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d155e829a1310">
            <mixed-citation id="d155e833" publication-type="journal">
Cassing, James H., and Peter G. Warr. 1985.
"The Distributional Impact of a Resources
Boom." Journal of International Economics
18:301-20.<person-group>
                  <string-name>
                     <surname>Cassing</surname>
                  </string-name>
               </person-group>
               <fpage>301</fpage>
               <volume>18</volume>
               <source>Journal of International Economics</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d155e872a1310">
            <mixed-citation id="d155e876" publication-type="book">
Chisholm, Anthony, and Robert Dumsday.
1987. Land Degradation: Problems and
Policies. Cambridge: Cambridge University
Press.<person-group>
                  <string-name>
                     <surname>Chisholm</surname>
                  </string-name>
               </person-group>
               <source>Land Degradation: Problems and Policies</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d155e908a1310">
            <mixed-citation id="d155e912" publication-type="book">
Clarke, Harry R. 1991. "Land Degradation and
Prices." Economics and Commerce Discus-
sion Papers No. 14/91. Melbourne: La Trobe
University.<person-group>
                  <string-name>
                     <surname>Clarke</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Land Degradation and Prices</comment>
               <source>Economics and Commerce Discussion Papers</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d155e947a1310">
            <mixed-citation id="d155e951" publication-type="book">
Codsi, George, and Ken R. Pearson. 1988. "An
Overview of GEMPACK: A Software Sys-
tem for Implementing and Solving Economic
Models." Impact Project, GEMPACK docu-
ment No. GED-22. Melbourne: Monash Uni-
versity.<person-group>
                  <string-name>
                     <surname>Codsi</surname>
                  </string-name>
               </person-group>
               <source>An Overview of GEMPACK: A Software System for Implementing and Solving Economic Models</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d155e989a1310">
            <mixed-citation id="d155e993" publication-type="journal">
Corden, W. Max, and J. Peter Neary. 1982.
"Booming Sector and De-industrialization in
a Small Open Economy." Economic Journal
92 (Dec.):825-48.<object-id pub-id-type="doi">10.2307/2232670</object-id>
               <fpage>825</fpage>
            </mixed-citation>
         </ref>
         <ref id="d155e1016a1310">
            <mixed-citation id="d155e1020" publication-type="journal">
Coxhead, Ian A. 1992. "Environment-Specific
Rates and Biases of Technical Change in
Agriculture." American Journal of Agricul-
tural Economics 74 (Aug.):592-604.<object-id pub-id-type="doi">10.2307/1242572</object-id>
               <fpage>592</fpage>
            </mixed-citation>
         </ref>
         <ref id="d155e1043a1310">
            <mixed-citation id="d155e1047" publication-type="book">
Coxhead, Ian A., and Sisira Jayasuriya. 1993.
"Food Crops, Tree Crops and Land Degra-
dation in Developing Countries: Effects of
Tax and Trade Policies." Staff Paper No.
363. University of Wisconsin-Madison De-
partment of Agricultural Economics. Madi-
son, WI.<person-group>
                  <string-name>
                     <surname>Coxhead</surname>
                  </string-name>
               </person-group>
               <source>Food Crops, Tree Crops and Land Degradation in Developing Countries: Effects of Tax and Trade Policies</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d155e1089a1310">
            <mixed-citation id="d155e1093" publication-type="journal">
Coxhead, Ian A., and Peter G. Warr. 1991.
"Technical Change, Land Quality, and In-
come Distribution: A General Equilibrium
Analysis." American Journal of Agricultural
Economics 73 (May):345-60.<object-id pub-id-type="doi">10.2307/1242719</object-id>
               <fpage>345</fpage>
            </mixed-citation>
         </ref>
         <ref id="d155e1119a1310">
            <mixed-citation id="d155e1123" publication-type="book">
Delos Angeles, M. S. 1991. "Integrative Re-
port." National Institute of Geological Sci-
ences, Philippine Natural Resources Ac-
counting Project Final Workshop. Quezon
City, Philippines. Mimeo.<person-group>
                  <string-name>
                     <surname>Delos Angeles</surname>
                  </string-name>
               </person-group>
               <source>Integrative Report</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d155e1158a1310">
            <mixed-citation id="d155e1162" publication-type="journal">
Dornbusch, Rudiger. 1974. "Tariffs and Non-
traded Goods." Journal ofInternational Eco-
nomics 4 (2):177-85.<person-group>
                  <string-name>
                     <surname>Dornbusch</surname>
                  </string-name>
               </person-group>
               <issue>2</issue>
               <fpage>177</fpage>
               <volume>4</volume>
               <source>Journal ofInternational Economics</source>
               <year>1974</year>
            </mixed-citation>
         </ref>
         <ref id="d155e1200a1310">
            <mixed-citation id="d155e1204" publication-type="book">
Feder, G., T. Onchan, C. Chalamwong, and C.
Hongladarom. 1988. Land Policies and Farm
Productivity in Thailand. Baltimore, MD:
Johns Hopkins University Press.<person-group>
                  <string-name>
                     <surname>Feder</surname>
                  </string-name>
               </person-group>
               <source>Land Policies and Farm Productivity in Thailand</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d155e1236a1310">
            <mixed-citation id="d155e1240" publication-type="book">
Gregerson, Hans, Sydney Draper, and Dieter
Elz. 1989. People and Trees: The Role of So-
cial Forestry in Sustainable Development.
Washington, DC: The World Bank.<person-group>
                  <string-name>
                     <surname>Gregerson</surname>
                  </string-name>
               </person-group>
               <source>People and Trees: The Role of Social Forestry in Sustainable Development</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d155e1272a1310">
            <mixed-citation id="d155e1278" publication-type="book">
Jayasuriya, Sisira. 1991. "Technology Genera-
tion and Transfer for Sustainable Upland
Agriculture: Problems and Challenges in
Southeast Asia." In Technologies for Sus-
tainable Agriculture on Marginal Uplands in
Southeast Asia, eds. Graeme Blair and Rod
Lefroy. Canberra: Australian Centre for In-
ternational Agricultural Research.<person-group>
                  <string-name>
                     <surname>Jayasuriya</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Technology Generation and Transfer for Sustainable Upland Agriculture: Problems and Challenges in Southeast Asia</comment>
               <source>Technologies for Sustainable Agriculture on Marginal Uplands in Southeast Asia</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d155e1327a1310">
            <mixed-citation id="d155e1331" publication-type="journal">
Jones, Ronald W. 1965. "The Structure of Sim-
ple General Equilibrium Models." Journal of
Political Economy 73 (Dec.):557-72.<object-id pub-id-type="jstor">10.2307/1829883</object-id>
               <fpage>557</fpage>
            </mixed-citation>
         </ref>
         <ref id="d155e1350a1310">
            <mixed-citation id="d155e1354" publication-type="book">
Jones, Ronald W. 1971. "tA Three-Factor Model
in Theory, Trade and History." In Trade,
Balance of Payments and Growth: Essays in
Honor of C.P. Kindleberger, eds. J. N. Bhag-
wati et al. Amsterdam: North-Holland.<person-group>
                  <string-name>
                     <surname>Jones</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">tA Three-Factor Model in Theory, Trade and History</comment>
               <source>Trade, Balance of Payments and Growth: Essays in Honor of C.P. Kindleberger</source>
               <year>1971</year>
            </mixed-citation>
         </ref>
         <ref id="d155e1392a1310">
            <mixed-citation id="d155e1396" publication-type="book">
LaFrance, J. T. 1990. "Supply Response and
Soil Conservation are Negatively Related."
Bozeman, MT: Montana State University.
Mimeo.<person-group>
                  <string-name>
                     <surname>LaFrance</surname>
                  </string-name>
               </person-group>
               <source>Supply Response and Soil Conservation are Negatively Related</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d155e1428a1310">
            <mixed-citation id="d155e1432" publication-type="journal">
Lipton, M. 1987. "Limits of Price Policy for
Agriculture: Which Way for the World
Bank?" Policy Development Review 5
(2):197-215.<person-group>
                  <string-name>
                     <surname>Lipton</surname>
                  </string-name>
               </person-group>
               <issue>2</issue>
               <fpage>197</fpage>
               <volume>5</volume>
               <source>Policy Development Review</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d155e1473a1310">
            <mixed-citation id="d155e1477" publication-type="journal">
McConnell, K. E. 1983. "An Economic Model
of Soil Conservation." American Journal of
Agricultural Economics 65 (Feb.):83-89.<object-id pub-id-type="doi">10.2307/1240340</object-id>
               <fpage>83</fpage>
            </mixed-citation>
         </ref>
         <ref id="d155e1496a1310">
            <mixed-citation id="d155e1502" publication-type="book">
Pagan, A. R., and J. H. Shannon. 1985. "How
Reliable are ORANI Conclusions?" Discus-
sion Paper No. 130. Canberra: Australian Na-
tional University Centre for Economic Policy
Research.<person-group>
                  <string-name>
                     <surname>Pagan</surname>
                  </string-name>
               </person-group>
               <source>How Reliable are ORANI Conclusions?</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d155e1538a1310">
            <mixed-citation id="d155e1542" publication-type="journal">
Perrings, Charles. 1989. "An Optimal Path to
Extinction? Poverty and Resource Degrada-
tion in an Open Agrarian Economy." Journal
of Development Economics 30 (1):1-24.<person-group>
                  <string-name>
                     <surname>Perrings</surname>
                  </string-name>
               </person-group>
               <issue>1</issue>
               <fpage>1</fpage>
               <volume>30</volume>
               <source>Journal of Development Economics</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d155e1583a1310">
            <mixed-citation id="d155e1587" publication-type="book">
Repetto, Robert. 1989. "Economic Incentives
for Sustainable Production." In Environmen-
tal Management and Economic Develop-
ment, eds. Gunter Schramm and Jeremy J.
Warford. Washington, DC: The World Bank.<person-group>
                  <string-name>
                     <surname>Repetto</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Economic Incentives for Sustainable Production</comment>
               <source>Environmental Management and Economic Development</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d155e1625a1310">
            <mixed-citation id="d155e1629" publication-type="book">
Varian, Hal. 1992. Microeconomic Analysis. (3d
ed.). New York: Norton.<person-group>
                  <string-name>
                     <surname>Varian</surname>
                  </string-name>
               </person-group>
               <edition>3</edition>
               <source>Microeconomic Analysis</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d155e1658a1310">
            <mixed-citation id="d155e1662" publication-type="book">
Warford, Jeremy J. 1989. "Environmental Pol-
icy and Economic Policy in Developing
Countries." In Environmental Management
and Economic Development, eds. Gunter
Schramm and Jeremy J. Warford. Washing-
ton, DC: The World Bank.<person-group>
                  <string-name>
                     <surname>Warford</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Environmental Policy and Economic Policy in Developing Countries</comment>
               <source>Environmental Management and Economic Development</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d155e1703a1310">
            <mixed-citation id="d155e1707" publication-type="book">
World Bank. 1989. Philippines: Environmental
and Natural Resource Management Study.
Washington, DC: The World Bank.<person-group>
                  <string-name>
                     <surname>World Bank</surname>
                  </string-name>
               </person-group>
               <source>Philippines: Environmental and Natural Resource Management Study</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d155e1736a1310">
            <mixed-citation id="d155e1740" publication-type="book">
---. 1990. Indonesia: Sustainable Develop-
ment of Forests, Land, and Water. Washing-
ton, DC: The World Bank.<person-group>
                  <string-name>
                     <surname>World Bank</surname>
                  </string-name>
               </person-group>
               <source>Indonesia: Sustainable Development of Forests, Land, and Water</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
