<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">jlaweconorga</journal-id>
         <journal-id journal-id-type="jstor">j101055</journal-id>
         <journal-title-group>
            <journal-title>Journal of Law, Economics, &amp; Organization</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Oxford University Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">87566222</issn>
         <issn pub-type="epub">14657341</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">765095</article-id>
         <title-group>
            <article-title>Equality, Wealth, and Political Stability</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Richard A.</given-names>
                  <surname>Posner</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>10</month>
            <year>1997</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">13</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id>i231116</issue-id>
         <fpage>344</fpage>
         <lpage>365</lpage>
         <page-range>344-365</page-range>
         <permissions>
            <copyright-statement>Copyright 1997 Oxford University Press</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/765095"/>
         <abstract>
            <p>The relation between economic equality and political stability presents an issue of great importance in view of recent increases in economic inequality in the United States and many other countries. This article presents theoretical reasons and empirical evidence derived from a large sample of the economic and political data of many different countries in support of the proposition that average incomes in a society, rather than the equality or inequality of the income distribution, increase political stability.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d568e120a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d568e127" publication-type="book">
Londregan and Poole (1992)  </mixed-citation>
            </p>
         </fn>
         <fn id="d568e136a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d568e143" publication-type="journal">
Bishop et al. (1991)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d568e151" publication-type="journal">
Economist
(Nov. 5, 1994:19)  <fpage>19</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d568e166a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d568e173" publication-type="journal">
Levy and Murname (1992)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d568e181" publication-type="book">
Freeman and Katz
(1994)  </mixed-citation>
            </p>
         </fn>
         <fn id="d568e193a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d568e200" publication-type="book">
Kaelbe and Thomas (1991:9-10, 42-47)  <fpage>9</fpage>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d568e211" publication-type="book">
Lee and Koo
(1988)  </mixed-citation>
            </p>
         </fn>
         <fn id="d568e224a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d568e231" publication-type="journal">
Dowrick and Nguyen
(1989)  </mixed-citation>
            </p>
         </fn>
         <fn id="d568e243a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d568e250" publication-type="journal">
Bollen (1990)  </mixed-citation>
            </p>
         </fn>
         <fn id="d568e259a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d568e266" publication-type="book">
Alesina and Rodrik (1992)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d568e274" publication-type="journal">
Muller (1988)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d568e282" publication-type="book">
Scully (1992)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d568e291" publication-type="journal">
Stack (1980)  </mixed-citation>
            </p>
         </fn>
         <fn id="d568e300a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d568e307" publication-type="journal">
Bollen and Jackman (1985)  </mixed-citation>
            </p>
         </fn>
         <fn id="d568e316a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d568e323" publication-type="other">
Barro (1995)</mixed-citation>
            </p>
         </fn>
         <fn id="d568e330a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d568e337" publication-type="book">
Green, Coder, and Ryscavage (1994)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d568e345" publication-type="book">
Katz and Murphy (1994:35, 65,
76)  <fpage>35</fpage>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d568e359" publication-type="journal">
Murphy and Welch (1992)  </mixed-citation>
            </p>
         </fn>
         <fn id="d568e369a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d568e378" publication-type="journal">
Gottfredson (1996:15)  <fpage>15</fpage>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d568e389" publication-type="journal">
Rosen (1981)  </mixed-citation>
            </p>
         </fn>
         <fn id="d568e398a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d568e405" publication-type="book">
Mill (1926)  </mixed-citation>
            </p>
         </fn>
         <fn id="d568e414a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d568e421" publication-type="journal">
Blank (1995)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d568e429" publication-type="book">
Massey and Denton (1993)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d568e437" publication-type="book">
Wilson (1987)  </mixed-citation>
            </p>
         </fn>
         <fn id="d568e446a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d568e453" publication-type="journal">
Bilson (1982:107)  <fpage>107</fpage>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d568e464" publication-type="journal">
Barro (1991)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d568e472" publication-type="journal">
note 13  </mixed-citation>
            </p>
         </fn>
         <fn id="d568e481a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d568e488" publication-type="book">
Lind (1995)  </mixed-citation>
            </p>
         </fn>
         <fn id="d568e497a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d568e504" publication-type="book">
Rawls (1971:533)  <fpage>533</fpage>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d568e515" publication-type="book">
Kant (1991:41, 45)  <fpage>41</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d568e528a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d568e535" publication-type="journal">
van Wijck (1994:531, 543; tab. 4)  <fpage>531</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d568e547a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d568e554" publication-type="journal">
Muller and Seligson 1987  </mixed-citation>
            </p>
         </fn>
         <fn id="d568e563a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d568e570" publication-type="journal">
Barro (1991:401, 437)  <fpage>401</fpage>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d568e581" publication-type="journal">
Grier and
Tullock (1989:259, 271-73)  <fpage>259</fpage>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d568e595" publication-type="journal">
Kormendi and Meguire (1985:141, 156)  <fpage>141</fpage>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d568e607" publication-type="journal">
Przeworski and
Limongi (1993:51-69)  <fpage>51</fpage>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d568e621" publication-type="journal">
Posner 1995:71  <fpage>71</fpage>
               </mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d568e642a1310">
            <mixed-citation id="d568e646" publication-type="book">
Alesina, Alberto, and Dani Rodrik. 1992. "Distribution, Political Conflict, and Economic Growth:
A Simple Theory and Some Empirical Evidence," in A. Cukierman, Z. Hercowitz, and
L. Leiderman, eds., Political Economy, Growth, and Business Cycles. Cambridge, Mass.: MIT
Press.<person-group>
                  <string-name>
                     <surname>Alesina</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Distribution, Political Conflict, and Economic Growth: A Simple Theory and Some Empirical Evidence</comment>
               <source>Political Economy, Growth, and Business Cycles</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d568e681a1310">
            <mixed-citation id="d568e685" publication-type="journal">
Barro, Robert J. 1991. "Economic Growth in a Cross Section of Countries," 101 Quarterly Journal
of Economics 407-43.<object-id pub-id-type="doi">10.2307/2937943</object-id>
               <fpage>407</fpage>
            </mixed-citation>
         </ref>
         <ref id="d568e701a1310">
            <mixed-citation id="d568e705" publication-type="other">
---. 1995. "Determinants of Democracy," unpublished manuscript, Department of Economics,
Harvard University.</mixed-citation>
         </ref>
         <ref id="d568e715a1310">
            <mixed-citation id="d568e719" publication-type="journal">
Blank, Rebecca M. 1995. "Changes in Inequality and Unemployment over the 1980s," 8 Journal
of Population Economics 1-21.<person-group>
                  <string-name>
                     <surname>Blank</surname>
                  </string-name>
               </person-group>
               <fpage>1</fpage>
               <volume>8</volume>
               <source>Journal of Population Economics</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d568e752a1310">
            <mixed-citation id="d568e756" publication-type="journal">
Bilson, John F. O. 1982. "Civil Liberty-An Econometric Investigation," 35 Kyklos 94-114.<person-group>
                  <string-name>
                     <surname>Bilson</surname>
                  </string-name>
               </person-group>
               <fpage>94</fpage>
               <volume>35</volume>
               <source>Kyklos</source>
               <year>1982</year>
            </mixed-citation>
         </ref>
         <ref id="d568e785a1310">
            <mixed-citation id="d568e789" publication-type="journal">
Bishop, John A., John P. Formby, and W. James Smith. 1991. "International Comparisons of Income
Inequality: Tests for Lorenz Dominance across Nine Countries," 58 Economica 461-77.<object-id pub-id-type="doi">10.2307/2554692</object-id>
               <fpage>461</fpage>
            </mixed-citation>
         </ref>
         <ref id="d568e805a1310">
            <mixed-citation id="d568e809" publication-type="journal">
Bollen, Kenneth A., and Robert W. Jackman. 1985. "Political Democracy and the Size Distribution
of Income," 50 American Sociological Review 438-57.<object-id pub-id-type="doi">10.2307/2095432</object-id>
               <fpage>438</fpage>
            </mixed-citation>
         </ref>
         <ref id="d568e825a1310">
            <mixed-citation id="d568e829" publication-type="journal">
---, and---. 1990. "Political Democracy: Conceptual and Measurement Traps," 25 Studies
in Comparative International Development 7-24.<person-group>
                  <string-name>
                     <surname>Bollen</surname>
                  </string-name>
               </person-group>
               <fpage>7</fpage>
               <volume>25</volume>
               <source>Studies in Comparative International Development</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d568e861a1310">
            <mixed-citation id="d568e865" publication-type="book">
Boudon, Raymond. 1988. "The Logic of Relative Frustration," in M. Taylor, ed., Rationality and
Revolution. Cambridge: Cambridge University Press.<person-group>
                  <string-name>
                     <surname>Boudon</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The Logic of Relative Frustration</comment>
               <source>Rationality and Revolution</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d568e894a1310">
            <mixed-citation id="d568e898" publication-type="journal">
Dowrick, Steve, and Duc-Tho Nguyen. 1989. "OECD Comparative Economic Growth 1950-85:
Catch-Up and Convergence," 79 American Economic Review 1010-30.<object-id pub-id-type="jstor">10.2307/1831434</object-id>
               <fpage>1010</fpage>
            </mixed-citation>
         </ref>
         <ref id="d568e915a1310">
            <mixed-citation id="d568e919" publication-type="book">
Dréze, Jean, and Amartya Sen. 1995. India Economic Development and Social Opportunity. Oxford:
Oxford University Press.<person-group>
                  <string-name>
                     <surname>Dréze</surname>
                  </string-name>
               </person-group>
               <source>India Economic Development and Social Opportunity</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d568e944a1310">
            <mixed-citation id="d568e948" publication-type="journal">
Economist. "For Richer, for Poorer," Economist, Nov. 5, 1994, 19-21.<person-group>
                  <string-name>
                     <surname>Economist</surname>
                  </string-name>
               </person-group>
               <issue>Nov. 5</issue>
               <fpage>19</fpage>
               <source>Economist</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d568e977a1310">
            <mixed-citation id="d568e981" publication-type="book">
Freeman, Richard B., and Lawrence F. Katz. 1994. "Rising Wage Inequality: The United States vs.
Other Advanced Countries," in R. B. Freeman, ed., Working under Different Rules. New York:
Russell Sage Foundation.<person-group>
                  <string-name>
                     <surname>Freeman</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Rising Wage Inequality: The United States vs. Other Advanced Countries</comment>
               <source>Working under Different Rules</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1013a1310">
            <mixed-citation id="d568e1017" publication-type="journal">
Gottfredson, Linda S. 1996. "What Do We Know about Intelligence?" 65 American Scholar 15-30.<person-group>
                  <string-name>
                     <surname>Gottfredson</surname>
                  </string-name>
               </person-group>
               <fpage>15</fpage>
               <volume>65</volume>
               <source>American Scholar</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1046a1310">
            <mixed-citation id="d568e1050" publication-type="book">
Green, Gordon W., Jr., John Coder, and Paul Ryscavage. 1994. "International Comparisons of
Earnings Inequality for Men in the 1980's," in D. B. Papadimitriou, ed., Aspects of Distribution
of Wealth and Income. New York: St. Martin's Press.<person-group>
                  <string-name>
                     <surname>Green</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">International Comparisons of Earnings Inequality for Men in the 1980's</comment>
               <source>Aspects of Distribution of Wealth and Income</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1082a1310">
            <mixed-citation id="d568e1086" publication-type="journal">
Grier, Kevin B., and Gordon Tullock. 1989. "An Empirical Analysis of Cross National Economic
Growth, 1951-80," 24 Journal of Monetary Economics 259-76.<person-group>
                  <string-name>
                     <surname>Grier</surname>
                  </string-name>
               </person-group>
               <fpage>259</fpage>
               <volume>24</volume>
               <source>Journal of Monetary Economics</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1119a1310">
            <mixed-citation id="d568e1123" publication-type="book">
Kaelbe, Hartmut, and Mark Thomas. 1991. "Introduction," in Y. S. Brenner, H. Kaelbe, and
M. Thomas, eds., Income Distribution in Historical Perspective. Cambridge: Cambridge
University Press.<person-group>
                  <string-name>
                     <surname>Kaelbe</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Introduction</comment>
               <source>Income Distribution in Historical Perspective</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1155a1310">
            <mixed-citation id="d568e1159" publication-type="book">
Kant, Immanuel. 1991. "Idea for a Universal History with a Cosmopolitan Purpose," in H. Reiss,
ed., Kant: Political Writings, 2nd ed. Cambridge: Cambridge University Press.<person-group>
                  <string-name>
                     <surname>Kant</surname>
                  </string-name>
               </person-group>
               <edition>2</edition>
               <comment content-type="section">Idea for a Universal History with a Cosmopolitan Purpose</comment>
               <source>Kant: Political Writings</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1191a1310">
            <mixed-citation id="d568e1195" publication-type="book">
Katz, Lawrence F., and Kevin M. Murphy. 1994. "Changes in Relative Wages, 1963-1987: Supply
and Demand Factors," in D. B. Papadimitriou, ed., Aspects of Distribution of Wealth and Income.
New York: St. Martin's Press.<person-group>
                  <string-name>
                     <surname>Katz</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Changes in Relative Wages, 1963-1987: Supply and Demand Factors</comment>
               <source>Aspects of Distribution of Wealth and Income</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1227a1310">
            <mixed-citation id="d568e1231" publication-type="journal">
Kormendi, Roger C., and Philip G. Meguire. 1985. "Macroeconomic Determinants of Growth:
Cross-Country Evidence," 16 Journal of Monetary Economics 141-63.<person-group>
                  <string-name>
                     <surname>Kormendi</surname>
                  </string-name>
               </person-group>
               <fpage>141</fpage>
               <volume>16</volume>
               <source>Journal of Monetary Economics</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1263a1310">
            <mixed-citation id="d568e1267" publication-type="book">
Lee, Jae Won, and Suk Mo Koo. 1988. "Trade-Off between Economic Growth and Economic
Equality: A Re-Evaluation," in Y. S. Brenner, J. P. G. Reijnders, and A. H. G. M. Spithoven,
eds., The Theory of Income and Wealth Distribution. New York: St. Martin's Press.<person-group>
                  <string-name>
                     <surname>Lee</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Trade-Off between Economic Growth and Economic Equality: A Re-Evaluation</comment>
               <source>The Theory of Income and Wealth Distribution</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1299a1310">
            <mixed-citation id="d568e1303" publication-type="journal">
Levy, Frank, and Richard J. Murname. 1992. "U.S. Earnings Levels and Earnings Inequality:
A Review of Recent Trends and Proposed Explanations," 30 Journal of Economic Literature
1333-81.<object-id pub-id-type="jstor">10.2307/2728062</object-id>
               <fpage>1333</fpage>
            </mixed-citation>
         </ref>
         <ref id="d568e1323a1310">
            <mixed-citation id="d568e1327" publication-type="book">
Lind, Michael. 1995. The Next American Nation: The New Nationalism and the Fourth American
Revolution. New York: Free Press.<person-group>
                  <string-name>
                     <surname>Lind</surname>
                  </string-name>
               </person-group>
               <source>The Next American Nation: The New Nationalism and the Fourth American Revolution</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1352a1310">
            <mixed-citation id="d568e1356" publication-type="book">
Londregan, John, and Keith Poole. 1992. "The Seizure of Executive Power and Economic Growth:
Some Additional Evidence," in A. Cukierman, Z. Hercowitz, and L. Leiderman, eds., Political
Economy, Growth, and Business Cycles. Cambridge, Mass.: MIT Press.<person-group>
                  <string-name>
                     <surname>Londregan</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The Seizure of Executive Power and Economic Growth: Some Additional Evidence</comment>
               <source>Political Economy, Growth, and Business Cycles</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1388a1310">
            <mixed-citation id="d568e1392" publication-type="book">
Massey, Douglas S., and Nancy A. Denton. 1993. American Apartheid: Segregation and the Making
of the Underclass. Cambridge: Harvard University Press.<person-group>
                  <string-name>
                     <surname>Massey</surname>
                  </string-name>
               </person-group>
               <source>American Apartheid: Segregation and the Making of the Underclass</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1417a1310">
            <mixed-citation id="d568e1421" publication-type="book">
Mill, John S. 1926. in W. J. Ashley, ed., Principles of Political Economy, bk. II, ch. I, § 1, at 200.
London: Longmans, Green and Company.<person-group>
                  <string-name>
                     <surname>Mill</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">ch. I</comment>
               <source>Principles of Political Economy</source>
               <year>1926</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1450a1310">
            <mixed-citation id="d568e1454" publication-type="journal">
Muller, Edward N., and Mitchell A. Seligson. 1987. "Inequality and Insurgency," 81 American
Political Science Review 425-51.<object-id pub-id-type="doi">10.2307/1961960</object-id>
               <fpage>425</fpage>
            </mixed-citation>
         </ref>
         <ref id="d568e1470a1310">
            <mixed-citation id="d568e1474" publication-type="journal">
---, and ---. 1988. "Democracy, Economic Development, and Income Inequality," 53
American Sociological Review 50-68.<object-id pub-id-type="doi">10.2307/2095732</object-id>
               <fpage>50</fpage>
            </mixed-citation>
         </ref>
         <ref id="d568e1491a1310">
            <mixed-citation id="d568e1495" publication-type="journal">
Murphy, Kevin M., and Finis Welch. 1992. "The Structure of Wages," 107 Quarterly Journal of
Economics 285-326.<object-id pub-id-type="doi">10.2307/2118330</object-id>
               <fpage>285</fpage>
            </mixed-citation>
         </ref>
         <ref id="d568e1511a1310">
            <mixed-citation id="d568e1515" publication-type="book">
Nussbaum, Martha C., and Amartya Sen, eds. 1993. The Quality of Life. Oxford: Clarendon Press.<person-group>
                  <string-name>
                     <surname>Nussbaum</surname>
                  </string-name>
               </person-group>
               <source>The Quality of Life</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1537a1310">
            <mixed-citation id="d568e1541" publication-type="book">
---, ---, and Jonathan Glover, eds. 1995. Women, Culture, and Development: A Study of
Human Capabilities. Oxford: Clarendon Press.<person-group>
                  <string-name>
                     <surname>Nussbaum</surname>
                  </string-name>
               </person-group>
               <source>Women, Culture, and Development: A Study of Human Capabilities</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1566a1310">
            <mixed-citation id="d568e1570" publication-type="book">
---, ---, and ---. 1995. Poetic Justice: The Literary Imagination and Public Life.
Boston: Beacon Press.<person-group>
                  <string-name>
                     <surname>Nussbaum</surname>
                  </string-name>
               </person-group>
               <source>Poetic Justice: The Literary Imagination and Public Life</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1595a1310">
            <mixed-citation id="d568e1599" publication-type="journal">
Peltzman, Samuel. 1980. "The Growth of Government," 23 Journal of Law and Economics 209-87.<object-id pub-id-type="jstor">10.2307/725128</object-id>
               <fpage>209</fpage>
            </mixed-citation>
         </ref>
         <ref id="d568e1612a1310">
            <mixed-citation id="d568e1616" publication-type="book">
Posner, Richard A. 1992. Economic Analysis of Law, 4th ed. Boston: Little, Brown.<person-group>
                  <string-name>
                     <surname>Posner</surname>
                  </string-name>
               </person-group>
               <edition>4</edition>
               <source>Economic Analysis of Law</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1642a1310">
            <mixed-citation id="d568e1646" publication-type="journal">
---. 1995. "The Costs of Enforcing Legal Rights," 4, Summer, East European Constitutional
Review 71-83.<person-group>
                  <string-name>
                     <surname>Posner</surname>
                  </string-name>
               </person-group>
               <issue>Summer</issue>
               <fpage>71</fpage>
               <volume>4</volume>
               <source>East European Constitutional Review</source>
            </mixed-citation>
         </ref>
         <ref id="d568e1678a1310">
            <mixed-citation id="d568e1682" publication-type="journal">
Przeworski, Adam, and Fernando Limongi. 1993. "Political Regimes and Economic Growth," 7
Journal of Economic Perspectives 51-69.<object-id pub-id-type="jstor">10.2307/2138442</object-id>
               <fpage>51</fpage>
            </mixed-citation>
         </ref>
         <ref id="d568e1698a1310">
            <mixed-citation id="d568e1702" publication-type="book">
Rawls, John. 1971. A Theory of Justice. Cambridge, Mass.: Belknap Press.<person-group>
                  <string-name>
                     <surname>Rawls</surname>
                  </string-name>
               </person-group>
               <source>A Theory of Justice</source>
               <year>1971</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1724a1310">
            <mixed-citation id="d568e1728" publication-type="journal">
Rosen, Sherwin. 1981. "The Economics of Superstars," 71 American Economic Review 845-58.<object-id pub-id-type="jstor">10.2307/1803469</object-id>
               <fpage>845</fpage>
            </mixed-citation>
         </ref>
         <ref id="d568e1741a1310">
            <mixed-citation id="d568e1745" publication-type="book">
Scully, Gerald W. 1992. Constitutional Environments and Economic Growth. Princeton, N.J.:
Princeton University Press.<person-group>
                  <string-name>
                     <surname>Scully</surname>
                  </string-name>
               </person-group>
               <source>Constitutional Environments and Economic Growth</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1770a1310">
            <mixed-citation id="d568e1774" publication-type="book">
Sen, Amartya. 1992. Inequality Reexamined. Cambridge: Harvard University Press.<person-group>
                  <string-name>
                     <surname>Sen</surname>
                  </string-name>
               </person-group>
               <source>Inequality Reexamined</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1797a1310">
            <mixed-citation id="d568e1801" publication-type="journal">
Stack, Steven. 1980. "The Political Economy of Income Inequality: A Comparative Analysis," 13
Canadian Journal of Political Science/Revue canadienne de science politique 273-86.<object-id pub-id-type="jstor">10.2307/3229724</object-id>
               <fpage>273</fpage>
            </mixed-citation>
         </ref>
         <ref id="d568e1817a1310">
            <mixed-citation id="d568e1821" publication-type="journal">
van Wijck, Peter. 1994. "Equity and Equality in East and West," 47 Kyklos 531-50.<person-group>
                  <string-name>
                     <surname>van Wijck</surname>
                  </string-name>
               </person-group>
               <fpage>531</fpage>
               <volume>47</volume>
               <source>Kyklos</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1850a1310">
            <mixed-citation id="d568e1854" publication-type="book">
Wilson, William Julius. 1987. The Truly Disadvantaged: The Inner City, the Underclass, and
Public Policy. Chicago: University of Chicago Press.<person-group>
                  <string-name>
                     <surname>Wilson</surname>
                  </string-name>
               </person-group>
               <source>The Truly Disadvantaged: The Inner City, the Underclass, and Public Policy</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
