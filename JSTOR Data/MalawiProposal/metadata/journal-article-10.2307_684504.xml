<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">sociforu</journal-id>
         <journal-id journal-id-type="jstor">j100768</journal-id>
         <journal-title-group>
            <journal-title>Sociological Forum</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Eastern Sociological Society</publisher-name>
         </publisher>
         <issn pub-type="ppub">08848971</issn>
         <issn pub-type="epub">15737861</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">684504</article-id>
         <title-group>
            <article-title>Democratic Transitions and the Semiperiphery of the World-Economy</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Roberto P.</given-names>
                  <surname>Korzeniewicz</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Kimberley</given-names>
                  <surname>Awbrey</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>12</month>
            <year>1992</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">7</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id>i227624</issue-id>
         <fpage>609</fpage>
         <lpage>640</lpage>
         <page-range>609-640</page-range>
         <permissions>
            <copyright-statement>Copyright 1992 Plenum Publishing Corporation</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/684504"/>
         <abstract>
            <p>The dramatic global transition to democracy of the last decade reveals important systematic patterns. Using recent contributions to world-systems theory, this study examines the relationship between political regime changes and the position of nations in the world-economy over the last two decades, to argue that democratic transitions have been centered among semiperipheral nations. The study also found a strong relationship between the structure of the labor force and the global distribution of democratic institutions, but a weak relationship between the structure of the labor force and the timing of transitions in the semiperiphery. Overall, the findings suggest that world-systemic categories provide a useful vantage point to distinguish global trends from the specific characteristics of individual nations, thereby allowing for greater analytical precision in identifying the crucial causal relations shaping transitions from dictatorship to democracy.</p>
         </abstract>
         <kwd-group>
            <kwd>Democratic Transitions</kwd>
            <kwd>Development</kwd>
            <kwd>World-Economy</kwd>
            <kwd>World-Systems Theory</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d162e147a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d162e154" publication-type="journal">
Snyder
and Kick (1979:1110)  <fpage>1110</fpage>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d162e168" publication-type="journal">
Arrighi and Drangel (1986:67-69)  <fpage>67</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d162e180a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d162e187" publication-type="journal">
Nemeth and Smith
(1985:527)  <fpage>527</fpage>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d162e201" publication-type="journal">
Arrighi and Drangel (1986:67-69)  <fpage>67</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d162e213a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d162e220" publication-type="journal">
Gonick and Rosh (1988:188)  <fpage>188</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d162e232a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d162e239" publication-type="book">
Przeworski (1986:58)  <fpage>58</fpage>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d162e250" publication-type="book">
O'Donnell and
Schmitter (1986:11)  <fpage>11</fpage>
               </mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d162e274a1310">
            <mixed-citation id="d162e278" publication-type="book">
Anderson, ThomasP.
1988Politics in Central America. New
York: Praeger.<person-group>
                  <string-name>
                     <surname>Anderson</surname>
                  </string-name>
               </person-group>
               <source>Politics in Central America</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d162e307a1310">
            <mixed-citation id="d162e311" publication-type="journal">
Arat, ZehraF.
1988 Democracy and economic develop-
ment: Modernization theory revisited."
Comparative Politics21:21-36.<object-id pub-id-type="doi">10.2307/422069</object-id>
               <fpage>21</fpage>
            </mixed-citation>
         </ref>
         <ref id="d162e334a1310">
            <mixed-citation id="d162e338" publication-type="book">
Arrighi, Giovanni
1990 "The developmentalist illusion: A re-
conceptualization of the semiperi-
phery." In William G. Martin (ed.),
Semiperipheral States in the World-
Economy: 11-42. New York: Green-
wood Press.<person-group>
                  <string-name>
                     <surname>Arrighi</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The developmentalist illusion: A reconceptualization of the semiperiphery</comment>
               <fpage>11</fpage>
               <source>Semiperipheral States in the World-Economy</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d162e386a1310">
            <mixed-citation id="d162e390" publication-type="journal">
Arrighi, Giovanni, and Jessica Drangel
1986 "The stratification of the world-econ-
omy: An exploration of the
semiperipheral zone." Review10:9-74.<person-group>
                  <string-name>
                     <surname>Arrighi</surname>
                  </string-name>
               </person-group>
               <fpage>9</fpage>
               <volume>10</volume>
               <source>Review</source>
               <year>1986</year>
            </mixed-citation>
         </ref>
         <ref id="d162e429a1310">
            <mixed-citation id="d162e433" publication-type="book">
Banks, ArthurS.
1970, 1984-
1985Political Handbook of the World.
Binghamton, NY: CSA Publications.<person-group>
                  <string-name>
                     <surname>Banks</surname>
                  </string-name>
               </person-group>
               <source>Political Handbook of the World</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d162e465a1310">
            <mixed-citation id="d162e469" publication-type="journal">
Bollen, Kenneth
1979 "Political democracy and the timing of
development." American Sociological
Review44:572-587.<object-id pub-id-type="doi">10.2307/2094588</object-id>
               <fpage>572</fpage>
            </mixed-citation>
            <mixed-citation id="d162e491" publication-type="journal">
1980"Issues in the comparative measure-
ment of political democracies."
American Sociological Review45:370-
390.<object-id pub-id-type="doi">10.2307/2095172</object-id>
               <fpage>370</fpage>
            </mixed-citation>
            <mixed-citation id="d162e513" publication-type="journal">
1983"World system position, dependency,
and democracy: The cross-national
evidence." American Sociological Re-
view48:468-479.<object-id pub-id-type="doi">10.2307/2117715</object-id>
               <fpage>468</fpage>
            </mixed-citation>
            <mixed-citation id="d162e535" publication-type="journal">
1988If you ignore outliers, will they go
away?:' A response to Gasiorowski."
Comparative Political Studies20:516-
522.<person-group>
                  <string-name>
                     <surname>Bollen</surname>
                  </string-name>
               </person-group>
               <fpage>516</fpage>
               <volume>20</volume>
               <source>Comparative Political Studies</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d162e573a1310">
            <mixed-citation id="d162e577" publication-type="journal">
Bollen, Kenneth, and Burke D. Grandjean
1981 "The dimension(s) of democracy: Fur-
ther issues in the measurement and ef-
fects of political democracy."
American Sociological Review46:651-
659.<object-id pub-id-type="doi">10.2307/2094946</object-id>
               <fpage>651</fpage>
            </mixed-citation>
         </ref>
         <ref id="d162e606a1310">
            <mixed-citation id="d162e610" publication-type="journal">
Bollen, Kenneth, and Robert W. Jackman
1985 "Political democracy and the size dis-
tribution of income." American Socio-
logical Review50:438-457.<object-id pub-id-type="doi">10.2307/2095432</object-id>
               <fpage>438</fpage>
            </mixed-citation>
         </ref>
         <ref id="d162e633a1310">
            <mixed-citation id="d162e637" publication-type="book">
Brucan, Silviu
1990Pluralism and Social Conflict. New
York: Praeger.<person-group>
                  <string-name>
                     <surname>Brucan</surname>
                  </string-name>
               </person-group>
               <source>Pluralism and Social Conflict</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d162e666a1310">
            <mixed-citation id="d162e670" publication-type="journal">
Cardoso, FernandoH.
1986 "Democracy in Latin America." Poli-
tics and Society15:2341.<person-group>
                  <string-name>
                     <surname>Cardoso</surname>
                  </string-name>
               </person-group>
               <fpage>2341</fpage>
               <volume>15</volume>
               <source>Politics and Society</source>
               <year>1986</year>
            </mixed-citation>
         </ref>
         <ref id="d162e706a1310">
            <mixed-citation id="d162e710" publication-type="journal">
Chase-Dunn, Christopher
1990 "Resistance to imperialism:
Semiperipheral actors." Review13:1-
31.<person-group>
                  <string-name>
                     <surname>Chase-Dunn</surname>
                  </string-name>
               </person-group>
               <fpage>1</fpage>
               <volume>13</volume>
               <source>Review</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d162e748a1310">
            <mixed-citation id="d162e752" publication-type="book">
Cooke, Christopher, and David Killingray
1983African Political Facts Since 1945.
London: MacMillan.<person-group>
                  <string-name>
                     <surname>Cooke</surname>
                  </string-name>
               </person-group>
               <source>African Political Facts Since 1945</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d162e781a1310">
            <mixed-citation id="d162e785" publication-type="journal">
Cummings, Bruce
1989 "The abortive Abertura: South Korea
in the light of Latin American experi-
ence." New Left Review173:5-32.<person-group>
                  <string-name>
                     <surname>Cummings</surname>
                  </string-name>
               </person-group>
               <fpage>5</fpage>
               <volume>173</volume>
               <source>New Left Review</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d162e823a1310">
            <mixed-citation id="d162e827" publication-type="book">
Dahl, Robert
1971Polyarchy: Participation and Opposi-
tion. New Haven, CT: Yale University
Press.<person-group>
                  <string-name>
                     <surname>Dahl</surname>
                  </string-name>
               </person-group>
               <source>Polyarchy: Participation and Opposition</source>
               <year>1971</year>
            </mixed-citation>
         </ref>
         <ref id="d162e859a1310">
            <mixed-citation id="d162e863" publication-type="journal">
Deutsch, KarlW.
1961 "Social mobilization and political de-
velopment." The American Political
Science Review55:493-514.<object-id pub-id-type="doi">10.2307/1952679</object-id>
               <fpage>493</fpage>
            </mixed-citation>
         </ref>
         <ref id="d162e886a1310">
            <mixed-citation id="d162e890" publication-type="book">
Deutscher, Isaac
1967The Unfinished Revolution. London:
Oxford University Press.<person-group>
                  <string-name>
                     <surname>Deutscher</surname>
                  </string-name>
               </person-group>
               <source>The Unfinished Revolution</source>
               <year>1967</year>
            </mixed-citation>
         </ref>
         <ref id="d162e920a1310">
            <mixed-citation id="d162e924" publication-type="book">
Diamond, Larry, and Juan Linz
1989 "Introduction: Politics, society and de-
mocracy in Latin America." In Larry
Diamond, Juan Linz, and Seymour M.
Lipset (eds.), Democracy in Develop-
ing Countries: Latin America: 1-58.
Boulder, CO: Lynne Rienner.<person-group>
                  <string-name>
                     <surname>Diamond</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Introduction: Politics, society and democracy in Latin America</comment>
               <fpage>1</fpage>
               <source>Democracy in Developing Countries: Latin America</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d162e972a1310">
            <mixed-citation id="d162e976" publication-type="book">
Diamond, Larry, Juan Linz, and Seymour M.
Lipset, eds.
1989 "Preface." In Democracy in Develop-
ing Countries: Latin America: ix-xxvii.
Boulder, CO: Lynne Rienner.<person-group>
                  <string-name>
                     <surname>Diamond</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Preface</comment>
               <fpage>ix</fpage>
               <source>In Democracy in Developing Countries: Latin America</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d162e1017a1310">
            <mixed-citation id="d162e1021" publication-type="book">
Evans, Peter
1979Dependent Development. Princeton,
NJ: Princeton University Press.<person-group>
                  <string-name>
                     <surname>Evans</surname>
                  </string-name>
               </person-group>
               <source>Dependent Development</source>
               <year>1979</year>
            </mixed-citation>
         </ref>
         <ref id="d162e1050a1310">
            <mixed-citation id="d162e1054" publication-type="journal">
Gasiorowski, MarkJ.
1988 "Economic dependence and political
democracy: A cross-national study."
Comparative Political Studies20:489-
515.<person-group>
                  <string-name>
                     <surname>Gasiorowski</surname>
                  </string-name>
               </person-group>
               <fpage>489</fpage>
               <volume>20</volume>
               <source>Comparative Political Studies</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d162e1095a1310">
            <mixed-citation id="d162e1099" publication-type="book">
Gastil, Raymond
1981Freedom in the World: Political
Rights and Civil Liberties. New York:
Freedom House.<person-group>
                  <string-name>
                     <surname>Gastil</surname>
                  </string-name>
               </person-group>
               <source>Freedom in the World: Political Rights and Civil Liberties</source>
               <year>1981</year>
            </mixed-citation>
         </ref>
         <ref id="d162e1131a1310">
            <mixed-citation id="d162e1135" publication-type="journal">
Gonick, LevS., and Robert M. Rosh
1988 "The structural constraints of the
world-economy on national political
development." Comparative Political
Studies21:171 199.<person-group>
                  <string-name>
                     <surname>Gonick</surname>
                  </string-name>
               </person-group>
               <fpage>171</fpage>
               <volume>21</volume>
               <source>Comparative Political Studies</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d162e1177a1310">
            <mixed-citation id="d162e1181" publication-type="journal">
Hewitt, Christopher
1977 "The effect of political democracy and
social democracy on inequality in in-
dustrial societies: A cross-national
comparison." American Sociological
Review42450-464.<object-id pub-id-type="doi">10.2307/2094750</object-id>
               <fpage>450</fpage>
            </mixed-citation>
         </ref>
         <ref id="d162e1210a1310">
            <mixed-citation id="d162e1214" publication-type="journal">
Huntington, Samuel
1984 "Will more countries become demo-
cratic?" Political Science Quarterly
99:193-218.<object-id pub-id-type="doi">10.2307/2150402</object-id>
               <fpage>193</fpage>
            </mixed-citation>
         </ref>
         <ref id="d162e1237a1310">
            <mixed-citation id="d162e1241" publication-type="journal">
Jackman, Robert
1974 "Political democracy and social equal-
ity. A comparative analysis." Ameri-
can Sociological Review39:29-45.<object-id pub-id-type="doi">10.2307/2094274</object-id>
               <fpage>29</fpage>
            </mixed-citation>
         </ref>
         <ref id="d162e1264a1310">
            <mixed-citation id="d162e1270" publication-type="book">
Kornai, Jainos
1959Overcentralization in Economic Admini-
stration. Oxford: Oxford University
Press.<person-group>
                  <string-name>
                     <surname>Kornai</surname>
                  </string-name>
               </person-group>
               <source>Overcentralization in Economic Administration</source>
               <year>1959</year>
            </mixed-citation>
            <mixed-citation id="d162e1301" publication-type="book">
1989"The Hungarian reform process: Vi-
sions, hopes and reality." In Victor Nee
and David Stark (eds.), Remaking the
Economic Institutions of Socialism in
China and Eastern Europe: 32-94.
Stanford, CA: Stanford University
Press.<person-group>
                  <string-name>
                     <surname>Kornai</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The Hungarian reform process: Visions, hopes and reality</comment>
               <fpage>32</fpage>
               <source>Remaking the Economic Institutions of Socialism in China and Eastern Europe</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d162e1349a1310">
            <mixed-citation id="d162e1353" publication-type="journal">
Lipset, SeymourM.
1959 "Some social requisites of democracy:
Economic development and political
legitimacy." The American Political
Science Review53:69-105.<object-id pub-id-type="doi">10.2307/1951731</object-id>
               <fpage>69</fpage>
            </mixed-citation>
            <mixed-citation id="d162e1378" publication-type="book">
1960Political Man. New York: Doubleday.<person-group>
                  <string-name>
                     <surname>Lipset</surname>
                  </string-name>
               </person-group>
               <source>Political Man</source>
               <year>1960</year>
            </mixed-citation>
         </ref>
         <ref id="d162e1400a1310">
            <mixed-citation id="d162e1404" publication-type="journal">
Marsh, RobertM.
1979 "Does democracy hinder economic
development in the latecomer devel-
oping nations?" Comparative Social
Research2:215-248.<person-group>
                  <string-name>
                     <surname>Marsh</surname>
                  </string-name>
               </person-group>
               <fpage>215</fpage>
               <volume>2</volume>
               <source>Comparative Social Research</source>
               <year>1979</year>
            </mixed-citation>
         </ref>
         <ref id="d162e1446a1310">
            <mixed-citation id="d162e1450" publication-type="book">
Martin, William, ed.
1990Semiperipheral States in the World-
Economy. New York: Greenwood Press.<person-group>
                  <string-name>
                     <surname>Martin</surname>
                  </string-name>
               </person-group>
               <source>Semiperipheral States in the World-Economy</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d162e1479a1310">
            <mixed-citation id="d162e1483" publication-type="journal">
Nemeth, RogerJ., and David A. Smith
1985 "International trade and world-system
structure: A multiple network analy-
sis." Review8:517-560.<person-group>
                  <string-name>
                     <surname>Nemeth</surname>
                  </string-name>
               </person-group>
               <fpage>517</fpage>
               <volume>8</volume>
               <source>Review</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d162e1521a1310">
            <mixed-citation id="d162e1525" publication-type="book">
O'Donnell, Guillermo
1973Modernization and Bureaucratic
Authoritarianism. Berkeley: University
of California, Institute of International
Studies.<person-group>
                  <string-name>
                     <surname>O'Donnell</surname>
                  </string-name>
               </person-group>
               <source>Modernization and Bureaucratic Authoritarianism</source>
               <year>1973</year>
            </mixed-citation>
            <mixed-citation id="d162e1559" publication-type="journal">
1978"Reflections on the patterns of change
in the bureaucratic-authoritarian
state." Latin American Research Re-
view13:3-38.<object-id pub-id-type="jstor">10.2307/2502640</object-id>
               <fpage>3</fpage>
            </mixed-citation>
         </ref>
         <ref id="d162e1582a1310">
            <mixed-citation id="d162e1586" publication-type="book">
O'Donnell, Guillermo, and Philippe C.
Schmitter
1986Transitions from Authoritarian Rule:
Tentative Conclusions about Uncer-
tain Democracies. Baltimore, MD:
Johns Hopkins University Press.<person-group>
                  <string-name>
                     <surname>O'Donnell</surname>
                  </string-name>
               </person-group>
               <source>Transitions from Authoritarian Rule: Tentative Conclusions about Uncertain Democracies</source>
               <year>1986</year>
            </mixed-citation>
         </ref>
         <ref id="d162e1624a1310">
            <mixed-citation id="d162e1628" publication-type="journal">
Portantiero, JuanC.
1984 "La democratizaci6n del estado." Pen-
samiento Iberoamericano5a:99-126.<person-group>
                  <string-name>
                     <surname>Portantiero</surname>
                  </string-name>
               </person-group>
               <fpage>99</fpage>
               <volume>5a</volume>
               <source>Pensamiento Iberoamericano</source>
               <year>1984</year>
            </mixed-citation>
            <mixed-citation id="d162e1662" publication-type="book">
1989"Political and economic crises in Ar-
gentina." In Guido Di Tella and Rudi-
ger Dornbusch (eds.), The Political
Economy of Argentina 1946-1983:16-
24. London: Macmillan.<person-group>
                  <string-name>
                     <surname>Portantiero</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Political and economic crises in Argentina</comment>
               <fpage>16</fpage>
               <source>The Political Economy of Argentina 1946-1983</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d162e1703a1310">
            <mixed-citation id="d162e1707" publication-type="book">
Przeworski, Adam
1986 "Some problems in the study of the
transition to democracy." In G.
O'Donnell, P. Schmitter, and L.
Whitehead (eds.), Transitions from
Authoritarian Rule: Comparative Per-
spectives: 47-63. Baltimore, MD:
Johns Hopkins University Press.<person-group>
                  <string-name>
                     <surname>Przeworski</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Some problems in the study of the transition to democracy</comment>
               <fpage>47</fpage>
               <source>Transitions from Authoritarian Rule: Comparative Perspectives</source>
               <year>1986</year>
            </mixed-citation>
         </ref>
         <ref id="d162e1759a1310">
            <mixed-citation id="d162e1763" publication-type="journal">
Rubinson, Richard, and Dan Quinlan
1977 "Democracy and social inequality: A
reanalysis." American Sociological Re-
view42:611-623.<object-id pub-id-type="doi">10.2307/2094559</object-id>
               <fpage>611</fpage>
            </mixed-citation>
         </ref>
         <ref id="d162e1786a1310">
            <mixed-citation id="d162e1790" publication-type="book">
Rueschemeyer, Dietrich, and Peter Evans
1985 "The state and economic transforma-
tion: Toward an analysis of the condi-
tions underlying effective
intervention." In Peter Evans, Dietrich
Rueschemeyer, and Theda Skocpol
(eds.), Bringing the State Back In: 44-
77. Cambridge, MA: Cambridge Uni-
versity Press.<person-group>
                  <string-name>
                     <surname>Rueschemeyer</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The state and economic transformation: Toward an analysis of the conditions underlying effective intervention</comment>
               <fpage>44</fpage>
               <source>Bringing the State Back In</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d162e1844a1310">
            <mixed-citation id="d162e1848" publication-type="journal">
Rustow, DankwartA.
1970 "Transitions to democracy: Toward a
dynamic model." Comparative Politics
2:337-363.<object-id pub-id-type="doi">10.2307/421307</object-id>
               <fpage>337</fpage>
            </mixed-citation>
         </ref>
         <ref id="d162e1871a1310">
            <mixed-citation id="d162e1875" publication-type="book">
Shue, Vivienne
1988The Reach of the State. Stanford:
Stanford University Press.<person-group>
                  <string-name>
                     <surname>Shue</surname>
                  </string-name>
               </person-group>
               <source>The Reach of the State</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d162e1904a1310">
            <mixed-citation id="d162e1908" publication-type="journal">
Snyder, D., and E. Kick
1979 "Structural position in the world-sys-
tem and economic growth, 1955-1970:
A multiple network analysis of tran-
snational interactions." American
Journal of Sociology84:1096-1126.<object-id pub-id-type="jstor">10.2307/2778218</object-id>
               <fpage>1096</fpage>
            </mixed-citation>
         </ref>
         <ref id="d162e1937a1310">
            <mixed-citation id="d162e1941" publication-type="book">
So, AlvinY.
1990Social Change and Development:
Modernization, Dependency and
World-System Theories. Newbury
Park, CA: Sage.<person-group>
                  <string-name>
                     <surname>So</surname>
                  </string-name>
               </person-group>
               <source>Social Change and Development: Modernization, Dependency and World-System Theories</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d162e1977a1310">
            <mixed-citation id="d162e1981" publication-type="journal">
Stack, Steven
1979 "The effects of political participation
and socialist party strength on the de-
gree of income inequality." American
Sociological Review44. 168-171.<object-id pub-id-type="doi">10.2307/2094826</object-id>
               <fpage>168</fpage>
            </mixed-citation>
         </ref>
         <ref id="d162e2007a1310">
            <mixed-citation id="d162e2011" publication-type="book">
Stepan, Alfred
1985 "State power and the strength of civil
society in the Southern Cone of Latin
America." In Peter Evans, Dietrich
Rueschemeyer, and Theda Skocpol
(eds.), Bringing the State Back In:
317-343. Cambridge, MA: Cambridge
University Press.<person-group>
                  <string-name>
                     <surname>Stepan</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">State power and the strength of civil society in the Southern Cone of Latin America</comment>
               <fpage>317</fpage>
               <source>Bringing the State Back In</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d162e2062a1310">
            <mixed-citation id="d162e2066" publication-type="journal">
Stephens, EvelynXI.
1989 "Capitalist development and democ-
racy in South America." Politics and
Society17:281-352.<person-group>
                  <string-name>
                     <surname>Stephens</surname>
                  </string-name>
               </person-group>
               <fpage>281</fpage>
               <volume>17</volume>
               <source>Politics and Society</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d162e2104a1310">
            <mixed-citation id="d162e2108" publication-type="book">
Szelényi, Ivan
1989 "Eastern Europe in an epoch of tran-
sition: Toward a socialist mixed econ-
omy?" In Victor Nee and David Stark
(eds.), Remaking the Economic Insti-
tutions of Socialism in China and
Eastern Europe: 208-232. Stanford,
CA: Stanford University Press.<person-group>
                  <string-name>
                     <surname>Szelényi</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Eastern Europe in an epoch of transition: Toward a socialist mixed economy?</comment>
               <fpage>208</fpage>
               <source>Remaking the Economic Institutions of Socialism in China and Eastern Europe</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d162e2159a1310">
            <mixed-citation id="d162e2163" publication-type="journal">
Therbörn, Goran
1977 "The rule of capital and the rise of
democracy." New Left Review103:
341.<person-group>
                  <string-name>
                     <surname>Therbörn</surname>
                  </string-name>
               </person-group>
               <fpage>341</fpage>
               <volume>103</volume>
               <source>New Left Review</source>
               <year>1977</year>
            </mixed-citation>
            <mixed-citation id="d162e2200" publication-type="journal">
1979"The travail of Latin American de-
mocracy." New Left Review113-114:
71-109.<person-group>
                  <string-name>
                     <surname>Therbörn</surname>
                  </string-name>
               </person-group>
               <fpage>71</fpage>
               <volume>113</volume>
               <source>New Left Review</source>
               <year>1979</year>
            </mixed-citation>
         </ref>
         <ref id="d162e2235a1310">
            <mixed-citation id="d162e2239" publication-type="book">
United Nations
1970-1988Statistical Yearbook. New
York: United Nations, Department of
International Economic and Social
Affairs, Statistical Office.<person-group>
                  <string-name>
                     <surname>United Nations</surname>
                  </string-name>
               </person-group>
               <source>Statistical Yearbook</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d162e2275a1310">
            <mixed-citation id="d162e2279" publication-type="journal">
Valenzuela, J. Samuel
1989 "Labor movements in transitions to
democracy: A framework for analy-
sis." Comparative Politics21:445-472.<object-id pub-id-type="doi">10.2307/422007</object-id>
               <fpage>445</fpage>
            </mixed-citation>
         </ref>
         <ref id="d162e2302a1310">
            <mixed-citation id="d162e2306" publication-type="book">
Wallerstein, Immanuel
1979The Capitalist World-Economy. New
York: Cambridge University Press.<person-group>
                  <string-name>
                     <surname>Wallerstein</surname>
                  </string-name>
               </person-group>
               <source>The Capitalist World-Economy</source>
               <year>1979</year>
            </mixed-citation>
         </ref>
         <ref id="d162e2335a1310">
            <mixed-citation id="d162e2339" publication-type="journal">
Weede, Erich
1982 "The effects of democracy and social-
ist strength on the size distribution of
income." International Journal of
Comparative Sociology23:152-165.<person-group>
                  <string-name>
                     <surname>Weede</surname>
                  </string-name>
               </person-group>
               <fpage>152</fpage>
               <volume>23</volume>
               <source>International Journal of Comparative Sociology</source>
               <year>1982</year>
            </mixed-citation>
         </ref>
         <ref id="d162e2380a1310">
            <mixed-citation id="d162e2384" publication-type="journal">
Weede, Erich, and Joachim Kummer
1985 "Some criticism of recent work on
world system status, inequality, and
democracy." International Journal of
Comparative Sociology26:135-148.<person-group>
                  <string-name>
                     <surname>Weede</surname>
                  </string-name>
               </person-group>
               <fpage>135</fpage>
               <volume>26</volume>
               <source>International Journal of Comparative Sociology</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d162e2425a1310">
            <mixed-citation id="d162e2429" publication-type="book">
World Bank
1987World Development Report. New
York: Oxford University Press.<person-group>
                  <string-name>
                     <surname>World Bank</surname>
                  </string-name>
               </person-group>
               <source>World Development Report</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>Periodicals</title>
         <ref id="d162e2467a1310">
            <mixed-citation id="d162e2471" publication-type="book">
Africa South of the Sahara 1990. London: Europa Publications (1989).<source>Africa South of the Sahara 1990</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d162e2484a1310">
            <mixed-citation id="d162e2488" publication-type="book">
Clements' Encyclopedia of World Governments. Vol. 8 and Vol. 9, Dallas: Political Research
Inc. (1988-89 and 1990-1991).<volume>8</volume>
               <source>Clements' Encyclopedia of World Governments</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d162e2507a1310">
            <mixed-citation id="d162e2511" publication-type="book">
The Far East and Australasia 1989. London: Europa Publishers (1988).<source>The Far East and Australasia 1989</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d162e2524a1310">
            <mixed-citation id="d162e2528" publication-type="book">
The Middle East and North Africa 1990. London: Europa Publishers (1989).<source>The Middle East and North Africa 1990</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d162e2542a1310">
            <mixed-citation id="d162e2546" publication-type="book">
The New African Yearbook. London: I.C. Magazines (1987-1988).<source>The New African Yearbook</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d162e2559a1310">
            <mixed-citation id="d162e2563" publication-type="book">
Political Handbook and Atlas of the World. New York: Simon &amp; Schuster (1970).<source>Political Handbook and Atlas of the World</source>
               <year>1970</year>
            </mixed-citation>
         </ref>
         <ref id="d162e2576a1310">
            <mixed-citation id="d162e2580" publication-type="book">
Worldmark Encyclopedia of the Nations. 7th. ed.: Europe. New York: Worldmark Press (1988).<edition>7</edition>
               <source>Worldmark Encyclopedia of the Nations</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
