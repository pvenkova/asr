<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">clininfedise</journal-id>
         <journal-id journal-id-type="jstor">j101405</journal-id>
         <journal-title-group>
            <journal-title>Clinical Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Chicago Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">10584838</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">4483874</article-id>
         <article-categories>
            <subj-group>
               <subject>Major Articles</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Large-Scale Evaluation of Enzyme-Linked Immunospot Assay and Skin Test for Diagnosis of Mycobacterium tuberculosis Infection against a Gradient of Exposure in the Gambia</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>Philip C. Hill</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>Roger H. Brookes</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>Annette Fox</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>Katherine Fielding</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>David J. Jeffries</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>Dolly Jackson-Sillah</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>Moses D. Lugos</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>Patrick K. Owiafe</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>Simon A. Donkor</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>Abdulrahman S. Hammond</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>Jacob K. Otu</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Tumani</given-names>
                  <surname>Corrah</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Richard A.</given-names>
                  <surname>Adegbola</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>Keith P. W. J. McAdam</string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>01</day>
            <month>4</month>
            <year>2004</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">38</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">7</issue>
         <issue-id>i401969</issue-id>
         <fpage>966</fpage>
         <lpage>973</lpage>
         <page-range>966-973</page-range>
         <permissions>
            <copyright-statement>Copyright 2004 The Infectious Diseases Society of America</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/4483874"/>
         <abstract>
            <p>The purified protein derivative (PPD) skin test for Mycobacterium tuberculosis infection lacks specificity. We assessed 2 more specific M. tuberculosis antigens (ESAT-6 and CFP-10) by enzyme-linked immunospot assay (ELISPOT) compared with PPD by ELISPOT and skin test in The Gambia. Of 735 household contacts of 130 sputum smear-positive tuberculosis cases, 476 (65%) tested positive by PPD ELISPOT, 300 (41%) tested positive by PPD skin test, and 218 (30%) tested positive by ESAT-6/CFP-10 ELISPOT. Only 15 (2%) had positive ESAT-6/CFP-10 results and negative PPD results by ELISPOT. With increasing M. tuberculosis exposure, the percentage of subjects who were PPD skin test positive/ESAT-6/CFP-10 ELISPOT negative increased (P&lt;.001), whereas the percentage of subjects who were PPD skin test negative/PPD ELISPOT positive decreased (P = .011). Eighteen (31%) ESAT-6/CFP-10 ELISPOT-positive subjects in the lowest exposure category had negative PPD skin test results. ESAT-6/CFP-10 ELISPOT probably offers increased specificity in the diagnosis of M. tuberculosis infection in this tropical setting of endemicity, at the cost of some sensitivity.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d771e224a1310">
            <label>1</label>
            <mixed-citation id="d771e231" publication-type="journal">
Small PM, Perkins MD. More rigour needed in trials of new diagnostic
agents for tuberculosis. Lancet2000; 356:1048-9.<person-group>
                  <string-name>
                     <surname>Small</surname>
                  </string-name>
               </person-group>
               <fpage>1048</fpage>
               <volume>356</volume>
               <source>Lancet</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d771e263a1310">
            <label>2</label>
            <mixed-citation id="d771e270" publication-type="journal">
Floyd S, Ponnoghaus JM, Bliss L, et al. Kinetics of delayed-type hy-
persensitivity to tuberculin induced by bacille Calmette-Guerin vac-
cination in Northern Malawi. J Infect Dis2002; 186:807-14.<person-group>
                  <string-name>
                     <surname>Floyd</surname>
                  </string-name>
               </person-group>
               <fpage>807</fpage>
               <volume>186</volume>
               <source>J Infect Dis</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d771e305a1310">
            <label>3</label>
            <mixed-citation id="d771e312" publication-type="journal">
Andersen P, Andersen AB, Sorensen AL, Nagai S. Recall of long-lived
immunity to Mycobacterium tuberculosis infection in mice. J Immunol
1995; 154:3359-72.<person-group>
                  <string-name>
                     <surname>Andersen</surname>
                  </string-name>
               </person-group>
               <fpage>3359</fpage>
               <volume>154</volume>
               <source>J Immunol</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d771e347a1310">
            <label>4</label>
            <mixed-citation id="d771e354" publication-type="journal">
Berthet FX, Rasmussen PB, Rosenkrands I, Andersen P, Gicquel B. A
Mycobacterium tuberculosis operon encoding ESAT-6 and a novel
low-molecular-mass culture filtrate protein (CFP-10). Microbiology
1998; 144:3195-203.<person-group>
                  <string-name>
                     <surname>Berthet</surname>
                  </string-name>
               </person-group>
               <fpage>3195</fpage>
               <volume>144</volume>
               <source>Microbiology</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d771e393a1310">
            <label>5</label>
            <mixed-citation id="d771e400" publication-type="journal">
Demissie A, Ravn P, Olobo J, et al. T-cell recognition of Mycobacterium
tuberculosis culture filtrate fractions in tuberculosis patients, and their
household contacts. Infect Immun1999;67:5967-71.<person-group>
                  <string-name>
                     <surname>Demissie</surname>
                  </string-name>
               </person-group>
               <fpage>5967</fpage>
               <volume>67</volume>
               <source>Infect Immun</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d771e435a1310">
            <label>6</label>
            <mixed-citation id="d771e442" publication-type="journal">
Vekemans J, Lienhardt, Sillah JS, et al. Tuberculosis contacts but not
patients have higher gamma interferon responses to ESAT-6 than do
community controls in The Gambia. Infect Immun2001;69:6554-7.<person-group>
                  <string-name>
                     <surname>Vekemans</surname>
                  </string-name>
               </person-group>
               <fpage>6554</fpage>
               <volume>69</volume>
               <source>Infect Immun</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d771e477a1310">
            <label>7</label>
            <mixed-citation id="d771e484" publication-type="journal">
Lalvani A, Nagvenkar P, Udwadia Z, et al. Enumeration of T cells
specific for RD1-encoded antigens suggests a high prevalence of latent
Mycobacterium tuberculosis infection in healthy urban Indians. J Infect
Dis2001; 183:469-77.<person-group>
                  <string-name>
                     <surname>Lalvani</surname>
                  </string-name>
               </person-group>
               <fpage>469</fpage>
               <volume>183</volume>
               <source>J Infect Dis</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d771e522a1310">
            <label>8</label>
            <mixed-citation id="d771e529" publication-type="journal">
Andersen P, Munk ME, Pollock JM, Doherty TM. Specific immune-
based diagnosis of tuberculosis. Lancet2000; 356:1099-104.<person-group>
                  <string-name>
                     <surname>Andersen</surname>
                  </string-name>
               </person-group>
               <fpage>1099</fpage>
               <volume>356</volume>
               <source>Lancet</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d771e561a1310">
            <label>9</label>
            <mixed-citation id="d771e568" publication-type="journal">
Grey van Pittius NC, Warren RM, van Helden PD. ESAT-6 and CFP-
10: what is the diagnosis? Infect Immun2002; 70:6509-11.<person-group>
                  <string-name>
                     <surname>Grey van Pittius</surname>
                  </string-name>
               </person-group>
               <fpage>6509</fpage>
               <volume>70</volume>
               <source>Infect Immun</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d771e600a1310">
            <label>10</label>
            <mixed-citation id="d771e607" publication-type="book">
National Leprosy and Tuberculosis Programme. Annual report. The
Gambia: KNCV, 2003.<person-group>
                  <string-name>
                     <surname>National Leprosy and Tuberculosis Programme</surname>
                  </string-name>
               </person-group>
               <source>Annual report</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d771e633a1310">
            <label>11</label>
            <mixed-citation id="d771e640" publication-type="book">
Heifets LB, Good RB. Current laboratory methods for the diagnosis
of tuberculosis. In: Bloom BR, ed. Tuberculosis: protection, pathogen-
esis, and control. Washington, DC: American Society for Microbiology,
1994:85-110.<person-group>
                  <string-name>
                     <surname>Heifets</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Current laboratory methods for the diagnosis of tuberculosis</comment>
               <fpage>85</fpage>
               <source>Tuberculosis: protection, pathogenesis, and control</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d771e678a1310">
            <label>12</label>
            <mixed-citation id="d771e685" publication-type="journal">
Lalvani A, Brookes R, Hambleton S, Britton WJ, Hill AV, McMichael
AJ. Rapid effector function in CD+ memory T cells. J Exp Med
1997; 186:859-65.<person-group>
                  <string-name>
                     <surname>Lalvani</surname>
                  </string-name>
               </person-group>
               <fpage>859</fpage>
               <volume>186</volume>
               <source>J Exp Med</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d771e720a1310">
            <label>13</label>
            <mixed-citation id="d771e727" publication-type="journal">
Lienhardt C, Fielding K, Sillah J, et al. Risk factors for tuberculosis
infection in sub-Saharan Africa: a contact study in The Gambia. Am
J Respir Crit Care Med2003; 168:448-55.<person-group>
                  <string-name>
                     <surname>Lienhardt</surname>
                  </string-name>
               </person-group>
               <fpage>448</fpage>
               <volume>168</volume>
               <source>Am J Respir Crit Care Med</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d771e762a1310">
            <label>14</label>
            <mixed-citation id="d771e769" publication-type="journal">
Ulrichs T, Munk ME, Mollenkopf H, et al. Differential T cell responses
to Mycobacterium tuberculosis ESAT6 in tuberculosis patients and
healthy donors. Eur J Immunol1998; 28:3949-58.<person-group>
                  <string-name>
                     <surname>Ulrichs</surname>
                  </string-name>
               </person-group>
               <fpage>3949</fpage>
               <volume>28</volume>
               <source>Eur J Immunol</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d771e804a1310">
            <label>15</label>
            <mixed-citation id="d771e811" publication-type="journal">
Cockle PJ, Gordon SV, Lalvani A, Buddle BM, Hewinson RG, Vor-
dermeier HM. Identification of novel Mycobacterium tuberculosis an-
tigens with potential as diagnostic reagents or subunit vaccine can-
didates by comparative genomics. Infect Immun2002; 70:6996-7003.<person-group>
                  <string-name>
                     <surname>Cockle</surname>
                  </string-name>
               </person-group>
               <fpage>6996</fpage>
               <volume>70</volume>
               <source>Infect Immun</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d771e849a1310">
            <label>16</label>
            <mixed-citation id="d771e856" publication-type="journal">
Ewer K, Deeks J, Alvarez L, et al. Comparison of T-cell-based assay
with tuberculin skin test for diagnosis of Mycobacterium tuberculosis
infection in a school tuberculosis outbreak. Lancet2003; 361:1168-73.<person-group>
                  <string-name>
                     <surname>Ewer</surname>
                  </string-name>
               </person-group>
               <fpage>1168</fpage>
               <volume>361</volume>
               <source>Lancet</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d771e892a1310">
            <label>17</label>
            <mixed-citation id="d771e901" publication-type="journal">
Black GF, Fine PEM, Warndorff DK, et al. Relationship between IFN-
gamma and skin test responsiveness to Mycobacterium tuberculosis PPD
in healthy, non-BCG-vaccinated young adults in northern Malawi. Int
J Tuberc Lung Dis2001; 5:664-72.<person-group>
                  <string-name>
                     <surname>Black</surname>
                  </string-name>
               </person-group>
               <fpage>664</fpage>
               <volume>5</volume>
               <source>Int J Tuberc Lung Dis</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d771e939a1310">
            <label>18</label>
            <mixed-citation id="d771e946" publication-type="journal">
Hoft DF, Brown RM, Belshe RB. Mucosal Calmette-Guerin vaccination
of humans inhibits delayed-type hypersensitivity to purified protein
derivative but induces mycobacteria-specific interferon-y responses.
Clin Infect Dis2000; 30:S217-22.<person-group>
                  <string-name>
                     <surname>Hoft</surname>
                  </string-name>
               </person-group>
               <fpage>S217</fpage>
               <volume>30</volume>
               <source>Clin Infect Dis</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d771e984a1310">
            <label>19</label>
            <mixed-citation id="d771e991" publication-type="journal">
Fine PEM, Bruce J, Ponninghaus JM, Nkhosa P, Harawa A, Vynnycky
E. Tuberculin sensitivity: conversions and reversions in a rural African
population. Int J Tuberc Lung Dis1999; 3:962-75.<person-group>
                  <string-name>
                     <surname>Fine</surname>
                  </string-name>
               </person-group>
               <fpage>962</fpage>
               <volume>3</volume>
               <source>Int J Tuberc Lung Dis</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d771e1026a1310">
            <label>20</label>
            <mixed-citation id="d771e1033" publication-type="journal">
Godkin AJ, Thomas HC, Openshaw PJ. Evolution of epitope-specific
memory CD4(+) T cells after clearance of hepatitis C virus. J Immunol
2002; 169:2210-4.<person-group>
                  <string-name>
                     <surname>Godkin</surname>
                  </string-name>
               </person-group>
               <fpage>2210</fpage>
               <volume>169</volume>
               <source>J Immunol</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d771e1068a1310">
            <label>21</label>
            <mixed-citation id="d771e1075" publication-type="journal">
Pathan AA, Wilkinson KA, Klenerman P, et al. Direct ex vivo analysis
of antigen-specific IFN-y-secreting CD4 T cells in Mycobacterium tu-
berculosis-infected individuals: associations with clinical disease state
and effect of treatment. J Immunol2001; 167:5217-25.<person-group>
                  <string-name>
                     <surname>Pathan</surname>
                  </string-name>
               </person-group>
               <fpage>5217</fpage>
               <volume>167</volume>
               <source>J Immunol</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d771e1113a1310">
            <label>22</label>
            <mixed-citation id="d771e1120" publication-type="journal">
Haile Y, Bjune G, Wiker HG. Expression of the mceA, esat-6 and hspX
genes in Mycobacterium tuberculosis and their responses to aerobic
conditions and to restricted oxygen supply. Microbiology2002; 148:
3881-6.<person-group>
                  <string-name>
                     <surname>Haile</surname>
                  </string-name>
               </person-group>
               <fpage>3881</fpage>
               <volume>148</volume>
               <source>Microbiology</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d771e1159a1310">
            <label>23</label>
            <mixed-citation id="d771e1166" publication-type="journal">
Sherman DR, Voskuil M, Schnappinger D, Liao R, Harrell MI, School-
nik GK. Regulation of the Mycobacterium tuberculosis hypoxic response
gene encoding a-crystallin. PNAS2001; 98:7534-9.<object-id pub-id-type="jstor">10.2307/3056023</object-id>
               <fpage>7534</fpage>
            </mixed-citation>
         </ref>
         <ref id="d771e1185a1310">
            <label>24</label>
            <mixed-citation id="d771e1192" publication-type="journal">
Young TK, Mirdad S. Determinants of tuberculin sensitivity in a child
population covered by mass BCG vaccination. Tuberc Lung Dis
1992; 73:94-100.<person-group>
                  <string-name>
                     <surname>Young</surname>
                  </string-name>
               </person-group>
               <fpage>94</fpage>
               <volume>73</volume>
               <source>Tuberc Lung Dis</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d771e1227a1310">
            <label>25</label>
            <mixed-citation id="d771e1234" publication-type="journal">
Pitman R, Jarman B, Coker R. Tuberculosis transmission and the im-
pact of intervention on the incidence of infection. Int J Tuberc Lung
Dis2002;6:485-91.<person-group>
                  <string-name>
                     <surname>Pitman</surname>
                  </string-name>
               </person-group>
               <fpage>485</fpage>
               <volume>6</volume>
               <source>Int J Tuberc Lung Dis</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d771e1269a1310">
            <label>26</label>
            <mixed-citation id="d771e1276" publication-type="journal">
Mazurek GH, LoBue PA, Daley CL, et al. Comparison of a whole-
blood interferon-y assay with tuberculin skin testing for detecting latent
Mycobacterium tuberculosis infection. JAMA2001; 286:1740-7.<person-group>
                  <string-name>
                     <surname>Mazurek</surname>
                  </string-name>
               </person-group>
               <fpage>1740</fpage>
               <volume>286</volume>
               <source>JAMA</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
