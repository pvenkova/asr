<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="doi">10.2307/j.ctt1bpmbn2</book-id>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>Rethinking Revolution</book-title>
         <subtitle>Socialist Register 2017</subtitle>
      </book-title-group>
      <contrib-group>
         <role content-type="editor">Edited by</role>
         <contrib contrib-type="editor" id="contrib1">
            <name name-style="western">
               <surname>PANITCH</surname>
               <given-names>LEO</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib2">
            <name name-style="western">
               <surname>ALBO</surname>
               <given-names>GREG</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>01</day>
         <month>12</month>
         <year>2016</year>
      </pub-date>
      <isbn content-type="ppub">9781583676332</isbn>
      <isbn content-type="epub">9781583676356</isbn>
      <isbn content-type="epub">158367635X</isbn>
      <publisher>
         <publisher-name>Monthly Review Press</publisher-name>
      </publisher>
      <permissions>
         <copyright-year>2016</copyright-year>
         <copyright-holder>The Merlin Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt1bpmbn2"/>
      <abstract abstract-type="short">
         <p>One hundred years ago, "October 1917" galvanized leftists and oppressed peoples around the globe, and became the lodestar for 20th century politics. Today, the left needs to reckon with this legacy-and transcend it. Social change, as it was understood in the 20th century, appears now to be as impossible as revolution, leaving the left to rethink the relationship between capitalist crises, as well as the conceptual tension between revolution and reform.</p>
         <p>Populated by an array of passionate thinkers and thoughtful activists,<italic>Rethinking Revolution</italic>reappraises the historical effects of the Russian revolution-positive and negative-on political, intellectual, and cultural life, and looks at consequent revolutions after 1917. Change needs to be understood in relation to the distinct trajectories of radical politics in different regions. But the main purpose of this<italic>Socialist Register</italic>edition-one century after "Red October"-is to look forward, to what might happen next.</p>
         <p>Acclaimed authors interrogate and explore compelling issues, including:</p>
         <p>• Greg Albo: New socialist strategies-or detours?</p>
         <p>• Jodi Dean: Are the multitudes communing? Revolutionary agency and political forms today.</p>
         <p>• Adolph Reed: Are racial minorities revolutionary agents?</p>
         <p>• Zillah Eisenstein: Revolutionary feminisms today.</p>
         <p>• Nina Power: Accelerated technology, decelerated revolution.</p>
         <p>• David Schwartzman: Beyond global warming: Is solar communism possible?</p>
         <p>• Andrea Malm: Revolution and counter-revolution in an era of climate change.</p>
      </abstract>
      <counts>
         <page-count count="369"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bpmbn2.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bpmbn2.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bpmbn2.3</book-part-id>
                  <title-group>
                     <title>PREFACE</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <string-name>LP</string-name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <string-name>GA</string-name>
                     </contrib>
                  </contrib-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bpmbn2.4</book-part-id>
                  <title-group>
                     <title>THE DISTINCTIVE HERITAGE OF 1917:</title>
                     <subtitle>RESUSCITATING REVOLUTION’S LONGUE DURÉE</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>PALMER</surname>
                           <given-names>BRYAN D.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>SANGSTER</surname>
                           <given-names>JOAN</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>For many on the revolutionary left, 1917 is an unpleasant apparition, a ghost that haunts us still. Our perspective is different: 1917 lives in our thoughts and actions, our theories and our sensibilities, because it remains a testimony to human agency and the irrepressible potential of revolution. The Bolsheviks, so often castigated as incarcerated in their slavish adherence to the determination of objective conditions, were nothing if not believers in the importance of the subjective factor in the making of history, evident in their own trajectory. The revolution that catapulted the Bolsheviks to power contradicted the prevailing European Marxist orthodoxy</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bpmbn2.5</book-part-id>
                  <title-group>
                     <title>CLASS, PARTY AND THE CHALLENGE OF STATE TRANSFORMATION</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>PANITCH</surname>
                           <given-names>LEO</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>GINDIN</surname>
                           <given-names>SAM</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>35</fpage>
                  <abstract>
                     <p>In 1917, not only those parties engaged in insurrectionary revolution but even those committed to gradual reform still spoke of eventually transcending capitalism. Half a century later social democrats had explicitly come to define their political goals as compatible with a welfare-state variety of capitalism; and well before the end of the century many who had formerly embraced the legacy of 1917 would join them in this. Yet this occurred just as the universalization of neoliberalism rendered threadbare any notion of distinct varieties of capitalism. The realism without imagination of the so-called ‘Third Way’ was shown to lack realism as</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bpmbn2.6</book-part-id>
                  <title-group>
                     <title>THE ACTUALITY OF REVOLUTION</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>DEAN</surname>
                           <given-names>JODI</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>59</fpage>
                  <abstract>
                     <p>For a certain North American and European left, revolution today names more a problem than it does a solution. We know that revolutions happen, but we have a hard time believing in revolution. We have a hard time believing in revolution because we are no longer confident that the revolutionary process leads in an emancipatory egalitarian direction. There are revolutions, but they are not for us, not the revolutions we were hoping for, not proletarian revolutions. And even if today there were or could be revolutions of the proletarianized, they would not be enough. Our goals are far grander (or</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bpmbn2.7</book-part-id>
                  <title-group>
                     <title>RADICALIZING THE MOVEMENT-PARTY RELATION:</title>
                     <subtitle>FROM RALPH MILIBAND TO JEREMY CORBYN AND BEYOND</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>WAINWRIGHT</surname>
                           <given-names>HILARY</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>80</fpage>
                  <abstract>
                     <p>It is time to rethink the relationship between social movements and struggles and parliamentary institutions. Such relationships have a long history, but by the 1970s a common understanding was framed, at least among activists on the left in Europe, by the metaphor of ‘voice’. A widely shared discourse referred to radical left parties, including Green parties, as ‘the political voice of the social movements’. Or at least this was the aspiration. I want to challenge this framework and, in doing so, work towards a framework that enables us to understand better the complexities, contradictions and tensions in the relationships between</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bpmbn2.8</book-part-id>
                  <title-group>
                     <title>THE HERITAGE OF EUROCOMMUNISM IN THE CONTEMPORARY RADICAL LEFT</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>ESCALONA</surname>
                           <given-names>FABIEN</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>102</fpage>
                  <abstract>
                     <p>Though the European radical left has not generally been of interest to mainstream political observers, it has nonetheless recently become the subject of significant media coverage. Such presentations have often fallen into two symmetrical pitfalls. On the one hand, left parties have sometimes been presented as completely new, with their historical development left unexamined. On the other hand, many editorialists and even academics have paid little attention to what is original about these organizations: some have seen them as a disagreeable avatar of the far left, others as a resurgent ‘traditional’ (and thus inoffensive) social democracy. In fact, the parties</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bpmbn2.9</book-part-id>
                  <title-group>
                     <title>REVOLUTION IN A WARMING WORLD:</title>
                     <subtitle>LESSONS FROM THE RUSSIAN TO THE SYRIAN REVOLUTIONS</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>MALM</surname>
                           <given-names>ANDREAS</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>120</fpage>
                  <abstract>
                     <p>It doesn’t take much imagination to associate climate change with revolution. If the planetary order upon which all societies are built starts breaking down, how can they possibly remain stable? Various more or less horrifying scenarios of upheaval have long been extrapolated from soaring temperatures. In his novel<italic>The Drowned World</italic>from 1962, today often considered the first prophetic work of climate fiction, J. G. Ballard conjured up melting icecaps, an English capital submerged under tropical marshes and populations fleeing the unbearable heat towards polar redoubts. The UN directorate seeking to manage the migration flows assumed that ‘within the new</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bpmbn2.10</book-part-id>
                  <title-group>
                     <title>BEYOND ECO-CATASTROPHISM:</title>
                     <subtitle>THE CONDITIONS FOR SOLAR COMMUNISM</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>SCHWARTZMAN</surname>
                           <given-names>DAVID</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>143</fpage>
                  <abstract>
                     <p>In a vivid, much-cited and much-abused passage, Karl Marx put forward one of his boldest formulations:</p>
                     <p>The bourgeois mode of production is the last antagonistic form of the social process of production – antagonistic not in the sense of individual antagonism but of an antagonism that emanates from the individuals’ social conditions of existence – but the productive forces developing within bourgeois society create also the material conditions for a solution of this antagonism. The prehistory of human society accordingly closes with this social formation.¹</p>
                     <p>Indeed the reproduction of capital, utilizing productive forces powered by fossil fuels, created the material</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bpmbn2.11</book-part-id>
                  <title-group>
                     <title>SOUTH AFRICA’S NEXT REVOLT:</title>
                     <subtitle>ECO-SOCIALIST OPPORTUNITIES</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>BOND</surname>
                           <given-names>PATRICK</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>161</fpage>
                  <abstract>
                     <p>The political ecology of South Africa reflects extreme uneven and combined development. As one crucial aspect of this condition, environmental degradation extends deep into the households and workplaces populated mainly by the country’s black majority. The regroupment of the socialist movement will, in the coming months and years, have to embrace the environmental challenge just as profoundly as it will need to reindustrialize the economy under worker and social control, while also redistributing wealth and restructuring the reproduction of labour power in a humane, rational manner to spread what is now women’s caregiving responsibilities properly. As these overlapping processes unfold,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bpmbn2.12</book-part-id>
                  <title-group>
                     <title>TURNING THE TIDE:</title>
                     <subtitle>REVOLUTIONARY POTENTIAL AND THE LIMITS OF BOLIVIA’S ‘PROCESS OF CHANGE’</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>CAVOORIS</surname>
                           <given-names>ROBERT</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>186</fpage>
                  <abstract>
                     <p>Revolutionary energy in Latin America today seems to be at a lull. During the last 20 years, leftist heads of state Hugo Chávez, Evo Morales, and Rafael Correa have made radical-sounding pronouncements denouncing capitalism, standing up to imperialism, and capturing the imagination of the international Left with a bit of panache. But it appears that the time of such leaders is coming to a close; Cristina Fernandez Kirchner’s chosen successor in Argentina, already a moderate choice, was defeated at the presidential polls at the end of 2015 by the notoriously corrupt right-wing politician Mauricio Macri, and shortly thereafter Venezuela’s<italic>Chavistas</italic>
                     </p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bpmbn2.13</book-part-id>
                  <title-group>
                     <title>SOMETHING LEFT IN LATIN AMERICA:</title>
                     <subtitle>VENEZUELA AND THE STRUGGLE FOR TWENTY-FIRST CENTURY SOCIALISM</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>STRIFFLER</surname>
                           <given-names>STEVE</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>207</fpage>
                  <abstract>
                     <p>The Latin American left at the start of the 1990s looked a lot like its counterparts throughout much of the world. It was a fragment of its former self. Military regimes had wiped out much of the left in South America during the 1970s and 1980s, and counterinsurgency had finished the job in Central America by 1990. Nor was there much reason for hope. Structural adjustment reigned supreme. Labour was on the defensive, unable to mount much of a challenge to neoliberal policies that were decimating the working class and its capacity to fight. The countryside was eerily quiet. Peasants</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bpmbn2.14</book-part-id>
                  <title-group>
                     <title>IN SEARCH OF THE ‘MODERN PRINCE’:</title>
                     <subtitle>THE NEW QUÉBEC REBELLION</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>BEAUDET</surname>
                           <given-names>PIERRE</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>230</fpage>
                  <abstract>
                     <p>Since the beginning of the millennium, there have been many experiences of mass struggles and even popular insurrections in various regions of the world. The most far-reaching have occurred in South America, where some neoliberal governments have been forced to retreat by popular pressure, leaving the space for new left governments. Unprecedented mass mobilizations have shaken other parts of the capitalist ‘periphery’ – the Arab Spring in North Africa and the Middle East, the popular uprisings in Nepal, Thailand, Burkina Faso, Guadeloupe, etc. On a lesser scale, there have been the mobilizations of the Indignados in southern Europe, Occupy in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bpmbn2.15</book-part-id>
                  <title-group>
                     <title>MARX AND ENGELS ON THE REVOLUTIONARY PARTY</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>NIMTZ</surname>
                           <given-names>AUGUST H.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>247</fpage>
                  <abstract>
                     <p>Engels began his brief remarks at Marx’s funeral in 1883 by describing his life-long political companion’s ‘scientific’ accomplishments. ‘But he looked upon science above all things as a grand historical lever, as a revolutionary power in the most eminent sense of the word … For he was indeed, what he called himself, a Revolutionist’.¹ As his closest collaborator, Engels knew better than anyone about this indispensable dimension of Marx’s project. If it wasn’t enough, as the young Marx had concluded in 1845, to ‘interpret the world’ but also necessary ‘to change it’, then action and organization were essential. Yet nowhere</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bpmbn2.16</book-part-id>
                  <title-group>
                     <title>1917 AND THE ‘WORKERS’ STATE’:</title>
                     <subtitle>LOOKING BACK</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>ZURBRUGG</surname>
                           <given-names>A W</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>265</fpage>
                  <abstract>
                     <p>Lenin’s<italic>State and Revolution</italic>was one of the most famous texts of the twentieth century. Written in 1917 before the October revolution and published shortly after, it looked forward to a new polity, one that should be fully democratic, with a ‘dictatorship of the proletariat’ creating ‘democracy for the people’.¹ The substance of the work was a review of Marx and Engels texts (the Paris Commune especially). Lenin did not ask if these texts were suited to framing a discussion of current issues. His discussion assumed that a Marxist party’s thinking should predominate. He did not consider differences between artisan</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bpmbn2.17</book-part-id>
                  <title-group>
                     <title>THE ‘PEOPLE’S WAR’ AND THE LEGACY OF THE CHINESE REVOLUTION</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>HUI</surname>
                           <given-names>WANG</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>286</fpage>
                  <abstract>
                     <p>The disintegration of the Soviet bloc after 1989 marked the beginning of the end of the revolutionary socialist movement that originated in the nineteenth century and was consolidated by the twentieth-century Communist regimes. The end of the ideological confrontations of the cold war brought proclamations both of the ‘end of history’ as well as of a new ‘clash of civilizations’. These two opposing theses together declared the end of twentieth-century politics: national liberation, land reform, class struggle, state and revolution have all apparently become anachronisms. In parallel with this, the power of capital overwhelmed the obstacles that the socialist movement</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bpmbn2.18</book-part-id>
                  <title-group>
                     <title>REVOLUTION AS ‘NATIONAL LIBERATION’ AND THE ORIGINS OF NEOLIBERAL ANTIRACISM</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>REED</surname>
                           <given-names>ADOLPH</given-names>
                           <suffix>JR</suffix>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>299</fpage>
                  <abstract>
                     <p>This essay, like this volume as a whole, is motivated by the centennial of 1917 providing occasion for reflection on the great revolutionary projects of the last century and rumination on the status of the notion of revolution now. My concern is fundamentally ‘presentist’ and best characterized as demystification or ideology-critique. Specifically, my interest is in reflecting on the emergence of antiracism as a discrete political stance – that is, not simply a principled opposition to discrimination and bigotry – and the impact that it, along with other strains of what is commonly called identity politics, has had on contemporary</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bpmbn2.19</book-part-id>
                  <title-group>
                     <title>PICTURING THE WHOLE:</title>
                     <subtitle>FORM, REFORM, REVOLUTION</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>MICHAELS</surname>
                           <given-names>WALTER BENN</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>323</fpage>
                  <abstract>
                     <p>Talking about the political ‘Uses of Photography,’ John Berger says ‘most photographs … are about suffering, and most of that suffering is man-made’.¹ The reason the point is worth making, he thinks, is because a lot of photography conceals it, his exemplary instance being Edward Steichen’s famous<italic>Family of Man</italic>show (1955), which – treating ‘the existing class-divided world as if it were a family’ – produced a ‘sentimental and complacent’ vision of that world. It’s not that Steichen was wrong to want to think of ‘man’ as belonging to one family. The sentimentality was in depicting the world as</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bpmbn2.20</book-part-id>
                  <title-group>
                     <title>ADDRESSING THE IMPOSSIBLE</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>ŽIŽEK</surname>
                           <given-names>SLAVOJ</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>339</fpage>
                  <abstract>
                     <p>It has been claimed that the main reason for the decline of social utopia in the last decades is that, due to technological progress, we no longer need to resort to utopias since in our reality itself (almost) everything is now possible. However, this sense of unlimited possibilities is accompanied by a set of impossibilities: today the very idea of a radical social transformation appears as an impossible dream – and the term ‘impossible’ should make us stop and think. Impossible and possible are distributed in a strange way, both simultaneously exploding into an excess.</p>
                     <p>On the one hand, in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bpmbn2.21</book-part-id>
                  <title-group>
                     <title>ON REVOLUTIONARY OPTIMISM OF THE INTELLECT</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>PANITCH</surname>
                           <given-names>LEO</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>356</fpage>
                  <abstract>
                     <p>Antonio Gramsci’s words here, written in a letter from prison to his brother Carlo in December 1929, provide useful perspective on the famous slogan, ‘pessimism of the intellect, optimism of the will’ so often wrongly attributed to Gramsci himself. In fact, he borrowed it from Romain Rolland to describe (in an article in<italic>L’Ordine Novo</italic>during the Turin general strike of April 1920) the traits of ‘the socialist conception of the revolutionary process’ in contrast with those anarchists who presented themselves as ‘the repository of revealed revolutionary truth … letting off steam with the satisfied observation: “We have said it</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bpmbn2.22</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>365</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
