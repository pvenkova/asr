<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1t6p4z7</book-id>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
         <subject>Anthropology</subject>
         <subject>Sociology</subject>
      </subj-group>
      <book-title-group>
         <book-title>The Political Economy of Everyday Life in Africa</book-title>
         <subtitle>Beyond the Margins</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="editor" id="contrib1">
            <role>Edited by</role>
            <name name-style="western">
               <surname>Adebanwi</surname>
               <given-names>Wale</given-names>
            </name>
         </contrib>
         <contrib contrib-type="foreword-author" id="contrib2">
            <role>Foreword by</role>
            <name name-style="western">
               <surname>Ferguson</surname>
               <given-names>James</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>16</day>
         <month>06</month>
         <year>2017</year>
      </pub-date>
      <isbn content-type="ppub">9781847011657</isbn>
      <isbn content-type="epub">9781787440517</isbn>
      <publisher>
         <publisher-name>Boydell &amp; Brewer</publisher-name>
         <publisher-loc>Woodbridge, Suffolk; Rochester, NY</publisher-loc>
      </publisher>
      <edition>NED - New edition</edition>
      <permissions>
         <copyright-year>2017</copyright-year>
         <copyright-holder>Contributors</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7722/j.ctt1t6p4z7"/>
      <abstract abstract-type="short">
         <p>What are the fundamental issues, processes, agency and dynamics that shape the political economy of life in modern Africa? In this book, the contributors - experts in anthropology, history, political science, economics, conflict and peace studies, philosophy and language - examine the opportunities and constraints placed on living, livelihoods and sustainable life on the continent. Reflecting on why and how the political economy of life approach is essential for understanding the social process in modern Africa, they engage with the intellectual oeuvre of the influential Africanist economic anthropologist Jane Guyer, who provides an Afterword. The contributors analyse the political economy of everyday life as it relates to money and currency; migrant labour forces and informal and formal economies; dispossession of land; debt and indebtedness; socio-economic marginality; and the entrenchment of colonial and apartheid pasts. Wale Adebanwi is the Rhodes Professor of Race Relations at the University of Oxford. He is author of Nation as Grand Narrative: The Nigerian Press and the Politics of Meaning (University of Rochester Press).</p>
      </abstract>
      <counts>
         <page-count count="384"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t6p4z7.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t6p4z7.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t6p4z7.3</book-part-id>
                  <title-group>
                     <title>Maps, Illustrations &amp; Tables</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t6p4z7.4</book-part-id>
                  <title-group>
                     <title>Notes on Contributors</title>
                  </title-group>
                  <fpage>x</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t6p4z7.5</book-part-id>
                  <title-group>
                     <title>Foreword</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Ferguson</surname>
                           <given-names>James</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>xvii</fpage>
                  <abstract>
                     <p>This volume presents an exciting collection of recent scholarship addressing some of the most important and intellectually consequential issues in contemporary African studies. It is inspired by, and achieves its thematic coherence in relation to, the extraordinarily profound and extensive contributions to that field by Jane I. Guyer over the last four decades. Guyer’s insights have extended widely across a range of foundational issues in African studies, but they have largely clustered around a set of themes and questions that animate the contributions to this volume. These include gender and household economy; wealth, knowledge and personhood; currencies, money and value;</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t6p4z7.6</book-part-id>
                  <title-group>
                     <title>Acknowledgements</title>
                  </title-group>
                  <fpage>xix</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t6p4z7.7</book-part-id>
                  <title-group>
                     <title>Approaching the Political Economy of Everyday Life</title>
                     <subtitle>An Introduction</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Adebanwi</surname>
                           <given-names>Wale</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>Africa is often encountered as a continent in which all manners of extremities converge. Abundant natural resources coexist with extreme poverty;¹ excessive wealth and conspicuous consumption are exhibited in the context of widespread immiseration. In the global imagination, life in Africa is characterized by excess and abjection: the excess of natural wealth and the abjection of pervasive poverty. In recent decades, due to a myriad of economic crises, political instability and social paralysis, the extremities of wealth and poverty have produced a progressive eradication of the middle ground between human happiness and human misery. Thus, much of the scholarship on</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t6p4z7.8</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Cattle, Currencies &amp; the Politics of Commensuration on a Colonial Frontier</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <string-name>Jean</string-name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Comaroff</surname>
                           <given-names>John L.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>35</fpage>
                  <abstract>
                     <p>Encounters between different regimes of value – regimes divided by cultural space and time – presume mediation, translation, communication and, therefore, currencies, at once verbal and material, that objectify them. This, in turn, depends on one thing above all else: on mechanisms of commensuration, mechanisms that render negotiable otherwise inimical, apparently intransitive, orders of signs and practices. Without such mechanisms, which have often been the object of conflict and contestation, large-scale historical projects, like colonialism, would have made no sense, neither as a world -undertaking on the part of colonizers nor as a lived reality to those upon whose worlds it was</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t6p4z7.9</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>Currency &amp; Conflict in Colonial Nigeria</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Pratten</surname>
                           <given-names>David</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>72</fpage>
                  <abstract>
                     <p>This chapter examines a central theme in Jane Guyer’s analysis of monetary transactions in Atlantic Africa – the implications of money valuation in relation to social order and disorder in Nigeria. It revisits the case of manilla currency exchange that she has discussed extensively in relation to calculation, ranking and political legitimacy (Guyer 2004, 2009). The manilla exchange rate is often ignored as a contributory factor in the various social upheavals of the region in the period before the manilla was ‘redeemed’ in 1948.¹ Following Guyer’s lead, and drawing on the pioneering scholarship of Naanen (1993), this chapter argues that manilla</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t6p4z7.10</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Coercion or Trade?</title>
                     <subtitle>Multiple Self-realization during the Rubber Boom in German Kamerun (1899–1913)</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Oestermann</surname>
                           <given-names>Tristan</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Geschiere</surname>
                           <given-names>Peter</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>92</fpage>
                  <abstract>
                     <p>One of the most inspiring ideas in Jane Guyer’s rich oeuvre is her consistent attention to ‘marginal gains’ to be made by crossing disjuncture between different registers of value and knowledge. Especially her work from the 1990s on the role of ‘composition’ (in contrast to ‘accumulation’) as characteristic to economic agency in Equatorial Africa was most inspiring for both of us in our research in that area – notably for our efforts to make sense of the quite chaotic developments during the boom in wild rubber that marked so deeply the brief period of German rule in Cameroon (see Guyer 1993,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t6p4z7.11</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>The Macroeconomics of Marginal Gains</title>
                     <subtitle>Africa’s Lessons to Social Theorists</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Monga</surname>
                           <given-names>Célestin</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>115</fpage>
                  <abstract>
                     <p>In his 1974 Nobel Prize lecture titled ‘The Pretence of Knowledge’, Friedrich von Hayek chastised his fellow economists about their love affair with analytical approaches that mimicked physics or biology, especially after their discipline had ‘been conceded some of the dignity and prestige of the physical sciences’. He argued forcefully and convincingly that some of the gravest errors of economic policy are a direct consequence of this ‘scientistic error’ (von Hayek 1974). Decades later, his criticism of economic methodology in general is still valid and has been endorsed by a wide range of economists, including some of the most influential</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t6p4z7.12</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>From Enslavement to Precarity?</title>
                     <subtitle>The Labour Question in African History</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Cooper</surname>
                           <given-names>Frederick</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>135</fpage>
                  <abstract>
                     <p>It is a particular pleasure to contribute to this engagement with Jane Guyer’s work. Jane and I have known each other since the beginning of both our careers. We were junior faculty members at Harvard, Jane in Anthropology, me in History, during the late 1970s and we were both part of a lively community of Africanists in the Boston area. Since Harvard had a tradition of faculty not talking to each other – and African studies there was particularly moribund at the time – we usually got together at Boston University’s African Studies Center, for a time located in a rambling old</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t6p4z7.13</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Navigating Formality in a Migrant Labour Force</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Bolt</surname>
                           <given-names>Maxim</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>157</fpage>
                  <abstract>
                     <p>Stack after stack of pallets, each piled high with crates of oranges, await the trucks that will take them from the farm of Grootplaas, located on South Africa’s border with Zimbabwe, to the port of Durban on South Africa’s Indian Ocean coast. From there, ships will take them to countries across Europe, the Middle East and East Asia. Different crate designs signal different agents, buyers and brands. The logos of British supermarkets and American citrus providers (even though South African oranges reportedly cannot be sold in the United States) jostle with those of Capespan fruit agents and the local Limpopo</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t6p4z7.14</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Precarious Life</title>
                     <subtitle>Violence &amp; Poverty under Boko Haram &amp; MEND</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Watts</surname>
                           <given-names>Michael J.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>179</fpage>
                  <abstract>
                     <p>This chapter engages with three broad themes addressed in Jane Guyer’s (2007, 2016a, 2016b) recent work: temporalities and near futures (and especially the temporalities of immediate precarity and deep or structural time), modernity’s aporias (the doctrines of the religious and the secular in relation to the contradictions and failures of modern secular national development) and the figure of the pauper or more properly the relations of violence and poverty. Much of Jane’s work has of course built upon her hugely influential exploration of the cultural and political economy of Nigeria, a part of the world that has struggled throughout its</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t6p4z7.15</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>The Debt Imperium</title>
                     <subtitle>Relations of Owing after Apartheid</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Makhulu</surname>
                           <given-names>Anne-Maria</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>216</fpage>
                  <abstract>
                     <p>In sub-Saharan Africa in the last decade or so ‘bottom-of-the-pyramid’ (BoP) or ‘bottom billion capitalism’ has taken off. So has the associated scholarship (see for example Cross and Street 2009; Dolan and Roll 2013; Blowfield and Dolan 2014). The literature spans two distinct sets of disciplines: economics, business and development studies, on one hand, keen to think about the global margins as sites of new opportunity, and those disciplines in the interpretive social sciences and humanities, on the other, that see a new market logic at work on the global limits and about which there is some significant scepticism. In</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t6p4z7.16</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>Marginal Men &amp; Urban Social Conflicts</title>
                     <subtitle>Okada Riders in Lagos</subtitle>
                  </title-group>
                  <fpage>239</fpage>
                  <abstract>
                     <p>In her preface to the important volume,<italic>Money Struggles and City Life</italic>, Jane Guyer (2002: ix-xvi) raises interesting questions about the ways in which those of us who live and work in urban Africa witness what de Certeau (1984) describes as the ‘practice of everyday life’, which ‘encompasses systems of employment, provisioning, and meaning-making of impressive magnitude and relentless resilience’ (Guyer 2002: ix). In reflecting on the ‘domain of human struggles and achievement’ – particularly in Ibadan and other urban centres in southern Nigeria – within which ‘chronic uncertainty is pervasive’ (ibid.: x), Guyer argues that ‘(c) ase studies have to be</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t6p4z7.17</book-part-id>
                  <title-group>
                     <label>10</label>
                     <title>Sopona, Social Relations &amp; the Political Economy of Colonial Smallpox Control in Ekiti, Nigeria</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Renne</surname>
                           <given-names>Elisha P.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>266</fpage>
                  <abstract>
                     <p>As the terrifying experience of Professor Adelola Adeloye in Ikole-Ekiti in 1945 indicates, Sopona or Baba – as the deity associated with the disease, smallpox, was known – was lethal for some of its victims while others survived. Yet, Adeloye had been vaccinated in school. The unpredictable consequences of contracting smallpox reinforced local beliefs about the deity, Sopona, whose arbitrary power was often immune to the blandishments of Western medicine. Not surprisingly, Sopona was widely feared throughout Ekiti, which affected people’s actions when it appeared. People were forbidden to ‘cry and mourn over a victim of smallpox’ for fear of provoking the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t6p4z7.18</book-part-id>
                  <title-group>
                     <label>11</label>
                     <title>History as Value Added?</title>
                     <subtitle>Valuing the Past in Africa</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Berry</surname>
                           <given-names>Sara</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>287</fpage>
                  <abstract>
                     <p>Historical narratives figure prominently in contemporary African affairs. Since the late 1980s, international initiatives seeking to ‘democratize’ African governments and decentralize governing institutions have prompted widespread debate, sometimes conflict, over land claims, citizenship, eligibility for political office, and the relevance of historical precedents for social and political entitlements in the present. Describing the return of electoral politics to a northern Beninese town in the early 1990s, Bako-Arifari wrote that ‘history [was] a subject of permanent discussion’ (1997: 6). By linking authority and resource access to precise delineations of territory and ownership, neoliberal projects for protecting property rights and decentralization intensified</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t6p4z7.19</book-part-id>
                  <title-group>
                     <label>12</label>
                     <title>Cultural Mediation, Colonialism &amp; Politics</title>
                     <subtitle>Colonial ‘Truchement’, Postcolonial Translator</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Diagne</surname>
                           <given-names>Souleymane Bachir</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>308</fpage>
                  <abstract>
                     <p>Let me start with a scene from a book by anthropologist, historian, novelist, but above all interpreter then translator of West African oral cultures and literatures into French: Amadou Hampâté Bâ (1901–1991) from Mali. The book is entitled<italic>Vie et enseignement de Tierno Bokar le Sage de Bandiagara</italic>(literally: ‘The life and teachings of Tierno Bokar, the Sage of Bandiagara’) and is the biography of his master and guide in Islamic sciences and spirituality (Tierno means ‘master’). The scene in question takes place in 1937, in the Malian town of Mopti under French colonial rule. The<italic>dramatis personae</italic>are</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t6p4z7.20</book-part-id>
                  <title-group>
                     <label>13</label>
                     <title>‘Kos’ona Miran?’</title>
                     <subtitle>Patronage, Prebendalism &amp; Democratic Life in Contemporary Nigeria</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Agbaje</surname>
                           <given-names>Adigun</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>318</fpage>
                  <abstract>
                     <p>In this chapter, I offer a view on the prospects for democracy in Nigeria against a backdrop of Jane Guyer’s timeless concern with the dynamics of autonomy, resilience, co-optation and capitulation of micro (marginal, local) processes, structures and actors in their daily interactions and negotiations with the macro (dominant, national). In this regard, I attempt an outline of elements of continuity, change and the suspended – if restive and tense – state between change and continuity, in the role of patronage in the evolving democratic possibilities in Nigeria’s Fourth Republic. I do all this in the specific context of Jane Guyer’s enduring</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t6p4z7.21</book-part-id>
                  <title-group>
                     <title>AFTERWORD</title>
                     <subtitle>The Landscapes Beyond the Margins Agency, Optimization &amp; the Power of the Empirical</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Guyer</surname>
                           <given-names>Jane I.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>335</fpage>
                  <abstract>
                     <p>This collection exemplifies the kinds of engagements across disciplines, subjects of study, and areas of the world that are so important to initiate and sustain, once we have recognized the fruitfulness of all the connections that provoke commitment to the intellectual tasks of the twenty-first century, appreciation of originality, and a stamina in our scholarship that can do justice to the commitment, originality and stamina of our colleagues and the world we study. The authors all represent disciplines with which I have tried to work – history, geography, economics, political studies, philosophy and broad subfields within anthropology – in ways that can</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t6p4z7.22</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>353</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t6p4z7.23</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>365</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
