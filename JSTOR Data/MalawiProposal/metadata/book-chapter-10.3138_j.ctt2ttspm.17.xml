<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt2ttspm</book-id>
      <subj-group>
         <subject content-type="call-number">S944.5.I57W55 1993</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Conservation of natural resources</subject>
         <subj-group>
            <subject content-type="lcsh">International cooperation</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Environmental policy</subject>
         <subj-group>
            <subject content-type="lcsh">International cooperation</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Business</subject>
         <subject>Environmental Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>North, South, and the Environmental Crisis</book-title>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>WHITE</surname>
               <given-names>RODNEY R.</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>08</day>
         <month>01</month>
         <year>1993</year>
      </pub-date>
      <isbn content-type="ppub">9780802068859</isbn>
      <isbn content-type="epub">9781442677937</isbn>
      <publisher>
         <publisher-name>University of Toronto Press</publisher-name>
         <publisher-loc>Toronto; Buffalo; London</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>1993</copyright-year>
         <copyright-holder>University of Toronto Press Incorporated</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.3138/j.ctt2ttspm"/>
      <abstract abstract-type="short">
         <p>Rodney White's clearly written, accessible book describes the basic scientific changes taking place in the environment in ways that can be understood by non-scientists.</p>
      </abstract>
      <counts>
         <page-count count="214"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2ttspm.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2ttspm.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2ttspm.3</book-part-id>
                  <title-group>
                     <title>List of Tables and Figures</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2ttspm.4</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>WHITE</surname>
                           <given-names>RODNEY</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2ttspm.5</book-part-id>
                  <title-group>
                     <title>Acronyms</title>
                  </title-group>
                  <fpage>xv</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2ttspm.6</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Snapshot of a Changing Earth</title>
                  </title-group>
                  <fpage>3</fpage>
                  <abstract>
                     <p>In its pursuit of material prosperity the human species has proliferated so quickly that it may well destroy the only home it has. People are not equal in their struggle for access to resources. Some are rich, and live in green and pleasant suburbs or modern cities; others are trapped halfway along the path to material wealth, in polluted villages and towns; while many scratch a meagre living on tiny farms threatened by drought and soil erosion. The gap between rich countries and poor countries is growing, and the global environmental crisis may force them even further apart.</p>
                     <p>The distinction</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2ttspm.7</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>A Framework for Analysis:</title>
                     <subtitle>Systems, Cycles, and Transitions</subtitle>
                  </title-group>
                  <fpage>22</fpage>
                  <abstract>
                     <p>How can we begin to even think about the complex interconnections between human beings and their global home? The natural world was already a highly complex system even before people made it more complicated by changing the composition of the atmosphere, diverting rivers, and extinguishing species. In this chapter we will begin to assess the evolving human impact as people change their diet, consume more energy, extend their life expectancy, and increase their number. The scope is vast even if we limit our concerns to that part of the global environment known as the biosphere, which is ‘that portion of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2ttspm.8</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Global Warming</title>
                  </title-group>
                  <fpage>39</fpage>
                  <abstract>
                     <p>The topic of global warming has become extremely contentious. Although the weight of scientific opinion inclines firmly towards the belief that human activities will result in the steady warming of the atmosphere, there is what the<italic>Economist</italic>described as a backlash against this opinion (see the conclusion of the preceding chapter). That such a backlash should occur is not at all surprising given that the database is still small, given the uncertainty of the implications of such a warming, and given the tremendous changes in behaviour that the acceptance of a belief in global warming would imply.</p>
                     <p>Before the issues</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2ttspm.9</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Ozone and CFCs</title>
                  </title-group>
                  <fpage>56</fpage>
                  <abstract>
                     <p>Ozone (O₃) is a pungent, trace gas of which the molecule consists of three atoms of oxygen, rather than the two atoms found in the oxygen molecule, on which humans and other animals depend for respiration. The ozone problem is a more difficult topic to grasp than global warming (or acid rain perhaps) because it is really two problems, not one. The duality of the problem lies in the fact that ozone, from the human perspective, plays both a beneficial role in the upper atmosphere and a harmful role near the ground. At the higher levels, as described by Mostafa</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2ttspm.10</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Acid Rain – The Global Implications</title>
                  </title-group>
                  <fpage>67</fpage>
                  <abstract>
                     <p>Acid rain was the subject of the first atmospheric ‘transboundary dispute’ to force its way into the popular imagination and onto the political agenda. In everyday life, the term ‘acid’ suggests corrosion; clearly, it is something that, in large quantities, would be unhealthy. Coming down with the rain it sounds distinctly threatening.</p>
                     <p>The scientific term ‘acid precipitation’ includes acidic deposition from snow, fog, dew, and cloud moisture, as well as the more familiar acid rain. It is the product of the reaction, in the atmosphere, of water with sulphur dioxide (SO₂) and nitric oxide and nitrogen dioxide (NO, NO₂, symbolized</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2ttspm.11</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Management of the Ocean</title>
                  </title-group>
                  <fpage>81</fpage>
                  <abstract>
                     <p>The last three chapters have dealt with the environmental implications of the changing composition of the atmosphere due to human interference with natural bio-geochemical cycles. It has been difficult for the nations of the earth to address these problems through traditional forums because until this century the atmosphere was regarded as a ‘free good,’ belonging to no person and no nation. Only with the invention of aircraft did the notion of national airspace emerge. The development of telecommunications brought in further need for management of the air.</p>
                     <p>Humankind has had much longer experience in regulating the rights of nations to</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2ttspm.12</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Land for Food and Energy</title>
                  </title-group>
                  <fpage>95</fpage>
                  <abstract>
                     <p>The complexities of the atmosphere and the oceans are unfamiliar to most people, being remote from their everyday concerns. The use of land, however, has direct daily consequences for everyone, whether a commuter in the North or a street-vendor in the South. Thus, it is the land issues that provide the most immediate problems for resolution between the North and the South. How much land does a family need to support itself? In the rich countries of the North this is a difficult calculation to make beyond the immediate allocations for a home, place of work, school, transportation, shops, hospitals,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2ttspm.13</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>Water Supply</title>
                  </title-group>
                  <fpage>114</fpage>
                  <abstract>
                     <p>Globally, there is no shortage of water per se. The problem is the cost of making water available at a particular place. Figure 8.1 illustrates the difference between absolute water supplies and available (or potentially available) water for domestic and agricultural use. Roughly, of the world’s total water only 3 per cent is fresh, and of that 3 per cent less than 1 per cent is available at the surface; a further 22 per cent is groundwater (i.e., underground water), some of which can be drawn up or pumped up for human use. Of the water that is available at</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2ttspm.14</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>Urban Management</title>
                  </title-group>
                  <fpage>129</fpage>
                  <abstract>
                     <p>In Chapter 1, urbanization, industrialization, and the increase in population were identified as the driving variables behind human-induced global environmental change. As we have seen, the residuals from urban-industrial society have changed global bio-geochemical cycles. In other chapters we have seen just how widespread those impacts have become, stretching far beyond human settlements (which are the sources of change) to the ocean depths and the polar deserts. Why has our civilization evolved in this way, and how well equipped are we to reverse the negative side-effects of our global expansion? To understand the dynamics behind these processes we must look</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2ttspm.15</book-part-id>
                  <title-group>
                     <label>10</label>
                     <title>Waste Management</title>
                  </title-group>
                  <fpage>145</fpage>
                  <abstract>
                     <p>In earlier chapters we reviewed agricultural and industrial processes which produce residual material that is potentially harmful to human beings, either directly or through the food chain, or that constitute a more indirect threat, like ozone depletion and carbon dioxide build-up. Many of these residuals, such as carbon dioxide, sulphur dioxide, CFCs, and so on, have entered the global bio-geochemical cycles and have begun to modify them. Decisions to control the growth of these by-products can be resolved only by international agreements.</p>
                     <p>Other toxic wastes, such as heavy metals and PCBs, have a more localized impact and can be controlled,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2ttspm.16</book-part-id>
                  <title-group>
                     <label>11</label>
                     <title>The Environment and International Relations</title>
                  </title-group>
                  <fpage>160</fpage>
                  <abstract>
                     <p>Environmental issues have always played a part in international relations. Mostly, they have been the subject of acrimonious disputes. Examples include disputes over international fishing quotas, over water allowances from international rivers, and over the resettlement of refugees from drought. However, never before have so many large and difficult issues appeared over such a short period of time. Ten years ago almost all environmental problems were dealt with (if at all) within national boundaries. Now, many of the most serious problems cannot be resolved in this way. Instead they require very broad and very long-term understandings among all the world’s</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2ttspm.17</book-part-id>
                  <title-group>
                     <label>12</label>
                     <title>Priorities for Analysis and Mitigation</title>
                  </title-group>
                  <fpage>172</fpage>
                  <abstract>
                     <p>The details of the many ways in which the expansion of the human domain has affected the environment can be confusing. But the big picture is really very simple, especially when we think of people as part of one of Howard Odum’s simple producer/consumer ecosystems. The big picture can be reduced to three propositions. First, improvements in material prosperity stimulate a rapid increase in population; then, once longevity is an established fact, populations stabilize, and may even decline. The first stage in this transition is now taking place among the poorest half of the world’s people. Completion of this transition</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2ttspm.18</book-part-id>
                  <title-group>
                     <title>Glossary</title>
                  </title-group>
                  <fpage>183</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2ttspm.19</book-part-id>
                  <title-group>
                     <title>References</title>
                  </title-group>
                  <fpage>195</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2ttspm.20</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>205</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
