<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt81nwk</book-id>
      <subj-group subj-group-type="discipline">
         <subject>History</subject>
      </subj-group>
      <book-title-group>
         <book-title>Managing British Colonial and Post-Colonial Development</book-title>
         <subtitle>The Crown Agents, 1914-1974</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Sunderland</surname>
               <given-names>David</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>21</day>
         <month>06</month>
         <year>2007</year>
      </pub-date>
      <isbn content-type="ppub">9781843833017</isbn>
      <isbn content-type="epub">9781846155673</isbn>
      <publisher>
         <publisher-name>Boydell &amp; Brewer</publisher-name>
         <publisher-loc>Woodbridge, Suffolk, UK; Rochester, NY, USA</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2007</copyright-year>
         <copyright-holder>David Sunderland</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7722/j.ctt81nwk"/>
      <abstract abstract-type="short">
         <p>Britain's Crown Agents' Office is a unique development agency. Until the early 1960s, its clients were colonial governments, and, thereafter, the administrations of dependencies and newly independent countries. As well as purchasing a large proportion of its customers' imports, it provided them with finance and managed their investments. It was thus one of the largest buyers of goods in the UK, and, after, the Bank of England, the country's biggest financial institution. This book, the sequel to the author's 'Managing the British Empire: The Crown Agents, 1833 -1914' (Boydell, 2004), examines the Agents' various development roles, including the disastrous venture into secondary banking in 1967 which collapsed in 1974, then the largest bankruptcy in British financial history. The book contributes to a number of current debates in development studies, adds to our understanding of the London financial market and the competitiveness of British industry, and shows how present day aid agencies can learn much from the arrangements of the past.</p>
      </abstract>
      <counts>
         <page-count count="312"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt81nwk.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt81nwk.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt81nwk.3</book-part-id>
                  <title-group>
                     <title>List of Figures and Tables</title>
                  </title-group>
                  <fpage>vi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt81nwk.4</book-part-id>
                  <title-group>
                     <title>Acknowledgements</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt81nwk.5</book-part-id>
                  <title-group>
                     <title>Abbreviations</title>
                  </title-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt81nwk.6</book-part-id>
                  <title-group>
                     <title>Introduction</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract abstract-type="extract">
                     <p>The literature on the British Empire during the twentieth century is now immense. It includes general studies of the operation of the Empire, regional surveys that examine the effects of policy decisions on the periphery, books on the provision of aid and the development of colonies, and countless monographs on decolonisation.¹ In addition, the impact of colonial development on Britain has been discussed at length in books and articles on the growth of the British economy during the period, and there have been a number of works on the financial relationship between colonial administrations and London, largely concentrating on the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt81nwk.7</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>The Public Issue of Loans</title>
                  </title-group>
                  <fpage>9</fpage>
                  <abstract abstract-type="extract">
                     <p>The finance required for development broadly came from four sources. The UK government and later international organizations provided aid in the form of loans and grants. The private sector invested large sums in businesses based in the colonies. Colonial governments financed projects using their own resources, and funds were obtained through the Crown Agents. Money via the Agents came from the public issue of loans in the London market, the sale of stock in the same market, inter-colonial loans and advances from the Joint Colonial Fund (JCF) and the Joint Miscellaneous Fund (JMF), investment Funds managed by the CAs. This</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt81nwk.8</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>Other Sources of Finance</title>
                  </title-group>
                  <fpage>42</fpage>
                  <abstract abstract-type="extract">
                     <p>The funds needed to finance development came from the public issue of loans, discussed in the previous chapter, but also from four further sources – private funds, other colonies, internal resources, and aid. The various ways in which the Agents extracted money from private investors and the City is the subject of the first section of this chapter, which also includes a brief discussion of corporate colonial investment. The means by which colonies lent money to other territories are investigated in part two, and the use of internal resources and the provision of aid are reviewed in sections three and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt81nwk.9</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>The Management of Colonial Investment Funds</title>
                  </title-group>
                  <fpage>69</fpage>
                  <abstract abstract-type="extract">
                     <p>During the period under review, the number and size of colonial Investment Funds managed by the Agents grew dramatically. In 1930, they supervised 320 worth £115m, in 1962 over 1,000, which contained cash and securities valued at £908m, and, in 1974, 700, belonging to 100 clients, that held £856m (Figure 3.1 and Appendix 4).¹ The largest Funds were those owned by colonial Currency Commissioners, which, in 1939, contained slightly over a quarter, and, in 1958, around one third of the investments held by all capital reserves.² Each Currency Fund contained sterling securities equal to 110 per cent of the value</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt81nwk.10</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>The Management of the Joint Colonial Fund and the Joint Miscellaneous Fund</title>
                  </title-group>
                  <fpage>97</fpage>
                  <abstract abstract-type="extract">
                     <p>Throughout the period, the Agents’ clients held funds in the UK to pay for goods purchased by the Agency and to meet salary, pension, loan interest and other commitments. The money, known as floating balances, was remitted to Britain, and also comprised the unspent proceeds of loans raised in London and the colonies, money from realized Sinking Funds that had yet to be used to repay associated loans, monies about to be invested on behalf of other Funds, unused British government aid, and, from around 1937, a portion of colonial Currency Funds.¹ Before 1924, each colony’s floating balances were invested</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt81nwk.11</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>The Cost of Supplies</title>
                  </title-group>
                  <fpage>115</fpage>
                  <abstract abstract-type="extract">
                     <p>From 1920 to 1974, the Agents purchased £2,064m of supplies (Figure 5.1). At the start of the period, colonial clients were required to buy of all of their stores that were not manufactured or produced within their own colonies or in adjacent countries from the Agency. Over time, however, this requirement lapsed, and, by 1959, they were free to buy supplies themselves either direct from overseas manufacturers or from local merchants. The articles purchased by the Agents largely went to Africa, the Far East and the West Indies and Caribbean in 1936 and to Africa and the Far East in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt81nwk.12</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Procurement from the Early 1960s and Delivery Delays</title>
                  </title-group>
                  <fpage>143</fpage>
                  <abstract abstract-type="extract">
                     <p>In 1959, the Agents’ purchasing departments made a loss of £134,455, which by 1962 had increased to £300,311.¹ The deficits arose from a fall in indents and a rise in costs. The more important of these two factors was the decline in the demand for the CAs’ services, which was related to colonial independence, environmental change, and greater competition from the private sector.² On independence, some client governments left the CAs and many used them less because they wished to demonstrate to their own supporters and to other independent countries their freedom from the old Imperial power, found it expedient</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt81nwk.13</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Miscellaneous Roles</title>
                  </title-group>
                  <fpage>160</fpage>
                  <abstract abstract-type="extract">
                     <p>The chapter examines a host of miscellaneous roles performed by the Agency, including the completion of a variety of personnel duties, the provision of various engineering services, and the discharge of a number of less important tasks, such as the sale of stamps, the granting of concessions, the purchase and rental of London properties, and the employment of solicitors.</p>
                     <p>The Agents’ personnel duties included the recruitment of expatriate officers, the arrangement of their journeys to and from the country of employment, the payment of their pensions and some salaries, and, from the mid-1960s, the provision of training courses. As well</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt81nwk.14</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>The Move into Secondary Banking</title>
                  </title-group>
                  <fpage>179</fpage>
                  <abstract abstract-type="extract">
                     <p>In March 1967, the Agency embarked on a new venture as a secondary bank, taking deposits from principals and financial institutions, which it re-lent or invested in equities, gilts, property and the private sector. Unlike its traditional financial service, the new pursuits were conducted on its own account and thus became known as ‘own account activities’. The Office acted, not as an agent, which had been the case over the previous 134 years, but as a principal, reaping all the profits from the new business, but also being liable for any losses. Begun by the Senior Crown Agent, Sir Stephen</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt81nwk.15</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>The Collapse of the Secondary Banking Venture</title>
                  </title-group>
                  <fpage>221</fpage>
                  <abstract abstract-type="extract">
                     <p>Having decided that the Agents’ own account activities had been carried out in an unacceptable manner, both enquiries placed part of the blame on the failure of internal and external control mechanisms. It was claimed that within the Agency neither the CA Board nor the Senior Crown Agent effectively monitored the activities of the Finance Directorate, and that outside the Office, the Ministry, the Treasury, the Bank of England and the Exchequer &amp; Audit Office all adopted a policy of laissez faire. The Agency’s supposedly high-risk pursuits were therefore neither restricted nor brought to a halt. As a result, when secondary</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt81nwk.16</book-part-id>
                  <title-group>
                     <title>Conclusion</title>
                  </title-group>
                  <fpage>241</fpage>
                  <abstract abstract-type="extract">
                     <p>The activities of the Crown Agents over the period 1920 to 1974 can perhaps be best understood in the context of principal-agent theory, which suggests that agents possess a number of interests. Their official goal is the completion of the task for which they are retained; in the case of a builder employed to construct a garage, for example, the completion of the building. Agents with more than one principal, however, may have a number of such interests that contradict each other; the builder may be employed by two people, one of whom wants a cheaply built garage and one</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt81nwk.17</book-part-id>
                  <title-group>
                     <title>Appendix 1.</title>
                     <subtitle>The Crown Agents, 1920–74</subtitle>
                  </title-group>
                  <fpage>254</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt81nwk.18</book-part-id>
                  <title-group>
                     <title>Appendix 2.</title>
                     <subtitle>Market finance, 1914–68 (£m)</subtitle>
                  </title-group>
                  <fpage>256</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt81nwk.19</book-part-id>
                  <title-group>
                     <title>Appendix 3.</title>
                     <subtitle>Non-market finance, 1920–65 (£m)</subtitle>
                  </title-group>
                  <fpage>260</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt81nwk.20</book-part-id>
                  <title-group>
                     <title>Appendix 4.</title>
                     <subtitle>1954 Investment Funds (£m), Wartime gifts and loans (£m), 1936 procurement (£m) and 1954 colonial appointments</subtitle>
                  </title-group>
                  <fpage>265</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt81nwk.21</book-part-id>
                  <title-group>
                     <title>Appendix 5.</title>
                     <subtitle>Crown Agents’ own account activities</subtitle>
                  </title-group>
                  <fpage>269</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt81nwk.22</book-part-id>
                  <title-group>
                     <title>Appendix 6.</title>
                     <subtitle>FMI investment in associated companies, 1969–74</subtitle>
                  </title-group>
                  <fpage>270</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt81nwk.23</book-part-id>
                  <title-group>
                     <title>Bibliography</title>
                  </title-group>
                  <fpage>279</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt81nwk.24</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>287</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt81nwk.25</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>297</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
