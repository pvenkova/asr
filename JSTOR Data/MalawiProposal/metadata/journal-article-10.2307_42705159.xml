<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">socianalysis</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50003651</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Social Analysis: The International Journal of Social and Cultural Practice</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Berghahn Books</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">0155977X</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15585727</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">42705159</article-id>
         <title-group>
            <article-title>Kuru, AIDS, and Witchcraft: Reconfiguring Culpability in Melanesia and Africa</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Isak</given-names>
                  <surname>Niehaus</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>12</month>
            <year>2013</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">57</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40101493</issue-id>
         <fpage>25</fpage>
         <lpage>41</lpage>
         <permissions>
            <copyright-statement>© 2013 Berghahn Journals</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:href="http://dx.doi.org/10.3167/sa.2013.570302"
                   xlink:title="an external site"/>
         <abstract>
            <p>This article examines the significance of witchcraft accusations during the South African AIDS epidemic. In search of broader intercontextual understanding, I compare experiences of AIDS in Bushbuckridge, where I have done fieldwork, with anthropological studies of kuru, a transmissible degenerative disease, in Papua New Guinea. Whereas scientists blamed the spread of kuru on the practice of cannibalism, those who were affected attributed it to sorcery. These dynamics resonate with the encounters between health workers and host populations during the AIDS epidemic in Bushbuckridge. Health propaganda attributed the rapid transmission of HIV to sexual promiscuity. In response, sufferers and their kin invoked witchcraft, shifting blame onto outsiders and reinforcing the relations that medical labeling threatened to disrupt. The comparison enables us to see witchcraft accusations as a means of reconfiguring culpability, cutting certain networks, and strengthening other existing configurations.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Notes</title>
         <ref id="d549e118a1310">
            <label>2</label>
            <mixed-citation id="d549e125" publication-type="other">
Lambek and Strathern (1998: 25-28)</mixed-citation>
         </ref>
         <ref id="d549e132a1310">
            <label>3</label>
            <mixed-citation id="d549e141" publication-type="other">
Arens (1979)</mixed-citation>
            <mixed-citation id="d549e147" publication-type="other">
Steadman and Merbs (1982),</mixed-citation>
         </ref>
         <ref id="d549e154a1310">
            <label>4</label>
            <mixed-citation id="d549e161" publication-type="other">
Anderson 2008: 220-225</mixed-citation>
         </ref>
         <ref id="d549e168a1310">
            <label>5</label>
            <mixed-citation id="d549e175" publication-type="other">
Fassin 2007: 41-49</mixed-citation>
         </ref>
         <ref id="d549e183a1310">
            <label>6</label>
            <mixed-citation id="d549e190" publication-type="other">
McPherson et al. (2008)</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>References</title>
         <ref id="d549e206a1310">
            <mixed-citation id="d549e210" publication-type="other">
Anderson, Warwick. 2000. "The Possession of Kuru: Medical Science and Biocolonial
Exchange." Comparative Studies in Society and History 42, no. 4: 713-744.</mixed-citation>
         </ref>
         <ref id="d549e220a1310">
            <mixed-citation id="d549e224" publication-type="other">
Anderson, Warwick. 2008. The Collectors of Lost Souls: Turning Kuru Scientists into
Whitemen. Baltimore: Johns Hopkins University Press.</mixed-citation>
         </ref>
         <ref id="d549e234a1310">
            <mixed-citation id="d549e238" publication-type="other">
Andersson, Jens. 2002. "Sorcery in the Era of 'Henry IV': Kinship, Mobility and Moral-
ity in Buhera District, Zimbabwe." Journal of the Royal Anthropological Institute 8,
no. 3: 425-499.</mixed-citation>
         </ref>
         <ref id="d549e251a1310">
            <mixed-citation id="d549e255" publication-type="other">
Arens, William. 1979. The Man-Eating Myth: Anthropology and Anthropophagy. New
York: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d549e266a1310">
            <mixed-citation id="d549e270" publication-type="other">
Ashforth, Adam. 2002. "An Epidemic of Witchcraft? The Implications of AIDS for the
Post-apartheid State." African Studies 61, no. 1: 121-145.</mixed-citation>
         </ref>
         <ref id="d549e280a1310">
            <mixed-citation id="d549e284" publication-type="other">
Beasley, Annette N. 2006. "The Promised Medicine: Fore Reflections on the Scientific
Investigation of Kuru." Oceania 76, no. 2: 186-202.</mixed-citation>
         </ref>
         <ref id="d549e294a1310">
            <mixed-citation id="d549e298" publication-type="other">
Beasley, Annette N. 2009. "Frontier Journeys: Fore Experiences on the Kuru Patrols."
Oceania 79, no. 1: 34-52.</mixed-citation>
         </ref>
         <ref id="d549e308a1310">
            <mixed-citation id="d549e312" publication-type="other">
Bennett, J. H., F. A. Rhodes, and H. N. Robson. 1958. "Observations on Kuru: A Pos-
sible Genetic Basis." Australian Annuals of Medicine 7: 269-278.</mixed-citation>
         </ref>
         <ref id="d549e322a1310">
            <mixed-citation id="d549e326" publication-type="other">
Berndt, Ronald. 1954. "Reaction to Contact in the Eastern Highlands of New Guinea."
Oceania 24, no. 3: 190-228.</mixed-citation>
         </ref>
         <ref id="d549e336a1310">
            <mixed-citation id="d549e340" publication-type="other">
Cannon, Walter B. 1942. "Voodoo Death." American Anthropologist 44, no. 1: 169-181.</mixed-citation>
         </ref>
         <ref id="d549e348a1310">
            <mixed-citation id="d549e352" publication-type="other">
Delius, Peter, and Clive Glaser. 2005. "Sex, Disease and Stigma in South Africa: Histori-
cal Perspectives." African Journal of AIDS Research 4, no. 1: 29-36.</mixed-citation>
         </ref>
         <ref id="d549e362a1310">
            <mixed-citation id="d549e366" publication-type="other">
Durham, Deborah, and Frederick Klaits. 2002. "Funerals and the Public Space of Senti-
ment in Botswana." Journal of Southern African Studies 28, no. 4: 777-795.</mixed-citation>
         </ref>
         <ref id="d549e376a1310">
            <mixed-citation id="d549e380" publication-type="other">
Epstein, Helen. 2007. The Invisible Cure: Africa, the West, and the Fight against AIDS.
London: Penguin.</mixed-citation>
         </ref>
         <ref id="d549e390a1310">
            <mixed-citation id="d549e394" publication-type="other">
Farmer, Paul. 1992. AIDS and Accusation: Haiti and the Geography of Blame. Berkeley:
University of California Press.</mixed-citation>
         </ref>
         <ref id="d549e404a1310">
            <mixed-citation id="d549e408" publication-type="other">
Fassin, Didier. 2007. When Bodies Remember: Experiences and Politics of AIDS in South
Africa. Tïans. Amy Jacobs and Gabrielle Varro. Berkeley: University of California Press.</mixed-citation>
         </ref>
         <ref id="d549e418a1310">
            <mixed-citation id="d549e422" publication-type="other">
Ferguson, James. 2006. Global Shadows: Africa in the Neoliberal Order. Durham, NC:
Duke University Press.</mixed-citation>
         </ref>
         <ref id="d549e433a1310">
            <mixed-citation id="d549e437" publication-type="other">
Herring, D. Ann, and Alan C. Swedlund, eds. 2010. Plagues and Epidemics: Infected
Spaces Past and Present. Oxford: Berg Publishers.</mixed-citation>
         </ref>
         <ref id="d549e447a1310">
            <mixed-citation id="d549e451" publication-type="other">
Hunter, Mark. 2005. "Cultural Politics and Masculinities: Multiple-Partners in Histori-
cal Perspective in KwaZulu-Natal." Pp. 139-160 in Men Behaving Differently: South
African Men Since 1994, ed. Graeme Reid and Liz Walker. Cape Town: Double
Storey Books.</mixed-citation>
         </ref>
         <ref id="d549e467a1310">
            <mixed-citation id="d549e471" publication-type="other">
Iliffe, John. 2006. The African AIDS Epidemic: A History. Athens: Ohio University Press.</mixed-citation>
         </ref>
         <ref id="d549e478a1310">
            <mixed-citation id="d549e482" publication-type="other">
Jonsen, Albert, and Jeff Stryker, eds. 1993. The Social Impact of AIDS in the United
States. Washington, DC: National Academy Press.</mixed-citation>
         </ref>
         <ref id="d549e492a1310">
            <mixed-citation id="d549e496" publication-type="other">
Kluckhohn, Clyde. 1944. Navaho Witchcraft. Boston: Beacon Press.</mixed-citation>
         </ref>
         <ref id="d549e503a1310">
            <mixed-citation id="d549e507" publication-type="other">
Kuhn, Thomas. 1962. The Structure of Scientific Revolutions. Chicago: University of
Chicago Press.</mixed-citation>
         </ref>
         <ref id="d549e518a1310">
            <mixed-citation id="d549e522" publication-type="other">
Lambek, Michael. 2002. "Fantasy in Practice: Projection and Introjection, or the Witch
and the Spirit-Medium." Pp. 198-214 in Beyond Rationalism: Rethinking Magic,
Witchcraft and Sorcery, ed. Bruce Kapferer. New York: Berghahn Books.</mixed-citation>
         </ref>
         <ref id="d549e535a1310">
            <mixed-citation id="d549e539" publication-type="other">
Lambek, Michael, and Andrew Strathern. 1998. "Introduction: Embodying Sociality:
Africanist-Melanesianist Comparisons." Pp. 1-28 in Bodies and Persons: Compara-
tive Perspectives from Africa and Melanesia, ed. Michael Lambek and Andrew
Strathern. Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d549e555a1310">
            <mixed-citation id="d549e559" publication-type="other">
Lindenbaum, Shirley. 1979. Kuru Sorcery: Disease and Danger in the New Guinea High-
lands. Palo Alto, CA: Mayfíeld.</mixed-citation>
         </ref>
         <ref id="d549e569a1310">
            <mixed-citation id="d549e573" publication-type="other">
Lindenbaum, Shirley. 2001. "Kuru, Prions, and Human Affairs: Thinking about Epi-
demics." Annual Review of Anthropology 30: 363-385.</mixed-citation>
         </ref>
         <ref id="d549e583a1310">
            <mixed-citation id="d549e587" publication-type="other">
Lindenbaum, Shirley. 2010. "Explaining Kuru: Three Ways of Thinking about an Epi-
demic." Pp. 323-344 in Herring and Swedlund 2010.</mixed-citation>
         </ref>
         <ref id="d549e597a1310">
            <mixed-citation id="d549e601" publication-type="other">
MacPherson, Peter, Mosa Moshabela, Neil Martinson, and Paul Pronyk. 2009. "Mortal-
ity and Loss to Follow-Up among HAART Initiators in Rural South Africa." Transac-
tions of the Royal Society of Tropical Medicine and Hygiene 103, no. 6: 588-593.</mixed-citation>
         </ref>
         <ref id="d549e615a1310">
            <mixed-citation id="d549e619" publication-type="other">
McNeill, Fraser G. 2009. "'Condoms Cause AIDS': Poison, Prevention and Denial in
Venda, South Africa." Afrìcan Affairs 108, no. 432: 353-370.</mixed-citation>
         </ref>
         <ref id="d549e629a1310">
            <mixed-citation id="d549e633" publication-type="other">
McNeill, Fraser G., and Isak Niehaus. 2010. Magic? AIDS Review 2009. Pretoria: Centre
for the Study of AIDS, University of Pretoria.</mixed-citation>
         </ref>
         <ref id="d549e643a1310">
            <mixed-citation id="d549e647" publication-type="other">
Mfecane, Sakhumsi. 2010. "Exploring Masculinities in the Context of ARV Use: A Study
of Men Living with HIV in a South African Village." PhD diss., University of the
Witwatersrand.</mixed-citation>
         </ref>
         <ref id="d549e660a1310">
            <mixed-citation id="d549e664" publication-type="other">
Myhre, Knut Christian. 2009. "Disease and Disruption: Chagga Witchcraft and Rela-
tional Fragility." Pp. 118-140 in Dealing with Uncertainty in Contemporary African
Lives, ed. Liv Haram and C. Bawa Yamba. Stockholm: Nordiska Afrikainstitutet.</mixed-citation>
         </ref>
         <ref id="d549e677a1310">
            <mixed-citation id="d549e681" publication-type="other">
Niehaus, Isak. 2007. "Death before Dying: Understanding AIDS Stigma in the South
African Lowveld." Journal of Southern African Studies 33, no. 4: 845-860.</mixed-citation>
         </ref>
         <ref id="d549e691a1310">
            <mixed-citation id="d549e695" publication-type="other">
Niehaus, Isak. 2013. Witchcraft and a Life in the New South Africa. New York: Cam-
bridge University Press.</mixed-citation>
         </ref>
         <ref id="d549e706a1310">
            <mixed-citation id="d549e710" publication-type="other">
Niehaus, Isak, with Gunvor Jonsson. 2005. "Dr. Wouter Basson, Americans, and Wild
Beasts: Men's Conspiracy Theories of HIV/AIDS in the South African Lowveld."
Medical Anthropology 24, no. 2: 179-208.</mixed-citation>
         </ref>
         <ref id="d549e723a1310">
            <mixed-citation id="d549e727" publication-type="other">
Obeyesekere, Gananath. 2005. Cannibal Talk: The Man-Eating Myth and Human Sacri-
fice in the South Seas. Berkeley: University of California Press.</mixed-citation>
         </ref>
         <ref id="d549e737a1310">
            <mixed-citation id="d549e741" publication-type="other">
Posel, Deborah. 2005. "Sex, Death and the Fate of the Nation: Reflections on the Politi-
cization of Sexuality in Post-Apartheid South Africa." Africa 75, no. 2: 125-153.</mixed-citation>
         </ref>
         <ref id="d549e751a1310">
            <mixed-citation id="d549e755" publication-type="other">
Probst, Peter. 1999. "Mchape '95, or, the Sudden Fame of Billy Goodson Chisupe: Heal-
ing, Social Memory and the Enigma of the Public Sphere in Post-Banda Malawi."
Afrìca 69, no. 1: 108-138.</mixed-citation>
         </ref>
         <ref id="d549e768a1310">
            <mixed-citation id="d549e772" publication-type="other">
Pronyk, Paul. 2001. "Assessing Health Seeking Behaviour among Tuberculosis Patients
in Rural South Africa." International Journal of Tuberculosis and Lung Disease 5,
no. 7: 619-627.</mixed-citation>
         </ref>
         <ref id="d549e785a1310">
            <mixed-citation id="d549e789" publication-type="other">
Rosenberg, Charles E. 1992. Explaining Epidemics and Other Studies in the History of
Medicine. Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d549e800a1310">
            <mixed-citation id="d549e804" publication-type="other">
Schoepf, Brooke. 2001. "International AIDS Research in Anthropology: Taking a Critical
Perspective on the Crisis." Annual Review of Anthropology 30: 335-361.</mixed-citation>
         </ref>
         <ref id="d549e814a1310">
            <mixed-citation id="d549e818" publication-type="other">
Stadler, Jonathan. 2003. "The Young, the Rich, and the Beautiful: Secrecy, Suspicion
and Discourses of AIDS in the South African Lowveld." African Journal of AIDS
Research 2, no. 2: 127-139.</mixed-citation>
         </ref>
         <ref id="d549e831a1310">
            <mixed-citation id="d549e835" publication-type="other">
Steadman, Lyle, and Charles Merbs. 1982. "Kuru and Cannibalism?" American Anthro-
pologist 84, no. 3: 611-627.</mixed-citation>
         </ref>
         <ref id="d549e845a1310">
            <mixed-citation id="d549e849" publication-type="other">
Strathern, Marilyn. 1996. "Cutting the Network." Journal of the Royal Anthropological
Institute 2, no. 3: 517-535.</mixed-citation>
         </ref>
         <ref id="d549e859a1310">
            <mixed-citation id="d549e863" publication-type="other">
Thornton, Robert J. 2009. "Sexual Networks and Social Capital: Multiple and Concur-
rent Sexual Partnerships as a Rational Response to Unstable Social Networks." Afrì-
can Journal of AIDS Research 8, no. 4: 413-421.</mixed-citation>
         </ref>
         <ref id="d549e876a1310">
            <mixed-citation id="d549e880" publication-type="other">
Wahlström, Âsa. 2002. "The Old Digging Graves for the Young: The Cultural Con-
struction of AIDS among Youth in the South African Lowveld." BSc diss., Brunei
University.</mixed-citation>
         </ref>
         <ref id="d549e894a1310">
            <mixed-citation id="d549e898" publication-type="other">
Weiss, Brad. 1993. "'Buying Her Grave': Money, Movement and AIDS in North-West
Tanzania." Africa 63, no. 1: 19-35.</mixed-citation>
         </ref>
         <ref id="d549e908a1310">
            <mixed-citation id="d549e912" publication-type="other">
Yamba, C. Bawa. 1997. "Cosmologies in Hirmoil: Witchfinding and AIDS in Chiawa,
Zambia." Africa 67, no. 2: 200-223.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
