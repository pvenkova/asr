<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">jmedethics</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000494</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Journal of Medical Ethics</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Institute of Medical Ethics and BMJ Publishing Group</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">03066800</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">14734257</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">20696772</article-id>
         <article-categories>
            <subj-group>
               <subject>Research ethics</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Ethics committees for biomedical research in some African emerging countries: which establishment for which independence? A comparison with the USA and Canada</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jean-Paul</given-names>
                  <surname>Rwabihama</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Catherine</given-names>
                  <surname>Girre</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Anne-Marie</given-names>
                  <surname>Duguet</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>4</month>
            <year>2010</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">36</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i20696758</issue-id>
         <fpage>243</fpage>
         <lpage>249</lpage>
         <permissions>
            <copyright-statement>Copyright © 2010 BMJ Publishing Group Ltd and the Institute of Medical Ethics</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/20696772"/>
         <abstract>
            <p>Context The conduct of medical research led by Northern countries in developing countries raises ethical questions. The assessment of research protocols has to be twofold, with a first reading in the country of origin and a second one in the country where the research takes place. This reading should benefit from an independent local ethical review of protocols. Consequently, ethics committees for medical research are evolving in Africa. Objective To investigate the process of establishing ethics committees and their independence. Method Descriptive study of 25 African countries and two North American countries. Data were recorded by questionnaire and interviews. Two visits of ethics committee meetings were conducted on the ground: over a period of 3 months in Kigali (Rwanda) and 2 months in Washington DC (USA). Results 22 countries participated in this study, 20 from Africa and two from North America. The response rate was 80%. 75% of local African committees developed into national ethics committees. During the last 5 years, these national committees have grown on a structural level. The circumstances of creation and the general context of underdevelopment remain the major challenges in Africa. Their independence could not be ensured without continuous training and efficient funding mechanisms. Institutional ethics committees are well established in USA and in Canada, whereas ethics committees in North America are weakened by the institutional affiliation of their members. Conclusion The process of establishing ethics committees could affect their functioning and compromise their independence in some African countries and in North America.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>REFERENCES</title>
         <ref id="d872e176a1310">
            <label>1</label>
            <mixed-citation id="d872e183" publication-type="other">
World Medical
                Association. Declaration of Helsinki: ethical principles for medical
research
                involving human subjects. Edinburgh: WMA, 2000.</mixed-citation>
         </ref>
         <ref id="d872e193a1310">
            <label>2</label>
            <mixed-citation id="d872e200" publication-type="other">
Council for
                International Organisations of Medical Sciences. International
guidelines for
                ethical review of epidemiologica! studies. Geneva: CIOMS, 1991.</mixed-citation>
         </ref>
         <ref id="d872e210a1310">
            <label>3</label>
            <mixed-citation id="d872e217" publication-type="other">
National
                Consensus on Bioethics and Health research in Uganda. Guidelines
for the conduct
                of health research involving human subjects in Uganda. Kampala:
The Ministry of
                Health, 1997.</mixed-citation>
         </ref>
         <ref id="d872e230a1310">
            <label>4</label>
            <mixed-citation id="d872e237" publication-type="other">
Thailand Ministry
                of Public Health Ethics Committee. Rule of the medical
council on the
                observance of medical ethics. Bangkok: The Ministry of Health, 1995.</mixed-citation>
         </ref>
         <ref id="d872e248a1310">
            <label>5</label>
            <mixed-citation id="d872e255" publication-type="other">
Attendus du
                jugement prononcé par le tribunal de Nuremberg dans le procès
États-Unis contre
                Karl Brandt, et al., le 19 août 1947. [Judgement pronounced by the
international
                trial of Nuremberg]</mixed-citation>
         </ref>
         <ref id="d872e268a1310">
            <label>6</label>
            <mixed-citation id="d872e275" publication-type="other">
Harkness JM.
                Nuremberg and the issue of wartime experiments on U.S. prisoners.
JAMA
                1996;276:1672-4.</mixed-citation>
         </ref>
         <ref id="d872e285a1310">
            <label>7</label>
            <mixed-citation id="d872e292" publication-type="other">
Anon. World
                Medical Association draft code of ethics on human experimentation.
BMJ
                1962;2:1119.</mixed-citation>
         </ref>
         <ref id="d872e302a1310">
            <label>8</label>
            <mixed-citation id="d872e309" publication-type="other">
Effa P. Ethics
                committees in Western and Central Africa: concrete foundations.
Dev World Bioeth
                2007;3:136-42.</mixed-citation>
         </ref>
         <ref id="d872e319a1310">
            <label>9</label>
            <mixed-citation id="d872e326" publication-type="other">
Rwabihama JP,
                Girre C, Duguet AM. Mise en place de comités d'éthique pour la
recherche
                biomédicale dans les pays émergents: cas du Rwanda. [Establishment
of ethics
                committees for biomedicai research in emerging countries; the case of
Rwanda.] Droit
                Déontologie et Soins 2009;2:209-15.</mixed-citation>
         </ref>
         <ref id="d872e342a1310">
            <label>10</label>
            <mixed-citation id="d872e349" publication-type="other">
Bikandou B. Actes
                du premier séminaire international sur l'éthique de la recherche.
Brazzaville:
                OMS/AFRO, 2004.</mixed-citation>
         </ref>
         <ref id="d872e360a1310">
            <label>11</label>
            <mixed-citation id="d872e367" publication-type="other">
Nyika A, Kilama
                W, Chilengi R, et al. Composition, training needs and independence
of ethics review
                committees across Africa: are the gate-keepers rising to the
emerging
                challenges? J Med Ethics 2009;35:189-93.</mixed-citation>
         </ref>
         <ref id="d872e380a1310">
            <label>12</label>
            <mixed-citation id="d872e387" publication-type="other">
Nancy EK, Adman
                AH, Ademóla A, et al. The structure and the function of research
ethic committee
                in Africa: a case study. PloS Med 2007;4:3.</mixed-citation>
         </ref>
         <ref id="d872e397a1310">
            <label>13</label>
            <mixed-citation id="d872e404" publication-type="other">
Kirigia JM,
                Wambebe C, Baba-Moussa A. Status of national research bioethics
committees in the
                WHO African region. BMC Med Ethics 2005;6:10.</mixed-citation>
         </ref>
         <ref id="d872e414a1310">
            <label>14</label>
            <mixed-citation id="d872e421" publication-type="other">
Ikingura JK,
                Kruger M, Zeleke W. Health research ethics review and needs
of institutional
                ethics committees in Tanzania. Jaman Health Res Bull 2007;
3:154-8.</mixed-citation>
         </ref>
         <ref id="d872e434a1310">
            <label>15</label>
            <mixed-citation id="d872e441" publication-type="other">
Benatar SR.
                Reflections and recommendations on research ethics in developing
countries. Soc
                Sci Med 2002;54:1131-41.</mixed-citation>
         </ref>
         <ref id="d872e451a1310">
            <label>16</label>
            <mixed-citation id="d872e458" publication-type="other">
Whittle H.
                Ethical standards in biomedicai research and Good clinical Practice,
Kololi-Gambia, le
                12/02/2008.</mixed-citation>
         </ref>
         <ref id="d872e469a1310">
            <label>17</label>
            <mixed-citation id="d872e476" publication-type="other">
Lemaire F. Sixty
                years after Nuremberg medical trial. Méd/Sci 2007;23:1063-7.</mixed-citation>
         </ref>
         <ref id="d872e483a1310">
            <label>18</label>
            <mixed-citation id="d872e490" publication-type="other">
U.S. Department
                of Health and Human Subject. Code of Federal Regulations:
45 Public
                welfare; Part 46. 2009. Office for Human Research Protection.</mixed-citation>
         </ref>
         <ref id="d872e500a1310">
            <label>19</label>
            <mixed-citation id="d872e507" publication-type="other">
Campbell EG,
                Weissman JS, Vogeli C, ef al. Financial relationships between IRB and
industry. N Engl
                J Med 2006;22:2321-9.</mixed-citation>
         </ref>
         <ref id="d872e517a1310">
            <label>20</label>
            <mixed-citation id="d872e524" publication-type="other">
Emmanuel E.
                Testimony delivered Sept 12, 2002 at the sixth meeting of the
President's Council
                on Bioethics. http://www.bioethics.gov/transcripts/sep02/
session2.html
                (accessed 5 Aug 2009).</mixed-citation>
         </ref>
         <ref id="d872e537a1310">
            <label>21</label>
            <mixed-citation id="d872e544" publication-type="other">
Anderson EE.
                Qualitative study of non-affiliated non-scientific IRB members.
Account Res
                2006;2:135-55.</mixed-citation>
         </ref>
         <ref id="d872e554a1310">
            <label>22</label>
            <mixed-citation id="d872e561" publication-type="other">
Wolf LE, Zandecki
                J. Conflicts of interest in research: how IRB address their own
conflicts. IRB
                2007;1:6-12.</mixed-citation>
         </ref>
         <ref id="d872e572a1310">
            <label>23</label>
            <mixed-citation id="d872e579" publication-type="other">
http://www.wirb.com (accessed 5 Aug 2009).</mixed-citation>
         </ref>
         <ref id="d872e586a1310">
            <label>24</label>
            <mixed-citation id="d872e593" publication-type="other">
Norton K, Wilson
                DM. Continuing ethics review practices by Canadian Research
Ethics Boards. IRB
                2008;3:10-14.</mixed-citation>
         </ref>
         <ref id="d872e603a1310">
            <label>25</label>
            <mixed-citation id="d872e610" publication-type="other">
Mcintosh S, Sierra
                E, Dozier A, et al. Ethical review issues in collaborative
research between
                U.S. and low-middle income country patterns: a case example.
Bioethics
                2008;8:414-22.</mixed-citation>
         </ref>
         <ref id="d872e623a1310">
            <label>26</label>
            <mixed-citation id="d872e630" publication-type="other">
Chima SC.
                Regulation of biomedicai research in Africa. BMJ 2006;332:
848-51.</mixed-citation>
         </ref>
         <ref id="d872e640a1310">
            <label>27</label>
            <mixed-citation id="d872e647" publication-type="other">
Macklin R. "How
                independent are IRBs?" IRB 2008;3:15-19.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
