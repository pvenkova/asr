<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">pakideverevi</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50002869</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>The Pakistan Development Review</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Pakistan Institute of Development Economics</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00309729</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">41261324</article-id>
         <article-categories>
            <subj-group>
               <subject>The Mahbub Ul Haq Memorial Lecture</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Demographic Transition and Unwanted Fertility: A Fresh Assessment [with Comments]</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>John B.</given-names>
                  <surname>Casterline</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Mohammad</given-names>
                  <surname>Nizamuddin</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Zeba A.</given-names>
                  <surname>Sathar</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Iqbal</given-names>
                  <surname>Alam</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>12</month>
            <year>2009</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">48</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40056830</issue-id>
         <fpage>387</fpage>
         <lpage>421</lpage>
         <permissions>
            <copyright-statement>© The Pakistan Development Review, 2009</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/41261324"/>
         <abstract>
            <p>The distinction between wanted and unwanted fertility has been crucial in many of the more intense debates in recent decades over the nature of contemporary fertility declines and, in particular, the potential impact of expanded provision of family planning services. In a muchdebated article published in 1994, Pritchett argues that decline in desired fertility is overwhelmingly the principal source of fertility decline, with the implication that family planning programmes are of little consequence. I revisit this debate drawing on a far larger body of survey data and, more importantly, an alternative fertility specification which relies on a non-conventional definition of wanted and unwanted fertility rates and which distinguishes rates and composition. Decompositions of fertility decline in the period from the mid-1970s to the present are carried out for 44 countries. The decomposition results indicate that declines in unwanted fertility rates have been at least as important, if not more important, than declines in wanted fertility rates. Surprisingly, shifts in the proportion of women wanting to stop childbearing—i.e., changes in preference composition—has contributed very little to fertility change in this period. Further, decline in wanted fertility and increases in non-marital exposure (due largely to delayed entry into first marriage) have also made substantial contributions, although on average they fall short of the contribution of declines in unwanted fertility rates. That declines in unwanted fertility have been an essential feature of contemporary fertility decline is the main conclusion from this research. This in turn opens the door to new perspectives on fertility pre-, mid-, and post-transition which recognises the inter-dependencies between fertility demand and unwanted fertility rates in the determination of the overall level of fertility.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d3729e324a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d3729e331" publication-type="other">
Pakistan—1975-1991 and 1991-2006.</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>REFERENCES</title>
         <ref id="d3729e347a1310">
            <mixed-citation id="d3729e351" publication-type="other">
Bongaarts, John (1990) The Measurement of Wanted Fertility. Population and
Development Review 16:3,487-506.</mixed-citation>
         </ref>
         <ref id="d3729e361a1310">
            <mixed-citation id="d3729e365" publication-type="other">
Bongaarts, John (1997) Trends in Unwanted Childbearing in the Developing World.
Studies in Family Planning 28:4, 267-277.</mixed-citation>
         </ref>
         <ref id="d3729e375a1310">
            <mixed-citation id="d3729e379" publication-type="other">
Bryant, John (2007) Theories of Fertility Decline and the Evidence from Development
Indicators. Population and Development Review 33:1, 101-127.</mixed-citation>
         </ref>
         <ref id="d3729e389a1310">
            <mixed-citation id="d3729e393" publication-type="other">
Casterline, John B. and Laila O. el-Zeini (2007) Estimation of Unwanted Fertility.
Demography 44:4, 729-745.</mixed-citation>
         </ref>
         <ref id="d3729e404a1310">
            <mixed-citation id="d3729e408" publication-type="other">
Freedman, Ronald (1997) Do Family Planning Programmes Affect Fertility Preferences?
A Literature Rview. Studies in Family Planning 28: 1,1-13.</mixed-citation>
         </ref>
         <ref id="d3729e418a1310">
            <mixed-citation id="d3729e422" publication-type="other">
Knowles, James C, John S. Akin, and David K. Guilkey (1994) The Impact of
Population Policies: Comment. Population and Development Review 20:3, 611-615.</mixed-citation>
         </ref>
         <ref id="d3729e432a1310">
            <mixed-citation id="d3729e436" publication-type="other">
Lightbourne, Robert E. (1985) Individual Preferences and Fertility Behaviour. In John
Cleland and John Hobcraft (eds.) Reproductive Change in Developing Countries:
Insights from the World Fertility Survey, 165-198. Oxford: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d3729e449a1310">
            <mixed-citation id="d3729e453" publication-type="other">
Pritchett, Lant H. (1994) Desired Fertility and the Impact of Population Policies.
Population and Development Review 20: 1, 1-55.</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>REFERENCE</title>
         <ref id="d3729e472a1310">
            <mixed-citation id="d3729e476" publication-type="other">
Freedman, R. (1997) Do Family Planning Programmes Affect Fertility
Preferences? A Literature Review. Studies in Family Planning 28:1, 1-13.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
