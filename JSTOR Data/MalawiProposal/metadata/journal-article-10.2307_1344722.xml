<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">economicpolicy</journal-id>
         <journal-id journal-id-type="jstor">j100599</journal-id>
         <journal-title-group>
            <journal-title>Economic Policy</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Blackwell Publishing</publisher-name>
         </publisher>
         <issn pub-type="ppub">02664658</issn>
         <issn pub-type="epub">14680327</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">1344722</article-id>
         <title-group>
            <article-title>One Money, One Market: The Effect of Common Currencies on Trade</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Andrew K.</given-names>
                  <surname>Rose</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Ben</given-names>
                  <surname>Lockwood</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Danny</given-names>
                  <surname>Quah</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>4</month>
            <year>2000</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">15</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">30</issue>
         <issue-id>i257850</issue-id>
         <fpage>9</fpage>
         <lpage>45</lpage>
         <page-range>7-45</page-range>
         <permissions>
            <copyright-statement>Copyright 2000 Centre for Economic Policy Research, Center for Economic Studies, Maison des Sciences de l'Homme</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/1344722"/>
         <abstract>
            <p>A gravity model is used to assess the separate effects of exchange rate volatility and currency unions on international trade. The panel data, bilateral observations for five years during 1970-90 covering 186 countries, includes 300+ observations in which both countries use the same currency. I find a large positive effect of a currency union on international trade, and a small negative effect of exchange rate volatility, even after controlling for a host of features, including the endogenous nature of the exchange rate regime. These effects, statistically significant, imply that two countries sharing the same currency trade three times as much as they would with different currencies. Currency unions like the European EMU may thus lead to a large increase in international trade, with all that that entails.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d211e144a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d211e151" publication-type="book">
The Calmfors Commission (1997)  </mixed-citation>
            </p>
         </fn>
         <fn id="d211e160a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d211e167" publication-type="other">
Murray and Papell (1999)</mixed-citation>
            </p>
         </fn>
         <fn id="d211e174a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d211e181" publication-type="book">
UN's International Trade Statistics Yearbook<source>International Trade Statistics Yearbook</source>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d211e191a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d211e198" publication-type="other">
The 1998 World Factbook available at http://www.odci.gov/cia/publications/factbook/index.html</mixed-citation>
            </p>
         </fn>
         <fn id="d211e206a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d211e213" publication-type="other">
The 1998 World Factbook available at http://www.odci.gov/cia/publications/factbook/index.html</mixed-citation>
            </p>
         </fn>
         <fn id="d211e220a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d211e227" publication-type="journal">
European Commission (1990)  </mixed-citation>
            </p>
         </fn>
         <fn id="d211e236a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d211e243" publication-type="book">
IMF: Annual Report on Exchange Arrangements and Exchange Restrictions<person-group>
                     <string-name>
                        <surname>IMF</surname>
                     </string-name>
                  </person-group>
                  <source>Annual Report on Exchange Arrangements and Exchange Restrictions</source>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d211e261" publication-type="journal">
Mauro
(1995)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d211e272" publication-type="other">
Fraser Institute at http://www.freetheworld.com</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d211e279" publication-type="journal">
Ghosh et al. (1999)  </mixed-citation>
            </p>
         </fn>
         <fn id="d211e288a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d211e295" publication-type="book">
Buiter, 1999  </mixed-citation>
            </p>
         </fn>
         <fn id="d211e304a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d211e311" publication-type="other">
Cohen states (p. 194)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d211e317" publication-type="book">
Worrell et al, 1998  </mixed-citation>
            </p>
         </fn>
         <fn id="d211e326a1310">
            <label>34</label>
            <p>
               <mixed-citation id="d211e333" publication-type="journal">
Baldwin (1991)  </mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d211e351a1310">
            <mixed-citation id="d211e355" publication-type="journal">
Anderson, J.E. (1979). 'A theoretical foundation for the gravity equation', American Economic Review.<person-group>
                  <string-name>
                     <surname>Anderson</surname>
                  </string-name>
               </person-group>
               <source>American Economic Review</source>
               <year>1979</year>
            </mixed-citation>
         </ref>
         <ref id="d211e377a1310">
            <mixed-citation id="d211e381" publication-type="book">
Bacchetta, P. and E. van Wincoop (1998). 'Does exchange rate stability increase trade and capital
flows?', FRBNY Research Paper Number 9818.<person-group>
                  <string-name>
                     <surname>Bacchetta</surname>
                  </string-name>
               </person-group>
               <source>Does exchange rate stability increase trade and capital flows?</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d211e406a1310">
            <mixed-citation id="d211e410" publication-type="journal">
Baldwin, R.E. (1991). 'On the microeconomics of the European Monetary Union', European
Economy.<person-group>
                  <string-name>
                     <surname>Baldwin</surname>
                  </string-name>
               </person-group>
               <source>European Economy</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d211e435a1310">
            <mixed-citation id="d211e439" publication-type="journal">
Bergstrand, J.H. (1985). 'The gravity equation in international trade: some microeconomic
foundations and empirical evidence', Review of Economics and Statistics.<object-id pub-id-type="doi">10.2307/1925976</object-id>
            </mixed-citation>
         </ref>
         <ref id="d211e453a1310">
            <mixed-citation id="d211e457" publication-type="journal">
Bergstrand, J.H. (1989). 'The generalised gravity equation monopolistic competition, and the
factor-proportions theory in international trade', Review of Economics and Statistics.<person-group>
                  <string-name>
                     <surname>Bergstrand</surname>
                  </string-name>
               </person-group>
               <source>Review of Economics and Statistics</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d211e482a1310">
            <mixed-citation id="d211e486" publication-type="journal">
Boisso, D. and M. Ferrantino (1997). 'Economic distance, cultural distance, and openness in
international trade: empirical puzzles', Journal of Economic Integration.<person-group>
                  <string-name>
                     <surname>Boisso</surname>
                  </string-name>
               </person-group>
               <source>Journal of Economic Integration</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d211e511a1310">
            <mixed-citation id="d211e515" publication-type="book">
Buiter, W.H. (1999). 'The EMU and the NAMU', CEPR Discussion Paper no. 2181.<person-group>
                  <string-name>
                     <surname>Buiter</surname>
                  </string-name>
               </person-group>
               <source>The EMU and the NAMU</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d211e537a1310">
            <mixed-citation id="d211e541" publication-type="book">
Calmfors Commission (1997). EMU: A Swedish Perspective, Kluwer, Norwell.<person-group>
                  <string-name>
                     <surname>Calmfors Commission</surname>
                  </string-name>
               </person-group>
               <source>EMU: A Swedish Perspective</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d211e563a1310">
            <mixed-citation id="d211e567" publication-type="journal">
Cohen, B.J. (1993). 'Beyond EMU: the problem of sustainability', Economics and Politics.<person-group>
                  <string-name>
                     <surname>Cohen</surname>
                  </string-name>
               </person-group>
               <source>Economics and Politics</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d211e589a1310">
            <mixed-citation id="d211e593" publication-type="book">
Deardorff, A.V. (1998). 'Determinants of bilateral trade: does gravity work in a neoclassical
world?', in J.A. Frankel (ed.), The Regionalization of the World Economy, Chicago University Press,
Chicago.<person-group>
                  <string-name>
                     <surname>Deardorff</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Determinants of bilateral trade: does gravity work in a neoclassical world</comment>
               <source>The Regionalization of the World Economy</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d211e626a1310">
            <mixed-citation id="d211e630" publication-type="book">
de Grauwe, P. (1988). 'Exchange rate variability and the slowdown in the growth of international
trade', IMF Staff Papers.<person-group>
                  <string-name>
                     <surname>de Grauwe</surname>
                  </string-name>
               </person-group>
               <source>Exchange rate variability and the slowdown in the growth of international trade</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d211e655a1310">
            <mixed-citation id="d211e659" publication-type="journal">
Eichengreen, B. and D.A. Irwin (1995). 'Trade blocs, currency blocs and the reorientation of world
trade in the 1930s', Journal of International Economics.<person-group>
                  <string-name>
                     <surname>Eichengreen</surname>
                  </string-name>
               </person-group>
               <source>Journal of International Economics</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d211e684a1310">
            <mixed-citation id="d211e688" publication-type="journal">
Engel, C.M. and J.H. Rogers (1996). 'How wide is the border?', American Economic Review.<person-group>
                  <string-name>
                     <surname>Engel</surname>
                  </string-name>
               </person-group>
               <source>American Economic Review</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d211e710a1310">
            <mixed-citation id="d211e714" publication-type="journal">
European Commission (1990). 'One market, one money', European Economy.<person-group>
                  <string-name>
                     <surname>European Commission</surname>
                  </string-name>
               </person-group>
               <source>European Economy</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d211e736a1310">
            <mixed-citation id="d211e740" publication-type="book">
Evenett, S.J. and W. Keller (1998). 'On theories explaining the success of the gravity equation',
NBER Working Paper no. 6529.<person-group>
                  <string-name>
                     <surname>Evenett</surname>
                  </string-name>
               </person-group>
               <source>On theories explaining the success of the gravity equation</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d211e765a1310">
            <mixed-citation id="d211e769" publication-type="book">
Feenstra, R.C., R.E. Lipsey and H.P. Bowen (1997). 'World trade flows, 1970-1992, with
production and tariff data', NBER Working Paper no. 5910.<person-group>
                  <string-name>
                     <surname>Feenstra</surname>
                  </string-name>
               </person-group>
               <source>World trade flows, 1970-1992, with production and tariff data</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d211e795a1310">
            <mixed-citation id="d211e799" publication-type="book">
Feenstra, R.C., J. Markusen and A. Rose (1998). 'Understanding the home market effect and the
gravity equation: the role of differentiating goods', NBER Working Paper no. 6804.<person-group>
                  <string-name>
                     <surname>Feenstra</surname>
                  </string-name>
               </person-group>
               <source>Understanding the home market effect and the gravity equation: the role of differentiating goods</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d211e824a1310">
            <mixed-citation id="d211e828" publication-type="book">
Feldstein, M. (1991). 'Does one market require one money?', in Policy Implications of Trade and
Currency Zones, Federal Reserve Bank of Kansas City.<person-group>
                  <string-name>
                     <surname>Feldstein</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Does one market require one money?</comment>
               <source>Policy Implications of Trade and Currency Zones</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d211e857a1310">
            <mixed-citation id="d211e861" publication-type="journal">
Feldstein, M. (1997). 'The political economy of the European Economic and Monetary Union',
Journal of Economic Perspectives.<object-id pub-id-type="jstor">10.2307/2138460</object-id>
            </mixed-citation>
         </ref>
         <ref id="d211e874a1310">
            <mixed-citation id="d211e878" publication-type="journal">
Frankel, J.A. and D. Romer (1999). 'Does trade cause growth?', American Economic Review.<person-group>
                  <string-name>
                     <surname>Frankel</surname>
                  </string-name>
               </person-group>
               <source>American Economic Review</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d211e900a1310">
            <mixed-citation id="d211e904" publication-type="journal">
Frankel, J.A. and A.K. Rose (1998). 'The endogeneity of the optimum currency area criteria',
Economic Journal.<object-id pub-id-type="jstor">10.2307/2565665</object-id>
            </mixed-citation>
         </ref>
         <ref id="d211e917a1310">
            <mixed-citation id="d211e921" publication-type="book">
Frankel, J.A. and S-J. Wei (1993). 'Trade blocs and currency blocks', NBER Working Paper No.
4335, published in G. de la Dehaza et al. (eds.), The Monetary Future of Europe, CEPR, London.<person-group>
                  <string-name>
                     <surname>Frankel</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Trade blocs and currency blocks</comment>
               <source>The Monetary Future of Europe</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d211e951a1310">
            <mixed-citation id="d211e955" publication-type="journal">
Ghosh, A.R., A-M. Gulde and H.C. Wolf (1999). 'Currency boards: more than a quick fix?',
Economic Policy.<person-group>
                  <string-name>
                     <surname>Ghosh</surname>
                  </string-name>
               </person-group>
               <source>Economic Policy</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d211e980a1310">
            <mixed-citation id="d211e984" publication-type="journal">
Helliwell, J.F. (1996). 'Do national borders matter for Quebec's trade?', Canadian Journal of
Economics.<person-group>
                  <string-name>
                     <surname>Helliwell</surname>
                  </string-name>
               </person-group>
               <source>Canadian Journal of Economics</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d211e1009a1310">
            <mixed-citation id="d211e1013" publication-type="journal">
Hooper, P. and S. Kohlhagen (1978). 'The effect of exchange rate uncertainty on prices and
volumes of international trade', Journal of International Economics.<person-group>
                  <string-name>
                     <surname>Hooper</surname>
                  </string-name>
               </person-group>
               <source>Journal of International Economics</source>
               <year>1978</year>
            </mixed-citation>
         </ref>
         <ref id="d211e1038a1310">
            <mixed-citation id="d211e1042" publication-type="book">
International Monetary Fund (1984). 'Exchange rate variability and world trade', Occasional
Paper 28.<person-group>
                  <string-name>
                     <surname>International Monetary Fund</surname>
                  </string-name>
               </person-group>
               <source>Exchange rate variability and world trade</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d211e1067a1310">
            <mixed-citation id="d211e1071" publication-type="journal">
Kenen, P. and D. Rodrik (1986). 'Measuring and analysing the effects of short-term volatility in
real exchange rates', Review of Economics and Statistics.<person-group>
                  <string-name>
                     <surname>Kenen</surname>
                  </string-name>
               </person-group>
               <source>Review of Economics and Statistics</source>
               <year>1986</year>
            </mixed-citation>
         </ref>
         <ref id="d211e1096a1310">
            <mixed-citation id="d211e1100" publication-type="book">
Leamer, E.E. and J. Levinsohn (1995). 'International trade theory: the evidence', in G. Grossman
and K. Rogoff (eds.), The Handbook of International Economics, vol. 3, Elsevier, North-Holland.<person-group>
                  <string-name>
                     <surname>Leamer</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">International trade theory: the evidence</comment>
               <volume>3</volume>
               <source>The Handbook of International Economics</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d211e1133a1310">
            <mixed-citation id="d211e1137" publication-type="journal">
Mauro, P. (1995). 'Corruption and growth', Quarterly Journal of Economics.<object-id pub-id-type="doi">10.2307/2946696</object-id>
            </mixed-citation>
         </ref>
         <ref id="d211e1147a1310">
            <mixed-citation id="d211e1151" publication-type="journal">
MCCALLUM, J. (1995). 'National borders matter: Canada-US regional trade patterns', American
Economic Review.<person-group>
                  <string-name>
                     <surname>McCallum</surname>
                  </string-name>
               </person-group>
               <source>American Economic Review</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d211e1176a1310">
            <mixed-citation id="d211e1180" publication-type="journal">
Mundell, R.A. (1961). 'A theory of optimum currency areas', American Economic Review.<person-group>
                  <string-name>
                     <surname>Mundell</surname>
                  </string-name>
               </person-group>
               <source>American Economic Review</source>
               <year>1961</year>
            </mixed-citation>
         </ref>
         <ref id="d211e1202a1310">
            <mixed-citation id="d211e1206" publication-type="other">
Murray, C.K. and D.H. Papell (1999). 'The purchasing power parity persistence paradigm',
mimeo.</mixed-citation>
         </ref>
         <ref id="d211e1216a1310">
            <mixed-citation id="d211e1220" publication-type="journal">
Obstfeld, M. (1997). 'Europe's gamble', Brookings Papers on Economic Activity.<object-id pub-id-type="doi">10.2307/2534689</object-id>
            </mixed-citation>
         </ref>
         <ref id="d211e1230a1310">
            <mixed-citation id="d211e1234" publication-type="journal">
Pöyhönen, P. (1963). 'A tentative model for the volume of trade between countries',
Weltwertschaftliches Archiv.<person-group>
                  <string-name>
                     <surname>Pöyhönen</surname>
                  </string-name>
               </person-group>
               <source>Weltwertschaftliches Archiv</source>
               <year>1963</year>
            </mixed-citation>
         </ref>
         <ref id="d211e1260a1310">
            <mixed-citation id="d211e1264" publication-type="journal">
Savvides, A. (1992). 'Unanticipated exchange rate volatility and the growth of international trade',
Weltwirtschaftliches Archiv.<person-group>
                  <string-name>
                     <surname>Savvides</surname>
                  </string-name>
               </person-group>
               <source>Weltwirtschaftliches Archiv</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d211e1289a1310">
            <mixed-citation id="d211e1293" publication-type="book">
Tinbergen, J. (1962). Shaping the world economy: suggestions for an international economic policy, New York.<person-group>
                  <string-name>
                     <surname>Tinbergen</surname>
                  </string-name>
               </person-group>
               <source>Shaping the world economy: suggestions for an international economic policy</source>
               <year>1962</year>
            </mixed-citation>
         </ref>
         <ref id="d211e1315a1310">
            <mixed-citation id="d211e1319" publication-type="book">
Worrell, D., D. Marshall and N. Smith (1998). 'The political economy of exchange rate policy in
the English-speaking Caribbean', mimeo, University of the West Indies.<person-group>
                  <string-name>
                     <surname>Worrell</surname>
                  </string-name>
               </person-group>
               <source>The political economy of exchange rate policy in the English-speaking Caribbean</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d211e1344a1310">
            <mixed-citation id="d211e1348" publication-type="journal">
Wyplosz, C. (1997). 'EMU: why and how it might happen', Journal of Economic Perspectives.<object-id pub-id-type="jstor">10.2307/2138459</object-id>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
