<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">racethmulglocon</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000611</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Race/Ethnicity: Multidisciplinary Global Contexts</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Indiana University Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">19358644</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">19358652</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">25594988</article-id>
         <title-group>
            <article-title>Sticks and Scones: Black and White Women in the Homecraft Movement in Colonial Zimbabwe</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Carolyn Martin</given-names>
                  <surname>Shaw</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>4</month>
            <year>2008</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i25594981</issue-id>
         <fpage>253</fpage>
         <lpage>278</lpage>
         <permissions>
            <copyright-statement>Copyright 2008 The Ohio State University</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/25594988"/>
         <abstract>
            <p>In Colonial Zimbabwe, white women, primarily British settlers from South Africa and the United Kingdom, formed groups to socialize with each other to counteract the isolation they experienced on dispersed farms and ranches. During a period marked by the idea of progress and the fear of African nationalism, these middle- and upper-class colonial women reached out to African women, forming Homecraft clubs that taught various domestic tasks such as sewing and cooking. Homecraft clubs were part of a colonialist project to capture the loyalty of upwardly mobile rural and urban women. In developing the very popular Homecraft clubs, white women placed their skills in the service of the colonial state on a scale never before seen. For black women, especially of the Shona ethnic group, the majority population and the group focused on in this study, Homecraft groups provided opportunities to gather socially in a new way, which ultimately led to new forms of social and political organization. Organizational skills (and cooking skills) that African women learned through their affiliation with a white-sponsored organization, in some cases protected them from the violence of the liberation forces and, in other cases, were used in support of the independence movement. The domestic was the site of civil society; domestic spaces and activities reached into the public realm with very real and significant effects. Thus, Homecraft groups challenged dualistic conceptions of home versus public and paved the way for greater participation in civil society for both black and white women.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Endnotes</title>
         <ref id="d14914e114a1310">
            <label>2</label>
            <mixed-citation id="d14914e121" publication-type="other">
"African Politics in Twentieth-Century Southern Rhodesia," 238.</mixed-citation>
            <mixed-citation id="d14914e127" publication-type="other">
Shamuyarira, Crisis in Rhodesia, 15-25.</mixed-citation>
         </ref>
         <ref id="d14914e134a1310">
            <label>3</label>
            <mixed-citation id="d14914e141" publication-type="other">
Home and Country, Vol. 1 (1952).</mixed-citation>
         </ref>
         <ref id="d14914e148a1310">
            <label>4</label>
            <mixed-citation id="d14914e155" publication-type="other">
Marja Hinfelaar
(Respectable and Responsible Women, 73-74)</mixed-citation>
         </ref>
         <ref id="d14914e165a1310">
            <label>6</label>
            <mixed-citation id="d14914e172" publication-type="other">
Shaw ("Disciplining the Black Female Body" and "Working with Fem-
inists in Zimbabwe").</mixed-citation>
         </ref>
         <ref id="d14914e183a1310">
            <label>8</label>
            <mixed-citation id="d14914e190" publication-type="other">
Barnes and Win, To
Live a Better Life, 157-58</mixed-citation>
         </ref>
         <ref id="d14914e200a1310">
            <label>9</label>
            <mixed-citation id="d14914e207" publication-type="other">
Home and Country, Vol. 1 (1952).</mixed-citation>
         </ref>
         <ref id="d14914e214a1310">
            <label>11</label>
            <mixed-citation id="d14914e221" publication-type="other">
Schmidt "Race, Sex, and Domestic Labor," 229-30.</mixed-citation>
         </ref>
         <ref id="d14914e228a1310">
            <label>12</label>
            <mixed-citation id="d14914e235" publication-type="other">
West (Rise of an African Middle Class, 71)</mixed-citation>
         </ref>
         <ref id="d14914e242a1310">
            <label>13</label>
            <mixed-citation id="d14914e249" publication-type="other">
Rhodesia Home and Country, Vol. 1 (1939).</mixed-citation>
         </ref>
         <ref id="d14914e256a1310">
            <label>14</label>
            <mixed-citation id="d14914e263" publication-type="other">
Rhodesia Home and Country, Vol. 1 (1938).</mixed-citation>
         </ref>
         <ref id="d14914e271a1310">
            <label>15</label>
            <mixed-citation id="d14914e280" publication-type="other">
Jock McCulloch (Black Peril, White Virtue, 103-06)</mixed-citation>
         </ref>
         <ref id="d14914e287a1310">
            <label>16</label>
            <mixed-citation id="d14914e294" publication-type="other">
McCulloch (105)</mixed-citation>
            <mixed-citation id="d14914e300" publication-type="other">
Schmidt ("Race, Sex, and Domestic Labor,"
234)</mixed-citation>
         </ref>
         <ref id="d14914e310a1310">
            <label>17</label>
            <mixed-citation id="d14914e317" publication-type="other">
McCulloch, 104-06.</mixed-citation>
         </ref>
         <ref id="d14914e324a1310">
            <label>18</label>
            <mixed-citation id="d14914e331" publication-type="other">
Hancock (White Liberals, Moderates, and Radicals in Rhodesia,
16-17)</mixed-citation>
            <mixed-citation id="d14914e340" publication-type="other">
Sylvester (Zimbabwe: The Terrain of Contradictory Development, 36-45)</mixed-citation>
            <mixed-citation id="d14914e346" publication-type="other">
Raftopoulos ("The Labour Movement in Zimbabwe: 1945-1965," 56)</mixed-citation>
         </ref>
         <ref id="d14914e353a1310">
            <label>19</label>
            <mixed-citation id="d14914e360" publication-type="other">
Scott, Refashioning Futures, 25.</mixed-citation>
            <mixed-citation id="d14914e366" publication-type="other">
Gordon, "Governmental
Rationality."</mixed-citation>
         </ref>
         <ref id="d14914e376a1310">
            <label>20</label>
            <mixed-citation id="d14914e383" publication-type="other">
Foucault, History of Sexuality, Vol. 3.</mixed-citation>
         </ref>
         <ref id="d14914e391a1310">
            <label>21</label>
            <mixed-citation id="d14914e398" publication-type="other">
Chennells, Ph.D. dissertation, 1982.</mixed-citation>
         </ref>
         <ref id="d14914e405a1310">
            <label>22</label>
            <mixed-citation id="d14914e412" publication-type="other">
Ranger, "African Politics in Twentieth-Century Southern Rho-
desia," 237.</mixed-citation>
         </ref>
         <ref id="d14914e422a1310">
            <label>23</label>
            <mixed-citation id="d14914e429" publication-type="other">
Phimister, "Rethinking the Reserves," 227.</mixed-citation>
         </ref>
         <ref id="d14914e436a1310">
            <label>24</label>
            <mixed-citation id="d14914e443" publication-type="other">
Ranger ("African Politics in Twentieth-Century Southern Rho-
desia," 237)</mixed-citation>
         </ref>
         <ref id="d14914e453a1310">
            <label>25</label>
            <mixed-citation id="d14914e460" publication-type="other">
Hancock; West; Schmidt, "Race, Sex, and Domestic Labor"</mixed-citation>
            <mixed-citation id="d14914e466" publication-type="other">
Ranger, Are We Not Also Men</mixed-citation>
            <mixed-citation id="d14914e472" publication-type="other">
Phimister [Economic and Social History of Zimbabwe]</mixed-citation>
            <mixed-citation id="d14914e479" publication-type="other">
Moore,
"Ideological Formation of the Zimbabwean Ruling Class"</mixed-citation>
         </ref>
         <ref id="d14914e489a1310">
            <label>26</label>
            <mixed-citation id="d14914e496" publication-type="other">
Hughes, Capricorn.</mixed-citation>
         </ref>
         <ref id="d14914e504a1310">
            <label>27</label>
            <mixed-citation id="d14914e511" publication-type="other">
Hancock, 41</mixed-citation>
            <mixed-citation id="d14914e517" publication-type="other">
Moore, 484</mixed-citation>
            <mixed-citation id="d14914e523" publication-type="other">
Shamuyarira, Crisis in Rhode-
sia, 15-25.</mixed-citation>
         </ref>
         <ref id="d14914e533a1310">
            <label>28</label>
            <mixed-citation id="d14914e540" publication-type="other">
Home and Country, Vol. 1 (1952).</mixed-citation>
         </ref>
         <ref id="d14914e547a1310">
            <label>29</label>
            <mixed-citation id="d14914e554" publication-type="other">
Faubion, ed., Power: Essential Works of Foucault.</mixed-citation>
         </ref>
         <ref id="d14914e561a1310">
            <label>30</label>
            <mixed-citation id="d14914e568" publication-type="other">
West (72)</mixed-citation>
            <mixed-citation id="d14914e574" publication-type="other">
Home and
Country, Vol. 1 (1952)</mixed-citation>
         </ref>
         <ref id="d14914e584a1310">
            <label>31</label>
            <mixed-citation id="d14914e593" publication-type="other">
West (79)</mixed-citation>
            <mixed-citation id="d14914e599" publication-type="other">
West (78).</mixed-citation>
         </ref>
         <ref id="d14914e606a1310">
            <label>32</label>
            <mixed-citation id="d14914e613" publication-type="other">
Ur-
ban-Mead's 2004 Ph.D. dissertation</mixed-citation>
         </ref>
         <ref id="d14914e624a1310">
            <label>33</label>
            <mixed-citation id="d14914e631" publication-type="other">
Ranchod-Nilsson, "Educating Eve," 196.</mixed-citation>
         </ref>
         <ref id="d14914e638a1310">
            <label>34</label>
            <mixed-citation id="d14914e645" publication-type="other">
Home and Country, Vol. 9 (1962).</mixed-citation>
         </ref>
         <ref id="d14914e652a1310">
            <label>37</label>
            <mixed-citation id="d14914e659" publication-type="other">
Gaidzanwa, "Ideology of Domesticity," 276.</mixed-citation>
         </ref>
         <ref id="d14914e666a1310">
            <label>38</label>
            <mixed-citation id="d14914e673" publication-type="other">
Ranger, Are We Not Also Men, 42 and Hinfelaar.</mixed-citation>
         </ref>
         <ref id="d14914e680a1310">
            <label>39</label>
            <mixed-citation id="d14914e687" publication-type="other">
Hinfelaar (50)</mixed-citation>
            <mixed-citation id="d14914e693" publication-type="other">
Re-
spectable and Responsible Women, 50.</mixed-citation>
         </ref>
         <ref id="d14914e703a1310">
            <label>40</label>
            <mixed-citation id="d14914e710" publication-type="other">
Barnes and Win, 155.</mixed-citation>
         </ref>
         <ref id="d14914e718a1310">
            <label>41</label>
            <mixed-citation id="d14914e725" publication-type="other">
Barnes and Win, 153-68.</mixed-citation>
         </ref>
         <ref id="d14914e732a1310">
            <label>42</label>
            <mixed-citation id="d14914e739" publication-type="other">
Ranchod-Nilsson 211-12.</mixed-citation>
         </ref>
         <ref id="d14914e746a1310">
            <label>43</label>
            <mixed-citation id="d14914e753" publication-type="other">
Zimbabwe Government Publica-
tion, Women's Groups in Zimbabwe.</mixed-citation>
         </ref>
         <ref id="d14914e763a1310">
            <label>44</label>
            <mixed-citation id="d14914e770" publication-type="other">
Hannan in the Standard Shona Dictionary</mixed-citation>
            <mixed-citation id="d14914e776" publication-type="other">
Ur-
ban's 2004 dissertation</mixed-citation>
         </ref>
         <ref id="d14914e786a1310">
            <label>45</label>
            <mixed-citation id="d14914e793" publication-type="other">
Interview with a forty-year-old woman who was not a member
of a women's group, 2000, Harare.</mixed-citation>
         </ref>
         <ref id="d14914e803a1310">
            <label>47</label>
            <mixed-citation id="d14914e810" publication-type="other">
Bourdieu, Logic of Practice.</mixed-citation>
         </ref>
         <ref id="d14914e818a1310">
            <label>48</label>
            <mixed-citation id="d14914e825" publication-type="other">
Ranger ("Protestant Missions in Africa," 293-95)</mixed-citation>
         </ref>
         <ref id="d14914e832a1310">
            <label>49</label>
            <mixed-citation id="d14914e839" publication-type="other">
Gelfand, African Witch, 212-13.</mixed-citation>
         </ref>
         <ref id="d14914e846a1310">
            <label>52</label>
            <mixed-citation id="d14914e853" publication-type="other">
Bourdieu (63-64).</mixed-citation>
         </ref>
         <ref id="d14914e860a1310">
            <label>53</label>
            <mixed-citation id="d14914e867" publication-type="other">
Schmidt (Peasants, Traders, and Wives and "Race,
Sex and Domestic Labor)</mixed-citation>
         </ref>
         <ref id="d14914e877a1310">
            <label>54</label>
            <mixed-citation id="d14914e884" publication-type="other">
Schmidt, Peasants, Traders, and Wives, 148.</mixed-citation>
         </ref>
         <ref id="d14914e891a1310">
            <label>55</label>
            <mixed-citation id="d14914e898" publication-type="other">
West, 72.</mixed-citation>
         </ref>
         <ref id="d14914e906a1310">
            <label>56</label>
            <mixed-citation id="d14914e913" publication-type="other">
Home and Country, Vol. 9 (1962).</mixed-citation>
         </ref>
         <ref id="d14914e920a1310">
            <label>58</label>
            <mixed-citation id="d14914e927" publication-type="other">
Ranchod-Nilsson 213-14.</mixed-citation>
         </ref>
         <ref id="d14914e934a1310">
            <label>59</label>
            <mixed-citation id="d14914e941" publication-type="other">
Dangaremgba's 2006</mixed-citation>
            <mixed-citation id="d14914e947" publication-type="other">
Jacobs ("Gender Divisions and the Forma-
tion of Ethnicities in Zimbabwe," 255-57)</mixed-citation>
         </ref>
         <ref id="d14914e957a1310">
            <label>60</label>
            <mixed-citation id="d14914e964" publication-type="other">
Lan, Guns and Rain: Guerillas and Spirit Mediums in Zim-
babwe</mixed-citation>
         </ref>
         <ref id="d14914e974a1310">
            <label>61</label>
            <mixed-citation id="d14914e981" publication-type="other">
Yvonne Vera's novel Without a Name.</mixed-citation>
            <mixed-citation id="d14914e987" publication-type="other">
Staun ton, Mothers of the Revolution</mixed-citation>
            <mixed-citation id="d14914e993" publication-type="other">
Kriger, Zimbabwe's Guerilla War</mixed-citation>
            <mixed-citation id="d14914e1000" publication-type="other">
Kaler, "Maternal Identity and War"</mixed-citation>
            <mixed-citation id="d14914e1006" publication-type="other">
Nhongo-Simbanegavi, For
Better or Worse, 23-36.</mixed-citation>
         </ref>
         <ref id="d14914e1016a1310">
            <label>63</label>
            <mixed-citation id="d14914e1023" publication-type="other">
Taussig, Mimesis and Alterity</mixed-citation>
         </ref>
         <ref id="d14914e1031a1310">
            <label>64</label>
            <mixed-citation id="d14914e1038" publication-type="other">
Nhongo-Simbanegavi, 19.</mixed-citation>
         </ref>
         <ref id="d14914e1045a1310">
            <label>65</label>
            <mixed-citation id="d14914e1052" publication-type="other">
Nhongo-Simbanegavi (52-59)</mixed-citation>
         </ref>
         <ref id="d14914e1059a1310">
            <label>66</label>
            <mixed-citation id="d14914e1066" publication-type="other">
Jekesa Pfungwa/Vulingqondo Newsletter, Special Edition, No. 2
(1997): 7.</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>Works Cited</title>
         <ref id="d14914e1085a1310">
            <mixed-citation id="d14914e1089" publication-type="other">
Barnes, Teresa. 'We Women Worked So Hard': Gender, Urbanization, and
Social Reproduction in Colonial Harare, Zimbabwe, 1930-1956. Ports-
mouth, N.H.: Heinemann, 1999.</mixed-citation>
         </ref>
         <ref id="d14914e1102a1310">
            <mixed-citation id="d14914e1106" publication-type="other">
Barnes, Terri, and Everjoyce Win. To Live a Better Life: An Oral History of
Women in the City of Harare, 1930-70. Harare, Zimbabwe: Baobab
Books, 1992.</mixed-citation>
         </ref>
         <ref id="d14914e1119a1310">
            <mixed-citation id="d14914e1123" publication-type="other">
Bourdieu, Pierre. The Logic of Practice. Translated by Richard Nice.
Cambridge, UK: Polity Press, 1990.</mixed-citation>
         </ref>
         <ref id="d14914e1133a1310">
            <mixed-citation id="d14914e1137" publication-type="other">
Chennells, Anthony. "Settler Myths and the Southern Rhodesian
Novel." Ph.D. diss., University of Zimbabwe, 1982.</mixed-citation>
         </ref>
         <ref id="d14914e1148a1310">
            <mixed-citation id="d14914e1152" publication-type="other">
Dangarembga, Tsitsi. The Book of Not: A Sequel to Nervous Conditions.
Oxfordshire: Ayebia Clarke Publishing Ltd., 2006.</mixed-citation>
         </ref>
         <ref id="d14914e1162a1310">
            <mixed-citation id="d14914e1166" publication-type="other">
Faubion, James, ed. Power: Essential Works of Foucault, 1954-1984, Vol-
ume II. Translated by Robert Hurley. New York: The New Press,
1994.</mixed-citation>
         </ref>
         <ref id="d14914e1179a1310">
            <mixed-citation id="d14914e1183" publication-type="other">
Foucault, Michel. History of Sexuality, Volume 3: The Care of the Self
Translated by Robert Hurley. New York: Pantheon Books, 1986.</mixed-citation>
         </ref>
         <ref id="d14914e1193a1310">
            <mixed-citation id="d14914e1197" publication-type="other">
Gaidzanwa, Rudo. "The Ideology of Domesticity and the Struggles of
Women Workers in Zimbabwe." In Confronting State, Capital and
Patriarchy: Women Organizing in the Process of Industrialization,
edited by Amrita Chhachhi and Renée Pittin, 273-89. New York:
St. Martin's Press, 1996.</mixed-citation>
         </ref>
         <ref id="d14914e1216a1310">
            <mixed-citation id="d14914e1220" publication-type="other">
Gelfand, Michael. The African Witch: With Particular Reference to
Witchcraft Beliefs and Practice Among the Shona of Rhodesia. Edin-
burgh: E. and S. Livingstone, 1967.</mixed-citation>
         </ref>
         <ref id="d14914e1233a1310">
            <mixed-citation id="d14914e1237" publication-type="other">
Gordon, Colin. "Governmental Rationality: An Introduction." In The
Foucault Effect: Studies in Governmentality, edited by Graham Bur-
chell, Colin Gordon, and Peter Miller, 1-52. Chicago: University of
Chicago Press, 1991.</mixed-citation>
         </ref>
         <ref id="d14914e1254a1310">
            <mixed-citation id="d14914e1258" publication-type="other">
Hancock, Ian. White Liberals, Moderates, and Radicals in Rhodesia,
1953-1980. New York: St. Martin's Press, 1984.</mixed-citation>
         </ref>
         <ref id="d14914e1268a1310">
            <mixed-citation id="d14914e1272" publication-type="other">
Hannan, Michael. Standard Shona Dictionary. New York: St. Martin's
Press, 1959.</mixed-citation>
         </ref>
         <ref id="d14914e1282a1310">
            <mixed-citation id="d14914e1286" publication-type="other">
Hinfelaar, Marja. Respectable and Responsible Women: Methodist and Ro-
man Catholic Women's Organisations in Harare, Zimbabwe (1919-1985).
Zoetermeer: Uitgeverij Boekencentrum, 2001.</mixed-citation>
         </ref>
         <ref id="d14914e1299a1310">
            <mixed-citation id="d14914e1303" publication-type="other">
Home and Country: The Journal of the Federation of Women's Institutes. Bu-
lawayo and Harare, Zimbabwe: Federation of Women's Institutes
of Southern Rhodesia/Rhodesia/Zimbabwe, 1936-2000.</mixed-citation>
         </ref>
         <ref id="d14914e1316a1310">
            <mixed-citation id="d14914e1320" publication-type="other">
Hughes, Richard. Capricorn: David Stirling's Second African Campaign.
New York: Radcliffe, 2003.</mixed-citation>
         </ref>
         <ref id="d14914e1330a1310">
            <mixed-citation id="d14914e1334" publication-type="other">
Jacobs, Susie. "Gender Divisions and the Formation of Ethnicities in
Zimbabwe." In Unsettling Settler Societies: Articulations of Race, Eth-
nicity and Class, edited by Daiva Stasiulis and Nira Yuval-Davis,
241-62. London: Sage Publications, 1995.</mixed-citation>
         </ref>
         <ref id="d14914e1351a1310">
            <mixed-citation id="d14914e1355" publication-type="other">
Jekesa Pfungwa/Vulingqondo. Jekesa Pfungwa/Vulingqondo Newslet-
ter, Special Edition: 50 Years: 1947-1997. Harare, Zimbabwe: Jekesa
Pfungwa/Vulingqondo, 1997.</mixed-citation>
         </ref>
         <ref id="d14914e1368a1310">
            <mixed-citation id="d14914e1372" publication-type="other">
Kaler, Amy. "Maternal Identity and War in Mothers of the Revolution."
NWSA Journal 9, no. 1 (1997): 1-21.</mixed-citation>
         </ref>
         <ref id="d14914e1382a1310">
            <mixed-citation id="d14914e1386" publication-type="other">
Kriger, Norma. Zimbabwe's Guerilla War: Peasant Voices. Cambridge:
Cambridge University Press, 1992.</mixed-citation>
         </ref>
         <ref id="d14914e1396a1310">
            <mixed-citation id="d14914e1400" publication-type="other">
Lan, David. Guns and Rain: Guerillas and Spirit Mediums in Zimbabwe.
Berkeley: University of California Press, 1985.</mixed-citation>
         </ref>
         <ref id="d14914e1410a1310">
            <mixed-citation id="d14914e1414" publication-type="other">
Mate, Rekopantswe. "Wombs as God's Laboratories: Pentecostal Dis-
courses of Femininity in Zimbabwe." Africa: Journal of the Interna-
tional African Institute 72, no. 4 (2002): 549-68.</mixed-citation>
         </ref>
         <ref id="d14914e1427a1310">
            <mixed-citation id="d14914e1431" publication-type="other">
McCulloch, Jock. Black Peril, White Virtue: Sexual Crime in Southern
Rhodesia, 1902-1935. Bloomington: Indiana University Press, 2000.</mixed-citation>
         </ref>
         <ref id="d14914e1442a1310">
            <mixed-citation id="d14914e1446" publication-type="other">
Moore, David B. "The Ideological Formation of the Zimbabwean Rul-
ing Class." Journal of Southern African Studies 17, no. 3 (1991): 472-
95.</mixed-citation>
         </ref>
         <ref id="d14914e1459a1310">
            <mixed-citation id="d14914e1463" publication-type="other">
Muchena, Olivia. Report on Women's Organizations in Zimbabwe, Min-
istry of Community Development and Women's Affairs, assisted
by United Nations Fund for Population Activities, July 1985.</mixed-citation>
         </ref>
         <ref id="d14914e1476a1310">
            <mixed-citation id="d14914e1480" publication-type="other">
Nhongo-Simbanegavi, Josephine. For Better or Worse: Women and ZANLA
in Zimbabwe's Liberation Struggle. Harare, Zimbabwe: Weaver Press,
2000.</mixed-citation>
         </ref>
         <ref id="d14914e1493a1310">
            <mixed-citation id="d14914e1497" publication-type="other">
Phimister, Ian. An Economic and Social History of Zimbabwe, 1890-1948:
Capital Accumulation and Class Struggle. New York: Longman, 1988.</mixed-citation>
         </ref>
         <ref id="d14914e1507a1310">
            <mixed-citation id="d14914e1511" publication-type="other">
-. "Rethinking the Reserves: Southern Rhodesia's Land Hus-
bandry Act Reviewed." Journal of Southern African Studies 19, no. 2
(1993): 225-39.</mixed-citation>
         </ref>
         <ref id="d14914e1524a1310">
            <mixed-citation id="d14914e1528" publication-type="other">
Phimister, Ian, and Charles van Onselen. "The Labour Movement in
Zimbabwe: 1900-1945." In Keep on Knocking: A History of the Labour
Movement in Zimbabwe, 1900-1997, edited by Brian Raftopolous
and Ian Phimister. Harare, Zimbabwe: Baobab Books on behalf of
the Zimbabwe Congress of Trade Unions and Friedrich Ebert Sti-
fling, 1997.</mixed-citation>
         </ref>
         <ref id="d14914e1552a1310">
            <mixed-citation id="d14914e1556" publication-type="other">
Raftopoulos, Brian. "The Labour Movement in Zimbabwe: 1945-1965."
In Keep on Knocking: A History of the Labour Movement in Zimbabwe
1900-1997, edited by Brian Raftopoulos and Ian Phimister. Harare,
Zimbabwe: Baobab Books, 1997.</mixed-citation>
         </ref>
         <ref id="d14914e1572a1310">
            <mixed-citation id="d14914e1576" publication-type="other">
Ranchod-Nilsson, Sita. "'Educating Eve': The Women's Club Move-
ment and Political Consciousness among Rural African Women in
Southern Rhodesia, 1950-1980." In African Encounters with Domes-
ticity, edited by Karen Tranberg Hansen, 195-217. New Bruns-
wick, N.J.: Rutgers University Press, 1992.</mixed-citation>
         </ref>
         <ref id="d14914e1595a1310">
            <mixed-citation id="d14914e1599" publication-type="other">
Ranger, Terence. "African Politics in Twentieth-Century Southern Rho-
desia." In Aspects of Central African History, edited by Terence
Ranger. Evanston, 111.: Northwestern University Press, 1968.</mixed-citation>
         </ref>
         <ref id="d14914e1612a1310">
            <mixed-citation id="d14914e1616" publication-type="other">
-. Are We Not Also Men? The Samkange Family and African Politics
in Zimbabwe, 1920-64. Harare, Zimbabwe: Baobab Press, 1995.</mixed-citation>
         </ref>
         <ref id="d14914e1626a1310">
            <mixed-citation id="d14914e1630" publication-type="other">
-. "Protestant Missions in Africa: The Dialectic of Conversion in
the American Methodist Episcopal Church in Eastern Zimbabwe,
1900-1950." In Religion in Africa: Experience and Expression, edited
by Thomas D. Blakely, Walter E. A. van Beek, and Dennis L.
Thomson, 275-312. Portsmouth, N.H.: Heinemann, 1994.</mixed-citation>
         </ref>
         <ref id="d14914e1649a1310">
            <mixed-citation id="d14914e1653" publication-type="other">
Schmidt, Elizabeth. Peasants, Traders, and Wives: Shona Women in the
History of Zimbabwe, 1870-1939. Portsmouth, N.H.: Heinemann,
1992.</mixed-citation>
         </ref>
         <ref id="d14914e1667a1310">
            <mixed-citation id="d14914e1671" publication-type="other">
-. "Race, Sex, and Domestic Labor: The Question of African Fe-
male Servants in Southern Rhodesia, 1900-1939." In African En-
counters with Domesticity, edited by Karen Tranberg Hansen,
221-41. New Brunswick, N.J.: Rutgers University Press, 1992.</mixed-citation>
         </ref>
         <ref id="d14914e1687a1310">
            <mixed-citation id="d14914e1691" publication-type="other">
Scott, David. Refashioning Futures: Criticism after Postcoloniality. Prince-
ton, N.J.: Princeton University Press, 1999.</mixed-citation>
         </ref>
         <ref id="d14914e1701a1310">
            <mixed-citation id="d14914e1705" publication-type="other">
Shamuyarira, Nathan M. Crisis in Rhodesia. New York: Transatlantic
Arts, Inc.,1966.</mixed-citation>
         </ref>
         <ref id="d14914e1715a1310">
            <mixed-citation id="d14914e1719" publication-type="other">
Shaw, Carolyn Martin. "Disciplining the Black Female Body: Learning
Feminism in Africa and the United States." In Black Feminist An-
thropology: Theory, Politics, Praxis, and Poetics, edited by Irma Mc-
Claurin, 102-25. New Brunswick, N.J.: Rutgers University Press,
2001.</mixed-citation>
         </ref>
         <ref id="d14914e1738a1310">
            <mixed-citation id="d14914e1742" publication-type="other">
-. "Working with Feminists in Zimbabwe: A Black American's
Experience of Transnational Alliances." In Feminism and Antirac-
ism: International Struggles for Justice, edited by France Winddance
Twine and Kathleen Blee, 250-76. New York: New York University
Press, 2001.</mixed-citation>
         </ref>
         <ref id="d14914e1761a1310">
            <mixed-citation id="d14914e1765" publication-type="other">
Staunton, Irene. Mothers of the Revolution: The War Experience of Thirty
Zimbabwean Women. Bloomington: Indiana University Press, 1991.</mixed-citation>
         </ref>
         <ref id="d14914e1776a1310">
            <mixed-citation id="d14914e1780" publication-type="other">
Sylvester, Christine. Zimbabwe: The Terrain of Contradictory Develop-
ment. Boulder, Colo: Westview Press, 1991.</mixed-citation>
         </ref>
         <ref id="d14914e1790a1310">
            <mixed-citation id="d14914e1794" publication-type="other">
Taussig, Michael. Mimesis and Alterity: A Particular History of the Senses.
New York: Routledge, 1993.</mixed-citation>
         </ref>
         <ref id="d14914e1804a1310">
            <mixed-citation id="d14914e1808" publication-type="other">
Urban-Mead, Wendy. "Religion, Women, and Gender in the Brethren
in Christ Church, Matabeleland, Zimbabwe." Ph.D. diss., Colum-
bia University, 2004.</mixed-citation>
         </ref>
         <ref id="d14914e1821a1310">
            <mixed-citation id="d14914e1825" publication-type="other">
Vera, Yvonne. Without a Name. Harare, Zimbabwe: Baobab Press, 1994.</mixed-citation>
         </ref>
         <ref id="d14914e1832a1310">
            <mixed-citation id="d14914e1836" publication-type="other">
West, Michael O. The Rise of an African Middle Class: Colonial Zimbabwe
1898-1965. Bloomington: Indiana University Press, 2002.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
