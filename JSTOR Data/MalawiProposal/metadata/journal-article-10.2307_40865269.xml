<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">reviworleconwelt</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000549</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Review of World Economics / Weltwirtschaftliches Archiv</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Springer</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">16102878</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">16102886</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">40865269</article-id>
         <title-group>
            <article-title>Output collapses and productivity destruction</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Juan S.</given-names>
                  <surname>Blyde</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Christian</given-names>
                  <surname>Daude</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Eduardo</given-names>
                  <surname>Fernández-Arias</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>6</month>
            <year>2010</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">146</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40038981</issue-id>
         <fpage>359</fpage>
         <lpage>387</lpage>
         <permissions>
            <copyright-statement>© Kiel Institute 2010</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/40865269"/>
         <abstract>
            <p>In this paper we analyze the long-run relationship between output collapses— defined as GDP falling substantially below trend— and total factor productivity (TFP). We use a panel of 76 developed and developing countries during the period 1960-2004 to identify episodes of output collapse and estimate counterfactual post-collapse TFP trends. Collapses are concentrated in developing countries, especially Africa and Latin America, and were particularly widespread in the 1980s in Latin America. Overall, output collapses are systematically associated with long-lasting declines in TFP. We explore the conditions under which collapses are least or most damaging, as well as the type of shocks that make collapses more likely or severe. Furthermore, we provide a quantification of the associated welfare loss with output collapses.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d880e268a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d880e275" publication-type="other">
Hausmann et al. (2005),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d880e281" publication-type="other">
Jones and Olken (2008)</mixed-citation>
            </p>
         </fn>
         <fn id="d880e288a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d880e295" publication-type="other">
Caballero and Hammour (2005)</mixed-citation>
            </p>
         </fn>
         <fn id="d880e302a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d880e309" publication-type="other">
Drazen and Easterly (2001)</mixed-citation>
            </p>
         </fn>
         <fn id="d880e316a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d880e323" publication-type="other">
Blyde et al. (2008)</mixed-citation>
            </p>
         </fn>
         <fn id="d880e331a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d880e338" publication-type="other">
Bergoeing et al. (2004).</mixed-citation>
            </p>
         </fn>
         <fn id="d880e345a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d880e352" publication-type="other">
Daude and Fernández-Arias (2010)</mixed-citation>
            </p>
         </fn>
         <fn id="d880e359a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d880e366" publication-type="other">
Maddala and
Wu 1999;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d880e375" publication-type="other">
Im et al. 2003;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d880e381" publication-type="other">
Pesaran 2003;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d880e388" publication-type="other">
Levin et al. 2002</mixed-citation>
            </p>
         </fn>
         <fn id="d880e395a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d880e402" publication-type="other">
Uhlig and Ravn (2002).</mixed-citation>
            </p>
         </fn>
         <fn id="d880e409a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d880e416" publication-type="other">
Cerra and
Saxena (2008).</mixed-citation>
            </p>
         </fn>
         <fn id="d880e426a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d880e433" publication-type="other">
Caselli (2005)</mixed-citation>
            </p>
         </fn>
         <fn id="d880e441a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d880e448" publication-type="other">
www.em-dat.net.</mixed-citation>
            </p>
         </fn>
         <fn id="d880e455a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d880e462" publication-type="other">
Bell and Pain (2000),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d880e468" publication-type="other">
Caprio and
Klingebiel (2003),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d880e477" publication-type="other">
Demirgiiç-Kunt and Detragiache (2005),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d880e484" publication-type="other">
Kaminsky and Reinhart (1999),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d880e490" publication-type="other">
Detraigiache and Spilimbergo (2001),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d880e496" publication-type="other">
Manasse and Roubini (2005),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d880e502" publication-type="other">
Reinhart et al. (2003).</mixed-citation>
            </p>
         </fn>
         <fn id="d880e509a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d880e516" publication-type="other">
Lucas (1987)</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d880e532a1310">
            <mixed-citation id="d880e536" publication-type="other">
Aghion, P., &amp; Saint-Paul, G. (1998). On the virtues of bad times: An analysis of the interaction between
productivity growth and economic fluctuations. Macroeconomic Dynamics, 2(3), 322-344.</mixed-citation>
         </ref>
         <ref id="d880e546a1310">
            <mixed-citation id="d880e550" publication-type="other">
Aguiar, M., &amp; Gopinath, G. (2007). Emerging market business cycles: The cycle is the trend. Journal of
Political Economy, 775(1), 69-102.</mixed-citation>
         </ref>
         <ref id="d880e560a1310">
            <mixed-citation id="d880e564" publication-type="other">
Bai, J., &amp; Perron, P. (1998). Estimating and testing linear models with multiple structural breaks.
Econometrica, 66(1), 47-78.</mixed-citation>
         </ref>
         <ref id="d880e574a1310">
            <mixed-citation id="d880e578" publication-type="other">
Bai, J., &amp; Perron, P. (2003). Computation and analysis of multiple structural change models. Journal of
Applied Econometrics, 18(1), 1-22.</mixed-citation>
         </ref>
         <ref id="d880e589a1310">
            <mixed-citation id="d880e593" publication-type="other">
Barro, R. J. (2006). On the welfare costs of consumption uncertainty, (NBER Working Papers 12763).
Cambridge, MA: National Bureau of Economic Research.</mixed-citation>
         </ref>
         <ref id="d880e603a1310">
            <mixed-citation id="d880e607" publication-type="other">
Barro, R. J., &amp; Lee, J.-W. (2000). International data on educational attainment: Updates and
implications, (CID Working Paper 42). Cambridge, MA: Harvard University.</mixed-citation>
         </ref>
         <ref id="d880e617a1310">
            <mixed-citation id="d880e621" publication-type="other">
Becker, T., &amp; Mauro, P. (2006). Output drops, and the shocks that matter, (IMF Working Paper WP/06/
172). Washington, DC: International Monetary Fund.</mixed-citation>
         </ref>
         <ref id="d880e631a1310">
            <mixed-citation id="d880e635" publication-type="other">
Bell, J., &amp; Pain, D. (2000). Leading indicators of banking crises: A critical review, Financial Stability
Report 9, Bank of England, pp. 113-129.</mixed-citation>
         </ref>
         <ref id="d880e645a1310">
            <mixed-citation id="d880e649" publication-type="other">
Berg, A., Ostry, J. D., &amp; Zettelmeyer, J. (2006). What makes growth sustained? Paper Presented at the
2006 IMF conference "Economic Growth and Latin America: what have we learned?".</mixed-citation>
         </ref>
         <ref id="d880e659a1310">
            <mixed-citation id="d880e663" publication-type="other">
Bergoeing, R., Loayza, N., &amp; Repetto, A. (2004). Slow recoveries. Journal of Development Economics,
75(2), 473-506.</mixed-citation>
         </ref>
         <ref id="d880e674a1310">
            <mixed-citation id="d880e678" publication-type="other">
Blyde, J.S., Daude, C, &amp; Fernandez-Arias, E. (2008). Output collapses and productivity destruction,
(Working Paper 666). Washington, DC: Research Department, Inter-American Development Bank.</mixed-citation>
         </ref>
         <ref id="d880e688a1310">
            <mixed-citation id="d880e692" publication-type="other">
Blyde, J.S., &amp; Fernández-Arias, E. (2005). Why Latin America is falling behind. In E. Fernandez-Ari as,
R. E. Manuelli, &amp; J. S. Blyde (Eds.), Sources of growth in latin America: What is missing?
Washington DC: Inter-American Development Bank.</mixed-citation>
         </ref>
         <ref id="d880e705a1310">
            <mixed-citation id="d880e709" publication-type="other">
Caballero, R. J., &amp; Hammour, M. L. (1994). The cleansing effect of recessions. American Economic
Review, 84, 1350-1368.</mixed-citation>
         </ref>
         <ref id="d880e719a1310">
            <mixed-citation id="d880e723" publication-type="other">
Caballero, R. J., &amp; Hammour, M. L. (2005). The cost of recessions revisited: A reverse-liquidationist
view. Review of Economic Studies, 72(2), 313-341.</mixed-citation>
         </ref>
         <ref id="d880e733a1310">
            <mixed-citation id="d880e737" publication-type="other">
Calvo, G., Izquierdo, A., &amp; Talvi, E. (2006). Sudden stops and phoenix miracles in emerging markets.
American Economic Review Papers and Proceedings, 96(2), 405-410.</mixed-citation>
         </ref>
         <ref id="d880e747a1310">
            <mixed-citation id="d880e751" publication-type="other">
Calvo, G., &amp; Talvi, E. (2005). Sudden stop, financial factors and economic collapse in Latin America:
Learning from Argentina and Chile, (NBER Working Paper 11153). Cambridge, MA: National
Bureau of Economic Research.</mixed-citation>
         </ref>
         <ref id="d880e765a1310">
            <mixed-citation id="d880e769" publication-type="other">
Caprio, G., &amp; Klingebiel, D. (2003). Episodes of systemic and borderline financial crises. Mimeo, World
Bank, Washington, DC.</mixed-citation>
         </ref>
         <ref id="d880e779a1310">
            <mixed-citation id="d880e783" publication-type="other">
Caselli, F. (2005). Accounting for cross-country income differences. In P. Aghion &amp; S. Durlauf (Eds.),
Handbook of Economic Growth (Vol. 1, Part 1, pp. 679-741). Amsterdam: North Holland.</mixed-citation>
         </ref>
         <ref id="d880e793a1310">
            <mixed-citation id="d880e797" publication-type="other">
Cerra, V., &amp; Saxena, S. C. (2008). Growth dynamics: The myth of economic recovery. American
Economic Review, 95(1), 439-457.</mixed-citation>
         </ref>
         <ref id="d880e807a1310">
            <mixed-citation id="d880e811" publication-type="other">
Chari, V. V., Kehoe, P., &amp; McGrattan, E. (2007). Business cycle accouting. Econometrica, 75(3), 781-
836.</mixed-citation>
         </ref>
         <ref id="d880e821a1310">
            <mixed-citation id="d880e825" publication-type="other">
Daude, C, &amp; Fernández-Arias, E. (2010). On the role of productivity and factor accumulation for
economic development in Latin America and the Caribbean, (IDB Working Paper 155).
Washington, DC: Inter-American Development Bank.</mixed-citation>
         </ref>
         <ref id="d880e838a1310">
            <mixed-citation id="d880e842" publication-type="other">
Demirgiiç-Kunt, A., &amp; Detragiache, E. (2005). Cross-country empirical studies of systemic bank distress:
A survey, (IMF Working Papers WP05/96). Washington, DC: International Monetary Fund.</mixed-citation>
         </ref>
         <ref id="d880e853a1310">
            <mixed-citation id="d880e857" publication-type="other">
Detraigiache, E., &amp; Spilimbergo, A. (2001). Crises and liquidity: Evidence and interpretation, (IMF
Working Paper WP/01/2). Washington, DC: International Monetary Fund.</mixed-citation>
         </ref>
         <ref id="d880e867a1310">
            <mixed-citation id="d880e871" publication-type="other">
Drazen, A., &amp; Easterly, W. (2001). Do crises induce reform? Simple empirical tests of conventional
wisdom. Economics and Politics, 13(2), 129-157.</mixed-citation>
         </ref>
         <ref id="d880e881a1310">
            <mixed-citation id="d880e885" publication-type="other">
Easterly, W., &amp; Levine, R. (2001). It's not factor accumulation: Stylized facts and growth models. World
Bank Economic Review, 15(2), 177-219.</mixed-citation>
         </ref>
         <ref id="d880e895a1310">
            <mixed-citation id="d880e899" publication-type="other">
Em-Dat. (2009). The International Disaster Database, Centre for Research on Epidemiology of
Disasters -CRED, www.em-dat.net Accessed October 2009.</mixed-citation>
         </ref>
         <ref id="d880e909a1310">
            <mixed-citation id="d880e913" publication-type="other">
Fernández-Arias, E., Panizza, U., &amp; Stein, E. (2002). Trade agreements, exchange rate disagreements.
Mimeo, Inter-American Development Bank, Washington, DC.</mixed-citation>
         </ref>
         <ref id="d880e923a1310">
            <mixed-citation id="d880e927" publication-type="other">
Grossman, G. M., &amp; Helpman, E. (1991). Innovation and growth in the global economy. Cambridge,
Massachusetts: MIT Press.</mixed-citation>
         </ref>
         <ref id="d880e938a1310">
            <mixed-citation id="d880e942" publication-type="other">
Hall, J., &amp; Jones, C. (1999). Why do some countries produce so much more output per worker than
others? Quarterly Journal of Economics, 114(1), 83-116.</mixed-citation>
         </ref>
         <ref id="d880e952a1310">
            <mixed-citation id="d880e956" publication-type="other">
Hausmann, R., Pritchett, L., &amp; Rodrik, D. (2005). Growth accelerations. Journal of Economic Growth,
10(4), 303-329.</mixed-citation>
         </ref>
         <ref id="d880e966a1310">
            <mixed-citation id="d880e970" publication-type="other">
Hausmann, R., Rodriguez, F., &amp; Wagner, R. (2006). Growth collapses, (Center for International
Development Working Paper 136), October. Cambridge, MA: Harvard University.</mixed-citation>
         </ref>
         <ref id="d880e980a1310">
            <mixed-citation id="d880e984" publication-type="other">
Hong, K., &amp; Tornell, A. (2005). Recovery after a currency crisis: some stylized facts. Journal of
Development Economics, 76(1), 71-96.</mixed-citation>
         </ref>
         <ref id="d880e994a1310">
            <mixed-citation id="d880e998" publication-type="other">
Im, K., Pesaran, H., &amp; Shin, Y. (2003). Testing for unit roots in heterogeneous panels. Journal of
Econometrics, 115(1), 53-74.</mixed-citation>
         </ref>
         <ref id="d880e1008a1310">
            <mixed-citation id="d880e1012" publication-type="other">
Jones, B. F., &amp; Olken, B. A. (2008). The anatomy of start-stop growth. Review of Economics and
Statistics, 90(3), 582-587.</mixed-citation>
         </ref>
         <ref id="d880e1023a1310">
            <mixed-citation id="d880e1027" publication-type="other">
Kaminsky, G. L., &amp; Reinhart, C. M. (1999). The twin crises: The causes of banking and balance-of-
payments problems. American Economic Review, 89(3), 473-500.</mixed-citation>
         </ref>
         <ref id="d880e1037a1310">
            <mixed-citation id="d880e1041" publication-type="other">
Kehoe, T. J., &amp; Prescott, E. C. (Eds.). (2007). Great depressions of the twentieth century. Minneapolis:
Federal Reserve Bank of Minneapolis.</mixed-citation>
         </ref>
         <ref id="d880e1051a1310">
            <mixed-citation id="d880e1055" publication-type="other">
Laeven, L., &amp; Valencia, F. (2008). Systemic banking crises: A new database, (IMF Working Paper 08/
250). Washington, DC: International Monetary Fund.</mixed-citation>
         </ref>
         <ref id="d880e1065a1310">
            <mixed-citation id="d880e1069" publication-type="other">
Levin, A., Lin, C.-F., &amp; Chu, C.-S. J. (2002). Unit root tests in panel data: Asymptotic and finite sample
properties. Journal of Econometrics, 705(1), 1-24.</mixed-citation>
         </ref>
         <ref id="d880e1079a1310">
            <mixed-citation id="d880e1083" publication-type="other">
Lucas, R. E. (1987). Models of business cycles. New York: Basil Blackwell.</mixed-citation>
         </ref>
         <ref id="d880e1090a1310">
            <mixed-citation id="d880e1094" publication-type="other">
Maddala, G. S., &amp; Wu, S. (1999). A comparative study of unit root tests with panel data and a new simple
test. Oxford Bulletin of Economics and Statistics, 67(4), 631-652.</mixed-citation>
         </ref>
         <ref id="d880e1105a1310">
            <mixed-citation id="d880e1109" publication-type="other">
Manasse, P., &amp; Roubini, N. (2005). 'Rules of thumb' for Sovereign debt crises, (IMF Working Papers
WP/05/42). Washington, DC: International Monetary Fund.</mixed-citation>
         </ref>
         <ref id="d880e1119a1310">
            <mixed-citation id="d880e1123" publication-type="other">
Martin, P., &amp; Rogers, C. A. (1997). Stabilization policy, learning-by-doing, and economic growth. Oxford
Economic Papers, 49(2), 152-166.</mixed-citation>
         </ref>
         <ref id="d880e1133a1310">
            <mixed-citation id="d880e1137" publication-type="other">
Pallage, S., &amp; Robe, M. A. (2003). On the welfare cost of economic fluctuations in developing countries.
International Economic Review, 44(2), 677-698.</mixed-citation>
         </ref>
         <ref id="d880e1147a1310">
            <mixed-citation id="d880e1151" publication-type="other">
Parente, S., &amp; Prescott, E. C. (2000). Barrier to riches. Cambridge, Massachusetts: MIT Press.</mixed-citation>
         </ref>
         <ref id="d880e1158a1310">
            <mixed-citation id="d880e1162" publication-type="other">
Parente, S., &amp; Prescott, E. C. (2005). A unified theory of the evolution of international income levels. In
P. Aghion &amp; S. Durlauf (Eds.), Handbook of Economic Growth (Chap. 21, pp. 1371-1141).
Rotterdam: North-Holland.</mixed-citation>
         </ref>
         <ref id="d880e1175a1310">
            <mixed-citation id="d880e1179" publication-type="other">
Pesaran, H. (2003). A simple panel unit root test in the presence of cross section dependence, (Cambridge
Working Papers in Economics 0346), Faculty of Economics (DAE), University of Cambridge.</mixed-citation>
         </ref>
         <ref id="d880e1190a1310">
            <mixed-citation id="d880e1194" publication-type="other">
Pritchett, L. (1997). Divergence, big time. Journal of Economic Perspectives, 77(3), 3-17.</mixed-citation>
         </ref>
         <ref id="d880e1201a1310">
            <mixed-citation id="d880e1205" publication-type="other">
Pritchett, L. (2000). Understanding patterns of economic growth: searching for hills among Plateaus,
Mountains, and Plains. World Bank Economic Review, 14(2), 221-250.</mixed-citation>
         </ref>
         <ref id="d880e1215a1310">
            <mixed-citation id="d880e1219" publication-type="other">
Ranciere, R., Tornell, A., &amp; Westermann, F. (2008). Systemic crises and growth. Quarterly Journal of
Economics, 723(1), 359^06.</mixed-citation>
         </ref>
         <ref id="d880e1229a1310">
            <mixed-citation id="d880e1233" publication-type="other">
Reinhart, C. M., Rogoff, K. S., &amp; Savastano, M. A. (2003). Debt intolerance. Brookings Papers on
Economic Activity, 1, 1-62.</mixed-citation>
         </ref>
         <ref id="d880e1243a1310">
            <mixed-citation id="d880e1247" publication-type="other">
Restuccia, D., &amp; Rogerson, R. (2008). Policy distortions and aggregate productivity with heterogeneous
plants. Review of Economic Dynamics, 77(4), 707-720.</mixed-citation>
         </ref>
         <ref id="d880e1257a1310">
            <mixed-citation id="d880e1261" publication-type="other">
Romer, P. (1990). Endogenous technological change. Journal of Political Economic, 98(5), 71-102.</mixed-citation>
         </ref>
         <ref id="d880e1269a1310">
            <mixed-citation id="d880e1273" publication-type="other">
Tommasi, M., &amp; Velsco, A. (1996). Where are we in the political economy of reform? Journal of Policy
Reform, 7(2), 187-238.</mixed-citation>
         </ref>
         <ref id="d880e1283a1310">
            <mixed-citation id="d880e1287" publication-type="other">
Uhlig, H., &amp; Ravn, M. (2002). On adjusting the HP-filter for the frequency of observations. Review of
Economics and Statistics, 84(2), 371-376.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
