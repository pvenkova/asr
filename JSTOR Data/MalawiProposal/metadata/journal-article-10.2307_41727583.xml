<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">procbiolscie</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100837</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Proceedings: Biological Sciences</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>The Royal Society</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">09628452</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">41727583</article-id>
         <title-group>
            <article-title>Rain reverses diel activity rhythms in an estuarine teleost</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Nicholas L.</given-names>
                  <surname>Payne</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Dylan E.</given-names>
                  <surname>van der Meulen</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Ruan</given-names>
                  <surname>Gannon</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jayson M.</given-names>
                  <surname>Semmens</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Iain M.</given-names>
                  <surname>Suthers</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Charles A.</given-names>
                  <surname>Gray</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Matthew D.</given-names>
                  <surname>Taylor</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>7</day>
            <month>1</month>
            <year>2013</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">280</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1750</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40081212</issue-id>
         <fpage>1</fpage>
         <lpage>7</lpage>
         <permissions>
            <copyright-statement>Copyright © 2012 The Royal Society</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:title="an external site"
                   xlink:href="http://dx.doi.org/10.1098/rspb.2012.2363"/>
         <abstract>
            <p>Activity rhythms are ubiquitous in nature, and generally synchronized with the day-night cycle. Several taxa have been shown to switch between nocturnal and diurnal activity in response to environmental variability, and these relatively uncommon switches provide a basis for greater understanding of the mechanisms and adaptive significance of circadian (approx. 24 h) rhythms. Plasticity of activity rhythms has been identified in association with a variety of factors, from changes in predation pressure to an altered nutritional or social status. Here, we report a switch in activity rhythm that is associated with rainfall. Outside periods of rain, the estuarine-associated teleost Acanthopagrus australis was most active and in shallower depths during the day, but this activity and depth pattern was reversed in the days following rain, with diurnality restored as estuarine conductivity and turbidity levels returned to pre-rain levels. Although representing the first example of a rain-induced reversal of activity rhythm in an aquatic animal of which we are aware, our results are consistent with established models on the trade-offs between predation risk and foraging efficiency.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d930e255a1310">
            <label>1</label>
            <mixed-citation id="d930e262" publication-type="other">
Sharma VK. 2003 Adaptive significance of circadian
clocks. Chronobiol. Int. 20, 901-919. (doi:10.1081/
cbi-120026099)</mixed-citation>
         </ref>
         <ref id="d930e275a1310">
            <label>2</label>
            <mixed-citation id="d930e282" publication-type="other">
Hut RA, Beersma DGM. 2011 Evolution of time-
keeping mechanisms: early emergence and
adaptation to photoperiod. Phil. Trans. R. Soc. B
366, 2141-2154. (doi:10.1098/rstb.2010.0409)</mixed-citation>
         </ref>
         <ref id="d930e298a1310">
            <label>3</label>
            <mixed-citation id="d930e305" publication-type="other">
Yerushalmi S, Green RM. 2009 Evidence for the
adaptive significance of circadian rhythms. Ecol.
Lett. 12, 970-981. (doi:10.1111/j.1461-0248.2009.
01343.x)</mixed-citation>
         </ref>
         <ref id="d930e321a1310">
            <label>4</label>
            <mixed-citation id="d930e328" publication-type="other">
Ouyang Y, Andersson CR, Kondo T, Golden S,
Johnson CH. 1998 Resonating circadian clocks
enhance fitness in cyanobacteria. Proc. Natl
Acad. Sci. USA 95, 8660-8664. (doi:10.1073/pnas.
95.15.8660)</mixed-citation>
         </ref>
         <ref id="d930e348a1310">
            <label>5</label>
            <mixed-citation id="d930e355" publication-type="other">
Woelfle MA, Yan OY, Phanvijhitsiri K, Johnson CH.
2004 The adaptive value of circadian clocks: an
experimental assessment in cyanobacteria.
Curr. Biol. 14, 1481-1486. (doi:10.1016/j.cub.2004.
08.023)</mixed-citation>
         </ref>
         <ref id="d930e374a1310">
            <label>6</label>
            <mixed-citation id="d930e383" publication-type="other">
Emerson KJ, Bradshaw WE, Holzapfel CM. 2008
Concordance of the circadian clock with the
environment is necessary to maximize fitness in
natural populations. Evolution 62, 979-983.
(doi:10.1111/j.1558-5646.2008.00324.X)</mixed-citation>
         </ref>
         <ref id="d930e402a1310">
            <label>7</label>
            <mixed-citation id="d930e409" publication-type="other">
Pittendrigh CS, Minis DH. 1972 Circadian systems:
longevity as a function of circadian resonance in
Drosophila melanogaster. Proc. Natl Acad. Sci. USA
69, 1537-1539. (doi:10.1073/pnas.69.6.1537)</mixed-citation>
         </ref>
         <ref id="d930e425a1310">
            <label>8</label>
            <mixed-citation id="d930e432" publication-type="other">
Hurd MW, Ralph MR. 1998 The significance of
circadian organization for longevity in the golden
hamster. J. Biol. Rhythms 13, 430-436. (doi:10.
1177/074873098129000255)</mixed-citation>
         </ref>
         <ref id="d930e448a1310">
            <label>9</label>
            <mixed-citation id="d930e455" publication-type="other">
Gattermann R et al. 2008 Golden hamsters are
nocturnal in captivity but diurnal in nature. Biol.
Lett. 4, 253-255. (doi:10.1098/rsbl.2008.0066)</mixed-citation>
         </ref>
         <ref id="d930e468a1310">
            <label>10</label>
            <mixed-citation id="d930e475" publication-type="other">
Levy 0, Dayan T, Kronfeld-Schor N. 2007 The
relationship between the golden spiny mouse
circadian system and its diurnal activity: an
experimental field enclosures and laboratory study.
Chronobiol. Int. 24, 599-613. (doi:10.1080/
07420520701534640)</mixed-citation>
         </ref>
         <ref id="d930e499a1310">
            <label>11</label>
            <mixed-citation id="d930e506" publication-type="other">
Bloch G, Robinson GE. 2001 Chronobiology: reversal
of honeybee behavioural rhythms. Nature 410,
1048. (doi:10.1038/35074183)</mixed-citation>
         </ref>
         <ref id="d930e519a1310">
            <label>12</label>
            <mixed-citation id="d930e526" publication-type="other">
Fraser NHC, Metcalfe NB, Thorpe JE. 1993
Temperature-dependent switch between diurnal
and nocturnal foraging in salmon. Proc. R. Soc.
Lond. B 252, 135-139. (doi:10.1098/rspb.1993.
0057)</mixed-citation>
         </ref>
         <ref id="d930e545a1310">
            <label>13</label>
            <mixed-citation id="d930e552" publication-type="other">
Metcalfe NB, Steele Gl. 2001 Changing nutritional
status causes a shift in the balance of nocturnal to
diurnal activity in European Minnows. Funct. Ecol.
15, 304-309. (doi:10.1046/j.1365-2435.2001.
00527.x)</mixed-citation>
         </ref>
         <ref id="d930e571a1310">
            <label>14</label>
            <mixed-citation id="d930e578" publication-type="other">
Fox RJ, Bellwood DR. 2011 Unconstrained by
the clock? Plasticity of diel activity rhythm in a
tropical reef fish, Siganus lineatus. Funct. Ecol.
25, 1096-1105. (doi:10.1111/j.1365-2435.2011.
01874.x)</mixed-citation>
         </ref>
         <ref id="d930e597a1310">
            <label>15</label>
            <mixed-citation id="d930e604" publication-type="other">
Allan RP, Soden BJ. 2008 Atmospheric warming and
the amplification of precipitation extremes. Science
321, 1481-1484. (doi:10.1126/science.1160787)</mixed-citation>
         </ref>
         <ref id="d930e617a1310">
            <label>16</label>
            <mixed-citation id="d930e624" publication-type="other">
Cooke SJ et al. 2004 Biotelemetry: a mechanistic
approach to ecology. Trends Ecol. Evol. 19,
334-343. (doi:10.1016/j.tree.2004.04.003)</mixed-citation>
         </ref>
         <ref id="d930e638a1310">
            <label>17</label>
            <mixed-citation id="d930e647" publication-type="other">
Heupel MR, Semmens JM, Hobday AJ. 2006
Automated acoustic tracking of aquatic animals:
scales, design and deployment of listening station
arrays. Mar. Freshw. Res. 57, 1-13. (doi:10.1071/
MF05091)</mixed-citation>
         </ref>
         <ref id="d930e666a1310">
            <label>18</label>
            <mixed-citation id="d930e673" publication-type="other">
Halsey LG, Shepard ELC, Wilson RP. 2011 Assessing
the development and application of the
accelerometry technique for estimating energy
expenditure. Comp. Biochem. Physiol. A Mol. Integr.
Physiol. 158, 305-314. (doi:10.1016/j.cbpa.2010.
09.002)</mixed-citation>
         </ref>
         <ref id="d930e696a1310">
            <label>19</label>
            <mixed-citation id="d930e703" publication-type="other">
How JR, de Lestang S. 2012 Acoustic tracking: issues
affecting design, analysis and interpretation of data
from movement studies. Mar. Freshw. Res. 63,
312-324. (doi:10.1071/mf11194)</mixed-citation>
         </ref>
         <ref id="d930e719a1310">
            <label>20</label>
            <mixed-citation id="d930e726" publication-type="other">
Walsh CT, Reinfelds IV, Gray CA, West RJ, van der
Meulen DE, Craig JR. 2012 Seasonal residency and
movement patterns of two co-occurring
catadromous percichthyids within a south-eastern
Australian river. Ecol. Freshw. Fish. 21, 145-159.
(doi:10.1111/j.1600-0633.2011.00534.x)</mixed-citation>
         </ref>
         <ref id="d930e749a1310">
            <label>21</label>
            <mixed-citation id="d930e756" publication-type="other">
Payne NL, Gillanders BM, Seymour RS, Webber DM,
Snelling EP, Semmens JM. 2011 Accelerometry
estimates field metabolic rate in giant Australian
cuttlefish Sepia apama during breeding. J. Anim.
Ecol. 80, 422-430. (doi:10.1111/j.1365-2656.2010.
01758.x)</mixed-citation>
         </ref>
         <ref id="d930e779a1310">
            <label>22</label>
            <mixed-citation id="d930e786" publication-type="other">
Butcher PA, Broadhurst MK, Orchard BA, Ellis MT.
2010 Using biotelemetry to assess the mortality and
behaviour of yellowfin bream (Acanthopagrus
australis) released with ingested hooks. ICES
J. Mar. Sci. 67, 1175-1184. (doi:10.1093/icesjms/
fsq028)</mixed-citation>
         </ref>
         <ref id="d930e810a1310">
            <label>23</label>
            <mixed-citation id="d930e817" publication-type="other">
Aitken LS, West SG. 1991 Multiple regression: testing
and interpreting interactions. Newbury Park, CA:
Sage Publications Inc.</mixed-citation>
         </ref>
         <ref id="d930e830a1310">
            <label>24</label>
            <mixed-citation id="d930e837" publication-type="other">
Burnham KP, Anderson DR. 2002 Model selection
and multimodel inference: a practical information-
theoretic approach, 2nd edn. New York, NY:
Springer.</mixed-citation>
         </ref>
         <ref id="d930e853a1310">
            <label>25</label>
            <mixed-citation id="d930e860" publication-type="other">
Fenn MGP, Macdonald DW. 1995 Use of middens by
red foxes: risk reverses rhythms of rats. J. Mammal
76, 130-136. (doi:10.2307/1382321)</mixed-citation>
         </ref>
         <ref id="d930e873a1310">
            <label>26</label>
            <mixed-citation id="d930e880" publication-type="other">
Alanara A, Burns MD, Metcalfe NB. 2001
Intraspecific resource partitioning in brown trout:
the temporal distribution of foraging is determined
by social rank. J. Anim. Ecol. 70, 980-986. (doi:10.
1046/j.0021-8790.2001.00550.x)</mixed-citation>
         </ref>
         <ref id="d930e899a1310">
            <label>27</label>
            <mixed-citation id="d930e906" publication-type="other">
Metcalfe NB, Fraser NHC, Bums MD. 1998 State-
dependent shifts between nocturnal and diurnal
activity in salmon. Proc. R. Soe. Lond. B 265, 1503-
1507. (doi:10.1098/rspb.1998.0464)</mixed-citation>
         </ref>
         <ref id="d930e922a1310">
            <label>28</label>
            <mixed-citation id="d930e929" publication-type="other">
Reebs SG. 2002 Plasticity of diel and circadian
activity rhythms in fishes. Rev. Fish. Biol. Fish. 12,
349-371. (doi:10.1023/a:1025371804611)</mixed-citation>
         </ref>
         <ref id="d930e943a1310">
            <label>29</label>
            <mixed-citation id="d930e950" publication-type="other">
Sakabe R, Lyle JM. 2010 The influence of tidal
cycles and freshwater inflow on the distribution and
movement of an estuarine resident fish
Acanthopagrus butcheri. J. Fish. Biol. 77, 643-660.
(doi:10.1111/j.1095-8649.2010.02703.x)</mixed-citation>
         </ref>
         <ref id="d930e969a1310">
            <label>30</label>
            <mixed-citation id="d930e976" publication-type="other">
Childs AR et al. 2008 Do environmental factors
influence the movement of estuarine fish? A case
study using acoustic telemetry. Estuar. Coast. Shelf
Sci. 78, 227-236. (doi:10.1016/j.ecss.2007.12.003)</mixed-citation>
         </ref>
         <ref id="d930e992a1310">
            <label>31</label>
            <mixed-citation id="d930e999" publication-type="other">
Taylor BR, Macinnis C, Floyd TA. 2010 Influence of
rainfall and beaver dams on upstream movement of
spawning Atlantic salmon in a restored brook in
Nova Scotia, Canada. River Res. Appl. 26, 183-193.
(doi:10.1002/rra.1252)</mixed-citation>
         </ref>
         <ref id="d930e1018a1310">
            <label>32</label>
            <mixed-citation id="d930e1025" publication-type="other">
Hindell JS, Jenkins GP, Womersley B. 2008 Habitat
utilisation and movement of black bream
Acanthopagrus butcheri (Sparidae) in an Australian
estuary. Mar. Ecol. Prog. Ser. 366, 219-229.
(doi:10.3354/meps07519)</mixed-citation>
         </ref>
         <ref id="d930e1044a1310">
            <label>33</label>
            <mixed-citation id="d930e1051" publication-type="other">
Railsback SF, Harvey BC, Hayse JW, LaGory KE. 2005
Tests of theory for diel variation in salmonid feeding
activity and habitat use. Ecology 86, 947-959.
(doi:10.1890/04-1178)</mixed-citation>
         </ref>
         <ref id="d930e1067a1310">
            <label>34</label>
            <mixed-citation id="d930e1074" publication-type="other">
Metcalfe NB, Fraser NHC, Bums MD. 1999 Food
availability and the nocturnal vs. diurnal foraging
trade-off in juvenile salmon. J. Anim. Ecol. 68,
371-381. (doi:10.1046/j.1365-2656.1999.00289.x)</mixed-citation>
         </ref>
         <ref id="d930e1091a1310">
            <label>35</label>
            <mixed-citation id="d930e1098" publication-type="other">
Gilliam JF, Fraser DF. 1987 Habitat selection under
prédation hazard: test of a model with foraging
minnows. Ecology 68, 1856-1862. (doi:10.2307/
1939877)</mixed-citation>
         </ref>
         <ref id="d930e1114a1310">
            <label>36</label>
            <mixed-citation id="d930e1121" publication-type="other">
Ochwada F, Loneragan NR, Gray CA, Suthers IM,
Taylor MD. 2009 Complexity affects habitat
preference and prédation mortality in postlarval
Penaeus plebejus: implications for stock
enhancement. Mar. Ecol. Prog. Ser. 380, 161-171.
(doi:10.3354/meps07936)</mixed-citation>
         </ref>
         <ref id="d930e1144a1310">
            <label>37</label>
            <mixed-citation id="d930e1153" publication-type="other">
Ochwada-Doyle F, Gray CA, Loneragan NR, Taylor
MD. 2010 Using experimental ecology to
understand stock enhancement: comparisons of
habitat-related prédation on wild and hatchery-
reared Penaeus plebejus Hess. J. Exp. Mar. Biol. Ecol.
390, 65-71. (doi:10.1016/j.jembe.2010.04.003)</mixed-citation>
         </ref>
         <ref id="d930e1176a1310">
            <label>38</label>
            <mixed-citation id="d930e1183" publication-type="other">
Taylor MD, Fairfax AV, Suthers IM. In press. The race
for space: using acoustic telemetry to understand
density-dependent emigration and habitat selection
in a released predatory fish. Rev. Fish. Sci.</mixed-citation>
         </ref>
         <ref id="d930e1199a1310">
            <label>39</label>
            <mixed-citation id="d930e1206" publication-type="other">
Dalla Via J, Villani P, Gasteiger E, Niederstatter H.
1998 Oxygen consumption in sea bass fingerling
Dicentrarchus labrax exposed to acute salinity and
temperature changes: metabolic basis for maximum
stocking density estimations. Aquaculture 169,
303-313. (doi:10.1016/s0044-8486(98)00375-5)</mixed-citation>
         </ref>
         <ref id="d930e1229a1310">
            <label>40</label>
            <mixed-citation id="d930e1236" publication-type="other">
Sangiao-Alvarellos S, Arjona FJ, del Rio MPM,
Miguez JM, Mancera JM, Soengas JL. 2005 Time
course of osmoregulatory and metabolic changes
during osmotic acclimation in Sparus auratus. J. Exp.
Biol. 208, 4291-4304. (doi:10.1242/jeb.01900)</mixed-citation>
         </ref>
         <ref id="d930e1256a1310">
            <label>41</label>
            <mixed-citation id="d930e1263" publication-type="other">
Taylor MD, Laffan SD, Fielder DS, Suthers IM. 2006
Key habitat and home range of mulloway
Argyrosomus japonicus in a south-east Australian
estuary: finding the estuarine niche to optimise
stocking. Mar. Ecol. Prog. Ser. 328, 237-247.
(doi:10.3354/meps328237)</mixed-citation>
         </ref>
         <ref id="d930e1286a1310">
            <label>42</label>
            <mixed-citation id="d930e1293" publication-type="other">
Taylor MD, Ko A. 2011 Monitoring acoustically
tagged king prawns Penaeus (Melicertus) plebejus in
an estuarine lagoon. Mar. Biol. 158, 835-844.
(doi:10.1007/s00227-010-1610-6)</mixed-citation>
         </ref>
         <ref id="d930e1309a1310">
            <label>43</label>
            <mixed-citation id="d930e1316" publication-type="other">
Wassenberg TJ, Hill BJ. 1994 Laboratory study of the
effect of light on the emergence behaviour of 8
species of commercially important adult penaeid
prawns. Mar. Freshw. Res. 45, 43-50. (doi:10.1071/
MF9940043)</mixed-citation>
         </ref>
         <ref id="d930e1335a1310">
            <label>44</label>
            <mixed-citation id="d930e1342" publication-type="other">
Gray SM, Sabbah S, Hawryshyn CW. 2011
Experimentally increased turbidity auses
behavioural shifts in Lake Malawi cichlids. Ecol.
Freshw. Fish. 20, 529-536. (doi:10.1111/j.1600-
0633.2011.00501.x)</mixed-citation>
         </ref>
         <ref id="d930e1361a1310">
            <label>45</label>
            <mixed-citation id="d930e1368" publication-type="other">
Huenemann TW, Dibble ED, Fleming JP. 2012
Influence of turbidity on the foraging of largemouth
bass. Trans. Am. Fish Soc. 141, 107-111. (doi:10.
1080/00028487.2011.651554)</mixed-citation>
         </ref>
         <ref id="d930e1384a1310">
            <label>46</label>
            <mixed-citation id="d930e1391" publication-type="other">
Mrosovsky N. 1999 Masking: history, definitions,
and measurement. Chronobiol. Int. 16, 415-429.
(doi:10.3109/07420529908998717)</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
