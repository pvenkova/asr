<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">publchoi</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000024</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Public Choice</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Springer</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00485829</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15737101</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">42003155</article-id>
         <title-group>
            <article-title>Elections and the structure of taxation in developing countries</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Helene</given-names>
                  <surname>Ehrhart</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>7</month>
            <year>2013</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">156</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1/2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40094362</issue-id>
         <fpage>195</fpage>
         <lpage>211</lpage>
         <permissions>
            <copyright-statement>© 2013 Springer Science+Business Media Dordrecht</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:href="http://dx.doi.org/10.1007/s11127-011-9894-8"
                   xlink:title="an external site"/>
         <abstract>
            <p>This article analyses the impact of the electoral calendar on the composition of tax revenue (direct versus indirect taxes). It thus represents an extension of traditional political budget-cycle analyses assessing the impact of elections on overall revenue. We appeal to the opportunistic political budget model of Drazen and Eslava (2010) to predict the relationship between taxation structure and elections. Panel data from 56 developing countries over the 1980-2006 period reveals a clear pattern of electorally-related policy interventions. Taking the potential endogeneity of election timing into account, we find robust evidence of lower indirect taxes being applied by incumbent governments in the period just prior to an election. Indirect tax revenue in election years is estimated to be 0.3 GDP percentage points lower than in other years, corresponding to a fall of about 3.4% of the average figure in the sample countries, while there is no such relationship with direct tax revenue.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d270e180a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d270e187" publication-type="other">
Andrikopoulos et al. (2004)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d270e193" publication-type="other">
Katsimi and Sarantides (2011)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d270e199" publication-type="other">
Mikesell (1978)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d270e206" publication-type="other">
Ashworth and Heyndels (2002)</mixed-citation>
            </p>
         </fn>
         <fn id="d270e213a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d270e220" publication-type="other">
Drazen and Eslava (2010),</mixed-citation>
            </p>
         </fn>
         <fn id="d270e227a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d270e234" publication-type="other">
Brender and Drazen (2005).</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d270e250a1310">
            <mixed-citation id="d270e254" publication-type="other">
Adam, C. S., Bevan, D. L., &amp; Chambas, G. (2001). Exchange rate regimes and revenue performance in sub-
Saharan Africa. Journal of Development Economics, 64(1), 173-213.</mixed-citation>
         </ref>
         <ref id="d270e264a1310">
            <mixed-citation id="d270e268" publication-type="other">
Alesina, A. (1987). Macroeconomic policy in a two-party system as a repeated game. Quarterly Journal of
Economics, 102, 651-678.</mixed-citation>
         </ref>
         <ref id="d270e278a1310">
            <mixed-citation id="d270e282" publication-type="other">
Alesina, A., &amp; Roubini, N. (1992). Political cycles in OECD economies. Review of Economic Studies, 59,
663-688.</mixed-citation>
         </ref>
         <ref id="d270e292a1310">
            <mixed-citation id="d270e296" publication-type="other">
Alesina, A., Roubini, N., &amp; Cohen, G. (1997). Political cycles and the macroeconomy. Cambridge: MIT
Press.</mixed-citation>
         </ref>
         <ref id="d270e307a1310">
            <mixed-citation id="d270e311" publication-type="other">
Andrikopoulos, A., Loizides, I., &amp; Prodromidis, K. (2004). Fiscal policy and political business cycles in the
EU. European Journal of Political Economy, 20, 125-152.</mixed-citation>
         </ref>
         <ref id="d270e321a1310">
            <mixed-citation id="d270e325" publication-type="other">
Arellano, M., &amp; Bond, S. (1991). Some tests of specification for panel data: Monte Carlo evidence and an
application to employment equations. Review of Economic Studies, 58, 277-297.</mixed-citation>
         </ref>
         <ref id="d270e335a1310">
            <mixed-citation id="d270e339" publication-type="other">
Arellano, M., &amp; Bover, O. (1995). Another look at the instrumental variable estimation of error-components
models. Journal of Econometrics, 68, 29-51.</mixed-citation>
         </ref>
         <ref id="d270e349a1310">
            <mixed-citation id="d270e353" publication-type="other">
Ashworth, J., &amp; Heyndels, B. (2002). Tax structure turbulence in OECD countries. Public Choice, III, 347-
376.</mixed-citation>
         </ref>
         <ref id="d270e363a1310">
            <mixed-citation id="d270e367" publication-type="other">
Baunsgaard, T., &amp; Keen, M. (2010). Tax revenue and (or?) trade liberalization. Journal of Public Economics,
94, 563-577.</mixed-citation>
         </ref>
         <ref id="d270e377a1310">
            <mixed-citation id="d270e381" publication-type="other">
Beck, T., Clarke, G., Groff, A., Keefer, P., &amp; Walsh, P. (2001). News tools in comparative political economy:
the database of political institutions. World Bank Economic Review, 15, 165-176.</mixed-citation>
         </ref>
         <ref id="d270e392a1310">
            <mixed-citation id="d270e396" publication-type="other">
Bird, R., &amp; Zolt, E. (2005). Redistribution via taxation: the limited role of the personal income tax in devel-
oping countries. UCLA Law Review, 52(6), 1627-1696.</mixed-citation>
         </ref>
         <ref id="d270e406a1310">
            <mixed-citation id="d270e410" publication-type="other">
Blais, A., &amp; Nadeau, R. (1992). The electoral budget cycle. Public Choice, 74(A), 389-403.</mixed-citation>
         </ref>
         <ref id="d270e417a1310">
            <mixed-citation id="d270e421" publication-type="other">
Block, S. A. (2002). Political business cycles, democratization, and economic reform: the case of Africa.
Journal of Development Economics, 67, 205-228.</mixed-citation>
         </ref>
         <ref id="d270e431a1310">
            <mixed-citation id="d270e435" publication-type="other">
Blundell, R., &amp; Bond, S. (1998). Initial conditions and moment restrictions in dynamic panel data models.
Journal of Econometrics, 87, 115-143.</mixed-citation>
         </ref>
         <ref id="d270e445a1310">
            <mixed-citation id="d270e449" publication-type="other">
Brender, A., &amp; Drazen, A. (2005). Political budget cycles in new versus established democracies. Journal of
Monetary Economics, 52, 1271-1295.</mixed-citation>
         </ref>
         <ref id="d270e459a1310">
            <mixed-citation id="d270e463" publication-type="other">
Chauvet, L., &amp; Collier, P. (2009). Elections and economic policy in developing countries. Economic Policy,
24(59), 509-550.</mixed-citation>
         </ref>
         <ref id="d270e474a1310">
            <mixed-citation id="d270e478" publication-type="other">
Drazen, A. (2001). The political business cycles after 25 years. In B. Bernanke, K. Rogoff (Eds.), NBER
Macroeconomics Annual (pp. 75-117). Cambridge: MIT Press.</mixed-citation>
         </ref>
         <ref id="d270e488a1310">
            <mixed-citation id="d270e492" publication-type="other">
Drazen, A., &amp; Eslava, M. (2010). Electoral manipulation via voter-friendly spending: theory and evidence.
Journal of Development Economics, 92(1), 39-52.</mixed-citation>
         </ref>
         <ref id="d270e502a1310">
            <mixed-citation id="d270e506" publication-type="other">
Dreher, A., &amp; Schneider, F. (2010). Corruption and the shadow economy: an empirical analysis. Public
Choice, 144(1-2), 215-238.</mixed-citation>
         </ref>
         <ref id="d270e516a1310">
            <mixed-citation id="d270e520" publication-type="other">
Efthyvoulou, G. (2011). Political budget cycles in the European Union and the impact of political pressures.
Public Choice, doi:10.1007/s11127-011-9795-x.</mixed-citation>
         </ref>
         <ref id="d270e530a1310">
            <mixed-citation id="d270e534" publication-type="other">
Fall, E. (2007). Political budget cycle in Papua New Guinea. IMF Working Paper 07/219.</mixed-citation>
         </ref>
         <ref id="d270e541a1310">
            <mixed-citation id="d270e545" publication-type="other">
Ghura, D. (1998). Tax revenue in sub-Saharan Africa: Effects of economic policies and corruption. IMF
Working Paper 98/135.</mixed-citation>
         </ref>
         <ref id="d270e556a1310">
            <mixed-citation id="d270e560" publication-type="other">
Gonzalez, M. d. 1. A. (2002). Do changes in democracy affect the political budget cycle? Evidence from
Mexico. Review of Development Economics, 6(2), 204-224.</mixed-citation>
         </ref>
         <ref id="d270e570a1310">
            <mixed-citation id="d270e574" publication-type="other">
Grier, K. (2008). US presidential elections and real GDP growth, 1961-2004. Public Choice, 135, 337-352.</mixed-citation>
         </ref>
         <ref id="d270e581a1310">
            <mixed-citation id="d270e585" publication-type="other">
Hakes, D. R. (1988). Monetary policy and presidential elections: a nonpartisan political cycle. Public Choice,
57, 175-182.</mixed-citation>
         </ref>
         <ref id="d270e595a1310">
            <mixed-citation id="d270e599" publication-type="other">
Hibbs, D. A. (1977). Political parties and macroeconomic policy. American Political Science Review, 71,
1467-1487.</mixed-citation>
         </ref>
         <ref id="d270e609a1310">
            <mixed-citation id="d270e613" publication-type="other">
Katsimi, M., &amp; Sarantides, V. (2011). Do elections affect the composition of fiscal policy in developed,
established democracies? Public Choice, doi:10.1007/s11127-010-9749-8.</mixed-citation>
         </ref>
         <ref id="d270e623a1310">
            <mixed-citation id="d270e627" publication-type="other">
Keen, M., &amp; Lockwood, B. (2010). The value-added tax: its causes and consequences. Journal of Develop-
ment Economics, 92(2), 138-151.</mixed-citation>
         </ref>
         <ref id="d270e638a1310">
            <mixed-citation id="d270e642" publication-type="other">
Keen, M., &amp; Mansour, M. (2010). Revenue mobilisation in sub-Saharan Africa: challenges from globaliza-
tion: I—Trade reform. Development Policy Review, 28(5), 553-571.</mixed-citation>
         </ref>
         <ref id="d270e652a1310">
            <mixed-citation id="d270e656" publication-type="other">
Khattry, B., &amp; Rao, J. M. (2002). Fiscal faux pas?: An analysis of the revenue implications of trade liberal-
ization. World Development, 30(8), 1431-1444.</mixed-citation>
         </ref>
         <ref id="d270e666a1310">
            <mixed-citation id="d270e670" publication-type="other">
Khemani, S. (2004). Political cycles in a developing economy: effect of elections in the Indian states. Journal
of Development Economics, 73, 125-154.</mixed-citation>
         </ref>
         <ref id="d270e680a1310">
            <mixed-citation id="d270e684" publication-type="other">
Lindbeck, A. (1976). Stabilization policies in open economics with endogenous politicians. American Eco-
nomic Review, 66(2), 1-19.</mixed-citation>
         </ref>
         <ref id="d270e694a1310">
            <mixed-citation id="d270e698" publication-type="other">
Mikesell, J. L. (1978). Election periods and state tax policy cycles. Public Choice, 33(3), 99-106.</mixed-citation>
         </ref>
         <ref id="d270e705a1310">
            <mixed-citation id="d270e709" publication-type="other">
Nickell, S. (1981). Biases in dynamic models with fixed effects. Econometrica, 49, 1417-1426.</mixed-citation>
         </ref>
         <ref id="d270e717a1310">
            <mixed-citation id="d270e721" publication-type="other">
Nordhaus, W. (1975). The political business cycle. Review of Economic Studies, 42, 169-190.</mixed-citation>
         </ref>
         <ref id="d270e728a1310">
            <mixed-citation id="d270e732" publication-type="other">
Persson, T., &amp; Tabellini, G. (1990). Macroeconomic policy, credibility and politics. New York: Harwood
Academic.</mixed-citation>
         </ref>
         <ref id="d270e742a1310">
            <mixed-citation id="d270e746" publication-type="other">
Rogoff, K. (1990). Equilibrium political budget cycles. American Economic Review, 80(1), 21-36.</mixed-citation>
         </ref>
         <ref id="d270e753a1310">
            <mixed-citation id="d270e757" publication-type="other">
Rogoff, K., &amp; Sibert, A. (1988). Elections and macroeconomic policy cycles. Review of Economic Studies,
55(1), 1-16.</mixed-citation>
         </ref>
         <ref id="d270e767a1310">
            <mixed-citation id="d270e771" publication-type="other">
Schneider, F., &amp; Enste, D. H. (2000). Shadow economies: size, causes, and consequences. Journal of Eco-
nomic Literature, 38(1), 77-1 14.</mixed-citation>
         </ref>
         <ref id="d270e781a1310">
            <mixed-citation id="d270e785" publication-type="other">
Schuknecht, L. (2000). Fiscal policy cycles and public expenditure in developing countries. Public Choice,
102, 115-130.</mixed-citation>
         </ref>
         <ref id="d270e796a1310">
            <mixed-citation id="d270e800" publication-type="other">
Shi, M, &amp; Svensson, J. (2006). Political budget cycles: do they differ across countries and why? Journal of
Public Economics, 90, 1367-1389.</mixed-citation>
         </ref>
         <ref id="d270e810a1310">
            <mixed-citation id="d270e814" publication-type="other">
Shughart, W. F. II (1997). Taxing choice: the predatory politics of fiscal discrimination. Oakland: The Inde-
pendent Institute.</mixed-citation>
         </ref>
         <ref id="d270e824a1310">
            <mixed-citation id="d270e828" publication-type="other">
Vergne, C. (2009). Democracy, elections and allocation of public expenditures in developing countries. Eu-
ropean Journal of Political Economy, 25, 63-77.</mixed-citation>
         </ref>
         <ref id="d270e838a1310">
            <mixed-citation id="d270e842" publication-type="other">
Yoo, K.-R. (1998). Intervention analysis of electoral tax cycle: the case of Japan. Public Choice, 96, 241-258.</mixed-citation>
         </ref>
         <ref id="d270e849a1310">
            <mixed-citation id="d270e853" publication-type="other">
Windmeijer, F. (2005). A finite sample correction for the variance of linear efficient two-step GMM estima-
tors. Journal of Econometrics, 126, 25-51.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
