<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">procnatiacadscie</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100014</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Proceedings of the National Academy of Sciences of the United States of America</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>National Academy of Sciences</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00278424</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">23058554</article-id>
         <title-group>
            <article-title>Malaria antifolate resistance with contrasting Plasmodium falciparum dihydrofolate reductase (DHFR) polymorphisms in humans and Anopheles mosquitoes</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Sungano</given-names>
                  <surname>Mharakurwa</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Taida</given-names>
                  <surname>Kumwenda</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Mtawa A. P.</given-names>
                  <surname>Mkulama</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Mulenga</given-names>
                  <surname>Musapa</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Sandra</given-names>
                  <surname>Chishimba</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Clive J.</given-names>
                  <surname>Shiff</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>David J.</given-names>
                  <surname>Sullivan</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Philip E.</given-names>
                  <surname>Thuma</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Kun</given-names>
                  <surname>Liu</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Peter</given-names>
                  <surname>Agre</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>15</day>
            <month>11</month>
            <year>2011</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">108</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">46</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i23057354</issue-id>
         <fpage>18796</fpage>
         <lpage>18801</lpage>
         <permissions>
            <copyright-statement>copyright © 1993—2008 National Academy of Sciences of the United States of America</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/23058554"/>
         <abstract>
            <p>Surveillance for drug-resistant parasites in human blood is a major effort in malaria control. Here we report contrasting antifolate resistance polymorphisms in Plasmodium falciparum when parasites in human blood were compared with parasites in Anopheles vector mosquitoes from sleeping huts in rural Zambia. DNA encoding P. falciparum dihydrofolate reductase (EC 1.5.1.3) was amplified by PCR with allele-specific restriction enzyme digestions. Markedly prevalent pyrimethamine-resistant mutants were evident in human P. falciparum infections—S108N (&gt;90%), with N51I, C59R, and 108N+51I+59R triple mutants (30—80%). This resistance level may be from selection pressure due to decades of sulfadoxine/pyrimethamine use in the region. In contrast, cycloguanil-resistant mutants were detected in very low frequency in parasites from human blood samples—S108T (13%), with A16V and 108T+16V double mutants (~4%). Surprisingly, pyrimethamine-resistant mutants were of very low prevalence (2—12%) in the midguts of Anopheles arabiensis vector mosquitoes, but cycloguanil-resistant mutants were highly prevalent—S108T (90%), with A16V and the 108T+16V double mutant (49—57%). Structural analysis of the dihydrofolate reductase by in silico modeling revealed a key difference in the enzyme within the NADPH binding pocket, predicting the S108N enzyme to have reduced stability but the S108T enzyme to have increased stability. We conclude that P. falciparum can bear highly host-specific drug-resistant polymorphisms, most likely reflecting different selective pressures found in humans and mosquitoes. Thus, it may be useful to sample both human and mosquito vector infections to accurately ascertain the epidemiological status of drug-resistant alleles.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>[Bibliography]</title>
         <ref id="d916e293a1310">
            <label>1</label>
            <mixed-citation id="d916e300" publication-type="other">
Björkman A, Bhattarai A (2005) Public health impact of drug resistant Plasmodium
falciparum malaria. Acta Trop 94:163-169.</mixed-citation>
         </ref>
         <ref id="d916e310a1310">
            <label>2</label>
            <mixed-citation id="d916e317" publication-type="other">
Trape JF (2001) The public health impact of chloroquine resistance in Africa. Am J
Trop Med Hyg 64(1-2 Suppl):12-17.</mixed-citation>
         </ref>
         <ref id="d916e327a1310">
            <label>3</label>
            <mixed-citation id="d916e334" publication-type="other">
Crabb C (2003) Plasmodium falciparum outwits Malarone, protector of travellers. Bull
World Health Organ 81:382-383.</mixed-citation>
         </ref>
         <ref id="d916e344a1310">
            <label>4</label>
            <mixed-citation id="d916e351" publication-type="other">
Fivelman QL, Butcher GA, Adagu IS, Warhurst DC, Pasvol G (2002) Malarone treat-
ment failure and in vitro confirmation of resistance of Plasmodium falciparum isolate
from Lagos, Nigeria. Malar J 1:1.</mixed-citation>
         </ref>
         <ref id="d916e365a1310">
            <label>5</label>
            <mixed-citation id="d916e372" publication-type="other">
Noedl H, et al.; Artemisinin Resistance in Cambodia 1 (ARC1) Study Consortium (2008)
Evidence of artemisinin-resistant malaria in western Cambodia. N Engl J Med 359:
2619-2620.</mixed-citation>
         </ref>
         <ref id="d916e385a1310">
            <label>6</label>
            <mixed-citation id="d916e392" publication-type="other">
Wongsrichanalai C, Meshnick SR (2008) Declining artesunate-mefloquine efficacy
against falciparum malaria on the Cambodia-Thailand border. Emerg Infect Dis 14:
716-719.</mixed-citation>
         </ref>
         <ref id="d916e405a1310">
            <label>7</label>
            <mixed-citation id="d916e412" publication-type="other">
Noedl H, Socheat D, Satimai W (2009) Artemisinin-resistant malaria in Asia. N Engl J
Med 361:540-541.</mixed-citation>
         </ref>
         <ref id="d916e422a1310">
            <label>8</label>
            <mixed-citation id="d916e429" publication-type="other">
Laufer MK, Djimdé AA, Plowe CV (2007) Monitoring and deterring drug-resistant
malaria in the era of combination therapy. Am J Trop Med Hyg 77(6 Suppl):160-169.</mixed-citation>
         </ref>
         <ref id="d916e439a1310">
            <label>9</label>
            <mixed-citation id="d916e446" publication-type="other">
Djimdé AA, et al. (2004) Molecular diagnosis of resistance to antimalarial drugs
during epidemics and in war zones. J Infect Dis 190:853-855.</mixed-citation>
         </ref>
         <ref id="d916e456a1310">
            <label>10</label>
            <mixed-citation id="d916e463" publication-type="other">
Plowe CV (2003) Monitoring antimalarial drug resistance: Making the most of the
tools at hand. J Exp Biol 206:3745-3752.</mixed-citation>
         </ref>
         <ref id="d916e474a1310">
            <label>11</label>
            <mixed-citation id="d916e481" publication-type="other">
Plowe CV, et al. (1997) Mutations in Plasmodium falciparum dihydrofolate reductase
and dihydropteroate synthase and epidemiologic patterns of pyrimethamine-sulfa-
doxine use and resistance. J Infect Dis 176:1590-1596.</mixed-citation>
         </ref>
         <ref id="d916e494a1310">
            <label>12</label>
            <mixed-citation id="d916e501" publication-type="other">
Plowe CV, Djimde A, Bouare M, Doumbo O, Wellems TE (1995) Pyrimethamine and
proguanil resistance-conferring mutations in Plasmodium falciparum dihydrofolate
reductase: Polymerase chain reaction methods for surveillance in Africa. Am J Trop
Med Hyg 52:565-568.</mixed-citation>
         </ref>
         <ref id="d916e517a1310">
            <label>13</label>
            <mixed-citation id="d916e524" publication-type="other">
Andriantsoanirina V, et al. (2010) Origins of the recent emergence of Plasmodium
falciparum pyrimethamine resistance alleles in Madagascar. Antimicrob Agents
Chemother 54:2323-2329.</mixed-citation>
         </ref>
         <ref id="d916e537a1310">
            <label>14</label>
            <mixed-citation id="d916e544" publication-type="other">
Kobbe R, et al. (2011) Follow-up survey of children who received sulfadoxine-pyri-
methamine for intermittent preventive antimalarial treatment in infants. J Infect Dis
203:556-560.</mixed-citation>
         </ref>
         <ref id="d916e557a1310">
            <label>15</label>
            <mixed-citation id="d916e564" publication-type="other">
Plowe CV (2009) The evolution of drug-resistant malaria. Trans R Soc Trop Med Hyg
103(Suppl 1):S11-S14.</mixed-citation>
         </ref>
         <ref id="d916e574a1310">
            <label>16</label>
            <mixed-citation id="d916e581" publication-type="other">
Clyde DF, Shute GT (1957) Resistance of Plasmodium falciparum in Tanganyika to
pyrimethamine administered at weekly intervals. Trans R Soc Trop Med Hyg 51:
505-513.</mixed-citation>
         </ref>
         <ref id="d916e595a1310">
            <label>17</label>
            <mixed-citation id="d916e602" publication-type="other">
Cowman AF, Morry MJ, Biggs BA, Cross GA, Foote SJ (1988) Amino acid changes
linked to pyrimethamine resistance in the dihydrofolate reductase-thymidylate syn-
thase gene of Plasmodium falciparum. Proc Natl Acad Sci USA 85:9109-9113.</mixed-citation>
         </ref>
         <ref id="d916e615a1310">
            <label>18</label>
            <mixed-citation id="d916e622" publication-type="other">
Peterson DS, Walliker D, Wellems TE (1988) Evidence that a point mutation in dihy-
drofolate reductase-thymidylate synthase confers resistance to pyrimethamine in
falciparum malaria. Proc Natl Acad Sci USA 85:9114-9118.</mixed-citation>
         </ref>
         <ref id="d916e635a1310">
            <label>19</label>
            <mixed-citation id="d916e642" publication-type="other">
Kublin JG, et al. (2002) Molecular markers for failure of sulfadoxine-pyrimethamine
and chlorproguanil-dapsone treatment of Plasmodium falciparum malaria. J Infect
Dis 185:380-388.</mixed-citation>
         </ref>
         <ref id="d916e655a1310">
            <label>20</label>
            <mixed-citation id="d916e662" publication-type="other">
Foote SJ, Galatis D, Cowman AF (1990) Amino acids in the dihydrofolate reductase-
thymidylate synthase gene of Plasmodium falciparum involved in cycloguanil re-
sistance differ from those involved in pyrimethamine resistance. Proc Natl Acad Sci
USA 87:3014-3017.</mixed-citation>
         </ref>
         <ref id="d916e678a1310">
            <label>21</label>
            <mixed-citation id="d916e685" publication-type="other">
Peterson DS, Milhous WK, Wellems TE (1990) Molecular basis of differential resistance
to cycloguanil and pyrimethamine in Plasmodium falciparum malaria. Proc Natl Acad
Sci USA 87:3018-3022.</mixed-citation>
         </ref>
         <ref id="d916e698a1310">
            <label>22</label>
            <mixed-citation id="d916e705" publication-type="other">
Cortese JF, Caraballo A, Contreras CE, Plowe CV (2002) Origin and dissemination of
Plasmodium falciparum drug-resistance mutations in South America. J Infect Dis 186:
999-1006.</mixed-citation>
         </ref>
         <ref id="d916e719a1310">
            <label>23</label>
            <mixed-citation id="d916e726" publication-type="other">
Djimde A, et al. (2001) A molecular marker for chloroquine-resistant falciparum
malaria. N Engl J Med 344:257-263.</mixed-citation>
         </ref>
         <ref id="d916e736a1310">
            <label>24</label>
            <mixed-citation id="d916e743" publication-type="other">
Alifrangis M, et al. (2003) Increasing prevalence of wildtypes in the dihydrofolate
reductase gene of Plasmodium falciparum in an area with high levels of sulfadoxine/
pyrimethamine resistance after introduction of treated bed nets. Am J Trop Med Hyg
69:238-243.</mixed-citation>
         </ref>
         <ref id="d916e759a1310">
            <label>25</label>
            <mixed-citation id="d916e766" publication-type="other">
Mharakurwa S, et al. (2004) Association of house spraying with suppressed levels of
drug resistance in Zimbabwe. Malar J 3:35.</mixed-citation>
         </ref>
         <ref id="d916e776a1310">
            <label>26</label>
            <mixed-citation id="d916e783" publication-type="other">
Duraisingh MT, Curtis J, Warhurst DC (1998) Plasmodium falciparum: Detection of
polymorphisms in the dihydrofolate reductase and dihydropteroate synthetase genes
by PCR and restriction digestion. Exp Parasitol 89:1-8.</mixed-citation>
         </ref>
         <ref id="d916e796a1310">
            <label>27</label>
            <mixed-citation id="d916e803" publication-type="other">
Mulenga M, et al. (2006) A randomised, double-blind, placebo-controlled trial of
atovaquone-proguanil vs. sulphadoxine-pyrimethamine in the treatment of malarial
anaemia in Zambian children. Trop Med Int Health 11:1643-1652.</mixed-citation>
         </ref>
         <ref id="d916e816a1310">
            <label>28</label>
            <mixed-citation id="d916e823" publication-type="other">
Mawili-Mboumba DP, Ekala MT, Lekoulou F, Ntoumi F (2001) Molecular analysis of
DHFR and DHPS genes in P. falciparum clinical isolates from the Haut—Ogooue re-
gion in Gabon. Acta Trop 78:231-240.</mixed-citation>
         </ref>
         <ref id="d916e837a1310">
            <label>29</label>
            <mixed-citation id="d916e844" publication-type="other">
Muehlen M, et al. (2004) Short communication: Prevalence of mutations associated
with resistance to atovaquone and to the antifolate effect of proguanil in Plasmo-
dium falciparum isolates from northern Ghana. Trop Med Int Health 9:361-363.</mixed-citation>
         </ref>
         <ref id="d916e857a1310">
            <label>30</label>
            <mixed-citation id="d916e864" publication-type="other">
Juliano JJ, Trottman P, Mwapasa V, Meshnick SR (2008) Detection of the dihy-
drofolate reductase-164L mutation in Plasmodium falciparum infections from Malawi
by heteroduplex tracking assay. Am J Trop Med Hyg 78:892-894.</mixed-citation>
         </ref>
         <ref id="d916e877a1310">
            <label>31</label>
            <mixed-citation id="d916e884" publication-type="other">
Alker AP, et al. (2005) Mutations associated with sulfadoxine-pyrimethamine and
chlorproguanil resistance in Plasmodium falciparum isolates from Blantyre, Malawi.
Antimicrob Agents Chemother 49:3919-3921.</mixed-citation>
         </ref>
         <ref id="d916e897a1310">
            <label>32</label>
            <mixed-citation id="d916e904" publication-type="other">
Rathod PK, McErlean T, Lee PC (1997) Variations in frequencies of drug resistance in
Plasmodium falciparum. Proc Natl Acad Sci USA 94:9389-9393.</mixed-citation>
         </ref>
         <ref id="d916e914a1310">
            <label>33</label>
            <mixed-citation id="d916e921" publication-type="other">
Kublin JG, et al. (2003) Reemergence of chloroquine-sensitive Plasmodium falciparum
malaria after cessation of chloroquine use in Malawi. J Infect Dis 187:1870-1875.</mixed-citation>
         </ref>
         <ref id="d916e931a1310">
            <label>34</label>
            <mixed-citation id="d916e938" publication-type="other">
Molyneux DH, Floyd K, Barnish G, Fevre EM (1999) Transmission control and drug
resistance in malaria: A crucial interaction. Parasitol Today 15:238-240.</mixed-citation>
         </ref>
         <ref id="d916e949a1310">
            <label>35</label>
            <mixed-citation id="d916e956" publication-type="other">
Sokhna CS, Trape JF, Robert V (2001) Gametocytaemia in Senegalese children with
uncomplicated falciparum malaria treated with chloroquine, amodiaquine or sulfa-
doxine + pyrimethamine. Parasite 8:243-250.</mixed-citation>
         </ref>
         <ref id="d916e969a1310">
            <label>36</label>
            <mixed-citation id="d916e976" publication-type="other">
Hayward R, Saliba KJ, Kirk K (2005) pfmdrl mutations associated with chloroquine
resistance incur a fitness cost in Plasmodium falciparum. Mol Microbiol 55:1285-1295.</mixed-citation>
         </ref>
         <ref id="d916e986a1310">
            <label>37</label>
            <mixed-citation id="d916e993" publication-type="other">
Osman ME, Mockenhaupt FP, Bienzle U, Elbashir MI, Giha HA (2007) Field-based ev-
idence for linkage of mutations associated with chloroquine (pfcrt/pfmdr1) and sul-
fadoxine-pyrimethamine (pfdhfr/pfdhps) resistance and for the fitness cost of
multiple mutations in P. falciparum. Infect Genet Evol 7:52-59.</mixed-citation>
         </ref>
         <ref id="d916e1009a1310">
            <label>38</label>
            <mixed-citation id="d916e1016" publication-type="other">
Picken RN, et al. (1996) Molecular characterization of Borrelia burgdorferi sensu lato
from Slovenia revealing significant differences between tick and human isolates. Eur J
Clin Microbiol Infect Dis 15:313-323.</mixed-citation>
         </ref>
         <ref id="d916e1029a1310">
            <label>39</label>
            <mixed-citation id="d916e1036" publication-type="other">
Sano G, Morimatsu K, Horii T (1994) Purification and characterization of dihy-
drofolate reductase of Plasmodium falciparum expressed by a synthetic gene in Es-
cherichia coli. Mol Biochem Parasitol 63:265-273.</mixed-citation>
         </ref>
         <ref id="d916e1049a1310">
            <label>40</label>
            <mixed-citation id="d916e1056" publication-type="other">
Sirawaraporn W, Prapunwattana P, Sirawaraporn R, Yuthavong Y, Santi DV (1993)
The dihydrofolate reductase domain of Plasmodium falciparum thymidylate syn-
thase-dihydrofolate reductase. Gene synthesis, expression, and anti-folate-resistant
mutants. J Biol Chem 268:21637-21644.</mixed-citation>
         </ref>
         <ref id="d916e1073a1310">
            <label>41</label>
            <mixed-citation id="d916e1080" publication-type="other">
Musapa M, et al. (2011) A simple chelex protocol for DNA extraction from Anopheles
spp. J Visualized Exp Immunol Infect, in press.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
