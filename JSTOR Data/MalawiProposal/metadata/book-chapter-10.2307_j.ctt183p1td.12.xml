<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="doi">10.2307/j.ctt183p1td</book-id>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>The Threat of Liberation</book-title>
         <subtitle>Imperialism and Revolution in Zanzibar</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Wilson</surname>
               <given-names>Amrit</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>20</day>
         <month>11</month>
         <year>2015</year>
      </pub-date>
      <isbn content-type="ppub">9780745334073</isbn>
      <isbn content-type="epub">9781849649391</isbn>
      <publisher>
         <publisher-name>Pluto Press</publisher-name>
         <publisher-loc>London</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2013</copyright-year>
         <copyright-holder>Amrit Wilson</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt183p1td"/>
      <abstract abstract-type="short">
         <p>The Threat of Liberation returns to the tumultuous years of the Cold War, when, in a striking parallel with today, imperialist powers were seeking to institute ‘regime change’ and install pliant governments. Using iconic photographs, declassified US and British documents, and in-depth interviews, Amrit Wilson examines the role of the Umma Party of Zanzibar and its leader, the visionary Marxist revolutionary, Abdulrahman Mohamed Babu. Drawing parallels between US paranoia about Chinese Communist influence in the 1960s with contemporary fears about Chinese influence, it looks at the new race for Africa’s resources, the creation of AFRICOM and how East African politicians have bolstered US control. The book also draws on US cables released by Wikileaks showing Zanzibar's role in the ‘War on Terror’ in Eastern Africa today. The Threat of Liberation reflects on the history of a party which confronted imperialism and built unity across ethnic divisions, and considers the contemporary relevance of such strategies.</p>
      </abstract>
      <counts>
         <page-count count="192"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183p1td.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183p1td.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183p1td.3</book-part-id>
                  <title-group>
                     <title>List of photographs</title>
                  </title-group>
                  <fpage>viii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183p1td.4</book-part-id>
                  <title-group>
                     <title>Acknowledgements</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Wilson</surname>
                           <given-names>Amrit</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>x</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183p1td.5</book-part-id>
                  <title-group>
                     <title>Acronyms and abbreviations</title>
                  </title-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183p1td.6</book-part-id>
                  <title-group>
                     <title>Introduction</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>A place of ‘tailor made adventures’, and ‘ecological safaris’, ‘a paradise whose very name evokes intrigue’: these are some of the most common descriptions of Zanzibar today. Increasingly, over the first decade of the 2000s, the Isles, as Zanzibar’s two islands (Unguja and Pemba) are often called, have been sold as a playground for western tourists. This is a place, we are being asked to believe, that begins and ends in the present, with neither a past of any political relevance nor a future any different from today. Even its history is available in pill form, neatly packaged for tourist</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183p1td.7</book-part-id>
                  <title-group>
                     <label>CHAPTER ONE</label>
                     <title>Anti-Colonial Struggles – The Early Days</title>
                  </title-group>
                  <fpage>11</fpage>
                  <abstract>
                     <p>The mid-1950s and early 1960s, when this story begins, had a number of striking similarities with today. In Africa, the United States and the United Kingdom were seeking desperately to institute ‘regime change’ and bring in governments that they could manipulate and use in their own interests. They were using covert methods and ideologies of fear, whipping up paranoia and unleashing witch hunts – but at that time against communists, not ‘Islamic terrorists’. In their plan for continuing to exploit the countries of Africa (many of which were either newly independent or fighting colonialism), Zanzibar was regarded as a crucial place.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183p1td.8</book-part-id>
                  <title-group>
                     <label>CHAPTER TWO</label>
                     <title>The British Transfer Power to the Sultan and His Allies</title>
                  </title-group>
                  <fpage>35</fpage>
                  <abstract>
                     <p>The leaders of the new ZNP–ZPPP alliance now moved further to the right and closer to the Sultan than the ZNP on its own had ever been. The next election was held in January 1961, and the period leading up to it was one of intense racial campaigning by both the ASP and the ZNP–ZPPP alliance.</p>
                     <p>The elections results proved indecisive, the 22 elected representatives being split equally between the two contenders. Since neither had a parliamentary majority, an interim caretaker government was set up and another election was held in June that year. The British, eager for</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183p1td.9</book-part-id>
                  <title-group>
                     <label>CHAPTER THREE</label>
                     <title>The Zanzibar Revolution and Imperialist Fears</title>
                  </title-group>
                  <fpage>46</fpage>
                  <abstract>
                     <p>Although Umma members frequently discussed revolutionary tactics, they neither planned the insurrection in Zanzibar nor knew anything concrete about it. Babu was still away in Dar es Salaam when it erupted on January 12, 1964.</p>
                     <p>On the night of Saturday January 11, Karume and Aboud Jumbe, who was at that time the ASP’s chief whip in the legislature, informed the Special Branch that they had information that there was going to be some trouble that night, and that neither they nor the ASP were involved (Shivji, 2008: 47). Clearly Karume, at least, was worried that if the revolution failed he</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183p1td.10</book-part-id>
                  <title-group>
                     <label>CHAPTER FOUR</label>
                     <title>The Union with Tanganyika</title>
                  </title-group>
                  <fpage>61</fpage>
                  <abstract>
                     <p>The events of the fortnight prior to the Union are particularly interesting, because as we shall see, declassified CIA and US State Department documents provide us with a glimpse of the methods used by the United States, many of which are still being used today. These cables illustrate too how Tanganyika and Zanzibar were located in the imperialist vision of East Africa.</p>
                     <p>The Americans and the British were considering a number of different strategies and plans. There was the possibility, for example, of uniting the ‘moderate’ governments of Kenya, Uganda and Tanganyika in an East African Federation and drawing in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183p1td.11</book-part-id>
                  <title-group>
                     <label>CHAPTER FIVE</label>
                     <title>Karume’s Despotic Rule</title>
                  </title-group>
                  <fpage>77</fpage>
                  <abstract>
                     <p>Having got rid of Babu and others whom he saw as potential threats from Zanzibar after the Union, Karume also removed almost all other educated members of the revolutionary Council, and began to rule the islands as his personal fiefdom.</p>
                     <p>In February 1965 laws were passed which indefinitely postponed the convening of a Constituent Assembly. The next eight years were a time of unprecedented bloodshed and terror, in the course of which the rule of law was ignored, opponents were brutally murdered, sexual assault and forced marriages were widespread, trade unions and all people’s organizations were banned, and movement in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183p1td.12</book-part-id>
                  <title-group>
                     <label>CHAPTER SIX</label>
                     <title>Trial in Zanzibar’s Kangaroo Court</title>
                  </title-group>
                  <fpage>90</fpage>
                  <abstract>
                     <p>While interrogations and tortures were being carried out on the mainland, in Zanzibar the ground had been prepared for a show trial of the left to be held in the People’s Courts. Developments a few months earlier had prepared the media and the people of Zanzibar for it. The first ASP Conference in ten years, held in December 1972 under the leadership of Jumbe, had all but proclaimed that this would be the case. In the presence of Nyerere and other dignitaries from TANU, four resolutions had been passed, each one confirming a racially vindictive and right-wing agenda. The first</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183p1td.13</book-part-id>
                  <title-group>
                     <label>CHAPTER SEVEN</label>
                     <title>Zanzibar and the Mainland in the Neoliberal Era</title>
                  </title-group>
                  <fpage>100</fpage>
                  <abstract>
                     <p>More than three decades after the events we looked at in the last few chapters, if we are to believe the media hype, Zanzibar appears recently to have a taken a huge step in the right direction. So what has changed? And what remains the same?</p>
                     <p>Let us look very briefly at some of the key political changes over the last 36 years. In February 1977, the ASP joined TANU to form the Chama cha Mapinduzi (CCM), and from then on the Isles and the mainland continued under CCM rule. In 1992, a multi-party system was implemented and a new</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183p1td.14</book-part-id>
                  <title-group>
                     <label>CHAPTER EIGHT</label>
                     <title>US Interventions in Zanzibar and on the Mainland Today</title>
                  </title-group>
                  <fpage>114</fpage>
                  <abstract>
                     <p>As far back as 2003, when Benjamin Mkapa was the president, the Tanzanian government was already deeply involved in the war on terror and carrying out abduction and ‘extraordinary rendition’ under US orders. One case which has come to light and is currently before the African Commission on Human and Peoples’ rights is that of Al-Asad, a Yemeni citizen who had been living and working in Tanzania since 1985. He was arrested at his home in Dar es Salaam in December 2003, bundled into a waiting plane and flown to Djibouti, where he had never been before. There he was</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183p1td.15</book-part-id>
                  <title-group>
                     <title>APPENDIX ONE</title>
                     <subtitle>A People’s Programme: The Political Programme and Constitution of the Umma Party</subtitle>
                  </title-group>
                  <fpage>139</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183p1td.16</book-part-id>
                  <title-group>
                     <title>APPENDIX TWO</title>
                     <subtitle>Charge Sheet: Case no. 292 of 1973 (the Umma Defendants)</subtitle>
                  </title-group>
                  <fpage>149</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183p1td.17</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>157</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183p1td.18</book-part-id>
                  <title-group>
                     <title>References</title>
                  </title-group>
                  <fpage>160</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183p1td.19</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>171</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
