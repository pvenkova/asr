<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">demography</journal-id>
         <journal-id journal-id-type="jstor">j100446</journal-id>
         <journal-title-group>
            <journal-title>Demography</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Population Association of America</publisher-name>
         </publisher>
         <issn pub-type="ppub">00703370</issn>
         <issn pub-type="epub">15337790</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">20616466</article-id>
         <title-group>
            <article-title>Proximate Sources of Population Sex Imbalance in India</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Emily</given-names>
                  <surname>Oster</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>5</month>
            <year>2009</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">46</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id>i20616459</issue-id>
         <fpage>325</fpage>
         <lpage>339</lpage>
         <permissions>
            <copyright-statement>Copyright Population Association of America</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/20616466"/>
         <abstract>
            <p>There is a population sex imbalance in India. Despite a consensus that this imbalance is due to excess female mortality, the specific source of this excess mortality remains poorly understood. I use microdata on child survival in India to analyze the proximate sources of the sex imbalance. I address two questions: when in life does the sex imbalance arise, and what health or nutritional investments are specifically responsible for its appearance? I present a new methodology that uses microdata on child survival. This methodology explicitly takes into account both the possibility of naturally occurring sex differences in survival and possible differences between investments in their importance for survival. Consistent with existing literature, I find significant excess female mortality in childhood, particularly between the ages of 1 and 5, and argue that the sex imbalance that exists by age 5 is large enough to explain virtually the entire imbalance in the population. Within this age group, sex differences in vaccinations explain between 20% and 30% of excess female mortality, malnutrition explains an additional 20%, and differences in treatment for illness play a smaller role. Together, these investments account for approximately 50% of the sex imbalance in mortality in India.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d874e125a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d874e132" publication-type="other">
Wells (2000)</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d874e148a1310">
            <mixed-citation id="d874e152" publication-type="other">
Aaby, P., H. Jensen, M.-L. Garly, C. Balé, C. Martins, and I. Lisse. 2002. "Vaccinations and Child
Survival in a War Situation With High Mortality: Effect of Gender." Vaccine 21(1-2): 15-20.</mixed-citation>
         </ref>
         <ref id="d874e162a1310">
            <mixed-citation id="d874e166" publication-type="other">
Agnihotri, S. 2000. Sex Ratio Patterns in the Indian Population: A Fresh Exploration. New Delhi,
India: Sage.</mixed-citation>
         </ref>
         <ref id="d874e176a1310">
            <mixed-citation id="d874e180" publication-type="other">
Agnihotri, S., R. Palmer-Jones, and A. Parikh. 2002. "Missing Women in Indian Districts: A Quantita-
tive Analysis." Structural Change and Economic Dynamics 13:285-314.</mixed-citation>
         </ref>
         <ref id="d874e190a1310">
            <mixed-citation id="d874e194" publication-type="other">
Basu, A. 1989. "Is Discrimination in Food Really Necessary for Explaining Sex Differentials in
Childhood Mortality?" Population Studies 43:193-210.</mixed-citation>
         </ref>
         <ref id="d874e205a1310">
            <mixed-citation id="d874e209" publication-type="other">
Borooah, V. 2004. "Gender Bias Among Children in India in Their Diet and Immunization Against
Disease." Social Science and Medicine 58:1719-31.</mixed-citation>
         </ref>
         <ref id="d874e219a1310">
            <mixed-citation id="d874e223" publication-type="other">
Chen, L., A. Chowdhury, and S. Huffman. 1980. "Anthropometric Assessment of Energy-Protein
Malnutrition and Subsequent Risk of Mortality Among Pre-School Aged Children." American
Journal of Clinical Nutrition 33:1836-45.</mixed-citation>
         </ref>
         <ref id="d874e236a1310">
            <mixed-citation id="d874e240" publication-type="other">
Coale, A. 1991. "Excess Female Mortality and the Balance of the Sexes: An Estimate of the Number
of 'Missing Females.'" Population and Development Review 17:517-23.</mixed-citation>
         </ref>
         <ref id="d874e250a1310">
            <mixed-citation id="d874e254" publication-type="other">
Coale, A., P. Demeny, and B. Vaughan. 1983. Regional Model Life Tables and Stable Populations.
Princeton, NJ: Princeton University Press.</mixed-citation>
         </ref>
         <ref id="d874e264a1310">
            <mixed-citation id="d874e268" publication-type="other">
Das Gupta, M. 1987. "Selective Discrimination Against Female Children in Rural Punjab, India."
Population and Development Review 13:77-100.</mixed-citation>
         </ref>
         <ref id="d874e278a1310">
            <mixed-citation id="d874e282" publication-type="other">
Das Gupta, M. and M. Bhat. 1997. "Fertility Decline and Increased Manifestation of Sex Bias in
India." Population Studies 51:307-15.</mixed-citation>
         </ref>
         <ref id="d874e293a1310">
            <mixed-citation id="d874e297" publication-type="other">
Das Gupta, M., Z. Jiang, B. Li, and W. Chung. 2003. "Why Is Son Preference So Persistent in East
and South Asia? A Cross-Country Study of China, India and the Republic of Korea." Journal of
Development Studies 40:153-87.</mixed-citation>
         </ref>
         <ref id="d874e310a1310">
            <mixed-citation id="d874e314" publication-type="other">
Dyson, T 1984. "Excess Male Mortality in India." Economic and Political Weekly 19:422-26.</mixed-citation>
         </ref>
         <ref id="d874e321a1310">
            <mixed-citation id="d874e325" publication-type="other">
Griffiths, P., Z. Matthews, and A. Hinde. 2002. "Gender, Family, and the Nutritional Status of Chil-
dren in Three Culturally Contrasting States of India." Social Science and Medicine 55:775-90.</mixed-citation>
         </ref>
         <ref id="d874e335a1310">
            <mixed-citation id="d874e339" publication-type="other">
Jha, P., R. Kumar, P. Vasa, N. Dhingra, D. Thiruchelvam, and R. Moineddin. 2006. "Low Male-to-
Female Sex Ratio of Children Born in India: National Survey of 1.1 Million Households." Lancet
367:211-18.</mixed-citation>
         </ref>
         <ref id="d874e352a1310">
            <mixed-citation id="d874e356" publication-type="other">
Kishor, S. 1993. "'May God Give Sons to All': Gender and Child Mortality in India." American
Sociological Review 58:247-65.</mixed-citation>
         </ref>
         <ref id="d874e366a1310">
            <mixed-citation id="d874e370" publication-type="other">
Klasen, S. 1994. '"Missing Women' Reconsidered." World Development 22:1061-71.</mixed-citation>
         </ref>
         <ref id="d874e378a1310">
            <mixed-citation id="d874e382" publication-type="other">
Klasen, S. and C. Wink. 2002. "A Turning Point in Gender Bias in Mortality? An Update on the
Number of Missing Women." Population and Development Review 28:285-312.</mixed-citation>
         </ref>
         <ref id="d874e392a1310">
            <mixed-citation id="d874e396" publication-type="other">
Mishra, V., T.K. Roy, and R. Retherford. 2004. "Sex Differentials in Childhood Feeding, Health Care,
and Nutritional Status in India." Population and Development Review 30:269-95.</mixed-citation>
         </ref>
         <ref id="d874e406a1310">
            <mixed-citation id="d874e410" publication-type="other">
Murthi, A.-C.,G. Mamta, and J. Dreze. 1995. "Mortality, Fertility and Gender Bias in India: A District
Level Analysis." Population and Development Review 4:745-82.</mixed-citation>
         </ref>
         <ref id="d874e420a1310">
            <mixed-citation id="d874e424" publication-type="other">
Padmanabha, P. 1982. "Mortality in India: A Note on Trends and Implications." Economic and Politi-
cal Weekly 17:1285-90.</mixed-citation>
         </ref>
         <ref id="d874e434a1310">
            <mixed-citation id="d874e438" publication-type="other">
Pande, R. 2003. "Selective Gender Differences in Childhood Nutrition and Immunization in Rural
India: The Role of Siblings." Demography 40:395-418.</mixed-citation>
         </ref>
         <ref id="d874e448a1310">
            <mixed-citation id="d874e452" publication-type="other">
Preston, S. and M. Bhat. 1984. "New Evidence on Fertility and Mortality Trends in India." Population
and Development Review 10:481-503.</mixed-citation>
         </ref>
         <ref id="d874e463a1310">
            <mixed-citation id="d874e467" publication-type="other">
Qian, N. 2008. "Missing Women and the Price of Tea in China: The Effect of Sex-Specific Earnings
on Sex Imbalance." Quarterly Journal of Economics 123:1251-85.</mixed-citation>
         </ref>
         <ref id="d874e477a1310">
            <mixed-citation id="d874e481" publication-type="other">
Rosenzweig, M. and T.P. Schultz. 1982. "Market Opportunities, Genetic Endowments and Intrafamily
Resource Distribution: Child Survival in India." American Economic Review 72:803-15.</mixed-citation>
         </ref>
         <ref id="d874e491a1310">
            <mixed-citation id="d874e495" publication-type="other">
Sen, A. 1990. "More Than 100 Million Women Are Missing." New York Review of Books, December
20.</mixed-citation>
         </ref>
         <ref id="d874e505a1310">
            <mixed-citation id="d874e509" publication-type="other">
—. 1992. "Missing Women." British Medical Journal 304:587-88.</mixed-citation>
         </ref>
         <ref id="d874e516a1310">
            <mixed-citation id="d874e520" publication-type="other">
Visaria, P. 1971. "The Sex Ratio of the Population of India." Monograph No. 10, Census of India.
Office of the Registrar General, New Delhi, India.</mixed-citation>
         </ref>
         <ref id="d874e530a1310">
            <mixed-citation id="d874e534" publication-type="other">
Wells, J. 2000. "Natural Selection and Sex Differences in Morbidity and Mortality in Early Life."
Journal of Theoretical Biology 202:65-76.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
