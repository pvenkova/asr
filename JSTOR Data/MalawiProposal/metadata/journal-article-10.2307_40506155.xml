<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">procbiolscie</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100837</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Proceedings: Biological Sciences</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>The Royal Society</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">09628452</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">40506155</article-id>
         <article-id pub-id-type="pub-doi">10.1098/rspb.2009.1660</article-id>
         <title-group>
            <article-title>Grandma Plays Favourites: X-Chromosome Relatedness and Sex-Specific Childhood Mortality</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Molly</given-names>
                  <surname>Fox</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Rebecca</given-names>
                  <surname>Sear</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jan</given-names>
                  <surname>Beise</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Gillian</given-names>
                  <surname>Ragsdale</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Eckart</given-names>
                  <surname>Voland</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Leslie A.</given-names>
                  <surname>Knapp</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>22</day>
            <month>2</month>
            <year>2010</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">277</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1681</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40022090</issue-id>
         <fpage>567</fpage>
         <lpage>573</lpage>
         <permissions>
            <copyright-statement>Copyright 2010 The Royal Society</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/40506155"/>
         <abstract>
            <p>Biologists use genetic relatedness between family members to explain the evolution of many behavioural and developmental traits in humans, including altruism, kin investment and longevity. Women's postmenopausal longevity in particular is linked to genetic relatedness between family members. According to the ' grandmother hypothesis', post-menopausal women can increase their genetic contribution to future generations by increasing the survivorship of their grandchildren. While some demographic studies have found evidence for this, others have found little support for it. Here, we re-model the predictions of the grandmother hypothesis by examining the genetic relatedness between grandmothers and grandchildren. We use this new model to re-evaluate the grandmother effect in seven previously studied human populations. Boys and girls differ in the per cent of genes they share with maternal versus paternal grandmothers because of differences in X-chromosome inheritance. Here, we demonstrate a relationship between X-chromosome inheritance and grandchild mortality in the presence of a grandmother. With this sex-specific and X-chromosome approach to interpreting mortality rates, we provide a new perspective on the prevailing theory for the evolution of human female longevity. This approach yields more consistent support for the grandmother hypothesis, and has implications for the study of human evolution.</p>
         </abstract>
         <kwd-group>
            <kwd>longevity</kwd>
            <kwd>grandmother</kwd>
            <kwd>altruism</kwd>
            <kwd>human evolution</kwd>
            <kwd>X-chromosome</kwd>
            <kwd>kin investment</kwd>
         </kwd-group>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d484e239a1310">
            <mixed-citation id="d484e243" publication-type="other">
Ananthaswamy, A. 2002 X-rated brains. New Sci. 174,
26-30.</mixed-citation>
         </ref>
         <ref id="d484e253a1310">
            <mixed-citation id="d484e257" publication-type="other">
Beise, J. 2005 The helping and the helpful grandmother:
the role of maternal and paternal grandmothers in child
mortality in the seventeenth-and eighteenth-century
population of French settlers in Québec, Canada. In
Grandmotherhood: the evolutionary significance of the
second half of female life (eds E. Voland, A. Chasiotis &amp;
W Schiefenhovel), pp. 215-238. Piscataway, NJ:
Rutgers University Press.</mixed-citation>
         </ref>
         <ref id="d484e286a1310">
            <mixed-citation id="d484e290" publication-type="other">
Bellis, M. A., Hughes, K., Hughes, S. &amp; Ashton, J. R. 2005
Measuring paternal discrepancy and its public health
consequences. Br. Med. J. 59, 749-754.</mixed-citation>
         </ref>
         <ref id="d484e303a1310">
            <mixed-citation id="d484e307" publication-type="other">
Bergquist, C., Nillius, S. J. &amp; Wide, L. 1979 Intranasal gon-
adotropin-releasing hormone agonist as a contraceptive
agent. Lancet 2, 215-217.</mixed-citation>
         </ref>
         <ref id="d484e321a1310">
            <mixed-citation id="d484e325" publication-type="other">
Cant, M. A. &amp; Johnstone, R. A. 2008 Reproductive conflict
and the separation of reproductive generations in humans.
Proc. Natl Acad. Sci. USA 105, 5332-5336. (doi:10.1073/
pnas.0711911105)</mixed-citation>
         </ref>
         <ref id="d484e341a1310">
            <mixed-citation id="d484e345" publication-type="other">
Chrastil, E., Getz, W, Euler, H. &amp; Starks, P. 2006 Paternity
uncertainty overrides sex chromosome selection for
preferential grandparenting. Evol. Hum. Behav. 27,
206-223. (doi:10.1016/j.evolhumbehav.2005.09.002)</mixed-citation>
         </ref>
         <ref id="d484e361a1310">
            <mixed-citation id="d484e365" publication-type="other">
Cropley, J. E., Suter, C. M., Beckman, K. B. &amp; Martin,
D. I. K. 2006 Germ-line epigenetic modification of the
murine Avy allele by nutritional supplementation. Proc.
Natl Acad. Sci. USA 103, 17 308-17 312. (doi:10.1073/
pnas.0607090103)</mixed-citation>
         </ref>
         <ref id="d484e384a1310">
            <mixed-citation id="d484e388" publication-type="other">
Dorland, M., Van Kooij, R. J. &amp; Velde, E. R. T. 1998 General
ageing and ovarian ageing. Maturitas (Amsterdam) 30,
113-118. (doi:10.1016/S0378-5122(98)00066-8)</mixed-citation>
         </ref>
         <ref id="d484e401a1310">
            <mixed-citation id="d484e405" publication-type="other">
Foley, R. A. &amp; Lee, P. C. 1991 Ecology and energetics of
encephalization in hominid evolution. Phil. Trans. R. Soc.
Lond.B 334, 223-232. (doi:10.1098/rstb.1991.0111)</mixed-citation>
         </ref>
         <ref id="d484e418a1310">
            <mixed-citation id="d484e422" publication-type="other">
Gibson, M. A. &amp; Mace, R. 2005 Helpful grandmothers in
rural Ethiopia: a study of the effect of kin on child survival
and growth. Evol. Hum. Behav. 26, 469-482. (doi:10.
1016/j.evolhumbehav.2005.03.004)</mixed-citation>
         </ref>
         <ref id="d484e439a1310">
            <mixed-citation id="d484e443" publication-type="other">
Griffiths, P., Hinde, A. &amp; Matthews, Z. 2001 Infant and child
mortality in three culturally contrasting states of India.
J. Biosoc. Sci. 33, 603-622. (doi:10.1017/S0021932001
006034)</mixed-citation>
         </ref>
         <ref id="d484e459a1310">
            <mixed-citation id="d484e463" publication-type="other">
Haig, D. 2000 Genomic imprinting, sex-biased dispersal,
and social behavior. Ann. NYAcad. Sci. 907, 149-163.</mixed-citation>
         </ref>
         <ref id="d484e473a1310">
            <mixed-citation id="d484e477" publication-type="other">
Hamilton, W. D. 1966 The moulding of senescence by natu-
ral selection. J. Theor. Biol. 12, 12-45. (doi:10.1016/
0022-5193(66)90184-6)</mixed-citation>
         </ref>
         <ref id="d484e490a1310">
            <mixed-citation id="d484e494" publication-type="other">
Harper, L. V. 2005 Epigenetic inheritance and the interge-
nerational transfer of experience. Psychol. Bull. 131,
340-360. (doi:10.1037/0033-2909.131.3.340)</mixed-citation>
         </ref>
         <ref id="d484e507a1310">
            <mixed-citation id="d484e511" publication-type="other">
Hawkes, K. 2003 Grandmothers and the evolution of human
longevity. Am. J. Hum. Biol. 15, 380-400. (doi:10.1002/
ajhb.10156)</mixed-citation>
         </ref>
         <ref id="d484e524a1310">
            <mixed-citation id="d484e528" publication-type="other">
Hawkes, K. &amp; Jones, N. B. 2005 Human age structures,
paleodemography, and the grandmother hypothesis.
Grandmotherhood: the evolutionary significance of the
second half of female life (eds E. Voland, A. Chasiotis &amp;
W. Schiefenhovel), pp. 118-140. Piscataway, NJ:
Rutgers University Press.</mixed-citation>
         </ref>
         <ref id="d484e552a1310">
            <mixed-citation id="d484e556" publication-type="other">
Hawkes, K., O'Connell, J. F., Jones, N. G. B., Alvarez, H. &amp;
Charnov, E. L. 1998 Grandmothering, menopause, and
the evolution of human life histories. Proc. Natl Acad. Sci.
USA. 95, 1336-1339.</mixed-citation>
         </ref>
         <ref id="d484e572a1310">
            <mixed-citation id="d484e576" publication-type="other">
Hill, K. &amp; Hurtado, A. M. 1991 The evolution of premature
reproductive senescence and menopause in human
females. Hum. Nat. 2, 313-350. (doi:10.1007/BF026
92196)</mixed-citation>
         </ref>
         <ref id="d484e592a1310">
            <mixed-citation id="d484e596" publication-type="other">
Horth, L. 2007 Sensory genes and mate choice: evidence that
duplications, mutations, and adaptive evolution alter vari-
ation in mating cue genes and their receptors. Genomics
90, 159-175. (doi:10.1016/j.ygeno.2007.03.021)</mixed-citation>
         </ref>
         <ref id="d484e612a1310">
            <mixed-citation id="d484e616" publication-type="other">
Hrdy, S. B. 2009 Mothers and others: the evolutionary origins
of mutual understanding. Cambridge, MA: Harvard Uni-
versity Press.</mixed-citation>
         </ref>
         <ref id="d484e629a1310">
            <mixed-citation id="d484e633" publication-type="other">
Jamison, C. S., Cornell, L. L., Jamison, P. L. &amp; Nakazato, H.
2002 Are all grandmothers equal? A review and a prelimi-
nary test of the 'grandmother hypothesis' in Tokugawa,
Japan. Am. J. Phys. Anthropol 119, 67-76. (doi:10.
1002/ajpa.10070)</mixed-citation>
         </ref>
         <ref id="d484e652a1310">
            <mixed-citation id="d484e656" publication-type="other">
Kemkes-Grottenthaler, A. 2005 Of grandmothers, grand-
fathers and wicked step-grandparents. Differential
impact of paternal grandparents on grandoffspring
survival. Hist. Soc. Res. 30, 219.</mixed-citation>
         </ref>
         <ref id="d484e673a1310">
            <mixed-citation id="d484e677" publication-type="other">
Ladusingh, L. &amp; Singh, C. 2006 Place, community
education, gender and child mortality in north-east India.
Popul. Space Place 12, 65-76. (doi:10.1002/psp.393)</mixed-citation>
         </ref>
         <ref id="d484e690a1310">
            <mixed-citation id="d484e694" publication-type="other">
Lahdenperä, M., Lummaa, V, Helle, S., Tremblay, M. &amp;
Russell, A. F. 2004 Fitness benefits of prolonged post-
reproductive lifespan in women. Nature 428, 178-181.
(doi:10.1038/nature02367)</mixed-citation>
         </ref>
         <ref id="d484e710a1310">
            <mixed-citation id="d484e714" publication-type="other">
Lumey, L. H. &amp; Stein, A. D. 1997 Offspring birth weights
after maternal intrauterine undernutrition: a comparison
within sibships. Am. J. Epidemiol. 146, 810-819.</mixed-citation>
         </ref>
         <ref id="d484e727a1310">
            <mixed-citation id="d484e731" publication-type="other">
Morgan, H. D., Santos, F., Green, K., Dean, W. &amp; Reik, W.
2005 Epigenetic reprogramming in mammals. Hum. Mol.
Genet. 14, 47-58.</mixed-citation>
         </ref>
         <ref id="d484e744a1310">
            <mixed-citation id="d484e748" publication-type="other">
NCBI. 2009a Homo sapiens Chromosome: X. Map Viewer.
Bethesda, MD: US National Library of Medicine.</mixed-citation>
         </ref>
         <ref id="d484e758a1310">
            <mixed-citation id="d484e762" publication-type="other">
NCBI. 2009b Homo sapiens Chromosome: Y. Map Viewer.</mixed-citation>
         </ref>
         <ref id="d484e770a1310">
            <mixed-citation id="d484e774" publication-type="other">
NIH. 2007 A guide to your genome. Bethesda, MD: National
Human Genome Research Institute.</mixed-citation>
         </ref>
         <ref id="d484e784a1310">
            <mixed-citation id="d484e788" publication-type="other">
Parang, M., Mural, R. &amp; Adams, M. 2008 Human genome
project frequently asked questions. Washington, DC: US
Department of Energy Office of Science. http://www.
genome.gov/11006943.</mixed-citation>
         </ref>
         <ref id="d484e804a1310">
            <mixed-citation id="d484e808" publication-type="other">
Peccei, J. S. 2001 A critique of the grandmother hypotheses:
old and new. Am. J. Hum. Biol. 13, 434-452. (doi:10.
1002/ajhb.1076)</mixed-citation>
         </ref>
         <ref id="d484e821a1310">
            <mixed-citation id="d484e825" publication-type="other">
Pennisi, E. 2003 Human genome: a low number wins the
GeneSweep pool. Science 300, 1484. (doi:10.1126/
science.300.5625.1484b)</mixed-citation>
         </ref>
         <ref id="d484e838a1310">
            <mixed-citation id="d484e842" publication-type="other">
Platek, S. M., Burch, R. L., Panyavin, I. S., Wasserman,
B. H. &amp; Gallup, G. G. 2002 Reactions to children's
faces: resemblance affects males more than females.
Evol. Hum. Behav. 23, 159-166. (doi:10.1016/S1090-
5138(01)00094-0)</mixed-citation>
         </ref>
         <ref id="d484e861a1310">
            <mixed-citation id="d484e865" publication-type="other">
Pollet, T., Nelisson, M. &amp; Nettle, D. 2009 Lineage based
differences in grandparental investment: evidence from a
large British cohort study. J. Biosoc. Sci. 41, 355-379.
(doi:10.1017/S0021932009003307)</mixed-citation>
         </ref>
         <ref id="d484e882a1310">
            <mixed-citation id="d484e886" publication-type="other">
Porter, R. H., Balogh, R. D., Cernoch, J. M. &amp; Franchi, C.
1986 Recognition of kin through characteristic body
odors. Chem. Senses 11, 389-395. (doi:10.1093/chemse/
11.3.389)</mixed-citation>
         </ref>
         <ref id="d484e902a1310">
            <mixed-citation id="d484e906" publication-type="other">
Ragsdale, G. 2004 Grandmothering in Cambridgeshire,
1770-1861. Hum. Nat. 15, 301-317. (doi:10.1007/
s12110-004-1011-y)</mixed-citation>
         </ref>
         <ref id="d484e919a1310">
            <mixed-citation id="d484e923" publication-type="other">
Sear, R. 2008 Kin and child survival in rural Malawi.
Hum. Nat. 19, 277-293. (doi:10.1007/s12110-008-
9042-4)</mixed-citation>
         </ref>
         <ref id="d484e936a1310">
            <mixed-citation id="d484e940" publication-type="other">
Sear, R. &amp; Mace, R. 2007 Who keeps children alive? A review
of the effects of kin on child survival. Evol. Hum. Behav.
29, 1-18.</mixed-citation>
         </ref>
         <ref id="d484e953a1310">
            <mixed-citation id="d484e957" publication-type="other">
Sear, R., Mace, R. &amp; McGregor, I. A. 2000 Maternal grand-
mothers improve nutritional status and survival of
children in rural Gambia. Proc. R. Soc. B 267,
1641-1647. (doi:10.1098/rspb.2000.1190)</mixed-citation>
         </ref>
         <ref id="d484e973a1310">
            <mixed-citation id="d484e977" publication-type="other">
Sear, R., Steele, F, McGregor, I. A. &amp; Mace, R. 2002
The effects of kin on child mortality in rural Gambia.
Demography 39, 43-63. (doi:10.1353/dem.2002.0010)</mixed-citation>
         </ref>
         <ref id="d484e991a1310">
            <mixed-citation id="d484e995" publication-type="other">
Simmons, L. W., Firman, R. C., Rhodes, G. &amp; Peters, M.
2004 Human sperm competition: testis size, sperm pro-
duction and rates of extra-pair copulations. Anim. Behav.
68, 297-302. (doi:10.1016/j.anbehav.2003.11.013)</mixed-citation>
         </ref>
         <ref id="d484e1011a1310">
            <mixed-citation id="d484e1015" publication-type="other">
Susser, M. &amp; Stein, Z. 1994 Timing in prenatal nutrition -a
reprise of the Dutch famine study. Nutr. Rev. 52, 84-94.</mixed-citation>
         </ref>
         <ref id="d484e1025a1310">
            <mixed-citation id="d484e1029" publication-type="other">
Thorn, M. D., Stockley, P., Beynon, R. J. &amp; Hurst, J. L. 2007
Scent, mate choice and genetic heterozygosity. Chem.
Signals Vertebr. 11, 269-279.</mixed-citation>
         </ref>
         <ref id="d484e1042a1310">
            <mixed-citation id="d484e1046" publication-type="other">
Vines, G. 1997 Where did you get your brains? New Sci.
154, 34.</mixed-citation>
         </ref>
         <ref id="d484e1056a1310">
            <mixed-citation id="d484e1060" publication-type="other">
Voland, E. &amp; Beise, J. 2002 Opposite effects of maternal and
paternal grandmothers on infant survival in historical
Krummhörn. Behav. Ecol. Sociobiol. 52, 435-443.
(doi:10.1007/s00265-002-0539-2)</mixed-citation>
         </ref>
         <ref id="d484e1076a1310">
            <mixed-citation id="d484e1080" publication-type="other">
Vrba, E. S. 1985 Environment and evolution: alternative
causes of the temporal distribution of evolutionary
events. 5. Afr. J. Sci. 81, 229-236.</mixed-citation>
         </ref>
         <ref id="d484e1094a1310">
            <mixed-citation id="d484e1098" publication-type="other">
Zechner, U., Wilda, M., Kehrer-Sawatzki, H., Vogel, W.,
Fundeie, R. &amp; Hameister, H. 2001 A high density of
X-linked genes for general cognitive ability: a run-away
process shaping human evolution? Trends Genet. 17,
697-701. (doi:10.1016/S0168-9525(01)02446-5)</mixed-citation>
         </ref>
         <ref id="d484e1117a1310">
            <mixed-citation id="d484e1121" publication-type="other">
Zeller, A. 2004 Socioecology or tradition. Rev. Anthropol. 33,
19-41. (doi:10.1080/713649338)</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
