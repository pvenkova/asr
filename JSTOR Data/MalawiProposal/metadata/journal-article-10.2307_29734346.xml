<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">afrijinteafriins</journal-id>
         <journal-id journal-id-type="jstor">j100045</journal-id>
         <journal-title-group>
            <journal-title>Africa: Journal of the International African Institute</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Edinburgh University Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">00019720</issn>
         <issn pub-type="epub">17500184</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">29734346</article-id>
         <article-id pub-id-type="pub-doi">10.3366/E0001972008000211</article-id>
         <article-categories>
            <subj-group>
               <subject>Local Intellectuals Narrative and Memory</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Life/History: Personal Narratives of Development Amongst Ngo Workers and Activists in Ghana</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Thomas</given-names>
                  <surname>Yarrow</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>1</month>
            <year>2008</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">78</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id>i29734343</issue-id>
         <fpage>334</fpage>
         <lpage>358</lpage>
         <permissions>
            <copyright-statement>Copyright 2008 International African Institute</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/29734346"/>
         <abstract>
            <p>Widespread assumptions about the extractive and self-serving nature of African elites have resulted in the relative neglect of questions concerning their personal ethics and morality. Using life-history interviews undertaken with a range of Ghanaian development workers, this article explores some of the different personal aspirations, ideologies and beliefs that such narratives express. The self-identification of many of those interviewed as 'activists' is examined in terms of the related concepts of 'ideology', 'commitment' and 'sacrifice'. Much recent work within history and anthropology uses the 'life-history' as a way of introducing 'agency' that is purported to be missing in accounts focusing on larger social abstractions. Yet it is the very opposition between abstractions such as 'history' and 'society' and their own more 'personal' lives that such narratives themselves enact. The article thus interrogates the various ways in which development workers variously imagine their lives in relation to broader social and historical processes. /// Les idées répandues sur la nature extractive et intéressée des élites africaines ont conduit à un désintérêt relatif des questions concernant leur éthique personnelle et leur moralité. À travers des entretiens de récits de vie menés auprès d'un éventail d'agents de développement ghanéens, cet article explore les différentes aspirations personnelles, idéologies et croyances qu'expriment ces récits. Il examine l'étiquette d'activiste que se donne un grand nombre de personnes interrogées, en termes de concepts liés d'«idéologie», d'«engagement» et de «sacrifice». Beaucoup de travaux récents menés en histoire et en anthropologie utilisent le «récit de vie» comme moyen d'introduire l'«action» qui est censée manquer dans les récits centrés sur des abstractions sociales plus larges. C'est pourtant l'opposition même entre des abstractions (comme l'«histoire» et la «société») et leur propre existence plus personnelle que ces récits interprètent eux-mêmes. L'article interroge ainsi les différentes manières dont les agents de développement imaginent leur existence par rapport à des processus sociaux et historiques plus larges.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d1244e128a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1244e135" publication-type="other">
Brydon and Legge 1996</mixed-citation>
            </p>
         </fn>
         <fn id="d1244e142a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1244e151" publication-type="other">
Bourdieu, who suggests that: 'Principles of division, inextricably logical and sociological,
function within and for the purposes of struggle between social groups; in producing concepts,
they produce groups, the very groups which produce the principles and the groups against
which they are produced' (1979: 479).</mixed-citation>
            </p>
         </fn>
         <fn id="d1244e167a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1244e174" publication-type="other">
Linde (1993)</mixed-citation>
            </p>
         </fn>
         <fn id="d1244e181a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1244e188" publication-type="other">
Barber (1991)</mixed-citation>
            </p>
         </fn>
         <fn id="d1244e196a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1244e203" publication-type="other">
GAPVOD/ISODEC (1999).</mixed-citation>
            </p>
         </fn>
         <fn id="d1244e210a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d1244e217" publication-type="other">
Bright and Dzorgbo 2001</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1244e223" publication-type="other">
Hansen 1987, 1991</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1244e229" publication-type="other">
Jeffries 1989</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1244e236" publication-type="other">
Nugent 1996</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1244e242" publication-type="other">
Yeebo 1991</mixed-citation>
            </p>
         </fn>
         <fn id="d1244e249a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d1244e256" publication-type="other">
Fortun (2001)</mixed-citation>
            </p>
         </fn>
         <fn id="d1244e263a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d1244e270" publication-type="other">
Arnold (2004)</mixed-citation>
            </p>
         </fn>
         <fn id="d1244e277a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d1244e286" publication-type="other">
Bright and Dzorgbo 2001</mixed-citation>
            </p>
         </fn>
         <fn id="d1244e293a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d1244e300" publication-type="other">
Nugent 1996</mixed-citation>
            </p>
         </fn>
         <fn id="d1244e308a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d1244e315" publication-type="other">
Kaufmann (1997)</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d1244e331a1310">
            <mixed-citation id="d1244e335" publication-type="other">
Abu-Lughod, L. (1992) Veiled Sentiments: honor and poetry in a Bedouin society.
Berkeley and Los Angeles CA: University of California Press.</mixed-citation>
         </ref>
         <ref id="d1244e345a1310">
            <mixed-citation id="d1244e349" publication-type="other">
Alleyne, B. (2002) Radicals against Race: black activism and cultural politics.
Oxford: Berg.</mixed-citation>
         </ref>
         <ref id="d1244e359a1310">
            <mixed-citation id="d1244e363" publication-type="other">
Amanor, K., A. Denkabe and K. Wellard (1993) 'Ghana: country overview' in
K. Wellard and J. G. Copestake (eds), Non-Governmental Organizations and
the State in Africa. London and New York: Routledge.</mixed-citation>
         </ref>
         <ref id="d1244e376a1310">
            <mixed-citation id="d1244e380" publication-type="other">
Andrews, M. (1991) Lifetimes of Commitment: ageing, politics and psychology.
Cambridge and New York: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d1244e391a1310">
            <mixed-citation id="d1244e395" publication-type="other">
Arnold, D. (2004) 'The self and the cell: Indian prison narratives as life
histories' in D. Arnold and S. Blackburn (eds), Telling Lives in India:
biography, autobiography and life history. Bloomington and Indianapolis IN:
Indiana University Press.</mixed-citation>
         </ref>
         <ref id="d1244e411a1310">
            <mixed-citation id="d1244e417" publication-type="other">
Arnold, D. and S. Blackburn (2004) 'Introduction: life histories in India'
in D. Arnold and S. Blackburn (eds), Telling Lives in India: biography,
autobiography and life history. Bloomington and Indianapolis IN: Indiana
University Press.</mixed-citation>
         </ref>
         <ref id="d1244e433a1310">
            <mixed-citation id="d1244e437" publication-type="other">
Barber, K. (1991) / Could Speak until Tomorrow: oriki, women and the past in
a Yoruba town. Edinburgh: Edinburgh University Press for the International
African Institute, London.</mixed-citation>
         </ref>
         <ref id="d1244e450a1310">
            <mixed-citation id="d1244e454" publication-type="other">
Bayart, J.-F. (1986) 'Civil society in Africa' in P. Chabal (ed.), Political
Domination in Africa. Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d1244e464a1310">
            <mixed-citation id="d1244e468" publication-type="other">
-(1993) The State in Africa: the politics of the belly. London: Longman.</mixed-citation>
         </ref>
         <ref id="d1244e475a1310">
            <mixed-citation id="d1244e479" publication-type="other">
Berglund, E. (1998) Knowing Nature, Knowing Science: an ethnography of
environmental activism. Cambridge: White Horse Press.</mixed-citation>
         </ref>
         <ref id="d1244e490a1310">
            <mixed-citation id="d1244e494" publication-type="other">
Bourdieu, P. (1979) Distinction: a social critique of the judgement of taste. New
York and London: Routledge.</mixed-citation>
         </ref>
         <ref id="d1244e504a1310">
            <mixed-citation id="d1244e508" publication-type="other">
Bright, D. and D. Dzorgbo (2001) Ghana in Search of Development: the challenge
of governance, economic management and institution building. Aldershot:
Ashgate.</mixed-citation>
         </ref>
         <ref id="d1244e521a1310">
            <mixed-citation id="d1244e525" publication-type="other">
Brydon, L. and K. Legge (1996) Adjusting Society: the World Bank, the IMF and
Ghana. London: Taurus Academic Studies.</mixed-citation>
         </ref>
         <ref id="d1244e535a1310">
            <mixed-citation id="d1244e539" publication-type="other">
Caplan, P. (1997) African Voices, African Lives. London: Routledge.</mixed-citation>
         </ref>
         <ref id="d1244e546a1310">
            <mixed-citation id="d1244e550" publication-type="other">
Chabal, P. and J.-P. Daloz (1999) Africa Works: disorder as political instrument.
Oxford: lames Currey.</mixed-citation>
         </ref>
         <ref id="d1244e560a1310">
            <mixed-citation id="d1244e564" publication-type="other">
Cohen, D. W., S. F. Miescher and L. White (2001) 'Introduction: voices,
words and African history' in L. White, S. F. Miescher and D. W.
Cohen (eds), African Words, African Voices: critical practices in oral history.
Bloomington IN: Indiana University Press.</mixed-citation>
         </ref>
         <ref id="d1244e581a1310">
            <mixed-citation id="d1244e585" publication-type="other">
Crapanzano, V. (1980) Tuhami: portrait of a Moroccan. Chicago and London:
University of Chicago Press.</mixed-citation>
         </ref>
         <ref id="d1244e595a1310">
            <mixed-citation id="d1244e599" publication-type="other">
Denzin, N. (1989) Interpretive Biography. London: Sage.</mixed-citation>
         </ref>
         <ref id="d1244e606a1310">
            <mixed-citation id="d1244e610" publication-type="other">
DuGay, P. (2000) In Praise of Bureaucracy: Weber, organization, ethics. London,
Thousand Oaks CA and New Delhi: Sage Publications.</mixed-citation>
         </ref>
         <ref id="d1244e620a1310">
            <mixed-citation id="d1244e624" publication-type="other">
Ebron, P. (2002) Performing Africa. Princeton NJ: Princeton University Press.</mixed-citation>
         </ref>
         <ref id="d1244e631a1310">
            <mixed-citation id="d1244e635" publication-type="other">
Englund, H. (2006) Prisoners of Freedom: human rights and the African poor.
Berkeley CA: University of California Press.</mixed-citation>
         </ref>
         <ref id="d1244e645a1310">
            <mixed-citation id="d1244e649" publication-type="other">
Ferguson, J. (1994) The Anti-Politics Machine: 'development', depoliticisation, and
bureaucratic power in Lesotho. Minneapolis MN and London: University of
Minnesota Press.</mixed-citation>
         </ref>
         <ref id="d1244e663a1310">
            <mixed-citation id="d1244e667" publication-type="other">
Fischer, W. F. (1997) 'Doing good? The politics and antipolitics of NGO
practices', Annual Review of Anthropology 26: 439-64.</mixed-citation>
         </ref>
         <ref id="d1244e677a1310">
            <mixed-citation id="d1244e681" publication-type="other">
Fortun, K. (2001) Advocacy after Bhopal: environmentalism, disaster, new global
orders. Chicago and London: University of Chicago Press.</mixed-citation>
         </ref>
         <ref id="d1244e691a1310">
            <mixed-citation id="d1244e695" publication-type="other">
GAPVOD/ISODEC (1999) 'Directory of non-governmental organisations in
Ghana'.</mixed-citation>
         </ref>
         <ref id="d1244e705a1310">
            <mixed-citation id="d1244e709" publication-type="other">
Greenhouse, C. (1996) A Moment's Notice: time politics across cultures. Ithaca
NY and London: Cornell University Press.</mixed-citation>
         </ref>
         <ref id="d1244e719a1310">
            <mixed-citation id="d1244e723" publication-type="other">
Hansen, E. (1987) 'The state and popular struggles in Ghana, 1982-1986' in
P. Anyang' Nyong'o (ed.), Popular Struggles for Democracy in Africa. London:
Zed Books.</mixed-citation>
         </ref>
         <ref id="d1244e736a1310">
            <mixed-citation id="d1244e740" publication-type="other">
-(1991) Ghana under Rawlings: early years. Legon and Oxford: Malthouse
Press.</mixed-citation>
         </ref>
         <ref id="d1244e751a1310">
            <mixed-citation id="d1244e755" publication-type="other">
Hasty, J. (2005) The Press and Political Culture in Ghana. Bloomington and
Indianapolis IN: Indiana University Press.</mixed-citation>
         </ref>
         <ref id="d1244e765a1310">
            <mixed-citation id="d1244e769" publication-type="other">
Herzfeld, M. (1997) Portrait of a Greek Imagination: an ethnographic biography
of Andreas Nenedakis. Chicago IL and London: University of Chicago
Press.</mixed-citation>
         </ref>
         <ref id="d1244e782a1310">
            <mixed-citation id="d1244e786" publication-type="other">
Holland, D. and J. Lave (2000) History in Person: enduring struggles, contentious
practice, intimate identities. Oxford: James Currey.</mixed-citation>
         </ref>
         <ref id="d1244e796a1310">
            <mixed-citation id="d1244e800" publication-type="other">
Hoskins, J. (1998) Biographical Objects: how things tell the stories of people's lives.
New York: Routledge.</mixed-citation>
         </ref>
         <ref id="d1244e810a1310">
            <mixed-citation id="d1244e814" publication-type="other">
Jeffries, R. (1989) 'Ghana: the political economy of personal rule5 in D. Cruise
O'Brien, J. Dunn and R. Rathbone (eds), Contemporary West African States.
Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d1244e827a1310">
            <mixed-citation id="d1244e831" publication-type="other">
Johnson, A. (1996) '"Beyond the smallness of self": oral history and British
Trotskyism', Oral History (Spring): 39-48.</mixed-citation>
         </ref>
         <ref id="d1244e842a1310">
            <mixed-citation id="d1244e846" publication-type="other">
Kaufmann, G. (1997) 'Watching the developers: a partial ethnography' in
R. Grillo and R. Stirrat (eds), Discourses of Development: anthropological
perspectives. Oxford: Berg.</mixed-citation>
         </ref>
         <ref id="d1244e859a1310">
            <mixed-citation id="d1244e863" publication-type="other">
Keck, M. E. and K. Sikkink (1998) Activists beyond Borders: advocacy networks
in international politics. Ithaca NY and London: Cornell University Press.</mixed-citation>
         </ref>
         <ref id="d1244e873a1310">
            <mixed-citation id="d1244e877" publication-type="other">
Kratz, C. A. (2001) 'Conversations and lives' in L. White, S. F. Miescher and
D. W. Cohen (eds), African Words, African Voices: critical practices in oral
history. Bloomington IN: Indiana University Press.</mixed-citation>
         </ref>
         <ref id="d1244e890a1310">
            <mixed-citation id="d1244e894" publication-type="other">
Linde, C. (1993) Life Stories: the creation of coherence. New York: Oxford
University Press.</mixed-citation>
         </ref>
         <ref id="d1244e904a1310">
            <mixed-citation id="d1244e908" publication-type="other">
McCaskie, T. C. (2000) 'The consuming passions of Kwame Boakye: an essay
on agency and identity in Asante history', Journal of African Cultural Studies
13: 43-62.</mixed-citation>
         </ref>
         <ref id="d1244e921a1310">
            <mixed-citation id="d1244e925" publication-type="other">
Metcalf, B. (2004) 'The past and the present: instruction, pleasure and
blessing in Maulana Muhammad Zakariyya's Aap Biitii* in D. Arnold and
S. Blackburn (eds), Telling Lives in India: biography, autobiography and life
history. Bloomington and Indianapolis IN: Indiana University Press.</mixed-citation>
         </ref>
         <ref id="d1244e942a1310">
            <mixed-citation id="d1244e946" publication-type="other">
Miescher, S. F. (2001) 'Boakye Yiadom (Akasease Kofi of Abetifi, Kwahu):
exploring the subjectivity and "voices" of a teacher-catechist in colonial
Ghana' in L. White, S. F. Miescher and D. W. Cohen (eds), African Words,
African Voices: critical practices in oral history. Bloomington IN: Indiana
University Press.</mixed-citation>
         </ref>
         <ref id="d1244e965a1310">
            <mixed-citation id="d1244e969" publication-type="other">
Nugent, P. (1996) Big Men, Small Boys and Politics in Ghana. Accra: Asempa.</mixed-citation>
         </ref>
         <ref id="d1244e976a1310">
            <mixed-citation id="d1244e980" publication-type="other">
- (2004) Africa since Independence. Basingstoke and New York: Palgrave
Macmillan.</mixed-citation>
         </ref>
         <ref id="d1244e990a1310">
            <mixed-citation id="d1244e994" publication-type="other">
Parry, J. (2004) 'The marital history of a "thumb-impression man'" in
D. Arnold and S. Blackburn (eds), Telling Lives in India: biography,
autobiography and life history. Bloomington and Indianapolis IN: Indiana
University Press.</mixed-citation>
         </ref>
         <ref id="d1244e1010a1310">
            <mixed-citation id="d1244e1014" publication-type="other">
Piot, C. (1999) Remotely Global: village modernity in West Africa. Chicago IL
and London: University of Chicago Press.</mixed-citation>
         </ref>
         <ref id="d1244e1024a1310">
            <mixed-citation id="d1244e1028" publication-type="other">
Reddaway, J. (2002) 'An Excursus on Developing Civil Society in the Black
Star State5. Undergraduate thesis, University of Cambridge.</mixed-citation>
         </ref>
         <ref id="d1244e1039a1310">
            <mixed-citation id="d1244e1043" publication-type="other">
Shillington, K. (1992) Ghana and the Rawlings Factor. London and
Basingstoke: Macmillan Press.</mixed-citation>
         </ref>
         <ref id="d1244e1053a1310">
            <mixed-citation id="d1244e1057" publication-type="other">
Shostak, M. (1981) Nisa: the life and words of a IKung Woman. London:
Earthscan.</mixed-citation>
         </ref>
         <ref id="d1244e1067a1310">
            <mixed-citation id="d1244e1071" publication-type="other">
Summerfield, P. (2004) 'Culture and composure: creating narratives of the
gendered self in oral history interviews', Cultural and Social History 1: 65-93.</mixed-citation>
         </ref>
         <ref id="d1244e1081a1310">
            <mixed-citation id="d1244e1085" publication-type="other">
Weiner, J. (1999) 'Afterword: the project of wholeness in anthropology5,
Canberra Anthropology 22: 70-8.</mixed-citation>
         </ref>
         <ref id="d1244e1095a1310">
            <mixed-citation id="d1244e1099" publication-type="other">
Werbner, R. (2004) Reasonable Radicals and Citizenship in Botswana: the public
anthropology of Kalanga elites. Bloomington and Indianapolis IN: Indiana
University Press.</mixed-citation>
         </ref>
         <ref id="d1244e1112a1310">
            <mixed-citation id="d1244e1116" publication-type="other">
Yankah, K. (1998) Free Speech in Traditional Society. Accra: Ghana Universities
Press.</mixed-citation>
         </ref>
         <ref id="d1244e1127a1310">
            <mixed-citation id="d1244e1131" publication-type="other">
Yarrow, T. (2005) 'Creating context: knowledge, ideology and relations
amongst NGOs and development organisations in Ghana.' PhD thesis,
University of Cambridge.</mixed-citation>
         </ref>
         <ref id="d1244e1144a1310">
            <mixed-citation id="d1244e1148" publication-type="other">
Yeebo, Z. (1991) Ghana: the struggle for popular power. London: New Beacon
Books.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
