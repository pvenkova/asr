<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">economicpolicy</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100599</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Economic Policy</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Press Syndicate of the University of Cambridge</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">02664658</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">14680327</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="doi">10.2307/1344571</article-id>
         <title-group>
            <article-title>Regulating Endangered Species</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Timothy</given-names>
                  <surname>Swanson</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Patrick</given-names>
                  <surname>Bolton</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Alan</given-names>
                  <surname>Manning</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>4</month>
            <year>1993</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">8</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">16</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i257835</issue-id>
         <fpage>185</fpage>
         <lpage>205</lpage>
         <page-range>183-205</page-range>
         <permissions>
            <copyright-statement>Copyright 1993 Centre for Economic Policy Research and Maison des Sciences de l'Homme</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/1344571"/>
         <abstract>
            <p>One important issue at the 'Earth Summit' in Rio de Janeiro was the preservation of diverse biological resources, or 'biodiversity'. To date the regulation of endangered species has been based on the premise that overexploitation is the principal problem. This diagnosis has led to a wide range of attempts to 'destroy demand' for wildlife, by means of bans on trade and consumption of the products of endangered species. This approach misses the fundamental cause of the problem. Terrestrial species compete for land; land use primarily reflects human decisions about its allocation. Even overexploitation is the result of human allocation decisions; species that are overexploited are those for which society chooses not to invest in their management. Species that survive are thus those in which humans chose to invest, whilst extinction should be interpreted as disinvestment in species or their ancillary resources (habitat management). Trade bans do not address this fundamental cause of extinction. The paper sets out how demand should be constructively managed to encourage continuing investment in endangered species.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d1345e168a1310">
            <p>
               <mixed-citation id="d1345e172" publication-type="other">
Swanson (1993)</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d1345e188a1310">
            <mixed-citation id="d1345e192" publication-type="journal">
Albon, S. and N. Leader-Williams (1988). 'Allocation of Resources for Conservation', Nature.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Albon</surname>
                  </string-name>
               </person-group>
               <source>Nature</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d1345e214a1310">
            <mixed-citation id="d1345e218" publication-type="book">
Barbier, E., J. Burgess, T. Swanson and D. Pearce (1990). Elephants, Economics and Ivory, Earthscan:
London.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Barbier</surname>
                  </string-name>
               </person-group>
               <source>Elephants, Economics and Ivory</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d1345e243a1310">
            <mixed-citation id="d1345e247" publication-type="book">
Bell, R. and E. McShane-Caluzi (eds.) (1984). 'Funding and Financial Control', in Conservation
and Wildlife Management in Africa, Peace Corps: Washington, D.C.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Bell</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Funding and Financial Control</comment>
               <source>Conservation and Wildlife Management in Africa</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d1345e276a1310">
            <mixed-citation id="d1345e280" publication-type="journal">
Caughley, G. and J. Goddard (1975). 'Abundance and Distribution of Elephants in Luangwa
Valley, Zambia', African Journal of Ecology.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Caughley</surname>
                  </string-name>
               </person-group>
               <source>African Journal of Ecology</source>
               <year>1975</year>
            </mixed-citation>
         </ref>
         <ref id="d1345e306a1310">
            <mixed-citation id="d1345e310" publication-type="journal">
Clark, C. (1973). 'Profiit Maximisation and the Extinction of Animal Species', Journal of Political
Economy.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Clark</surname>
                  </string-name>
               </person-group>
               <source>Journal of Political Economy</source>
               <year>1973</year>
            </mixed-citation>
         </ref>
         <ref id="d1345e335a1310">
            <mixed-citation id="d1345e339" publication-type="book">
--- (1976). Mathematical Bioeconomics: The Optimal Management of Renewable Resources, Wiley:
New York.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Clark</surname>
                  </string-name>
               </person-group>
               <source>Mathematical Bioeconomics: The Optimal Management of Renewable Resources</source>
               <year>1976</year>
            </mixed-citation>
         </ref>
         <ref id="d1345e364a1310">
            <mixed-citation id="d1345e368" publication-type="book">
Cumming, D., C. DuToit and S. Stewart (1986). African Elephants and Rhinos, IUCN, Gland.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Cumming</surname>
                  </string-name>
               </person-group>
               <source>African Elephants and Rhinos</source>
               <year>1986</year>
            </mixed-citation>
         </ref>
         <ref id="d1345e390a1310">
            <mixed-citation id="d1345e394" publication-type="book">
Ehrlich, P. and A. Ehrlich (1981). Extinction, Random House: New York.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Ehrlich</surname>
                  </string-name>
               </person-group>
               <source>Extinction, Random House</source>
               <year>1981</year>
            </mixed-citation>
         </ref>
         <ref id="d1345e416a1310">
            <mixed-citation id="d1345e420" publication-type="journal">
Farrell, J. (1987). 'Information and the Coase Theorem', Journal of Economic Perspectives.<object-id pub-id-type="jstor">10.2307/1942984</object-id>
            </mixed-citation>
         </ref>
         <ref id="d1345e430a1310">
            <mixed-citation id="d1345e434" publication-type="journal">
Gordon, H. S. (1954). 'The Economic Theory of a Common-property Resource: The Fishery',
Journal of Political Economy.<object-id pub-id-type="jstor">10.2307/1825571</object-id>
            </mixed-citation>
         </ref>
         <ref id="d1345e448a1310">
            <mixed-citation id="d1345e452" publication-type="journal">
Hanks, J. (1972). 'Reproduction of Elephant in the Luangwa Valley, Zambia', Journal of Reproduction
and Fertility.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Hanks</surname>
                  </string-name>
               </person-group>
               <source>Journal of Reproduction and Fertility</source>
               <year>1972</year>
            </mixed-citation>
         </ref>
         <ref id="d1345e477a1310">
            <mixed-citation id="d1345e481" publication-type="journal">
Hotelling, H. (1931). 'The Economics of Exhaustible Resources', Journal of Political Economy.<object-id pub-id-type="jstor">10.2307/1822328</object-id>
            </mixed-citation>
         </ref>
         <ref id="d1345e491a1310">
            <mixed-citation id="d1345e495" publication-type="book">
IUCN Environmental Law Centre (1985). African Wildlife Laws, IUCN: Gland.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>IUCN Environmental Law Centre</surname>
                  </string-name>
               </person-group>
               <source>African Wildlife Laws</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d1345e517a1310">
            <mixed-citation id="d1345e521" publication-type="conference">
Ivory Trade Review Group (ITRG) (1989). The Ivory Trade and the Future of the African Elephant,
Report to the Conference of the Parties to CITES, Lausanne.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Ivory Trade Review Group (ITRG)</surname>
                  </string-name>
               </person-group>
               <source>The Ivory Trade and the Future of the African Elephant</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d1345e546a1310">
            <mixed-citation id="d1345e550" publication-type="book">
Lovejoy, T. (1980). 'A Projection of Species Extinctions', in G. Barney (ed.) The Global 2000 Report
to the President, Council on Environmental Quality: Washington.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Lovejoy</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">A Projection of Species Extinctions</comment>
               <source>The Global 2000 Report to the President</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
         <ref id="d1345e579a1310">
            <mixed-citation id="d1345e583" publication-type="book">
Lyster, S. (1985). International Wildlife Law, Grotius: London.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Lyster</surname>
                  </string-name>
               </person-group>
               <source>International Wildlife Law, Grotius</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d1345e606a1310">
            <mixed-citation id="d1345e610" publication-type="book">
Marks, S. (1985). The Imperial Lion: Human Dimensions of Wildlife Management in Africa, Colorado:
Westview Press.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Marks</surname>
                  </string-name>
               </person-group>
               <source>The Imperial Lion: Human Dimensions of Wildlife Management in Africa</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d1345e635a1310">
            <mixed-citation id="d1345e639" publication-type="book">
Raup, D. (1988). 'Diversity Crises in the Geological Past', in E. Wilson (op. cit.).<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Raup</surname>
                  </string-name>
               </person-group>
               <source>Diversity Crises in the Geological Past</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d1345e661a1310">
            <mixed-citation id="d1345e665" publication-type="book">
Raven, P. (1988). 'Our Diminishing Tropical Forests', in E. Wilson, op. cit.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Raven</surname>
                  </string-name>
               </person-group>
               <source>Our Diminishing Tropical Forests</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d1345e687a1310">
            <mixed-citation id="d1345e691" publication-type="book">
Reid, W. and K. Miller (1989). Keeping Options Alive, World Resources Institute: Washington.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Reid</surname>
                  </string-name>
               </person-group>
               <source>Keeping Options Alive</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d1345e713a1310">
            <mixed-citation id="d1345e717" publication-type="book">
Repetto, R. and M. Gillis (1988). Public Policies and the Misuse of Forest Resources, Cambridge
University Press: Cambridge.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Repetto</surname>
                  </string-name>
               </person-group>
               <source>Public Policies and the Misuse of Forest Resources</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d1345e742a1310">
            <mixed-citation id="d1345e746" publication-type="book">
Simberloff, D. (1986). 'Are We on the Verge of a Mass Extinction in Tropical Rain Forests?', in
D. Elliot (ed.) Dynamics of Extinction, Wiley: New York.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Simberloff</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Are We on the Verge of a Mass Extinction in Tropical Rain Forests?</comment>
               <source>Dynamics of Extinction</source>
               <year>1986</year>
            </mixed-citation>
         </ref>
         <ref id="d1345e776a1310">
            <mixed-citation id="d1345e780" publication-type="book">
Spence, M. (1975). 'Blue Whales and Applied Control Theory', in H. Gottinger (ed.) System
Approaches and Environmental Problems, Vandenhoek: Gottingen.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Spence</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Blue Whales and Applied Control Theory</comment>
               <source>System Approaches and Environmental Problems</source>
               <year>1975</year>
            </mixed-citation>
         </ref>
         <ref id="d1345e809a1310">
            <mixed-citation id="d1345e813" publication-type="book">
Swanson, T. (1989). 'Policy Options for the Regulation of the Ivory Trade', in ITRG (op. cit.).<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Swanson</surname>
                  </string-name>
               </person-group>
               <source>Policy Options for the Regulation of the Ivory Trade</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d1345e835a1310">
            <mixed-citation id="d1345e839" publication-type="book">
--- (1990). 'Conserving Biological Diversity', in D. Pearce (ed.) Blueprint 2: Greening the World
Economy, Earthscan: London.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Swanson</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Conserving Biological Diversity</comment>
               <source>Blueprint 2: Greening the World Economy</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d1345e868a1310">
            <mixed-citation id="d1345e872" publication-type="conference">
--- (1991). 'Animal Welfare and Economics: the Case of the Live Bird Trade', in S. Edwards
and J. Thomsen (eds.) Conservation and Management of Wild Birds in Trade, Report to the
Conference of the Parties to CITES, Kyoto, Japan.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Swanson</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Animal Welfare and Economics: the Case of the Live Bird Trade</comment>
               <source>Conservation and Management of Wild Birds in Trade</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d1345e904a1310">
            <mixed-citation id="d1345e908" publication-type="book">
--- (1992a). 'Policies for the Conservation of Biological Diversity', in T. Swanson and E. Barbier
(op. cit.).<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Swanson</surname>
                  </string-name>
               </person-group>
               <source>Policies for the Conservation of Biological Diversity</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d1345e933a1310">
            <mixed-citation id="d1345e937" publication-type="book">
--- (1992b). 'The Economics of Extinction Revisited and Revised', Centre for Social and Economic
Research on the Global Environment (CSERGE) Discussion Paper, CSERGE: London and
Norwich.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Swanson</surname>
                  </string-name>
               </person-group>
               <source>The Economics of Extinction Revisited and Revised</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d1345e967a1310">
            <mixed-citation id="d1345e971" publication-type="journal">
--- (1992f). 'The Economics of a Biodiversity Convention', Ambio.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Swanson</surname>
                  </string-name>
               </person-group>
               <source>Ambio</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d1345e993a1310">
            <mixed-citation id="d1345e997" publication-type="journal">
--- (1992g). 'The Evolving Trade Mechanisms in CITES', Review of European Community and
International Environmental Law.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Swanson</surname>
                  </string-name>
               </person-group>
               <source>Review of European Community and International Environmental Law</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d1345e1022a1310">
            <mixed-citation id="d1345e1026" publication-type="book">
--- (forthcoming). The International Regulation of Extinction, Macmillan: London.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Swanson</surname>
                  </string-name>
               </person-group>
               <source>The International Regulation of Extinction</source>
            </mixed-citation>
         </ref>
         <ref id="d1345e1045a1310">
            <mixed-citation id="d1345e1049" publication-type="book">
Swanson, T. and E. Barbier (eds.) (1992). Economics for the Wilds: Wildlands, Wildlife, Diversity and
Development, Earthscan: London.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Swanson</surname>
                  </string-name>
               </person-group>
               <source>Economics for the Wilds: Wildlands, Wildlife, Diversity and Development</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d1345e1074a1310">
            <mixed-citation id="d1345e1078" publication-type="journal">
Weitzman, M. (1992). 'The Value of Diversity', Quarterly Journal of Economics.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Weitzman</surname>
                  </string-name>
               </person-group>
               <source>Quarterly Journal of Economics</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d1345e1100a1310">
            <mixed-citation id="d1345e1104" publication-type="book">
Wilson, E. (1988). Biodiversity, National Academy Press: Washington.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Wilson</surname>
                  </string-name>
               </person-group>
               <source>Biodiversity</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d1345e1127a1310">
            <mixed-citation id="d1345e1131" publication-type="book">
World Conservation Monitoring Centre (WCMC) (1992). Global biodiversity, Chapman &amp; Hall:
London.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>World Conservation Monitoring Centre (WCMC)</surname>
                  </string-name>
               </person-group>
               <source>Global biodiversity</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
