<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt127zz0</book-id>
      <subj-group>
         <subject content-type="call-number">JZ5625.G56 2006</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Arms control</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Weapons of mass destruction</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Terrorism-Prevention</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">United Nations. Security Council-Resolutions</subject>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>Global Non-Proliferation and Counter-Terrorism</book-title>
         <subtitle>The Impact of UNSCR 1540</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="editor" id="contrib1">
            <name name-style="western">
               <surname>BOSCH</surname>
               <given-names>OLIVIA</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib2">
            <name name-style="western">
               <surname>VAN HAM</surname>
               <given-names>PETER</given-names>
            </name>
         </contrib>
         <role content-type="editor">editors</role>
      </contrib-group>
      <pub-date>
         <day>29</day>
         <month>08</month>
         <year>2007</year>
      </pub-date>
      <isbn content-type="ppub">9780815710172</isbn>
      <isbn content-type="epub">9780815710189</isbn>
      <publisher>
         <publisher-name>Brookings Institution Press</publisher-name>
         <publisher-loc>Washington, D.C.</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2007</copyright-year>
         <copyright-holder>ROYAL INSTITUTE OF INTERNATIONAL AFFAIRS</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7864/j.ctt127zz0"/>
      <abstract abstract-type="short">
         <p>A Brookings Institution Press, Chatham House, and Clingendael Institute publication</p>
         <p>Adopted in April 2004, UN Security Council Resolution 1540 obliges all states to take steps to prevent non-state actors, especially terrorist organizations and arms traffickers, from acquiring weapons of mass destruction and related materials. The United Nations placed itself firmly in the center of one of the world's key international security challenges.</p>
         <p>
            <italic>Global Non-Proliferation and Counter-Terrorism</italic>brings together renowned scholars and policymakers to examine a wide range of new policy-related questions arising from the resolution's impact on the bio-scientific community, the Chemical Weapons Convention, the IAEA, trade and customs, and counter-proliferation initiatives such as the Proliferation Security Initiative (PSI).The impact of 1540 goes beyond setting new legal requirements. It focuses on enforcement not only nationally but also internationally, pressing all states to place their own houses in order. Among the key questions is how the resolution will change the existing network of non-proliferation regimes. Will it merely reinforce requirements of the existing non-proliferation treaties? Or will it offer a legal framework for counter-proliferation activities and other measures to enforce the non-proliferation network?This book provides an overview of the novel policy questions UNSCR 1540's future implementation and enforcement will offer for years to come.Contributors include Jeffrey Almond, Thomas J. Biersteker (Brown University), Olivia Bosch (Chatham House), Gerald Epstein (CSIS), Chandré Gould (Center for Conflict Resolution, Cape Town )], Ron Manley (former OPCW Director of Verification) Sarah Meek (ISS), Siew Gay Ong (Ministry of Foreign Affairs, Singapore), Elizabeth Prescott (AAAS Congressional Fellow), Tariq Rauf (IAEA), Will Robinson (World Customs Organization), Roelof Jan Manschot (Eurojust), Peter van Ham (Netherlands Institute of International Relations), Ted Whiteside (NATO), and Angela Woodward (VERTIC).</p>
      </abstract>
      <counts>
         <page-count count="253"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt127zz0.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt127zz0.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt127zz0.3</book-part-id>
                  <title-group>
                     <title>Foreword</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>MOTOC</surname>
                           <given-names>MIHNEA</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>ix</fpage>
                  <abstract>
                     <p>I was honored to be invited to contribute the foreword to this volume. To my knowledge, it is the first-ever scientific undertaking of such breadth on this topic. Inputs from the specialized academic and scientific community have been enormously helpful and inspirational throughout the early and most difficult stages of the implementation process. They still are now, when the Security Council, indeed the United Nations as a whole, ponders how to take the process regarding the 1540 regime further.</p>
                     <p>UNSCR 1540 is an exceptional multilateral response to an exceptional multifaceted security challenge. By adopting it, the Security Council took the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt127zz0.4</book-part-id>
                  <title-group>
                     <title>List of Abbreviations</title>
                  </title-group>
                  <fpage>xiii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt127zz0.5</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Global Non-Proliferation and Counter-Terrorism:</title>
                     <subtitle>The Role of Resolution 1540 and Its Implications</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>VAN HAM</surname>
                           <given-names>PETER</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>BOSCH</surname>
                           <given-names>OLIVIA</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>3</fpage>
                  <abstract>
                     <p>Scenarios in which terrorists use weapons of mass destruction (WMD) have been posited for decades, but the threat and the issues involved have received new attention in the early 2000s.¹ U.S. Secretary of Defense Donald Rumsfeld has reformulated what has been popularly called the “sum of all fears” as the “nexus between weapons of mass destruction and terrorist networks,” arguing that we “really have to think very carefully about what we do as a people, and as a world, and as a society.”² This nexus—part of a more complex and interrelated composite picture of threats and vulnerabilities—and the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt127zz0.6</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>The UN’s Counter-Terrorism Efforts:</title>
                     <subtitle>Lessons for UNSCR 1540</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>BIERSTEKER</surname>
                           <given-names>THOMAS J.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>24</fpage>
                  <abstract>
                     <p>The United Nations is often criticized as an institution that is inefficient and unable to respond quickly and effectively in crisis situations and that generally lacks the capacity to adapt creatively to changing circumstances. While some have called for its fundamental reform or overhaul, others have urged its abolishment altogether. Criticisms of the UN’s management of the Oil-for-Food Program, developed to ameliorate the negative humanitarian consequences produced by comprehensive sanctions against Iraq in the 1990s, raised the volume of criticism of the organization to new levels, particularly within the United States.</p>
                     <p>Although many of the criticisms are well founded, few</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt127zz0.7</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>UNSCR 1540 and the Scientific Community as a Non-State Actor</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>PRESCOTT</surname>
                           <given-names>ELIZABETH M.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>41</fpage>
                  <abstract>
                     <p>Curbing the proliferation of high-consequence materials that could be used in biological, chemical, radiological, and nuclear weapons and related delivery systems has received increasing attention in recent years. Most of these materials and delivery technologies have legitimate uses in society and continue to be researched with the aim of improving the human condition. However, the potential exists for the inappropriate and unauthorized use of these materials and technologies, which could result in great harm. Thus setting oversight standards for non-state actors in science and technology requires a delicate balance between maximizing benefits and reducing adverse consequences.</p>
                     <p>This chapter examines the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt127zz0.8</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Motivations and Means:</title>
                     <subtitle>Scientists in Apartheid South Africa’s Nuclear, Biological, and Chemical Weapons Programs and Relevance for UNSCR 1540</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>MEEK</surname>
                           <given-names>SARAH</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>GOULD</surname>
                           <given-names>CHANDRÉ</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>54</fpage>
                  <abstract>
                     <p>For more than a decade the South African apartheid government developed its nuclear, biological, and chemical (NBC) weapons programs in secret. Only those who worked directly on the programs or oversaw their strategic direction, including at least the state president and the minister of defense, knew the details of their existence. In 1993 President F.W. de Klerk announced publicly that South Africa had pursued a program to develop nuclear warheads; until then rumors of it had been unsubstantiated. It was even later, in 1996, that details of the country’s chemical and biological weapons programs, referred to as Project Coast, its</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt127zz0.9</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Restricting Non-State Actors’ Access to Chemical Weapons and Related Materials:</title>
                     <subtitle>Implications for UNSCR 1540</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>MANLEY</surname>
                           <given-names>RON G.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>73</fpage>
                  <abstract>
                     <p>UN Security Council Resolution 1540 places a number of duties on UN member states, including to “adopt and enforce appropriate effective laws which prohibit any non-state actor to manufacture, acquire, possess, develop, transport, transfer or use nuclear, chemical or biological weapons and their means of delivery, in particular for terrorist purposes” (operative paragraph 1). The primary mechanism for ensuring the non-proliferation and eventual elimination of chemical weapons is the Chemical Weapons Convention (CWC).¹ UNSCR 1540 encourages states not only to adopt this convention but also to meet, in full, their obligations under it.</p>
                     <p>The CWC, which opened for signature in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt127zz0.10</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>UNSCR 1540 and the Role of the IAEA</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>RAUF</surname>
                           <given-names>TARIQ</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>LODDING</surname>
                           <given-names>JAN</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>86</fpage>
                  <abstract>
                     <p>Several of the programs and activities of the International Atomic Energy Agency (IAEA, the Agency) are relevant to the objectives of UN Security Council Resolution (UNSCR) 1540. The Agency assists states in, among other activities, preventing nuclear material and related technologies from falling into the hands of non-state actors, and thus its activities contribute to the implementation of Resolution 1540 by member states of the United Nations.¹ The relationship between the IAEA and UNSCR 1540 is specified in the Resolution as complementary: “none of the obligations set forth in this resolution shall be interpreted so as to conflict with .</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt127zz0.11</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>The Biological Weapons Convention and UNSCR 1540</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>WOODWARD</surname>
                           <given-names>ANGELA</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>96</fpage>
                  <abstract>
                     <p>The threat posed by biological weapons (BW) has undoubtedly grown in recent years, as non-state actors have now joined states in demonstrating an interest in developing and using these weapons of mass panic, if not destruction. Terrorist groups and individuals have sought to develop and use BW for various purposes, such as to influence political processes, to inflict economic and psychological panic as well as to kill people. The 1984 salmonella poisoning perpetrated by the Rajneesh sect in Oregon in the United States and the 2001 anthrax attacks there illustrate the means and motivations of those seeking to use BW.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt127zz0.12</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>UNSCR 1540 and “Means of Delivery”</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>WHITESIDE</surname>
                           <given-names>TED</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>113</fpage>
                  <abstract>
                     <p>UNSC Resolution 1540, adopted on April 28, 2004, is a very important step in the international community’s response to one of the challenges of the proliferation of weapons of mass destruction (WMD). The Resolution calls upon all states to “refrain from providing any form of support to non-State actors that attempt to develop, acquire, manufacture, possess, transport, transfer or use nuclear, chemical or biological weapons<italic>and their means of delivery</italic>” (operative paragraph (OP) 1, emphasis added). The last element, “means of delivery,” is the focus of this chapter. The analysis explores in particular the broader framework of the potential relationship</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt127zz0.13</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>Industry Codes of Conduct</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>ALMOND</surname>
                           <given-names>JEFFREY</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>125</fpage>
                  <abstract>
                     <p>New legislation to comply with United Nations Security Council Resolution 1540 will affect the pharmaceutical and the biotechnology industries, most notably in relation to their work on developing drugs and vaccines against infectious diseases. Access to dangerous microorganisms and to expertise in genetic manipulation is an obvious area of concern for security authorities intent on limiting the spread of potentially dual-use technology. However, safety practices are already in place throughout much of these industries (referred to below as the industry) and academia. Developed in response to biosafety concerns, to the need for quality standards for drug and vaccine products, and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt127zz0.14</book-part-id>
                  <title-group>
                     <label>10</label>
                     <title>New Border and Customs Controls for Implementing UNSCR 1540</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>ROBINSON</surname>
                           <given-names>WILL</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>136</fpage>
                  <abstract>
                     <p>United Nations Security Council Resolution (UNSCR) 1540 provides customs authorities with an important mandate and opportunity to contribute to the non-proliferation of weapons of mass destruction (WMD). Building upon UNSCR 1373 (2001), which requires all UN member states to take action against the harboring of terrorists, their movement using false travel documents, and the financing of terrorism, Resolution 1540 recognizes the need for customs expertise in detecting and preventing particularly the illegal trafficking of WMD, related materials, and delivery systems. It has drawn attention to and codified illicit trafficking as a new form of proliferation.</p>
                     <p>Resolution 1540 has the effect</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt127zz0.15</book-part-id>
                  <title-group>
                     <label>11</label>
                     <title>The Proliferation Security Initiative and Counter-Proliferation:</title>
                     <subtitle>A View from Asia</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>ONG</surname>
                           <given-names>SIEW GAY</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>153</fpage>
                  <abstract>
                     <p>The terrorist attacks on the World Trade Center and the Pentagon on September 11, 2001 (9/11), raised the specter that similarly ruthless and organized terrorists equipped with weapons of mass destruction (WMD) could wreak even more horrific carnage. Thus as combating terrorism rose to the top of the international agenda in the wake of the disaster, so too did countering the proliferation of WMD, their delivery systems, and related materials and technology. This has resulted in a number of regional and global counter-proliferation and other security initiatives, including the Proliferation Security Initiative (PSI).</p>
                     <p>First launched by President George W. Bush</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt127zz0.16</book-part-id>
                  <title-group>
                     <label>12</label>
                     <title>Law Enforcement and the Prevention of Bioterrorism:</title>
                     <subtitle>Its Impact on the U.S. Research Community</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>EPSTEIN</surname>
                           <given-names>GERALD L.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>168</fpage>
                  <abstract>
                     <p>UN Security Council Resolution 1540 requires states to “adopt and enforce appropriate effective laws which prohibit any non-State actor to manufacture, acquire, possess, develop, transport, transfer or use nuclear, chemical or biological weapons and their means of delivery, in particular for terrorist purposes.” Although the authority of the Security Council, acting under Chapter VII of the UN Charter, to compel states to enact domestic laws and regulations to combat bioterrorism is clear, if unprecedented, the precise nature of those legislative measures is not. Biological science and biotechnology are pervasively dual-use: practically all the materials, know-how, information, and biological organisms needed</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt127zz0.17</book-part-id>
                  <title-group>
                     <label>13</label>
                     <title>Prosecuting Violations of Non-Proliferation Legislation</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>MANSCHOT</surname>
                           <given-names>ROELOF JAN</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>189</fpage>
                  <abstract>
                     <p>Violations of legislation for the non-proliferation of weapons of mass destruction (WMD) confront prosecutors with a variety of obstacles to obtaining a conviction. This chapter describes some of these problems from a prosecutor’s perspective and provides examples of cases that have or are due to come to trial in the Netherlands, the seat of several international courts in its capital, The Hague. The Netherlands is among the most densely populated countries in the world, and in 2004, the Dutch port at Rotterdam was Europe’s largest (and the world’s sixth largest) container port.¹ These factors suggest the challenges that confront European</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt127zz0.18</book-part-id>
                  <title-group>
                     <label>14</label>
                     <title>UNSCR 1540:</title>
                     <subtitle>Its Future and Contribution to Global Non-Proliferation and Counter-Terrorism</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>BOSCH</surname>
                           <given-names>OLIVIA</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>VAN HAM</surname>
                           <given-names>PETER</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>207</fpage>
                  <abstract>
                     <p>Until 9/11 the United Nations was, in terms of effectiveness, a marginal actor in dealing with international terrorism. But since the “war on terror” took preeminence in the security agenda of the United States, the UN has had little choice other than to prove its relevance in a vastly changed strategic environment. In his speech to the UN General Assembly on September 12, 2002, President Bush argued, “All the world now faces a test, and the United Nations a difficult and defining moment. . . . Will the United Nations serve the purpose of its founding, or will it be</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt127zz0.19</book-part-id>
                  <title-group>
                     <title>APPENDIX A</title>
                     <subtitle>Text of UN Security Council Resolution 1540 (2004)</subtitle>
                  </title-group>
                  <fpage>227</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt127zz0.20</book-part-id>
                  <title-group>
                     <title>APPENDIX B</title>
                     <subtitle>Text of UN Security Council Resolution 1673 (2006)</subtitle>
                  </title-group>
                  <fpage>233</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt127zz0.21</book-part-id>
                  <title-group>
                     <title>APPENDIX C</title>
                     <subtitle>Guidelines for Work of the 1540 Committee</subtitle>
                  </title-group>
                  <fpage>235</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt127zz0.22</book-part-id>
                  <title-group>
                     <title>About the Authors</title>
                  </title-group>
                  <fpage>239</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt127zz0.23</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>243</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
