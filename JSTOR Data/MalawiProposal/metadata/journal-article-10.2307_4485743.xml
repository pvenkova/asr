<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">clininfedise</journal-id>
         <journal-id journal-id-type="jstor">j101405</journal-id>
         <journal-title-group>
            <journal-title>Clinical Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Chicago Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">10584838</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">4485743</article-id>
         <article-categories>
            <subj-group>
               <subject>HIV/AIDS</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Predictors of Incomplete Adherence, Virologic Failure, and Antiviral Drug Resistance among HIV-Infected Adults Receiving Antiretroviral Therapy in Tanzania</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>Habib O. Ramadhani</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Nathan M.</given-names>
                  <surname>Thielman</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>Keren Z. Landman</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>Evaline M. Ndosi</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Feng</given-names>
                  <surname>Gao</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>Jennifer L. Kirchherr</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Rekha</given-names>
                  <surname>Shah</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>Humphrey J. Shao</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>Susan C. Morpeth</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>Jonathan D. McNeill</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>John F. Shao</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>John A.</given-names>
                  <surname>Bartlett</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>John A.</given-names>
                  <surname>Crump</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>01</day>
            <month>12</month>
            <year>2007</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">45</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">11</issue>
         <issue-id>i402814</issue-id>
         <fpage>1492</fpage>
         <lpage>1498</lpage>
         <page-range>1492-1498</page-range>
         <permissions>
            <copyright-statement>Copyright 2007 The Infectious Diseases Society of America</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/4485743"/>
         <abstract>
            <p>Background. Access to antiretroviral therapy is rapidly expanding in sub-Saharan Africa. Identifying the predictors of incomplete adherence, virologic failure, and antiviral drug resistance is essential to achieving long-term success. Methods. A total of 150 subjects who had received antiretroviral therapy for at least 6 months completed a structured questionnaire and adherence assessment, and plasma human immunodeficiency virus (HIV) RNA levels were measured. Virologic failure was defined as an HIV RNA level &gt;400 copies/mL; for patients with an HIV RNA level &gt;1000 copies/mL, genotypic antiviral drug resistance testing was performed. Predictors were analyzed using bivariable and multivariable logistic regression models. Results. A total of 23 (16%) of 150 subjects reported incomplete adherence. Sacrificing health care for other necessities (adjusted odds ratio [AOR], 19.8; P &lt; .01) and the proportion of months receiving self-funded treatment (AOR, 23.5; P = .04) were associated with incomplete adherence. Virologic failure was identified in 48 (32%) of 150 subjects and was associated with incomplete adherence (AOR, 3.6; P = .03) and the proportion of months receiving self-funded antiretroviral therapy (AOR, 13.0; P = .02). Disclosure of HIV infection status to family members or others was protective against virologic failure (AOR, 0.10; P = .04). Conclusions. Self-funded treatment was associated with incomplete adherence and virologic failure, and disclosure of HIV infection status was protective against virologic failure. Efforts to provide free antiretroviral therapy and to promote social coping may enhance adherence and reduce rates of virologic failure.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d1821e264a1310">
            <label>1</label>
            <mixed-citation id="d1821e271" publication-type="book">
World Health Organization (WHO). Progress on global access to HIV
antiretroviral therapy: a report on "3 by 5" and beyond. Geneva, Swit-
zerland: WHO, 2006.<person-group>
                  <string-name>
                     <surname>World Health Organization (WHO)</surname>
                  </string-name>
               </person-group>
               <source>Progress on global access to HIV antiretroviral therapy: a report on "3 by 5" and beyond</source>
               <year>2006</year>
            </mixed-citation>
         </ref>
         <ref id="d1821e300a1310">
            <label>2</label>
            <mixed-citation id="d1821e307" publication-type="journal">
Landman KZ, Thielman NM, Mgonja A, et al. Antiretroviral treatment
literacy among HIV voluntary counseling and testing clients in Moshi,
Tanzania, 2003 to 2005. J Int Assoc Physicians AIDS Care2007; 6:24-6.<person-group>
                  <string-name>
                     <surname>Landman</surname>
                  </string-name>
               </person-group>
               <fpage>24</fpage>
               <volume>6</volume>
               <source>J Int Assoc Physicians AIDS Care</source>
               <year>2007</year>
            </mixed-citation>
         </ref>
         <ref id="d1821e342a1310">
            <label>3</label>
            <mixed-citation id="d1821e349" publication-type="journal">
Nachega JB, Steiner DM, Lehman DA, et al. Adherence to antiretroviral
therapy in HIV-infected adults in Soweto, South Africa. AIDS Res Hum
Retroviruses2004; 20:1053.<person-group>
                  <string-name>
                     <surname>Nachega</surname>
                  </string-name>
               </person-group>
               <fpage>1053</fpage>
               <volume>20</volume>
               <source>AIDS Res Hum Retroviruses</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d1821e384a1310">
            <label>4</label>
            <mixed-citation id="d1821e391" publication-type="journal">
Parruti G, Manzoli L, Toro PM, et al. Long-term adherence to first-
line highly active antiretroviral therapy in a hospital-based cohort:
predictors and impact on virologic response and relapse. AIDS Patient
Care STDs2006; 20:48-57.<person-group>
                  <string-name>
                     <surname>Parruti</surname>
                  </string-name>
               </person-group>
               <fpage>48</fpage>
               <volume>20</volume>
               <source>AIDS Patient Care STDs</source>
               <year>2006</year>
            </mixed-citation>
         </ref>
         <ref id="d1821e430a1310">
            <label>5</label>
            <mixed-citation id="d1821e437" publication-type="journal">
Stringer JSA, Zulu I, Levy J, et al. Rapid scale-up of antiretroviral theray
at primary care sites in Zambia. JAMA2006; 296:782-93.<person-group>
                  <string-name>
                     <surname>Stringer</surname>
                  </string-name>
               </person-group>
               <fpage>782</fpage>
               <volume>296</volume>
               <source>JAMA</source>
               <year>2006</year>
            </mixed-citation>
         </ref>
         <ref id="d1821e469a1310">
            <label>6</label>
            <mixed-citation id="d1821e476" publication-type="journal">
Ferradini L, Jeannin A, Pinoges L, et al. Scaling up of highly active
antiretroviral therapy in a rural district of Malawi: an effectiveness
assessment. Lancet2006; 367:1335-42.<person-group>
                  <string-name>
                     <surname>Ferradini</surname>
                  </string-name>
               </person-group>
               <fpage>1335</fpage>
               <volume>367</volume>
               <source>Lancet</source>
               <year>2006</year>
            </mixed-citation>
         </ref>
         <ref id="d1821e511a1310">
            <label>7</label>
            <mixed-citation id="d1821e518" publication-type="journal">
Calmy A, Pinoges L, Szumilin E, et al. Generic fixed-dose combination
antiretroviral treatment in resource-poor settings: multicentric obser-
vational cohort. AIDS2006;20:1163-9.<person-group>
                  <string-name>
                     <surname>Calmy</surname>
                  </string-name>
               </person-group>
               <fpage>1163</fpage>
               <volume>20</volume>
               <source>AIDS</source>
               <year>2006</year>
            </mixed-citation>
         </ref>
         <ref id="d1821e553a1310">
            <label>8</label>
            <mixed-citation id="d1821e560" publication-type="journal">
Mills EJ, Nachega JB, Buchan I, et al. Adherence to antiretroviral ther-
apy in sub-Saharan Africa and North America. JAMA2006;296:
679-90.<person-group>
                  <string-name>
                     <surname>Mills</surname>
                  </string-name>
               </person-group>
               <fpage>679</fpage>
               <volume>296</volume>
               <source>JAMA</source>
               <year>2006</year>
            </mixed-citation>
         </ref>
         <ref id="d1821e595a1310">
            <label>9</label>
            <mixed-citation id="d1821e602" publication-type="journal">
Weidle PJ, Malamba S, Mwebaze R, et al. Assessment of a pilot an-
tiretroviral drug therapy programme in Uganda: patients' response,
survival, and drug resistance. Lancet2002; 360:34-40.<person-group>
                  <string-name>
                     <surname>Weidle</surname>
                  </string-name>
               </person-group>
               <fpage>34</fpage>
               <volume>360</volume>
               <source>Lancet</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d1821e637a1310">
            <label>10</label>
            <mixed-citation id="d1821e644" publication-type="journal">
Parienti J-J, Massari V, Deschamps D, et al. Predictors of virologic
failure and resistairce in HIV-infected patients treated with nevirapine-
and efavirenz-based antiretroviral therapy. Clin Infect Dis2004;38:
1311-6.<person-group>
                  <string-name>
                     <surname>Parienti</surname>
                  </string-name>
               </person-group>
               <fpage>1311</fpage>
               <volume>38</volume>
               <source>Clin Infect Dis</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d1821e683a1310">
            <label>11</label>
            <mixed-citation id="d1821e690" publication-type="journal">
Paterson DL, Swindells S, Mohr J, et al. Adherence to protease inhibitor
therapy and outcomes in patients with HIV infection. Ann Intern Med
2000; 133:21-30.<person-group>
                  <string-name>
                     <surname>Paterson</surname>
                  </string-name>
               </person-group>
               <fpage>21</fpage>
               <volume>133</volume>
               <source>Ann Intern Med</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d1821e725a1310">
            <label>12</label>
            <mixed-citation id="d1821e732" publication-type="journal">
Sethi AK, Celentano DD, Gange SJ, Moore RD, Gallant JE. Association
between adherence to antiretroviral therapy and human immunode-
ficiency virus drug resistance. Clin Infect Dis2003; 37:1112-8.<person-group>
                  <string-name>
                     <surname>Sethi</surname>
                  </string-name>
               </person-group>
               <fpage>1112</fpage>
               <volume>37</volume>
               <source>Clin Infect Dis</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d1821e767a1310">
            <label>13</label>
            <mixed-citation id="d1821e774" publication-type="journal">
Weiser S, Wolfe W, Bangsberg D, et al. Barriers to antiretroviral ad-
herence for patients living with HIV infection and AIDS in Botswana.
J Acquir Immune Defic Syndr2003;34:281-8.<person-group>
                  <string-name>
                     <surname>Weiser</surname>
                  </string-name>
               </person-group>
               <fpage>281</fpage>
               <volume>34</volume>
               <source>J Acquir Immune Defic Syndr</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d1821e809a1310">
            <label>14</label>
            <mixed-citation id="d1821e816" publication-type="journal">
Byakika-Tusiime J, Oyugi JH, Tumwikirize WA, Katabira ET, Mugyenyi
PN, Bangsberg DR. Adherence to HIV antiretroviral therapy in HIV+
Ugandan patients purchasing therapy. Int J STD AIDS2005; 16:38-41.<person-group>
                  <string-name>
                     <surname>Byakika-Tusiime</surname>
                  </string-name>
               </person-group>
               <fpage>38</fpage>
               <volume>16</volume>
               <source>Int J STD AIDS</source>
               <year>2005</year>
            </mixed-citation>
         </ref>
         <ref id="d1821e851a1310">
            <label>15</label>
            <mixed-citation id="d1821e858" publication-type="journal">
Braitstein P, Brinkhof MW, Dabis F, et al. Mortality of HIV-1-infected
patients in the first year of antiretroviral therapy: comparison between
low-income and high-income countries. Lancet2006; 367:817-24.<person-group>
                  <string-name>
                     <surname>Braitstein</surname>
                  </string-name>
               </person-group>
               <fpage>817</fpage>
               <volume>367</volume>
               <source>Lancet</source>
               <year>2006</year>
            </mixed-citation>
         </ref>
         <ref id="d1821e893a1310">
            <label>16</label>
            <mixed-citation id="d1821e900" publication-type="journal">
Mills EJ, Nachega JB, Bangsberg DR, et al. Adherence to HAART: a
systematic review of developed and developing nation patient-reported
barriers and facilitators. PLoS Med2006; 3:e438.<person-group>
                  <string-name>
                     <surname>Mills</surname>
                  </string-name>
               </person-group>
               <fpage>e438</fpage>
               <volume>3</volume>
               <source>PLoS Med</source>
               <year>2006</year>
            </mixed-citation>
         </ref>
         <ref id="d1821e936a1310">
            <label>17</label>
            <mixed-citation id="d1821e943" publication-type="book">
INDEPTH. Vol. 3. Measuring health equity in small areas: findings
from demographic surveillance systems—population and health in de-
veloping countries. Aldershot, UK: Ashgate Publishing, 2005.<person-group>
                  <string-name>
                     <surname>Indepth</surname>
                  </string-name>
               </person-group>
               <volume>3</volume>
               <source>Measuring health equity in small areas: findings from demographic surveillance systems—population and health in developing countries</source>
               <year>2005</year>
            </mixed-citation>
         </ref>
         <ref id="d1821e975a1310">
            <label>18</label>
            <mixed-citation id="d1821e982" publication-type="journal">
Kaaya SF, Fawzi MCS, Mbwambo JK, Lee B, Msamanga GI, Fawzi WW.
Validity of the Hopkins Symptom Checklist-25 amongst HIV-positive
pregnant women in Tanzania. Acta Psychiatr Scand2002; 106:9-19.<person-group>
                  <string-name>
                     <surname>Kaaya</surname>
                  </string-name>
               </person-group>
               <fpage>9</fpage>
               <volume>106</volume>
               <source>Acta Psychiatr Scand</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d1821e1017a1310">
            <label>19</label>
            <mixed-citation id="d1821e1026" publication-type="journal">
Oyugi JH, Byakika-Tusiime J, Charlebois ED, et al. Multiple validated
measures of adherence indicate high levels of adherence to generic HIV
antiretroviral therapy in a resource-limited setting. J Acquir Immune
Defic Syndr2004; 36:1100-2.<person-group>
                  <string-name>
                     <surname>Oyugi</surname>
                  </string-name>
               </person-group>
               <fpage>1100</fpage>
               <volume>36</volume>
               <source>J Acquir Immune Defic Syndr</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d1821e1064a1310">
            <label>20</label>
            <mixed-citation id="d1821e1071" publication-type="journal">
Cai F, Chen H, Hicks CB, Bartlett JA, Zhu J, Gao F. Detection of minor
drug-resistant populations by parallel allele-specific sequencing. Nat
Methods2007; 4:123-5.<person-group>
                  <string-name>
                     <surname>Cai</surname>
                  </string-name>
               </person-group>
               <fpage>123</fpage>
               <volume>4</volume>
               <source>Nat Methods</source>
               <year>2007</year>
            </mixed-citation>
         </ref>
         <ref id="d1821e1106a1310">
            <label>21</label>
            <mixed-citation id="d1821e1113" publication-type="journal">
Johnson VA, Brun-Vezinet F, Clotet B, et al. Update of the drug re-
sistance mutations in HIV-1: fall 2005. Top HIV Med2005; 13:125-131.<person-group>
                  <string-name>
                     <surname>Johnson</surname>
                  </string-name>
               </person-group>
               <fpage>125</fpage>
               <volume>13</volume>
               <source>Top HIV Med</source>
               <year>2005</year>
            </mixed-citation>
         </ref>
         <ref id="d1821e1145a1310">
            <label>22</label>
            <mixed-citation id="d1821e1152" publication-type="journal">
Idigbe EO, Adewole TA, Eisen G, et al. Management of HIV-1 infection
with a combination of nevirapine, stavudine, and lamivudine: a pre-
liminary report on the Nigerian antiretroviral program. J Acquir Im-
mune Defic Syndr2005; 40:65-9.<person-group>
                  <string-name>
                     <surname>Idigbe</surname>
                  </string-name>
               </person-group>
               <fpage>65</fpage>
               <volume>40</volume>
               <source>J Acquir Immune Defic Syndr</source>
               <year>2005</year>
            </mixed-citation>
         </ref>
         <ref id="d1821e1191a1310">
            <label>23</label>
            <mixed-citation id="d1821e1198" publication-type="journal">
Laurent C, Diakhate N, Gueye NFN, et al. The Senegalese government's
highly active antiretroviral therapy initiative: an 18-month follow-up
study. AIDS2002; 16:1363-70.<person-group>
                  <string-name>
                     <surname>Laurent</surname>
                  </string-name>
               </person-group>
               <fpage>1363</fpage>
               <volume>16</volume>
               <source>AIDS</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d1821e1233a1310">
            <label>24</label>
            <mixed-citation id="d1821e1240" publication-type="journal">
Laurent C, Kouanfack C, Koulla-Shiro S, et al. Effectiveness and safety
of a generic fixed-dose combination of nevirapine, stavudine, and la-
mivudine in HIV-l-infected adults in Cameroon: open-label multi-
centre trial. Lancet2004; 364:29-34.<person-group>
                  <string-name>
                     <surname>Laurent</surname>
                  </string-name>
               </person-group>
               <fpage>29</fpage>
               <volume>364</volume>
               <source>Lancet</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d1821e1278a1310">
            <label>25</label>
            <mixed-citation id="d1821e1285" publication-type="journal">
Weidle PJ, Wamai N, Solberg P, et al. Adherence to antiretroviral ther-
apy in a home-based AIDS care programme in rural Uganda. Lancet
2006; 368:1587-94.<person-group>
                  <string-name>
                     <surname>Weidle</surname>
                  </string-name>
               </person-group>
               <fpage>1587</fpage>
               <volume>368</volume>
               <source>Lancet</source>
               <year>2006</year>
            </mixed-citation>
         </ref>
         <ref id="d1821e1320a1310">
            <label>26</label>
            <mixed-citation id="d1821e1327" publication-type="journal">
Kumarasamy N, Vallabhaneni S, Cecelia AJ, et al. Reasons for modi-
fication of generic highly active antiretroviral therapeutic regimens
among patients in southern India. J Acquir Immune Defic Syndr
2006; 41:53-8.<person-group>
                  <string-name>
                     <surname>Kumarasamy</surname>
                  </string-name>
               </person-group>
               <fpage>53</fpage>
               <volume>41</volume>
               <source>J Acquir Immune Defic Syndr</source>
               <year>2006</year>
            </mixed-citation>
         </ref>
         <ref id="d1821e1365a1310">
            <label>27</label>
            <mixed-citation id="d1821e1372" publication-type="journal">
Ammassari A, Trotta MP, Murri R, et al. Correlates and predictors of
adherence to highly active antiretroviral therapy: an overview of pub-
lished literature. J Acquir Immune Defic Syndr2002;31(Suppl 3):
S123-7.<person-group>
                  <string-name>
                     <surname>Ammassari</surname>
                  </string-name>
               </person-group>
               <issue>Suppl 3</issue>
               <fpage>S123</fpage>
               <volume>31</volume>
               <source>J Acquir Immune Defic Syndr</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d1821e1413a1310">
            <label>28</label>
            <mixed-citation id="d1821e1420" publication-type="journal">
Spacek LA, Shihab HM, Kamya MR, et al. Response to antiretroviral
therapy in HIV-infected patients attending a public urban clinic in
Kampala, Uganda. Clin Infect Dis2006; 42:252-9.<person-group>
                  <string-name>
                     <surname>Spacek</surname>
                  </string-name>
               </person-group>
               <fpage>252</fpage>
               <volume>42</volume>
               <source>Clin Infect Dis</source>
               <year>2006</year>
            </mixed-citation>
         </ref>
         <ref id="d1821e1456a1310">
            <label>29</label>
            <mixed-citation id="d1821e1463" publication-type="journal">
Thielman NM, Chu HY, Ostermann J, et al. Cost-effectiveness of free
HIV-1 voluntary counseling and testing through a community-based
AIDS service organization in northern Tanzania. Am J Public Health
2005; 96:114-9.<person-group>
                  <string-name>
                     <surname>Thielman</surname>
                  </string-name>
               </person-group>
               <fpage>114</fpage>
               <volume>96</volume>
               <source>Am J Public Health</source>
               <year>2005</year>
            </mixed-citation>
         </ref>
         <ref id="d1821e1501a1310">
            <label>30</label>
            <mixed-citation id="d1821e1508" publication-type="journal">
Carrieri MP, Leport C, Protopopescu C, et al. Factors associated with
nonadherence to highly active antiretroviral therapy: a 5-year follow-
up analysis with correction for the bias induced by missing data in the
treatment maintenance phase. J Acquir Immune Defic Syndr2006; 41:
477-85.<person-group>
                  <string-name>
                     <surname>Carrieri</surname>
                  </string-name>
               </person-group>
               <fpage>477</fpage>
               <volume>41</volume>
               <source>J Acquir Immune Defic Syndr</source>
               <year>2006</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
