<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">theorysociety</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100635</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Theory and Society</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Springer</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">03042421</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15737853</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">43694718</article-id>
         <title-group>
            <article-title>Problems and prospects of measurement in the study of culture</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>John W.</given-names>
                  <surname>Mohr</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Amin</given-names>
                  <surname>Ghaziani</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>7</month>
            <year>2014</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">43</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3/4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40148922</issue-id>
         <fpage>225</fpage>
         <lpage>246</lpage>
         <permissions>
            <copyright-statement>© 2014 Springer Science+Business Media Dordrecht</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/43694718"/>
         <abstract>
            <p>What is the role of measurement in the sociology of culture and how can we sort out the complexities that distinguish qualitative from quantitative approaches to this domain? In this article, we compare the issues and concerns of contemporary scholars who work on matters of culture with the writings of a group of scholars who had prepared papers for a special symposium on scientific measurement held at the annual meeting of the American Association for the Advancement of Science (AAAS) back in 1956. We focus on three issues—the recurring need to reinvent measurement (as illustrated by the career of the psychologist S.S. Stevens), the linkage between qualitative and quantitative methods of analysis (as articulated in the writings of the sociologist Paul Lazarsfeld), and the assertion (by philosophers Ernst Cassirer and Peter Caws) that theorizing necessarily precedes measuring. We review a number of important advances in the way that measurement is theorized and implemented in the sociology of culture and we also point to a number of enduring dilemmas and conundrums that continue to occupy researchers in the field today.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d939e126a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d939e133" publication-type="other">
Harthorn and Mohr 2012</mixed-citation>
            </p>
         </fn>
         <fn id="d939e140a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d939e147" publication-type="other">
AAAS 2014b</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d939e153" publication-type="other">
AAAS 2014a</mixed-citation>
            </p>
         </fn>
         <fn id="d939e160a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d939e167" publication-type="other">
Friedland and Mohr 2004</mixed-citation>
            </p>
         </fn>
         <fn id="d939e174a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d939e181" publication-type="other">
Liu (2013)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d939e187" publication-type="other">
Moretti (2013).</mixed-citation>
            </p>
         </fn>
         <fn id="d939e195a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d939e202" publication-type="other">
Teghtsoonian (2001)</mixed-citation>
            </p>
         </fn>
         <fn id="d939e209a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d939e216" publication-type="other">
Stevens 1959, p. 37</mixed-citation>
            </p>
         </fn>
         <fn id="d939e223a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d939e230" publication-type="other">
Stevens 1959, p. 21</mixed-citation>
            </p>
         </fn>
         <fn id="d939e237a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d939e244" publication-type="other">
Stevens 1959,
p. 23</mixed-citation>
            </p>
         </fn>
         <fn id="d939e254a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d939e261" publication-type="other">
Stevens 1975, p. 47</mixed-citation>
            </p>
         </fn>
         <fn id="d939e268a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d939e275" publication-type="other">
Caws 1959, p. 6</mixed-citation>
            </p>
         </fn>
         <fn id="d939e283a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d939e290" publication-type="other">
Caws 1959, p. 6</mixed-citation>
            </p>
         </fn>
         <fn id="d939e297a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d939e304" publication-type="other">
Mohr 2010</mixed-citation>
            </p>
         </fn>
         <fn id="d939e311a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d939e318" publication-type="other">
Bourdieu 1977, etc.</mixed-citation>
            </p>
         </fn>
         <fn id="d939e325a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d939e332" publication-type="other">
Appel 2000;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d939e338" publication-type="other">
Clark 2008;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d939e344" publication-type="other">
Douglass 2007;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d939e351" publication-type="other">
Geiger 1993;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d939e357" publication-type="other">
Zachary 1997</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d939e373a1310">
            <mixed-citation id="d939e377" publication-type="other">
American Association for the Advancement of Science Online Archives. (2014a). 150 years of advancing
science: A history of AAAS and the maturing of American science: 1941-1970. (http://archives.aaas.org/
exhibit/maturing2 .php).</mixed-citation>
         </ref>
         <ref id="d939e390a1310">
            <mixed-citation id="d939e394" publication-type="other">
American Association for the Advancement of Science Online Archives. (2014b). Arden house statement.
(http://www.aaas.org/page/statement-policy-aaas-arden-house-statement).</mixed-citation>
         </ref>
         <ref id="d939e404a1310">
            <mixed-citation id="d939e408" publication-type="other">
Appel, T. A. (2000). Shaping biology: The National Science Foundation and American biological research,
1945-1975. Baltimore: Johns Hopkins University Press.</mixed-citation>
         </ref>
         <ref id="d939e418a1310">
            <mixed-citation id="d939e422" publication-type="other">
Bourdieu, P. (1977). Outline of a theory of practice. Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d939e430a1310">
            <mixed-citation id="d939e434" publication-type="other">
Cassirer, E. (1923). Einstein's theory of relativity considered from the cpistemological standpoint. Supplement
to Substance and function. (Substanzbegriff und fitnktionsbegrifi) (trans: Swabey W.C. &amp; Swabey M.C.).
Chicago: The Open Court Publishing Co.</mixed-citation>
         </ref>
         <ref id="d939e447a1310">
            <mixed-citation id="d939e451" publication-type="other">
Caws, P. (1959). Definition and measurement in physics. In C. W. Churchman &amp; P. Ratoosh (Eds.),
Measurement: Definitions and theories (pp. 3-17). New York: Wiley.</mixed-citation>
         </ref>
         <ref id="d939e461a1310">
            <mixed-citation id="d939e465" publication-type="other">
Churchman, C. W., &amp; Ratoosh, P. (1959). Preface. In C. W. Churchman &amp; P. Ratoosh (Eds.), Measurement:
Definitions and theories (pp. v-vi). New York: Wiley.</mixed-citation>
         </ref>
         <ref id="d939e475a1310">
            <mixed-citation id="d939e479" publication-type="other">
Cicourel, A. V. (1964). Method and measurement in sociology. New York: Macmillan.</mixed-citation>
         </ref>
         <ref id="d939e486a1310">
            <mixed-citation id="d939e490" publication-type="other">
Clark, B. R. (2008). On higher education: Selected writings, 1956-2006. Baltimore: Johns Hopkins Press.</mixed-citation>
         </ref>
         <ref id="d939e497a1310">
            <mixed-citation id="d939e501" publication-type="other">
Converse, J. M. (1987). Survey research in the United States: Roots and emergence 1890-1960. New
Brunswick: Transaction Publishers.</mixed-citation>
         </ref>
         <ref id="d939e512a1310">
            <mixed-citation id="d939e516" publication-type="other">
Douglass, J. A. (2007). The California idea and American higher education: 1850 to the 1960 master plan.
Stanford: Stanford University Press.</mixed-citation>
         </ref>
         <ref id="d939e526a1310">
            <mixed-citation id="d939e530" publication-type="other">
Friedland, R., &amp; Mohr, J. W. (2004). The cultural turn in American sociology. In R. Friedland &amp; J. W. Mohr (Eds.),
Matters of culture: Cultural sociology in practice (pp. 1-68). Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d939e540a1310">
            <mixed-citation id="d939e544" publication-type="other">
Geiger, R. L. (1993). Research and relevant knowledge: American research universities since World War II.
New York, Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d939e554a1310">
            <mixed-citation id="d939e558" publication-type="other">
Ghaziani, A. (2009). An "amorphous mist"? The problem of measurement in the study of culture. Theory and
Society, 38(6), 581-612.</mixed-citation>
         </ref>
         <ref id="d939e568a1310">
            <mixed-citation id="d939e572" publication-type="other">
Haithorn, B. H., &amp; Mohr, J. W. (2012). Introduction: The social scientific view of nanotechnologies. In B. H.
Harthorn &amp; J. W. Mohr (Eds.), The social life of nanotechnology (pp. 1-18). NY: Routledge.</mixed-citation>
         </ref>
         <ref id="d939e582a1310">
            <mixed-citation id="d939e586" publication-type="other">
Lazarsfeld, P. F. (1957a). From the introduction to the first edition. In H. Zeisel (Ed.), Say it with figures (pp.
xv-xviii). New York: Harper and Row Publishers.</mixed-citation>
         </ref>
         <ref id="d939e597a1310">
            <mixed-citation id="d939e601" publication-type="other">
Lazarsfeld, P. F. (1957b). Introduction to the fourth edition. In H. Zeisel (Ed.), Say it with figures (pp. xi-xiii).
New York: Harper and Row Publishers.</mixed-citation>
         </ref>
         <ref id="d939e611a1310">
            <mixed-citation id="d939e615" publication-type="other">
Lazarsfeld, P. F. (1982). An episode in the history of social research: A memoir. In P. L. Kendall (Ed.), The
varied sociology of Paul F. Lazarsfeld (pp. 11-73). New York: Columbia University Press.</mixed-citation>
         </ref>
         <ref id="d939e625a1310">
            <mixed-citation id="d939e629" publication-type="other">
Lazarsfeld, P. F., &amp; Barton, A. H. (1951). Qualitative measurement in the social sciences: Classification,
typologies, and indices. In D. Lemcr and H. D. Lasswell (Eds.), The policy sciences: Recent develop-
ments in scope and method, (pp. 155 -192). Stanford, California: Stanford University Press.</mixed-citation>
         </ref>
         <ref id="d939e642a1310">
            <mixed-citation id="d939e646" publication-type="other">
Lazarsfeld, P. F., &amp; Thielens, W. (1958). The academic mind. Glencoe: The Free Press.</mixed-citation>
         </ref>
         <ref id="d939e653a1310">
            <mixed-citation id="d939e657" publication-type="other">
Liu, A. (2013). The meaning of the digital humanities. PMLA, 128 , 409-423.</mixed-citation>
         </ref>
         <ref id="d939e664a1310">
            <mixed-citation id="d939e668" publication-type="other">
Mohr, J. W. (2010). Emst Cassirer: Science, symbols and logic. In C. Edling &amp; J. Rydgren (Eds.), Sociological
insights of great thinkers: From Aristotle to Zola (pp. 113-122). Santa Barbara, CA: Praeger.</mixed-citation>
         </ref>
         <ref id="d939e679a1310">
            <mixed-citation id="d939e683" publication-type="other">
Moretti, F. (2013). Distant reading. London: Verso.</mixed-citation>
         </ref>
         <ref id="d939e690a1310">
            <mixed-citation id="d939e694" publication-type="other">
Russell, B. (1937). The principles of mathematics (2nd ed.). New York: Norton.</mixed-citation>
         </ref>
         <ref id="d939e701a1310">
            <mixed-citation id="d939e705" publication-type="other">
Stevens, S. S. (1946). On the theory of scales and measurement. Science, 103(2684), 677-680.</mixed-citation>
         </ref>
         <ref id="d939e712a1310">
            <mixed-citation id="d939e716" publication-type="other">
Stevens, S. S. (1959). Measurement, psychophysics, and utility. In C. W. Churchmen &amp; P. Ratoosh (Eds.),
Measurement: Definitions and theories (dd. 18-63). New York: Wilev.</mixed-citation>
         </ref>
         <ref id="d939e726a1310">
            <mixed-citation id="d939e730" publication-type="other">
Stevens, S. S. (1975). Psychophysics. New York: Transaction.</mixed-citation>
         </ref>
         <ref id="d939e737a1310">
            <mixed-citation id="d939e741" publication-type="other">
Teghtsoonian, R. (2001). Stevens, Stanley Smith (1906-73). In N. J. Smelser &amp; P. B. Baltes (Eds.), International
encyclopedia of the social and behavioral sciences (pp. 15105-15108). Amsterdam: Elsevier.</mixed-citation>
         </ref>
         <ref id="d939e752a1310">
            <mixed-citation id="d939e756" publication-type="other">
Zachary, G. P. (1997). Endless frontier : Vannevar Bush, engineer of the American century. New York: Free Press.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
