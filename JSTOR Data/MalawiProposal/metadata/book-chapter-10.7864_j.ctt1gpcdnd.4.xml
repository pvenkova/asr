<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1gpcdnd</book-id>
      <subj-group>
         <subject content-type="call-number">E902.D23 2003</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">United States</subject>
         <subj-group>
            <subject content-type="lcsh">Foreign relations</subject>
            <subj-group>
               <subject content-type="lcsh">2001–</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">United States</subject>
         <subj-group>
            <subject content-type="lcsh">Foreign relations</subject>
            <subj-group>
               <subject content-type="lcsh">Philosophy</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Bush, George W. (George Walker), 1946</subject>
         <subj-group>
            <subject content-type="lcsh">Views on international relations</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Balance of power</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Unilateral acts (International law)</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">War on Terrorism, 2001</subject>
         <subj-group>
            <subject content-type="lcsh">Diplomatic history</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">September 11 Terrorist Attacks, 2001</subject>
         <subj-group>
            <subject content-type="lcsh">Influence</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>America Unbound</book-title>
         <subtitle>The Bush Revolution in Foreign Policy</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>DAALDER</surname>
               <given-names>IVO H.</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib2">
            <name name-style="western">
               <surname>LINDSAY</surname>
               <given-names>JAMES M.</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>30</day>
         <month>09</month>
         <year>2003</year>
      </pub-date>
      <isbn content-type="ppub">9780815716884</isbn>
      <isbn content-type="epub">9780815796619</isbn>
      <publisher>
         <publisher-name>Brookings Institution Press</publisher-name>
         <publisher-loc>Washington, D.C.</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2003</copyright-year>
         <copyright-holder>THE BROOKINGS INSTITUTION</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7864/j.ctt1gpcdnd"/>
      <abstract abstract-type="short">
         <p>George W. Bush has launched a revolution in American foreign policy. He has redefined how America engages the world, shedding the constraints that friends, allies, and international institutions impose on its freedom of action. He has insisted that an America unbound is a more secure America. How did a man once mocked for knowing little about the world come to be a foreign policy revolutionary? In America Unbound, Ivo H. Daalder and James M. Lindsay dismiss claims that neoconservatives have captured the heart and mind of the president. They show that George W. Bush has been no one's puppet. He has been a strong and decisive leader with a coherent worldview that was evident even during the 2000 presidential campaign. Daalder and Lindsay caution that the Bush revolution comes with significant risks. Raw power alone is not enough to preserve and extend America's security and prosperity in the modern world. The United States often needs the help of others to meet the challenges it faces overseas. But Bush's revolutionary impulse has stirred great resentment abroad. At some point, Daalder and Lindsay warn, Bush could find that America's friends and allies refuse to follow his lead. America will then stand alone-a great power unable to achieve its most important goals.</p>
      </abstract>
      <counts>
         <page-count count="246"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcdnd.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcdnd.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcdnd.3</book-part-id>
                  <title-group>
                     <label>ONE</label>
                     <title>The Bush Revolution</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>George W. Bush had reason to be pleased as he peered down at Baghdad from the window of Air Force One in early June 2003. He had just completed a successful visit to Europe and the Middle East. The trip began in Warsaw, where he had the opportunity to personally thank Poland for being one of just two European countries to contribute troops to the Iraq War effort. He then traveled to Russia to celebrate the three hundredth birthday of St. Petersburg and to sign the papers formally ratifying a treaty committing Moscow and Washington to slash their nuclear arsenals.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcdnd.4</book-part-id>
                  <title-group>
                     <label>TWO</label>
                     <title>George Bush and the Vulcans</title>
                  </title-group>
                  <fpage>17</fpage>
                  <abstract>
                     <p>George W. Bush sat down for the interview with WHDH-TV in Boston in early November 1999 expecting it to be like the dozens of other local television interviews he had done around the country since declaring himself a presidential candidate months earlier. He would chat with the reporter for a dozen or so minutes, use key passages from his stump speech to answer questions, and then move on to his next interview. Reporter Andy Hiller had something different in mind, however. Bush had been knocked for knowing little about foreign affairs. So Hiller intended to find out what the Texas</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcdnd.5</book-part-id>
                  <title-group>
                     <label>THREE</label>
                     <title>Bush’s Worldview</title>
                  </title-group>
                  <fpage>35</fpage>
                  <abstract>
                     <p>When foreign policy came up during the 2000 Bush campaign, the discussions typically focused on what the Texas governor knew about world politics and whether it was enough to be commander in chief. Often overlooked was a different, and more important question: What did he believe? The fixation on how much George W. Bush knew is in many ways understandable. Journalists and intellectuals often assume that beliefs are built on a foundation of facts. This assumption is usually wrong. Just listen to the visceral pronouncements on talk radio. As these show, people generally come to their beliefs about how the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcdnd.6</book-part-id>
                  <title-group>
                     <label>FOUR</label>
                     <title>Building a Team</title>
                  </title-group>
                  <fpage>50</fpage>
                  <abstract>
                     <p>George W. Bush won the 2000 presidential election, but hardly in the way he expected. In the final weeks of the campaign his advisers confidently predicted victory. Karl Rove, Bush’s chief political strategist, told Bush that he would beat Al Gore by 4 to 7 percentage points in the popular vote and pick up 320 votes in the Electoral College.¹ Election Day, however, produced arguably the closest presidential race in American history. Bush drew 540,000 fewer votes than Gore. The dispute over the vote count in Florida threw the outcome into doubt for five weeks. It took a ruling by</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcdnd.7</book-part-id>
                  <title-group>
                     <label>FIVE</label>
                     <title>The First Eight Months</title>
                  </title-group>
                  <fpage>62</fpage>
                  <abstract>
                     <p>World affairs might not have been at the top of George W. Bush’s priority list during the presidential campaign, but many of his supporters expected him to move quickly to revamp U.S. national security policy. Hawks assumed he would turn the defense budget tap wide open. Missile defense enthusiasts predicted a rapid U.S. withdrawal from the ABM Treaty and a new Manhattan Project to build a national missile defense. Beijing haters anticipated a push to redirect the U.S. military to counter a rising China, a blunt declaration of the administration’s intent to defend Taiwan, and massive arms sales to Taipei.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcdnd.8</book-part-id>
                  <title-group>
                     <label>SIX</label>
                     <title>September 11</title>
                  </title-group>
                  <fpage>78</fpage>
                  <abstract>
                     <p>By Labor Day 2001, the political winds were beginning to blow against George W. Bush. The slumping economy, the Enron scandal, growing anger in foreign capitals over the administration’s unilateralism, and criticism of his month-long “working” vacation in Crawford had obscured his impressive success in engineering passage of a $1.35 trillion tax cut. Early September polls showed that his public approval rating had fallen to 51 percent. Only Gerald Ford, who alienated many voters when he pardoned Richard Nixon, had experienced lower ratings during his first eight months in office. David Frum, a Bush speechwriter at the time, probably exaggerated</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcdnd.9</book-part-id>
                  <title-group>
                     <label>SEVEN</label>
                     <title>Onto the Offensive</title>
                  </title-group>
                  <fpage>98</fpage>
                  <abstract>
                     <p>The gravity of the terrorist attacks on September 11 left no question that the United States would respond. “Terrorism against our nation will not stand,” George W. Bush declared moments after the second jet slammed into the south tower of the World Trade Center.¹ CIA director George Tenet and White House counterterrorism czar Richard Clarke immediately concluded that Osama bin Laden’s fingerprints were all over the attack. Bin Laden was responsible for many earlier attacks against U.S. targets, including the bombing of U.S. embassies in Kenya and Tanzania in 1998 and of the USS<italic>Cole</italic>in Yemen in 2000. He</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcdnd.10</book-part-id>
                  <title-group>
                     <label>EIGHT</label>
                     <title>The Bush Strategy</title>
                  </title-group>
                  <fpage>116</fpage>
                  <abstract>
                     <p>The Afghan war enjoyed broad international support. A large coalition of countries offered troops, aircraft, naval vessels, and other forms of direct military assistance. After initially rejecting many offers of help, the Bush administration eventually accepted troop contributions from nearly twenty countries. Britain supplied special forces, and one of its cruise missiles was fired in the very first volley of the war. France contributed Mirage fighter jets that bombed al Qaeda concentrations during Operation Anaconda in the battle of Shar-e-Kot. Germany, Denmark, Australia, and others sent special forces. Key Gulf states offered bases from which to fly bombing and surveillance</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcdnd.11</book-part-id>
                  <title-group>
                     <label>NINE</label>
                     <title>The Inevitable Showdown</title>
                  </title-group>
                  <fpage>129</fpage>
                  <abstract>
                     <p>Had terrorists not attacked America on September 11, Iraq would likely have remained a secondary issue in American foreign policy. Before the attacks, the Bush administration had sought to strengthen the containment of Saddam Hussein through the adoption of so-called smart sanctions, which focused on preventing Iraq from importing military goods while relaxing restrictions on trade more generally. Colin Powell went as far as to claim the policy a success. “We have kept him contained, kept him in his box,” he said following his first tour as secretary of state around the Middle East.¹ That was not the view of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcdnd.12</book-part-id>
                  <title-group>
                     <label>TEN</label>
                     <title>The Iraq War</title>
                  </title-group>
                  <fpage>145</fpage>
                  <abstract>
                     <p>At 3:30 p.m. on March 19, George W. Bush’s war council gathered in the Oval Office for a hastily called meeting. CIA director George Tenet had important information. A reliable source inside Baghdad had told the agency that Saddam Hussein, his sons, and much of the senior Iraqi leadership would be gathering that night at a private home called Dora Farms on the southern outskirts of Baghdad. Should the president give the order to strike the complex in the hope of killing Saddam even before the war against his regime had started? Only hours before, Bush and his aides had</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcdnd.13</book-part-id>
                  <title-group>
                     <label>ELEVEN</label>
                     <title>Who’s Next?</title>
                  </title-group>
                  <fpage>172</fpage>
                  <abstract>
                     <p>When Colin Powell was asked where foreign policy would be heading after the Iraq War, the secretary of state answered that rebuilding Iraq and resuming the Middle East peace process would come first. “What we will do beyond that is to fully engage again on the President’s agenda, [going] back to his emphasis on free trade agreements, his emphasis on creating communities and democracies around the world, continuing to build the relationship that we have with Russia and China.” And there were other important issues to address: “HIV/AIDS, famine, climate, obviously, we’re not going to join Kyoto, but we have</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcdnd.14</book-part-id>
                  <title-group>
                     <label>TWELVE</label>
                     <title>The Perils of Power</title>
                  </title-group>
                  <fpage>188</fpage>
                  <abstract>
                     <p>Chinese leader Chou En-lai was once asked what he thought of the French Revolution. “It’s too early to tell,” he replied. The same could be said of the Bush revolution. It was tempting to call it a smashing success after impressive military victories against the Taliban and Saddam Hussein. However, the ultimate question was not whether it worked in the short run, but whether it enhanced the security, prosperity, and liberty of the American people in the long run. Were Americans better off with or without the Bush revolution?</p>
                     <p>George W. Bush was hardly alone in understanding that America possessed</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcdnd.15</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>201</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcdnd.16</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <fpage>227</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcdnd.17</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>229</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
