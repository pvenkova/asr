<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">jinfedise</journal-id>
         <journal-id journal-id-type="jstor">j50000007</journal-id>
         <journal-title-group>
            <journal-title>The Journal of Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Chicago Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">00221899</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">30087726</article-id>
         <article-id pub-id-type="pub-doi">10.1086/522011</article-id>
         <article-categories>
            <subj-group>
               <subject>Major Articles and Brief Reports</subject>
               <subj-group>
                  <subject>Parasites</subject>
               </subj-group>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Spatially and Genetically Distinct African Trypanosome Virulence Variants Defined by Host Interferon-γ Response</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Lorna</given-names>
                  <surname>MacLean</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Martin</given-names>
                  <surname>Odiit</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Annette</given-names>
                  <surname>MacLeod</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Liam</given-names>
                  <surname>Morrison</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Lindsay</given-names>
                  <surname>Sweeney</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Anneli</given-names>
                  <surname>Cooper</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Peter G. E.</given-names>
                  <surname>Kennedy</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Jeremy M.</given-names>
                  <surname>Sternberg</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>12</month>
            <year>2007</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">196</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">11</issue>
         <issue-id>i30087717</issue-id>
         <fpage>1620</fpage>
         <lpage>1628</lpage>
         <permissions>
            <copyright-statement>Copyright 2007 Infectious Diseases Society of America</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/30087726"/>
         <abstract>
            <p>We describe 2 spatially distinct foci of human African trypansomiasis in eastern Uganda. The Tororo and Soroti foci of Trypanosoma brucei rhodesiense infection were genetically distinct as characterized by 6 microsatellite and 1 minisatellite polymorphic markers and were characterized by differences in disease progression and hostimmune response. In particular, infections with the Tororo genotype exhibited an increased frequency of progression to and severity of the meningoencephalitic stage and higher plasma interferon (IFN)-γ concentration, compared with those with the Soroti genotype. We propose that the magnitude of the systemic IFN-γ response determines the time at which infected individuals develop central nervous system infection and that this is consistent with the recently described role of IFN-γ in facilitating blood-brain barrier transmigration of trypanosomes in an experimental model of infection. The identification of trypanosome isolates with differing disease progression phenotypes provides the first field-based genetic evidence for virulence variants in T. brucei rhodesiense.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d253e318a1310">
            <label>1</label>
            <mixed-citation id="d253e325" publication-type="other">
World Health Organization (WHO). Control and surveillance of Afri-
can trypanosomiasis. WHO Tech Rep Ser 1998; 881:1-113.</mixed-citation>
         </ref>
         <ref id="d253e335a1310">
            <label>2</label>
            <mixed-citation id="d253e342" publication-type="other">
Kennedy PGE. Human African trypanosomiasis of the CNS: current
issues and challenges. J Clin Invest 2004; 113:496-504.</mixed-citation>
         </ref>
         <ref id="d253e352a1310">
            <label>3</label>
            <mixed-citation id="d253e359" publication-type="other">
Barrett MP, Burchmore RI, Stich A, et al. The trypanosomiases. Lancet
2003; 362:1469-80.</mixed-citation>
         </ref>
         <ref id="d253e369a1310">
            <label>4</label>
            <mixed-citation id="d253e376" publication-type="other">
Ormerod WE. Taxonomy of the sleeping sickness trypanosomes. J Para-
sitol 1967; 53:824-30.</mixed-citation>
         </ref>
         <ref id="d253e387a1310">
            <label>5</label>
            <mixed-citation id="d253e394" publication-type="other">
MacLean L, Chisi IE, Odiit M, et al. Severity of human african trypano-
somiasis in east Africa is associated with geographic location, parasite
genotype and host-inflammatory cytokine response profile. Infect Im-
mun 2004; 72:7040-4.</mixed-citation>
         </ref>
         <ref id="d253e410a1310">
            <label>6</label>
            <mixed-citation id="d253e417" publication-type="other">
Masocha W, Robertson B, Rottenberg ME, Mhlanga I, Sorokin L, Kris-
tensson K. Cerebral vessel laminins and IFN-gamma define Trypano-
soma brucei brucei penetration of the blood-brain barrier. J Clin Invest
2004; 114:689-94.</mixed-citation>
         </ref>
         <ref id="d253e433a1310">
            <label>7</label>
            <mixed-citation id="d253e440" publication-type="other">
Bancroft GJ, Sutton CJ, Morris AG, Askonas BA. Production of inter-
ferons during experimental African trypanosomiasis. Clin Exp Immu-
nol 1983; 52:135-43.</mixed-citation>
         </ref>
         <ref id="d253e453a1310">
            <label>8</label>
            <mixed-citation id="d253e460" publication-type="other">
Mabbott NA, Coulson PS, Smythies LE, Wilson RA, Sternberg IM. Af-
rican trypanosome infections in mice that lack the interferon-gamma
receptor gene: nitric oxide-dependent and-independent suppression of
T-cell proliferative responses and the development of anaemia. Immu-
nology 1998; 94:476-80.</mixed-citation>
         </ref>
         <ref id="d253e479a1310">
            <label>9</label>
            <mixed-citation id="d253e486" publication-type="other">
MacLean L, Odiit M, Okitoi D, Sternberg JM. Plasma nitrate and
interferon-gamma in Trypanosoma brucei rhodesiense infections: evi-
dence that nitric oxide production is induced during both early blood-
stage and late meningoencephalitic-stage infections. Trans R Soc Trop
Med Hyg 1999; 93:169-70.</mixed-citation>
         </ref>
         <ref id="d253e505a1310">
            <label>10</label>
            <mixed-citation id="d253e512" publication-type="other">
Sternberg IM, Rodgers J, Bradley B, Maclean L, Murray M, Kennedy PG.
Meningoencephalitic African trypanosomiasis: brain IL-10 and IL-6 are
associated with protection from neuro-inflammatory pathology. J Neu-
roimmunol 2005; 167:81-9.</mixed-citation>
         </ref>
         <ref id="d253e529a1310">
            <label>11</label>
            <mixed-citation id="d253e536" publication-type="other">
Fevre EM, Coleman PG, Odiit M, Magona JW, Welburn SC, Woolhouse
ME. The origins of a new Trypanosoma brucei rhodesiense sleeping sick-
ness outbreak in eastern Uganda. Lancet 2001; 358:625-8.</mixed-citation>
         </ref>
         <ref id="d253e549a1310">
            <label>12</label>
            <mixed-citation id="d253e556" publication-type="other">
Woo PT. Evaluation of the haematocrit centrifuge and other techniques
for the field diagnosis of human trypanosomiasis and filariasis. Acta
Trop 1971; 28:298-303.</mixed-citation>
         </ref>
         <ref id="d253e569a1310">
            <label>13</label>
            <mixed-citation id="d253e576" publication-type="other">
Teasdale G, Jennett B. Assessment of coma and impaired consciousness:
a practical scale. Lancet 1974; 2:81-4.</mixed-citation>
         </ref>
         <ref id="d253e586a1310">
            <label>14</label>
            <mixed-citation id="d253e593" publication-type="other">
Gordon RG Jr. Ethnologue: languages of the world. 15th ed. Dallas: SIL
International, 2005.</mixed-citation>
         </ref>
         <ref id="d253e603a1310">
            <label>15</label>
            <mixed-citation id="d253e610" publication-type="other">
Cattand P, Miezan BT, de Raadt P. Human African trypanosomiasis: use
of double centrifugation of cerebrospinal fluid to detect trypanosomes.
Bull World Health Organ 1988; 66:83-6.</mixed-citation>
         </ref>
         <ref id="d253e623a1310">
            <label>16</label>
            <mixed-citation id="d253e630" publication-type="other">
MacLean L, Odiit M, Sternberg JM. Nitric oxide and cytokine synthesis
in human African trypanosomiasis. J Infect Dis 2001; 184:1086-90.</mixed-citation>
         </ref>
         <ref id="d253e641a1310">
            <label>17</label>
            <mixed-citation id="d253e648" publication-type="other">
Morrison LJ, McCormick G, Sweeney L, et al. The use of multiple dis-
placement amplification to increase the detection and genotyping of
Trypanosoma species samples immobilised on FTA filters. Am I Trop
Med Hyg 2007; 76:1132-7.</mixed-citation>
         </ref>
         <ref id="d253e664a1310">
            <label>18</label>
            <mixed-citation id="d253e671" publication-type="other">
MacLeod A, Tweedie A, McLellan S, et al. The genetic map and compar-
ative analysis with the physical map of Trypanosoma brucei. Nucleic Ac-
ids Res 2005; 33:6688-93.</mixed-citation>
         </ref>
         <ref id="d253e684a1310">
            <label>19</label>
            <mixed-citation id="d253e691" publication-type="other">
MacLeod A, Turner CM, Tait A. A high level of mixed Trypanosoma
brucei infections in tsetse flies detected by three hypervariable minisat-
ellites. Mol Biochem Parasitol 1999; 102:237-48.</mixed-citation>
         </ref>
         <ref id="d253e704a1310">
            <label>20</label>
            <mixed-citation id="d253e711" publication-type="other">
MacLeod A, Tweedie A, Welburn SC, Maudlin I, Turner CM, Tait A.
Minisatellite marker analysis of Trypanosomna brucei: reconciliation of
clonal, panmictic, and epidemic population genetic structures. Proc
Natl Acad Sci USA 2000; 97:13442-7.</mixed-citation>
         </ref>
         <ref id="d253e727a1310">
            <label>21</label>
            <mixed-citation id="d253e734" publication-type="other">
Brzustowski J. Clustering Calculator, 2002. Available at: http://www2
.biology.ualberta.ca/jbrzusto/cluster.php. Accessed 1 February 2007</mixed-citation>
         </ref>
         <ref id="d253e744a1310">
            <label>22</label>
            <mixed-citation id="d253e751" publication-type="other">
Page RDM. Treeview: an application to display phylogenetic trees on
personal computers. Comp Apps Biosciences 1996; 12:357-8.</mixed-citation>
         </ref>
         <ref id="d253e762a1310">
            <label>23</label>
            <mixed-citation id="d253e769" publication-type="other">
Lewis PO, Zaykin D. GDA (genetic data analysis): computer program
for the analysis of allelic data. Available at: http://hydrodictyon
.eeb.uconn.edu/people/plewis/software.php. Accessed 1 February 2007.</mixed-citation>
         </ref>
         <ref id="d253e782a1310">
            <label>24</label>
            <mixed-citation id="d253e789" publication-type="other">
Nei M. Genetic distance between populations. Am Nat 1972; 106:283-292.</mixed-citation>
         </ref>
         <ref id="d253e796a1310">
            <label>25</label>
            <mixed-citation id="d253e803" publication-type="other">
Wright S. Evolution and genetics of populations. Vol 2. Chicago: Uni-
versity of Chicago Press, 1984.</mixed-citation>
         </ref>
         <ref id="d253e813a1310">
            <label>26</label>
            <mixed-citation id="d253e820" publication-type="other">
MacLeod A, Turner CM, Tait A. The detection of geographical substruc-
turing of Trypanosoma brucei populations by the analysis of minisatellite
polymorphisms. Parasitology 2001; 123:475-82.</mixed-citation>
         </ref>
         <ref id="d253e833a1310">
            <label>27</label>
            <mixed-citation id="d253e840" publication-type="other">
Magez S, Truyens C, Merimi M, et al. P75 tumor necrosis factor-recep-
tor shedding occurs as a protective host response during African trypan-
osomiasis. J Infect Dis 2004; 189:527-39.</mixed-citation>
         </ref>
         <ref id="d253e853a1310">
            <label>28</label>
            <mixed-citation id="d253e862" publication-type="other">
Lejon V, Lardon J, Kenis G, et al. Interleukin (IL)-6, IL-8 and IL-10 in
serum and CSF of Trypanosoma brucei gambiense sleeping sickness pa-
tients before and after treatment. Trans R Soc Trop Med Hyg 2002; 96:
329-33.</mixed-citation>
         </ref>
         <ref id="d253e879a1310">
            <label>29</label>
            <mixed-citation id="d253e886" publication-type="other">
Courtin D, Jamonneau V, Mathieu JF, et al. Comparison of cytokine
plasma levels in human African trypanosomiasis. Trop Med Int Health
2006; 11:647-53.</mixed-citation>
         </ref>
         <ref id="d253e899a1310">
            <label>30</label>
            <mixed-citation id="d253e906" publication-type="other">
Bakhiet M, Olsson T, van der Meide P, Kristensson K. Depletion of
CD8+ T cells suppresses growth of Trypanosoma brucei brucei and
interferon-gamma) production in infected rats. Clin Exp Immunol
1990; 81:195-9.</mixed-citation>
         </ref>
         <ref id="d253e922a1310">
            <label>31</label>
            <mixed-citation id="d253e929" publication-type="other">
Sternberg JM. Immunobiology of African trypanosomiasis. Chem Im-
munol 1998; 70:186-99.</mixed-citation>
         </ref>
         <ref id="d253e939a1310">
            <label>32</label>
            <mixed-citation id="d253e946" publication-type="other">
Schleifer KW, Filutowicz H, SchopfLR, Mansfield JM. Characterization
of T helper cell responses to the trypanosome variant surface glycopro-
tein. J Immunol 1993; 150:2910-9.</mixed-citation>
         </ref>
         <ref id="d253e959a1310">
            <label>33</label>
            <mixed-citation id="d253e966" publication-type="other">
Hill KIL, Hutchings NR, Grandgenett PM, Donelson JE. T lymphocyte-
triggering factor of african trypanosomes is associated with the flagellar
fraction of the cytoskeleton and represents a new family of proteins that
are present in several divergent eukaryotes. J Biol Chem 2000; 275:
39369-78.</mixed-citation>
         </ref>
         <ref id="d253e985a1310">
            <label>34</label>
            <mixed-citation id="d253e992" publication-type="other">
Sternberg J, McGuigan F. Nitric oxide mediates suppression of T cell
responses in murine Trypanosoma brucei infection. Eur J Immunol
1992; 22:2741-4.</mixed-citation>
         </ref>
         <ref id="d253e1006a1310">
            <label>35</label>
            <mixed-citation id="d253e1013" publication-type="other">
Schleifer KW, Mansfield JM. Suppressor macrophages in African try-
panosomiasis inhibit T cell proliferative responses by nitric oxide and
prostaglandins. J Immunol 1993; 151:5492-503.</mixed-citation>
         </ref>
         <ref id="d253e1026a1310">
            <label>36</label>
            <mixed-citation id="d253e1033" publication-type="other">
Hertz CJ, Filutowicz H, Mansfield JM. Resistance to the African try-
panosomes is IFN-gamma dependent. J Immunol 1998; 161:6775-83.</mixed-citation>
         </ref>
         <ref id="d253e1043a1310">
            <label>37</label>
            <mixed-citation id="d253e1050" publication-type="other">
Namangala B, Noel W, De Baetselier P, Brys L, Beschin A. Relative con-
tribution of interferon-gamma and interleukin-10 to resistance to mu-
rine African trypanosomosis. J Infect Dis 2001; 183:1794-800.</mixed-citation>
         </ref>
         <ref id="d253e1063a1310">
            <label>38</label>
            <mixed-citation id="d253e1070" publication-type="other">
Mansfield JM, Paulnock DM. Regulation of innate and acquired immu-
nity in African trypanosomiasis. Parasite Immunol 2005; 27:361-71.</mixed-citation>
         </ref>
         <ref id="d253e1080a1310">
            <label>39</label>
            <mixed-citation id="d253e1087" publication-type="other">
Hunter CA, Gow JW, Kennedy PG, Jennings FW, Murray M. Immuno-
pathology of experimental African sleeping sickness: detection of cyto-
kine mRNA in the brains of Trypanosomrna brucei brucei-infected mice.
Infect Immun 1991; 59:4636-40.</mixed-citation>
         </ref>
         <ref id="d253e1103a1310">
            <label>40</label>
            <mixed-citation id="d253e1110" publication-type="other">
Penkowa M, Giralt M, Lago N, et al. Astrocyte-targeted expression of
IL-6 protects the CNS against a focal brain injury. Exp Neuro1 2003; 181:
130-48.</mixed-citation>
         </ref>
         <ref id="d253e1124a1310">
            <label>41</label>
            <mixed-citation id="d253e1131" publication-type="other">
Eckersall PD, Gow JW, McComb C, et al. Cytokines and the acute phase
response in post-treatment reactive encephalopathy of Trypanosoma
brucei brucei infected mice. Parasitol Int 2001; 50:15-26.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
