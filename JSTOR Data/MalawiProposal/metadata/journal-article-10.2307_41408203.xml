<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">demography</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100446</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Demography</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Springer</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00703370</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15337790</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">41408203</article-id>
         <title-group>
            <article-title>On Nonstable and Stable Population Momentum</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Thomas J.</given-names>
                  <surname>Espenshade</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Analia S.</given-names>
                  <surname>Olgiati</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Simon A.</given-names>
                  <surname>Levin</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>11</month>
            <year>2011</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">48</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40068525</issue-id>
         <fpage>1581</fpage>
         <lpage>1599</lpage>
         <permissions>
            <copyright-statement>© 2011 Population Association of America</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/41408203"/>
         <abstract>
            <p>This article decomposes total population momentum into two constituent and multiplicative parts: "nonstable" momentum and "stable" momentum. Nonstable momentum depends on deviations between a population's current age distribution and its implied stable age distribution. Stable momentum is a function of deviations between a population's implied stable and stationary age distributions. In general, the factorization of total momentum into the product of nonstable and stable momentum is a very good approximation. The factorization is exact, however, when the current age distribution is stable or when observed fertility is already at replacement. We provide numerical illustrations by calculating nonstable, stable, and total momentum for 176 countries, the world, and its major regions. In short, the article brings together disparate strands of the population momentum literature and shows how the various kinds of momentum fit together into a single unifying framework.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d1002e204a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1002e211" publication-type="other">
Preston
1986;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1002e220" publication-type="other">
Schoen and Kim 1991</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1002e226" publication-type="other">
Guillot (2005)</mixed-citation>
            </p>
         </fn>
         <fn id="d1002e233a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1002e240" publication-type="other">
Vincent (1945)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1002e246" publication-type="other">
Keyfitz (1969, 1971).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1002e252" publication-type="other">
Frejka (1973).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1002e259" publication-type="other">
Li and
Tuljapurkar (1999, 2000)</mixed-citation>
            </p>
         </fn>
         <fn id="d1002e269a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1002e276" publication-type="other">
Kim and Schoen (1993)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1002e282" publication-type="other">
Kim and Schoen 1997;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1002e288" publication-type="other">
Kim et al. 1991</mixed-citation>
            </p>
         </fn>
         <fn id="d1002e295a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d1002e302" publication-type="other">
(United Nations 2007)</mixed-citation>
            </p>
         </fn>
         <fn id="d1002e310a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d1002e317" publication-type="other">
(United
Nations 2007).</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d1002e336a1310">
            <mixed-citation id="d1002e340" publication-type="other">
Bongaarts, J. (1994). Population policy options in the developing world. Science, 263, 771-776.</mixed-citation>
         </ref>
         <ref id="d1002e347a1310">
            <mixed-citation id="d1002e351" publication-type="other">
Bongaarts, J. (1999). Population momentum. In A. Mason, T. Merrick, &amp; R. P. Shaw (Eds.), Population
economics, demographic transition, and development: Research and policy implications (World Bank
Working Paper, pp. 3-15). Washington, DC: IBRD/World Bank.</mixed-citation>
         </ref>
         <ref id="d1002e364a1310">
            <mixed-citation id="d1002e368" publication-type="other">
Bongaarts, J. (2007, November). Population growth and policy options in the developing world. Paper
presented at the Beijing Forum, Beijing. New York: The Population Council.</mixed-citation>
         </ref>
         <ref id="d1002e378a1310">
            <mixed-citation id="d1002e382" publication-type="other">
Bongaarts, J., &amp; Bulatao, R. A. (1999). Completing the demographic transition. Population and
Development Review, 25, 515-529.</mixed-citation>
         </ref>
         <ref id="d1002e393a1310">
            <mixed-citation id="d1002e397" publication-type="other">
Bourgeois-Pichat, J. (1971). Stable, semi-stable populations and growth potential. Population Studies, 25,
235-254.</mixed-citation>
         </ref>
         <ref id="d1002e407a1310">
            <mixed-citation id="d1002e411" publication-type="other">
Carlson, E. (2008). The lucky few: Between the greatest generation and the baby boom. New York:
Springer Publishers.</mixed-citation>
         </ref>
         <ref id="d1002e421a1310">
            <mixed-citation id="d1002e425" publication-type="other">
Espenshade, T. J. (1975). The stable decomposition of the rate of natural increase. Theoretical Population
Biology, 8, 97-115.</mixed-citation>
         </ref>
         <ref id="d1002e435a1310">
            <mixed-citation id="d1002e439" publication-type="other">
Espenshade, T. J., &amp; Campbell, G. (1977). The stable equivalent population, age composition, and Fisher's
reproductive value function. Demography, 14, 77-86.</mixed-citation>
         </ref>
         <ref id="d1002e449a1310">
            <mixed-citation id="d1002e453" publication-type="other">
Feeney, G. (2003). Momentum of population growth. In P. Demeny &amp; G. McNicoll (Eds.), Encyclopedia
of Population (Vol. 2, pp. 646-649). New York: Macmillan Reference USA.</mixed-citation>
         </ref>
         <ref id="d1002e463a1310">
            <mixed-citation id="d1002e467" publication-type="other">
Finkbeiner, D. T. (1960). Introduction to matrices and linear transformations. San Francisco, CA: W.H.
Freeman and Company.</mixed-citation>
         </ref>
         <ref id="d1002e478a1310">
            <mixed-citation id="d1002e482" publication-type="other">
Fisher, R. A. (1930). The genetical theory of natural selection. Oxford, UK: Clarendon Press.</mixed-citation>
         </ref>
         <ref id="d1002e489a1310">
            <mixed-citation id="d1002e493" publication-type="other">
Frejka, T. (1973). The future of population growth: Alternative paths to equilibrium. New York: John
Wiley &amp; Sons.</mixed-citation>
         </ref>
         <ref id="d1002e503a1310">
            <mixed-citation id="d1002e507" publication-type="other">
Goldstein, J. R. (2002). Population momentum for gradual demographic transitions: An alternative
approach. Demography, 39, 65-73.</mixed-citation>
         </ref>
         <ref id="d1002e517a1310">
            <mixed-citation id="d1002e521" publication-type="other">
Goldstein, J. R., &amp; Stecklov, G. (2002). Long-range population projections made simple. Population and
Development Review, 28, 121-141.</mixed-citation>
         </ref>
         <ref id="d1002e531a1310">
            <mixed-citation id="d1002e535" publication-type="other">
Guillot, M. (2005). The momentum of mortality change. Population Studies, 59, 283-294.</mixed-citation>
         </ref>
         <ref id="d1002e542a1310">
            <mixed-citation id="d1002e546" publication-type="other">
Keyfitz, N. (1968). Introduction to the mathematics of population. Reading, MA: Addison-Wesley
Publishing Company.</mixed-citation>
         </ref>
         <ref id="d1002e557a1310">
            <mixed-citation id="d1002e561" publication-type="other">
Keyfitz, N. (1969). Age distribution and the stable equivalent. Demography, 6, 261-269.</mixed-citation>
         </ref>
         <ref id="d1002e568a1310">
            <mixed-citation id="d1002e572" publication-type="other">
Keyfitz, N. (1971). On the momentum of population growth. Demography, 8, 71-80.</mixed-citation>
         </ref>
         <ref id="d1002e579a1310">
            <mixed-citation id="d1002e583" publication-type="other">
Keyfitz, N. (1985). Applied mathematical demography (2nd ed.). New York: Springer-Verlag.</mixed-citation>
         </ref>
         <ref id="d1002e590a1310">
            <mixed-citation id="d1002e594" publication-type="other">
Kim, Y. J., &amp; Schoen, R. (1993). Crossovers that link populations with the same vital rates. Mathematical
Population Studies, 4, 1-19.</mixed-citation>
         </ref>
         <ref id="d1002e604a1310">
            <mixed-citation id="d1002e608" publication-type="other">
Kim, Y. J., &amp; Schoen, R. (1997). Population momentum expresses population aging. Demography, 34,
421-427.</mixed-citation>
         </ref>
         <ref id="d1002e618a1310">
            <mixed-citation id="d1002e622" publication-type="other">
Kim, Y. J., Schoen, R., &amp; Sarma, P. S. (1991). Momentum and the growth-free segment of a population.
Demography, 28, 159-173.</mixed-citation>
         </ref>
         <ref id="d1002e633a1310">
            <mixed-citation id="d1002e637" publication-type="other">
Knodel, J. (1999). Deconstructing population momentum. Population Today, 27(3), 1-2, 7.</mixed-citation>
         </ref>
         <ref id="d1002e644a1310">
            <mixed-citation id="d1002e648" publication-type="other">
Li, N., &amp; Tuljapurkar, S. (1999). Population momentum for gradual demographic transitions. Population
Studies, 53, 255-262.</mixed-citation>
         </ref>
         <ref id="d1002e658a1310">
            <mixed-citation id="d1002e662" publication-type="other">
Li, N., &amp; Tuljapurkar, S. (2000). The solution of time-dependent population models. Mathematical
Population Studies, 7, 311-329.</mixed-citation>
         </ref>
         <ref id="d1002e672a1310">
            <mixed-citation id="d1002e676" publication-type="other">
O'Neill, B. C., Scherbov, S., &amp; Lutz, W. (1999). The long-term effect of the timing of the fertility decline
on population size. Population and Development Review, 25, 749-756.</mixed-citation>
         </ref>
         <ref id="d1002e686a1310">
            <mixed-citation id="d1002e690" publication-type="other">
Preston, S. H. (1986). The relation between actual and intrinsic growth rates. Population Studies,
40, 343-351.</mixed-citation>
         </ref>
         <ref id="d1002e700a1310">
            <mixed-citation id="d1002e704" publication-type="other">
Preston, S. H., &amp; Guillot, M. (1997). Population dynamics in an age of declining fertility. Genus, 53(3-4),
15-31.</mixed-citation>
         </ref>
         <ref id="d1002e715a1310">
            <mixed-citation id="d1002e719" publication-type="other">
Schoen, R., &amp; Jonsson, S. H. (2003). Modeling momentum in gradual demographic transitions.
Demography, 40, 621-635.</mixed-citation>
         </ref>
         <ref id="d1002e729a1310">
            <mixed-citation id="d1002e733" publication-type="other">
Schoen, R., &amp; Kim, Y. J. (1991). Movement toward stability as a fundamental principle of population
dynamics. Demography, 28, 455-466.</mixed-citation>
         </ref>
         <ref id="d1002e743a1310">
            <mixed-citation id="d1002e747" publication-type="other">
Schoen, R., &amp; Kim, Y. J. (1998). Momentum under a gradual approach to zero growth. Population Studies,
52, 295-299.</mixed-citation>
         </ref>
         <ref id="d1002e757a1310">
            <mixed-citation id="d1002e761" publication-type="other">
United Nations. (2007). World population prospects: The 2006 revision, CD-ROM edition (United Nations
publication, Extended Dataset, Sales No. E.07.XIII.7), New York: United Nations.</mixed-citation>
         </ref>
         <ref id="d1002e771a1310">
            <mixed-citation id="d1002e775" publication-type="other">
Vincent, P. (1945). Potentiel d'accroissement d'une population [Growth potential of a population]. Journal
de la Société de Statistique de Paris, lre Série, Janvier-Février, 86(1-2), 16-39.</mixed-citation>
         </ref>
         <ref id="d1002e785a1310">
            <mixed-citation id="d1002e789" publication-type="other">
World Health Organization. (2008). Life tables for WHO member states [Machine-readable database].
Retrieved from http://www.who.int/whosis/database/life_tables/life_tables.cfin</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
