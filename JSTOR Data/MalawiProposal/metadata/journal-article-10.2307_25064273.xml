<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">systematicbotany</journal-id>
         <journal-id journal-id-type="jstor">j100680</journal-id>
         <journal-title-group>
            <journal-title>Systematic Botany</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>American Society of Plant Taxonomists</publisher-name>
         </publisher>
         <issn pub-type="ppub">03636445</issn>
         <issn pub-type="epub">15482324</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">25064273</article-id>
         <title-group>
            <article-title>Evolutionary Relationships within the Species-Rich Genus Ruellia (Acanthaceae)</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Erin A.</given-names>
                  <surname>Tripp</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>7</month>
            <year>2007</year>
         
            <day>1</day>
            <month>9</month>
            <year>2007</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">32</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id>i25064259</issue-id>
         <fpage>628</fpage>
         <lpage>649</lpage>
         <permissions>
            <copyright-statement>Copyright 2007 The American Society of Plant Taxonomists</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/25064273"/>
         <abstract>
            <p>Phylogenetic relationships among species of the large genus Ruellia (Acanthaceae) have never been studied. Ruellia, with approximately 300 species, is geographically widespread and morphologically diverse. Molecular data for 196 specimens from the nuclear ribosomal ITS region and the chloroplast trnG-trnR region were used to test monophyly of the genus against closely related genera in Ruellieae, to reconstruct phylogenetic relationships among species of Ruellia on a global scale, to re-evaluate previous morphology-based classifications, and to examine the utility of morphological characters, especially corolla morphology, for future sectional delimitation. Bayesian and parsimony analyses indicate four genera are evolutionarily allied to Ruellia. Acanthopale is sister to Ruellia s. l. with strong support. Blechum, Eusiphon, and Polylychnis are nested within Ruellia s. l., and species in Eusiphon and Polylychnis are here formally transferred to Ruellia resulting in the new combinations Ruellia geayi and Ruellia fulgens. Ruellia s. l., including Blechum, Eusiphon, and Polylychnis, is monophyletic but only weakly supported by parsimony. Within Ruellia, Old World taxa form a basal grade and New World taxa are monophyletic and nested within the Old World grade. Alternative hypotheses involving non-monophyly of New World Ruellia were significantly less parsimonious and less likely. Within New World Ruellia, many clades are informally recognized, several of which reflect previous taxonomic groupings to some extent. Constraining all putatively hummingbird-pollinated taxa to monophyly was strongly rejected. This suggests that corolla morphology has undergone convergent evolution and is therefore likely an inappropriate character for sectional delimitation, contrary to previous use.</p>
         </abstract>
         <kwd-group>
            <kwd>Asteroid</kwd>
            <kwd>Flower</kwd>
            <kwd>Hummingbird</kwd>
            <kwd>ITS</kwd>
            <kwd>Lamiales</kwd>
            <kwd>Phylogeny</kwd>
            <kwd>Ruellieae</kwd>
            <kwd>trnG-trnR</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Literature Cited</title>
         <ref id="d594e193a1310">
            <mixed-citation id="d594e197" publication-type="other">
Andersson, S. 2006. On the phylogeny of the genus
Calceolaria (Calceolariaceae) as inferred from ITS and
plastid matK sequences. Taxon 55: 125-137.</mixed-citation>
         </ref>
         <ref id="d594e210a1310">
            <mixed-citation id="d594e214" publication-type="other">
Barber, J. C., J. Francisco-Ortega, A. Santos-Guerra, K. G.
Turner, and R. K. Jansen. 2002. Origin of Macaronesian
Sideritis L. (Lamioideae: Lamiaceae) inferred from
nuclear and chloroplast sequence datasets. Molecular
Phylogenetics and Evolution 23: 293-306.</mixed-citation>
         </ref>
         <ref id="d594e233a1310">
            <mixed-citation id="d594e237" publication-type="other">
Benoist, R. 1939. Nouvelles Acanthacées Malgaches. Pp. 144
in Notulae Systematicae, vol. 8, ed. H. Humbert.
Contribution a l'etude de la flore de Madagascar et des
Comores. Fasc. 3. Paris.</mixed-citation>
         </ref>
         <ref id="d594e253a1310">
            <mixed-citation id="d594e257" publication-type="other">
-. 1967. Famille 182 — Acanthacées. In Flore de
Madagascar et des Comores, eds. H. Humbert and Tome
I. Mus. Nat. Hist. Nat.: Paris.</mixed-citation>
         </ref>
         <ref id="d594e271a1310">
            <mixed-citation id="d594e275" publication-type="other">
Bentham, G. and J. D. Hooker. 1876. Acanthaceae. Pp. 1060-
1122 in General Plantarum vol. 2. London.</mixed-citation>
         </ref>
         <ref id="d594e285a1310">
            <mixed-citation id="d594e289" publication-type="other">
Boggan, J., V. Funk, and C. Kelloff. 1997. Checklist of the
Plants of the Guianas (Guyana, Surinam, French
Guiana), Ed. 2, Smithsonian Institution. Pub. #30.
Biological Diversity of the Guianas Program publication
series: Washington, D. C.</mixed-citation>
         </ref>
         <ref id="d594e308a1310">
            <mixed-citation id="d594e312" publication-type="other">
Bremekamp, C. E. B. 1938a. Acanthaceae. Pp. 166-252 in Flora
of Surinam vol.4, ed. A. Pulle. Amsterdam: J. H. de
Bussy, Ltd.</mixed-citation>
         </ref>
         <ref id="d594e325a1310">
            <mixed-citation id="d594e329" publication-type="other">
-. 1938b. Recueil des Travaux Botaniques Néerlandais.
Mededeelingen van het Botanisch Museum en Herbarium van
de Rijks Universiteit te Utrect 45: 160.</mixed-citation>
         </ref>
         <ref id="d594e342a1310">
            <mixed-citation id="d594e346" publication-type="other">
-. 1943. Ueber Dischistocalyx T. And. ex Bth. und
Acanthopale C.B. Clarke (Acanthaceae). Botanisch Jahrbü-
cher für Systematik 73: 126-150.</mixed-citation>
         </ref>
         <ref id="d594e359a1310">
            <mixed-citation id="d594e365" publication-type="other">
- and N. E. N. Bremekamp. 1948. A preliminary survey
of the Ruelliinae of the Malay Archipelago and New
Guinea. Verhandelingen der Koninklijke Nederlandse Aka-
demie van Wetenschappen, Afd. Natuurkunde. Tweede sect
45: 1-39.</mixed-citation>
         </ref>
         <ref id="d594e385a1310">
            <mixed-citation id="d594e389" publication-type="other">
-. 1969. An annotated list of the Acanthaceae collected
by Miss W.M.A. Brooke on her travels to Bolivia.
Proceedings of the Koninklijke Nederlandse Akademie van
Wetenschappen, Series C 72: 420-430.</mixed-citation>
         </ref>
         <ref id="d594e405a1310">
            <mixed-citation id="d594e409" publication-type="other">
Bremer, K. 1988. The limits of amino acid sequence data in
angiosperm phylogenetic reconstructions. Evolution 42:
795-803.</mixed-citation>
         </ref>
         <ref id="d594e422a1310">
            <mixed-citation id="d594e426" publication-type="other">
Browne, P. 1756. In: The Civil and Natural History of Jamaica in
Three Parts. London.</mixed-citation>
         </ref>
         <ref id="d594e436a1310">
            <mixed-citation id="d594e440" publication-type="other">
Carvalheira, G. M. G., M. Guerra, G. A. dos Santos, V. C.
de Andrade, and M. C. A. de Farias. 1991. Citogenética
de angiospermas coletadas em Pernambuco — IV. Acta
Botanica Brasilica 5: 37-51.</mixed-citation>
         </ref>
         <ref id="d594e456a1310">
            <mixed-citation id="d594e460" publication-type="other">
Chávez, P. R. 1974. Observaciones en el polen de plantas con
probable polinización quiropterofila. Anales de la Escuela
Nacional de Ciencias Biológicas 21: 115-143.</mixed-citation>
         </ref>
         <ref id="d594e473a1310">
            <mixed-citation id="d594e477" publication-type="other">
Clarke, J. L. and E. A. Zimmer. 2003. A preliminary
phylogeny of Alloplectus (Gesneriaceae): implications
for the evolution of flower resupination. Systematic
Botany 28: 365-375.</mixed-citation>
         </ref>
         <ref id="d594e494a1310">
            <mixed-citation id="d594e498" publication-type="other">
Clarke, C. B. 1908. Flora of the Malayan Peninsula. Family
LXXXIX—Acanthaceae. Journal of the Asiatic Society of
Bengal. LXXIV 2: 658-659.</mixed-citation>
         </ref>
         <ref id="d594e511a1310">
            <mixed-citation id="d594e515" publication-type="other">
Daniel, T. D., B. D. Parfitt, and M. A. Baker. 1984.
Chromosome numbers and their systematic implications
in some North American Acanthaceae. Systematic Botany
9: 346-355.</mixed-citation>
         </ref>
         <ref id="d594e531a1310">
            <mixed-citation id="d594e535" publication-type="other">
-, T. I. Chuang, and -. 1990a. Chromosome
numbers of American Acanthaceae. Systematic Botany
15: 13-25.</mixed-citation>
         </ref>
         <ref id="d594e548a1310">
            <mixed-citation id="d594e552" publication-type="other">
-. 1990b. New, reconsidered, and little-known Mexican
species of Ruellia (Acanthaceae). Contributions from the
University of Michigan Herbarium 17: 139-162.</mixed-citation>
         </ref>
         <ref id="d594e565a1310">
            <mixed-citation id="d594e569" publication-type="other">
- and T. I. Chuang. 1993. Chromosome numbers of
New World Acanthaceae. Systematic Botany 18: 283-289.</mixed-citation>
         </ref>
         <ref id="d594e579a1310">
            <mixed-citation id="d594e583" publication-type="other">
-. 1995a. Acanthaceae. Pp. 1-158 in Flora of Chiapas, pt.
4, ed. D. Breedlove. San Francisco: California Academy
of Sciences.</mixed-citation>
         </ref>
         <ref id="d594e597a1310">
            <mixed-citation id="d594e601" publication-type="other">
-. 1995b. New and reconsidered Mexican Acanthaceae.
VI. Chiapas. Proceedings of the California Academy of
Sciences 48: 253-282.</mixed-citation>
         </ref>
         <ref id="d594e614a1310">
            <mixed-citation id="d594e618" publication-type="other">
-. 1998. Pollen morphology of Mexican Acanthaceae:
diversity and systematic significance. Proceedings of the
California Academy of Sciences 50: 217-256.</mixed-citation>
         </ref>
         <ref id="d594e631a1310">
            <mixed-citation id="d594e635" publication-type="other">
Doyle, J. J. and J. L. Doyle. 1987. A rapid isolation procedure
for small quantities of fresh leaf tissue. Phytochemical
Bulletin 19: 11-15.</mixed-citation>
         </ref>
         <ref id="d594e648a1310">
            <mixed-citation id="d594e652" publication-type="other">
Ezcurra, C. 1989. Ruellia sanguinea (Acanthaceae) y especies
relacionadas en Argentina, Uruguay y sur de Brasil.
Darwiniana 29: 269-287.</mixed-citation>
         </ref>
         <ref id="d594e665a1310">
            <mixed-citation id="d594e669" publication-type="other">
- and D. de Azkue. 1989. Validation and genetic and
morphological relationships of Ruellia macrosolen
(Acanthaceae) from southern South America. Systematic
Botany 14: 297-303.</mixed-citation>
         </ref>
         <ref id="d594e685a1310">
            <mixed-citation id="d594e689" publication-type="other">
-. 1993. Systematics of Ruellia (Acanthaceae) in south-
ern South America. Annals of the Missouri Botanical
Garden 80: 787-845.</mixed-citation>
         </ref>
         <ref id="d594e703a1310">
            <mixed-citation id="d594e707" publication-type="other">
Fedorov, A. A. 1969. Chromosome numbers of flowering plants.
Leningrad: Academy of Sciences of the U.S.S.R.</mixed-citation>
         </ref>
         <ref id="d594e717a1310">
            <mixed-citation id="d594e721" publication-type="other">
Fernald, M. L. 1945. Ruellia in the eastern United States.
Rhodora 47: 1-38, 47-63, 69-90.</mixed-citation>
         </ref>
         <ref id="d594e731a1310">
            <mixed-citation id="d594e735" publication-type="other">
Freeman, C. E. 1986. Nectar-sugar composition in an
individual of Ruellia peninsularis (Acanthaceae). Madroño
33: 300-302.</mixed-citation>
         </ref>
         <ref id="d594e748a1310">
            <mixed-citation id="d594e754" publication-type="other">
Gibbs Russell, G. E., W. G. Welman, E. Reitief, K. L.
Immelman, G. Germishuizen, B. J. Pienaar, M. van
Wyk, and A. Nicholas. 1987. List of species of southern
African plants. Memoirs of the Botanical Survey of South
Africa 56, ed. 2, pt. 2.</mixed-citation>
         </ref>
         <ref id="d594e773a1310">
            <mixed-citation id="d594e777" publication-type="other">
Gracie, C. 1991. Observation of dual function of nectarines in
Ruellia radicans (Nees) Lindau (Acanthaceae). Bulletin of
the Torrey Botanical Club 118: 188-190.</mixed-citation>
         </ref>
         <ref id="d594e790a1310">
            <mixed-citation id="d594e794" publication-type="other">
Grant, W. F. 1955. A cytogenetic study of Acanthaceae of
Thailand. Brittonia 8: 121-149.</mixed-citation>
         </ref>
         <ref id="d594e805a1310">
            <mixed-citation id="d594e809" publication-type="other">
Gray, A. 1878. Synoptical Flora of North America, part II. New
York.</mixed-citation>
         </ref>
         <ref id="d594e819a1310">
            <mixed-citation id="d594e823" publication-type="other">
Grubert, M. 1974. Studies on the distribution of myxospermy
among seeds and fruits of Angiospermae and its
ecological importance. Acta Biologica Venezuelica 8:
315-551.</mixed-citation>
         </ref>
         <ref id="d594e839a1310">
            <mixed-citation id="d594e843" publication-type="other">
Hedrén, M., M. W. Chase, and R. G. Olmstead. 1995.
Relationships in the Acanthaceae and related families as
suggested by cladistic analysis of rbcL nucleotide
sequences. Plant Systematics and Evolution 194: 93-109.</mixed-citation>
         </ref>
         <ref id="d594e859a1310">
            <mixed-citation id="d594e863" publication-type="other">
Huelsenbeck, J. P. and F. Ronquist. 2001. MrBayes: Bayesian
inference of phylogenetic trees. Bioinformatics 17:
754-755.</mixed-citation>
         </ref>
         <ref id="d594e876a1310">
            <mixed-citation id="d594e880" publication-type="other">
Jamzad, Z., M. W. Chase, M. Ingrouille, M. S. J. Simmonds,
and A. Jalili. Phylogenetic relationships in Nepeta L.
(Lamiaceae) and related genera based on ITS sequence
data. Taxon 52: 21-32.</mixed-citation>
         </ref>
         <ref id="d594e896a1310">
            <mixed-citation id="d594e900" publication-type="other">
Kiel, C. A., L. A. McDade, T. F. Daniel, and D. Champlu-
vier. 2006. Phylogenetic delimitation of Isoglossinae
(Acanthaceae: Justicieae) and relationships among con-
stituent genera. Taxon 55: 683-693.</mixed-citation>
         </ref>
         <ref id="d594e917a1310">
            <mixed-citation id="d594e921" publication-type="other">
Leonard, E. C. 1938. Contributions to the flora of tropical
America: XXXIV. Plantae Hintonianae: VI. Kew Bulletin,
59-60.</mixed-citation>
         </ref>
         <ref id="d594e934a1310">
            <mixed-citation id="d594e938" publication-type="other">
-. 1951-1958. The Acanthaceae of Colombia Contributions
from the U.S. National Herbarium 31: 66-109, 684-696.</mixed-citation>
         </ref>
         <ref id="d594e948a1310">
            <mixed-citation id="d594e952" publication-type="other">
Lima, N. A. de S., M. F. Vieria, R. M. de Carvalho-Okano,
and A. A. Azevedo. 2005. Cleistogamia em Ruellia
menthoides (Nees) Hiern e R. brevifolia (Pohl) C. Ezcurra
(Acanthaceae) em fragmento florestal do Sudeste brasi-
leiro. Acta Botanica Brasilica 19: 443-449.</mixed-citation>
         </ref>
         <ref id="d594e971a1310">
            <mixed-citation id="d594e975" publication-type="other">
Lindau, G. 1895. Acanthaceae. Pp. 274-354 in Die Natürlichen
Pflanzenfamilien, vol. 4, eds. A. Engler and H. Prantl.
Leipzig: W. Engelmann.</mixed-citation>
         </ref>
         <ref id="d594e988a1310">
            <mixed-citation id="d594e992" publication-type="other">
Linnaeus, C. 1737. Genera Plantarum Conradum Wishoff.
Leiden.</mixed-citation>
         </ref>
         <ref id="d594e1002a1310">
            <mixed-citation id="d594e1006" publication-type="other">
-. 1753. Species Plantarum, ed. 1. Stockholm: Laurentii
Salvii.</mixed-citation>
         </ref>
         <ref id="d594e1017a1310">
            <mixed-citation id="d594e1021" publication-type="other">
Long, R. W. 1964. Biosystematic investigations in south
Florida populations of Ruellia (Acanthaceae). American
Journal of Botany 51: 842-852.</mixed-citation>
         </ref>
         <ref id="d594e1034a1310">
            <mixed-citation id="d594e1038" publication-type="other">
-. 1970. The genera of Acanthaceae in the southeastern
United States. Journal of the Arnold Arboretum 51:
257-309.</mixed-citation>
         </ref>
         <ref id="d594e1051a1310">
            <mixed-citation id="d594e1055" publication-type="other">
-. 1973. A biosystematic approach to generic delimita-
tion in Ruellia (Acanthaceae). Taxon 22; 543-555.</mixed-citation>
         </ref>
         <ref id="d594e1065a1310">
            <mixed-citation id="d594e1069" publication-type="other">
-. 1975. Artificial interspecific hybridization in temper-
ate and tropical species of Ruellia (Acanthaceae).
Brittonia 27: 289-296.</mixed-citation>
         </ref>
         <ref id="d594e1082a1310">
            <mixed-citation id="d594e1086" publication-type="other">
-. 1977. Artificial induction of obligate cleistogamy in
species hybrids in Ruellia (Acanthaceae). Bulletin of the
Forrey Botanical Club 104: 53-56.</mixed-citation>
         </ref>
         <ref id="d594e1099a1310">
            <mixed-citation id="d594e1103" publication-type="other">
Machado, I. C. E M. Sazima. 1995. Biologia da polinização e
pilhagem por beija-flores em Ruellia asperula Lindau
(Acanthaceae) na caatinga, nordeste brasileiro. Revista
Brasileira de Botânica 18: 27-33.</mixed-citation>
         </ref>
         <ref id="d594e1120a1310">
            <mixed-citation id="d594e1124" publication-type="other">
- and A. V. Lopes. 2004. Floral traits and pollination
systems in the Caatinga, a Brazilian tropical dry forest.
Annals of Botany 94: 365-376.</mixed-citation>
         </ref>
         <ref id="d594e1137a1310">
            <mixed-citation id="d594e1143" publication-type="other">
Maddison, D. R. and W. P. Maddison. 2003. MacClade:
analysis of phylogeny and character evolution. Version
4.06. Sunderland: Sinauer Associates.</mixed-citation>
         </ref>
         <ref id="d594e1156a1310">
            <mixed-citation id="d594e1160" publication-type="other">
Manktelow, M. 2000. The filament curtain: a structure
important to systematics and pollination biology in the
Acanthaceae. Botanical Journal of the Linnean Society 133:
129-160.</mixed-citation>
         </ref>
         <ref id="d594e1176a1310">
            <mixed-citation id="d594e1180" publication-type="other">
-, L. A. McDade, B. Oxelman, C. A. Furness, and M. J.
Balkwill. 2001. The enigmatic tribe Whitfieldieae
(Acanthaceae): delimitation and phylogenetic relation-
ships based on molecular and morphological data.
Systematic Botany 26: 104-119.</mixed-citation>
         </ref>
         <ref id="d594e1199a1310">
            <mixed-citation id="d594e1203" publication-type="other">
Manos, P. S., J. J. Doyle, and K. C. Nixon. 1999. Phylogeny,
biogeography, and processes of molecular differentia-
tion in Quercus subgenus Quercus (Fagaceae). Molecular
Phylogenetics and Evolution 12: 333-349.</mixed-citation>
         </ref>
         <ref id="d594e1219a1310">
            <mixed-citation id="d594e1223" publication-type="other">
Mason-Gamer, R. and E. Kellogg. 1996. Testing for
phylogenetic conflict among molecular datasets in the
tribe Triticeae (Gramineae). Systematic Botany 45:
524-545.</mixed-citation>
         </ref>
         <ref id="d594e1240a1310">
            <mixed-citation id="d594e1244" publication-type="other">
McDade, L. A. and M. L. Moody. 1999. Phylogenetic
relationships among Acanthaceae: evidence from non-
coding trnL-trnF chloroplast DNA sequences. American
Journal of Botany 86: 70-80.</mixed-citation>
         </ref>
         <ref id="d594e1260a1310">
            <mixed-citation id="d594e1264" publication-type="other">
-, S. E. Masta, M. L. Moody, and E. Waters. 2000a.
Phylogenetic relationships among Acanthaceae: evi-
dence from two genomes. Systematic Botany 25: 106-121.</mixed-citation>
         </ref>
         <ref id="d594e1277a1310">
            <mixed-citation id="d594e1281" publication-type="other">
-, T. F. Daniel, S. E. Masta, and K. M. Riley. 2000b.
Phylogenetic relationships within the tribe Justicieae
(Acanthaceae): evidence from molecular sequences,
morphology, and cytology. Annals of the Missouri
Botanical Garden 87: 435-458.</mixed-citation>
         </ref>
         <ref id="d594e1300a1310">
            <mixed-citation id="d594e1304" publication-type="other">
-, -, C. A. Kiel, and K. Vollesen. 2005. Phyloge-
netic relationships among Acantheae (Acanthaceae):
major lineages present contrasting patterns of molecular
evolution and morphological differentiation. Systematic
Botany 30: 834-862.</mixed-citation>
         </ref>
         <ref id="d594e1323a1310">
            <mixed-citation id="d594e1327" publication-type="other">
- and E. A. Tripp. In Press. Synopsis of Ruellia L.
(Acanthaceae) in Costa Rica, with descriptions of four
new species. Brittonia.</mixed-citation>
         </ref>
         <ref id="d594e1340a1310">
            <mixed-citation id="d594e1344" publication-type="other">
Moylan, E. C., J. R. Bennett, M. A. Carine, R. G. Olmstead,
and R. W. Scotland. 2004a. Phylogenetic relationships
among Strobilanthes s. 1. (Acanthaceae): evidence from
ITS nrDNA, trnL-F cpDNA, and morphology. American
Journal of Botany 91: 724-735.</mixed-citation>
         </ref>
         <ref id="d594e1364a1310">
            <mixed-citation id="d594e1368" publication-type="other">
-, P. J. Rudall, and R. W. Scotland. 2004b. Compar-
ative floral anatomy of Strobilanthinae (Acanthaceae),
with particular reference to internal partitioning of the
flower. Plant Systematics and Evolution 249: 77-98.</mixed-citation>
         </ref>
         <ref id="d594e1384a1310">
            <mixed-citation id="d594e1388" publication-type="other">
Nagalingum, N. S., H. Schneider, and K. M. Pryer. In Press.
Molecular phylogenetic relationships and morphologi-
cal evolution in the heterosporous fern genus Marsilea.
Systematic Botany.</mixed-citation>
         </ref>
         <ref id="d594e1404a1310">
            <mixed-citation id="d594e1408" publication-type="other">
Nees, C. G. 1847a. Acanthaceae. Pp. 1-164 in Flora Brasiliensis
vol. 9, ed. C. Martius. München.</mixed-citation>
         </ref>
         <ref id="d594e1418a1310">
            <mixed-citation id="d594e1422" publication-type="other">
-. 1847b. Acanthaceae. Pp. 46-519 in Prodromus System-
atis Naturalis. Regni Vegetabilis, vol. 11, ed. A. P. de
Candolle. Paris: Treuttel and Wurtz.</mixed-citation>
         </ref>
         <ref id="d594e1435a1310">
            <mixed-citation id="d594e1439" publication-type="other">
Oersted, A. S. 1854. Mexicos og Centralamerikas Acantha-
ceer. Videnskabelige Meddelelser Fra Dansk Naturhistorisk
Forening I Kjobenhavn 1854: 113-181.</mixed-citation>
         </ref>
         <ref id="d594e1452a1310">
            <mixed-citation id="d594e1456" publication-type="other">
Pelser, P. B., B. Gravendeel, and R. van der meijden. 2002.
Tackling speciose genera: species composition and
phylogenetic position of Senecio sect. Jacobaea (Astera-
ceae) based on plastid and nrDNA sequences. American
Journal of Botany 89: 929-939.</mixed-citation>
         </ref>
         <ref id="d594e1476a1310">
            <mixed-citation id="d594e1480" publication-type="other">
Posada, D. and K. A. Crandall. 1998. Modeltest: testing the
model of DNA substitution. Bioinformatics 14: 817-818.</mixed-citation>
         </ref>
         <ref id="d594e1490a1310">
            <mixed-citation id="d594e1494" publication-type="other">
Raj, B. 1961. Pollen morphological studies in the Acantha-
ceae. Grana Palynologica 3: 3-108.</mixed-citation>
         </ref>
         <ref id="d594e1504a1310">
            <mixed-citation id="d594e1508" publication-type="other">
Ramamoorthy, T. P. 1988. A new species of Ruellia
(Acanthaceae) from western Mexico. Annals of the
Missouri Botanical Garden 75: 1664-1665.</mixed-citation>
         </ref>
         <ref id="d594e1521a1310">
            <mixed-citation id="d594e1525" publication-type="other">
-. 1991. Ruellia section Chiropterophila (Acanthaceae): A
novelty from Mexico. Botanical Journal of the Linnaean
Society 107: 79-88.</mixed-citation>
         </ref>
         <ref id="d594e1538a1310">
            <mixed-citation id="d594e1542" publication-type="other">
-. 1992. Ruellia sect. Urceolata (Acanthaceae), a novel
species complex from southern Mexico. Plant Systematics
and Evolution 180: 221-225.</mixed-citation>
         </ref>
         <ref id="d594e1555a1310">
            <mixed-citation id="d594e1559" publication-type="other">
Richardson, J. E., R. T. Pennington, T. D. Pennington, and
P. M. Hollingsworth. 2001. Rapid diversification of
a species-rich genus of neotropical rain forest trees.
Science 293: 5538.</mixed-citation>
         </ref>
         <ref id="d594e1576a1310">
            <mixed-citation id="d594e1580" publication-type="other">
Schmidt-Lebuhn, A. N., M. Kessler, and J. Müller. 2005.
Evolution of Suessenguthia (Acanthaceae) inferred from
morphology, AFLP data, and ITS rDNA sequences.
Organisms, Diversity, &amp;amp; Evolution 5: 1-13.</mixed-citation>
         </ref>
         <ref id="d594e1596a1310">
            <mixed-citation id="d594e1600" publication-type="other">
Scotland, R. W., P. K. Endress, and T. J. Lawrence. 1994.
Corolla ontogeny and aestivation in the Acanthaceae.
Botanical Journal of the Linnean Society 114: 49-65.</mixed-citation>
         </ref>
         <ref id="d594e1613a1310">
            <mixed-citation id="d594e1617" publication-type="other">
-, J. A. Sweere, P. A. Reeves, and R. G. Olmstead. 1995.
Higher-level systematics of Acanthaceae determined by
chloroplast DNA sequences. American Journal of Botany
82: 266-275.</mixed-citation>
         </ref>
         <ref id="d594e1633a1310">
            <mixed-citation id="d594e1637" publication-type="other">
- and K. Vollesen. 2000. Classification of Acanthaceae.
Kew Bulletin 55: 513-589.</mixed-citation>
         </ref>
         <ref id="d594e1647a1310">
            <mixed-citation id="d594e1651" publication-type="other">
Sigrist, M. R. e M. Sazima. 2002. Ruellia brevifolia (Pohl)
Ezcurra (Acanthaceae): fenologia da floração, biologia
da polinização e reprodução. Revista Brasileira de Botânica
25: 35-42.</mixed-citation>
         </ref>
         <ref id="d594e1667a1310">
            <mixed-citation id="d594e1671" publication-type="other">
Swofford, D. L. 2002. PAUP*. Phylogenetic Analysis Using
Parsimony (*and Other Methods). Version 4. Sunder-
land: Sinauer Associates.</mixed-citation>
         </ref>
         <ref id="d594e1685a1310">
            <mixed-citation id="d594e1689" publication-type="other">
Tharp, B. C. and F. A. Barkley. 1949. The genus Ruellia in
Texas. American Midland Naturalist 42: 1-86.</mixed-citation>
         </ref>
         <ref id="d594e1699a1310">
            <mixed-citation id="d594e1703" publication-type="other">
Tripp, E. A. 2005. The current status of Ruellia (Acanthaceae)
in Pennsylvania: two endangered / threatened species.
Bartonia 62: 55-62.</mixed-citation>
         </ref>
         <ref id="d594e1716a1310">
            <mixed-citation id="d594e1720" publication-type="other">
Vogel, S., I. C. MAChADO, and A. V. Lopes. 2004. Harpochilus
neesianus and other novel cases of chiropterophily in
neotropical Acanthaceae. Taxon 53: 55-60.</mixed-citation>
         </ref>
         <ref id="d594e1733a1310">
            <mixed-citation id="d594e1737" publication-type="other">
Wasshausen, D. C. 1995. Acanthaceae. Pp. 335-374 in Flora of
the Venezuelan Guayana, vol.2, eds. P. E. Berry, B. K.
Hoist, and K. Yatskievych. Portland: Timber Press.</mixed-citation>
         </ref>
         <ref id="d594e1750a1310">
            <mixed-citation id="d594e1754" publication-type="other">
- and J. R. I. Wood. 2003. Note on the genus Ruellia
(Acanthaceae) in Bolivia, Peru, and Brazil. Proceedings of
the Biological Society of Washington 116: 263-274.</mixed-citation>
         </ref>
         <ref id="d594e1767a1310">
            <mixed-citation id="d594e1771" publication-type="other">
-. 2006. Family 156. Acanthaceae. Pp. 1-141 in Flora of
the Guianas. Series A: Phanerogams. Fascicle 23. Royal
Botanic Gardens: Kew, England.</mixed-citation>
         </ref>
         <ref id="d594e1785a1310">
            <mixed-citation id="d594e1789" publication-type="other">
- and J. R. I. Wood. 2006. Acanthaceae of Bolivia.
Contributions from the United States National Herbarium 49:
1-152.</mixed-citation>
         </ref>
         <ref id="d594e1802a1310">
            <mixed-citation id="d594e1806" publication-type="other">
White, T. J., T. Bruns, S. Lee, and J. Taylor. 1990. Amplification
and direct sequencing of fungal ribosomal RNA genes for
phylogenetics. Pp. 315-322 in PCR protocols: a guide to
methods and application, eds. M. Innis, D. Gelfand, J.
Sninsky, and T. White. San Diego: Academy Press.</mixed-citation>
         </ref>
         <ref id="d594e1825a1310">
            <mixed-citation id="d594e1829" publication-type="other">
Woiciechowski, M. F., M. J. Sanderson, and J. M Hu. 1999.
Evidence on the monophyly of Astragalus (Fabaceae) and
its major subgroups based on nuclear ribosomal DNA
ITS and chloroplast DNA trnh intron data. Systematic
Botany 49: 143-159.</mixed-citation>
         </ref>
         <ref id="d594e1848a1310">
            <mixed-citation id="d594e1852" publication-type="other">
Yoder, A. D., J. A. Irwin, and B. A. Payseur. 2001. Failure of
the ILD to determine data combinability for slow loris
phylogeny. Systematic Biology 50: 408-424.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
