<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">evolution</journal-id>
         <journal-id journal-id-type="jstor">j100004</journal-id>
         <journal-title-group>
            <journal-title>Evolution</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Society for the Study of Evolution</publisher-name>
         </publisher>
         <issn pub-type="ppub">00143820</issn>
         <issn pub-type="epub">15585646</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">3094597</article-id>
         <article-categories>
            <subj-group>
               <subject>Brief Communications</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Socially Mediated Speciation</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Michael E.</given-names>
                  <surname>Hochberg</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Barry</given-names>
                  <surname>Sinervo</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Sam P.</given-names>
                  <surname>Brown</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>1</month>
            <year>2003</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">57</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id>i200293</issue-id>
         <fpage>154</fpage>
         <lpage>158</lpage>
         <page-range>154-158</page-range>
         <permissions>
            <copyright-statement>Copyright 2003 The Society for the Study of Evolution</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/3094597"/>
         <abstract>
            <p>We employ a simple model to show that social selection can lead to prezygotic reproductive isolation. The evolution of social discrimination causes the congealing of phenotypically similar individuals into different, spatially distinct tribes. However, tribal formation is only obtained for certain types of social behavior: altruistic and selfish acts can produce tribes, whereas spiteful and mutualistic behaviors never do. Moreover, reduced hybrid fitness at tribal borders leads to the selection of mating preferences, which then spread to the core areas of the respective tribes. Unlike models of resource competition, our model generates reproductive isolation in an ecologically homogeneous environment. We elaborate on how altruistic acts can lead to reproductive isolation, but also predict that certain types of competition can lead to the speciation effect. Our theory provides a framework for how individual-level interactions mold lineage diversification, with parapatric speciation as a possible end product.</p>
         </abstract>
         <kwd-group>
            <kwd>Altruism</kwd>
            <kwd>Competition</kwd>
            <kwd>Diversification</kwd>
            <kwd>Reproductive Isolation</kwd>
            <kwd>Selfishness</kwd>
            <kwd>Social Behavior</kwd>
            <kwd>Speciation</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Literature Cited</title>
         <ref id="d739e206a1310">
            <mixed-citation id="d739e210" publication-type="journal">
Axelrod, R., and W. D. Hamilton. 1981. The evolution of coop-
eration. Science 211:1390-1396.<object-id pub-id-type="jstor">10.2307/1685895</object-id>
               <fpage>1390</fpage>
            </mixed-citation>
         </ref>
         <ref id="d739e226a1310">
            <mixed-citation id="d739e230" publication-type="journal">
Baker, M. C., K. J. Spitler-Nabors, and D. C. Bradley. 1981. Early
experience determines song dialect responsiveness of female
sparrows. Science 214:819-821.<object-id pub-id-type="jstor">10.2307/1686989</object-id>
               <fpage>819</fpage>
            </mixed-citation>
         </ref>
         <ref id="d739e249a1310">
            <mixed-citation id="d739e253" publication-type="journal">
Barbujani, G. and R. R. Sokal. 1990. Zones of sharp genetic change
in Europe are also linguistic boundaries. Proc. Natl. Acad. Sci.
USA 87:1816-1819.<object-id pub-id-type="jstor">10.2307/2353936</object-id>
               <fpage>1816</fpage>
            </mixed-citation>
         </ref>
         <ref id="d739e272a1310">
            <mixed-citation id="d739e276" publication-type="book">
Boyd, R., and P. Richerson. 1985. Culture and the evolutionary
process. Basic Books, New York.<person-group>
                  <string-name>
                     <surname>Boyd</surname>
                  </string-name>
               </person-group>
               <source>Culture and the evolutionary process</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d739e302a1310">
            <mixed-citation id="d739e306" publication-type="journal">
Crespi, B. J. 2001. The evolution of social behavior in microor-
ganisms. Trends Ecol. Evol. 16:178-183.<person-group>
                  <string-name>
                     <surname>Crespi</surname>
                  </string-name>
               </person-group>
               <fpage>178</fpage>
               <volume>16</volume>
               <source>Trends Ecol. Evol.</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d739e338a1310">
            <mixed-citation id="d739e342" publication-type="journal">
Danley, P. D., and T. D. Kocher. 2001. Speciation in rapidly di-
verging systems: lessons from Lake Malawi. Mol. Ecol. 10:
1075-1086.<person-group>
                  <string-name>
                     <surname>Danley</surname>
                  </string-name>
               </person-group>
               <fpage>1075</fpage>
               <volume>10</volume>
               <source>Mol. Ecol.</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d739e377a1310">
            <mixed-citation id="d739e381" publication-type="journal">
Dieckmann, U., and M. Doebeli. 1999. On the origin of species by
sympatric speciation. Nature 400:354-357.<person-group>
                  <string-name>
                     <surname>Dieckmann</surname>
                  </string-name>
               </person-group>
               <fpage>354</fpage>
               <volume>400</volume>
               <source>Nature</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d739e413a1310">
            <mixed-citation id="d739e417" publication-type="journal">
Dobzhansky, T. 1940. Speciation as a stage in evolutionary diver-
gence. Am. Nat. 74:312-321.<object-id pub-id-type="jstor">10.2307/2457524</object-id>
               <fpage>312</fpage>
            </mixed-citation>
         </ref>
         <ref id="d739e433a1310">
            <mixed-citation id="d739e437" publication-type="book">
Dugatkin, L. A. 1997. Cooperation among animals. Oxford Uni-
versity Press, Oxford, U.K.<person-group>
                  <string-name>
                     <surname>Dugatkin</surname>
                  </string-name>
               </person-group>
               <source>Cooperation among animals</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d739e462a1310">
            <mixed-citation id="d739e466" publication-type="journal">
Godard, R. 1993. Tit-for-tat among neighboring hooded warblers.
Behav. Ecol. Sociobiol. 33:45-50.<person-group>
                  <string-name>
                     <surname>Godard</surname>
                  </string-name>
               </person-group>
               <fpage>45</fpage>
               <volume>33</volume>
               <source>Ecol. Sociobiol.</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d739e499a1310">
            <mixed-citation id="d739e503" publication-type="journal">
Grosberg, R. K., and M. W. Hart. 2000. Mate selection and the
evolution of highly polymorphic self/nonself recognition genes.
Science 289:2111-2114.<object-id pub-id-type="jstor">10.2307/3078081</object-id>
               <fpage>2111</fpage>
            </mixed-citation>
         </ref>
         <ref id="d739e522a1310">
            <mixed-citation id="d739e526" publication-type="journal">
Hamilton, W. D. 1964. The genetical evolution of social behavior.
I and II. J. Theor. Biol. 7:1-52.<person-group>
                  <string-name>
                     <surname>Hamilton</surname>
                  </string-name>
               </person-group>
               <fpage>1</fpage>
               <volume>7</volume>
               <source>J. Theor. Biol.</source>
               <year>1964</year>
            </mixed-citation>
         </ref>
         <ref id="d739e558a1310">
            <mixed-citation id="d739e562" publication-type="journal">
—.1970. Selfish and spiteful behavior in an evolutionary mod-
el. Nature 228:1218-1220.<person-group>
                  <string-name>
                     <surname>Hamilton</surname>
                  </string-name>
               </person-group>
               <fpage>1218</fpage>
               <volume>228</volume>
               <source>Nature</source>
               <year>1970</year>
            </mixed-citation>
         </ref>
         <ref id="d739e594a1310">
            <mixed-citation id="d739e598" publication-type="journal">
Keller, L., and K. G. Ross. 1998. Selfish genes: a green beard in
the red fire ant. Nature 394:573-575.<person-group>
                  <string-name>
                     <surname>Keller</surname>
                  </string-name>
               </person-group>
               <fpage>573</fpage>
               <volume>394</volume>
               <source>Nature</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d739e630a1310">
            <mixed-citation id="d739e634" publication-type="journal">
Kirkpatrick, M. 2000. Reinforcement and divergence under assor-
tative mating. Proc. R. Soc. Lond. B267:1649-1655.<object-id pub-id-type="jstor">10.2307/2665911</object-id>
               <fpage>1649</fpage>
            </mixed-citation>
         </ref>
         <ref id="d739e650a1310">
            <mixed-citation id="d739e654" publication-type="journal">
Kirkpatrick, M., and V. Ravign 6. 2002. Speciation by natural and
sexual selection: models and experiments. Am Nat. 159:
S22-S55.<object-id pub-id-type="jstor">10.2307/3078919</object-id>
               <fpage>S22</fpage>
            </mixed-citation>
         </ref>
         <ref id="d739e674a1310">
            <mixed-citation id="d739e678" publication-type="journal">
Koella, J. C. 2000. The spatial spread of altruism versus the evo-
lutionary response of egoists. Proc. R. Soc. Lond. B267:
1979-1985.<object-id pub-id-type="jstor">10.2307/2665686</object-id>
               <fpage>1979</fpage>
            </mixed-citation>
         </ref>
         <ref id="d739e697a1310">
            <mixed-citation id="d739e701" publication-type="journal">
Komdeur, J., and B. J. Hatchwell. 1999. Kin recognition: function
and mechanism in avian societies. Trends Ecol. Evol. 14:
237-241.<person-group>
                  <string-name>
                     <surname>Komdeur</surname>
                  </string-name>
               </person-group>
               <fpage>237</fpage>
               <volume>14</volume>
               <source>Trends Ecol. Evol.</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d739e736a1310">
            <mixed-citation id="d739e740" publication-type="journal">
Kondrashov, A. S., and F. A. Kondrashov. 1999. Interactions among
quantitative traits in the course of sympatric speciation. Nature
400:351-354.<person-group>
                  <string-name>
                     <surname>Kondrashov</surname>
                  </string-name>
               </person-group>
               <fpage>351</fpage>
               <volume>400</volume>
               <source>Nature</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d739e775a1310">
            <mixed-citation id="d739e779" publication-type="book">
Krebs, J. R. and N. B. Davies, Eds. 1993. An introduction to be-
havioral ecology. 3rd ed., Blackwell, Oxford, U.K.<person-group>
                  <string-name>
                     <surname>Krebs</surname>
                  </string-name>
               </person-group>
               <edition>3</edition>
               <source>An introduction to behavioral ecology</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d739e808a1310">
            <mixed-citation id="d739e812" publication-type="journal">
Lande, R. 1981. Models of speciation by sexual selection on poly-
genic traits. Proc. Natl. Acad. Sci. USA 78:3721-3725.<object-id pub-id-type="jstor">10.2307/10908</object-id>
               <fpage>3721</fpage>
            </mixed-citation>
         </ref>
         <ref id="d739e828a1310">
            <mixed-citation id="d739e832" publication-type="journal">
—.1984. The genetic correlation between characters main-
tained by selection, linkage and inbreeding. Genet. Res. Camb.
44:309-320.<person-group>
                  <string-name>
                     <surname>Lande</surname>
                  </string-name>
               </person-group>
               <fpage>309</fpage>
               <volume>44</volume>
               <source>Genet. Res. Camb.</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d739e868a1310">
            <mixed-citation id="d739e872" publication-type="book">
Macnair, M. R., and M. Gardner. 1998. The evolution of edaphic
endemics. Pp. 157-171 in D. J. Howard and S. H. Berlocher,
eds. Endless forms: species and speciation. Oxford University
Press, Oxford, U.K.<person-group>
                  <string-name>
                     <surname>Macnair</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The evolution of edaphic endemics</comment>
               <fpage>157</fpage>
               <source>Endless forms: species and speciation</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d739e910a1310">
            <mixed-citation id="d739e914" publication-type="journal">
Nowak, M. A., and K. Sigmund. 1998. Evolution of indirect reci-
procity by image scoring. Nature 393:573-577.<person-group>
                  <string-name>
                     <surname>Nowak</surname>
                  </string-name>
               </person-group>
               <fpage>573</fpage>
               <volume>393</volume>
               <source>Nature</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d739e946a1310">
            <mixed-citation id="d739e950" publication-type="journal">
Payne, R. J. H., and D. C. Krakauer. 1997. Sexual selection, space,
and speciation. Evolution 51:1-9.<object-id pub-id-type="doi">10.2307/2410954</object-id>
               <fpage>1</fpage>
            </mixed-citation>
         </ref>
         <ref id="d739e966a1310">
            <mixed-citation id="d739e970" publication-type="journal">
Queller, D. C. 1992. Does population viscosity promote kin selec-
tion? Trends Ecol. Evol. 7:322-324.<person-group>
                  <string-name>
                     <surname>Queller</surname>
                  </string-name>
               </person-group>
               <fpage>322</fpage>
               <volume>7</volume>
               <source>Trends Ecol. Evol.</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d739e1002a1310">
            <mixed-citation id="d739e1006" publication-type="journal">
Riolo, R. L., M. D. Cohen, and R. Axelrod. 2001. Evolution of
cooperation without reciprocity. Nature 414:441-443.<person-group>
                  <string-name>
                     <surname>Riolo</surname>
                  </string-name>
               </person-group>
               <fpage>441</fpage>
               <volume>414</volume>
               <source>Nature</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d739e1038a1310">
            <mixed-citation id="d739e1042" publication-type="book">
Schluter, D. 2000. The ecology of adaptive radiations. Oxford Uni-
versity Press, Oxford, U.K.<person-group>
                  <string-name>
                     <surname>Schluter</surname>
                  </string-name>
               </person-group>
               <source>The ecology of adaptive radiations</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d739e1068a1310">
            <mixed-citation id="d739e1072" publication-type="journal">
Sherratt, T. N., and G. Roberts. 1998. The evolution of generosity
and choosiness in cooperative exchanges. J. Theor. Biol. 193:
167-177.<person-group>
                  <string-name>
                     <surname>Sherratt</surname>
                  </string-name>
               </person-group>
               <fpage>167</fpage>
               <volume>193</volume>
               <source>J. Theor. Biol.</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d739e1107a1310">
            <mixed-citation id="d739e1111" publication-type="journal">
Shoemaker, D. D. and K. G. Ross. 1996. Effects of social orga-
nization on gene flow in the fire ant Solenopsis invicta. Nature
383:613-616.<person-group>
                  <string-name>
                     <surname>Shoemaker</surname>
                  </string-name>
               </person-group>
               <fpage>613</fpage>
               <volume>383</volume>
               <source>Nature</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d739e1146a1310">
            <mixed-citation id="d739e1150" publication-type="journal">
Temeles, E. J. 1994. The role of neighbours in territorial systems:
when are they "dear enemies"? Anim. Behav. 47:339-350.<person-group>
                  <string-name>
                     <surname>Temeles</surname>
                  </string-name>
               </person-group>
               <fpage>339</fpage>
               <volume>47</volume>
               <source>Anim. Behav.</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d739e1182a1310">
            <mixed-citation id="d739e1186" publication-type="journal">
Trivers, R. L. 1971. The evolution of reciprocal altruism. Q. Rev.
Biol. 46:35-57.<object-id pub-id-type="jstor">10.2307/2822435</object-id>
               <fpage>35</fpage>
            </mixed-citation>
         </ref>
         <ref id="d739e1202a1310">
            <mixed-citation id="d739e1206" publication-type="journal">
West-Eberhard, M. J. 1983. Sexual selection, social competition
and speciation. Q. Rev. Biol. 58:155-183.<object-id pub-id-type="jstor">10.2307/2828804</object-id>
               <fpage>155</fpage>
            </mixed-citation>
         </ref>
         <ref id="d739e1222a1310">
            <mixed-citation id="d739e1226" publication-type="journal">
Wilson, D. S., G. B. Pollock, and L. A. Dugatkin. 1992. Can altruism
evolve in purely viscous populations? Evol. Ecol. 6:331-341.<person-group>
                  <string-name>
                     <surname>Wilson</surname>
                  </string-name>
               </person-group>
               <fpage>331</fpage>
               <volume>6</volume>
               <source>Evol. Ecol.</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d739e1259a1310">
            <mixed-citation id="d739e1263" publication-type="journal">
Wolf, J. B., E. D. Brodie III, and A. J. Moore. 1999. Interacting
phenotypes and the evolutionary process. II. Selection resulting
from social interactions. Am. Nat. 153:254-266.<object-id pub-id-type="jstor">10.2307/2463822</object-id>
               <fpage>254</fpage>
            </mixed-citation>
         </ref>
         <ref id="d739e1282a1310">
            <mixed-citation id="d739e1286" publication-type="journal">
Wright, T. F., and G. S. Wilkinson. 2001. Population genetic struc-
ture and vocal dialects in an Amazon parrot. Proc. R. Soc. Lond.
B268:609-616.<object-id pub-id-type="jstor">10.2307/3067602</object-id>
               <fpage>609</fpage>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
