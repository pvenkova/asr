<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">envicons</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50020266</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Environmental Conservation</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>CAMBRIDGE UNIVERSITY PRESS</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">03768929</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">14694387</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">44520598</article-id>
         <title-group>
            <article-title>Conservation on community lands: the importance of equitable revenue sharing</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>ROSEMARY</given-names>
                  <surname>GROOM</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>STEPHEN</given-names>
                  <surname>HARRIS</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>9</month>
            <year>2008</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">35</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40189803</issue-id>
         <fpage>242</fpage>
         <lpage>251</lpage>
         <permissions>
            <copyright-statement>© 2008 Foundation for Environmental Conservation</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/44520598"/>
         <abstract>
            <p>Attempts to establish local support for wildlife and conservation through the sharing of revenues and empowerment of local communities to manage their wildlife have proliferated over the past two decades. Data from two neighbouring Maasai group ranches in the wildlife dispersal area of Amboseli and Tsavo National Parks (Kenya) indicated one ranch generated considerable wildlife revenues from a tourist operation and community trust while the other received no direct benefits from wildlife. The overall attitude to wildlife on the ranch with wildlife revenues was significantly more positive, but attitudes within the ranch varied significantly, depending on both costs from wildlife and perception of the distribution of wildlife revenues. Ordinal logistic regression analyses showed that it was not the amount of revenue received or the scale of costs from wildlife which determined people's attitudes, but simply the presence or absence of wildlife benefits. The importance of addressing inequitable distribution of benefits is emphasized.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d2895e199a1310">
            <mixed-citation id="d2895e203" publication-type="other">
Adams, W.M. &amp; Infield, M. (2003) Who is on the gorilla's payroll?
Claims on tourist revenue from a Ugandan national park. World
Developmental: 177-190.</mixed-citation>
         </ref>
         <ref id="d2895e216a1310">
            <mixed-citation id="d2895e220" publication-type="other">
Altmann, J., Alberts, S.C., Altmann, S.A. &amp; Roy, S.B. (2002)
Dramatic change in local climate patterns in the Amboseli basin,
Kenya. African Journal of Ecology 40: 248-251.</mixed-citation>
         </ref>
         <ref id="d2895e233a1310">
            <mixed-citation id="d2895e237" publication-type="other">
Archabald, K. &amp; Naughton-Treves, L. (2001) Tourism sharing
around national parks in western Uganda: early efforts to identify
and reward local communities. Environmental Conservation 28:
135-149.</mixed-citation>
         </ref>
         <ref id="d2895e253a1310">
            <mixed-citation id="d2895e257" publication-type="other">
Arjunan, M., Holmes, C., Puyravaud, J.P. &amp; Davidar, P. (2006)
Do developmental initiatives influence local attitudes toward
conservation? A case study from the Kalakad-Mundanthurai Tiger
Reserve, India. Journal of Environmental Management 79: 188 -
197.</mixed-citation>
         </ref>
         <ref id="d2895e277a1310">
            <mixed-citation id="d2895e281" publication-type="other">
Barrett, C.B. &amp; Arcese, P. (1995) Are integrated conservation-
development projects (ICDPs) sustainable? On the conservation
of large mammals in Sub-Saharan Africa. World Development 23:
1073-1084.</mixed-citation>
         </ref>
         <ref id="d2895e297a1310">
            <mixed-citation id="d2895e301" publication-type="other">
Bekure, S., de Leeuw, P.N., Grandin, B.E. &amp; Neate, P.J.H. (1991)
Maasai Herding. An Analysis of the Livestock Production System
of Maasai Pastoralists in Eastern Kajiado District, Kenya. Addis
Ababa, Ethiopia: ILCA Systems Study 4. ILCA (International
Livestock Centre for Africa): 172 pp.</mixed-citation>
         </ref>
         <ref id="d2895e320a1310">
            <mixed-citation id="d2895e324" publication-type="other">
Bulte, E.H. &amp; Rondeau, D. (2005) Why compensating wildlife
damages may be bad for conservation. Journal of Wildlife
Management 69: 14-19.</mixed-citation>
         </ref>
         <ref id="d2895e337a1310">
            <mixed-citation id="d2895e341" publication-type="other">
Campbell, D.J. (1999) Response to drought among farmers and
herders in southern Kajiado District, Kenya: a comparison of
1972-1976 and 1994-1995. Human Ecology 27: 377-415.</mixed-citation>
         </ref>
         <ref id="d2895e354a1310">
            <mixed-citation id="d2895e358" publication-type="other">
Child, B. (2000) Making wildlife pay: converting wildlife's
comparative advantage into real incentives for having wildlife in
African savannas, case studies from Zimbabwe and Zambia. In:
Wildlife Conservation by Sustainable Use, ed. H.H.T. Prins, J.G.
Grootenhuis &amp; T.T. Dolan, pp. 335-387. Boston, USA: Kluwer
Academic Publishers.</mixed-citation>
         </ref>
         <ref id="d2895e381a1310">
            <mixed-citation id="d2895e385" publication-type="other">
Deodatus, F. (2000) Wildlife damage in rural areas with emphasis on
Malawi. In: Wildlife Conservation by Sustainable Use, ed. H.H.T.
Prins, J.G. Grootenhuis &amp; T.T. Dolan, pp. ?-?. Boston, USA:
Kluwer Academic Publishers.</mixed-citation>
         </ref>
         <ref id="d2895e402a1310">
            <mixed-citation id="d2895e406" publication-type="other">
Earnshaw, A. &amp; Emerton, L. (2000) The economics of wildlife
tourism: theory and reality for landholders in Africa. In:
Wildlife Conservation by Sustainable Use, ed. H.H.T. Prins, J.G.
Grootenhuis &amp; T.T. Dolan, pp. 315-334. Boston, USA: Kluwer
Academic Publishers.</mixed-citation>
         </ref>
         <ref id="d2895e425a1310">
            <mixed-citation id="d2895e429" publication-type="other">
Fiallo, E.A. &amp; Jacobson, S.K. (1995) Local communities and
protected areas: attitudes of rural residents towards conservation
in Ecuador. Environmental Conservation 22: 241-249.</mixed-citation>
         </ref>
         <ref id="d2895e442a1310">
            <mixed-citation id="d2895e446" publication-type="other">
Gadd, M.E. (2005) Conservation outside of parks: attitudes of local
people in Laikipia, Kenya. Environmental Conservation 32: 50-63.</mixed-citation>
         </ref>
         <ref id="d2895e456a1310">
            <mixed-citation id="d2895e460" publication-type="other">
Gibson, C.C. &amp; Marks, S.A. (1995) Transforming rural hunters
into conservationists: an assessment of community-based wildlife
management programs in Africa. World Development 23: 941-
957.</mixed-citation>
         </ref>
         <ref id="d2895e476a1310">
            <mixed-citation id="d2895e480" publication-type="other">
Gillingham, S. &amp; Lee, P.C. (1999) The impact of wildlife-related
benefits on the conservation attitudes of local people around the
Selous Game Reserve, Tanzania. Environmental Conservation 26:
218-228.</mixed-citation>
         </ref>
         <ref id="d2895e496a1310">
            <mixed-citation id="d2895e500" publication-type="other">
Groom, R. (2007) How to make land subdivision work: an analysis of
the ecological and socio-economic factors affecting conservation
outcomes during land privatisation in Kenyan Maasailand. Ph.D.
thesis, University of Bristol, Bristol, UK.</mixed-citation>
         </ref>
         <ref id="d2895e517a1310">
            <mixed-citation id="d2895e521" publication-type="other">
Hackel, J.D. (1999) Community conservation and the future of
Africa's wildlife. Conservation Biology 13: 726-734.</mixed-citation>
         </ref>
         <ref id="d2895e531a1310">
            <mixed-citation id="d2895e535" publication-type="other">
Hartup, B.K. (1994) Community conservation in Belize:
demography, resource use, and attitudes of participating
landowners. Biological Conservation 69: 235-241.</mixed-citation>
         </ref>
         <ref id="d2895e548a1310">
            <mixed-citation id="d2895e552" publication-type="other">
Hazzah, L.N. (2007) Living among lions {Panthera leo): coexistence
or killing? Community attitudes towards conservation initiatives
and the motivations behind lion killing in Kenyan Maasailand.
Masters of Science, University of Wisconsin-Madison,
USA.</mixed-citation>
         </ref>
         <ref id="d2895e571a1310">
            <mixed-citation id="d2895e575" publication-type="other">
Heinen, J.T. (1993) Park people relations in Kosi Tappu
Wildlife Reserve, Nepal: a socio-economic analysis. Environmental
Conservation 20: 25-34.</mixed-citation>
         </ref>
         <ref id="d2895e588a1310">
            <mixed-citation id="d2895e592" publication-type="other">
Infield, M. (1988) Attitudes of a rural community towards
conservation and a local conservation area in Natal, South Africa.
Biological Conservation 45: 21-46.</mixed-citation>
         </ref>
         <ref id="d2895e605a1310">
            <mixed-citation id="d2895e609" publication-type="other">
Infield, M. &amp; Ñamara, A. (2001) Community attitudes and behavior
towards conservation: an assessment of a community conservation
programme around Lake Mburo National Park, Uganda. Oryx 35:
48-60.</mixed-citation>
         </ref>
         <ref id="d2895e626a1310">
            <mixed-citation id="d2895e630" publication-type="other">
Lewis, D., Kaweche, D.B. &amp; Mwenya, A. (1990) Wildlife
conservation outside protected areas -lessons from an experiment
in Zambia. Conservation Biology 4: 171-180.</mixed-citation>
         </ref>
         <ref id="d2895e643a1310">
            <mixed-citation id="d2895e647" publication-type="other">
Marker, L.L., Mills, M.G.L. &amp; Macdonald, D.W. (2003) Factors
influencing perceptions of conflict and tolerance towards
cheetahs on Namibian farmlands. Conservation Biology 17: 1290-
1298.</mixed-citation>
         </ref>
         <ref id="d2895e663a1310">
            <mixed-citation id="d2895e667" publication-type="other">
McNeely, J. A. (1995) Expanding Partnerships in Conservation.
Washington DC, USA: Island Press.</mixed-citation>
         </ref>
         <ref id="d2895e677a1310">
            <mixed-citation id="d2895e681" publication-type="other">
Ministry of Agriculture (1968) Annual Report. Nairobi, Kenya:
Government Printers.</mixed-citation>
         </ref>
         <ref id="d2895e691a1310">
            <mixed-citation id="d2895e695" publication-type="other">
Mishra, C. (1997) Livestock depredation by large carnivores in
the Indian trans-Himalaya: conflict perceptions and conservation
prospects. Environmental Conservation 24: 338-343.</mixed-citation>
         </ref>
         <ref id="d2895e708a1310">
            <mixed-citation id="d2895e712" publication-type="other">
Mishra, C., Allen, P., McCarthy, T., Madhusudan, M.D.,
Agvaantserengiin, B. &amp; Prins, H.H.T. (2003) The role of incentive
programs in conserving the snow leopard. Conservation Biology 17:
1512-1520.</mixed-citation>
         </ref>
         <ref id="d2895e729a1310">
            <mixed-citation id="d2895e733" publication-type="other">
Newmark, W.D., Leonard, N.L., Sariko, H.I. &amp; Gamassa, D.M.
(1993) Conservation attitudes of local people living adjacent to
five protected areas in Tanzania. Biological Conservation 63: 177-
183.</mixed-citation>
         </ref>
         <ref id="d2895e749a1310">
            <mixed-citation id="d2895e753" publication-type="other">
Norton-Griffiths, M., Said, M.Y., Seernals, S., Kaelo, D.S.,
Coughenour, M., Lamprey, R.H., Thompson, D.M. &amp; Reid, R.S.
(2008) Land use economics in the Mara area of the Serengeti
ecosystem. In: Serengeti III: Human Impacts on Ecosystem
Dynamics., ed. A.R.E. Sinclair, C. Packer, S.A.R. Mduma &amp;
J.M. Fryxell, (in press). Chicago, IL, USA: Chicago University
Press.</mixed-citation>
         </ref>
         <ref id="d2895e779a1310">
            <mixed-citation id="d2895e783" publication-type="other">
Ntiati, P. (2002) Group ranch subdivision study in Loitokitok
Division of Kajiado District, Kenya. LUCID Project,
International Livestock Research Institute, Nairobi.</mixed-citation>
         </ref>
         <ref id="d2895e796a1310">
            <mixed-citation id="d2895e800" publication-type="other">
Ogada, M.O., Woodroffe, R., Oguge, N.O. &amp; Frank, L.G. (2003)
Limiting depredation by African carnivores: the role of livestock
husbandry. Conservation Biology 17: 1-10.</mixed-citation>
         </ref>
         <ref id="d2895e813a1310">
            <mixed-citation id="d2895e817" publication-type="other">
Ogutu, Z.A. (2002) The impact of ecotourism on livelihood and
natural resource management in Eselenkei, Amboseli Ecosystem,
Kenya. Land Degradation and Development 13: 251-256.</mixed-citation>
         </ref>
         <ref id="d2895e830a1310">
            <mixed-citation id="d2895e834" publication-type="other">
Ottichilo, W.K., Grunblatt, J.M., Said, M.Y. &amp; Wargute, P. (2000)
Wildlife and livestock population trends in the Kenya rangeland.
In: Wildlife Conservation by Sustainable Useì ed. H.H.T. Prins,
J.G. Grootenhuis &amp; T.T. Dolan, pp. 203-218. Boston, CT, USA:
Kluwer Academic Publishers.</mixed-citation>
         </ref>
         <ref id="d2895e854a1310">
            <mixed-citation id="d2895e858" publication-type="other">
Parry, D. &amp; Campbell, B. (1992) Attitudes of rural communities to
animal wildlife and its utilisation in Chobe Enclave and Mababe
Depression, Botswana. Environmental Conservation 19: 245-252.</mixed-citation>
         </ref>
         <ref id="d2895e871a1310">
            <mixed-citation id="d2895e875" publication-type="other">
Prins, H.H.T. (2000) Competition between wildlife and livestock in
Africa. In: Wildlife Conservation by Sustainable Use, ed. H.H.T.
Prins, J.G. Grootenhuis &amp; T.T. Dolan, pp. 51-80. Boston, CT,
USA: Kluwer Academic Publishers.</mixed-citation>
         </ref>
         <ref id="d2895e891a1310">
            <mixed-citation id="d2895e895" publication-type="other">
Robson, C. (2002) Real World Research: a Resource for Social
Scientists and Practitioner-researchers. Second edition. Oxford, UK:
Blackwell Publishing Ltd.</mixed-citation>
         </ref>
         <ref id="d2895e908a1310">
            <mixed-citation id="d2895e912" publication-type="other">
Sombroek, W.C., Braun, H.M.H. &amp; Van Der Pouw, B.J.A. (1982)
Explanatory soil map and agro-climatic zone map of Kenya. Report
El. National Agricultural Laboratories, Soil Survey Unit, Nairobi,
Kenya.</mixed-citation>
         </ref>
         <ref id="d2895e928a1310">
            <mixed-citation id="d2895e932" publication-type="other">
Sutton, W.R., Larson, D.M. &amp; Jarvis, L.S. (2004) A new
approach for assessing the costs of living with wildlife in
developing countries. UC Davis Agricultural and Resource
Economics Working Paper no. 04-001 [www document]. URL
http://ssrn.com/abstract=525582</mixed-citation>
         </ref>
         <ref id="d2895e951a1310">
            <mixed-citation id="d2895e955" publication-type="other">
Thompson, D.M. &amp; Homewood, K. (2002) Entrepreneurs, elites,
and exclusion in Maasailand: trends in wildlife conservation and
pastoral development. Human Ecology 30: 107-137.</mixed-citation>
         </ref>
         <ref id="d2895e969a1310">
            <mixed-citation id="d2895e973" publication-type="other">
Weladji, R.B., Moe, S.R. &amp; Vedeld, P. (2003) Stakeholder attitudes
towards wildlife policy and the Bénoué Wildlife Conservation
Area, North Cameroon. Environmental Conservation 30: 334-343.</mixed-citation>
         </ref>
         <ref id="d2895e986a1310">
            <mixed-citation id="d2895e990" publication-type="other">
Western, D. (1973) The structure, dynamics and changes of the
Amboseli Ecosystem. Ph.D. thesis, University of Nairobi, Nairobi,
Kenya.</mixed-citation>
         </ref>
         <ref id="d2895e1003a1310">
            <mixed-citation id="d2895e1007" publication-type="other">
Western, D. &amp; Wright, R.M. (1994) The background to community
based conservation. In: Natural Connections: Perspectives in
Community-based Conservation., ed. D. Western and R.M. Wright,
pp. 1-14. Washington DC, USA: Island Press.</mixed-citation>
         </ref>
         <ref id="d2895e1023a1310">
            <mixed-citation id="d2895e1027" publication-type="other">
White, P.C.L., Vaughan Jennings, N., Renwick, A.R. &amp; Barker,
N.H.L. (2005) Questionnaires in ecology: a review of past use and
recommendations for best practice. Journal of Applied Ecology 42:
421-430.</mixed-citation>
         </ref>
         <ref id="d2895e1043a1310">
            <mixed-citation id="d2895e1047" publication-type="other">
Worden, J., Reid, R. &amp; Gichohi, H. (2003) Land-use impacts on
large wildlife and livestock in the swamps of the greater Amboseli
ecosystem, Kajiado District, Kenya. Lucid Project, International
Livestock Research Institute, Nairobi, Kenya.</mixed-citation>
         </ref>
         <ref id="d2895e1063a1310">
            <mixed-citation id="d2895e1067" publication-type="other">
Zimmerman, A., Walpole, M.J. &amp; Leader-Williams, N. (2005) Cattle
ranchers' attitudes to conflicts with jaguar Panthera onca in the
Pantanal of Brazil. Oryx 39: 406-412.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
