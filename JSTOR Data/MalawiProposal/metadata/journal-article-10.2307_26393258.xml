<?xml version="1.0" encoding="UTF-8"?>
<article article-type="research-article" dtd-version="1.0" xml:lang="eng">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="publisher-id">conssoci</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50019729</journal-id>
         <journal-title-group>
            <journal-title>Conservation and Society</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>WOLTERS KLUWER INDIA PRIVATE LIMITED</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">09724923</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">09753133</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">26393258</article-id>
         <article-categories>
            <subj-group>
               <subject>Article</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Termites, Mud Daubers and their Earths</article-title>
            <subtitle>A Multispecies Approach to Fertility and Power in West Africa</subtitle>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Fairhead</surname>
                  <given-names>James R.</given-names>
               </string-name>
               <aff>Current Affiliation: Department of Anthropology, University of Sussex, Brighton, UK</aff>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>1</month>
            <year>2016</year>
            <string-date>2016</string-date>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink">14</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink">4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">e26393251</issue-id>
         <fpage>359</fpage>
         <lpage>367</lpage>
         <permissions>
            <copyright-statement>Copyright: © Fairhead 2016</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/26393258"/>
         <abstract xml:lang="eng">
            <p>The termites and mud-dauber wasps of West Africa build earthen structures in which their eggs and larvae develop. This paper examines how these insect earths are understood and used in West Africa, focusing on their direct consumption (geophagy) and medicinal qualities. Existing research reveals these earths to be enriched in minerals otherwise lacking in the diets of the region, and suggests that insects may also introduce anti-microbial properties into them. The paper examines the place of these earths in the lives of those who use them and through a ‘multispecies’ approach provides new insights into the ecological dimensions to ‘religious’ thought and practice, and of the respect that these insects command.</p>
         </abstract>
         <kwd-group>
            <label>Keywords:</label>
            <kwd>Termites</kwd>
            <kwd>wasps</kwd>
            <kwd>geophagy</kwd>
            <kwd>anti-microbials</kwd>
            <kwd>earth</kwd>
            <kwd>human-animal relations</kwd>
            <kwd>West Africa</kwd>
            <kwd>health</kwd>
         </kwd-group>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="unparsed-citations">
         <title>REFERENCES</title>
         <ref id="ref1">
            <mixed-citation publication-type="other">Abrahams, P.W. 2012. Involuntary soil ingestion and geophagia: a source and sink of mineral nutrients and potentially harmful elements to consumers of earth materials. Applied Geochemistry 27(5): 954-958.</mixed-citation>
         </ref>
         <ref id="ref2">
            <mixed-citation publication-type="other">Abrahams, P.W. 2013. Geophagy and the involuntary ingestion of soil. In: Essentials in Medical Geology (ed. Selinus, O). Pp. 433-455. London : Springer.</mixed-citation>
         </ref>
         <ref id="ref3">
            <mixed-citation publication-type="other">Adriaeus, E.L. 1951. Recherches sur l’alimentation des populations au Kwango. Bulletin Agricole du Congo Beige 42(2): 227-270.</mixed-citation>
         </ref>
         <ref id="ref4">
            <mixed-citation publication-type="other">Ahn, P. 1974. West African Soils. Oxford: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="ref5">
            <mixed-citation publication-type="other">Anell, B. and S. Lagercrantz.1958. Geophagical customs. Uppsala: Studia Ethnographica Upsaliensia volume 17.</mixed-citation>
         </ref>
         <ref id="ref6">
            <mixed-citation publication-type="other">Begg, G.C., W.L. Griffin, L.M. Natapov, S.Y. O’Reilly, S.P. Grand, C.J. O’Neill, J.M.A. Hronsky, et al. 2009. The lithosphere architecture of Africa: seismic tomography, mantle petrology, and tectonic evolution. Geosphere 5(1): 23-50.</mixed-citation>
         </ref>
         <ref id="ref7">
            <mixed-citation publication-type="other">Brett-Smith, S. 1994. The making ofBamana sculpture: creativity and gender. New York: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="ref8">
            <mixed-citation publication-type="other">Brett-Smith, S. 1997. The mouth of the Komo. Anthropology and Aesthetics 31(Spring 1997): 71-96.</mixed-citation>
         </ref>
         <ref id="ref9">
            <mixed-citation publication-type="other">Brett-Smith, S. 2001. When is an object finished? the creation of the invisible among the Bamana of Mali. Anthropology and Aesthetics 39 (Spring 2001): 102-136.</mixed-citation>
         </ref>
         <ref id="ref10">
            <mixed-citation publication-type="other">Brett-Smith, S. 2014. The silence of the women: Bamana mud cloths. Milan : 5 Continents.</mixed-citation>
         </ref>
         <ref id="ref11">
            <mixed-citation publication-type="other">Bureau, R. 1971. La religion d’Eboga. Essai sur le Bwiti-Fang. Abidjan: Universite d’Abidjan, Institut d’Ethno-sociologie.</mixed-citation>
         </ref>
         <ref id="ref12">
            <mixed-citation publication-type="other">Cailleau, G., O. Braissant and E.P. Verrecchia. 2011. Turning sunlight into stone: the oxalate-carbonate pathway in a tropical tree ecosystem. Biogeosciences 8(7): 1755-1757.</mixed-citation>
         </ref>
         <ref id="ref13">
            <mixed-citation publication-type="other">Calme-Griaule, G. 1965. Ethnologie et langage: la parole chez les Dogon. Paris: Gallimard.</mixed-citation>
         </ref>
         <ref id="ref14">
            <mixed-citation publication-type="other">Chouvenc T., C.A. Efstathion, M.L. Elliott, and N-Y. Su. 2013. Extended disease resistance emerging from the faecal nest of a subterranean termite. Proceedings of the Royal Society B 280: 20131885: 1-9.</mixed-citation>
         </ref>
         <ref id="ref15">
            <mixed-citation publication-type="other">DeFoliart, G. N.d. Insects as Food. 2015. http://labs.russell.wisc.edu/insectsasfood/chapter 16. Accessed on July 28, 2015.</mixed-citation>
         </ref>
         <ref id="ref16">
            <mixed-citation publication-type="other">Dewey, John. 1920. Reconstruction in Philosophy. New York: H. Holt &amp; Co.</mixed-citation>
         </ref>
         <ref id="ref17">
            <mixed-citation publication-type="other">Dewey, John. 1929. Experience and Nature. London : Allen and Unwin.</mixed-citation>
         </ref>
         <ref id="ref18">
            <mixed-citation publication-type="other">Dieterlen, G. 1951. Essai sur la religion Bambara. Paris: Presses Universitaires de France.</mixed-citation>
         </ref>
         <ref id="ref19">
            <mixed-citation publication-type="other">Dieterlen, G. 1952. Classification des vegetaux chez les Dogon. Journal de la Societe des Africanistes 22(1): 115-158.</mixed-citation>
         </ref>
         <ref id="ref20">
            <mixed-citation publication-type="other">Dieterlen, G. and Y. Cisse. 1972. Les fondements de la societe d’initiation du Komo. Paris: Cahiers de l’Homme.</mixed-citation>
         </ref>
         <ref id="ref21">
            <mixed-citation publication-type="other">Fairhead, J. and M. Leach. 1996. Misreading the African Landscape: society and ecology in a forest-savanna mosaic. Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="ref22">
            <mixed-citation publication-type="other">Fairhead, J. and M. Leach. 2003a. Science, society and power: environmental knowledge and policy in West Africa and the Caribbean. Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="ref23">
            <mixed-citation publication-type="other">Fairhead, J. and M. Leach. 2003b. Termites, Society and Ecology: perspectives from West Africa. In: Insects in oral literature and traditions (eds. Motte-Florac, E. and J.M.C. Thomas). Societe d’Etudes Linguistiques et Anthropologiques de France (Book 407), Ethnosciences (11) Pp.197-219. Leuven: Peeters.</mixed-citation>
         </ref>
         <ref id="ref24">
            <mixed-citation publication-type="other">Fairhead, J., M. Leach, T. Geysbeek, and S. Holsoe. 2003. African American exploration in West Africa. Bloomington Indiana: Indiana University Press.</mixed-citation>
         </ref>
         <ref id="ref25">
            <mixed-citation publication-type="other">Frazer, J. 1930. Myths of the Origin of Fire. London: Macmillan.</mixed-citation>
         </ref>
         <ref id="ref26">
            <mixed-citation publication-type="other">Geissler, P.W., D. L. Mwaniki, F. Thiong’o, and H. Friis. 1997. Geophagy among school children in western Kenya. Tropical Medicine and International Health 2(7): 624-630.</mixed-citation>
         </ref>
         <ref id="ref27">
            <mixed-citation publication-type="other">Geissler, P.W. 2000. The significance of earth-eating: social and cultural aspects of Geophagy among Luo Children. Africa 70(4): 653-682.</mixed-citation>
         </ref>
         <ref id="ref28">
            <mixed-citation publication-type="other">Griaule, M. 1961. Classification des Insectes chez les Dogon. Journal de la Societe des Africanists 31(1): 7-71.</mixed-citation>
         </ref>
         <ref id="ref29">
            <mixed-citation publication-type="other">Griaule, M. 1975. Conversations with Ogotemelli. Oxford: Oxford University Press and the International African Institute.</mixed-citation>
         </ref>
         <ref id="ref30">
            <mixed-citation publication-type="other">Griaule, M. and G. Dieterlen. 1991. Le renard Pale. tome 1. Le mythe cosmogonique. 2nd edition. Paris : Travaux et Memoires de l’Institut d’Ethnologies 72.</mixed-citation>
         </ref>
         <ref id="ref31">
            <mixed-citation publication-type="other">Haraway, D. 2008. When species meet. Posthumanities volume 3. Minneapolis: University of Minnesota Press.</mixed-citation>
         </ref>
         <ref id="ref32">
            <mixed-citation publication-type="other">Horner, R.C., K. Lackey, K. Kolasa, and K. Warren. 1991. Pica practices of pregnant women. Journal of the American Dietetic Association 91(1): 34-38.</mixed-citation>
         </ref>
         <ref id="ref33">
            <mixed-citation publication-type="other">Huffman, R. 1929. Nuer-English Dictionary. Berlin: Dietrich Reimer (Ernst Vohsen).</mixed-citation>
         </ref>
         <ref id="ref34">
            <mixed-citation publication-type="other">Hunter, J. 1984. Insect clay geophagy in Sierre Leone. Journal of Cultural Geography 4(2): 2-13.</mixed-citation>
         </ref>
         <ref id="ref35">
            <mixed-citation publication-type="other">Hunter, J. 1993. Macroterme geophagy and pregnancy clays in southern Africa. Journal of Cultural Geography 14(1): 69-92.</mixed-citation>
         </ref>
         <ref id="ref36">
            <mixed-citation publication-type="other">Iroko, A.F. 1982. Le role des termitieres dans l’histoire des peuples de la Republique Populaire du Benin des origines a nos jours. Bulletin de l'I. F. A. N. 44(1-2): 50-75.</mixed-citation>
         </ref>
         <ref id="ref37">
            <mixed-citation publication-type="other">Iroko, A.F. 1996. L'homme et les termitieres enAfrique. Paris: Karthala.</mixed-citation>
         </ref>
         <ref id="ref38">
            <mixed-citation publication-type="other">Johns, T. and M. Duquette. 1991. Detoxification and Mineral Supplementation as Functions of Geophagy. American Journal of Clinical Nutrition 53(2): 448-456.</mixed-citation>
         </ref>
         <ref id="ref39">
            <mixed-citation publication-type="other">Kalumanga, E., S. Cousins, and D.G. Mpanduji. 2015. Geophagic termite mounds as one of the resources for African elephants in Ugalla Game Reserve, Western Tanzania. http://diva-portal.org/smash/record.jsf7pid =diva2°%3A806000&amp;dswid=-1102. Accessed on July 28, 2015.</mixed-citation>
         </ref>
         <ref id="ref40">
            <mixed-citation publication-type="other">Kibblewhite, M.G., S.J. Van Rensburg, M.C. Laker, and E.F. Rose. 1984. Evidence for an intimate geochemical factor in the etiology of esophageal cancer. Environmental Research 33(2): 370-378.</mixed-citation>
         </ref>
         <ref id="ref41">
            <mixed-citation publication-type="other">Kirksey, S.E. and S. Helmreich. 2010. The emergence of multispecies ethnography. Cultural Anthropology 25(4): 545-576.</mixed-citation>
         </ref>
         <ref id="ref42">
            <mixed-citation publication-type="other">Klaus, G. and B. Schmidg. 1998. Geophagy at natural licks and mammal ecology: a review. Mammalia 62(4): 482-98.</mixed-citation>
         </ref>
         <ref id="ref43">
            <mixed-citation publication-type="other">Krief, S.C., M.H. Daujeard, N. Moncel, N. Lamon, and V. Reynolds. 2015. Flavouring food: the contribution of chimpanzee behaviour to the understanding of Neanderthal calculus composition and the plant use in Neanderthal diets. Antiquity 89(344): 464-471.</mixed-citation>
         </ref>
         <ref id="ref44">
            <mixed-citation publication-type="other">Kumar, V., A. Bharti, V.K. Gupta, O. Gusain, and B.S. Bisht. 2012. Actinomycetes from solitary wasp mud nest and swallow bird mud nest: isolation and screening for their antibacterial activity. World Journal of Microbiology and Biotechnology 28(3): 871-880.</mixed-citation>
         </ref>
         <ref id="ref45">
            <mixed-citation publication-type="other">Leach, M. and J. Fairhead. 2007. Vaccine anxieties: global science, child health and society. London: Routledge.</mixed-citation>
         </ref>
         <ref id="ref46">
            <mixed-citation publication-type="other">Le Moal, G. 1980. Les Bobo: nature et fonction des masques. Travaux et Documents de L’O.R.S.T.O.M no. 121. Paris: ORSTOM.</mixed-citation>
         </ref>
         <ref id="ref47">
            <mixed-citation publication-type="other">Lopez-Hernandez, D., M. Brossard, J.C. Fardeau, and M. Lepage. 2006. Effect of different termite feeding groups on P sorption and P availability in African and South American savannas. Biology and fertility of soils 42(3): 207-214.</mixed-citation>
         </ref>
         <ref id="ref48">
            <mixed-citation publication-type="other">Madden, A.A., G.A Grassetti, J.A. Soriano, and P.T. Starks. 2013. Actinomycetes with antimicrobial activity isolated from paper wasp (Hymenoptera: Vespidae: Polistinae) nests. Environmental Entomology 42(4): 703-710.</mixed-citation>
         </ref>
         <ref id="ref49">
            <mixed-citation publication-type="other">McNaughton, PR. 1979. Secret sculptures of Komo: art and power in Bamana (Bambara) initiation associations. Philadelphia: Institute for the study of human issues, Working Papers in the Traditional Arts No. 4.</mixed-citation>
         </ref>
         <ref id="ref50">
            <mixed-citation publication-type="other">McNaughton, PR. 1988. TheMande Blacksmiths: knowledge, power, and art in West Africa. Bloomington and Indianapolis: Indiana University Press.</mixed-citation>
         </ref>
         <ref id="ref51">
            <mixed-citation publication-type="other">Meel, B.L. 2012. Geophagia in Transkei region of South Africa: case reports. African Health Sciences 12(4): 566-568.</mixed-citation>
         </ref>
         <ref id="ref52">
            <mixed-citation publication-type="other">Millimouno, D. 2015. Forthcoming. Rapport sur Meliandou.</mixed-citation>
         </ref>
         <ref id="ref53">
            <mixed-citation publication-type="other">Mondjannagni, A.C. 1975. Vie rurale et rapports ville-campagne dans le Bas-Dahomey. These pour le doctorat d’Etat es Lettres. Paris, 2 tomes. 720 pages.</mixed-citation>
         </ref>
         <ref id="ref54">
            <mixed-citation publication-type="other">Moore, H., T. Sanders, and B. Kaare. 1996. Those who play with fire: gender, fertility and transformation in East and Southern Africa. London: LSE.</mixed-citation>
         </ref>
         <ref id="ref55">
            <mixed-citation publication-type="other">Morris, B. 2006. Insects and Human Life. Oxford: Berg.</mixed-citation>
         </ref>
         <ref id="ref56">
            <mixed-citation publication-type="other">Morris, B. 2011. Medical herbalism in Malawi. Anthropology &amp; Medicine 18(2): 245-55.</mixed-citation>
         </ref>
         <ref id="ref57">
            <mixed-citation publication-type="other">Mujinya, B.B., F. Mees, P. Boeckx, S. Bode, G. Baert, H. Erens, S. Delefortrie, et al. 2011. The origins of carbonates in termite mounds of the Lubumbashi area, D. R. Congo. Geoderma 165(1): 95-105.</mixed-citation>
         </ref>
         <ref id="ref58">
            <mixed-citation publication-type="other">Nagaraju, A.K., S. Kumar, and A. Thejaswi. 2013. Distribution of chemical elements and certain rare earths in termite mounds: a case study from Nellore Mica Belt, Andhra Pradesh, India. World Environment 3(5): 174-182.</mixed-citation>
         </ref>
         <ref id="ref59">
            <mixed-citation publication-type="other">Ndiaye, V. and P. Clement. 1996. Le mythe de la guepe mat^onne, Trema 9-10. http://trema.revues.org/2044#tocto1n1. Accessed on July 28, 2015.</mixed-citation>
         </ref>
         <ref id="ref60">
            <mixed-citation publication-type="other">Njiru, H., U. Elchalal, and O. Paltiel. 2011. Geophagy during pregnancy in Africa: a literature review. Obstetrical &amp; Gynecological Survey 66(7): 452^59.</mixed-citation>
         </ref>
         <ref id="ref61">
            <mixed-citation publication-type="other">Oates, J.F. 1978. Water-plant and soil consumption by Guereza Monkeys (Colobus guereza): a relationship with minerals and toxins in the diet? Biotropica 10(4): 241-253.</mixed-citation>
         </ref>
         <ref id="ref62">
            <mixed-citation publication-type="other">Poulsen, M., D-C. Oh, J. Clardy, and C.R. Currie. 2011. Chemical analyses of wasp-associated Streptomyces bacteria reveal a prolific potential for natural products discovery. PLOS One 6(2): e16763. doi:10.1371/journal.pone.0016763.</mixed-citation>
         </ref>
         <ref id="ref63">
            <mixed-citation publication-type="other">Procopio, R.E de Lima, I.R. da Silva, M. Martins, J.L de Azevedo, and J.M de Araujo. 2012. Antibiotics produced by Streptomyces. The Brazilian Journal of Infectious Diseases 16(5): 466^71.</mixed-citation>
         </ref>
         <ref id="ref64">
            <mixed-citation publication-type="other">Saez, A.M., S. Weiss, K. Nowak, V. Lapeyre, F. Zimmermann, A. Dux, H. Kuhl, et al. 2015. Investigating the zoonotic origin of the West African Ebola epidemic. EMBO Molecular Medicine 7(1): 17-33.</mixed-citation>
         </ref>
         <ref id="ref65">
            <mixed-citation publication-type="other">Seymour, C.L., A.V. Milewski, A.J. Mills., G.S. Joseph, G.S. Cumming, D.H.M. Cumming, and Z. Mahlangu. 2014. Do the large termite mounds of Macrotermes concentrate micronutrients in addition to macronutrients in nutrient-poor African savannas. Soil Biology and Biochemistry 68(January 2014): 95-105.</mixed-citation>
         </ref>
         <ref id="ref66">
            <mixed-citation publication-type="other">Soohyun, U., A. Fraimout, P. Sapountzis, D.C. Oh, and M. Poulson. 2013. The fungus-growing termite Macrotermes natalensis harbors bacillaene-producing Bacillus sp. that inhibit potentially antagonistic fungi. Nature Scientific Reports 3250 (2013) doi:10.1038/srep03250.</mixed-citation>
         </ref>
         <ref id="ref67">
            <mixed-citation publication-type="other">Toupou. 1989. l’Histoire du pays Toma a travers les toponymes. Memoire de fin d’etudes superieures (Masters thesis). Universite Jules Nyerere de Kankan, Republique de Guinee.</mixed-citation>
         </ref>
         <ref id="ref68">
            <mixed-citation publication-type="other">Tsing, A. 2013. More-than-human sociality: a call for critical description. In: Anthropology and Nature (ed. Hastrup, K.). Pp. 27-42. London: Routledge.</mixed-citation>
         </ref>
         <ref id="ref69">
            <mixed-citation publication-type="other">Van Huis, A. 2003. Medical and stimulating properties ascribed to arthropods and their products in Sub-Saharan Africa. In: ‘Insects ’in oral literature and traditions (eds. Motte-Florac, E. and J.M.C. Thomas). Pp. 367-383. Leuven: Peeters Publishers.</mixed-citation>
         </ref>
         <ref id="ref70">
            <mixed-citation publication-type="other">Vermeer, D. 1966. Geophagy among the Tiv of Nigeria. Annals of the Association of American Geographers 56(2): 197-204.</mixed-citation>
         </ref>
         <ref id="ref71">
            <mixed-citation publication-type="other">Vermeer, D.E. and R.E. Ferrellir. 1985. Nigerian geophagical clay: a traditional antidiarrheal pharmaceutical. Science 227(4687): 634-36.</mixed-citation>
         </ref>
         <ref id="ref72">
            <mixed-citation publication-type="other">Watson, J. P. 1974. Calcium carbonate in termite mounds. Nature 247: 74.</mixed-citation>
         </ref>
         <ref id="ref73">
            <mixed-citation publication-type="other">Wiley, A.S. and S.H. Katz 1998. Geophagy in pregnancy: a test of a hypothesis. Current Anthropology 39(4): 532-545.</mixed-citation>
         </ref>
         <ref id="ref74">
            <mixed-citation publication-type="other">Young, S.L. 2011. Craving earth: understanding Pica—the urge to eat clay, starch, ice and chalk. New York: Columbia University Press.</mixed-citation>
         </ref>
         <ref id="ref75">
            <mixed-citation publication-type="other">Young, S.L., P.W. Sherman, J.B. Lucks, and G.H. Pelto. 2011. Why on earth? evaluating hypotheses about the physiological functions of human geophagy. The Quarterly Review of Biology 86(2): 97-120.</mixed-citation>
         </ref>
         <ref id="ref76">
            <mixed-citation publication-type="other">Zhang, Y.L., S. Li, Dong-hua Jiang, Li-chun Kong, Ping-hua Zhang, and Jie-dong Xu. 2013. Antifungal activities of metabolites produced by a termite-associated Streptomyces canus BYB02. Journal of agricultural and food chemistry 61(7): 1521-1524.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
