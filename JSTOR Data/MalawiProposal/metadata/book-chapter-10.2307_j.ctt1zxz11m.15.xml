<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1zxz11m</book-id>
      <subj-group subj-group-type="discipline">
         <subject>History</subject>
      </subj-group>
      <book-title-group>
         <book-title>A History of Britain</book-title>
         <subtitle>1945 to Brexit</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>BLACK</surname>
               <given-names>JEREMY</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>18</day>
         <month>08</month>
         <year>2017</year>
      </pub-date>
      <isbn content-type="ppub">9780253029720</isbn>
      <isbn content-type="epub">9780253030184</isbn>
      <isbn content-type="epub">0253030188</isbn>
      <publisher>
         <publisher-name>Indiana University Press</publisher-name>
         <publisher-loc>Bloomington, Indiana</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2017</copyright-year>
         <copyright-holder>Jeremy Black</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt1zxz11m"/>
      <abstract abstract-type="short">
         <p>In 2016, Britain stunned itself and the world by voting to pull out of the European Union, leaving financial markets reeling and global politicians and citizens in shock. But was Brexit really a surprise, or are there clues in Britain's history that pointed to this moment? In A History of Britain: 1945 to the Brexit, award-winning historian Jeremy Black reexamines modern British history, considering the social changes, economic strains, and cultural and political upheavals that brought Britain to Brexit. This sweeping and engaging book traces Britain's path through the destruction left behind by World War II, Thatcherism, the threats of the IRA, the Scottish referendum, and on to the impact of waves of immigration from the European Union. Black overturns many conventional interpretations of significant historical events, provides context for current developments, and encourages the reader to question why we think the way we do about Britain's past.</p>
      </abstract>
      <counts>
         <page-count count="276"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1zxz11m.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1zxz11m.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1zxz11m.3</book-part-id>
                  <title-group>
                     <title>PREFACE:</title>
                     <subtitle>FROM EMPIRE TO WHERE?</subtitle>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1zxz11m.4</book-part-id>
                  <title-group>
                     <title>PRIME MINISTERS FROM 1945</title>
                  </title-group>
                  <fpage>xiii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1zxz11m.5</book-part-id>
                  <title-group>
                     <title>ABBREVIATIONS</title>
                  </title-group>
                  <fpage>xv</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1zxz11m.6</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>ENVIRONMENT UNDER STRAIN</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>In writing on Britain in the late twentieth century and into the 2000s, the theme of the environment under strain principally referred, for much of the public, to “green” issues. This assessment reflected the extent to which these issues, and the related attitudes, both of which had developed from the 1960s, had been diffused more widely into the political community. In contrast, by the mid-2010s, there was, for many, a more specific and pointed concern, that of people, and more particularly their number. This concern related to anxieties about the consequences of both large-scale immigration and population aging due to</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1zxz11m.7</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>ECONOMY UNDER STRAIN</title>
                  </title-group>
                  <fpage>33</fpage>
                  <abstract>
                     <p>Britain, the country that invented the Industrial Revolution in the eighteenth century and became the “Workshop of the World” in the nineteenth, also became unwillingly “postindustrial” after 1945. This change had major consequences: for the country itself, for its society and culture as well as its economy, and for Britain’s relations with the rest of the world.</p>
                     <p>Problems had been long coming and were systemic. Nineteenth-century British industry had faced serious international competition, notably from rapid and large-scale industrialization in the United States and in Germany. By the late nineteenth century, having been passed by both, Britain was no longer</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1zxz11m.8</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>CHANGING SOCIETY</title>
                  </title-group>
                  <fpage>56</fpage>
                  <abstract>
                     <p>To British and foreign people alive in 1945 and able to revisit the country seventy years later (a group that was a decreasing percentage of the population), it is the changes in its society that would have appeared most extraordinary. The visual transformation, of the people in the streets, and notably in the cities, would have been joined by changes in their organization, assumptions, and sense of themselves. Britain and the British have both changed profoundly since 1945. These changes had a greater impact than the doings of politicians that generally dominate historical discussion, although they cannot be separated from</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1zxz11m.9</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>CHANGING CULTURE</title>
                  </title-group>
                  <fpage>92</fpage>
                  <abstract>
                     <p>As with environmental, economic, and social circumstances and developments, cultural counterparts showed a mixture of international trends and of distinctive national ones. As also with these other factors, there was a lessening of the distinctive national ones and a degree of homogenization at the international level. Whether described or discussed as Americanization or as globalization, and both indeed were at play, the homogenization can readily be seen as value laden. It was an aspect of the consumerism that was central to both Americanization and globalization.</p>
                     <p>Advertising played a major role in this consumerism, but advertising was not the only element.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1zxz11m.10</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>THE AFTER-ECHOES OF WAR, 1945–60</title>
                  </title-group>
                  <fpage>119</fpage>
                  <abstract>
                     <p>Before war with Japan ended in 1945, Britain had already elected a new government, returning the first majority Labour administration, with Clement Attlee as prime minister.¹ Winston Churchill, the wartime prime minister from 1940, and the Conservative Party he had led from then, were rejected. This result was in line with Gallup poll results from 1942 onward. It was a reaction against the 1930s and against the Conservatives as the party of privilege and prewar division. This was ironic as Churchill had been in the political wilderness during that period. The result was also an endorsement of the role of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1zxz11m.11</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>THE POLITICS OF CRISIS, 1961–79</title>
                  </title-group>
                  <fpage>132</fpage>
                  <abstract>
                     <p>The 1950s were a decade of prosperity after the postwar gloom, and, as a result, a period of reasonable optimism as far as the bulk of the population was concerned. One index was a relatively high birth rate that, in combination with the response to peace and demobilization in 1945, produced the postwar baby boomers. The 1960s and 1970s was far more mixed. For the young, these years, and especially 1968, might be one of optimism focused on personal liberation and a sense of social progress, and the period was subsequently to be celebrated in these terms. This was especially</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1zxz11m.12</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>THATCHERISM, 1979–90</title>
                  </title-group>
                  <fpage>151</fpage>
                  <abstract>
                     <p>The death of Margaret Thatcher in 2013 underlined the contentious nature of her legacy. The death also demonstrated the changing character of British society, being greeted with more division than that of any former prime minister. A state funeral in St Paul’s Cathedral attended, most unusually, by the Queen in a very public show of respect, contrasted with the vocal abuse circulating in, and from, some circles. This difference captured the ambiguities of Thatcher’s government and legacy, each of which bulk especially large in foreign attention. These ambiguities also relate to very different histories of Britain, and these histories focus</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1zxz11m.13</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>CHANGING DIRECTIONS, 1990–2016</title>
                  </title-group>
                  <fpage>169</fpage>
                  <abstract>
                     <p>Initially seen as a stop-gap until a likely Labour victory at the general election due in 1992 at the latest, John Major, a politician who had come through the middle to win the leadership, proved both more and less successful than might have been expected. More because he was prime minister for six and a half years, winning the 1992 general election and also seeing off challenges from within the Conservative Party. In the event, Major was prime minister for longer than Heath or Callaghan, or, indeed Eden, Douglas-Home, and Brown. He held on to power as Douglas-Home, having replaced</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1zxz11m.14</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>BRITISH ISSUES, 1945–2016</title>
                  </title-group>
                  <fpage>186</fpage>
                  <abstract>
                     <p>Britain has changed in many ways since 1945, but especially so in the British dimension of politics and public identity. In 1945, the nation was very much Britain, and the only significant institutional division occurred at the level of the United Kingdom. That title rested on the union between Ireland and Great Britain (or Britain for short). Northern Ireland, that section of Ireland that had remained with Britain and not followed the rest into the Irish Free State in 1922 (later Eire, the Republic of Ireland), had a separate Parliament in Belfast—Stormont—as well as a separate government. There</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1zxz11m.15</book-part-id>
                  <title-group>
                     <label>10</label>
                     <title>EUROPEAN AND WORLD QUESTIONS</title>
                  </title-group>
                  <fpage>198</fpage>
                  <abstract>
                     <p>In 1945, Britain was a world power able to support Western European unity but not to see its destiny in those terms. By 2016, there were overseas possessions, including the Falkland Islands, Ascension Island, and Saint Helena, but there was no empire. Moreover, Britain was an extremely uneasy member of the EU having voted to leave it. From the global perspective, the change from 1945 was the most significant aspect of British history in this period and notably the abandonment of empire. In 1945, this empire had included what is now India, Pakistan, Bangladesh, Sri Lanka, Myanmar, Malaysia, Singapore, Hong</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1zxz11m.16</book-part-id>
                  <title-group>
                     <label>11</label>
                     <title>INTO THE FUTURE</title>
                  </title-group>
                  <fpage>228</fpage>
                  <abstract>
                     <p>The referendum debates of 2014 and 2016, over Scottish independence and EU membership, respectively, revealed (to put it mildly) considerable division over the future of the country, division expressed in clashing uncertainties. These uncertainties comprised past, present, and future. Dissension over history was a key aspect of this uncertainty over the future, and this dissension reached to the present. Both the Labour government of Gordon Brown (2007–10), and its Conservative-dominated coalition successor under David Cameron (2010–15), sought to emphasize Britishness and to encourage the teaching of national history. However, the British national account, with its focus on freedom,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1zxz11m.17</book-part-id>
                  <title-group>
                     <label>12</label>
                     <title>CONCLUSIONS</title>
                  </title-group>
                  <fpage>234</fpage>
                  <abstract>
                     <p>Resolution, decline, and adaptability were key narratives in the accounts the British told about themselves. Initially, the theme was resolution. Germany and Japan had been defeated in a war that exhausted Britain and strained the cohesion of its empire, but a war, nevertheless, that had been won, one that underlined British accounts of their special character. The “finest hour,” the “band of brothers,” Britain “fighting on alone” all became key themes and were extensively repeated in the films of the late 1940s, 1950s, and early 1960s.</p>
                     <p>The hostile international environment was to continue with the threat from the Soviet Union</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1zxz11m.18</book-part-id>
                  <title-group>
                     <title>NOTES</title>
                  </title-group>
                  <fpage>243</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1zxz11m.19</book-part-id>
                  <title-group>
                     <title>SELECTED FURTHER READING</title>
                  </title-group>
                  <fpage>249</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1zxz11m.20</book-part-id>
                  <title-group>
                     <title>INDEX</title>
                  </title-group>
                  <fpage>251</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1zxz11m.21</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>269</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
