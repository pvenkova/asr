<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">jafricultstud</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100921</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Journal of African Cultural Studies</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Taylor &amp; Francis</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">13696815</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">14699346</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">40647475</article-id>
         <title-group>
            <article-title>A post-colonial and feminist reading of selected testimonies to trauma in post-liberation South Africa and Zimbabwe</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jessica</given-names>
                  <surname>Murray</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>6</month>
            <year>2009</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">21</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40028022</issue-id>
         <fpage>1</fpage>
         <lpage>21</lpage>
         <permissions>
            <copyright-statement>© 2009 Journal of African Cultural Studies</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:href="http://dx.doi.org/10.1080/13696810902986409"
                   xlink:title="an external site"/>
         <abstract>
            <p>This article explores the testimonial significance of Antjie Krog and Yvonne Vera's work by considering the extent to which their choice of literary fiction facilitates and enables the urgent political and social intervention that their texts undertake. Their work responds to the violence in the Zimbabwean and South African contexts from and about which they write. This violence, which is a recurring theme in their work, is physical as well as psychic and results in traumatized individual and collective identities that pose particular challenges to representation. The role that the witness to trauma plays is an active one that carries its own responsibility. The onus that rests on the witness is related to the traumatic nature of what is being testified to. The article provides a detailed exploration of the dynamics that are involved in the process of witnessing trauma. Since traumatic events cause an overflow of the cognitive system, it is not comprehensively experienced by the victims at the time when it occurs. It can only be fully ' known' in the aftermath of the event and then when it is being received by an empathetic listener (or reader). Vera and Krog use literature to enable the reader to endure the pain and difficulty that come with being an active participant in the creation of new knowledge when that knowledge concerns a traumatic event.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d506e116a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d506e123" publication-type="other">
Bill Ashcroft, Gareth Griffiths and Helen Tiffin (1995, 2)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d506e129" publication-type="other">
(Abrahamsen 2003,
195).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d506e138" publication-type="other">
"postcolonial" for
Apartheid itself (1996, 136).</mixed-citation>
            </p>
         </fn>
         <fn id="d506e148a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d506e155" publication-type="other">
Benedict Anderson's (1983, 15)</mixed-citation>
            </p>
         </fn>
         <fn id="d506e162a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d506e169" publication-type="other">
Thierry Bokanowski (2005)
especially pages 252-4.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d506e178" publication-type="other">
Juan Tutté (2004, 897-921)</mixed-citation>
            </p>
         </fn>
         <fn id="d506e185a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d506e192" publication-type="other">
Isabel Moore (2005, 90),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d506e198" publication-type="other">
Jenny
Edkins (2001, 15)</mixed-citation>
            </p>
         </fn>
         <fn id="d506e209a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d506e216" publication-type="other">
(Whitlock 2001, 207).</mixed-citation>
            </p>
         </fn>
         <fn id="d506e223a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d506e230" publication-type="other">
Mary Daly (1978, 435n).</mixed-citation>
            </p>
         </fn>
         <fn id="d506e237a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d506e244" publication-type="other">
Radstone (2001, 59-60).</mixed-citation>
            </p>
         </fn>
         <fn id="d506e251a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d506e260" publication-type="other">
Horace Campbell (2003, 7, 84, 276 and
290-2).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d506e269" publication-type="other">
Barbara Nussbaum's (2003, 2)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d506e276" publication-type="other">
Tsitsi Dangarembga's The Book of Not (2006)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d506e282" publication-type="other">
'Tiripo, kana makadini wo! I am well if you are all right too! [. . .] Everything
was reciprocal and so were we; we all knew it, so said it every day in our greetings' (2006, 65).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d506e291" publication-type="other">
Krog's poem ietter-poem lullaby for Ntombizana Atoo' (2006, 59)</mixed-citation>
            </p>
         </fn>
         <fn id="d506e298a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d506e305" publication-type="other">
Kyeong Hwangbo (2004, 222)</mixed-citation>
            </p>
         </fn>
         <fn id="d506e312a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d506e319" publication-type="other">
Krog and Vera. In his analysis
of J.M. Coetzee's Boyhood (1997)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d506e328" publication-type="other">
Alexandra Fuller's Don't Let's Go to the Dogs Tonight (2002),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d506e334" publication-type="other">
Tony Da Silva (2005, 472),</mixed-citation>
            </p>
         </fn>
         <fn id="d506e342a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d506e349" publication-type="other">
Echoing Silences and The Stone Virgins Chan (2005, 380)</mixed-citation>
            </p>
         </fn>
         <fn id="d506e356a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d506e363" publication-type="other">
Dennis Foster (2000, 747)</mixed-citation>
            </p>
         </fn>
         <fn id="d506e370a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d506e377" publication-type="other">
Reychler and Jacobs (2004, 4)</mixed-citation>
            </p>
         </fn>
         <fn id="d506e384a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d506e391" publication-type="other">
A Change of Tongue (2003, 148).</mixed-citation>
            </p>
         </fn>
         <fn id="d506e398a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d506e405" publication-type="other">
Linda Craft (1997, 22)</mixed-citation>
            </p>
         </fn>
         <fn id="d506e412a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d506e421" publication-type="other">
Beverley 2004, 37).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d506e427" publication-type="other">
David Stoll, Rigoberta Menchú and the Story of All Poor Guatamalans
(1999).</mixed-citation>
            </p>
         </fn>
         <fn id="d506e438a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d506e445" publication-type="other">
Felman
(1975)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d506e454" publication-type="other">
Hélène Cixous' essay 'Sorties' in The Newly Born Woman (1987).</mixed-citation>
            </p>
         </fn>
         <fn id="d506e461a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d506e468" publication-type="other">
Juliet Mitchell (1998, 122)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d506e474" publication-type="other">
Under the Tongue Zhizha spells 'duck' (1996, 96)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d506e480" publication-type="other">
'a e i o u' (1996, 82)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d506e487" publication-type="other">
'I remember all my letters' (1996, 82).</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d506e503a1310">
            <mixed-citation id="d506e507" publication-type="other">
Abrahamsen, Rita. 2003. African studies and the postcolonial challenge. African Affairs 102: 189-210.</mixed-citation>
         </ref>
         <ref id="d506e514a1310">
            <mixed-citation id="d506e518" publication-type="other">
Ahmed, Sara, and Jackie Stacey. 2001. Testimonial cultures: An introduction. Cultural Values 5, no. 1:
1-6.</mixed-citation>
         </ref>
         <ref id="d506e528a1310">
            <mixed-citation id="d506e532" publication-type="other">
Anderson, Benedict. 1983. Imagined communities: Reflections on the origin and spread of nationalism.
London: Verso.</mixed-citation>
         </ref>
         <ref id="d506e542a1310">
            <mixed-citation id="d506e546" publication-type="other">
Ashcroft, Bill, Gareth Griffiths, and Helen Tiffin. 1995. General introduction. In The postcolonial studies
reader, ed. Bill Ashcroft. Gareth Griffiths and Helen Tiffin. 1 -4. London: Routledee.</mixed-citation>
         </ref>
         <ref id="d506e557a1310">
            <mixed-citation id="d506e561" publication-type="other">
Battle, Michael. 1997. Reconciliation: The ubuntu theology of Desmond Tutu. Cleveland: Pilgrim Press.</mixed-citation>
         </ref>
         <ref id="d506e568a1310">
            <mixed-citation id="d506e572" publication-type="other">
Bennett, Jill. 2003. 'Tenebrae' after September 1 1 : Art, empathy and the global politics of belonging. In World
memory: Personal trajectories in global time, ed. Jill Bennett and Rosanne Kennedy, 177-94.
New York: Palgrave Macmillan.</mixed-citation>
         </ref>
         <ref id="d506e585a1310">
            <mixed-citation id="d506e589" publication-type="other">
Bennett, Jill. 2005. Empathie vision: Affect, trauma and contemporary art. Stanford: Stanford University
Press.</mixed-citation>
         </ref>
         <ref id="d506e599a1310">
            <mixed-citation id="d506e603" publication-type="other">
Bennett, Jill, and Rosanne Kennedy. 2003. Introduction. In World memory: Personal trajectories in global
time, ed. Jill Bennett and Rosanne Kennedy, 1-15. New York: Palgrave Macmillan.</mixed-citation>
         </ref>
         <ref id="d506e613a1310">
            <mixed-citation id="d506e617" publication-type="other">
Beverley, John. 2004. Testimonio: On the politics of truth. Minneapolis: University of Minnesota Press.</mixed-citation>
         </ref>
         <ref id="d506e624a1310">
            <mixed-citation id="d506e628" publication-type="other">
Bokanowski, Thierry. 2005. Variations on the concept of traumatism: Traumatism, traumatic, trauma.
International Journal of Psychoanalysis 86: 251-65.</mixed-citation>
         </ref>
         <ref id="d506e639a1310">
            <mixed-citation id="d506e643" publication-type="other">
Bryce, Jane. 2002. Interview with Yvonne Vera, 1 August 2000, Bulawayo, Zimbabwe: 'Survival is in the
mouth.' In Sign and taboo: Perspectives on the poetic fiction of Yvonne Vera, ed. Robert Muponde
and Mandi Taruvinga, 217-26. Harare, Zimbabwe: Weaver Press.</mixed-citation>
         </ref>
         <ref id="d506e656a1310">
            <mixed-citation id="d506e660" publication-type="other">
Bufacchi, Vittorio. 2005. Two concepts of violence. Political Studies Review 3: 193-204.</mixed-citation>
         </ref>
         <ref id="d506e667a1310">
            <mixed-citation id="d506e671" publication-type="other">
Campbell, Horace. 2003. Reclaiming Zimbabwe: The exhaustion of the patriarchal model of liberation.
Claremont, South Africa: David Philips Publishers.</mixed-citation>
         </ref>
         <ref id="d506e681a1310">
            <mixed-citation id="d506e685" publication-type="other">
Caruth, Cathy. 1995. Recapturing the past: Introduction. In Trauma: Explorations in memory, ed. Cathy
Caruth, 151-7. London: John Hopkins University Press.</mixed-citation>
         </ref>
         <ref id="d506e695a1310">
            <mixed-citation id="d506e699" publication-type="other">
Césaire, Aimé. 1994. From 'Discourse on colonialism'. In Colonial discourse and post-colonial theory: A
reader, ed. Laura Chrisman and Patrick Williams, 172-80. New York: Columbia University Press.</mixed-citation>
         </ref>
         <ref id="d506e709a1310">
            <mixed-citation id="d506e713" publication-type="other">
Chan, Stephen. 2005. The memory of violence: Trauma in the writings of Alexander Kanengoni and
Yvonne Vera and the idea of unreconciled citizenship in Zimbabwe. Third World Quarterly 26,
no. 2: 369-82.</mixed-citation>
         </ref>
         <ref id="d506e727a1310">
            <mixed-citation id="d506e731" publication-type="other">
Cixous, Hélène. 1987. The newly born woman. Manchester: U.P.</mixed-citation>
         </ref>
         <ref id="d506e738a1310">
            <mixed-citation id="d506e742" publication-type="other">
Coetzee, J.M. 1986. Foe. Harmondsworth, UK: Penguin.</mixed-citation>
         </ref>
         <ref id="d506e749a1310">
            <mixed-citation id="d506e753" publication-type="other">
---. 1997. Boyhood: Scenes from provincial life. New York: Viking.</mixed-citation>
         </ref>
         <ref id="d506e760a1310">
            <mixed-citation id="d506e764" publication-type="other">
Craft, Linda J. 1997. Novels of testimony and resistance from Central America. Gainesville, FL: University
Press of Florida.</mixed-citation>
         </ref>
         <ref id="d506e774a1310">
            <mixed-citation id="d506e778" publication-type="other">
Daly, Mary. 1978. Gyn/Ecology: The Metaethics of Radical Feminism. London: The Women s Press.</mixed-citation>
         </ref>
         <ref id="d506e785a1310">
            <mixed-citation id="d506e789" publication-type="other">
Dangarembga, Tsitsi. 2006. The book of not. Banbury, Oxfordshire: Ayebia Clarke Publishing Ltd.</mixed-citation>
         </ref>
         <ref id="d506e797a1310">
            <mixed-citation id="d506e801" publication-type="other">
Da Silva, Tony S. 2005. Narrating a white South Africa: Autobiography, race and history. Third World
Quarterly 26, no. 3: 471-8.</mixed-citation>
         </ref>
         <ref id="d506e811a1310">
            <mixed-citation id="d506e815" publication-type="other">
Durrani, Sam. 2004. Postcolonial narrative and the work of mourning: J.M.Coetzee, Wilson Harris, and
Toni Morrison. Albany: State University of New York.</mixed-citation>
         </ref>
         <ref id="d506e825a1310">
            <mixed-citation id="d506e829" publication-type="other">
Edkins, Jenny. 2001. If No Story is Possible: Trauma, Testimony and Biopolitics after Auschwitz. Paper
presented at the 2001 Hong Kong Convention ot international studies. nttpY/isanet.ccit.anzona.
edu/archive/edkins2.doc (accessed 27 August 2007).</mixed-citation>
         </ref>
         <ref id="d506e842a1310">
            <mixed-citation id="d506e846" publication-type="other">
---. 2003. Trauma and the memory of politics. Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d506e853a1310">
            <mixed-citation id="d506e857" publication-type="other">
Ells Howes, Dustin. 2003. The challenge of violence for political theory. Paper prepared for delivery at the
2003 Annual meeting of the American Political Science Association, httpv/www.allacademic.com/
meta/p63628_index.html (accessed 21 August 2005).</mixed-citation>
         </ref>
         <ref id="d506e870a1310">
            <mixed-citation id="d506e874" publication-type="other">
Felman, Shoshana. 1975. Women and madness: the critical phallacy. In Feminisms: An anthology of
literary theory and criticism, ed. Diane race Herndl and Robin R. Warhol, 7-20. Mew Brunswick,
New Jersey: Rutgers University Press.</mixed-citation>
         </ref>
         <ref id="d506e888a1310">
            <mixed-citation id="d506e892" publication-type="other">
---. 1992a. Education and crisis, or the vicissitudes of teaching. In Testimony: Crises of witnessing in
literature, psychoanalysis, and history, ed. Shoshana Felman and Don Laub, 1-56. London:
Routledge.</mixed-citation>
         </ref>
         <ref id="d506e905a1310">
            <mixed-citation id="d506e909" publication-type="other">
---. 1992b. Camus' The Plague, or a monument to witnessing. In Testimony: Crises of witnessing in
literature, psycnoanaiysis, ana nistory, ea. snosnana reúnan ana Don Laub, 93-119. JLonaon:
Routledge.</mixed-citation>
         </ref>
         <ref id="d506e922a1310">
            <mixed-citation id="d506e926" publication-type="other">
Felman, Shoshana, and Dori Laub. 1992. Foreword. In Testimony: Crises of witnessing in literature,</mixed-citation>
         </ref>
         <ref id="d506e933a1310">
            <mixed-citation id="d506e937" publication-type="other">
psychoanalysis, and history, ed. Shoshana Felman and Don Laub, xiii-xx. London: Routledge.
Foster, Dennis A. 2000. Trauma and memory. Contemporary Literature 41, no. 4: 740-7.</mixed-citation>
         </ref>
         <ref id="d506e947a1310">
            <mixed-citation id="d506e951" publication-type="other">
Freud, Sigmund. 1989. Beyond the pleasure pnnciple. In The Freud reader, ed. Peter Gay, 594-7. London:
Vintage.</mixed-citation>
         </ref>
         <ref id="d506e961a1310">
            <mixed-citation id="d506e965" publication-type="other">
Fuller, Alexandra. 2002. Don t let s go to the dogs tonight: An African childhood. New York: Random
House.</mixed-citation>
         </ref>
         <ref id="d506e976a1310">
            <mixed-citation id="d506e980" publication-type="other">
Gilmore, Leigh. 2001. The limits of autobiography: Trauma and testimony. London: Cornell University
Press.</mixed-citation>
         </ref>
         <ref id="d506e990a1310">
            <mixed-citation id="d506e994" publication-type="other">
Harding, Sandra. 1993. The racial economy of science: Toward a democratic future. Bloomington:
Indiana University Press.</mixed-citation>
         </ref>
         <ref id="d506e1004a1310">
            <mixed-citation id="d506e1008" publication-type="other">
Henri, Yazir. 2000. Where healing begins. In Looking back, reaching forward: Reflections on the truth
ana reconciliation commission in South Africa, ed. Charles Villa-Vicencio and wlihelm
Verwoerd, 166-73. Cape Town: University of cape Town Press.</mixed-citation>
         </ref>
         <ref id="d506e1021a1310">
            <mixed-citation id="d506e1025" publication-type="other">
Herman, Judith L. 1992. Trauma and recovery: Domestic abuse to political terror. London: Basic Books.</mixed-citation>
         </ref>
         <ref id="d506e1032a1310">
            <mixed-citation id="d506e1036" publication-type="other">
Hwangbo, Kyeong. 2004. Trauma, narrative, and the marginal sell in seiectea contemporary Amencan
novels, http://etd.icla.edu/UF/UFE0007302/hwangbo_k.paf (accessed 27 February 2006).</mixed-citation>
         </ref>
         <ref id="d506e1046a1310">
            <mixed-citation id="d506e1050" publication-type="other">
Kanengoni, Alexander. 1997. Echoing silences. Johannesburg: Heinemann.</mixed-citation>
         </ref>
         <ref id="d506e1058a1310">
            <mixed-citation id="d506e1062" publication-type="other">
Kaplan, Temma. 2U02. Reversing the shame and gendenng me memory, òigns: Journal of women in
Culture and Society 28, no. 1: 179-99.</mixed-citation>
         </ref>
         <ref id="d506e1072a1310">
            <mixed-citation id="d506e1076" publication-type="other">
Kennedy, Rosanne, and Jan T. Wilson. 2003. Constructing shared histories: Stolen generation testimony,
narrative therapy and address. In World memory: Personal trajectories in global time, ed. Jill
Bennett and Rosanne Kennedy, 119-39. New York: Palgrave Macmillan.</mixed-citation>
         </ref>
         <ref id="d506e1089a1310">
            <mixed-citation id="d506e1093" publication-type="other">
Krog, Antjie. 2003. A change of tongue. Johannesburg: Random House.</mixed-citation>
         </ref>
         <ref id="d506e1100a1310">
            <mixed-citation id="d506e1104" publication-type="other">
---. 2006. Body bereft, Roggebaai, South Africa: Umuzi.</mixed-citation>
         </ref>
         <ref id="d506e1111a1310">
            <mixed-citation id="d506e1115" publication-type="other">
LaCapra, Dominick. 2004. History in transit: Experience, identity, critical theory. London: Cornell
University Press.</mixed-citation>
         </ref>
         <ref id="d506e1125a1310">
            <mixed-citation id="d506e1129" publication-type="other">
Laub, Dori. 1992a. Bearing witness or the vicissitudes of listening. In Testimony: Crises of witnessing in
literature, psychoanalysis and history, ed. Shoshana Felman and Don Laub, 57-74. London:
Routledge.</mixed-citation>
         </ref>
         <ref id="d506e1143a1310">
            <mixed-citation id="d506e1147" publication-type="other">
---. 1992b. An event without a witness: Truth, testimony and survival. In Testimony: Crises of witnes-
sing in literature, psychoanalysis and history, ed. ahoshana relman ana Don Lauo, n-y¿. Lonaon:
Routledge.</mixed-citation>
         </ref>
         <ref id="d506e1160a1310">
            <mixed-citation id="d506e1164" publication-type="other">
Levi, Primo. 1989. The drowned and the saved. London: Abacus.</mixed-citation>
         </ref>
         <ref id="d506e1171a1310">
            <mixed-citation id="d506e1175" publication-type="other">
Libin. Mark. 2003. Can the subaltern be heard? Response and responsibility in 'South Africa's human
Spirit'. Textual Practice 17, no.: 119-40.</mixed-citation>
         </ref>
         <ref id="d506e1185a1310">
            <mixed-citation id="d506e1189" publication-type="other">
Leys, Ruth. 2000. Trauma: A genealogy. Chicago, IL: University of Chicago Press.</mixed-citation>
         </ref>
         <ref id="d506e1196a1310">
            <mixed-citation id="d506e1200" publication-type="other">
Meinig, Sigrun. 2004. Witnessing the past: History and post-colonialism in Australian historical novels.
Tübingen, Germany: Günter Narr Verlag.</mixed-citation>
         </ref>
         <ref id="d506e1210a1310">
            <mixed-citation id="d506e1214" publication-type="other">
Menchú, Rigoberta. 1984. /, Rigoberta Menchú: An Indian woman in Guatemala, ed. Elisabeth Burgos-
Dehrav. Trans. Ann Wrieht. London: Verso.</mixed-citation>
         </ref>
         <ref id="d506e1225a1310">
            <mixed-citation id="d506e1229" publication-type="other">
Miller, Nancy K., and Jason D. Tougaw. 2002. Introduction. In Extremities: Trauma, testimony and
community, ed. Nancy K. Miller and Jason D. Tougaw, 1-25. Chicago: University of Illinois Press.</mixed-citation>
         </ref>
         <ref id="d506e1239a1310">
            <mixed-citation id="d506e1243" publication-type="other">
Mitchell, Juliet. 1998. Trauma, recognition, and the place of language. Diacritics 28, no. 4: 121-33.</mixed-citation>
         </ref>
         <ref id="d506e1250a1310">
            <mixed-citation id="d506e1254" publication-type="other">
Mizen, Richard. 2003. A contribution to an analytic theory of violence. Journal of Analytical Psychology
48: 285-305.</mixed-citation>
         </ref>
         <ref id="d506e1264a1310">
            <mixed-citation id="d506e1268" publication-type="other">
Moore, Isabel A. 2005. 'Speak, you also': Encircling trauma. Journal for Cultural Research 9, no. 1: 87-99.</mixed-citation>
         </ref>
         <ref id="d506e1275a1310">
            <mixed-citation id="d506e1279" publication-type="other">
Nancy, Naples A. 2003. Deconstructing and locating survivor discourse: Dynamics of narrative, empow-
erment, and resistance for survivors of childhood sexual abuse. Signs: Journal of Women in
Culture and Society 28, no. 4: 1151-85.</mixed-citation>
         </ref>
         <ref id="d506e1292a1310">
            <mixed-citation id="d506e1296" publication-type="other">
Ndebele, Njabulo. 1994. Liberation and the crisis of culture. In Altered state? Writing and South Africa, ed.
Elleke Boehmer, Laura Chrisman and Kenneth Parker, 1-11. Sydney: Dangaroo Press.</mixed-citation>
         </ref>
         <ref id="d506e1307a1310">
            <mixed-citation id="d506e1311" publication-type="other">
Nourbese Philip, Marlene. 1989. She tries her tongue, her silence softly breaks. Charlottetown, Prince
Edward Island, Canada: Ragweed Press.</mixed-citation>
         </ref>
         <ref id="d506e1321a1310">
            <mixed-citation id="d506e1325" publication-type="other">
Nussbaum, Barbara. 2003. Ubuntu. Resurgence 221. http^/www.resurgence.org/resurgence/issues/
nussbaum221.htm (accessed 1 August 2007).</mixed-citation>
         </ref>
         <ref id="d506e1335a1310">
            <mixed-citation id="d506e1339" publication-type="other">
Radstone, Susannah. 2001. Social bonds and psychical order: Testimonies. Cultural Values 5, no. 1: 59-78.</mixed-citation>
         </ref>
         <ref id="d506e1346a1310">
            <mixed-citation id="d506e1350" publication-type="other">
Reychler, Luc, and Michèle Jacobs. 2004. Limits to violence: Towards a comprehensive violence audit.
Centre for Peace Research and Strategic Studies. http://soc.kuleuven.be/iieb/CPRS/cahiers/
Vol%2068.ndf faccessed 21 August 2005V</mixed-citation>
         </ref>
         <ref id="d506e1363a1310">
            <mixed-citation id="d506e1367" publication-type="other">
Ross, Fiona C. 2003. Bearing witness to ripples of pa. In World memory: Personal trajectories in global
time, ed. Jill Bennett and Rosanne Kennedy, 143-59. New York: Palgrave Macmillan.</mixed-citation>
         </ref>
         <ref id="d506e1377a1310">
            <mixed-citation id="d506e1381" publication-type="other">
Saporta Sternbach, Nancy. 1991. Re-membering the dead: Latin American women's 'testimonial'
discourse. Latin American Perspective 18, no. 3: 91-102.</mixed-citation>
         </ref>
         <ref id="d506e1392a1310">
            <mixed-citation id="d506e1396" publication-type="other">
Scarry, Elaine. 1985. The body in pain: The making and unmaking of the world. Oxford: Oxford University
Press.</mixed-citation>
         </ref>
         <ref id="d506e1406a1310">
            <mixed-citation id="d506e1410" publication-type="other">
---. 1988. Introduction. In Literature and the body: Essays on populations and persons, ed. Elaine
Scarry, xi-xxii. London: Johns Hopkins University Press.</mixed-citation>
         </ref>
         <ref id="d506e1420a1310">
            <mixed-citation id="d506e1424" publication-type="other">
Spearey, Susan. 2000. Displacement, dispossession and conciliation: The politics and poetics of homecom-
ing in Antjie Krog's 'Country of my Skull'. Scrutiny 25, no. 1: 64-77. http://www.uwc.ac.za/arts/
english/LAMP/spearey.html (accessed 28 March 2007).</mixed-citation>
         </ref>
         <ref id="d506e1437a1310">
            <mixed-citation id="d506e1441" publication-type="other">
Stoll, David. 1999. Rigoberta Menchu and the story of all poor Guatemalans. Boulder, CO: Westview
Press.</mixed-citation>
         </ref>
         <ref id="d506e1451a1310">
            <mixed-citation id="d506e1455" publication-type="other">
Tal, Kali. 1996. Worlds of hurt: Reading the literature of trauma. Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d506e1462a1310">
            <mixed-citation id="d506e1466" publication-type="other">
Tambulasi, Richard, and Happy Kayuni. 2005. Can African feet divorce Western shoes? The case of
'ubuntu' and democratic good governance in Malawi. Nordic Journal of African Studies 14, no. 2:
147-61.</mixed-citation>
         </ref>
         <ref id="d506e1480a1310">
            <mixed-citation id="d506e1484" publication-type="other">
Tanner, Laura E. 1994. Intimate violence: Reading rape and torture in twentieth-century fiction.
Indianapolis: Indiana Universitv Press.</mixed-citation>
         </ref>
         <ref id="d506e1494a1310">
            <mixed-citation id="d506e1498" publication-type="other">
Taylor, Diana. 1997. Disappearing acts: Spectacles of gender and nationalism in Argentina's 'dirty war'.
Durham, North Carolina: Duke University Press.</mixed-citation>
         </ref>
         <ref id="d506e1508a1310">
            <mixed-citation id="d506e1512" publication-type="other">
Thornton, Robert. 1996. The potential of boundaries in South Africa: Steps towards a theory of the social
edge. In Postcolonial identities in Africa, ed. Richard Werbner and Terence Ranger, 136-61.
London: Zed Books.</mixed-citation>
         </ref>
         <ref id="d506e1525a1310">
            <mixed-citation id="d506e1529" publication-type="other">
Tutte, Juan C. 2004. The concept of psychical trauma: a bridge in interdisciplinary space. International
Journal of Psychoanalysis 85: 897-921.</mixed-citation>
         </ref>
         <ref id="d506e1539a1310">
            <mixed-citation id="d506e1543" publication-type="other">
Vera, Yvonne. 1996. Under the tongue. Harare, Zimbabwe: Baobab Books.</mixed-citation>
         </ref>
         <ref id="d506e1550a1310">
            <mixed-citation id="d506e1554" publication-type="other">
Whitehead, Anne. 2004. Trauma fiction. Edinburgh: Edinburgh University Press.</mixed-citation>
         </ref>
         <ref id="d506e1562a1310">
            <mixed-citation id="d506e1566" publication-type="other">
Whitlock, Gillian. 2001. In the second person: Narrative transactions in stolen generation testimony.
Biography 24, no. 1: 197-215.</mixed-citation>
         </ref>
         <ref id="d506e1576a1310">
            <mixed-citation id="d506e1580" publication-type="other">
Young, James E. 1988. Writing and rewriting the Holocaust. Bloomington: Indiana University Press.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
