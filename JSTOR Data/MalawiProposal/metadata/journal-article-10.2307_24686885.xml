<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">waterlines</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50017770</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Waterlines</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Practical Action Publishing</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">02628104</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">17563488</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">24686885</article-id>
         <title-group>
            <article-title>Menstrual management in low-income countries: needs and trends</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>ASHWINI</given-names>
                  <surname>SEBASTIAN</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>VIVIAN</given-names>
                  <surname>HOFFMANN</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>SARAH</given-names>
                  <surname>ADELMAN</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>4</month>
            <year>2013</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">32</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i24686878</issue-id>
         <fpage>135</fpage>
         <lpage>153</lpage>
         <permissions>
            <copyright-statement>Copyright © Practical Action Publishing 2013</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/24686885"/>
         <abstract>
            <p>Access to appropriate facilities for menstrual hygiene and management (MHM) is recognized as a neglected need within the sanitation sector globally. However, little is known about the magnitude or trajectory of this need. Further, the particular services and facilities required to meet MHM needs depend on the practices and products employed by women and girls. We use nationally representative data from 58 low and lower-middle income countries to estimate that 800 million women in such countries menstruate regularly. Positive correlations between menstruation and nutritional status, as well as menstruation and household wealth, imply a growing need for menstrual management solutions as poverty declines, but both effects are dwarfed by the impact of declining fertility rates. Data on imports of disposable menstrual products reveal exponential growth in their use in low and especially lower-middle income countries. This trend is likely to continue as incomes grow and markets develop, making ever greater the need for provision of appropriate disposal facilities.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d1254e229a1310">
            <mixed-citation id="d1254e233" publication-type="other">
Allen, L. (1997) 'Pregnancy and iron deficiency: unresolved issues', Nutrition Reviews 55:
91-101. &lt;http://dx.doi.org/10.1201/9781420036787.ch2&gt;</mixed-citation>
         </ref>
         <ref id="d1254e243a1310">
            <mixed-citation id="d1254e247" publication-type="other">
Allen, L. and Casterline-Sabel, J. (2001) 'Prevalence and causes of nutritional anemias', in
U. Ramakrishnan (ed.), Nutritional Anemias, Chapter 2, Boca Raton, FL: CRC Press</mixed-citation>
         </ref>
         <ref id="d1254e257a1310">
            <mixed-citation id="d1254e261" publication-type="other">
Bharadwaj, S. and Patkar, A. (2004) 'Menstrual hygiene and management in developing
countries: taking stock' [online], Museum of Menstruation &amp; Women's Health, &lt;www.mum.
org/menhydev.htm&gt; [accessed 27 February 2013].</mixed-citation>
         </ref>
         <ref id="d1254e274a1310">
            <mixed-citation id="d1254e278" publication-type="other">
Crofts, T. and Fisher, J. (2012) 'Menstrual hygiene in Ugandan schools: an investigation of
low-cost sanitary pads', Journal of Water, Sanitation and Hygiene for Development 2: 50-58.</mixed-citation>
         </ref>
         <ref id="d1254e289a1310">
            <mixed-citation id="d1254e293" publication-type="other">
Garg, R., Goyal, S. and Gupta, S. (2012) 'India moves towards menstrual hygiene: subsidized
sanitary napkins for rural adolescent girls - issues and challenges', Maternal and Child Health
Journal 16 (4): 767-74. &lt;http://dx.doi.org/10.1007/sl0995-011-0798-5&gt;</mixed-citation>
         </ref>
         <ref id="d1254e306a1310">
            <mixed-citation id="d1254e310" publication-type="other">
Harlow, S.D. and Campbell, O.M.R. (2000) 'Menstrual dysfunction: a missed opportunity for
improving reproductive health in developing countries', Reproductive Health Matters 8: 142-57.
&lt;www.jstor.org/stable/3775199&gt;</mixed-citation>
         </ref>
         <ref id="d1254e323a1310">
            <mixed-citation id="d1254e327" publication-type="other">
Harlow, S.D. and Campbell, O.M.R. (2004) 'Epidemiology of menstrual disorders in developing
countries: a systematic review', International Journal of Obstetrics and Gynecology 111: 6-16.
&lt;http://dx.doi.Org/10.llll/j.1471-0528.2004.00012.x&gt;</mixed-citation>
         </ref>
         <ref id="d1254e340a1310">
            <mixed-citation id="d1254e344" publication-type="other">
Kjellen, M., Pensulo, C., Nordqvist, P. and Fodge, M. (2011) Global Review of Sanitation System
Trends and Interactions with Menstrual Management Practices. Report for the Menstrual Management
and Sanitation Systems Project, Stockholm: Stockholm Environment Institute.</mixed-citation>
         </ref>
         <ref id="d1254e357a1310">
            <mixed-citation id="d1254e361" publication-type="other">
Northern Cape Provincial Government (2010) 'Premier launches sanitary towels/pads
campaign' [online], press release &lt;www.northern-cape.gov.za/index.php?option=com conten
t&amp;view=article&amp;id=405:premier-launches-sanitary-towelspads-campaign&amp;catid=42:latestnews
&amp;Itemid=91&gt; [accessed 27 February 2013].</mixed-citation>
         </ref>
         <ref id="d1254e377a1310">
            <mixed-citation id="d1254e381" publication-type="other">
Oster, E. and Thornton, R. (2009) Determinants of Technology Adoption: Private Value and Peer
Effects in Menstrual Cup Take-Up, National Bureau of Economic Research Working Paper 14828,
Cambridge, MA: NBER.</mixed-citation>
         </ref>
         <ref id="d1254e395a1310">
            <mixed-citation id="d1254e399" publication-type="other">
Proctor &amp; Gamble (2010) 2010 Annual Report [online] &lt;http://annualreport.pg.com/annual-
report2010/touching-and-improving-lives/index.shtml&gt; [accessed 27 February 2013].</mixed-citation>
         </ref>
         <ref id="d1254e409a1310">
            <mixed-citation id="d1254e413" publication-type="other">
Scott, L., Dopson, S., Montgomery, P., Dolan, C. and Ryus, C. (2009) Impact of Providing Sanitary
Pads to Poor Girls in Africa, Oxford: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d1254e423a1310">
            <mixed-citation id="d1254e427" publication-type="other">
Seymour, K. (2008) Bangladesh: Tackling Menstrual Hygiene Taboos, UNICEF Sanitation and
Hygiene, Case #10, New York: UNICEF.</mixed-citation>
         </ref>
         <ref id="d1254e437a1310">
            <mixed-citation id="d1254e441" publication-type="other">
Sommer, M. (2010) 'Putting menstrual hygiene management on to the school water and
sanitation agenda', Waterlines 29 (4): 268-78. &lt;http://dx.doi.org.proxy-um.researchport.umd.
edu/10.3362/1756-3488.2010.030&gt;</mixed-citation>
         </ref>
         <ref id="d1254e454a1310">
            <mixed-citation id="d1254e458" publication-type="other">
Sommer, M. (2011) Global Review of Menstrual Beliefs and Behaviors in Low Income Countries:
Implications for Menstrual Hygiene Management, report prepared for Menstrual Management and
Sanitation Systems, University of Maryland, College Park, MD: University of Maryland.</mixed-citation>
         </ref>
         <ref id="d1254e471a1310">
            <mixed-citation id="d1254e475" publication-type="other">
SWETA (2010) Operational Guidelines, Promotion of Menstrual Hygiene among adolescent girls
(10-19 years) in Rural Areas, New Delhi, India: National Rural Health Mission.</mixed-citation>
         </ref>
         <ref id="d1254e486a1310">
            <mixed-citation id="d1254e490" publication-type="other">
Thomas, F., Renaud, F., Benefice, E., De Meeus, T. and Guegan, J-F. (2001) 'International
variability of ages at menarche and menopause: patterns and main determinants', Journal of
Human Biology 73: 271-90. &lt;http://dx.doi.org/10.1353/hub.2001.0029&gt;</mixed-citation>
         </ref>
         <ref id="d1254e503a1310">
            <mixed-citation id="d1254e507" publication-type="other">
The Tribune (2011) 'One-rupee sanitary napkin for mral girls: Phase I of the scheme will cover
1.5 cr girls in 152 districts of 20 states' [online], Tribune News Service, &lt;www.tribuneindia.
com/2011/20110518/main8.htm&gt; [accessed 27 February 2013].</mixed-citation>
         </ref>
         <ref id="d1254e520a1310">
            <mixed-citation id="d1254e524" publication-type="other">
WHO/UNICEF (2012) 'Progress on drinking water and sanitation: 2012 update', New York, NY:
WHO/UNICEF Joint Monitoring Programme for Water Supply and Sanitation, cwww.unicef.
org/media/files/JMPreport2012.pdf&gt;</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
