<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">worlbankeconrevi</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j101235</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>The World Bank Economic Review</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Oxford University Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">02586770</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">1564698X</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">40891368</article-id>
         <title-group>
            <article-title>Natural Disasters and Human Capital Accumulation</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jesus Crespo</given-names>
                  <surname>Cuaresma</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>1</month>
            <year>2010</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">24</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40039832</issue-id>
         <fpage>280</fpage>
         <lpage>302</lpage>
         <permissions>
            <copyright-statement>COPYRIGHT © 2010 The International Bank for Reconstruction and Development/THE WORLD BANK</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/40891368"/>
         <abstract>
            <p>The empirical literature on the relationship between natural disaster risk and investment in education is inconclusive. Model averaging methods in a framework of crosscountry and panel regressions show an extremely robust negative partial correlation between secondary school enrollment and natural disaster risk. This result is driven exclusively by geologic disasters. Exposure to natural disaster risk is a robust determinant of differences in secondary school enrollment between countries but not necessarily within countries Natural disasters, human capital, education, school enrollment, Bayesian model averaging</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d1421e189a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1421e196" publication-type="other">
Okuyama (2009)</mixed-citation>
            </p>
         </fn>
         <fn id="d1421e203a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1421e210" publication-type="other">
Skidmore (2001)</mixed-citation>
            </p>
         </fn>
         <fn id="d1421e217a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1421e224" publication-type="other">
(CRED 2004).</mixed-citation>
            </p>
         </fn>
         <fn id="d1421e231a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1421e238" publication-type="other">
Raftery (1995)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1421e244" publication-type="other">
Clyde and George (2004)</mixed-citation>
            </p>
         </fn>
         <fn id="d1421e252a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d1421e259" publication-type="other">
Fernandez, Ley, and Steel (2001a)</mixed-citation>
            </p>
         </fn>
         <fn id="d1421e266a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d1421e273" publication-type="other">
Skidmore and Toya (2002)</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d1421e289a1310">
            <mixed-citation id="d1421e293" publication-type="other">
Albala-Bertrand, J. 1993a. "Natural Disaster Situations and Growth: A Macroeconomic Model for
Sudden Disaster Impacts." World Development 21: 1417-34.</mixed-citation>
         </ref>
         <ref id="d1421e303a1310">
            <mixed-citation id="d1421e307" publication-type="other">
---. 1993b. Political Economy of Large Natural Disasters. Oxford, UK: Clarendon Press.</mixed-citation>
         </ref>
         <ref id="d1421e314a1310">
            <mixed-citation id="d1421e318" publication-type="other">
Asian Development Bank and World Bank. 2005. "Pakistan 2005 Earthquake Preliminary Damage and
Needs Assessment." Asian Development Bank and World Bank, Islamabad.</mixed-citation>
         </ref>
         <ref id="d1421e328a1310">
            <mixed-citation id="d1421e332" publication-type="other">
Checchi, D., and C. Garcia-Penalosa. 2004. "Risk and the Distribution of Human Capital." Economics
Letters 82: 53-61.</mixed-citation>
         </ref>
         <ref id="d1421e343a1310">
            <mixed-citation id="d1421e347" publication-type="other">
Chipman, H.A. 1996. "Bayesian Variable Selection with Related Predictors." Canadian Journal of
Statistics 24: 17-36.</mixed-citation>
         </ref>
         <ref id="d1421e357a1310">
            <mixed-citation id="d1421e361" publication-type="other">
Clyde, M.A., and E.I. George. 2004. "Model Uncertainty." Statistical Science 19: 81-94.</mixed-citation>
         </ref>
         <ref id="d1421e368a1310">
            <mixed-citation id="d1421e372" publication-type="other">
CRED (Collaborating Centre for Research on the Epidemiology of Disasters, a World Health
Organization Collaborating Centre). 2004. Emergency Events Database, EM-DAT. Université
Catholique de Louvain, Brussels. Available at http://www.emdat.be/.</mixed-citation>
         </ref>
         <ref id="d1421e385a1310">
            <mixed-citation id="d1421e389" publication-type="other">
Crespo Cuaresma, J. Forthcoming. "How Different Is Africa?". Journal of Applied Econometrics.</mixed-citation>
         </ref>
         <ref id="d1421e396a1310">
            <mixed-citation id="d1421e400" publication-type="other">
Crespo Cuaresma, J., G. Doppelhofer, and M. Feldkircher. 2008. "The Determinants of Economic
Growth in European Regions." Working Paper 2008-26. Faculty of Economics and Statistics,
University of Innsbruck.</mixed-citation>
         </ref>
         <ref id="d1421e413a1310">
            <mixed-citation id="d1421e417" publication-type="other">
Crespo Cuaresma, J., J. Hlouskova, and M. Obersteiner. 2008. "Natural Disasters as Creative
Destruction: Evidence from Developing Countries." Economic Inquiry 46: 214-26.</mixed-citation>
         </ref>
         <ref id="d1421e428a1310">
            <mixed-citation id="d1421e432" publication-type="other">
Dacy, D.C., and H.C. Kunreuther. 1969. The Economics of Natural Disasters. New York: Free Press.</mixed-citation>
         </ref>
         <ref id="d1421e439a1310">
            <mixed-citation id="d1421e443" publication-type="other">
Duryea, S., D. Lamb, and D. Levison. 2007. "Effects of Economic Shocks on Children's Employment
and Schooling in Brazil." Journal of Development Economics 84: 188-214.</mixed-citation>
         </ref>
         <ref id="d1421e453a1310">
            <mixed-citation id="d1421e457" publication-type="other">
Fernandez, C, E. Ley, and M.F. Steel. 2001a. "Benchmark Priors for Bayesian Model Averaging."
Journal of Econometrics 100: 381-427.</mixed-citation>
         </ref>
         <ref id="d1421e467a1310">
            <mixed-citation id="d1421e471" publication-type="other">
---. 2001b. "Model Uncertainty in Cross-Country Growth Regressions." Journal of Applied
Econometrics 16: 563-76.</mixed-citation>
         </ref>
         <ref id="d1421e481a1310">
            <mixed-citation id="d1421e485" publication-type="other">
Flug, Κ., A. Spilimbergo, and Ε. Wachtenheim. 1998. "Investment in Education: Do Economic
Volatility and Credit Constraints Matter?" Journal of Development Economics 55: 465-81.</mixed-citation>
         </ref>
         <ref id="d1421e495a1310">
            <mixed-citation id="d1421e499" publication-type="other">
Foster, D.P., and E.I. George. 1994. "The Risk Inflation Criterion for Multiple Regression." Annals of
Statistics 22: 1947-75.</mixed-citation>
         </ref>
         <ref id="d1421e510a1310">
            <mixed-citation id="d1421e514" publication-type="other">
Hanushek, E.A., J.F. Kain, and S.G. Rivkin. 2004. "Disruption versus Tiebout Improvement: The Costs
and Benefits of Switching Schools." Journal of Public Economics 88: 1721-46.</mixed-citation>
         </ref>
         <ref id="d1421e524a1310">
            <mixed-citation id="d1421e528" publication-type="other">
Heston, Α., R. Summers, and B. Aten. 2006. Penn World Table Version 6.2. University of Pennsylvania,
Center for International Comparisons of Production, Income, and Prices, Philadelphia, Pa. Available
at http://pwt.econ.upenn.edu.</mixed-citation>
         </ref>
         <ref id="d1421e541a1310">
            <mixed-citation id="d1421e545" publication-type="other">
Gertler, P., D.I. Levine, and M. Ames. 2004. Schooling and Parental Death. Review of Economics and
Statistics 86:211-25.</mixed-citation>
         </ref>
         <ref id="d1421e555a1310">
            <mixed-citation id="d1421e559" publication-type="other">
Guha-Sapir, D., and R. Below. 2002. "The Quality and Accuracy of Disaster Data: A Comparative
Analysis of 3 Global Data Sets." Working paper prepared for the Disaster Management Facility.
World Bank and Collaborating Centre for Research on the Epidemiology of Disasters, Brussels.</mixed-citation>
         </ref>
         <ref id="d1421e572a1310">
            <mixed-citation id="d1421e576" publication-type="other">
Kass, R.E., and A.E. Raftery. 1995. "Bayes Factors." Journal of the American Statistical Association 90,
773-795.</mixed-citation>
         </ref>
         <ref id="d1421e586a1310">
            <mixed-citation id="d1421e590" publication-type="other">
Kass, R.E., and L. Wasserman. 1995. "A Reference Bayesian Test for Nested Hypotheses and Its
Relationship to the Schwarz Criterion." Journal of the American Statistical Association 90: 928-34.</mixed-citation>
         </ref>
         <ref id="d1421e601a1310">
            <mixed-citation id="d1421e605" publication-type="other">
Kim, N. 2008. "Impact of Extreme Climate Events on Educational Attainment: Evidence from Cross
Section Data and Welfare Projection." UNDP/ODS Working Paper. United Nations Development
Programme, New York.</mixed-citation>
         </ref>
         <ref id="d1421e618a1310">
            <mixed-citation id="d1421e622" publication-type="other">
Ley, E., and M.F. Steel. 2009. "On the Effect of Prior Assumptions in Bayesian Model Averaging with
Applications to Growth Regression." Journal of Applied Econometrics 24: 651-74.</mixed-citation>
         </ref>
         <ref id="d1421e632a1310">
            <mixed-citation id="d1421e636" publication-type="other">
Masanjala, W.H., and C. Papageorgiou. 2008. "Rough and Lonely Road to Prosperity: A
Reexamination of the Sources of Growth in Africa Using Bayesian Model Averaging." Journal of
Applied Econometrics 23: 671-82.</mixed-citation>
         </ref>
         <ref id="d1421e649a1310">
            <mixed-citation id="d1421e653" publication-type="other">
Marshall, M.G., and K. Jaggers. 1995. Polity IV Project. Political Regime Characteristics and
Transition, 1800-2004, database. Version 2005. www.systemicpeace.org/polity/polity4.htm.</mixed-citation>
         </ref>
         <ref id="d1421e663a1310">
            <mixed-citation id="d1421e667" publication-type="other">
Noy, I. 2009. "The Macroeconomic Consequences of Disasters." Journal of Development Economics
88 (2): 221-31.</mixed-citation>
         </ref>
         <ref id="d1421e677a1310">
            <mixed-citation id="d1421e681" publication-type="other">
Okuyama, Y. 2009. "Critical Review of Methodologies on Disaster Impact Estimation." Background
paper for the joint World Bank-United Nations Assessment on the Economics of Disaster Risk
Reduction. The Global Facility for Disaster Reduction and Recovery, Washington, DC.</mixed-citation>
         </ref>
         <ref id="d1421e695a1310">
            <mixed-citation id="d1421e699" publication-type="other">
Raftery, A. E. 1995. "Bayesian Model Selection for Social Research." Sociological Methodology 25:
111-63.</mixed-citation>
         </ref>
         <ref id="d1421e709a1310">
            <mixed-citation id="d1421e713" publication-type="other">
Rasmussen, T. N. 2004. "Macroeconomic Implications of Natural Disasters in the Caribbean." IMF
Working Paper WP/04/224. International Monetary Fund, Washington, DC.</mixed-citation>
         </ref>
         <ref id="d1421e723a1310">
            <mixed-citation id="d1421e727" publication-type="other">
Sacerdote, B. 2008. "When the Saints Come Marching in: Effects of Hurricanes Katrina and Rita on
Student Evacuees." Dartmouth College, Department of Economics, Hanover, New Hampshire.</mixed-citation>
         </ref>
         <ref id="d1421e737a1310">
            <mixed-citation id="d1421e741" publication-type="other">
Sala-i-Martin, X., G. Doppelhofer, and R. Miller. 2004. "Determinants of Long-Term Growth: A
Bayesian Averaging of Classical Estimates (BACE) Approach." American Economic Review 94:
813-35.</mixed-citation>
         </ref>
         <ref id="d1421e754a1310">
            <mixed-citation id="d1421e758" publication-type="other">
Skidmore, M. 2001. Risk, Natural Disasters, and Household Saving in a Life Cycle Model. Japan and
the World Economy 13: 15-34.</mixed-citation>
         </ref>
         <ref id="d1421e768a1310">
            <mixed-citation id="d1421e772" publication-type="other">
Skidmore, M., and H. Toya. 2002. Do Natural Disasters Promote Long-run Growth?, Economic
Inquiry 40: 664-87.</mixed-citation>
         </ref>
         <ref id="d1421e783a1310">
            <mixed-citation id="d1421e787" publication-type="other">
---. 2007. "Economic Development and the Impacts of Natural Disasters." Economic Letters 94:
20-25.</mixed-citation>
         </ref>
         <ref id="d1421e797a1310">
            <mixed-citation id="d1421e801" publication-type="other">
Stijns, J-P. 2006. "Natural Resource Abundance and Human Capital Accumulation. world
Development 34: 1060-83.</mixed-citation>
         </ref>
         <ref id="d1421e811a1310">
            <mixed-citation id="d1421e815" publication-type="other">
Toi, R., and F. Leek. 1999. "Economic Analysis of Natural Disasters." in T.E. Downing, AJ.
OlsthoornR.S.T. Toi eds., Climate, Change and Risk. London: Routledge.</mixed-citation>
         </ref>
         <ref id="d1421e825a1310">
            <mixed-citation id="d1421e829" publication-type="other">
Bank, World. 2006. World Development Indicators 2006. Washington, DC: World Bank.</mixed-citation>
         </ref>
         <ref id="d1421e836a1310">
            <mixed-citation id="d1421e840" publication-type="other">
Zellner, A. 1986. "On Assessing Prior Distributions and Bayesian Regression Analysis with g-prior
Distributions." In P.K. Goel, and A. Zellner eds., Bayesian Inference and Decision Techniques:
Essays in Honour of Bruno de Finetti. Amsterdam: North Holland.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
