<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">jpacihist</journal-id>
         <journal-id journal-id-type="jstor">j50000262</journal-id>
         <journal-title-group>
            <journal-title>The Journal of Pacific History</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Oxford University Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">00223344</issn>
         <issn pub-type="epub">14699605</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">25169284</article-id>
         <title-group>
            <article-title>The Gendering of Indirect Rule: Criminal Law and Colonial Fiji, 1875-1900</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Norman</given-names>
                  <surname>Etherington</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>6</month>
            <year>1996</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">31</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id>i25169278</issue-id>
         <fpage>42</fpage>
         <lpage>57</lpage>
         <self-uri xlink:href="https://www.jstor.org/stable/25169284"/>
         <abstract>
            <p>The administration of criminal justice in the period 1875-1900 had very different effects on men and women in three separate arenas: colonial towns, plantations and areas subject to chiefly control. In the towns of Levuka and Suva, men were the main concern of the courts; women rarely came before the courts and were almost never imprisoned. On the plantations, British magistrates dealt mainly with alleged breaches of the labour ordinances, with the result that women were prosecuted on an equal basis with men. The gender balance of criminal prosecutions was also closer to equal in the Fijian Provincial Courts, but the nature of the crimes charged against women was totally different from that in the other two spheres of colonial administration, the main concern being breaches of the traditional moral code. Understanding the segmented nature of the judicial system sheds further light on the workings of indirect rule in Fiji and suggests comparisons with colonial administrations in Africa.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d659e107a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d659e114" publication-type="other">
D. Scarr, Viceroy of the Pacific, The Majesty of Colour, A Life of Sir John Bates Thurston (Canberra 1980), 25.</mixed-citation>
            </p>
         </fn>
         <fn id="d659e121a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d659e128" publication-type="other">
J. Hay and M. Wright (eds),
African Women and the Law: Historical Perspectives (Boston 1982).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d659e137" publication-type="other">
'Deconstruction and Reconstruction of
Zulu Society', in S. Marks and R. Rathbone (eds), Industrialisation and Social Change in South Africa (London 1982),
167-94.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d659e149" publication-type="other">
Diana Jeater traces the shift in attitude in Southern Rhodesia in Marriage, Perversion and Power, The
Construction of Moral Discourse in Southern Rhodesia, 1894-1930 (Oxford 1993).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d659e159" publication-type="other">
Law, Custom and Social Order: The Colonial Experience in Malawi and Zambia (Cambridge 1985), 197.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d659e165" publication-type="other">
Jane L. Parpart,
in '"Where is your mother?": gender, urban marriage, and colonial discourse on the Zambian Copperbelt,
1924-1945', International Journal of African Historical Studies, 27 (1994), 241-71</mixed-citation>
            </p>
         </fn>
         <fn id="d659e178a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d659e185" publication-type="other">
Amirah Inglis, Not a White Woman Safe; Sexual Anxiety and Politics in Port Moresby, 1920-1934
(Canberra 1974)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d659e194" publication-type="other">
N. Etherington, 'Natal's black rape scare of the 1870's', Journal of Southern African Studies,
15 (1988), 123-40.</mixed-citation>
            </p>
         </fn>
         <fn id="d659e204a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d659e211" publication-type="other">
J. Innes and J. Styles have noted in Adrian Wilson
(ed.), Rethinking Social History (Manchester 1994), 201-65.</mixed-citation>
            </p>
         </fn>
         <fn id="d659e222a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d659e229" publication-type="other">
C.C.M.
Griffin, 'Crime and Punishment: Social Meanings', in C. Griffin and M.M. Monsell-Davis, Fijians in Town (Suva
1986), 102-31.</mixed-citation>
            </p>
         </fn>
         <fn id="d659e242a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d659e249" publication-type="other">
J.D. Legge, Britain in Fiji, 1858-1880 (Melbourne 1958), 202-30.</mixed-citation>
            </p>
         </fn>
         <fn id="d659e256a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d659e263" publication-type="other">
V. Naidu, The Violence of Indenture in Fiji (Suva 1980), 53-79.</mixed-citation>
            </p>
         </fn>
         <fn id="d659e270a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d659e277" publication-type="other">
B.V. Lal, 'Murmurs of dissent: non-resistance on Fiji plantations', Hawaiian Journal of History, 20 (1986),
188-214, esp. pp 200-9.</mixed-citation>
            </p>
         </fn>
         <fn id="d659e287a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d659e294" publication-type="other">
Charlotte
Macdonald, 'Crime and punishment in New Zealand, 1840-1913: a gendered history', New Zealand Journal of
History, 23 (1989), 11-12.</mixed-citation>
            </p>
         </fn>
         <fn id="d659e307a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d659e314" publication-type="other">
H. Anson (Col Sec), 1/5/85 on W.L. Allardyce SM to CS, Navua, 18 Apr. 1885</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d659e320" publication-type="other">
CSO
1885/1084, Suva, National Archives of Fiji (hereinafter NAF).</mixed-citation>
            </p>
         </fn>
         <fn id="d659e331a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d659e338" publication-type="other">
Naidu, The Violence of Indenture, 53.</mixed-citation>
            </p>
         </fn>
         <fn id="d659e345a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d659e352" publication-type="other">
W.L. Allardyce, SM Navua, Report for May, 1885, CSO 1885/1469.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d659e358" publication-type="other">
his monthly report for Feb.
1885, CSO 1885/635</mixed-citation>
            </p>
         </fn>
         <fn id="d659e368a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d659e375" publication-type="other">
Fiji Times, 28 Jan. and 7 Mar. 1885</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d659e381" publication-type="other">
C.R.
Swayne, Chief Magistrate, Lau, to Acting Colonial Secretary, 12 Feb. 1885</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d659e390" publication-type="other">
H. Anson,
Acting Agent General of Immigration to Manager, Mango Island Co. Ld., Mago, 22 Jan. 1885, CSO 85/536,
NAF</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d659e403" publication-type="other">
Acting Attorney General to Acting Colonial Secretary, 24 Mar. 1885, CSO 85/825, NAF.</mixed-citation>
            </p>
         </fn>
         <fn id="d659e410a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d659e417" publication-type="other">
K.L. Gillion, Fiji's Indian Migrants, A History to the End of Indenture in
1920 (Melbourne 1962), 1 -18.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d659e426" publication-type="other">
B. V. Lai, 'Labouring Men and Nothing More: Some Problems of Indian
Indenture in Fiji', in K. Saunders (ed.), Indentured Labour in the British Empire, 1834-1920 (London 1984), 126-7</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d659e435" publication-type="other">
C.F. Gordon Cumming, At Home in Fiji (2 vols, Edinburgh 1881), I, 101.</mixed-citation>
            </p>
         </fn>
         <fn id="d659e442a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d659e449" publication-type="other">
CSO 76/714, NAF.</mixed-citation>
            </p>
         </fn>
         <fn id="d659e456a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d659e463" publication-type="other">
CSO 75/544, NAF.</mixed-citation>
            </p>
         </fn>
         <fn id="d659e471a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d659e478" publication-type="other">
Scarr, Viceroy of the Pacific, 23-4.</mixed-citation>
            </p>
         </fn>
         <fn id="d659e485a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d659e492" publication-type="other">
R. Swanston to the Colonial Secretary, 2 May 1876, CSO 76/742, NAF.</mixed-citation>
            </p>
         </fn>
         <fn id="d659e499a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d659e506" publication-type="other">
Minute from Col Sec Anson to Administrator, CSO 1885/85, NAF.</mixed-citation>
            </p>
         </fn>
         <fn id="d659e513a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d659e520" publication-type="other">
Scarr, Viceroy of the Pacific, 24.</mixed-citation>
            </p>
         </fn>
         <fn id="d659e527a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d659e534" publication-type="other">
Magistrate's notebook of Provincial Court cases 1878-82, NAF</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d659e540" publication-type="other">
H.L Tripp, SM, Solevu, monthly report
for June, CSO 1885/1804, NAF.</mixed-citation>
            </p>
         </fn>
         <fn id="d659e550a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d659e557" publication-type="other">
Attorney General on Magistrate A.B. Joske's summary of Provincial Court cases for
June 1885, CSO 1885/2052, NAF.</mixed-citation>
            </p>
         </fn>
         <fn id="d659e568a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d659e575" publication-type="other">
Magistrate's notebook of Provincial Court cases, 1878-82, NAF.</mixed-citation>
            </p>
         </fn>
         <fn id="d659e582a1310">
            <label>50</label>
            <p>
               <mixed-citation id="d659e589" publication-type="other">
B.H. Thomson, SM, Nadroea to Col Sec, 22/6/85, CSO 1885/1679, NAF.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d659e595" publication-type="other">
CSO 1885/1611, NAF.</mixed-citation>
            </p>
         </fn>
         <fn id="d659e602a1310">
            <label>31</label>
            <p>
               <mixed-citation id="d659e609" publication-type="other">
Macdonald, 'Crime and punishment in New Zealand', passim.</mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
