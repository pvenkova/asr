<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt2ttr9v</book-id>
      <subj-group>
         <subject content-type="call-number">HV875.58.A45D82 2010</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Intercountry adoption</subject>
         <subj-group>
            <subject content-type="lcsh">America</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Interracial adoption</subject>
         <subj-group>
            <subject content-type="lcsh">America</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Sociology</subject>
      </subj-group>
      <book-title-group>
         <book-title>Babies without Borders</book-title>
         <subtitle>Adoption and the Symbolic Child in a Globalizing World</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>DUBINSKY</surname>
               <given-names>KAREN</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>31</day>
         <month>12</month>
         <year>2010</year>
      </pub-date>
      <isbn content-type="ppub">9781442610194</isbn>
      <isbn content-type="epub">9781442686120</isbn>
      <publisher>
         <publisher-name>University of Toronto Press</publisher-name>
         <publisher-loc>Toronto; London</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2010</copyright-year>
         <copyright-holder>University of Toronto Press Incorporated</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.3138/j.ctt2ttr9v"/>
      <abstract abstract-type="short">
         <p>Integrating the personal with the scholarly,<italic>Babies Without Borders</italic>exposes what happens when children bear the weight of adult political conflicts.</p>
      </abstract>
      <counts>
         <page-count count="204"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2ttr9v.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2ttr9v.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2ttr9v.3</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2ttr9v.4</book-part-id>
                  <title-group>
                     <label>CHAPTER ONE</label>
                     <title>Children and the Stories We Tell about Them</title>
                  </title-group>
                  <fpage>3</fpage>
                  <abstract>
                     <p>Some people keep a lock of their babyʹs hair, or their first lost tooth. I keep a Guatemalan newspaper from 30 April 2000, the day after a Japanese tourist was stoned to death by villagers who mistook him for a baby-snatcher. I read the headline, ʹLinchan a japones,ʹ over someoneʹs shoulder in a crowded bus in Guatemala City, where I had just arrived to meet the baby my partner and I adopted. It has become part of the archives of our sonʹs life and my research. A week later, we arrived at the Toronto airport and proceeded to the immigration</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2ttr9v.5</book-part-id>
                  <title-group>
                     <label>CHAPTER TWO</label>
                     <title>The National Baby:</title>
                     <subtitle>Creating Monumental Children in Cuba, from Operation Peter Pan to Elián González</subtitle>
                  </title-group>
                  <fpage>23</fpage>
                  <abstract>
                     <p>In Havana, my morning run along the cityʹs seawall, the Malecón, takes me past the oddly named U.S. Special Interests Section – essentially the embassy in a country which has not had a U.S. diplomatic presence since 1961. Having borne the dreams of the left and the nightmares of the right for five decades, Cuba fairly screams symbolism at every turn, and these days the area around the Special Interests Section is ground zero in a global battle for signification. I usually visit Havana in December, for me a respite not only from Canadaʹs cold but also from Christmas, an</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2ttr9v.6</book-part-id>
                  <title-group>
                     <label>CHAPTER THREE</label>
                     <title>The Hybrid Baby:</title>
                     <subtitle>Domestic Interracial Adoption since the 1950s</subtitle>
                  </title-group>
                  <fpage>57</fpage>
                  <abstract>
                     <p>An efficient combination of U.S. government money, church social welfare systems, and an army of committed volunteers met most of the problems faced by Cuban refugee children. Yet despite the ideological enthusiasm for their presence in the United States, the 8,331 (documented) Peter Pan children who were not immediately taken in by families or friends taxed even this impressive effort. Church officials took shortcuts and ignored established child-welfare practices. For example, they crossed denominational lines when placing children in foster care, and, more tragically, they separated siblings. Many Peter Pan children went to orphanages. In North America, the pendulum had</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2ttr9v.7</book-part-id>
                  <title-group>
                     <label>CHAPTER FOUR</label>
                     <title>The Missing Baby:</title>
                     <subtitle>Transnational Adoption and the Vanishing Children of Guatemala</subtitle>
                  </title-group>
                  <fpage>93</fpage>
                  <abstract>
                     <p>The controversies created when children moved across the boundaries of race within<italic>one</italic>country shaped the cultural and political ground on which transnational adoption takes place. Opening adoptionʹs domestic closet logically was necessary before adoption across national borders could begin to enjoy visibility and cultural support. After the 1960s, as Ellen Herman has observed, matching looked ʹmore like a lie than the truth.ʹ¹ The white suburban couples posed shyly with their black babies in 1960s newspaper features – always written as tributes to the inherent oddity of such a grouping – gave way, a few decades later, to the near</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2ttr9v.8</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Conclusion:</title>
                     <subtitle>Setting the Agenda for a Happy Childhood</subtitle>
                  </title-group>
                  <fpage>127</fpage>
                  <abstract>
                     <p>One of the things I did when I returned to Guatemala to research this book was to write the name of the lawyers my Canadian adoption agency used in the front of my notebook. Every time I conducted an interview, opened a report, or read newspaper clippings at the archives, I hoped I wouldnʹt encounter their names.</p>
                     <p>I didnʹt. Nor did I read my agencyʹs name in any of the voluminous reporting of adoption scandals in Guatemalaʹs recent history. My adoption application was processed less than twelve months before the Canadian government cut ties with Guatemala because of its non-ratification</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2ttr9v.9</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>133</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2ttr9v.10</book-part-id>
                  <title-group>
                     <title>Selected Bibliography</title>
                  </title-group>
                  <fpage>173</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2ttr9v.11</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>191</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
