<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">deveprac</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j101253</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Development in Practice</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Routledge, Taylor &amp; Francis</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">09614524</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">27751953</article-id>
         <article-id pub-id-type="pub-doi">10.1080/09614520802181210</article-id>
         <title-group>
            <article-title>Some Common Questions about Participatory Research: A Review of the Literature</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Nina</given-names>
                  <surname>Lilja</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Mauricio</given-names>
                  <surname>Bellon</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>8</month>
            <year>2008</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">18</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4/5</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i27751950</issue-id>
         <fpage>479</fpage>
         <lpage>488</lpage>
         <permissions>
            <copyright-statement>Copyright 2008 Oxfam GB</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/27751953"/>
         <abstract>
            <p>This article reviews, through reference to the published literature, some key questions about participatory research. When should participatory research be used? How should participatory research be applied? What about quality of science in participatory research? Are there any institutional issues associated with the use of participatory research? And what are the benefits and costs of participatory research? The article is not a comprehensive literature review on participatory research, it is not meant to set standards for participatory research, nor to define what constitutes 'good' participatory research, but rather it seeks to summarise the realities of implementing participatory research, as discussed and debated by several published authors, and to provide some useful background for this special issue.</p>
         </abstract>
         <kwd-group>
            <kwd>Methods</kwd>
            <kwd>Participatory Research</kwd>
            <kwd>Literature Review</kwd>
            <kwd>Benefits and Costs</kwd>
            <kwd>Quality of Science</kwd>
            <kwd>Institutionalisation</kwd>
         </kwd-group>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Note</title>
         <ref id="d1198e160a1310">
            <label>1</label>
            <mixed-citation id="d1198e167" publication-type="other">
Lilja and Bellon (2006).</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>References</title>
         <ref id="d1198e183a1310">
            <mixed-citation id="d1198e187" publication-type="other">
Agarwal, B. (2001) 'Participatory exclusions, community forestry and gender: an analysis for South Asia
and a conceptual framework', World Development 29 (10): 1623–48.</mixed-citation>
         </ref>
         <ref id="d1198e197a1310">
            <mixed-citation id="d1198e201" publication-type="other">
Ashby, J. A. (1996) 'What do we mean by participatory research in agriculture?', in New Frontiers in Par-
ticipatory Research and Gender Analysis, Proceedings of the International Seminar on Participatory
Research and Gender Analysis (PRGA), 9–14 September 1996, Cali, Colombia. Centro Internacional
de Agricultura Tropical (CIAT) Publication No. 294.</mixed-citation>
         </ref>
         <ref id="d1198e217a1310">
            <mixed-citation id="d1198e221" publication-type="other">
Bellon, M. R., J. Berthaud, M. Smale, J. A. Aguirre, S. Taba, F. Aragon, J. Diaz, and H. Castro
(2003) 'Participatory landrace selection for on-farm conservation: an example from the Central Valleys
of Oaxaca, Mexico', Genetic Resources and Crop Evolution 50: 401–16.</mixed-citation>
         </ref>
         <ref id="d1198e234a1310">
            <mixed-citation id="d1198e238" publication-type="other">
Berardi, G. (2002) 'Commentary on the challenge to change: participatory research and professional
realities', Society and Natural Resources 15 (9): 847–52.</mixed-citation>
         </ref>
         <ref id="d1198e249a1310">
            <mixed-citation id="d1198e253" publication-type="other">
Biggs, S. D. (1989) 'Resource-poor farmer participation in research: a synthesis of experiences from
nine national agricultural research systems', OFCOR Comparative Study Paper No. 3, The Hague, The
Netherlands: International Service for National Agricultural Research (ISNAR).</mixed-citation>
         </ref>
         <ref id="d1198e266a1310">
            <mixed-citation id="d1198e270" publication-type="other">
Biggs, S, and J. Farrington (1991) Agricultural Research and the Rural Poor: A Review of Social Science
Analysis, Ottawa: IDRC.</mixed-citation>
         </ref>
         <ref id="d1198e280a1310">
            <mixed-citation id="d1198e284" publication-type="other">
Byerlee, D. and P. Heisey (1996) 'Past and potential impacts of maize research in sub-Saharan Africa: a
critical assessment', Food Policy 21: 255–77'.</mixed-citation>
         </ref>
         <ref id="d1198e294a1310">
            <mixed-citation id="d1198e298" publication-type="other">
Campbell, J. R. (2001) 'Participatory Rural Appraisal as qualitative research: distinguishing methodology
issues from participatory claims', Human Organization 60 (4): 380–89.</mixed-citation>
         </ref>
         <ref id="d1198e308a1310">
            <mixed-citation id="d1198e312" publication-type="other">
Ceccarelli, S., S. Grando, E. Bailey, A. Amri, M. El-Felah, F. Nassif, S. Rezqui, and A. Yahyaoui
(2001) 'Farmer participation in barley breeding in Syria, Morocco and Tunisia', Euphytica 122: 521–36.</mixed-citation>
         </ref>
         <ref id="d1198e322a1310">
            <mixed-citation id="d1198e326" publication-type="other">
Ceccarelli, S., S. Grando, R. Tutwiler, J. Baha, A.M. Martini, H. Salahieh, A. Goodchild, and
M. Michael (2003) 'A methodological study on participatory barley breeding. II. Response to selection',
Euphytica 133: 185–200.</mixed-citation>
         </ref>
         <ref id="d1198e340a1310">
            <mixed-citation id="d1198e344" publication-type="other">
Chambers, R. (1997) Whose Reality Counts? Putting The First Last, London: IT Publications.</mixed-citation>
         </ref>
         <ref id="d1198e351a1310">
            <mixed-citation id="d1198e355" publication-type="other">
Chambers, R., A. Pacey, and L. A. Thrupp (eds.) (1989) Farmers First: Farmer Innovation and
Agricultural Research, London: IT Publications.</mixed-citation>
         </ref>
         <ref id="d1198e365a1310">
            <mixed-citation id="d1198e369" publication-type="other">
Courtois, B., B. Bartholome, D. Chaudhary, G. McLaren, C. H. Misra, N. P. Mandal, S. Pandey,
T. Paris, C. Piggin, K. Prasad, A. T. Roy, R. K. Sahu, V. N. Sahu, S. Sarkarung, S. K. Sharma,
A. Singh, H. N. Singh, O. N. Singh, N. K. Singh, R. K. Singh, S. Singh, P. K. Sinha,
B. V. S. Sisodia, and R. Takhur (2001) 'Comparing farmers' and breeders' rankings in varietal selection
for low-input environments: a case study of rainfed rice in eastern India', Euphytica 122: 537–50.</mixed-citation>
         </ref>
         <ref id="d1198e388a1310">
            <mixed-citation id="d1198e392" publication-type="other">
Dalton, T. and R. Guei (2003) 'Productivity gains from rice genetic enhancements in West Africa:
countries and ecologies', World Development 31 (2): 359–74.</mixed-citation>
         </ref>
         <ref id="d1198e402a1310">
            <mixed-citation id="d1198e406" publication-type="other">
Ekboir, J. (2003) 'Why impact analysis should not be used for research evaluation and what the alterna-
tives are', Agricultural Systems 78 (2): 166–84.</mixed-citation>
         </ref>
         <ref id="d1198e416a1310">
            <mixed-citation id="d1198e420" publication-type="other">
Evenson, R. E. and D. Gollin (eds.) (2002) Crop Variety Improvement and Its Effect on Productivity: The
Impact of International Research, Wallingford: CAB International.</mixed-citation>
         </ref>
         <ref id="d1198e431a1310">
            <mixed-citation id="d1198e435" publication-type="other">
Farrington, J. (1988) 'Farmer participatory research: editorial introduction', Experimental Agriculture
24 (3): 269–79.</mixed-citation>
         </ref>
         <ref id="d1198e445a1310">
            <mixed-citation id="d1198e449" publication-type="other">
Franzel, S., C. Wambugu, and P. Tuwei (2003) 'The adoption and dissemination of fodder shrubs in
Central Kenya', ODI/AGREN Network Paper No. 131.</mixed-citation>
         </ref>
         <ref id="d1198e459a1310">
            <mixed-citation id="d1198e463" publication-type="other">
Gladwin, C. H., J. S. Peterson, and A. C. Mwale (2002) 'The quality of science in participatory research:
a case study from eastern Zambia', World Development 30 (4): 523–43.</mixed-citation>
         </ref>
         <ref id="d1198e473a1310">
            <mixed-citation id="d1198e477" publication-type="other">
Groverman, V. and J. D. Gurung (2001) Gender and Organizational Change: A Training Manual,
Kathmandu: International Center for Integrated Mountain Development.</mixed-citation>
         </ref>
         <ref id="d1198e487a1310">
            <mixed-citation id="d1198e491" publication-type="other">
Haddinott, J. (2002) 'Participation and poverty reduction: an analytical framework and overview of the
issues', Journal of African Economies 11 (1): 146–68.</mixed-citation>
         </ref>
         <ref id="d1198e501a1310">
            <mixed-citation id="d1198e505" publication-type="other">
Hall, A. and S. Nahdy (1999) 'New methods and old institutions: the "systems context" of farmer parti-
cipatory research in national agricultural research systems. The case of Uganda', ODI/AgREN Network
Paper No. 93.</mixed-citation>
         </ref>
         <ref id="d1198e519a1310">
            <mixed-citation id="d1198e523" publication-type="other">
Hall, A., V. R. Sulaiman, N. Clark, and B. Yoganand (2003) 'From measuring impact to learning
institutional lessons: an innovation systems perspective on improving the management of international
agricultural research', Agricultural Systems 78: 213–41.</mixed-citation>
         </ref>
         <ref id="d1198e536a1310">
            <mixed-citation id="d1198e540" publication-type="other">
Hay ward, C., L. Simpson, and L. Wood (2004) 'Still left out in the cold: problematising participatory
research and development', Sociologia Ruralis 44 (1): 95–108.</mixed-citation>
         </ref>
         <ref id="d1198e550a1310">
            <mixed-citation id="d1198e554" publication-type="other">
Heong, K. L. and M. M. Escalada (1998) 'Changing rice farmers' pest management practices through
participation in a small-scale experiment', International Journal of Pest Management 44 (4): 191–7.</mixed-citation>
         </ref>
         <ref id="d1198e564a1310">
            <mixed-citation id="d1198e568" publication-type="other">
Johnson, N. L., N. Lilja, and J. A. Ashby (2003) 'Measuring the impact of user participation in
agricultural and natural resource management research', Agricultural Systems 78 (2): 287–306.</mixed-citation>
         </ref>
         <ref id="d1198e578a1310">
            <mixed-citation id="d1198e582" publication-type="other">
Johnson, N., N. Lilja, J. A. Ashby, and J. A. Garcia (2004) 'The practice of participatory research and
gender analysis in natural resource management', Natural Resources Forum 28: 189–200.</mixed-citation>
         </ref>
         <ref id="d1198e592a1310">
            <mixed-citation id="d1198e596" publication-type="other">
Joshi, K. D. and J. R. Witcombe (2002) 'Participatory varietal selection in rice in Nepal in favourable
agricultural environments - a comparison of methods assessed by variable adoption', Euphytica 127:
445–58.</mixed-citation>
         </ref>
         <ref id="d1198e610a1310">
            <mixed-citation id="d1198e614" publication-type="other">
Joshi, K. D., B. R. Sthapit, and J. R. Witcombe (2001) 'How narrowly adapted are the products of
decentralized breeding? The spread of rice varieties from a participatory breeding programme in
Nepal', Euphytica 122 (3): 589–97.</mixed-citation>
         </ref>
         <ref id="d1198e627a1310">
            <mixed-citation id="d1198e631" publication-type="other">
Lilja, Nina and Mauricio Bellon (2006) 'Analysis of Participatory Research Projects in the International
Maize and Wheat Improvement Center', Mexico, DF: CIMMYT.</mixed-citation>
         </ref>
         <ref id="d1198e641a1310">
            <mixed-citation id="d1198e645" publication-type="other">
Maredia, M., D. Byerlee, and P. Pee (1998) 'Impact of food crop research in Africa', SPAAR Occasional
Papers series, No. 1.</mixed-citation>
         </ref>
         <ref id="d1198e655a1310">
            <mixed-citation id="d1198e659" publication-type="other">
Martin, A. and J. Sherington (1997) 'Participatory research methods - implementation, effectiveness
and institutional context', Agricultural Systems 55 (2): 195–216.</mixed-citation>
         </ref>
         <ref id="d1198e669a1310">
            <mixed-citation id="d1198e673" publication-type="other">
Morris, M. L. and M. R. Bellon (2004) 'Participatory plant breeding research: opportunities and chal-
lenges for the international crop improvement system', Euphytica 136: 21–35.</mixed-citation>
         </ref>
         <ref id="d1198e683a1310">
            <mixed-citation id="d1198e687" publication-type="other">
Nowak, P. (1992) 'Why farmers adopt production technology', Journal of Soil and Water Conservation
47 (1): 14–16.</mixed-citation>
         </ref>
         <ref id="d1198e698a1310">
            <mixed-citation id="d1198e702" publication-type="other">
Ohemke, J. F. and E. F. Crawford (1996) 'The impact of agricultural technology in sub-Saharan
agriculture', Journal of African Economics 5 (2): 271–92.</mixed-citation>
         </ref>
         <ref id="d1198e712a1310">
            <mixed-citation id="d1198e716" publication-type="other">
Okali, C., J. Sumberg, and J. Farrington (1994) Farmer Participatory Research: Rhetoric and Reality,
London: IT Publications.</mixed-citation>
         </ref>
         <ref id="d1198e726a1310">
            <mixed-citation id="d1198e730" publication-type="other">
Phiri, D., S. Franzel, P. Mafongoya, I. Jere, R. Katanga, and S. Phiri (2004) Who is using the new
technology? The association of wealth status and gender with the planting of improved tree fallows in
Eastern Province, Zambia', Agricultural Systems 79: 131–44.</mixed-citation>
         </ref>
         <ref id="d1198e743a1310">
            <mixed-citation id="d1198e747" publication-type="other">
Pingali, P. (2007) 'Will the Gene Revolution Reach the Poor? - Lessons from the Green Revolution',
Mansholt Lecture, Waganingen University, 26 January.</mixed-citation>
         </ref>
         <ref id="d1198e757a1310">
            <mixed-citation id="d1198e761" publication-type="other">
Pretty, J. N. (1994) 'Alternative systems of inquiry for a sustainable agriculture', IDS Bulletin 25: 37–48.</mixed-citation>
         </ref>
         <ref id="d1198e768a1310">
            <mixed-citation id="d1198e772" publication-type="other">
Roling, N. and A. Wagemakers (1998) Facilitating Sustainable Agriculture: Participatory Learning
and Adaptive Management in Times of Environmental Uncertainty, New York, NY: Cambridge
University Press.</mixed-citation>
         </ref>
         <ref id="d1198e786a1310">
            <mixed-citation id="d1198e790" publication-type="other">
Smale, M., M. R. Bellon, I. M. Rosas, J. Mendoza, A. M. Solano, R. Martinez, A. Ramirez, and
J. Berthaud (2003) 'The economic costs and benefits of a participatory project to conserve maize land-
races on farms in Oaxaco, Mexico', Agricultural Economics 29: 265–75.</mixed-citation>
         </ref>
         <ref id="d1198e803a1310">
            <mixed-citation id="d1198e807" publication-type="other">
Snapp, S. S., H. A. Freeman, F. Simtowe, and D. D. Rohrbach (2002) Sustainable soil management
options for Malawi: can smallholder farmers grow more legumes?', Agriculture, Ecosystems and Environ-
ment 91 (1/3): 159–74.</mixed-citation>
         </ref>
         <ref id="d1198e820a1310">
            <mixed-citation id="d1198e824" publication-type="other">
Sperling, L., J. A. Ashby, M. E. Smith, E. Weltzein, and S. McGuire (2001) A framework for analyz-
ing participatory plant breeding approaches and results', Euphytica 122: 439–50.</mixed-citation>
         </ref>
         <ref id="d1198e834a1310">
            <mixed-citation id="d1198e838" publication-type="other">
van der Fliert, E. and A. R. Braun (2002) 'Conceptualizing integrative, farmer participatory
research for sustainable agriculture: from opportunities to impact', Agriculture &amp;amp; Human Values 19
(1): 25–38.</mixed-citation>
         </ref>
         <ref id="d1198e851a1310">
            <mixed-citation id="d1198e855" publication-type="other">
Vernooy, R. and C. McDougall (2003) 'Principles for good practice in participatory research: reflecting
on lessons from the field', in B. Pound, S. Snapp, C. McDougall, and A. Braun (eds.) Managing Natural
Resources for Sustainable Livelihoods: Uniting Science and Participation, London: Earthscan
Publications.</mixed-citation>
         </ref>
         <ref id="d1198e871a1310">
            <mixed-citation id="d1198e875" publication-type="other">
Weltzein, E., M. E. Smith, L. Meitzner, and L. Sperling (2000) 'Technical and institutional issues in
participatory plant breeding from the perspective of formal plant breeding: a global analysis of issues,
results and current experience', PRGA Working Document no. 3, Cali, Colombia: CGIAR Systemwide
Program of Participatory Research and Gender Analysis for Technology Development and Institutional
Innovation.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
