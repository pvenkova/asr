<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1w6tfgw</book-id>
      <subj-group>
         <subject content-type="call-number">HC800 .S955 2017</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Economic assistance</subject>
         <subj-group>
            <subject content-type="lcsh">Political aspects</subject>
            <subj-group>
               <subject content-type="lcsh">Africa, Sub-Saharan</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">International agencies</subject>
         <subj-group>
            <subject content-type="lcsh">Political aspects</subject>
            <subj-group>
               <subject content-type="lcsh">Africa, Sub-Saharan</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Non-governmental organizations</subject>
         <subj-group>
            <subject content-type="lcsh">Political aspects</subject>
            <subj-group>
               <subject content-type="lcsh">Africa, Sub-Saharan</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Economic development</subject>
         <subj-group>
            <subject content-type="lcsh">Political aspects</subject>
            <subj-group>
               <subject content-type="lcsh">Africa, Sub-Saharan</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Africa, Sub-Saharan</subject>
         <subj-group>
            <subject content-type="lcsh">Foreign economic relations</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
         <subject>Sociology</subject>
      </subj-group>
      <book-title-group>
         <book-title>The Development Dance</book-title>
         <subtitle>How Donors and Recipients Negotiate the Delivery of Foreign Aid</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Swedlund</surname>
               <given-names>Haley J.</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>15</day>
         <month>09</month>
         <year>2017</year>
      </pub-date>
      <isbn content-type="epub">9781501709784</isbn>
      <isbn content-type="epub">150170978X</isbn>
      <publisher>
         <publisher-name>Cornell University Press</publisher-name>
         <publisher-loc>ITHACA; LONDON</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2017</copyright-year>
         <copyright-holder>Cornell University</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7591/j.ctt1w6tfgw"/>
      <abstract abstract-type="short">
         <p>In a book full of directly applicable lessons for policymakers, Haley J. Swedlund explores why foreign aid is delivered in different ways at different times, and why various approaches prove to be politically unsustainable. She finds that no aid-delivery mechanism has yet resolved commitment problems in the donor-recipient relationship; bargaining compromises break down and have to be renegotiated; frustration grows; new ways of delivering aid gain traction over existing practices; and the dance resumes.</p>
         <p>Swedlund draws on hundreds of interviews with key decision makers representing both donor agencies and recipient governments, policy and archival documents in Ghana, Rwanda, Tanzania, and Uganda, and an original survey of top-level donor officials working across twenty countries in Sub-Saharan Africa. This wealth of data informs Swedlund's analysis of fads and fashions in the delivery of foreign aid and the interaction between effectiveness and aid delivery. The central message of<italic>The Development Dance</italic>is that if we want to know whether an aid delivery mechanism is likely to be sustained over the long term, we need to look at whether it induces credible commitments from both donor agencies and recipient governments over the long term.</p>
      </abstract>
      <counts>
         <page-count count="202"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1w6tfgw.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1w6tfgw.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1w6tfgw.3</book-part-id>
                  <title-group>
                     <title>List of Figures and Tables</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1w6tfgw.4</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1w6tfgw.5</book-part-id>
                  <title-group>
                     <title>List of Abbreviations</title>
                  </title-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1w6tfgw.6</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>THE DEVELOPMENT DANCE</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>As country director for the United States Agency for International Development (USAID) in Uganda, it is your job to oversee the disbursement of roughly $300 million in foreign aid every year.¹ This money is given on behalf of the American people with the expectation that it will help raise thousands of people out of poverty, increase life expectancies and levels of education, and limit the spread of HIV/AIDS in Uganda. How do you make sure foreign aid dollars are well spent? How do you make sure that the aid you have been tasked with allocating is not wasted?</p>
                     <p>To deliver</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1w6tfgw.7</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>IT TAKES TWO TO TANGO:</title>
                     <subtitle>Aid Policy Bargaining</subtitle>
                  </title-group>
                  <fpage>18</fpage>
                  <abstract>
                     <p>Let’s say you work at the UK’s Department for International Development (DFID) and you’ve been told that £3 million has been approved to help combat the high rate of infant mortality in Zambia over the next three years. You know that to implement an effective program you will have to work with lots of other stakeholders within a health care system that is lacking in capacity and personnel. But having been involved in Zambia for several years, you have also seen the human cost of the country’s high rate of infant deaths. You want to get the money there quickly,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1w6tfgw.8</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>STUDYING THE DANCE:</title>
                     <subtitle>Research Design, Methodology, and Historical Context</subtitle>
                  </title-group>
                  <fpage>36</fpage>
                  <abstract>
                     <p>How did I go about studying how donor agencies and recipient governments negotiate the delivery of aid? In this chapter, I outline the methodological tools and strategies I used to gain insight into the development dance. How I carried out the analysis and why I chose the countries I did are just as important as the empirical findings. Without a sound methodology, we cannot be assured that the results are reliable and valid. This chapter also includes historical background on the countries where I carried out fieldwork, background that is vital to fully understand my empirical findings.</p>
                     <p>My empirical approach</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1w6tfgw.9</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>MAY I HAVE THIS DANCE?</title>
                     <subtitle>Donor–Government Relations in Aid-Dependent Countries</subtitle>
                  </title-group>
                  <fpage>59</fpage>
                  <abstract>
                     <p>At the recipient country level, what do donor–government relations actually look like? How do donor and government officials interact with one another on a daily basis? Although the literature on foreign aid is vast, we actually have little empirical analysis on what donor agencies and recipient governments bargain and negotiate over on a daily basis, particularly across multiple countries and years. In this chapter, my goal is to provide the reader with a window into donor–government relations in each of the four country case studies. To those working in the field of international development, these observations should feel</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1w6tfgw.10</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>A HALFHEARTED SHUFFLE:</title>
                     <subtitle>Commitment Problems in Aid Policy Bargaining</subtitle>
                  </title-group>
                  <fpage>77</fpage>
                  <abstract>
                     <p>At the recipient-country level, donor agencies and recipient governments are engaged in a complicated dance in which the end result is an aid policy compromise. For both, a great deal rides on these agreements. But how likely is it that each side will keep its part of the bargain? Let’s imagine, for example, that a donor agency has promised $10 million to support the building of a toll road from a recipient country’s national airport to the capital city. The toll road is badly needed to ease congestion and decrease the costs of exports and imports. In return for the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1w6tfgw.11</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>TRACKING A CRAZE:</title>
                     <subtitle>The Rise (and Fall) of Budget Support</subtitle>
                  </title-group>
                  <fpage>97</fpage>
                  <abstract>
                     <p>In the late 1990s and early 2000s, many donor agencies began to embrace a new way of delivering foreign aid, called budget support. Within just over a decade, there was close to a tenfold increase in the amount of budget support disbursed worldwide. In 1990, donors disbursed $11.5 billion in general budget support. By 2002, this figure had risen to $98.6 billion.¹ Although related to aid delivery mechanisms used in the past, budget support is unique in that it requires donor agencies to disburse funds directly into the treasury of the recipient government without placing strong conditions on how the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1w6tfgw.12</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>THE FUTURE OF THE DEVELOPMENT DANCE AND WHY WE SHOULD CARE</title>
                  </title-group>
                  <fpage>122</fpage>
                  <abstract>
                     <p>In this book, I told the story of the development dance, a seemingly never-ending process of bargaining and negotiation between donor agencies and recipient-country governments. It is a dance that both partners willingly join. But it is a dance that leaves neither side particularly satisfied. Time and time again, donor and recipient government officials enter into new arrangements, adopting new aid policies and practices with the hope that this time their hard-fought compromises will be politically sustainable. However, again and again, both parties are left disappointed, as compromises break down and have to be renegotiated. The breakdown of aid policy</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1w6tfgw.13</book-part-id>
                  <title-group>
                     <title>Appendixes</title>
                  </title-group>
                  <fpage>133</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1w6tfgw.14</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>151</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1w6tfgw.15</book-part-id>
                  <title-group>
                     <title>Works Cited</title>
                  </title-group>
                  <fpage>163</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1w6tfgw.16</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>181</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
