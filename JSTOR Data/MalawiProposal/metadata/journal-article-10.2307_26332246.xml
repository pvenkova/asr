<?xml version="1.0" encoding="UTF-8"?>
<article article-type="research-article" dtd-version="1.0" xml:lang="eng">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="publisher-id">demorese</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50020442</journal-id>
         <journal-title-group>
            <journal-title>Demographic Research</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Max Planck Institute for Demographic Research</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">14359871</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">23637064</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">26332246</article-id>
         <article-categories>
            <subj-group>
               <subject>Research Article</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Household structure vs. composition</article-title>
            <subtitle>Understanding gendered effects on educational progress in rural South Africa</subtitle>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Madhavan</surname>
                  <given-names>Sangeetha</given-names>
               </string-name>
               <xref ref-type="aff" rid="af1">¹</xref>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Myroniuk</surname>
                  <given-names>Tyler W.</given-names>
               </string-name>
               <xref ref-type="aff" rid="af2">²</xref>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Kuhn</surname>
                  <given-names>Randall</given-names>
               </string-name>
               <xref ref-type="aff" rid="af3">³</xref>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Collinson</surname>
                  <given-names>Mark A.</given-names>
               </string-name>
               <xref ref-type="aff" rid="af4">⁴</xref>
            </contrib>
            <aff id="af1">
               <label>¹</label>University of Maryland, College Park, USA.</aff>
            <aff id="af2">
               <label>²</label>George Mason University, Fairfax, USA.</aff>
            <aff id="af3">
               <label>³</label>University of California, Los Angeles, USA.</aff>
            <aff id="af4">
               <label>⁴</label>MRC/Wits University Rural Public Health and Health Transitions Research Unit (Agincourt), School of Public Health, Faculty of Health Sciences, University of the Witwatersrand, South Africa; South African Population Research Infrastructure Network (SAPRIN), Department of Science and Technology/Medical Research Council, South Africa; INDEPTH Network, Ghana.</aff>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>7</month>
            <year>2017</year>
            <string-date>JULY - DECEMBER 2017</string-date>
         </pub-date>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>12</month>
            <year>2017</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink">37</volume>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">e26332187</issue-id>
         <fpage>1891</fpage>
         <lpage>1916</lpage>
         <permissions>
            <copyright-statement>© 2017 Madhavan, Myroniuk, Kuhn &amp; Collinson</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/26332246"/>
         <abstract xml:lang="eng">
            <sec>
               <label>BACKGROUND</label>
               <p>Demographers have long been interested in the relationship between living arrangements and gendered outcomes for children in sub-Saharan Africa. Most research conflates household structure with composition and has revealed little about the pathways that link these components to gendered outcomes.</p>
            </sec>
            <sec>
               <label>OBJECTIVES</label>
               <p>We offer a conceptual approach that differentiates structure from composition with a focus on gendered processes that operate in the household in rural South Africa.</p>
            </sec>
            <sec>
               <label>METHODS</label>
               <p>We use data from the 2002 round of the Agincourt Health and Socio-Demographic Surveillance System. Our analytical sample includes 22,997 children aged 6–18 who were neither parents themselves nor lived with a partner or partner’s family. We employ ordinary least squares regression models to examine the effects of structure and composition on educational progress of girls and boys.</p>
            </sec>
            <sec>
               <label>RESULTS</label>
               <p>Non-nuclear structures are associated with similar negative effects for both boys and girls compared to children growing up in nuclear households. However, the presence of other kin in the absence of one or both parents results in gendered effects favouring boys.</p>
            </sec>
            <sec>
               <label>CONCLUSION</label>
               <p>The absence of any gendered effects when using a household structure typology suggests that secular changes to attitudes about gender equity trump any specific gendered processes stemming from particular configurations. On the other hand, gendered effects that appear when one or both parents are absent show that traditional gender norms and/or resource constraints continue to favour boys.</p>
            </sec>
            <sec>
               <label>CONTRIBUTION</label>
               <p>We have shown the value of unpacking household structure to better understand how gender norms and gendered resource allocations are linked to an important outcome for children in sub-Saharan Africa.</p>
            </sec>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="unparsed-citations">
         <title>References</title>
         <ref id="ref1">
            <mixed-citation publication-type="other">Ankrah, E.M. (1993). The impact of HIV/AIDS on the family and other significant relationships: The African clan revisited. AIDS Care 5(1): 5‒22. doi:10.1080/09540129308258580.</mixed-citation>
         </ref>
         <ref id="ref2">
            <mixed-citation publication-type="other">Babalola, S., Tambashe, B.O., and Vondrasek, C. (2005). Parental factors and sexual risk-taking among young people in Cote d’Ivoire. African Journal of Reproductive Health 9(1): 49‒65. doi:10.2307/3583160.</mixed-citation>
         </ref>
         <ref id="ref3">
            <mixed-citation publication-type="other">Bicego, G., Rutstein, S., and Johnson, K. (2003). Dimensions of the emerging orphan crisis in sub-Saharan Africa. Social Science and Medicine 56(6): 1235‒1247. doi:10.1016/S0277-9536(02)00125-9.</mixed-citation>
         </ref>
         <ref id="ref4">
            <mixed-citation publication-type="other">Bledsoe, C.H. and Isiugo-Abanihe, U.C. (1985). Strategies of child fosterage among Mende grannies in Sierra Leone. Presented at the Annual Meeting of the Population Association of America, Boston, March 28‒31.</mixed-citation>
         </ref>
         <ref id="ref5">
            <mixed-citation publication-type="other">Bongaarts, J. (2001). Household size and composition in the developing world in the 1990s. Population Studies 55(3): 263‒279. doi:10.1080/00324720127697.</mixed-citation>
         </ref>
         <ref id="ref6">
            <mixed-citation publication-type="other">Brady, D. and Burroway, R. (2012). Targeting, universalism, and single-mother poverty: A multilevel analysis across 18 affluent democracies. Demography 49(2): 719‒746. doi:10.1007/s13524-012-0094-z.</mixed-citation>
         </ref>
         <ref id="ref7">
            <mixed-citation publication-type="other">Brown, S.L. (2010). Marriage and child well-being: Research and policy perspectives. Journal of Marriage and Family 72(5): 1059‒1077. doi:10.1111/j.1741-3737.2010.00750.x.</mixed-citation>
         </ref>
         <ref id="ref8">
            <mixed-citation publication-type="other">Buchmann, C. (2000). Family structure, parental perceptions, and child labor in Kenya: What factors determine who is enrolled in school? Social Forces 78(4): 1349‒ 1378. doi:10.1093/sf/78.4.1349.</mixed-citation>
         </ref>
         <ref id="ref9">
            <mixed-citation publication-type="other">Case, A. and Deaton, A. (1998). Large cash transfers to the elderly in South Africa. The Economic Journal 108: 1338‒1361. doi:10.1111/1468-0297.00345.</mixed-citation>
         </ref>
         <ref id="ref10">
            <mixed-citation publication-type="other">Case, A., Paxson, C., and Ableidinger, J. (2004). Orphans in Africa: Parental death, poverty, and school enrollment. Demography 41(3): 483‒508. doi:10.1353/dem. 2004.0019.</mixed-citation>
         </ref>
         <ref id="ref11">
            <mixed-citation publication-type="other">Casper, L. and Bianchi, S. (2002). Change and continuity in the American family. Thousand Oaks: Sage Publications.</mixed-citation>
         </ref>
         <ref id="ref12">
            <mixed-citation publication-type="other">Clark, S., Kabiru, C., and Mathur, R. (2010). Relationship transitions among youth in urban Kenya. Journal of Marriage and Family 72(1): 73‒88. doi:10.1111/j.1741-3737.2009.00684.x.</mixed-citation>
         </ref>
         <ref id="ref13">
            <mixed-citation publication-type="other">Clark, S., Madhavan, S., Beguy, D., Kabiru, C., and Cotton, C. (2017). Who helps single mothers in Nairobi? The role of kin support. Journal of Marriage and Family 79(4): 1186–1204. doi:10.1111/jomf.12404.</mixed-citation>
         </ref>
         <ref id="ref14">
            <mixed-citation publication-type="other">Conley, D. (2005). The pecking order: A bold new look at how family and society determine who we become. New York: Vintage.</mixed-citation>
         </ref>
         <ref id="ref15">
            <mixed-citation publication-type="other">Cunningham, S.A., Elo, I.T., Herbst, K., and Hosegood, V. (2010). Prenatal development in rural South Africa: Relationship between birth weight and access to fathers and grandparents. Population Studies 64(3): 229‒246. doi:10.1080/00324728.2010.510201.</mixed-citation>
         </ref>
         <ref id="ref16">
            <mixed-citation publication-type="other">Das Gupta, M. (1987). Selective discrimination against female children in rural Punjab, India. Population and Development Review 13(1): 77‒100. doi:10.2307/1972 121.</mixed-citation>
         </ref>
         <ref id="ref17">
            <mixed-citation publication-type="other">Desai, S. (1992). Children at risk: The role of family structure in Latin America and West Africa. Population and Development Review 18(4): 689‒717. doi:10.2307/1973760.</mixed-citation>
         </ref>
         <ref id="ref18">
            <mixed-citation publication-type="other">Doan, R.M. and Bisharat, L. (1990). Female autonomy and child nutritional status: the extended-family residential unit in Amman, Jordan. Social Science and Medicine 31(7): 783‒789. doi:10.1016/0277-9536(90)90173-P.</mixed-citation>
         </ref>
         <ref id="ref19">
            <mixed-citation publication-type="other">Dodoo, F.N.A. and Frost, A.E. (2008). Gender in African population research: The fertility/reproductive health example. Annual Review of Sociology 34: 431‒452. doi:10.1146/annurev.soc.34.040507.134552.</mixed-citation>
         </ref>
         <ref id="ref20">
            <mixed-citation publication-type="other">Duflo, E. (2000). Child health and household resources in South Africa: Evidence from the Old Age Pension program. American Economic Review 90(2): 393‒398. doi:10.1257/aer.90.2.393.</mixed-citation>
         </ref>
         <ref id="ref21">
            <mixed-citation publication-type="other">Edin, K. and Lein, L. (1997). Making ends meet: How single mothers survive welfare and low-wage work. New York: Russell Sage Foundation.</mixed-citation>
         </ref>
         <ref id="ref22">
            <mixed-citation publication-type="other">Evans, D.K. and Miguel, E. (2007). Orphans and schooling in Africa: A longitudinal analysis. Demography 44(1): 35‒57. doi:10.1353/dem.2007.0002.</mixed-citation>
         </ref>
         <ref id="ref23">
            <mixed-citation publication-type="other">Folbre, N. (1986). Cleaning house: New perspectives on households and economic development. Journal of Development Economics 22(1): 5–40. doi:10.1016/0304-3878(86)90051-9.</mixed-citation>
         </ref>
         <ref id="ref24">
            <mixed-citation publication-type="other">Gage, A., Sommerfelt, A.E., and Piani, A.L. (1996). Household structure socioeconomic level and child health in sub-Saharan Africa. (Demographic and health surveys analytical report 1) Calverton: Macro International.</mixed-citation>
         </ref>
         <ref id="ref25">
            <mixed-citation publication-type="other">Garenne, M., Tollman, S., and Kahn, K. (2000). Premarital fertility in rural South Africa: A challenge to existing population policy. Studies in Family Planning 31(1): 47‒54. doi:10.1111/j.1728-4465.2000.00047.x.</mixed-citation>
         </ref>
         <ref id="ref26">
            <mixed-citation publication-type="other">Glick, P. and Sahn, D.E. (2000). Schooling of girls and boys in a West African country: The effects of parental education, income, and household structure. Economics of Education Review 19(1): 63‒87. doi:10.1016/S0272-7757(99)00029-1.</mixed-citation>
         </ref>
         <ref id="ref27">
            <mixed-citation publication-type="other">Goldberg, R.E. (2013a). Family instability and early initiation of sexual activity in Western Kenya. Demography 50(2): 725‒750. doi:10.1007/s13524-012-0150-8.</mixed-citation>
         </ref>
         <ref id="ref28">
            <mixed-citation publication-type="other">Goldberg, R.E. (2013b). Family instability and pathways to adulthood in Cape Town, South Africa. Population and Development Review 39(2): 231‒256. doi:10.1111/j.1728-4457.2013.00590.x.</mixed-citation>
         </ref>
         <ref id="ref29">
            <mixed-citation publication-type="other">Goldberg, R.E. and Short, S.E. (2012). ‘The luggage that isn’t theirs is too heavy…’: Understandings of orphan disadvantage in Lesotho. Population Research and Policy Review 31(1): 67‒83. doi:10.1007/s11113-011-9223-4.</mixed-citation>
         </ref>
         <ref id="ref30">
            <mixed-citation publication-type="other">Haider, S.J. and McGarry, K. (2006). Recent trends in income sharing among the poor. In: Blank, R., Danziger, S., and Schoeni, R.F. (eds.). Working and poor: How economic and policy changes are affecting low wage workers. New York: Russell Sage: 205‒232.</mixed-citation>
         </ref>
         <ref id="ref31">
            <mixed-citation publication-type="other">Hamoudi, A. and Thomas, D. (2005). Pension income and the well-being of children and grandchildren: New evidence from South Africa. (Working paper: California Center for Population Research).</mixed-citation>
         </ref>
         <ref id="ref32">
            <mixed-citation publication-type="other">Hawkins, D.N., Amato, P.R., and King, V. (2007). Nonresident father involvement and adolescent well-being: Father effects or child effects? American Sociological Review 72(6): 990‒1010. doi:10.1177/000312240707200607.</mixed-citation>
         </ref>
         <ref id="ref33">
            <mixed-citation publication-type="other">Hill, C., Hosegood, V., and Newell, M.L. (2008). Children’s care and living arrangements in a high HIV prevalence area in rural South Africa. Vulnerable Children and Youth Studies 3(1): 65‒77. doi:10.1080/17450120701602091.</mixed-citation>
         </ref>
         <ref id="ref34">
            <mixed-citation publication-type="other">Hofferth, S.L. (2006). Residential father family type and child well-being: Investment versus selection. Demography 43(1): 53‒77. doi:10.1353/dem.2006.0006.</mixed-citation>
         </ref>
         <ref id="ref35">
            <mixed-citation publication-type="other">Hosegood, V., Floyd, S., Marston, M., Hill, C., McGrath, N., Isingo, R., Campin, A., and Zaba, B. (2007). The effects of high HIV prevalence on orphanhood and living arrangements of children in Malawi, Tanzania, and South Africa. Population Studies 61(3): 327‒336. doi:10.1080/00324720701524292.</mixed-citation>
         </ref>
         <ref id="ref36">
            <mixed-citation publication-type="other">Isiugo-Abanihe, U.C. (1985). Child fosterage in West Africa. Population and Development Review 11(1): 53‒73. doi:10.2307/1973378.</mixed-citation>
         </ref>
         <ref id="ref37">
            <mixed-citation publication-type="other">Junod, H.L. (1962). The life of a South African tribe. New Hyde Park: University Books.</mixed-citation>
         </ref>
         <ref id="ref38">
            <mixed-citation publication-type="other">Kazeem, A., Jensen, L., and Stokes, C.S. (2010). School attendance in Nigeria: Understanding the impact and intersection of gender, urban-rural residence, and socioeconomic status. Comparative Education Review 54(2): 295‒319. doi:10.1086/652139.</mixed-citation>
         </ref>
         <ref id="ref39">
            <mixed-citation publication-type="other">Kuhn, R. (2006). The effects of fathers’ and siblings’ migration on children’s pace of schooling in rural Bangladesh. Asian Population Studies 2(1): 69‒92. doi:10.1080/17441730600700572.</mixed-citation>
         </ref>
         <ref id="ref40">
            <mixed-citation publication-type="other">Lam, D., Ardington, C., and Leibbrandt, M. (2011). Schooling as a lottery: Racial differences in school advancement in urban South Africa. Journal of Development Economics 95(2): 121‒136. doi:10.1016/j.jdeveco.2010.05.005.</mixed-citation>
         </ref>
         <ref id="ref41">
            <mixed-citation publication-type="other">Lindskog, A. (2013). The effect of siblings’ education on school-entry in the Ethiopian highlands. Economics of Education Review 34: 45‒68. doi:10.1016/j.econedu rev.2013.01.012.</mixed-citation>
         </ref>
         <ref id="ref42">
            <mixed-citation publication-type="other">Lloyd, C.B. and Blanc, A.K. (1996). Children’s schooling in sub-Saharan Africa: The role of fathers, mothers, and others. Population and Development Review 22(2): 265‒298. doi:10.2307/2137435.</mixed-citation>
         </ref>
         <ref id="ref43">
            <mixed-citation publication-type="other">Lloyd, C.B. and Desai, S. (1992). Children’s living arrangements in developing countries. Population Research and Policy Review 11(3): 193‒216. doi:10.1007/BF00124937.</mixed-citation>
         </ref>
         <ref id="ref44">
            <mixed-citation publication-type="other">Mabika, C.M. and Shapiro, D. (2012). School enrollment in the Democratic Republic of the Congo: family economic well-being, gender, and place of residence. African Population Studies 26(2): 197‒220. doi:10.11564/26-2-213.</mixed-citation>
         </ref>
         <ref id="ref45">
            <mixed-citation publication-type="other">Madhavan, S. and Crowell, J. (2014). Who would you like to be like? Family, village, and national role models among Black youth in rural South Africa. Journal of Adolescent Research 29(6): 716‒737. doi:10.1177/0743558413502535.</mixed-citation>
         </ref>
         <ref id="ref46">
            <mixed-citation publication-type="other">Madhavan, S., Clark, S., Beguy, D., Kabiru, C., and Gross, M. (2017). Moving beyond the household: Innovations in data collection on kinship. Population Studies 71: 117‒132. doi:10.1080/00324728.2016.1262965.</mixed-citation>
         </ref>
         <ref id="ref47">
            <mixed-citation publication-type="other">Madhavan, S., Schatz, E., Clark, S., and Collinson, M. (2012). Child mobility, maternal status, and household composition in rural South Africa. Demography 49(2): 699‒718. doi:10.1007/s13524-011-0087-3.</mixed-citation>
         </ref>
         <ref id="ref48">
            <mixed-citation publication-type="other">Mberu, B. (2007). Household structure and living conditions in Nigeria. Journal of Marriage and the Family 69(2): 513‒527. doi:10.1111/j.1741-3737.2007.00 380.x.</mixed-citation>
         </ref>
         <ref id="ref49">
            <mixed-citation publication-type="other">Meillasoux, C. (1981). Maidens, meal and money: Capitalism and the domestic community. Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="ref50">
            <mixed-citation publication-type="other">Mkhize, N. (2006). African traditions and the social, economic and moral dimensions of fatherhood. In: Richter, L. and Morrell, R. (eds.). Baba: men and fatherhood in South Africa. Pretoria: HSRC Press: 183‒198.</mixed-citation>
         </ref>
         <ref id="ref51">
            <mixed-citation publication-type="other">Ngom, P., Magadi, M.A., and Owuor, T. (2003). Parental presence and adolescent reproductive health among the Nairobi urban poor. Journal of Adolescent Health 33(5): 369‒377. doi:10.1016/S1054-139X(03)00213-1.</mixed-citation>
         </ref>
         <ref id="ref52">
            <mixed-citation publication-type="other">Niehaus, I. (2001). Witchcraft, power and politics: Exploring the occult in the South African Lowveld. London: Pluto Press.</mixed-citation>
         </ref>
         <ref id="ref53">
            <mixed-citation publication-type="other">Nyamukapa, C. and Gregson, S. (2005). Extended family’s and women’s roles in safeguarding orphans’ education in AIDS-afflicted rural Zimbabwe. Social Science and Medicine 60(10): 2155‒2167. doi:10.1016/j.socscimed.2004.10. 005.</mixed-citation>
         </ref>
         <ref id="ref54">
            <mixed-citation publication-type="other">Parker, E. and Short, S. (2009). Grandmother co-residence, maternal orphans, and school enrollment in sub-Saharan Africa. Journal of Family Issues 30(6): 813‒ 836. doi:10.1177/0192513X09331921.</mixed-citation>
         </ref>
         <ref id="ref55">
            <mixed-citation publication-type="other">Pedzisai, N. and Nompumelelo, N. (2016). Impact of family structure on schooling outcomes for children in South Africa. In: Makawane, M., Nduna, M., and Khalema, N. (eds.). Children in South African families: Lives and times. Newcastle Upon Tyne: Cambridge Scholars Publishing: 58‒115.</mixed-citation>
         </ref>
         <ref id="ref56">
            <mixed-citation publication-type="other">Posel, D. (2001). Who are the heads of household, what do they do, and is the concept of headship useful? An analysis of headship in South Africa. Development South Africa 18(5): 651‒670. doi:10.1080/03768350120097487.</mixed-citation>
         </ref>
         <ref id="ref57">
            <mixed-citation publication-type="other">Richardson Jr, J.B. (2009). Men do matter: Ethnographic insights on the socially supportive role of the African American uncle in the lives of inner-city African American male youth. Journal of Family Issues 30(8): 1041‒1069. doi:10.1177/0192513X08330930.</mixed-citation>
         </ref>
         <ref id="ref58">
            <mixed-citation publication-type="other">Sear, R., Steele, F., McGregor, I.A., and Mace, R. (2002). The effects of kin on child mortality in rural Gambia. Demography 39(1): 43‒63. doi:10.1353/dem.2002. 0010.</mixed-citation>
         </ref>
         <ref id="ref59">
            <mixed-citation publication-type="other">Sen, A. (1990). Gender and cooperative conflicts. Tinker, I.I. (ed.). Persistent inequalities: Women and world development. Oxford: Oxford University Press: 123‒149.</mixed-citation>
         </ref>
         <ref id="ref60">
            <mixed-citation publication-type="other">Shapiro, D. and Tambashe, B.O. (2001). Gender, poverty, family structure, and investments in children’s education in Kinshasa, Congo. Economics of Education Review 20(4): 359‒375. doi:10.1016/S0272-7757(00)00059-5.</mixed-citation>
         </ref>
         <ref id="ref61">
            <mixed-citation publication-type="other">Smith, D. (2001). Romance, parenthood, and gender in a modern African society. Ethnology 40(2): 129‒151. doi:10.2307/3773927.</mixed-citation>
         </ref>
         <ref id="ref62">
            <mixed-citation publication-type="other">Spaull, N. (2013). Poverty and privilege: Primary school inequality in South Africa. International Journal of Educational Development 33(5): 436‒447. doi:10.1016/j.ijedudev.2012.09.009.</mixed-citation>
         </ref>
         <ref id="ref63">
            <mixed-citation publication-type="other">Stucki, B.R. (1995). Managing the social clock: The negotiation of elderhood among rural Asante of Ghana. [PhD thesis]. Northwestern University.</mixed-citation>
         </ref>
         <ref id="ref64">
            <mixed-citation publication-type="other">Taylor, S. and Yu, D. (2009). The importance of socio-economic status in determining educational achievement in South Africa. Unpublished working paper (Economics). Stellenbosch: Stellenbosch University.</mixed-citation>
         </ref>
         <ref id="ref65">
            <mixed-citation publication-type="other">Thomas, D. (1990). Intra-household resource allocation: An inferential approach. Journal of Human Resources 25(4): 635‒664. doi:10.2307/145670.</mixed-citation>
         </ref>
         <ref id="ref66">
            <mixed-citation publication-type="other">Timaeus, I. and Moultrie, T. (2008). On postponement and birth intervals. Population and Development Review 34(3): 483‒510. doi:10.1111/j.1728-4457.2008.00 233.x.</mixed-citation>
         </ref>
         <ref id="ref67">
            <mixed-citation publication-type="other">Townsend, N., Madhavan, S., Tollman, S., Garenne, M., and Kahn, K. (2002). Children’s residence patterns and educational attainment in rural South Africa, 1997. Population Studies 56(2): 215‒225. doi:10.1080/00324720215925.</mixed-citation>
         </ref>
         <ref id="ref68">
            <mixed-citation publication-type="other">UNAIDS (2002). Report on the Global HIV/AIDS Epidemic. Geneva: UNAIDS.</mixed-citation>
         </ref>
         <ref id="ref69">
            <mixed-citation publication-type="other">Wittenberg, M. and Collinson, M. (2007). Household transitions in rural South Africa, 1996‒2003. Scandinavian Journal of Public Health 35(69 suppl.): 130‒137. doi:10.1080/14034950701355429.</mixed-citation>
         </ref>
         <ref id="ref70">
            <mixed-citation publication-type="other">World Bank (2002). Education and HIV/AIDS: A window of hope. Washington, D.C.: World Bank. doi:10.1596/0-8213-5117-6.</mixed-citation>
         </ref>
         <ref id="ref71">
            <mixed-citation publication-type="other">Yamano, T., Shimamura, Y., and Sserunkuuma, D. (2006). Living arrangements and schooling of orphaned children and adolescents in Uganda. Economic Development and Cultural Change 54(4): 833‒856. doi:10.1086/503586.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
