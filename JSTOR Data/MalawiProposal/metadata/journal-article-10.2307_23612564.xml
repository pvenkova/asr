<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">histeconsoci</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50008870</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Histoire, Économie et Société</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>SEDES</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">07525702</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">17775906</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">23612564</article-id>
         <title-group>
            <article-title>ÉCOLOGIE ET HISTOIRE EN AFRIQUE NOIRE</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Catherine</given-names>
                  <surname>COQUERY-VIDROVITCH</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>7</month>
            <year>1997</year>
      
            <day>1</day>
            <month>9</month>
            <year>1997</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">16</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i23612528</issue-id>
         <fpage>483</fpage>
         <lpage>504</lpage>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/23612564"/>
         <abstract>
            <p>L'article s'interroge d'abord sur la faisabilité d'une histoire de l'environnement, compte tenu de la rareté des sources. L'histoire de la pluviométrie, élément clef du climat en Afrique noire, a été tentée y compris pour la période précoloniale. On repère ainsi, dans le Sahel occidental mais aussi sur la côte angolaise ou en Éthiopie, la succession des sécheresses et des famines depuis le XVIe siècle. Mais nos données deviennent nettement plus serrées à partir du XIXe siècle. Au XXe siècle, les enquêtes orales offrent des précisions utiles. Il n'y a pas de déterminisme en histoire et, malgré des conditions climatiques difficiles, les sociétés africaines ont su répondre, à leur manière, aux défis de la nature. Mais les facteurs d'aggravation se sont démultipliés depuis l'époque contemporaine. La conjonction de périodes longues de sécheresse — ce qui n'est pas nouveau — et d'une expansion démographique en revanche sans précédent risque, pour la première fois dans l'histoire, d'avoir des effets irréversibles. The first question is to know whether or not a history of the environment is possible, when one knows how poor may be the possible sources. A history of rainfalls is a key question for African history. It now exists, including the precolonial era. From the sixteenth century onwards, we know, as well in the Western Sudan as in coastal Angola or in Ethiopia how and when droughts and famines occurred. Nevertheless, precisions grow better and better from the nineteenth century. For the twentieth century, oral inquiries may prove quite useful. History is not predetermined, and in spite of a vulnerable climate, African societies knew, according to their abilities, how to adapt to natural challenges. But a series of factors made things more and more difficult since modern times. The coeval occurrence of long dry periods of time — which was not an innovation — with a dramatic recent population boom — which is quite a new trend — may for the first time result into non reversable ecological change.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>fre</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d197e196a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d197e203" publication-type="other">
Guy Bois dans La mutation de l'an Mil, Paris, Fayard, 1989.</mixed-citation>
            </p>
         </fn>
         <fn id="d197e210a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d197e217" publication-type="other">
Benedict Anderson, L'imaginaire national. Réflexions sur l'origine et l'essor du nationalisme, Paris,
Maspero, 1996.</mixed-citation>
            </p>
         </fn>
         <fn id="d197e227a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d197e234" publication-type="other">
John Iliffe, The African Poor. A History, Oxford University Press, 1981.</mixed-citation>
            </p>
         </fn>
         <fn id="d197e241a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d197e248" publication-type="other">
Ester Boserup, La femme face au développement économique, Paris, PUF, 1983, p. 1-37.</mixed-citation>
            </p>
         </fn>
         <fn id="d197e256a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d197e263" publication-type="other">

              Catherine Coquery-Vidrovitch, Les Africaines. Histoire des femmes d'Afrique noire du XIX
              
              au
            

              XX
              
              siècle, Paris, Desjonquères, 1994.
            </mixed-citation>
            </p>
         </fn>
         <fn id="d197e279a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d197e286" publication-type="other">
Alpha Gado Boureïma, Crises alimentaires et stratégies de subsistance en Afrique sahélienne, Paris,
L'Harmattan, 1992.</mixed-citation>
            </p>
         </fn>
         <fn id="d197e296a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d197e303" publication-type="other">
Pierre Boilley, Les Kel Adagh et les colonisations (1893-1993), Thèse de l'Université Paris-Vil,
1994, sous presse à Karthala.</mixed-citation>
            </p>
         </fn>
         <fn id="d197e313a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d197e320" publication-type="other">
Michael Watts, Silent Violence (Food, Famine and Peasantry in Northern Nigeria), Berkeley,
University of California Press, 1983.</mixed-citation>
            </p>
         </fn>
         <fn id="d197e330a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d197e337" publication-type="other">
J.B. Webster, ed., Chronology, Migration and Drought in Interlacustrine Africa, Londres, 1979,
347 p.</mixed-citation>
            </p>
         </fn>
         <fn id="d197e347a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d197e354" publication-type="other">
J.-C. Miller, « The significance of Drought, Disease and Famine in the Agriculturally Marginal
Zones of West-Central Africa », Journal of African History, XXIII, 1, 1982, p. 17-61.</mixed-citation>
            </p>
         </fn>
         <fn id="d197e365a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d197e372" publication-type="other">
Rapports coloniaux et textes d'époque cités par Alpha Gado Bouréïma, Crises économiques et stra-
tégies de subsistances, thèse ronéotée, Université Paris-VII, 1988, t. 1, p. 93.</mixed-citation>
            </p>
         </fn>
         <fn id="d197e382a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d197e389" publication-type="other">
John Ford, The Role of Trypanosomiases in African Ecology. A Study of the Tse-tse Fly Problem,
Londres, Oxford Clarendon Press, 1971.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d197e398" publication-type="other">
Helge Kjekshus, Ecology Control and Economic Development in
East Africa : the Case of Tanganyika 1850-1950, Londres, Heinemaan, 1977, 215 p.</mixed-citation>
            </p>
         </fn>
         <fn id="d197e408a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d197e415" publication-type="other">
Claire Bernard, Les aménagements du fleuve Sénégal pendant la colonisation 1850-1920, Thèse de
l'Université Paris-Vil, 1995.</mixed-citation>
            </p>
         </fn>
         <fn id="d197e425a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d197e432" publication-type="other">
Jean-Paul Harroy, Afrique, Terre qui meurt. La dégradation des sols africains sous l'influence
de la colonisation, Paris, Paul Lechevalier, 1944, 553 p.</mixed-citation>
            </p>
         </fn>
         <fn id="d197e442a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d197e449" publication-type="other">
Op. cit., note 9.</mixed-citation>
            </p>
         </fn>
         <fn id="d197e456a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d197e463" publication-type="other">
Megan Vaughan, The Story of an African Famine. Gender and Famine in Twentieth Century
Malawi, Cambridge University Press, 1987, 183 p.</mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
