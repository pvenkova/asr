<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">thirworlquar</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j101236</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Third World Quarterly</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Routledge, Taylor &amp; Francis</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">01436597</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">13602241</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">40388304</article-id>
         <article-id pub-id-type="pub-doi">10.1080/01436590903037424</article-id>
         <title-group>
            <article-title>Making Sense of Mugabeism in Local and Global Politics: 'So Blair, Keep Your England and Let Me Keep My Zimbabwe'</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Sabelo J.</given-names>
                  <surname>Ndlovu-Gatsheni</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>1</month>
            <year>2009</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">30</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">6</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40017308</issue-id>
         <fpage>1139</fpage>
         <lpage>1158</lpage>
         <permissions>
            <copyright-statement>Copyright 2009 Third World Quarterly</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:href="http://dx.doi.org/10.1080/01436590903037424"
                   xlink:title="an external site"/>
         <abstract>
            <p>President Robert Mugabe of Zimbabwe has emerged as one of the most controversial political figures since 2000, eliciting both admiration and condemnation. What is termed 'Mugabeism' is a summation of a constellation of political controversies, political behaviour, political ideas, utterances, rhetoric and actions that have crystallised around Mugabe's political life. It is a contested phenomenon with the nationalist aligned scholars understanding it as a pan-African redemptive ideology opposed to all forms of imperialism and colonialism and dedicated to a radical redistributive project predicated on redress of colonial injustices. A neoliberal-inspired perspective sees Mugabeism as a form of racial chauvinism and authoritarianism marked by antipathy towards norms of liberal governance and disdain for human rights and democracy. This article seeks to analyse Mugabeism as populist phenomenon propelled through articulatory practices and empty signifiers. As such it can be read at many levels: as a form of left-nationalism; as Afro-radicalism and nativism; a patriarchal neo-traditional cultural nationalism and an antithesis of democracy and human rights. All these representations make sense within the context of colonial, nationalist, postcolonial and even precolonial history that Mugabe has deployed to sustain and support his political views.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Notes</title>
         <ref id="d761e119a1310">
            <label>1</label>
            <mixed-citation id="d761e128" publication-type="other">
New African Magazine: Zimbabwe Special Issue, Summer 2007, pp 5-6.</mixed-citation>
            <mixed-citation id="d761e134" publication-type="other">
'Short, Clare, how it all started', New African Magazine, March
2002. The fallout between Mugabe and Blair was further exacerbated by the British imposition of an
arms embargo against Zimbabwe in May 2000.</mixed-citation>
         </ref>
         <ref id="d761e147a1310">
            <label>2</label>
            <mixed-citation id="d761e154" publication-type="other">
The Herald, 6 December 1997.</mixed-citation>
         </ref>
         <ref id="d761e161a1310">
            <label>3</label>
            <mixed-citation id="d761e168" publication-type="other">
J Chaumba, I Scoones &amp; W Wolmer, 'New politics, new livelihoods: agrarian change in
Zimbabwe', Review of African Political Economy, 98, 2003, pp 585-608.</mixed-citation>
         </ref>
         <ref id="d761e178a1310">
            <label>4</label>
            <mixed-citation id="d761e185" publication-type="other">
S Zizek, The Sublime Object of Ideology, London: Phronesis, 1989.</mixed-citation>
         </ref>
         <ref id="d761e193a1310">
            <label>5</label>
            <mixed-citation id="d761e200" publication-type="other">
SJ Ndlovu-Gatsheni, 'Patriots, puppets, dissidents and the politics of inclusion and exclusion in
contemporary Zimbabwe', Eastern Africa Social Science Research Review, XXIV (1), 2008, pp 81-
108.</mixed-citation>
         </ref>
         <ref id="d761e213a1310">
            <label>6</label>
            <mixed-citation id="d761e220" publication-type="other">
E Laclau, On Populist Reason, London: Verso, 2005, pp ix-xi.</mixed-citation>
         </ref>
         <ref id="d761e227a1310">
            <label>7</label>
            <mixed-citation id="d761e234" publication-type="other">
G Germani, Authoritarianism, Fascism and National Populism, Brunswick, NJ: Transaction Books,
1978, p 88.</mixed-citation>
         </ref>
         <ref id="d761e244a1310">
            <label>8</label>
            <mixed-citation id="d761e251" publication-type="other">
Ρ Worsley, 'The concept of populism', in G Ionescu &amp; E Gellner (eds), Populism: Its Meaning and
National Characteristics, London: Macmillan, 1969, p 213.</mixed-citation>
         </ref>
         <ref id="d761e261a1310">
            <label>9</label>
            <mixed-citation id="d761e268" publication-type="other">
A Norman, Mugabe: Teacher, Revolutionary, Tyrant, Gloucestershire: History Press, 2008.</mixed-citation>
         </ref>
         <ref id="d761e275a1310">
            <label>10</label>
            <mixed-citation id="d761e282" publication-type="other">
Ibid, p 161.</mixed-citation>
         </ref>
         <ref id="d761e290a1310">
            <label>11</label>
            <mixed-citation id="d761e297" publication-type="other">
E Laclau &amp; C Mouffe, Hegemony and Socialist Strategy, London: Verso, 1985, p 67.</mixed-citation>
         </ref>
         <ref id="d761e304a1310">
            <label>12</label>
            <mixed-citation id="d761e311" publication-type="other">
Laclau, On Populist Reason, p 74.</mixed-citation>
         </ref>
         <ref id="d761e318a1310">
            <label>13</label>
            <mixed-citation id="d761e325" publication-type="other">
E Laclau, 'Towards a theory of populism', in Laclau (ed), Politics and Ideology in Marxist Theory,
London: New Left Books, 1977.</mixed-citation>
         </ref>
         <ref id="d761e335a1310">
            <label>14</label>
            <mixed-citation id="d761e342" publication-type="other">
Laclau, On Populist Reason, p 225.</mixed-citation>
         </ref>
         <ref id="d761e349a1310">
            <label>15</label>
            <mixed-citation id="d761e356" publication-type="other">
D Moore, 'Is land the economy and the economy the land? Primitive accumulation in Zimbabwe ,
Journal of Contemporary African Studies, 19 (2), 2001, pp 253-266.</mixed-citation>
         </ref>
         <ref id="d761e366a1310">
            <label>16</label>
            <mixed-citation id="d761e373" publication-type="other">
E Laclau, 'Ideology and post-Marxism', Journal of Political Ideologies, 11 (2), 2006, p 112. Emphasis
in original.</mixed-citation>
         </ref>
         <ref id="d761e384a1310">
            <label>17</label>
            <mixed-citation id="d761e391" publication-type="other">
E Laclau, 'Why do empty signifiers matter in politics? , in Laclau, Emancipatwn(s), London: Verso,
1996, p 44.</mixed-citation>
         </ref>
         <ref id="d761e401a1310">
            <label>18</label>
            <mixed-citation id="d761e408" publication-type="other">
Norman, Mugabe, p 18.</mixed-citation>
         </ref>
         <ref id="d761e415a1310">
            <label>19</label>
            <mixed-citation id="d761e422" publication-type="other">
DB Moore, 'The ideological formation of the Zimbabwean ruling class', Journal of Southern African
Studies, 17(3), 1991, p 492.</mixed-citation>
         </ref>
         <ref id="d761e432a1310">
            <label>20</label>
            <mixed-citation id="d761e439" publication-type="other">
Ranger, 'The changing of the old guard: Robert Mugabe and the revival of ZANU ,
Journal of Southern African Studies: Special Issue on Contemporary Politics, 7(1), 1980, p 83.</mixed-citation>
         </ref>
         <ref id="d761e449a1310">
            <label>21</label>
            <mixed-citation id="d761e456" publication-type="other">
'Comrade Mugabe lays the line at historic Chimoio Central Committee Meeting', Zimbabwe News,
July-December 1977, p 14.</mixed-citation>
         </ref>
         <ref id="d761e466a1310">
            <label>22</label>
            <mixed-citation id="d761e473" publication-type="other">
RG Mugabe, 'Independence message', Struggles for Independence: Documents of Recent Developments
of Zimbabwe, 1975-1980, Vol 7, Hamburg: Institute of African Studies Documentation Centre,
December 1979-April 1980.</mixed-citation>
         </ref>
         <ref id="d761e487a1310">
            <label>23</label>
            <mixed-citation id="d761e494" publication-type="other">
I Mandaza, 'Reconciliation and social justice in Southern Africa: the Zimbabwe experience', in MW
Malegapuru (ed), African Renaissance: The New Struggle, Cape Town: Mafube Publishing, 1999, p 79.</mixed-citation>
         </ref>
         <ref id="d761e504a1310">
            <label>24</label>
            <mixed-citation id="d761e511" publication-type="other">
Ibid</mixed-citation>
         </ref>
         <ref id="d761e518a1310">
            <label>25</label>
            <mixed-citation id="d761e525" publication-type="other">
Β Raftopoulos, 'The Zimbabwean crisis and the challenges of the Left , Journal of Southern African
Studies, 32 (2), 2006, p 203.</mixed-citation>
         </ref>
         <ref id="d761e535a1310">
            <label>26</label>
            <mixed-citation id="d761e542" publication-type="other">
S Moyo &amp; Ρ Yeros, 'Intervention: the Zimbabwe question and the two lefts', Historical Materialism,
15, 2007, pp 173-174.</mixed-citation>
         </ref>
         <ref id="d761e552a1310">
            <label>27</label>
            <mixed-citation id="d761e559" publication-type="other">
S Moyo &amp; Ρ Yeros, 'The radicalised state: Zimbabwe's interrupted revolution', Review of African
Political Economy, 111, 2007, p 106.</mixed-citation>
         </ref>
         <ref id="d761e569a1310">
            <label>28</label>
            <mixed-citation id="d761e576" publication-type="other">
Ibid, p 106.</mixed-citation>
         </ref>
         <ref id="d761e584a1310">
            <label>29</label>
            <mixed-citation id="d761e591" publication-type="other">
Ibid, pp 106-107.</mixed-citation>
         </ref>
         <ref id="d761e598a1310">
            <label>30</label>
            <mixed-citation id="d761e605" publication-type="other">
M Mamdani, 'Lessons of Zimbabwe', London Review of Books, 30 (23), 2008, p 1.</mixed-citation>
         </ref>
         <ref id="d761e612a1310">
            <label>31</label>
            <mixed-citation id="d761e619" publication-type="other">
Ibid, p 3.</mixed-citation>
         </ref>
         <ref id="d761e626a1310">
            <label>32</label>
            <mixed-citation id="d761e633" publication-type="other">
Τ Scarnecchia, J Alexander and 33 others, 'Lessons of Zimbabwe', at http://ww.lrb.co.uk/v31/n01/
letters.html</mixed-citation>
         </ref>
         <ref id="d761e643a1310">
            <label>33</label>
            <mixed-citation id="d761e650" publication-type="other">
Τ Ranger, 'From Terence Ranger', at http://ww.lrb.co.uk/v31/n01/letters.html</mixed-citation>
         </ref>
         <ref id="d761e657a1310">
            <label>34</label>
            <mixed-citation id="d761e664" publication-type="other">
H Campbell, 'Mamdani, Mugabe and the African scholarly community', at http ://www. pambazu-
ka.org/en/category/features/52845</mixed-citation>
         </ref>
         <ref id="d761e675a1310">
            <label>35</label>
            <mixed-citation id="d761e682" publication-type="other">
I Phimister &amp; Β Raftopoulos, 'Mugabe, Mbeki and the politics of anti-imperialism', Review of African
Political Economy, 101, 2004, pp 385-400.</mixed-citation>
         </ref>
         <ref id="d761e692a1310">
            <label>36</label>
            <mixed-citation id="d761e699" publication-type="other">
Β Raftopoulos, 'The Zimbabwean crisis and the challenges for the Left', Journal of Southern African
Studies, 32 (2), 2006, p 203.</mixed-citation>
         </ref>
         <ref id="d761e709a1310">
            <label>37</label>
            <mixed-citation id="d761e716" publication-type="other">
Ibid, p 219.</mixed-citation>
         </ref>
         <ref id="d761e723a1310">
            <label>38</label>
            <mixed-citation id="d761e730" publication-type="other">
British Secretary of State for Foreign and Commonwealth Affairs, Response of the Secretary of State
for Session 2001-2002, London: Stationary Office, 2002, pp 2-3.</mixed-citation>
         </ref>
         <ref id="d761e740a1310">
            <label>39</label>
            <mixed-citation id="d761e747" publication-type="other">
S Mair &amp; M Sithole, Blocked Democracies in Africa: Case Study of Zimbabwe, Harare: Konrad
Adenauer Stiftung, 2002, p 22.</mixed-citation>
         </ref>
         <ref id="d761e757a1310">
            <label>40</label>
            <mixed-citation id="d761e764" publication-type="other">
SJ Ndlovu-Gatsheni, 'Dynamics of the Zimbabwe crisis in the 21st century', African Journal on
Conflict Resolution, 3 (1), 2003, pp 99-105.</mixed-citation>
         </ref>
         <ref id="d761e775a1310">
            <label>41</label>
            <mixed-citation id="d761e782" publication-type="other">
C Young, The African Colonial State in Comparative Perspective, New Haven, CT: Yale University
Press, 1994, pp 283, 225.</mixed-citation>
         </ref>
         <ref id="d761e792a1310">
            <label>42</label>
            <mixed-citation id="d761e799" publication-type="other">
Τ Ranger, 'Introduction to Volume Two', in Ranger (ed), The Historical Dimensions of Democracy
and Human Rights in Zimbabwe, Vol 2, Nationalism, Democracy and Human Rights, Harare:
University of Zimbabwe, 2003, pp 1-37.</mixed-citation>
         </ref>
         <ref id="d761e812a1310">
            <label>43</label>
            <mixed-citation id="d761e819" publication-type="other">
K-S Chen, 'Introduction: the decolonisation question', in Chen (ed), Trajectories: Inter-Asia Cultural
Studies, London: Routledge, 1998, p 14.</mixed-citation>
         </ref>
         <ref id="d761e829a1310">
            <label>44</label>
            <mixed-citation id="d761e836" publication-type="other">
M Mamdani, 'When does a settler become a native? Citizenship and identity in a settler society',
Pretext: Literacy and Cultural Studies, 10, (1), 2001, pp 63-66.</mixed-citation>
         </ref>
         <ref id="d761e846a1310">
            <label>45</label>
            <mixed-citation id="d761e853" publication-type="other">
The Herald, 6 December 1997.</mixed-citation>
         </ref>
         <ref id="d761e860a1310">
            <label>46</label>
            <mixed-citation id="d761e867" publication-type="other">
M Mamdani, 'When does a settler become a native? Reflections on the roots of citizenship in
equatorial and South Africa', text of Inaugural Lecture delivered as AC Jordan Professor of African
Studies, University of Cape Town, 13 May 1998.</mixed-citation>
         </ref>
         <ref id="d761e881a1310">
            <label>47</label>
            <mixed-citation id="d761e888" publication-type="other">
F Fanon, The Wretched of the Earth, New York: Grove, 1968, pp 157-159.</mixed-citation>
         </ref>
         <ref id="d761e895a1310">
            <label>48</label>
            <mixed-citation id="d761e902" publication-type="other">
Ρ Bond, 'Radical rhetoric and the working class during Zimbabwean nationalism's dying days',
Journal of World-Systems Research, VII (1), 2001, p 59.</mixed-citation>
         </ref>
         <ref id="d761e912a1310">
            <label>49</label>
            <mixed-citation id="d761e919" publication-type="other">
Phimister &amp; Raftopoulos, 'Mugabe, Mbeki and the politics of anti-imperialism', p 285.</mixed-citation>
         </ref>
         <ref id="d761e926a1310">
            <label>50</label>
            <mixed-citation id="d761e933" publication-type="other">
M Mamdani, Ά brief history of genocide', Kwani, 2003, p 43.</mixed-citation>
         </ref>
         <ref id="d761e940a1310">
            <label>51</label>
            <mixed-citation id="d761e947" publication-type="other">
Β Raftopoulos, 'Nation, race and history in Zimbabwean polities', in S Dormán, D Hammett &amp; Ρ
Nugent (eds), Making Nations, Creating Strangers: States and Citizenship in Africa, Leiden: Brill,
2007, p 181.</mixed-citation>
         </ref>
         <ref id="d761e960a1310">
            <label>52</label>
            <mixed-citation id="d761e967" publication-type="other">
A Mbembe, 'African modes of self-writing', Public Culture, 14 (1), 2002, pp 239-273.</mixed-citation>
         </ref>
         <ref id="d761e975a1310">
            <label>53</label>
            <mixed-citation id="d761e982" publication-type="other">
A Mbembe, 'On the power of the false', Public Culture, 14 (3), 2002, p 629.</mixed-citation>
         </ref>
         <ref id="d761e989a1310">
            <label>54</label>
            <mixed-citation id="d761e996" publication-type="other">
A Mbembe, 'The cultural politics of South Africa's foreign policy: between black (inter)nationalism
and Afropolitanism', unpublished paper presented at Wits Institute of Economic and Social
Research, University of Witwatersrand, 2006, p 8.</mixed-citation>
         </ref>
         <ref id="d761e1009a1310">
            <label>55</label>
            <mixed-citation id="d761e1016" publication-type="other">
Ibid.</mixed-citation>
         </ref>
         <ref id="d761e1023a1310">
            <label>56</label>
            <mixed-citation id="d761e1030" publication-type="other">
Mbembe, 'African modes of self-writing', pp 239-240.</mixed-citation>
         </ref>
         <ref id="d761e1037a1310">
            <label>57</label>
            <mixed-citation id="d761e1044" publication-type="other">
Ibid. pp 240-241.</mixed-citation>
         </ref>
         <ref id="d761e1051a1310">
            <label>58</label>
            <mixed-citation id="d761e1058" publication-type="other">
Ibid, p 241.</mixed-citation>
         </ref>
         <ref id="d761e1066a1310">
            <label>59</label>
            <mixed-citation id="d761e1073" publication-type="other">
Ibid.</mixed-citation>
         </ref>
         <ref id="d761e1080a1310">
            <label>60</label>
            <mixed-citation id="d761e1087" publication-type="other">
Ibid.</mixed-citation>
         </ref>
         <ref id="d761e1094a1310">
            <label>61</label>
            <mixed-citation id="d761e1101" publication-type="other">
R Mugabe, Inside the Third Chimurenga, Harare: Government Printers, 2001, pp 92-93.</mixed-citation>
         </ref>
         <ref id="d761e1108a1310">
            <label>62</label>
            <mixed-citation id="d761e1115" publication-type="other">
Mbembe, 'African modes of self-writing', p 242.</mixed-citation>
         </ref>
         <ref id="d761e1122a1310">
            <label>63</label>
            <mixed-citation id="d761e1129" publication-type="other">
Ibid.</mixed-citation>
         </ref>
         <ref id="d761e1136a1310">
            <label>64</label>
            <mixed-citation id="d761e1143" publication-type="other">
Ibid, p 243.</mixed-citation>
         </ref>
         <ref id="d761e1151a1310">
            <label>65</label>
            <mixed-citation id="d761e1158" publication-type="other">
SJ Ndlovu-Gatsheni, 'Putting people first -from regime security to human security: a quest for social
peace in Zimbabwe, 1980-2002', in AG Nhema (ed), The Quest for Peace in Africa: Transformations,
Democracy and Public Policy, Utrecht &amp; Addis Ababa: International Books with OSSREA, 2004, pp
297-322.</mixed-citation>
         </ref>
         <ref id="d761e1174a1310">
            <label>66</label>
            <mixed-citation id="d761e1181" publication-type="other">
Mbembe, 'African modes of self-writing', pp 243-244.</mixed-citation>
         </ref>
         <ref id="d761e1188a1310">
            <label>67</label>
            <mixed-citation id="d761e1195" publication-type="other">
Ibid, p 244.</mixed-citation>
         </ref>
         <ref id="d761e1202a1310">
            <label>68</label>
            <mixed-citation id="d761e1209" publication-type="other">
Ibid, p 250.</mixed-citation>
         </ref>
         <ref id="d761e1216a1310">
            <label>69</label>
            <mixed-citation id="d761e1223" publication-type="other">
Ibid, p 251.</mixed-citation>
         </ref>
         <ref id="d761e1230a1310">
            <label>70</label>
            <mixed-citation id="d761e1237" publication-type="other">
Ibid.</mixed-citation>
         </ref>
         <ref id="d761e1245a1310">
            <label>71</label>
            <mixed-citation id="d761e1252" publication-type="other">
Ibid.</mixed-citation>
         </ref>
         <ref id="d761e1259a1310">
            <label>72</label>
            <mixed-citation id="d761e1266" publication-type="other">
KC Dunn, '"Sons of the soil" and contemporary state making: autochthony, uncertainty and political
violence in Africa'. Third World Ouarterlv. 30 (1). 2009. p 115.</mixed-citation>
         </ref>
         <ref id="d761e1276a1310">
            <label>73</label>
            <mixed-citation id="d761e1283" publication-type="other">
Mbembe, 'The power of the false', p 635.</mixed-citation>
         </ref>
         <ref id="d761e1290a1310">
            <label>74</label>
            <mixed-citation id="d761e1297" publication-type="other">
Dunn. '"Sons of the soil'", pp 114-115.</mixed-citation>
         </ref>
         <ref id="d761e1304a1310">
            <label>75</label>
            <mixed-citation id="d761e1311" publication-type="other">
Ν Kriger, 'From patriotic memories to "patriotic history" in Zimbabwe, 1990-2005', Third World
Quarterly, 27 (6), 2006, p 1154.</mixed-citation>
         </ref>
         <ref id="d761e1321a1310">
            <label>76</label>
            <mixed-citation id="d761e1328" publication-type="other">
A Appadurai, 'Dead certainty: ethnic violence in the era of globalisation', in Β Meyer &amp; Ρ Geschiere
(eds), Globalisation and Identity: Dialectics of Flow and Closure, Oxford: Blackwell, 1999, p 322.</mixed-citation>
         </ref>
         <ref id="d761e1339a1310">
            <label>77</label>
            <mixed-citation id="d761e1346" publication-type="other">
R Muponde, 'The worm and the hoe: cultural politics and reconciliation after the third chimurenga',
in Β Raftopoulos &amp; Τ Savage (eds), Zimbabwe: Injustice and Political Reconciliation, Cape Town:
Institute for Justice and Reconciliation, 2004, p 176.</mixed-citation>
         </ref>
         <ref id="d761e1359a1310">
            <label>78</label>
            <mixed-citation id="d761e1366" publication-type="other">
Ibid.</mixed-citation>
         </ref>
         <ref id="d761e1373a1310">
            <label>79</label>
            <mixed-citation id="d761e1380" publication-type="other">
Mbembe, 'African modes of self-writing', p 252.</mixed-citation>
         </ref>
         <ref id="d761e1387a1310">
            <label>80</label>
            <mixed-citation id="d761e1394" publication-type="other">
Ibid, p 252. Emphasis in the original.</mixed-citation>
         </ref>
         <ref id="d761e1401a1310">
            <label>8</label>
            <mixed-citation id="d761e1408" publication-type="other">
D Blair, Degrees in Violence: Robert Mugabe and the Struggle for Power in Zimbabwe, London &amp; New
York: Continuum, 2002.</mixed-citation>
         </ref>
         <ref id="d761e1418a1310">
            <label>82</label>
            <mixed-citation id="d761e1425" publication-type="other">
Mbembe, 'Africa modes ot self-writing', p 268.</mixed-citation>
         </ref>
         <ref id="d761e1433a1310">
            <label>83</label>
            <mixed-citation id="d761e1440" publication-type="other">
Ibid, p 260.</mixed-citation>
         </ref>
         <ref id="d761e1447a1310">
            <label>84</label>
            <mixed-citation id="d761e1454" publication-type="other">
Ibid, p 267.</mixed-citation>
         </ref>
         <ref id="d761e1461a1310">
            <label>85</label>
            <mixed-citation id="d761e1468" publication-type="other">
S Moyo, The Zimbabwe Crisis and Normalisation, Centre for Policy Studies: Issues
and Actors, 18 (7), 2005, Johannesburg, p 5.</mixed-citation>
         </ref>
         <ref id="d761e1478a1310">
            <label>86</label>
            <mixed-citation id="d761e1485" publication-type="other">
E Chitando, 'In the beginning was the land: the appropriation of religious themes in political
discourse in Zimbabwe', Africa: Journal of the International Studies Institute, 75 (2), 2005, pp 220-
239.</mixed-citation>
         </ref>
         <ref id="d761e1498a1310">
            <label>87</label>
            <mixed-citation id="d761e1505" publication-type="other">
Mugabe, Inside the Third Chimurenga</mixed-citation>
         </ref>
         <ref id="d761e1512a1310">
            <label>88</label>
            <mixed-citation id="d761e1519" publication-type="other">
Terence Ranger, 'Nationalist historiography, patriotic history and history of the nation: the struggle
over the past in Zimbabwe', Journal of Southern African Studies, 30 (2), 2004, pp 215-234.</mixed-citation>
            <mixed-citation id="d761e1528" publication-type="other">
L
Bull-Chrisstiansen, Tales of the Nation: Feminist Nationalism or Patriotic History? Defining National
History and Identity in Zimbabwe, Uppsala: Nordiska Afrikainstitutet, 2004.</mixed-citation>
         </ref>
         <ref id="d761e1542a1310">
            <label>89</label>
            <mixed-citation id="d761e1549" publication-type="other">
Muponde, 'The worm and the hoe', p 177.</mixed-citation>
         </ref>
         <ref id="d761e1556a1310">
            <label>90</label>
            <mixed-citation id="d761e1563" publication-type="other">
Chitando, 'In the beginning was the land', p 224.</mixed-citation>
         </ref>
         <ref id="d761e1570a1310">
            <label>91</label>
            <mixed-citation id="d761e1577" publication-type="other">
Ibid.</mixed-citation>
         </ref>
         <ref id="d761e1584a1310">
            <label>92</label>
            <mixed-citation id="d761e1591" publication-type="other">
B-M Tendi, 'Patriotic history and public intellectuals critical of power', Journal of Southern
African Studies, 34 (2), 2008, p 386.</mixed-citation>
         </ref>
         <ref id="d761e1601a1310">
            <label>93</label>
            <mixed-citation id="d761e1608" publication-type="other">
E Worby, 'The end of modernity in Zimbabwe? Passages from development to sovereignty', in A
Hammar, Β Raftopoulos &amp; S Jensen (eds), Zimbabwe's Unfinished Business: Rethinking Land, State
and Nation in the Context of Crisis, Harare: Weaver Press, 2003, p 70.</mixed-citation>
         </ref>
         <ref id="d761e1621a1310">
            <label>94</label>
            <mixed-citation id="d761e1628" publication-type="other">
Muponde, 'The worm and the hoe', p 191.</mixed-citation>
         </ref>
         <ref id="d761e1636a1310">
            <label>95</label>
            <mixed-citation id="d761e1643" publication-type="other">
Worby, 'The end of modernity in Zimbabwe?', p 71.</mixed-citation>
         </ref>
         <ref id="d761e1650a1310">
            <label>96</label>
            <mixed-citation id="d761e1657" publication-type="other">
Chitando, 'In the beginning was the land', p 229.</mixed-citation>
         </ref>
         <ref id="d761e1664a1310">
            <label>97</label>
            <mixed-citation id="d761e1671" publication-type="other">
Chitando, 'In the beginning was the land', p 229.</mixed-citation>
            <mixed-citation id="d761e1677" publication-type="other">
E Chitando, '"Down with the
Devil, forward with Christ!" A study of the interface between religious and political discourses in
Zimbabwe', African Sociological Review, 6(1), 2002, pp 1-16.</mixed-citation>
         </ref>
         <ref id="d761e1690a1310">
            <label>98</label>
            <mixed-citation id="d761e1697" publication-type="other">
Ambuya Nehanda and Chaminuka were pre-colonial religious figures who were renowned for
predicting and seeing into the future. Nehanda was accused and hanged by the British colonialists for
having been the brains behind the Shona uprising of 1896-97.</mixed-citation>
         </ref>
         <ref id="d761e1710a1310">
            <label>100</label>
            <mixed-citation id="d761e1717" publication-type="other">
JG Todd, Through the Darkness: A Life in Zimbabwe, Cape Town: Zebra Press, 2007, p 424.</mixed-citation>
         </ref>
         <ref id="d761e1724a1310">
            <label>101</label>
            <mixed-citation id="d761e1731" publication-type="other">
Citizenship of Zimbabwe Amendment Act, 2003.</mixed-citation>
         </ref>
         <ref id="d761e1739a1310">
            <label>102</label>
            <mixed-citation id="d761e1746" publication-type="other">
Phineas Chihota, Deputy Minister of Industry and International Trade, 'Urban dwellers not
Zimbabwean—MP', The Zimbabwean Independent, 1 July 2005.</mixed-citation>
         </ref>
         <ref id="d761e1756a1310">
            <label>103</label>
            <mixed-citation id="d761e1763" publication-type="other">
Daily News, 14 October 2002.</mixed-citation>
         </ref>
         <ref id="d761e1770a1310">
            <label>104</label>
            <mixed-citation id="d761e1777" publication-type="other">
Operation Murambatsvina took place in 2005 and left about 70 000</mixed-citation>
         </ref>
         <ref id="d761e1784a1310">
            <label>105</label>
            <mixed-citation id="d761e1791" publication-type="other">
J Muzondidya, '"Zimbabwe for Zimbabweans": invisible subject minorities and the quest for justice
and reconciliation in post-colonial Zimbabwe', in Raftopoulos &amp; Savage, Zimbabwe, pp 213-235.</mixed-citation>
         </ref>
         <ref id="d761e1801a1310">
            <label>106</label>
            <mixed-citation id="d761e1808" publication-type="other">
Ibid, p 225.</mixed-citation>
         </ref>
         <ref id="d761e1815a1310">
            <label>107</label>
            <mixed-citation id="d761e1822" publication-type="other">
Ibid, pp 225-226.</mixed-citation>
         </ref>
         <ref id="d761e1830a1310">
            <label>108</label>
            <mixed-citation id="d761e1837" publication-type="other">
Η Campbell, Reclaiming Zimbabwe: The Exhaustion of Patriarchal Model of Liberation, Trenton, NJ:
Africa World Press, 2003.</mixed-citation>
         </ref>
         <ref id="d761e1847a1310">
            <label>109</label>
            <mixed-citation id="d761e1854" publication-type="other">
Moyo, The Zimbabwe Crisis and Normalisation, p 6.</mixed-citation>
         </ref>
         <ref id="d761e1861a1310">
            <label>1</label>
            <mixed-citation id="d761e1868" publication-type="other">
Muponde, 'The worm and the hoe' p 178.</mixed-citation>
         </ref>
         <ref id="d761e1875a1310">
            <label>111</label>
            <mixed-citation id="d761e1882" publication-type="other">
Ibid.</mixed-citation>
         </ref>
         <ref id="d761e1889a1310">
            <label>112</label>
            <mixed-citation id="d761e1896" publication-type="other">
Ibid.</mixed-citation>
         </ref>
         <ref id="d761e1903a1310">
            <label>113</label>
            <mixed-citation id="d761e1910" publication-type="other">
R Gaidzanwa, 'Bourgeois theories of gender and feminism and their shortcomings with reference to
Southern African countries', in R Meena (ed), Gender in Southern Africa: Conceptual and Theoretical
Issues, Harare: SAPES, 1992;</mixed-citation>
            <mixed-citation id="d761e1922" publication-type="other">
E' Schmidt, 'Review article: For better or worse? Women and ZANLA in
Zimbabwe's liberation struggle', International Journal of Historical Studies, 34 (2), 2001, pp 416-417;</mixed-citation>
            <mixed-citation id="d761e1931" publication-type="other">
J Nhongo-Simbanegavi, For Better or Worse? Women and ZANLA in Zimbabwe's Liberation
Struggle, Harare: Weaver Press, 2000.</mixed-citation>
         </ref>
         <ref id="d761e1942a1310">
            <label>114</label>
            <mixed-citation id="d761e1949" publication-type="other">
C Enloe, Bananas, Beaches and Bases: Making Feminist Sense of International Politics, Berkeley, CA:
University of California Press, 1990, p 45.</mixed-citation>
         </ref>
         <ref id="d761e1959a1310">
            <label>115</label>
            <mixed-citation id="d761e1966" publication-type="other">
SJ Ndlovu-Gatsheni, 'Patriarchy and domestication of women in Zimbabwe: a critique of female-to-
female relations of oppression', Zambezia, XXX (ii), 2003, pp 229-245.</mixed-citation>
         </ref>
         <ref id="d761e1976a1310">
            <label>116</label>
            <mixed-citation id="d761e1983" publication-type="other">
The Herald, 16 February 2002.</mixed-citation>
         </ref>
         <ref id="d761e1990a1310">
            <label>117</label>
            <mixed-citation id="d761e1997" publication-type="other">
Sunday Mail, 16 March 2003.</mixed-citation>
         </ref>
         <ref id="d761e2004a1310">
            <label>118</label>
            <mixed-citation id="d761e2011" publication-type="other">
Tafataona Mahoso's column in the Sunday Mail, 19 July 2008.</mixed-citation>
         </ref>
         <ref id="d761e2018a1310">
            <label>119</label>
            <mixed-citation id="d761e2025" publication-type="other">
R Desai, 'The Political Economy and Cultural Politics of Nationalisms in Historical Perspective',
Third World Quaterly, 29 (3), 2008, p 663.</mixed-citation>
            <mixed-citation id="d761e2034" publication-type="other">
R Desai, 'From national bourgeoisies to rogues,
failures and bullies: 21st century imperialism and the unravelling of the Third World', Third World
Quarterly, 25 (1), 2004, pp 169-185.</mixed-citation>
         </ref>
         <ref id="d761e2048a1310">
            <label>120</label>
            <mixed-citation id="d761e2055" publication-type="other">
Desai, 'The Political Economy and Cultural Politics of Nationalisms', p 668.</mixed-citation>
            <mixed-citation id="d761e2061" publication-type="other">
R Desai,
'Nation against democracy: the rise of cultural nationalism in Asia', in F Quadir &amp; J Lele (eds),
Democracy and Civil Society in Asia, Vol 1, London: Palgrave Macmillan, 2004, pp 81-110.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
