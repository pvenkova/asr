<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">afrijinteafriins</journal-id>
         <journal-id journal-id-type="jstor">j100045</journal-id>
         <journal-title-group>
            <journal-title>Africa: Journal of the International African Institute</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Edinburgh University Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">00019720</issn>
         <issn pub-type="epub">17500184</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">3556840</article-id>
         <title-group>
            <article-title>Differences That Matter: The Struggle of the Marginalised in Somalia</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Christian</given-names>
                  <surname>Webersik</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>1</month>
            <year>2004</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">74</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id>i369286</issue-id>
         <fpage>516</fpage>
         <lpage>533</lpage>
         <page-range>516-533</page-range>
         <permissions>
            <copyright-statement>Copyright 2004 International African Institute</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/3556840"/>
         <abstract>
            <p> Somalia has been without a government for the past thirteen years. After the ousting of Siyaad Barre in 1991 observers were left with the question why a promising, even democratic, society sharing the same ethnicity, one religion, a common language and a predominantly pastoral culture was overtaken by a devastating civil war. Analysts stressed the significance of kinship and clan politics in the maintenance of sustained conflict. They argued that Somalia's state collapse must be placed in a historical context taking into consideration the cultural heritage of Somali society and the legacy of the colonial past. The purpose of the article is twofold: first, it seeks to explore an alternative explanation for the breakdown of Barre's dictatorial regime; and second, to analyse the social consequences of political and economic exclusion that followed the state collapse. The paper argues that Somalia's state failure can be explained by the unjust distribution of new sources of wealth in post-colonial Somalia. This modernisation process was accompanied by violent clashes and continued insecurity. The breakdown of the former regime did not create a representative government. Instead, faction leaders fought for political supremacy at the cost of the lives of thousands of civilians. In the absence of a functioning government that could guarantee security and protection, clan loyalties gained importance. Clan affiliation became a condition of being spared from violence. Unjust distribution of pockets of wealth, such as the high-potential agricultural land in the riverine areas in southern Somalia, led to localised clashes. It will be argued that horizontal inequalities, or inequalities between groups, are based on both material and imagined differences. Somali faction leaders use these differences instrumentally, to maintain and to exercise power. Irrespective of the existence of invisible and physical markers, it is important to understand what existing social boundaries mean to their participants. A localised clan conflict in Lower Shabelle between the Jido and the Jareer clan families illustrates the consequences of social and economic exclusion. Groups who felt excluded from economic and political life, such as the Jareer, took up arms. Violence became a means of being heard and taken seriously in the current Somali peace talks in Kenya./ La Somalie est sans gouvernement depuis treize ans. Après le renversement de Siad Barré en 1991, les observateurs se sont retrouvés devant la question de savoir comment une société prometteuse, même démocratique, partageant une même ethnicité, une seule religion, une langue commune et une culture essentiellement pastorale a pu plonger dans une guerre civile dévastatrice. Les analystes ont souligné l'importance de la politique de parenté et de clans dans le maintien du conflit. Ils ont affirmé qu'il convenait de placer la chute de l'État somalien dans un contexte historique prenant en considération l'héritage culturel de la société somalienne et l'héritage du passé colonial. L'objectif de cet article est double: premièrement, il recherche une autre explication à la chute du régime dictatorial de Barré; deuxièmement, il cherche à analyser les conséquences sociales de l'exclusion politique et économique qui a suivi l'effondrement de l'État. Selon l'article, l'échec de l'État somalien peut s'expliquer par la distribution inégale de nouvelles sources de richesse en Somalie post-coloniale. Ce processus de modernisation s'est accompagné d'affrontements violents et d'une insécurité permanente. L'effondrement de l'ancien régime n'a pas créé de gouvernement représentatif. Au lieu de cela, les chefs de factions se sont battus pour la suprématie politique, au prix de milliers de morts civiles. En l'absence de gouvernement fonctionnel capable de garantir la sécurité et la protection, les loyautés claniques ont pris plus d'importance. L'affiliation à un clan est devenue une condition pour être épargné de la violence. Une distribution inégale de poches de richesse, comme les terres à fort potentiel agricole des régions fluviales du sud de la Somalie, a entraîné des conflits localisés. L'article poursuit en précisant que les inégalités horizontales, ou inégalités entre groupes, sont basées sur des différences concrètes et imaginées. Les chefs de factions somaliens instrumentalisent ces différences pour conserver et exercer le pouvoir. Indé pendamment de l'existence de marqueurs invisibles et physiques, il est important de comprendre ce que signifient les frontières sociales existantes pour les participants. Une lutte de clan localisée dans le Bas-Shabelle entre les familles des clans Jido et Jareer illustre les conséquences de l'exclusion sociale et économique. Les groupes qui se sont sentis exclus de la vie économique et politique, comme les Jareer, ont pris les armes. La violence est devenue un moyen de se faire entendre et de se faire prendre au sérieux dans les discussions de paix pour la Somalie qui se déroulent actuellement au Kenya. </p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d1079e135a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1079e142" publication-type="other">
Menkhaus 1999: 28</mixed-citation>
            </p>
         </fn>
         <fn id="d1079e149a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1079e156" publication-type="other">
Interview conducted by the Agency for Co-operation and Research in Development in
Kurtunwarey, southern Somalia, 16, April 2001.</mixed-citation>
            </p>
         </fn>
         <fn id="d1079e166a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d1079e173" publication-type="other">
Inrerview conducted by the Agency for Co-operation and Research in Development in
Kurtunwarey, southern Somalia, 16 April 2001.</mixed-citation>
            </p>
         </fn>
         <fn id="d1079e183a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d1079e190" publication-type="other">
Author's interview in Eldoret, Kenya, 9 November 2002.</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d1079e206a1310">
            <mixed-citation id="d1079e210" publication-type="book">
Barth, F. (ed.). 1969. Ethnic Groups and Boundaries: the social organization
of cultural difference. Bergen: Universitetsforlaget; London: George Allen
&amp; Unwin.<person-group>
                  <string-name>
                     <surname>Barth</surname>
                  </string-name>
               </person-group>
               <source>Ethnic Groups and Boundaries: the social organization of cultural difference</source>
               <year>1969</year>
            </mixed-citation>
         </ref>
         <ref id="d1079e239a1310">
            <mixed-citation id="d1079e243" publication-type="journal">
Berry, S. 2002. 'Debating the Land Question in Africa', Comparative Studies in
Society and History 44: 638-668.<person-group>
                  <string-name>
                     <surname>Berry</surname>
                  </string-name>
               </person-group>
               <fpage>638</fpage>
               <volume>44</volume>
               <source>Comparative Studies in Society and History</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d1079e275a1310">
            <mixed-citation id="d1079e279" publication-type="book">
Cassanelli, L. V. 1982. The Shaping of Somali Society: reconstructing the history of
a pastoral people, 1600-1900. Philadelphia: University of Pennsylvania Press.<person-group>
                  <string-name>
                     <surname>Cassanelli</surname>
                  </string-name>
               </person-group>
               <source>The Shaping of Somali Society: reconstructing the history of a pastoral people, 1600-1900</source>
               <year>1982</year>
            </mixed-citation>
         </ref>
         <ref id="d1079e304a1310">
            <mixed-citation id="d1079e308" publication-type="book">
---1996. 'Explaining the Somali Crisis', in C. L. Besteman and L. V.
Cassanelli (eds), The Struggle for Land in Southern Somalia: the war behind the
war. Boulder CO: Westview Press; London: HAAN.<person-group>
                  <string-name>
                     <surname>Cassanelli</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Explaining the Somali Crisis</comment>
               <source>The Struggle for Land in Southern Somalia: the war behind the war</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d1079e341a1310">
            <mixed-citation id="d1079e345" publication-type="book">
Compagnon, D. 1998. 'Somali Armed Movements', in C. S. Clapham
(ed.), African Guerrillas. Oxford: James Currey; Bloomington: Indiana
University Press.<person-group>
                  <string-name>
                     <surname>Compagnon</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Somali Armed Movements</comment>
               <source>African Guerrillas</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d1079e377a1310">
            <mixed-citation id="d1079e381" publication-type="book">
De Waal, A. 1997. Famine Crimes: politics and the disaster relief industry in Africa.
African Issues. London: Africa Rights and the International African Institute;
Oxford: James Currey; Bloomington: Indiana University Press.<person-group>
                  <string-name>
                     <surname>De Waal</surname>
                  </string-name>
               </person-group>
               <source>Famine Crimes: politics and the disaster relief industry in Africa</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d1079e410a1310">
            <mixed-citation id="d1079e414" publication-type="journal">
Doornbos, M., and J. Markakis. 1994. 'Society and state in crisis: what went
wrong in Somalia?' Review of African Political Economy 21: 82-89.<person-group>
                  <string-name>
                     <surname>Doornbos</surname>
                  </string-name>
               </person-group>
               <fpage>82</fpage>
               <volume>21</volume>
               <source>Review of African Political Economy</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d1079e446a1310">
            <mixed-citation id="d1079e450" publication-type="book">
Hansch, S., S. Lillibridge, G. Egeland, C. Teller, and M. Toole. 1994. Lives
Lost, Lives Saved: excess mortality and the impact of health interventions in the
Somalia emergency. Washington DC: Refugee Policy Group.<person-group>
                  <string-name>
                     <surname>Hansch</surname>
                  </string-name>
               </person-group>
               <source>Lives Lost, Lives Saved: excess mortality and the impact of health interventions in the Somalia emergency</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d1079e479a1310">
            <mixed-citation id="d1079e483" publication-type="book">
Hogendoorn, E. J., M. A. M'Backe, and B. Mugaas. 2003. Report of the Panel of
Experts on Somalia pursuant to Security Council Resolution 1425 (2002). United
Nations Security Council.<person-group>
                  <string-name>
                     <surname>Hogendoorn</surname>
                  </string-name>
               </person-group>
               <source>Report of the Panel of Experts on Somalia pursuant to Security Council Resolution 1425 (2002)</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d1079e512a1310">
            <mixed-citation id="d1079e516" publication-type="book">
IGAD (Intergovernmental Authority on Development). 2003. Somalia National
Reconciliation Conference, Eldoret, Kenya: Committee III: Land and Property
Rights. Draft Report. Intergovernmental Authority on Development.<person-group>
                  <string-name>
                     <surname>IGAD</surname>
                  </string-name>
               </person-group>
               <source>Somalia National Reconciliation Conference, Eldoret, Kenya: Committee III: Land and Property Rights. Draft Report</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d1079e546a1310">
            <mixed-citation id="d1079e550" publication-type="book">
Laitin, D. D., and S. S. Samatar. 1987. Somalia: nation in search of a state.
Boulder CO: Westview; London: Gower.<person-group>
                  <string-name>
                     <surname>Laitin</surname>
                  </string-name>
               </person-group>
               <source>Somalia: nation in search of a state</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d1079e575a1310">
            <mixed-citation id="d1079e579" publication-type="book">
Lewis, I. M. 1961. A Pastoral Democracy: a study of pastoralism and politics among
the northern Somali of the Horn of Africa. London and New York: Oxford
University Press; New York: Africana, for the International African Institute.<person-group>
                  <string-name>
                     <surname>Lewis</surname>
                  </string-name>
               </person-group>
               <source>A Pastoral Democracy: a study of pastoralism and politics among the northern Somali of the Horn of Africa</source>
               <year>1961</year>
            </mixed-citation>
         </ref>
         <ref id="d1079e608a1310">
            <mixed-citation id="d1079e612" publication-type="book">
---1988. A Modern History of Somalia: nation and state in the Horn of Africa.
Revised edn. Boulder CO: Westview.<person-group>
                  <string-name>
                     <surname>Lewis</surname>
                  </string-name>
               </person-group>
               <source>A Modern History of Somalia: nation and state in the Horn of Africa</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d1079e637a1310">
            <mixed-citation id="d1079e641" publication-type="journal">
---2004. 'Visible and invisible differences: the Somali paradox', Africa 74 (4):
489-515.<person-group>
                  <string-name>
                     <surname>Lewis</surname>
                  </string-name>
               </person-group>
               <issue>4</issue>
               <fpage>489</fpage>
               <volume>74</volume>
               <source>Africa</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d1079e676a1310">
            <mixed-citation id="d1079e680" publication-type="journal">
Luckham, R., and D. Bekele. 1984. 'Foreign powers and militarism in the
Horn of Africa: part I', Review of African Political Economy 11: 8-20.<person-group>
                  <string-name>
                     <surname>Luckham</surname>
                  </string-name>
               </person-group>
               <fpage>8</fpage>
               <volume>11</volume>
               <source>Review of African Political Economy</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d1079e712a1310">
            <mixed-citation id="d1079e716" publication-type="book">
Marchal, R. 1997. Lower Shabelle Region, Study on Governance. Nairobi: United
Nations Development Office for Somalia.<person-group>
                  <string-name>
                     <surname>Marchal</surname>
                  </string-name>
               </person-group>
               <source>Lower Shabelle Region, Study on Governance</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d1079e742a1310">
            <mixed-citation id="d1079e746" publication-type="book">
---2002.A Survey of Mogadishu's Economy Nairobi European
Commission/Somali Unit<person-group>
                  <string-name>
                     <surname>Marchal</surname>
                  </string-name>
               </person-group>
               <source>Lower Shabelle Region, Study on Governance</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d1079e771a1310">
            <mixed-citation id="d1079e775" publication-type="book">
Marchal, R., J. A. Mubarak, M. Del Buono, and D. L. Manzolillo. 2000.
Globalization and its Impact on Somalia. United Nations Development Office
for Somalia.<person-group>
                  <string-name>
                     <surname>Marchal</surname>
                  </string-name>
               </person-group>
               <source>Globalization and its Impact on Somalia</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d1079e804a1310">
            <mixed-citation id="d1079e808" publication-type="book">
Markakis, J. 1998. Resource Conflict in the Horn of Africa. London and Thousand
Oaks CA: Sage.<person-group>
                  <string-name>
                     <surname>Markakis</surname>
                  </string-name>
               </person-group>
               <source>Resource Conflict in the Horn of Africa</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d1079e833a1310">
            <mixed-citation id="d1079e837" publication-type="other">
Menkhaus, K. 1999. 'Lower Jubba Region'. Unpublished typescript, prepared
for the Studies on Governance series sponsored by United Nations
Development Office for Somalia, Nairobi.</mixed-citation>
         </ref>
         <ref id="d1079e850a1310">
            <mixed-citation id="d1079e856" publication-type="book">
Menkhaus, K., and K. Craven. 1996. 'Land alienation and the imposition
of state farms in the Lower Jubba Valley', in C. L. Besteman and L. V.
Cassanelli (eds), The Struggle for Land in Southern Somalia: the war behind the
war. Boulder CO: Westview Press; London: HAAN.<person-group>
                  <string-name>
                     <surname>Menkhaus</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Land alienation and the imposition of state farms in the Lower Jubba Valley</comment>
               <source>The Struggle for Land in Southern Somalia: the war behind the war</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d1079e891a1310">
            <mixed-citation id="d1079e895" publication-type="journal">
Mukhtar, M. H. 1996. 'The plight of the agro-pastoral society of Somalia',
Review of African Political Economy 23: 543-553.<person-group>
                  <string-name>
                     <surname>Mukhtar</surname>
                  </string-name>
               </person-group>
               <fpage>543</fpage>
               <volume>23</volume>
               <source>Review of African Political Economy</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d1079e928a1310">
            <mixed-citation id="d1079e932" publication-type="book">
Omar, M. O. 1982. 'Probleme der ländlichen Entwicklung in der
Demokratischen Republik Somalia 1974-80'. Doktorarbeit. Freie
Universitdit Berlin.<person-group>
                  <string-name>
                     <surname>Omar</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Probleme der ländlichen Entwicklung in der Demokratischen Republik Somalia 1974-80</comment>
               <source>Doktorarbeit</source>
               <year>1982</year>
            </mixed-citation>
         </ref>
         <ref id="d1079e964a1310">
            <mixed-citation id="d1079e968" publication-type="journal">
Prendergast, J. 1994. 'The forgotten agenda in Somalia', Review of African
Political Economy 21: 66-71.<person-group>
                  <string-name>
                     <surname>Prendergast</surname>
                  </string-name>
               </person-group>
               <fpage>66</fpage>
               <volume>21</volume>
               <source>Review of African Political Economy</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d1079e1000a1310">
            <mixed-citation id="d1079e1004" publication-type="book">
Reno, W. 2003. Somalia and Survival in the Shadow of the Global Economy.
Working Paper No. 100. Oxford: Queen Elizabeth House, University
of Oxford.<person-group>
                  <string-name>
                     <surname>Reno</surname>
                  </string-name>
               </person-group>
               <source>Somalia and Survival in the Shadow of the Global Economy</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d1079e1033a1310">
            <mixed-citation id="d1079e1037" publication-type="other">
Salim Said, Z. 2002. 'State Decomposition and Clan Identities in Somalia'.
M.Phil. thesis. University of Oxford.</mixed-citation>
         </ref>
         <ref id="d1079e1047a1310">
            <mixed-citation id="d1079e1051" publication-type="journal">
Samatar, A. 1988. 'The state, agrarian change and crisis of hegemony in
Somalia', Review of African Political Economy 43: 26-41.<person-group>
                  <string-name>
                     <surname>Samatar</surname>
                  </string-name>
               </person-group>
               <fpage>26</fpage>
               <volume>43</volume>
               <source>Review of African Political Economy</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d1079e1083a1310">
            <mixed-citation id="d1079e1087" publication-type="journal">
Samatar, A. I. 1992. 'Social classes and economic restructuring in pastoral
Africa: Somali notes', African Studies Review 35: 101-127.<object-id pub-id-type="doi">10.2307/524447</object-id>
               <fpage>101</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1079e1104a1310">
            <mixed-citation id="d1079e1108" publication-type="book">
---1999. An African miracle: state and class leadership and colonial legacy in
Botswana development. Portsmouth NH: Heinemann.<person-group>
                  <string-name>
                     <surname>Samatar</surname>
                  </string-name>
               </person-group>
               <source>An African miracle: state and class leadership and colonial legacy in Botswana development</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d1079e1133a1310">
            <mixed-citation id="d1079e1137" publication-type="book">
Samatar, S. S. 1991. Somalia: a nation in turmoil. London: Minority
Rights Group.<person-group>
                  <string-name>
                     <surname>Samatar</surname>
                  </string-name>
               </person-group>
               <source>Somalia: a nation in turmoil</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d1079e1162a1310">
            <mixed-citation id="d1079e1166" publication-type="journal">
Turton, D. 1997. 'War and ethnicity: global connections and local violence in
North East Africa and Former Yugoslavia', Oxford Development Studies 25:
77-94.<person-group>
                  <string-name>
                     <surname>Turton</surname>
                  </string-name>
               </person-group>
               <fpage>77</fpage>
               <volume>25</volume>
               <source>Oxford Development Studies</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d1079e1201a1310">
            <mixed-citation id="d1079e1205" publication-type="book">
UNDP (United Nations Development Programme). 1998. Human Development
Report, Somalia 1998. United Nations Development Programme Somalia
Country Office.<person-group>
                  <string-name>
                     <surname>UNDP</surname>
                  </string-name>
               </person-group>
               <source>Human Development Report, Somalia 1998</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d1079e1234a1310">
            <mixed-citation id="d1079e1238" publication-type="book">
---2001. Human Development Report, Somalia 2001. United Nations
Development Programme Somalia Country Office.<person-group>
                  <string-name>
                     <surname>UNDP</surname>
                  </string-name>
               </person-group>
               <source>Human Development Report, Somalia 2001</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d1079e1263a1310">
            <mixed-citation id="d1079e1267" publication-type="book">
Van Lehman, D., and O. Eno. 2002. The Somali Bantu: their history and culture.
Washington DC: Center for Applied Linguistics.<person-group>
                  <string-name>
                     <surname>Van Lehman</surname>
                  </string-name>
               </person-group>
               <source>The Somali Bantu: their history and culture</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
