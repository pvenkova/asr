<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt8f9fmv</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="doi">10.14321/j.ctt1qd8zk3</book-id>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
         <subject>History</subject>
         <subject>Sociology</subject>
      </subj-group>
      <book-title-group>
         <book-title>A Motorcycle on Hell Run</book-title>
         <subtitle>Tanzania, Black Power, and the Uncertain Future of Pan-Africanism, 1964–1974</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Markle</surname>
               <given-names>Seth M.</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>01</day>
         <month>08</month>
         <year>2017</year>
      </pub-date>
      <isbn content-type="ppub">9781611862522</isbn>
      <isbn content-type="epub">9781609175344</isbn>
      <publisher>
         <publisher-name>Michigan State University Press</publisher-name>
         <publisher-loc>East Lansing</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2017</copyright-year>
         <copyright-holder>Seth M. Markle</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.14321/j.ctt1qd8zk3"/>
      <abstract abstract-type="short">
         <p>Between 1964 and 1974 Tanzania came to be regarded as a model nation and a leading frontline state in the struggle for African liberation on the continent and beyond. During this time, a number of African American and Caribbean nationalists, leftists, and pan-Africanists traveled to and settled in Tanzania to join the country that many believed to be leading Africa's liberation struggle. This historical study examines the political landscape of that crucial moment when African American, Caribbean, and Tanzanian histories overlapped, shedding light on the challenges of creating a new nation and the nature of African American and Caribbean participation in Tanzania's nationalist project. In examining the pragmatic partnerships and exchanges between socialist Tanzania and activists and organizations associated with the Black Power movements in the United States and the Caribbean, this study argues that the Tanzanian one-party government actively engaged with the diaspora and sought to utilize its political, cultural, labor, and intellectual capital to further its national building agenda, but on its own terms, creating tension within the pan-Africanism movement. An excellent resource for academics and nonacademics alike, this work is the first of its kind, revealing the significance of the radical political and social movements of Tanzania and what it means for us today.</p>
      </abstract>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.14321/j.ctt1qd8zk3.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.14321/j.ctt1qd8zk3.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.14321/j.ctt1qd8zk3.3</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.14321/j.ctt1qd8zk3.4</book-part-id>
                  <title-group>
                     <title>Introduction</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>In an article that appeared in his independently published journal,<italic>The Crusader</italic>, Robert F. Williams, an African American political exile, wrote about his unforgettable 1,5 00-mile motorcycle journey from Dar es Salaam, Tanzania, to the border of Zambia and back in May 1968. Titled “African Safari: Hells Run on a Motorcycle,” a reference to the country’s most notorious highway, Williams reflects on his “grueling act of daring.” Through daylight and darkness he rode past herds of elephants, zebras, and giraffes, and the corpses of over “50 heavy trucks and trailers overturned” along the side of the road. He also visited</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.14321/j.ctt1qd8zk3.5</book-part-id>
                  <title-group>
                     <title>Abbreviations</title>
                  </title-group>
                  <fpage>15</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.14321/j.ctt1qd8zk3.6</book-part-id>
                  <title-group>
                     <label>Chapter 1</label>
                     <title>Malcolm X, A. M. Babu, and the Seeds of Solidarity</title>
                  </title-group>
                  <fpage>19</fpage>
                  <abstract>
                     <p>The speed with which the dismantling of colonialism occurred in Africa was cause for celebration among African Americans. Between 1957 and 1963, approximately twenty-six independent nations in Africa came into being.¹ For African Americans eager to identify and connect with African states and their leaders sympathetic to and supportive of their struggle for racial justice, Tanzanian President Nyerere’s pro–civil rights and pan-Africanist positions put forward in the late 1950s and early 1960s left a favorable impression. Although he may not have received the same amount of veneration and media coverage as Kwame Nkrumah in Ghana, Patrice Lumumba in the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.14321/j.ctt1qd8zk3.7</book-part-id>
                  <title-group>
                     <label>Chapter 2</label>
                     <title>Growth and Conflict in SNCC-Tanzania Relations</title>
                  </title-group>
                  <fpage>43</fpage>
                  <abstract>
                     <p>Malcolm X’s assassination left many African Americans shaken, confused, and angry. Among those significantly wounded by his death were the young black organizers of the Student Nonviolent Coordinating Committee (sncc, pronounced “snick”). Founded in 1960 after students in Greensboro, North Carolina, initiated a nationwide sit-in movement against racial segregation, SNCC emerged to channel that energy, spirit, and courage exhibited by the nation’s young people into grassroots community organizing and nonviolent direct action campaigns in the southern United States. Between 1960 and 1965, sncc activists operated as “the shock troops” of the civil rights struggle, a multiracial organization largely committed to</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.14321/j.ctt1qd8zk3.8</book-part-id>
                  <title-group>
                     <label>Chapter 3</label>
                     <title>Walter Rodney, African Students, and the Struggle to Define University Education</title>
                  </title-group>
                  <fpage>75</fpage>
                  <abstract>
                     <p>Malcolm X and Carmichael’s visits to Dar es Salaam in 1964 and 1967, respectively, illustrated how international travel became an important practice of African solidarity work and represented a breakthrough in Black Power–Tanzania relations. For political activists in the African Diaspora, however, it became just as important to show solidarity in action through active participation in Tanzania’s nationalist project by living and working in the country. In the period between 1968 and 1974, Tanzania became the epicenter of the pan-Africanism movement in large part due to the passing of the Arusha Declaration, which shaped Black Power activists’ conceptions of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.14321/j.ctt1qd8zk3.9</book-part-id>
                  <title-group>
                     <label>Chapter 4</label>
                     <title>The Drum and Spear Press and the Cultural Politics of Book Publishing</title>
                  </title-group>
                  <fpage>105</fpage>
                  <abstract>
                     <p>In the context of the Black Power movement in the United States, the publication of<italic>How Europe Underdeveloped Africa</italic>positioned Walter Rodney as one of the most important intellectuals of the era. For anyone interested in gaining a historical understanding of slavery and colonialism in Africa and the challenges that postcolonial states faced in ridding their nations of these legacies, Rodney’s text became required reading and a part of a canon of revolutionary literature joining the likes of Malcolm X’s<italic>The Autobiography of Malcolm X</italic>, Frantz Fanon’s<italic>Wretched of the Earth</italic>, and Mao Tse Tung’s<italic>Quotations of Mao Tse Tung</italic>,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.14321/j.ctt1qd8zk3.10</book-part-id>
                  <title-group>
                     <title>[Illustrations]</title>
                  </title-group>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.14321/j.ctt1qd8zk3.11</book-part-id>
                  <title-group>
                     <label>Chapter 5</label>
                     <title>Convergence and Rejection at the Sixth Pan-African Congress</title>
                  </title-group>
                  <fpage>141</fpage>
                  <abstract>
                     <p>Through much of the late 1960s and early 1970s, the Black Power movement continued to put on display its internationalist sensibilities. Though various efforts were made to boost black popular and political support for African liberation, activists identified the lack of functional unity between Africa and its diaspora as a problem of increasing importance. In a context in which regions in Africa were still under colonial rule and white minority control, not to mention independent nation-states having little to show for their ambitious efforts at nation-building, Black Power activists feared that the commonalities between these various black struggles were being</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.14321/j.ctt1qd8zk3.12</book-part-id>
                  <title-group>
                     <title>Conclusion</title>
                  </title-group>
                  <fpage>177</fpage>
                  <abstract>
                     <p>By the mid-1980s, the enthusiasm for pan-Africanist activity surrounding Tanzania’s nationalist project had considerably waned. The Pan-African Skills Project continued to send African American skilled technicians to Africa until 1980, but the project’s expansion to other nations and its attempt to implement other projects also hindered the progress and growth of the skilled-labor project in Tanzania.¹ President Nyerere, perhaps sensing the shift in mood among Tanzanian citizens, decided against running for reelection in Tanzania’s general elections slated for October 27, 1985. Approximately eighteen years had passed since issuing the Arusha Declaration that sent the nation and its peoples on a</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.14321/j.ctt1qd8zk3.13</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>187</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.14321/j.ctt1qd8zk3.14</book-part-id>
                  <title-group>
                     <title>Bibliography</title>
                  </title-group>
                  <fpage>223</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.14321/j.ctt1qd8zk3.15</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>255</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
