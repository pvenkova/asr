<?xml version="1.0" encoding="UTF-8"?>
<article article-type="research-article" dtd-version="1.0" xml:lang="eng">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="publisher-id">afrdevafrdev</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50014718</journal-id>
         <journal-title-group>
            <journal-title>Africa Development / Afrique et Développement</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>CODESRIA</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">08503907</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">90013900</article-id>
         <title-group>
            <article-title>Challenges of the Devolved Health Sector in Kenya</article-title>
            <subtitle>Teething Problems or Systemic Contradictions?</subtitle>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Kimathi</surname>
                  <given-names>Leah</given-names>
               </string-name>
               <xref ref-type="aff" rid="a1">
                  <sup>*</sup>
               </xref>
            </contrib>
            <aff id="a1">
               <sup>*</sup>Jomo Kenyatta University of Agriculture and Technology (JKUAT), Kenya.</aff>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>1</month>
            <year>2017</year>
            <string-date>2017</string-date>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink">42</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">e90013894</issue-id>
         <fpage>55</fpage>
         <lpage>77</lpage>
         <permissions>
            <copyright-statement>© Conseil pour le développement de la recherche en sciences sociales en Afrique, 2017</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/90013900"/>
         <abstract>
            <label>Abstract</label>
            <p>The promulgation of the new constitution in Kenya in August 2010 effectively ushered in devolution as the latest and highest form of decentralization in Kenya. The health sector was the largest service sector to be devolved under this new governance arrangement. The rationale for devolving the sector was to allow the county governments to design innovative models and interventions that suited the unique health needs in their contexts, encourage effective citizen participation and make autonomous and quick decisions on resource mobilization and management possible issues. However, the sector in nearly all counties is currently bedevilled with monumental challenges ranging from capacity gaps, human resource deficiency, lack of critical legal and institutional infrastructure, rampant corruption and a conflictual relationship with the national government. The net effect of these challenges is the stagnation of healthcare and even a reversal of some gains according to health indicators. No doubt what is needed to guarantee an all-inclusive rights-based approach to health service delivery is its proper institutionalization to ensure good governance and effective community participation. This must however be accompanied by wider governance reforms as envisaged in the new constitution for the sustainability of Healthcare Reforms.</p>
         </abstract>
         <trans-abstract xml:lang="fre">
            <label>Résumé</label>
            <p>Au Kenya, la promulgation de la nouvelle constitution en août 2010 a véritablement introduit la dévolution en tant que forme de décentralisation la plus récente et la plus élevée. Le secteur de la santé était le plus important secteur des services à compétences dévolues dans ce nouveau système de gouvernance. La logique de cette dévolution était de permettre aux gouvernements de comtés de concevoir des modèles et des interventions novateurs adaptés aux besoins uniques en matière de santé dans leurs contextes, d’encourager la participation efficace des citoyens et de prendre des décisions autonomes et rapides concernant la mobilisation des ressources et la gestion des questions qui se posent. Cependant, dans presque tous les comtés, le secteur est actuellement miné par des défis colossaux qui sont notamment les lacunes en matière de capacité, l’insuffisance des ressources humaines, le manque d’infrastructures légales et institutionnelles cruciales, la corruption rampante et une relation conflictuelle avec le gouvernement national. Le résultat final de ces défis est la stagnation des soins de santé, voire un renversement de certains gains d’après les indicateurs de santé. Il ne fait aucun doute que ce qu’il faut pour garantir une approche des prestations de services de santé inclusive et fondée sur les droits, c’est qu’elle soit elle-même institutionnalisée pour assurer une bonne gouvernance et une participation communautaire efficace. Toutefois, cela doit s’accompagner de plus vastes réformes de la gouvernance, tel que prévu dans la nouvelle constitution pour la durabilité des réformes des systèmes de santé.</p>
         </trans-abstract>
         <kwd-group>
            <label>Key Words:</label>
            <kwd>Devolution</kwd>
            <x>,</x>
            <kwd>Healthcare Delivery</kwd>
            <x>,</x>
            <kwd>Healthcare Financing</kwd>
            <x>,</x>
            <kwd>Health Workforce</kwd>
            <x>,</x>
            <kwd>health governance</kwd>
            <x>.</x>
         </kwd-group>
         <kwd-group>
            <label>Mots clés :</label>
            <kwd>Dévolution</kwd>
            <x>,</x>
            <kwd>prestations de services de santé</kwd>
            <x>,</x>
            <kwd>financement des soins de santé</kwd>
            <x>,</x>
            <kwd>personnels de santé</kwd>
            <x>,</x>
            <kwd>gouvernance de la santé</kwd>
            <x>.</x>
         </kwd-group>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="unparsed-citations">
         <title>References</title>
         <ref id="ref1">
            <mixed-citation publication-type="other">Center for Health Solutions in Kenya, 2013, CHS Forum on Health System Strengthening in the Devolved System of Government, https://www.chskenya.org/wp-content/uploads/2014/04/chs-forum-on-hss-in-devolution-report.pdf, accessed 7 September 2015.</mixed-citation>
         </ref>
         <ref id="ref2">
            <mixed-citation publication-type="other">Commission on Revenue Allocation, 2014, Revenue Allocation Formula, http//www.crakenya.org/information /revenue-allocation-formula/, accessed 10 September 2015.</mixed-citation>
         </ref>
         <ref id="ref3">
            <mixed-citation publication-type="other">Dubusho, T. et al., 2009, Ethiopia: Improving Health Service Delivery, Washington DC: World Bank and the International Bank for Reconstruction and Development.</mixed-citation>
         </ref>
         <ref id="ref4">
            <mixed-citation publication-type="other">El-Saharty, S. et al., 2009, Ethiopia: Making Health Service Delivery More Effective, Washington DC: The World Bank.</mixed-citation>
         </ref>
         <ref id="ref5">
            <mixed-citation publication-type="other">Esidene, C., 2011, Local Government Administration in Kenya: Problems and Prospects, http://www.researchgate.net/profile/Canice_Erunke/publication/233762485_local_government_administration_in_kenya/links/09e4150b52114015ef000000?origin=publication_detail, accessed 6 September 2015.</mixed-citation>
         </ref>
         <ref id="ref6">
            <mixed-citation publication-type="other">Government of Kenya, 2010, Constitution of Kenya, Nairobi.</mixed-citation>
         </ref>
         <ref id="ref7">
            <mixed-citation publication-type="other">Government of Kenya, 2013, Kenya Health Policy 2012-2030, Nairobi: Ministry of Medical Services and Ministry of Public Health and Sanitation.</mixed-citation>
         </ref>
         <ref id="ref8">
            <mixed-citation publication-type="other">Government of Kenya, 2014, Options for Kenya’s Health Financing Systems: A Policy Brief, Nairobi: Ministry of Health.</mixed-citation>
         </ref>
         <ref id="ref9">
            <mixed-citation publication-type="other">Hawkins, L., 2009, Devolution of Health Centers and Hospital Autonomy in Thailand: a rapid assessment, Washington DC: World Bank.</mixed-citation>
         </ref>
         <ref id="ref10">
            <mixed-citation publication-type="other">International Rescue Committee, 2015, Every Voice Counts Program Report, Nairobi: International Rescue Committee.</mixed-citation>
         </ref>
         <ref id="ref11">
            <mixed-citation publication-type="other">Kariuki, P., 2014, Corruption Devolved to the Counties, Daily Nation, 4 April, Nairobi.</mixed-citation>
         </ref>
         <ref id="ref12">
            <mixed-citation publication-type="other">Kibui et al., 2015, ‘Health Policies in Kenya and the New Constitution for Vision 2030’, International Journal of Scientific Research and Innovative Technology 2(1):127.</mixed-citation>
         </ref>
         <ref id="ref13">
            <mixed-citation publication-type="other">Kinuthia, M., 2016, Challenges Facing Devolution in Kenya Nairobi: Kenyatta University.</mixed-citation>
         </ref>
         <ref id="ref14">
            <mixed-citation publication-type="other">KPMG Africa, 2014, Devolution of Healthcare Services in Kenya, https://www.kpmg.com/Africa/en/IssuesAndInsights/Articles-Publications/Documents/Devolution%20of%20HC%20Services%20in%20Kenya.pdf, accessed 8 September 2015.</mixed-citation>
         </ref>
         <ref id="ref15">
            <mixed-citation publication-type="other">Magokha, T., 2015, ‘Blame Game as Kenya’s Health Crisis Hurts’, Standard, Nairobi, http://www.standardmedia.co.ke/kenyaat50/article/2000174647/blame-game-as-kenya-s-health-crisis-hurts?pageNo=2, accessed 14 September 2015.</mixed-citation>
         </ref>
         <ref id="ref16">
            <mixed-citation publication-type="other">Ministry of Devolution and National Planning, 2015, Improving Healthcare Delivery in Kenya, Nairobi: Ministry of Devolution and National Planning.</mixed-citation>
         </ref>
         <ref id="ref17">
            <mixed-citation publication-type="other">Ministry of Health, 2006, Taking the Kenya Essential Package for Health to the Community: A strategy for the Delivery of Level One Services, Nairobi: Ministry of Health.</mixed-citation>
         </ref>
         <ref id="ref18">
            <mixed-citation publication-type="other">Ministry of Health, 2013, Kenya Service Availability and Readiness Assessment Mapping, Nairobi: Ministry of Health.</mixed-citation>
         </ref>
         <ref id="ref19">
            <mixed-citation publication-type="other">Ministry of Health, 2014, Kenya Health Policy 2012–2030, Nairobi: Ministry of Health.</mixed-citation>
         </ref>
         <ref id="ref20">
            <mixed-citation publication-type="other">Muoko, B. and Baker C., 2014, Decentralization and Rural Service Delivery in Uganda, Kampala: International Food Policy Research Institute.</mixed-citation>
         </ref>
         <ref id="ref21">
            <mixed-citation publication-type="other">Mutai, A., 2015, Devolution on Trial in Kenya: Case Study of Isiolo County, http://somalianewsroom.com/devolution-on-trial-in-kenya-case-study-on-isiolo-county/, accessed 2 September 2015.</mixed-citation>
         </ref>
         <ref id="ref22">
            <mixed-citation publication-type="other">Mwamuye, K., and Nyamu, H., 2014, ‘Devolution of Healthcare System in Kenya: A Strategic Approach and its Implementation in Mombasa County, Kenya’, International Journal of Advanced Research 2 (4): 263–8.</mixed-citation>
         </ref>
         <ref id="ref23">
            <mixed-citation publication-type="other">Mwangi, C., 2013, ‘Accessibility to the Kenyan Healthcare System: Barriers to Accessing Proper Healthcare’, unpublished PhD thesis, Arcada University.</mixed-citation>
         </ref>
         <ref id="ref24">
            <mixed-citation publication-type="other">Olugo, S., 2015, ‘Counties Urged to Embrace Partnerships to Improve Healthcare Services’, Standard, Nairobi, http://www.standardmedia.co.ke/sports/article/2000163505/counties-urged-to-embrace-partnership-to-improve-healthcare-services, accessed 18 September 2015.</mixed-citation>
         </ref>
         <ref id="ref25">
            <mixed-citation publication-type="other">Patrick, I., 2013, Relooking at Healthcare in Kenya in the Age of Devolution, University of KwaZulu-Natal.</mixed-citation>
         </ref>
         <ref id="ref26">
            <mixed-citation publication-type="other">Pongpisut, J., 2012, ‘A Decade of Health-Care Decentralization in Thailand: What Lessons can be drawn?’, WHO South-East Asia Journal of Public Health 1 (3): 347–56.</mixed-citation>
         </ref>
         <ref id="ref27">
            <mixed-citation publication-type="other">World Bank, 2014, Delivering Primary Health Services in Devolved Health Systems of Kenya: Challenges and Prospects, http://documents.worldbank.org/curated/en/2014/09/20351566/delivering-primary-health-services-devolved-health-systems-kenya-challenges-opportunities, accessed 7 September 2015.</mixed-citation>
         </ref>
         <ref id="ref28">
            <mixed-citation publication-type="other">World Health Organization, 1990, Health Systems Decentralization, Geneva: World Health Organization.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
