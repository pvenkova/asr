<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">estuasiaafri</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000544</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Estudios de Asia y Africa</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>El Colegio de Mexico</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">01850164</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">40312657</article-id>
         <title-group>
            <article-title>La administración para el desarrollo en África: reflexiones sobre dos décadas de experiencia</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jeggan C.</given-names>
                  <surname>Senghor</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Patrick</given-names>
                  <surname>Goldsmith</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>10</month>
            <year>1982</year>
      
            <day>1</day>
            <month>12</month>
            <year>1982</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">17</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4 (54)</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40012848</issue-id>
         <fpage>628</fpage>
         <lpage>683</lpage>
         <permissions>
            <copyright-statement>Copyright El Colegio de Mexico</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/40312657"/>
         <abstract>
            <p>Soon after formal political independence in most African countries —mainly during the early sixties— there was a profund faith in the ability of government bureaucracies to undertake programmes of economic, social and cultural development. It was argued that with proper planning and proper choice of developmental priorities, the scarce resources available to developing African countries would be adequately used for development. In political science, these ideas were expressed in a new discipline calle "development administration". When, after a few years of experience, objectives were not being realized and governmental agencies were not behaving rationally, students of development administration started looking at the environments in which their models were meant to operate. It was found that politics, class struggles, class interests, ideologies, structural constraints to the economy and many other factors all impinged on the ability of bureaucracies to achieve their objectives. But the critique of development administration remained schematic and haphazard. The idea of planning, for example, and whether it was possible for the neo-colonial states in Africa to plan for development, was not seriously looked into by students of development administration. Instead, a lot of emphasis was put on plan implementation and why implementation apparently fails even in cases where plans, on paper, are reasonably sound. This essay seeks to critically review the last two decades of development administration in Africa. With the profound poverty and underdevelopmentof African countries continuing to get worse, it is necessary to find out whether the public institutions which currently exist are capable of doing anything positive towards developing Africa in the future. We will seek to identify the problems and difficulties that have faced these institutions; we shall discuss the environment in which these institutions have operated, looking into specific issues as resource availability; resource use and planning; attempts at administrative reforms and their outcomes; and the possibilities, if any, of creating public administration institutions that can achieve developmental objectives.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>spa</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d71e156a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d71e163" publication-type="other">
Coralie Bryant y Louise White, Managing Development in the Third
World, Colorado, Westview Press, 1982, cap. 1.</mixed-citation>
            </p>
         </fn>
         <fn id="d71e173a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d71e180" publication-type="other">
Special Measures for the Social and Economic Development of Africa in the
1980s, Informe del Secretano General, A/36/513, 24 de septiembre de 1981, p. 2.</mixed-citation>
            </p>
         </fn>
         <fn id="d71e190a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d71e197" publication-type="other">
Los criterios son presentados en Identification of the Least Developed among the
Developing Countries, Documento del Secretariado, E/AC 54/1982/3, 12 de abril de
1982, p. 3.</mixed-citation>
            </p>
         </fn>
         <fn id="d71e210a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d71e217" publication-type="other">
Comisión Económica para Africa, The África Region: Some Key Development
Issues in the Context of the African Strategy and Plan of Action, E/AC54/L 103, 23
de enero de 1981, p. 3.</mixed-citation>
            </p>
         </fn>
         <fn id="d71e231a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d71e238" publication-type="other">
Ibid, p. 13</mixed-citation>
            </p>
         </fn>
         <fn id="d71e245a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d71e252" publication-type="other">
Ibid., p. 3.</mixed-citation>
            </p>
         </fn>
         <fn id="d71e259a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d71e266" publication-type="other">
Ibid., p. 13.</mixed-citation>
            </p>
         </fn>
         <fn id="d71e273a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d71e280" publication-type="other">
Ibid., p. 16.</mixed-citation>
            </p>
         </fn>
         <fn id="d71e287a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d71e294" publication-type="other">
A. Adedeji, Africa: The Crisis of Development and the Challenge of a New
Economic Order, Addis Ababa, Economic Commission for Africa, 1977.</mixed-citation>
            </p>
         </fn>
         <fn id="d71e304a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d71e311" publication-type="other">
The African Region, op. cit., p. 4.</mixed-citation>
            </p>
         </fn>
         <fn id="d71e319a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d71e326" publication-type="other">
Ibid., p. 4.</mixed-citation>
            </p>
         </fn>
         <fn id="d71e333a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d71e340" publication-type="other">
Ibid., p. 26.</mixed-citation>
            </p>
         </fn>
         <fn id="d71e347a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d71e354" publication-type="other">
Naciones Unidas, Survey of Economic and Social Conditions in Africa, 1977-1978
(Sumario), E/1979/70, 23 de mayo de 1979, p. 14.</mixed-citation>
            </p>
         </fn>
         <fn id="d71e364a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d71e371" publication-type="other">
Ibid., p. 14.</mixed-citation>
            </p>
         </fn>
         <fn id="d71e378a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d71e385" publication-type="other">
Ibid., p. 14.</mixed-citation>
            </p>
         </fn>
         <fn id="d71e392a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d71e399" publication-type="other">
Warren F., Illman y Norman T. Uphoff, The Political Economy of Change,
University of California Press, 1971, p. 244.</mixed-citation>
            </p>
         </fn>
         <fn id="d71e410a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d71e417" publication-type="other">
Loc. cit.</mixed-citation>
            </p>
         </fn>
         <fn id="d71e424a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d71e431" publication-type="other">
Milton Esman, "Development Administration and Constituency
Organizations", en Public Administration Review, vol. 38, marzo-abril de 1978, pp.
166-172.</mixed-citation>
            </p>
         </fn>
         <fn id="d71e444a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d71e451" publication-type="other">
Gobierno de Sierra Leona, National Development Plan of Sierra Leone, 1974/5
1978/9,</mixed-citation>
            </p>
         </fn>
         <fn id="d71e461a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d71e468" publication-type="other">
R. Merton, "The Use of State Power to Overcome Underdevelopment", en
Journal of Modern African Studies, vol. 18, No. 2, 1980, p. 319.</mixed-citation>
            </p>
         </fn>
         <fn id="d71e478a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d71e485" publication-type="other">
Victor Thompson, "Administrative Objectives for Development", en Adminis-
trative Science Quaterly, junio de 1964, p. 102.</mixed-citation>
            </p>
         </fn>
         <fn id="d71e495a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d71e502" publication-type="other">
Nigeria, Public Service Review Commission, Ministerio de
Información, Lagos, septiembre de 1974, pp. 14-15.</mixed-citation>
            </p>
         </fn>
         <fn id="d71e513a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d71e520" publication-type="other">
Banco Mundial, Accelerateci Development in Sub -S abaran Africa, Washington,
D. C, p. 40.</mixed-citation>
            </p>
         </fn>
         <fn id="d71e530a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d71e537" publication-type="other">
William Siffin, "Two Decades of Public Administration in Developing Coun-
tries", en Public Administration Review, enero-febrero de 1975, p. 66.</mixed-citation>
            </p>
         </fn>
         <fn id="d71e547a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d71e554" publication-type="other">
ibid., p. 68.</mixed-citation>
            </p>
         </fn>
         <fn id="d71e561a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d71e568" publication-type="other">
Noami y Gerald Caiden, "Administrative Corruption", en Public Administration
Review, vol. 37, No. 3, mayo-junio de 1977, p. 306.</mixed-citation>
            </p>
         </fn>
         <fn id="d71e578a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d71e585" publication-type="other">
D. J. Gould, Bureaucratic Corruption and Underdevelopment in the Third World:
The Case of Zaire, New York, Pergamon Press, 1980.</mixed-citation>
            </p>
         </fn>
         <fn id="d71e595a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d71e602" publication-type="other">
Bernard Schaffer, "The Deadlock in Development Administration", en Colin
Leys, ed., Politics and Change in Developing Countries, Cambridge, Cambridge
University Press, 1969, p. 184.</mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
