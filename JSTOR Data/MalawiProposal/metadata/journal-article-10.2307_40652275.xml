<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">healtranrev</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000806</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Health Transition Review</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Health Transition Centre, National Centre for Epidemiology and Population Health, The Australian National University</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">10364005</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">40652275</article-id>
         <title-group>
            <article-title>The effects of water supply on infant and childhood mortality: a review of historical evidence</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Frans</given-names>
                  <surname>van Poppel</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Cor</given-names>
                  <surname>van der Heijden</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>10</month>
            <year>1997</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">7</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40028211</issue-id>
         <fpage>113</fpage>
         <lpage>148</lpage>
         <permissions>
            <copyright-statement>© 1997 Health Transition Centre</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/40652275"/>
         <abstract>
            <p>The provision of clean water is mentioned as an important factor in many studies dealing with the decline of mortality in Europe during the late nineteenth and early twentieth centuries. In developing countries too, improved water supply is assumed to have a strong impact on mortality. When studying the effect of water supply on public health, researchers are confronted with many methodological problems. Most of these also apply to historical studies of the subject We review the evidence from this historical research, taking into account the methodological problems observed in contemporary impact evaluation studies, and we use more refined data from the Dutch city of Tilburg, enabling us to overcome many of these shortcomings. Finally, we discuss some factors which may explain why we failed to discover an effect of the availability of piped water on the level of childhood mortality.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d911e208a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d911e215" publication-type="other">
International Child Development Centre,
Florence, 12-14 December 1996,</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d911e224" publication-type="other">
Annales de démographie historique, 1997, 1, Paris: Odile Jacob, pp. 157-204.</mixed-citation>
            </p>
         </fn>
         <fn id="d911e231a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d911e238" publication-type="other">
(Preston and Van der Walle 1978).</mixed-citation>
            </p>
         </fn>
         <fn id="d911e245a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d911e252" publication-type="other">
Rohwer, G. (1994)</mixed-citation>
            </p>
         </fn>
         <fn id="d911e259a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d911e266" publication-type="other">
Municipal Archive Tilburg. Archives of the Tilburg Water Company, Inv. No. 129, Incoming
correspondence, section Water examinations 1896-1960.</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d911e285a1310">
            <mixed-citation id="d911e289" publication-type="other">
Bernhardt, E. 1992. Crowding and child survival in Stockholm 1895-1920. Paper presented at IUSSP
Seminar on Child and Infant Mortality in the Past, Université de Montréal, Montréal, 7-9
October.</mixed-citation>
         </ref>
         <ref id="d911e302a1310">
            <mixed-citation id="d911e306" publication-type="other">
Blossfeld, H.-P. and G. Rohwer. 1995. Techniques of Event History Modeling. New Approaches to
Causal Analysis. Mahwah NJ: Lawrence Erlbaum Associates.</mixed-citation>
         </ref>
         <ref id="d911e316a1310">
            <mixed-citation id="d911e320" publication-type="other">
Blum, D. and R.G. Feachem. 1983. Measuring the impact of water supply and sanitation investments
on diarrhoeal diseases: problems of methodology. International Journal of Epidemiology 12:357-
365.</mixed-citation>
         </ref>
         <ref id="d911e333a1310">
            <mixed-citation id="d911e337" publication-type="other">
Bradley, D. 1977. Health aspects of water supplies in tropical countries. Pp. 1-17 in Water, Wastes
and Health in Hot Climates, ed. R. Feachem, M. McGarry and D. Mara. Chichester: John Wiley.</mixed-citation>
         </ref>
         <ref id="d911e348a1310">
            <mixed-citation id="d911e352" publication-type="other">
Bradley, D.J., C. Stephens, T. Harpham and S. Cairncross. 1992. A Review of Environmental Health
Impacts in Developing Country Cities. Washington DC: World Bank.</mixed-citation>
         </ref>
         <ref id="d911e362a1310">
            <mixed-citation id="d911e366" publication-type="other">
Breschi, M. and M. Livi-Bacci. 1992. Month of birth as a factor of children's survival. Paper
presented at IUSSP Seminar on Child and Infant Mortality in the Past, Université de Montréal,
Montréal, 7-9 October.</mixed-citation>
         </ref>
         <ref id="d911e379a1310">
            <mixed-citation id="d911e383" publication-type="other">
Briscoe, J., R. G. Feachem and M. Mujibur Rahaman. 1986. Evaluating Health Impact. Water
Supply, Sanitation, and Hygiene Education. Ottawa: International Development Research Centre,
UNICEF and ICDDR,B.</mixed-citation>
         </ref>
         <ref id="d911e396a1310">
            <mixed-citation id="d911e400" publication-type="other">
Brown, J.C. 1990. Public health reform and the decline in urban mortality. The case of Germany,
1876-1912. Paper presented at International Economic History Congress, Louvain.</mixed-citation>
         </ref>
         <ref id="d911e410a1310">
            <mixed-citation id="d911e414" publication-type="other">
Butz, W.P., J.P. Habicht, J. DaVanzo et al. 1984. Environmental factors in the relationship between
breastfeeding and infant mortality: the role of sanitation and water in Malaysia. American
Journal of Epidemiology 119 : 516-525 .</mixed-citation>
         </ref>
         <ref id="d911e427a1310">
            <mixed-citation id="d911e431" publication-type="other">
Castensson, R., M. Löwgren and J. Sundin. 1988. Urban water supply and improvement of health
conditions. Pp. 273-298 in Society, Health and Population during the Demographic Transition,
ed. A. Brandstöm and L.-G. Tedebrand. Stockholm: Almqvist and Wiksell International.</mixed-citation>
         </ref>
         <ref id="d911e445a1310">
            <mixed-citation id="d911e449" publication-type="other">
Condran, G.A. 1987. Declining mortality in the United States in the late nineteenth and early
twentieth centuries. Annales de Démographie Historique:! 19-141 .</mixed-citation>
         </ref>
         <ref id="d911e459a1310">
            <mixed-citation id="d911e463" publication-type="other">
Condran, G.A. and R.A. Cheney. 1982. Mortality trends in Philadelphia: age-and cause-specific
death rates 1870-1930. Demography 19:97-123.</mixed-citation>
         </ref>
         <ref id="d911e473a1310">
            <mixed-citation id="d911e477" publication-type="other">
Condran, G.A. and E. Crimmins-Gardner. 1978. Public Health measures and mortality in U.S. cities
in the late nineteenth century. Human Ecology 6:27-58.</mixed-citation>
         </ref>
         <ref id="d911e487a1310">
            <mixed-citation id="d911e491" publication-type="other">
Cvjetanovic, B. 1986. Health effects and impact of water supply and sanitation. World Health
Statistics Quarterly 39:105-1 17.</mixed-citation>
         </ref>
         <ref id="d911e501a1310">
            <mixed-citation id="d911e505" publication-type="other">
De Sweemer, C. 1984. The influence of child spacing on child survival. Population Studies 38:47-72.</mixed-citation>
         </ref>
         <ref id="d911e512a1310">
            <mixed-citation id="d911e516" publication-type="other">
Edvinsson, S. 1993. Urban health and social class. Pp. 55-108 in Health and Social Change. Disease,
Health and Public Care in the Sundsvall District 1750-1950, ed. A. Brändström and L.-G.
Tedebrand. Umeâ: Demographic Data Base, Umeâ University.</mixed-citation>
         </ref>
         <ref id="d911e530a1310">
            <mixed-citation id="d911e534" publication-type="other">
Edvinsson, S. 1995. Mortality and the urban environment: Sundsvall in the 1880's. Pp. 93-113 in
Swedish Urban Demography during Industrialization, ed. A. Brändström and L.-G. Tedebrand.
Umeâ: Demographic Data Base, Umeâ University.</mixed-citation>
         </ref>
         <ref id="d911e547a1310">
            <mixed-citation id="d911e551" publication-type="other">
Esrey, S.A. 1994. Multi-country study to examine relationships between the health of children and the
level of water and sanitation service, distance to water, and type of water used. Final Report,
McGill University, Québec.</mixed-citation>
         </ref>
         <ref id="d911e564a1310">
            <mixed-citation id="d911e568" publication-type="other">
Esrey, S.A. and J.-P. Habicht. 1985. The Impact of Improved Water Supplies and Excreta Disposal
Facilities on Diarrheal Morbidity, Growth and Mortality among Children. Cornell International
Nutrition Monograph Series No. 15. Ithaca NY: Cornell University.</mixed-citation>
         </ref>
         <ref id="d911e581a1310">
            <mixed-citation id="d911e585" publication-type="other">
Esrey, S.A. and J.-P. Habicht. 1986. Epidemiologie evidence for health benefits from improved water
and sanitation in developing countries. Epidemiologie Reviews 8:1 17-127.</mixed-citation>
         </ref>
         <ref id="d911e595a1310">
            <mixed-citation id="d911e599" publication-type="other">
Esrey, S.A., J.B. Potash, L. Roberts and C. Shiff. 1991. Effects of improved water supply and
sanitation on ascariasis, diarrhoea, dracunculiasis, hookworm infection, schistosomiasis, and
trachoma. Bulletin of the World Health Organization 69,5:609-621.</mixed-citation>
         </ref>
         <ref id="d911e612a1310">
            <mixed-citation id="d911e616" publication-type="other">
Esrey, S. A. and E. Sommerfelt. 1991. Water, sanitation and nutritional status of young children in Sri
Lanka. Pp. 1525-1545 in Proceedings of the Demographic and Health Surveys World
Conference, Washington DC, Vol. 2. Columbia MD: IRD/Macro International.</mixed-citation>
         </ref>
         <ref id="d911e630a1310">
            <mixed-citation id="d911e634" publication-type="other">
Gaspari, K.C. and A.G. Woolf. 1985. Income, public works, and mortality in early twentieth-century
American cities. Journal of Economic History 45:355-361 .</mixed-citation>
         </ref>
         <ref id="d911e644a1310">
            <mixed-citation id="d911e648" publication-type="other">
Goubert, J. 1984. Public hygiene and mortality decline in France in the 19th century. Pp. 151-159 in
Pre-Industrial Population Change: The Mortality Decline and Short-term Population Move-
ments, éd. T. Bengtsson, G. Fridlizius, and R. Ohlsson. Stockholm: Almqvist and Wiksell
International.</mixed-citation>
         </ref>
         <ref id="d911e664a1310">
            <mixed-citation id="d911e668" publication-type="other">
Hardy, A. 1993. The Epidemic Streets. Infectious Disease and the Rise of Preventive Medicine, 1856-
1900. Oxford: Clarendon Press.</mixed-citation>
         </ref>
         <ref id="d911e678a1310">
            <mixed-citation id="d911e682" publication-type="other">
Hemminki, E. and A. Paakkulainen. 1976. The effect of antibiotics on mortality from infectious
diseases in Sweden and Finland. American Journal of Public Health 66: 1 180-1 184.</mixed-citation>
         </ref>
         <ref id="d911e692a1310">
            <mixed-citation id="d911e696" publication-type="other">
Higgs, R. and D. Booth. 1979. Mortality differentials within large American cities in 1890. Human
Ecology 7:353-369.</mixed-citation>
         </ref>
         <ref id="d911e706a1310">
            <mixed-citation id="d911e710" publication-type="other">
Hofstee, E.W. 1978. De Demografische Ontwikkeling van Nederland in de Eerste Helft van de Negen-
tiende Eeuw. Een Historisch-demografische en Sociologische Studie. Deventer: Van Loghum
Slaterus.</mixed-citation>
         </ref>
         <ref id="d911e724a1310">
            <mixed-citation id="d911e728" publication-type="other">
Janssens, A. 1993. Family and Social Change. The Household as a Process in an Industrializing
Community. Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d911e738a1310">
            <mixed-citation id="d911e742" publication-type="other">
Lindskog, U., P. Lindskog and S. Wall. 1987. Water supply, sanitation and health education
programmes in developing countries: problems of evaluation. Scandinavian Journal of Social
Medicine 15:123-130.</mixed-citation>
         </ref>
         <ref id="d911e755a1310">
            <mixed-citation id="d911e759" publication-type="other">
Lindskog, P. and J. Lundqvist. 1989. Why Poor Children Stay Sick. The Human Ecology of Child
Health and Welfare in Rural Malawi. Uppsala Research Report no. 85. Uppsala: Scandinavian
Institute of African Studies.</mixed-citation>
         </ref>
         <ref id="d911e772a1310">
            <mixed-citation id="d911e776" publication-type="other">
Mackenbach, J.P. and C. W. N. Looman. 1988. Secular trends of infectious disease mortality in the
Netherlands, 1911-1978: quantitative estimates of changes coinciding with the introduction of
antibiotics. International Journal of Epidemiology 17:618-624.</mixed-citation>
         </ref>
         <ref id="d911e789a1310">
            <mixed-citation id="d911e793" publication-type="other">
Marland, H. 1995. De missie van de vroedvrouw: tussen traditionele en moderne kraamzorg in het
begin van de twintigste eeuw. Pp. 67-86 in Op Zoek naar Genezing. Medische Geschiedenis van
Nederland vanaf de Zestiende Eeuw, éd. M. Gijswijt-Hofstra. Amsterdam: Amsterdam
University Press.</mixed-citation>
         </ref>
         <ref id="d911e809a1310">
            <mixed-citation id="d911e813" publication-type="other">
Marland, H. 1996. Unequal childbirth: midwives and regional differences in maternity care in the
Netherlands 1900-1940. Paper presented at European Social Science History Conference,
Noordwijkerhout, 9-11 May.</mixed-citation>
         </ref>
         <ref id="d911e827a1310">
            <mixed-citation id="d911e831" publication-type="other">
Massée Bateman, O., S. Smith and P. Roark. 1993. A Comparison of the Health Effects of Water
Supply and Sanitation in Urban and Rural Areas of Five African Countries. Washington DC: US
Agency for International Development.</mixed-citation>
         </ref>
         <ref id="d911e844a1310">
            <mixed-citation id="d911e848" publication-type="other">
McKeown, T. 1976. The Modern Rise of Population. London: Edward Arnold.</mixed-citation>
         </ref>
         <ref id="d911e855a1310">
            <mixed-citation id="d911e859" publication-type="other">
Meckel, R.A. 1985. Immigration, mortality and population growth in Boston, 1840-1880. Journal of
Interdisciplinary History 15:393-417.</mixed-citation>
         </ref>
         <ref id="d911e869a1310">
            <mixed-citation id="d911e873" publication-type="other">
Meeker, E. 1972. The improving health of the United States, 1850-1915. Explorations in Economic
History 9:353-373.</mixed-citation>
         </ref>
         <ref id="d911e883a1310">
            <mixed-citation id="d911e887" publication-type="other">
Merrick, T.W. 1985. The effect of piped water on early childhood mortality in urban Brazil, 1970 to
1976. Demography 22:1-23.</mixed-citation>
         </ref>
         <ref id="d911e897a1310">
            <mixed-citation id="d911e901" publication-type="other">
Nilsson, H. 1994. Mot Bättre Hälsa. Dödlighet och Hälsoarbete i Linköping 1860-1894. Linköping:
Linköping University.</mixed-citation>
         </ref>
         <ref id="d911e912a1310">
            <mixed-citation id="d911e916" publication-type="other">
Oomens, CA. 1989. De Loop der Bevolking van Nederland in de Negentiende Eeuw. CBS.
Statistische Onderzoekingen. The Hague: Staatsuitgeverij.</mixed-citation>
         </ref>
         <ref id="d911e926a1310">
            <mixed-citation id="d911e930" publication-type="other">
Pisani, V. 1994. Improving personal and domestic hygiene: does it reduce diarrhoeal disease? MSc
Public Health in Developing Countries, University of London.</mixed-citation>
         </ref>
         <ref id="d911e940a1310">
            <mixed-citation id="d911e944" publication-type="other">
Preston, S.H. and E. van de Walle. 1978. Urban French mortality in the nineteenth century.
Population Studies 32:275-298.</mixed-citation>
         </ref>
         <ref id="d911e954a1310">
            <mixed-citation id="d911e958" publication-type="other">
Sorkin, A.L. 1988. The impact of water and sanitation on health and development. Pp. 77-98 in
Research in Human Capital and Development. A Research Annual, Vol. 5, ed. I. Sirageldin.
Greenwich CT: JAI Press Inc.</mixed-citation>
         </ref>
         <ref id="d911e971a1310">
            <mixed-citation id="d911e975" publication-type="other">
Swartsenberg, L.A. 1981. Sterfte in de stad 1869-1910: een onderzoek naar het verloop van de
sterftedaling in de Nederlandse steden met behulp van de doodsoorzakenstatistiek. Master's
Thesis, University of Amsterdam.</mixed-citation>
         </ref>
         <ref id="d911e988a1310">
            <mixed-citation id="d911e992" publication-type="other">
United Nations. 1985. Socio-economic Differentials in Child Mortality in Developing Countries. New
York.</mixed-citation>
         </ref>
         <ref id="d911e1003a1310">
            <mixed-citation id="d911e1007" publication-type="other">
Van der Heijden, C.G. W.P. 1995a. Kleurloos, Reukloos en Smaakloos Drinkwater. De Watervoor-
ziening in Tilburg vanaf het Einde van de Negentiende Eeuw. Tilburg: Tilburgse Historische
Reeks.</mixed-citation>
         </ref>
         <ref id="d911e1020a1310">
            <mixed-citation id="d911e1024" publication-type="other">
Van der Heijden, C.G.W.P. 1995b. Het Heeft niet Willen Groeien. Zuigelingen-en Kindersterfte in
Tilburg, 1820-1930. Tilburg: Stichting Zuidelijk Historisch Contact.</mixed-citation>
         </ref>
         <ref id="d911e1034a1310">
            <mixed-citation id="d911e1038" publication-type="other">
Van Doremalen, H. 1993. Blauwsloten en Riolen. Een Milieu-Historische Studie over Tilburg en Zijn
Rioolstelsel. Tilburg: Tilburgse Historische Reeks.</mixed-citation>
         </ref>
         <ref id="d911e1048a1310">
            <mixed-citation id="d911e1052" publication-type="other">
Van Poppel, F. and J. P. Van Dijk. In press. The development of cause-of-death registration in the
Netherlands: 1865-1955. Continuity and Change.</mixed-citation>
         </ref>
         <ref id="d911e1062a1310">
            <mixed-citation id="d911e1066" publication-type="other">
Van Tulder, J.J.M. 1962. De Beroepsmobiliteit in Nederland van 1919 tot 1954. Een Sociaal-
statis tische Studie. Leiden: Stenfert Kroese.</mixed-citation>
         </ref>
         <ref id="d911e1076a1310">
            <mixed-citation id="d911e1080" publication-type="other">
Vögele, J. 1993. Sanitäre Reformen und der Sterblichkeitsrückgang in deutschen Städten, 1877-1913.
Vierteljahrschrift für Sozial-und Wirtschaftsgeschichte 80:345-365.</mixed-citation>
         </ref>
         <ref id="d911e1091a1310">
            <mixed-citation id="d911e1095" publication-type="other">
World Health Organization. 1992. Our Planet, Our Health. Report of the WHO Commission on
Health and Environment. Geneva.</mixed-citation>
         </ref>
         <ref id="d911e1105a1310">
            <mixed-citation id="d911e1109" publication-type="other">
Yamaguchi, K. 1991. Event History Analysis. Newbury Park: Sage.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
