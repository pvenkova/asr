<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt5hhs2t</book-id>
      <subj-group>
         <subject content-type="call-number">KZ3410.A44 2013</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">International law</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">International courts</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Human rights</subject>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
         <subject>Law</subject>
      </subj-group>
      <book-title-group>
         <book-title>The New Terrain of International Law</book-title>
         <subtitle>Courts, Politics, Rights</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Alter</surname>
               <given-names>Karen J.</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>24</day>
         <month>01</month>
         <year>2014</year>
      </pub-date>
      <isbn content-type="ppub">9780691154756</isbn>
      <isbn content-type="ppub">0691154740</isbn>
      <isbn content-type="epub">9781400848683</isbn>
      <publisher>
         <publisher-name>Princeton University Press</publisher-name>
         <publisher-loc>Princeton; Oxford</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2014</copyright-year>
         <copyright-holder>Princeton University Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt5hhs2t"/>
      <abstract abstract-type="short">
         <p>In 1989, when the Cold War ended, there were six permanent international courts. Today there are more than two dozen that have collectively issued over thirty-seven thousand binding legal rulings.<italic>The New Terrain of International Law</italic>charts the developments and trends in the creation and role of international courts, and explains how the delegation of authority to international judicial institutions influences global and domestic politics.</p>
         <p>
            <italic>The New Terrain of International Law</italic>presents an in-depth look at the scope and powers of international courts operating around the world. Focusing on dispute resolution, enforcement, administrative review, and constitutional review, Karen Alter argues that international courts alter politics by providing legal, symbolic, and leverage resources that shift the political balance in favor of domestic and international actors who prefer policies more consistent with international law objectives. International courts name violations of the law and perhaps specify remedies. Alter explains how this limited power--the power to speak the law--translates into political influence, and she considers eighteen case studies, showing how international courts change state behavior. The case studies, spanning issue areas and regions of the world, collectively elucidate the political factors that often intervene to limit whether or not international courts are invoked and whether international judges dare to demand significant changes in state practices.</p>
      </abstract>
      <counts>
         <page-count count="392"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5hhs2t.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5hhs2t.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5hhs2t.3</book-part-id>
                  <title-group>
                     <title>List of Illustrations</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5hhs2t.4</book-part-id>
                  <title-group>
                     <title>Case Study Index</title>
                  </title-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5hhs2t.5</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <fpage>xv</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5hhs2t.6</book-part-id>
                  <title-group>
                     <title>List of Abbreviations</title>
                  </title-group>
                  <fpage>xxv</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5hhs2t.7</book-part-id>
                  <title-group>
                     <label>CHAPTER 1</label>
                     <title>The New Terrain of International Law:</title>
                     <subtitle>Courts, Politics, Rights</subtitle>
                  </title-group>
                  <fpage>3</fpage>
                  <abstract>
                     <p>International relations have long been considered outside of the domain of law. Most people presume that law is only meaningful when backed by a central enforcer. By this logic, absent a world state international law cannot meaningfully exist. But since the end of the Cold War, the rulings of international judges have led Latin American governments to secure indigenous peoples’ land rights; the United States Congress to eliminate a tax benefit for American corporations; Germany to grant women a wider role in the military; Niger to compensate a former slave for her entrapment in Niger’s family law justice system; and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5hhs2t.8</book-part-id>
                  <title-group>
                     <label>CHAPTER 2</label>
                     <title>International Courts Altering Politics</title>
                  </title-group>
                  <fpage>32</fpage>
                  <abstract>
                     <p>International courts have the power to issue binding rulings in cases that are adjudicated. How does creating an international court with such limited powers change international politics? This chapter answers this question by considering the power of courts in general. Although we often talk about courts enforcing the law, the truth of the matter is that no courts enforce the law. States, with their monopoly on the legitimate use of force, enforce the law. But at the national level court rulings are arguably backed by state power. Many people consider international courts to be particularly handicapped because there is no</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5hhs2t.9</book-part-id>
                  <title-group>
                     <label>CHAPTER 3</label>
                     <title>The New International Courts</title>
                  </title-group>
                  <fpage>68</fpage>
                  <abstract>
                     <p>When the Cold War ended in 1989, there were six permanent international courts plus the noncompulsory dispute settlement system of the General Agreement on Tariffs and Trade (GATT). The European Court of Justice (ECJ) offered the model of an active and effective new-style international court. The other six international adjudicatory mechanisms had collectively issued only 373 binding judgments between 1945 and 1989. Today, there are at least twenty-four permanent international courts that have collectively issued over 37,000 binding legal judgments. The greater influence of ICs today is not simply a matter of numbers. Today’s ICs are fundamentally different from their</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5hhs2t.10</book-part-id>
                  <title-group>
                     <label>CHAPTER 4</label>
                     <title>World History and the Evolving International Judiciary</title>
                  </title-group>
                  <fpage>112</fpage>
                  <abstract>
                     <p>My objective for this book is to understand how and when the creation of international courts affects international and domestic politics. This chapter, however, speaks to the question of why there are more international courts today than at any point in history.¹ Delegation to international courts (ICs) at its core is inspired by a distrust of governments, a belief the rule of law is enhanced when individuals, including elected officials, are not judges of their own causes and that domestic checks and balances are also insufficient to ensure that governments keep their international covenants. Citizens and investors have increasingly desired</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5hhs2t.11</book-part-id>
                  <title-group>
                     <label>CHAPTER 5</label>
                     <title>International Dispute Settlement</title>
                  </title-group>
                  <fpage>163</fpage>
                  <abstract>
                     <p>Hedley Bull once argued that international society, like all societies, organizes to achieve the elemental, primary, and universal goals of peace, property protection, and the keeping of covenants.¹ International systems of order have changed over time, from empires to religious orders, to the rule of kings and colonial powers, to the current international state system, but all of these systems have included mechanisms to facilitate the peaceful settlement of disputes. In more recent history, kings, queens, and colonial officers dealt with transborder disputes within their spheres of influence. Often proceedings were more diplomatic than legal in nature, with adjudicators using</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5hhs2t.12</book-part-id>
                  <title-group>
                     <label>CHAPTER 6</label>
                     <title>International Administrative Review</title>
                  </title-group>
                  <fpage>199</fpage>
                  <abstract>
                     <p>The administrative review role of ICs is often underappreciated because the implementation of international law garners less attention and most regulatory issues (for example, environmental regulation, regulation of manufacturing, employer-employee relations) remain primarily national. Lawyers who deal with intellectual property issues, foreign contracts, and mergers and acquisitions involving multinational corporations are surely aware of the importance of international law regulating these issues, but unless administrators are grossly misapplying international rules, there will be no reason to seek recourse in international adjudicatory bodies. That said, the body of global regulatory law has grown significantly since the end of World War II,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5hhs2t.13</book-part-id>
                  <title-group>
                     <label>CHAPTER 7</label>
                     <title>International Law Enforcement</title>
                  </title-group>
                  <fpage>244</fpage>
                  <abstract>
                     <p>Many people assume that courts exist to enforce the law. As this book makes clear, courts play a number of roles besides law enforcement. Indeed, international law enforcement is a relatively new role for international courts (ICs). The first ICs were created to provide a peaceful means to resolve disputes should state parties so choose. The Permanent Court of Arbitration (1899–present), the Central American Court of Justice (1908–18), the Permanent Court of Justice (1920–40), the International Court of Justice (1945–present), and even the original dispute resolution system of General Agreement on Tariffs and Trade (1945–94)</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5hhs2t.14</book-part-id>
                  <title-group>
                     <label>CHAPTER 8</label>
                     <title>International Constitutional Review</title>
                  </title-group>
                  <fpage>282</fpage>
                  <abstract>
                     <p>The previous chapter discussed international courts in their enforcement role of declaring states’ noncompliance with international law. An international judicial finding of state noncompliance may make violating international law more costly, but it does not invalidate conflicting domestic laws. Constitutional review differs in that it is designed to check sovereign power. I adopt Alec Stone Sweet’s definition of constitutional review as the judicial authority to invalidate laws and government acts on the basis of a conflict with higher order legal obligations.¹ Constitutional review raises the stakes of law enforcement because where the legal conflict concerns a higher order law, the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5hhs2t.15</book-part-id>
                  <title-group>
                     <label>CHAPTER 9</label>
                     <title>International Courts and Democratic Politics</title>
                  </title-group>
                  <fpage>335</fpage>
                  <abstract>
                     <p>The new terrain of international law is an artifact of a number of indirectly connected decisions: the decision to expand the substantive reach of international law, to embed international legal rules into national legal orders, to expand extraterritorial enforcement capacity of domestic judges, and to create more international courts with a compulsory jurisdiction and access for nonstate actors to initiate litigation. The result of these trends is the globalization of judicial politics and the judicialization of international politics. Judicialization occurs where citizens, organizations, and firms see law as conferring upon them rights, and where politicians conceive of their policy and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5hhs2t.16</book-part-id>
                  <title-group>
                     <title>Chapter Appendixes</title>
                  </title-group>
                  <fpage>367</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5hhs2t.17</book-part-id>
                  <title-group>
                     <title>Legal Cases Index and Citations</title>
                  </title-group>
                  <fpage>401</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5hhs2t.18</book-part-id>
                  <title-group>
                     <title>Court Treaty Bibliography and Litigation Data Sources</title>
                  </title-group>
                  <fpage>407</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5hhs2t.19</book-part-id>
                  <title-group>
                     <title>Bibliography of Cited Works</title>
                  </title-group>
                  <fpage>415</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5hhs2t.20</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>441</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
