<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt2jcz2r</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt2jcrx4</book-id>
      <subj-group>
         <subject content-type="call-number">HC60.5.W493 2011</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Peace Corps (U.S.)</subject>
         <subj-group>
            <subject content-type="lcsh">History</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>History</subject>
      </subj-group>
      <book-title-group>
         <book-title>Voices from the Peace Corps</book-title>
         <subtitle>Fifty Years of Kentucky Volunteers</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Wilson</surname>
               <given-names>Angene</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib2">
            <name name-style="western">
               <surname>Wilson</surname>
               <given-names>Jack</given-names>
            </name>
         </contrib>
         <contrib contrib-type="foreword-author" id="contrib3">
            <role>Foreword by</role>
            <name name-style="western">
               <surname>Dodd</surname>
               <given-names>Christopher J.</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>28</day>
         <month>01</month>
         <year>2011</year>
      </pub-date>
      <isbn content-type="ppub">9780813129754</isbn>
      <isbn content-type="epub">9780813129822</isbn>
      <isbn content-type="epub">0813129826</isbn>
      <publisher>
         <publisher-name>The University Press of Kentucky</publisher-name>
         <publisher-loc>Lexington, Kentucky</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2011</copyright-year>
         <copyright-holder>The University Press of Kentucky</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt2jcrx4"/>
      <abstract abstract-type="short">
         <p>President John F. Kennedy established the Peace Corps on March 1, 1961. In the fifty years since, nearly 200,000 Americans have served in 139 countries, providing technical assistance, promoting a better understanding of American culture, and bringing the world back to the United States.</p>
         <p>In<italic>Voices from the Peace Corps: Fifty Years of Kentucky Volunteers,</italic>Angene Wilson and Jack Wilson, who served in Liberia from 1962 to 1964, follow the experiences of volunteers as they make the decision to join, attend training, adjust to living overseas and the job, make friends, and eventually return home to serve in their communities. They also describe how the volunteers made a difference in their host countries and how they became citizens of the world for the rest of their lives. Among many others, the interviewees include a physics teacher who served in Nigeria in 1961, a smallpox vaccinator who arrived in Afghanistan in 1969, a nineteen-year-old Mexican American who worked in an agricultural program in Guatemala in the 1970s, a builder of schools and relationships who served in Gabon from 1989 to 1992, and a retired office administrator who taught business in Ukraine from 2000 to 2002.<italic>Voices from the Peace Corps</italic>emphasizes the value of practical idealism in building meaningful cultural connections that span the globe.</p>
      </abstract>
      <counts>
         <page-count count="400"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jcrx4.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jcrx4.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jcrx4.3</book-part-id>
                  <title-group>
                     <title>Series Editors’ Foreword</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Klotter</surname>
                           <given-names>James C.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Birdwhistell</surname>
                           <given-names>Terry L.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib3" xlink:type="simple">
                        <name name-style="western">
                           <surname>Boyd</surname>
                           <given-names>Doug</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>vii</fpage>
                  <abstract>
                     <p>In the field of oral history, Kentucky is a national leader. Over the past several decades, thousands of its citizens have been interviewed. The Kentucky Remembered series brings into print the most important of those collections, with each volume focusing on a particular subject.</p>
                     <p>Oral history is, of course, only one type of source material. Yet by the very personal nature of recollection, hidden aspects of history are often disclosed. Oral sources provide a vital thread in the rich fabric that is Kentucky history.</p>
                     <p>
                        <italic>Voices from the Peace Corps: Fifty Years of Kentucky Volunteers</italic>is the ninth volume in<italic>Kentucky</italic>
                     </p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jcrx4.4</book-part-id>
                  <title-group>
                     <title>Foreword</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Dodd</surname>
                           <given-names>Christopher J.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>ix</fpage>
                  <abstract>
                     <p>As the Peace Corps reaches its fiftieth anniversary, it is a time for celebration. We celebrate one of the most radical ideas I know—that a great nation should send its people abroad not to extend its power, not to intimidate its enemies, not to kill and be killed but to build, to dig, to teach, and to ask nothing in return.</p>
                     <p>The Peace Corps allows the world to know America and America to know the world. It sends dedicated and industrious people to the far corners of the world to deliver much-needed assistance to countries and communities in the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jcrx4.5</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Wilson</surname>
                           <given-names>Angene</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Wilson</surname>
                           <given-names>Jack</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jcrx4.6</book-part-id>
                  <title-group>
                     <title>Map of Peace Corps Host Countries</title>
                  </title-group>
                  <fpage>xx</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jcrx4.7</book-part-id>
                  <title-group>
                     <label>Chapter 1</label>
                     <title>Why We Went</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>Martin grew up in Murray, Kentucky, where his father was head of the Speech Department and debate coach at Murray State University. Martin’s Kentucky roots grow deep. He is a direct descendant of Jane Kenton, sister of pioneer settler Simon Kenton, who came to Kentucky in 1783 and gave Kenton County its name. After high school graduation in 1958 Martin joined the army to do something different and to travel; he was stationed at a missile base in New Jersey for three years, working as a guidance system and computer operator. Then he returned to Murray for college.</p>
                     <p>Patsy grew</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jcrx4.8</book-part-id>
                  <title-group>
                     <label>Chapter 2</label>
                     <title>Getting In</title>
                  </title-group>
                  <fpage>35</fpage>
                  <abstract>
                     <p>Martin remembered that “the application process went smoothly. We did have to go through a psychologist that we met with, and there were a lot of questions about whether or not our marriage could sustain living in harsh conditions.” Patsy said, “Everything seemed very routine, except when an aunt of Martin’s had been interviewed by the FBI in California. She was an active member of the John Birch Society, so we worried that her interview might be the glitch that might keep us out.”</p>
                     <p>Patsy remembered that her “mother was horrified. My husband had brought me to rural western Kentucky,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jcrx4.9</book-part-id>
                  <title-group>
                     <label>Chapter 3</label>
                     <title>Training</title>
                  </title-group>
                  <fpage>59</fpage>
                  <abstract>
                     <p>Martin and Patsy trained for two months at Princeton University and for one month at what was then called Robert College in Istanbul. Martin remembered that the Princeton part oriented the volunteers to Turkish culture, history, politics, and the language and included training in teaching English as a second language. Patsy’s memories from Princeton were of “sixteen-hour days, grueling days learning language and culture. And bonding, of course. It was a very large training group, 250. It was exciting and demanding and interesting. We got a huge dose of history, politics, religion, societal change. We felt like, by the time</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jcrx4.10</book-part-id>
                  <title-group>
                     <label>Chapter 4</label>
                     <title>Living</title>
                  </title-group>
                  <fpage>95</fpage>
                  <abstract>
                     <p>For the first year, Martin and Patsy lived above a hardware store in Kirsehir, a town of about 20,000 in Cappadocia, about six hours southeast of Ankara, the capital. Martin described their living quarters: “The apartment had a living room, bedroom, and bathroom (oriental style, just a hole in the ground). There was cold running water. The shower was a jerry-rigged mechanism where you had to put wood underneath and heat the tank up. We heated with a potbelly stove using kindling and coal. We had a nice little balcony, so you could walk out and see the town. Behind</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jcrx4.11</book-part-id>
                  <title-group>
                     <label>Chapter 5</label>
                     <title>The Toughest . . .</title>
                  </title-group>
                  <fpage>129</fpage>
                  <abstract>
                     <p>“Patsy lived pretty much in a separate world,” Martin said. “When we walked out into town, she would wear a coat and scarf and walk behind me a couple of steps. One of the wonderful traditions in small towns in Turkey is that in the evening everybody strolls. Two husbands are holding hands, walking along, and then two wives in back are holding hands and walking along, just chatting about the day’s events. That’s what we did with our friends. I spent time with men in coffeehouses, where we would talk about politics, sports, and local events.”</p>
                     <p>Patsy remembered: “Of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jcrx4.12</book-part-id>
                  <title-group>
                     <label>Chapter 6</label>
                     <title>. . . Job You’ll Ever Love</title>
                  </title-group>
                  <fpage>155</fpage>
                  <abstract>
                     <p>Martin and Patsy had three successive teaching jobs in three different places in Turkey. For the first ten months, they taught English in the public high school and offered courses in the evening to interested people in the community in Kirsehir. Martin remembered:</p>
                     <p>The classrooms were not well equipped. They were often very cold in the wintertime, and in the summers very hot. All the male students had to have a Western-style jacket, often frayed, torn, patched. The girls had to have a standard dress. The students were remarkably receptive, interested, but we were confronted with the things all Turkish</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jcrx4.13</book-part-id>
                  <title-group>
                     <title>Photographs</title>
                  </title-group>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jcrx4.14</book-part-id>
                  <title-group>
                     <label>Chapter 7</label>
                     <title>Telling Stories</title>
                  </title-group>
                  <fpage>199</fpage>
                  <abstract>
                     <p>Martin told the following story:</p>
                     <p>In Kırsehir, there was a man we met from our evening class, and he had an unusual name, Günaydın Günaydın, which means “good morning, good morning.” He was in charge of ensuring that the local towns had ample supplies of freshwater. And he would take us out on some of his trips to the villages because he knew we were interested in small-town life and communities. He took us to one village that had been there for maybe a thousand years. And all that time they had never had a ready water supply. There was</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jcrx4.15</book-part-id>
                  <title-group>
                     <label>Chapter 8</label>
                     <title>Friends Can Become Family</title>
                  </title-group>
                  <fpage>227</fpage>
                  <abstract>
                     <p>Martin talked about a friend in Kırsehir: “We offered English courses in the evening to people in the community, and that allowed us to meet some very interesting folks. One individual that I remember particularly was a lawyer—a very, very large man, especially for central Turkey—who took us under his wing and had picnics for us out at his nice, spacious home. He owned some land and he had sheep, and he introduced us to other people and made it possible for me to interact with locals at the clubhouse, where people would play backgammon and chess.”</p>
                     <p>I</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jcrx4.16</book-part-id>
                  <title-group>
                     <label>Chapter 9</label>
                     <title>Coming Home</title>
                  </title-group>
                  <fpage>247</fpage>
                  <abstract>
                     <p>Patsy described their trip home: “We took our airline tickets and cashed them in. Since we’d lost forty pounds, we made a plan to visit every country in Europe and eat our way home so that by the time we got home, our parents would recognize us. So we took off through the Balkans and Vienna and Paris. And we ate. We spent all our money having fun because we knew when we came home we’d be back to being poor. We’d be graduate students.”</p>
                     <p>Martin described what happened next: “We knew we were coming back to something positive that</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jcrx4.17</book-part-id>
                  <title-group>
                     <label>Chapter 10</label>
                     <title>Making a Difference</title>
                  </title-group>
                  <fpage>269</fpage>
                  <abstract>
                     <p>Patsy described a project they started in their second town of Ürgüp. “We took on the merchants’ association, who wanted to learn business and tourism English and marketing skills. Most of the tourists came from Scandinavia and Germany and brought in all their supplies, enjoyed the area, and left—and never left any money. So we became like Robin Hood: fleece the rich and give it to the poor. The poor merchants began to open up their restaurants and fix box lunches and put out their carpets and advertise in English [with menus translated by Martin and Patsy]. And so</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jcrx4.18</book-part-id>
                  <title-group>
                     <label>Chapter 11</label>
                     <title>Citizens of the World for the Rest of Our Lives</title>
                  </title-group>
                  <fpage>285</fpage>
                  <abstract>
                     <p>Martin began by talking about the Peace Corps’ influence on his and Patsy’s professional career choices.</p>
                     <p>Political science came out of that for me, as did Patsy’s social work, and then eventually I went into international service in the Social Security Administration. I traveled all over the world with the International Social Security Administration. We were in Geneva for two years. Then I got a doctorate in international social work and taught at the University of Iowa and Southern Illinois University and finally came back to Kentucky to be associate dean for research in the School of Social Work at</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jcrx4.19</book-part-id>
                  <title-group>
                     <title>Postscript:</title>
                     <subtitle>Our Story</subtitle>
                  </title-group>
                  <fpage>327</fpage>
                  <abstract>
                     <p>In March 1961 we were idealistic seniors at The College of Wooster in Ohio. We had heard John F. Kennedy’s inaugural address and wanted to see other parts of the world, to learn about other cultures and peoples, to travel—and to serve. We thought Peace Corps would be an exciting and a good thing to do.¹</p>
                     <p>Our life experiences influenced why we joined. We grew up with strong roots in our families, who also encouraged us to spread our wings. Angene remembers wearing flags representing countries of the United Nations as a costume in a 1946 Fourth of July</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jcrx4.20</book-part-id>
                  <title-group>
                     <title>Appendix:</title>
                     <subtitle>Interviewee Information</subtitle>
                  </title-group>
                  <fpage>345</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jcrx4.21</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>351</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jcrx4.22</book-part-id>
                  <title-group>
                     <title>Selected Bibliography</title>
                  </title-group>
                  <fpage>359</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jcrx4.23</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>363</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
