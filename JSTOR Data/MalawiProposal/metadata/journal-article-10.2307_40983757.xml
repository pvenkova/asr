<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">canccauscont</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100826</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Cancer Causes &amp; Control</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Springer</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">09575243</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15737225</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">40983757</article-id>
         <title-group>
            <article-title>Nighttime light level co-distributes with breast cancer incidence worldwide</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Itai</given-names>
                  <surname>Kloog</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Richard G.</given-names>
                  <surname>Stevens</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Abraham</given-names>
                  <surname>Haim</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Boris A.</given-names>
                  <surname>Portnov</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>12</month>
            <year>2010</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">21</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">12</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40044211</issue-id>
         <fpage>2059</fpage>
         <lpage>2068</lpage>
         <permissions>
            <copyright-statement>© 2010 Springer</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/40983757"/>
         <abstract>
            <p>Breast cancer incidence varies widely among countries of the world for largely unknown reasons. We investigated whether country-level light at night (LAN) is associated with incidence. We compared incidence rates of five common cancers in women (breast, lung, colorectal, larynx, and liver), observed in 164 countries of the world from the GLOBOCAN database, with population-weighted country-level LAN, and with several developmental and environmental indicators, including fertility rate, per capita income, percent of urban population, and electricity consumption. Two types of regression models were used in the analysis: Ordinary Least Squares and Spatial Errors. We found a significant positive association between population LAN level and incidence rates of breast cancer. There was no such an association between LAN level and colorectal, larynx, liver, and lung cancers. A sensitivity test, holding other variables at their average values, yielded a 30-50% higher risk of breast cancer in the highest LAN exposed countries compared to the lowest LAN exposed countries. The possibility that under-reporting from the registries in the low-resource, and also low-LAN, countries created a spurious association was evaluated in several ways and shown not to account for the results. These findings provide coherence of the previously reported case-control and cohort studies with the co-distribution of LAN and breast cancer in entire populations.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d19e207a1310">
            <label>1</label>
            <mixed-citation id="d19e214" publication-type="other">
Stevens RG (2009) Light-at-night, circadian disruption and breast
cancer: assessment of existing evidence. Int J Epidemiol
38:963-970</mixed-citation>
         </ref>
         <ref id="d19e227a1310">
            <label>2</label>
            <mixed-citation id="d19e234" publication-type="other">
Kolstad HA (2008) Nightshift work and risk of breast cancer and
other cancers-a critical review of the epidemiologie evidence.
Scand J Work Environ Health 34:5-22</mixed-citation>
         </ref>
         <ref id="d19e247a1310">
            <label>3</label>
            <mixed-citation id="d19e254" publication-type="other">
Blask DE, Brainard GC, Dauchy RT, Hanifin JP, Davidson LK,
Krause JA, Sauer LA, Rivera-Bermudez MA, Dubocovich ML,
Jasser SA, Lynch DT, Rollag MD, Zalatan F (2005) Melatonin-
depleted blood from premenopausal women exposed to light at
night stimulates growth of human breast cancer xenografts in
nude rats. Cancer Res 65:11174-11184</mixed-citation>
         </ref>
         <ref id="d19e277a1310">
            <label>4</label>
            <mixed-citation id="d19e284" publication-type="other">
Srinivasan V, Spence DW, Pandi-Perumal SR, Trakht I, Esquif-
ino AI, Cardinali DP, Maestroni GJ (2008) Melatonin, environ-
mental light, and breast cancer. Breast Cancer Res Treat
108:339-350</mixed-citation>
         </ref>
         <ref id="d19e301a1310">
            <label>5</label>
            <mixed-citation id="d19e308" publication-type="other">
Haim A, Shanas U, Zubidad AS, Scantelbry M (2005) Season-
ality and seasons out of time-The thermoregulatory effects of
light interference. Chronobiol Int 22:57-64</mixed-citation>
         </ref>
         <ref id="d19e321a1310">
            <label>6</label>
            <mixed-citation id="d19e328" publication-type="other">
Nelson RJ (2004) seasonal immune function and sickness
responses. Trends Immunol 25:187-192</mixed-citation>
         </ref>
         <ref id="d19e338a1310">
            <label>7</label>
            <mixed-citation id="d19e345" publication-type="other">
Stevens RG, Blask DE, Brainard GC, Hansen J, Lockley SW,
Provencio I, Rea MS, Reinlib L (2007) Meeting report: the role of
environmental lighting and circadian disruption in cancer and
other diseases. Environ Health Perspect 115:1357-1362</mixed-citation>
         </ref>
         <ref id="d19e361a1310">
            <label>8</label>
            <mixed-citation id="d19e368" publication-type="other">
Stevens RG, Rea MS (2001) Light in the built environment:
potential role of circadian disruption in endocrine disruption and
breast cancer. Cancer Causes Control 12:279-287</mixed-citation>
         </ref>
         <ref id="d19e381a1310">
            <label>9</label>
            <mixed-citation id="d19e388" publication-type="other">
Davis S, Mirick DK, Stevens RG (2001) Night shift work, light
at night, and risk of breast cancer. J Natl Cancer Inst 93:
1557-1562</mixed-citation>
         </ref>
         <ref id="d19e401a1310">
            <label>10</label>
            <mixed-citation id="d19e408" publication-type="other">
Hansen J (2001) Increased breast cancer risk among women who
work predominantly at night. Epidemiology 12:74-77</mixed-citation>
         </ref>
         <ref id="d19e419a1310">
            <label>11</label>
            <mixed-citation id="d19e426" publication-type="other">
Lie JA, Roessink J, Kjaerheim K (2006) Breast cancer and night
work among Norwegian nurses. Cancer Causes Control 17:39-44</mixed-citation>
         </ref>
         <ref id="d19e436a1310">
            <label>12</label>
            <mixed-citation id="d19e443" publication-type="other">
Schernhammer ES, Kroenke CH, Laden F, Hankinson SE (2006)
Night work and risk of breast cancer. Epidemiology 17:108-111</mixed-citation>
         </ref>
         <ref id="d19e453a1310">
            <label>13</label>
            <mixed-citation id="d19e460" publication-type="other">
Hahn RA (1991) Profound bilateral blindness and the incidence
of breast cancer. Epidemiology 2:208-210</mixed-citation>
         </ref>
         <ref id="d19e470a1310">
            <label>14</label>
            <mixed-citation id="d19e477" publication-type="other">
Kliukiene J, Tynes T, Andersen A (2001) Risk of breast cancer
among Norwegian women with visual impairment. Br J Cancer
84:397-399</mixed-citation>
         </ref>
         <ref id="d19e490a1310">
            <label>15</label>
            <mixed-citation id="d19e497" publication-type="other">
Verkasalo PK, Pukkala E, Stevens RG, Ojamo M, Rudanko SL
(1999) Inverse association between breast cancer incidence and
degree of visual impairment in Finland. Br J Cancer
80:1459-1460</mixed-citation>
         </ref>
         <ref id="d19e513a1310">
            <label>16</label>
            <mixed-citation id="d19e520" publication-type="other">
Pinheiro SP, Schernhammer ES, Tworoger SS, Michels KB
(2006) A prospective study on habitual duration of sleep and
incidence of breast cancer in a large cohort of women. Cancer
Res 66:5521-5525</mixed-citation>
         </ref>
         <ref id="d19e537a1310">
            <label>17</label>
            <mixed-citation id="d19e544" publication-type="other">
Verkasalo PK, Lillberg K, Stevens RG, Hublin C, Partinen M,
Koskenvuo M, Kaprio J (2005) Sleep duration and breast cancer:
a prospective cohort study. Cancer Res 65:9595-9600</mixed-citation>
         </ref>
         <ref id="d19e557a1310">
            <label>18</label>
            <mixed-citation id="d19e564" publication-type="other">
Kakizaki M, Kuriyama S, Sone T, Ohmori-Matsuda K, Hozawa
A, Nakaya N, Fukudo S, Tsuji I (2008) Sleep duration and the
risk of breast cancer: the Ohsaki Cohort Study. Br J Cancer
99:1502-1505</mixed-citation>
         </ref>
         <ref id="d19e580a1310">
            <label>19</label>
            <mixed-citation id="d19e587" publication-type="other">
Wu AH, Wang R, Koh W-P, Stanczyk FZ, Lee H-P, Yu MC
(2008) Sleep duration, melatonin and breast cancer among Chi-
nese women in Singapore. Carcinogenesis 29:1244-1248</mixed-citation>
         </ref>
         <ref id="d19e600a1310">
            <label>20</label>
            <mixed-citation id="d19e607" publication-type="other">
Straif K, Baan R, Grosse Y, Secretan B, El Ghissassi F, Bouvard
V, Altieri A, Benbrahim-Tallaa L, Cogliano V (2007) Carcino-
genicity of shift-work, painting, and fire-fighting. Lancet Oncol
12:1065-1066</mixed-citation>
         </ref>
         <ref id="d19e623a1310">
            <label>21</label>
            <mixed-citation id="d19e630" publication-type="other">
ACS (2007) Global cancer facts and figures</mixed-citation>
         </ref>
         <ref id="d19e637a1310">
            <label>22</label>
            <mixed-citation id="d19e644" publication-type="other">
Parkin DM, Bray F, Ferlay J, Pisani P (2001) Estimating the
world cancer burden: Globocan 2000. Int J Cancer 94:153-156</mixed-citation>
         </ref>
         <ref id="d19e655a1310">
            <label>23</label>
            <mixed-citation id="d19e662" publication-type="other">
Parkin DM, Bray F, Ferlay J, Pisani P (2005) Global cancer
statistics, 2002. CA Cancer J Clin 55:74-108</mixed-citation>
         </ref>
         <ref id="d19e672a1310">
            <label>24</label>
            <mixed-citation id="d19e679" publication-type="other">
Parkin DM, Bray FI, Devesa SS (2001) Cancer burden in the year
2000. The elobal picture. Eur J Cancer 37(Sudd1 8):S4-S66</mixed-citation>
         </ref>
         <ref id="d19e689a1310">
            <label>25</label>
            <mixed-citation id="d19e696" publication-type="other">
Kloog I, Haim A, Stevens RG, Portnov BA (2009) Global co-
distribution of light at night (LAN) and cancers of prostate, colon,
and lung in men. Chronobiol Int 26:108-125</mixed-citation>
         </ref>
         <ref id="d19e709a1310">
            <label>26</label>
            <mixed-citation id="d19e716" publication-type="other">
Kloog I, Haim A, Stevens RG, Barchana M, Portnov BA (2008)
Light at Night Co-distributes with Incident Breast but not Lung
Cancer in the Female Population of Israel. Chronobiol Int
25:65-81</mixed-citation>
         </ref>
         <ref id="d19e732a1310">
            <label>27</label>
            <mixed-citation id="d19e739" publication-type="other">
de Sanjose S, Diaz M, Castellsague X, Clifford G, Bruni L,
Muñoz N, Bosch FX (2007) Worldwide prevalence and genotype
distribution of cervical human papillomavirus DNA in women
with normal cytology: a meta-analysis. Lancet Infect Dis
7:453-459</mixed-citation>
         </ref>
         <ref id="d19e758a1310">
            <label>28</label>
            <mixed-citation id="d19e765" publication-type="other">
Boyle P, Ferlay J (2005) Cancer incidence and mortality in
Europe, 2004. Ann Oncol 16:481</mixed-citation>
         </ref>
         <ref id="d19e776a1310">
            <label>29</label>
            <mixed-citation id="d19e783" publication-type="other">
Hulshof KF, Brussaard JH, Kruizinga AG, Telman J, Lowik MR
(2003) Socio-economic status, dietary intake and 10 y trends: the
Dutch National Food Consumption Survey. Eur J Clin Nutr
57:128-137</mixed-citation>
         </ref>
         <ref id="d19e799a1310">
            <label>30</label>
            <mixed-citation id="d19e806" publication-type="other">
Hulshof KF, Lowik MR, Kok FJ, Wedel M, Brants HA, Hermus
RJ, ten Hoor F (1991) Diet and other life-style factors in high and
low socio-economic groups (Dutch Nutrition Surveillance Sys-
tem). Eur J Clin Nutr 45:441—450</mixed-citation>
         </ref>
         <ref id="d19e822a1310">
            <label>31</label>
            <mixed-citation id="d19e829" publication-type="other">
Bray F, McCarron P, Parkin DM (2004) The changing global
patterns of female breast cancer incidence and mortality. Breast
Cancer Res 6:229-239</mixed-citation>
         </ref>
         <ref id="d19e842a1310">
            <label>32</label>
            <mixed-citation id="d19e849" publication-type="other">
Han X, Naeher LP (2006) A review of traffic-related air pollution
exposure assessment studies in the developing world. Environ Int
32:106-120</mixed-citation>
         </ref>
         <ref id="d19e862a1310">
            <label>33</label>
            <mixed-citation id="d19e869" publication-type="other">
Volzke H, Neuhauser H, Moebus S, Baumert J, Berger K, Stang
A, Ellert U, Werner A, Döring A (2006) Urban-rural disparities in
smoking behaviour in Germany. BMC Public Health 6:146</mixed-citation>
         </ref>
         <ref id="d19e882a1310">
            <label>34</label>
            <mixed-citation id="d19e889" publication-type="other">
Gram-Hansenn K, Petersen NK. Diffrenet everday lives-Differnt
patterns of electrical use. In: ACEEE Summer Study on Energy
Efficiency in Buildings 2004. Pacific Grove, California</mixed-citation>
         </ref>
         <ref id="d19e903a1310">
            <label>35</label>
            <mixed-citation id="d19e910" publication-type="other">
Jumbe BLC (2004) Cointegration and causality between elec-
tricity consumption and GDP: empirical evidence from Malawi.
Energy Economics 26:61-68</mixed-citation>
         </ref>
         <ref id="d19e923a1310">
            <label>36</label>
            <mixed-citation id="d19e930" publication-type="other">
Kelsey JL, Gammon MD (1990) The epidemiology of breast
cancer. CA: Cancer J Clin 41:146-165</mixed-citation>
         </ref>
         <ref id="d19e940a1310">
            <label>37</label>
            <mixed-citation id="d19e947" publication-type="other">
CIA (2006) "CIA World Factbook." Retrieved 2006, 2006, from
httn://www. cia. pov/index. html</mixed-citation>
         </ref>
         <ref id="d19e957a1310">
            <label>38</label>
            <mixed-citation id="d19e964" publication-type="other">
ESRI (2007) ARCGIS. In. 9.2 ed: ESRI</mixed-citation>
         </ref>
         <ref id="d19e971a1310">
            <label>39</label>
            <mixed-citation id="d19e978" publication-type="other">
DMSP (2004) DMSP Nighttime lights data download</mixed-citation>
         </ref>
         <ref id="d19e985a1310">
            <label>40</label>
            <mixed-citation id="d19e992" publication-type="other">
Banerjee S, Wall MM, Carlin BP (2003) Frailty modeling for
spatially correlated survival data, with application to infant
mortality in Minnesota. Biostatistics 4:123-142</mixed-citation>
         </ref>
         <ref id="d19e1006a1310">
            <label>41</label>
            <mixed-citation id="d19e1013" publication-type="other">
Krieger N, Chen JT, Waterman PD, Soobader MJ, Subramanian
SV, Carson R (2002) Geocoding and monitoring of US socio-
economic inequalities in mortality and cancer incidence: does the
choice of area-based measure and geographic level matter?: the
Public Health Disparities Geocoding Project. Am J Epidemiol
156:471—482</mixed-citation>
         </ref>
         <ref id="d19e1036a1310">
            <label>42</label>
            <mixed-citation id="d19e1043" publication-type="other">
Maheswaran R, Strachan DP, Dodgeon B, Best NG (2002) A
population-based case-control study for examining early life
influences on geographical variation in adult mortality in England
and Wales using stomach cancer and stroke as examples. Int J
Epidemiol 31:375-382</mixed-citation>
         </ref>
         <ref id="d19e1062a1310">
            <label>43</label>
            <mixed-citation id="d19e1069" publication-type="other">
O'Leary ES, Vena JE, Freudenheim JL, Brasure J (2004) Pesti-
cide exposure and risk of breast cancer: a nested case-control
study of residentially stable women living on Long Island.
Environ Res 94:134-144</mixed-citation>
         </ref>
         <ref id="d19e1085a1310">
            <label>44</label>
            <mixed-citation id="d19e1092" publication-type="other">
Scott D, Curtis B, Twumasi FO (2002) Towards the creation of a
health information system for cancer in KwaZulu-Natal, South
Africa. Health Place 8:237-249</mixed-citation>
         </ref>
         <ref id="d19e1105a1310">
            <label>45</label>
            <mixed-citation id="d19e1112" publication-type="other">
Minami M (2000) ESRI. Using ArcMap: GIS. ESRI, Redlands,
California</mixed-citation>
         </ref>
         <ref id="d19e1122a1310">
            <label>46</label>
            <mixed-citation id="d19e1129" publication-type="other">
Kloog I, Haim A, Portnov BA (2009) Using kernel density
function as an urban analysis tool: Investigating the association
between nightlight exposure and the incidence of breast cancer in
Haifa, Israel. Comput Environ Urban Syst 33:55-63</mixed-citation>
         </ref>
         <ref id="d19e1146a1310">
            <label>47</label>
            <mixed-citation id="d19e1153" publication-type="other">
Kinnear PR, Gray CD (2007) SPSS 15 Made Simple. Psychology
Press, Philadelphia, PA</mixed-citation>
         </ref>
         <ref id="d19e1163a1310">
            <label>48</label>
            <mixed-citation id="d19e1170" publication-type="other">
Anselin L (1999) Spatial Econometrics. Bruton Center, School of
Social Sciences,University of Texas at Dallas, Dallas</mixed-citation>
         </ref>
         <ref id="d19e1180a1310">
            <label>49</label>
            <mixed-citation id="d19e1187" publication-type="other">
Anselin L, Syabri I, Kho Y (2005) GeoDa: An Introduction to
Spatial Data Analysis. Geogr Ana006C 38:5-22</mixed-citation>
         </ref>
         <ref id="d19e1197a1310">
            <label>50</label>
            <mixed-citation id="d19e1204" publication-type="other">
Jenks G (1967) The data model concept in statistical mapping. Int
YearbCartogr 7:186-190</mixed-citation>
         </ref>
         <ref id="d19e1214a1310">
            <label>51</label>
            <mixed-citation id="d19e1221" publication-type="other">
Bradley CJ, Given CW, Roberts C (2002) Race, socioeconomic
status, and breast cancer treatment and survival. J Nati Cancer
Inst 94:490-496</mixed-citation>
         </ref>
         <ref id="d19e1234a1310">
            <label>52</label>
            <mixed-citation id="d19e1241" publication-type="other">
Madison T, Schottenfeld D, James SA, Schwartz AG, Gruber SB
(2004) Endometrial cancer: socioeconomic status and racial/eth-
nic differences in stage at diagnosis, treatment, and survival. Am
J Public Health 94:2104-2111</mixed-citation>
         </ref>
         <ref id="d19e1258a1310">
            <label>53</label>
            <mixed-citation id="d19e1265" publication-type="other">
Wells BL, Horm JW (1992) Stage at diagnosis in breast cancer:
race and socioeconomic factors. Am J Public Health 82:
1383-1385</mixed-citation>
         </ref>
         <ref id="d19e1278a1310">
            <label>54</label>
            <mixed-citation id="d19e1285" publication-type="other">
Adler N, Boyce T, Chesney M, Cohen S, Folkman S, Kahn RL,
Syme SL (1994) Socioeconomic status and health: The challenge
of the gradient. Am Psychol 49:15-24</mixed-citation>
         </ref>
         <ref id="d19e1298a1310">
            <label>55</label>
            <mixed-citation id="d19e1305" publication-type="other">
Jha P, Peto R, Zatonski W, Boreham J, Jarvis MJ, Lopez AD
(2006) Social inequalities in male mortality, and in male mor-
tality from smoking: indirect estimation from national death rates
in England and Wales, Poland, and North America. Lancet
368:367-370</mixed-citation>
         </ref>
         <ref id="d19e1324a1310">
            <label>56</label>
            <mixed-citation id="d19e1331" publication-type="other">
Parkin DM, Wabinga H, Nambooze S (2001) Completeness in an
African cancer registry. Cancer Causes Control 12:147-152</mixed-citation>
         </ref>
         <ref id="d19e1341a1310">
            <label>57</label>
            <mixed-citation id="d19e1348" publication-type="other">
Curado MP, Voti L, Sortino-Rachou AM (2009) Cancer regis-
tration data and quality indicators in low and middle income
countries: their interpretation and potential use for the improve-
ment of cancer care. Cancer Causes Control 20:751-756</mixed-citation>
         </ref>
         <ref id="d19e1364a1310">
            <label>58</label>
            <mixed-citation id="d19e1371" publication-type="other">
Schernhammer ES, Laden F, Speizer FÉ et al (2003) Night-Shift
work and risk of colorectal cancer in the Nurses' Health Study.
J Nati Cancer Inst 95:825-828</mixed-citation>
         </ref>
         <ref id="d19e1385a1310">
            <label>59</label>
            <mixed-citation id="d19e1392" publication-type="other">
Hoogerwerf WA, Hellmich HL, Cornélisson G et al (2007) Clock
gene expression in the murine gastrointestinal tract: endogenous
rhythmicity and effects of a feeding regimen. Gastroenterology
133:1250-1260</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
