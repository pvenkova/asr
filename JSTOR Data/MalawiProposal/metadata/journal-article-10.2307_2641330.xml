<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">ecolappl</journal-id>
         <journal-id journal-id-type="jstor">j100864</journal-id>
         <journal-title-group>
            <journal-title>Ecological Applications</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Ecological Society of America</publisher-name>
         </publisher>
         <issn pub-type="ppub">10510761</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="doi">10.2307/2641330</article-id>
         <article-categories>
            <subj-group>
               <subject>Issues in Ecosystem Management</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>On the Relevance of Nonequilibrium Concepts to Arid and Semiarid Grazing Systems</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>A. W.</given-names>
                  <surname>Illius</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>T. G.</given-names>
                  <surname>O'Connor</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>8</month>
            <year>1999</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">9</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id>i325033</issue-id>
         <fpage>798</fpage>
         <lpage>813</lpage>
         <page-range>798-813</page-range>
         <permissions>
            <copyright-statement>Copyright 1999 The Ecological Society of America</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/2641330"/>
         <abstract>
            <p>The dynamics of arid and semiarid grazing systems are prone to the effects of highly variable rainfall, with droughts causing frequent episodic mortality in herbivore populations. This has led to the suggestion that they are nonequilibrium systems, in which animal impacts on plants are strongly attenuated or absent. We examine the utility and appropriateness of nonequilibrium concepts for understanding ecosystem processes in Africanrangeland, attempt to distinguish disequilibrium from nonequilibrium, and argue that such concepts do not justify the view that herbivory has little impact in climatically variable systems. We present evidence for an alternative view of African rangeland function. We argue that, despite the apparent lack of equilibrium, animal numbers are regulated in a density-dependent manner by the limited forage available in key resource areas, which are utilized in the dry season. This model asserts that strong equilibrial forces exist over a limited part of the system, with the animal population being virtually uncoupled from resources elsewhere in the system. Spatially and temporally, the whole system is heterogeneous in the strength of the forces tending to equilibrium, these diminishing with distance from watering and key resource areas and during the wet season. We argue that wet-season range is more heavily utilized by animal populations sustained by key resource areas than would apply in the absence of key resources, and that uncoupling of the animal population from vegetation carries an increased risk of degradation. Droughts may impose intense and localized defoliation on vegetation, and this may result in altered species composition, reduced rain-use efficiency, soil erosion, and loss of productive potential. Rather than ignoring degradation, policy-makers and ecologists should seek to identify the characteristics of grazing systems that predispose some systems toward degradation, while others appear to be resistant. Development policies should focus on the spatial heterogeneity insusceptibility to grazing impacts and on preserving the productive capacity of key resource areas.</p>
         </abstract>
         <kwd-group>
            <kwd>Climatic Variability</kwd>
            <kwd>Consumer-Resource Dynamics</kwd>
            <kwd>Degradation</kwd>
            <kwd>Grazing</kwd>
            <kwd>Livestock</kwd>
            <kwd>Nonequilibrium Systems</kwd>
            <kwd>Pastoralism</kwd>
            <kwd>Plant-Herbivore Relations</kwd>
            <kwd>Rangeland</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
</article>
