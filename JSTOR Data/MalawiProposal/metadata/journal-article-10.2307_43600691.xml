<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">procbiolscie</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100837</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Proceedings: Biological Sciences</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>The Royal Society</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">09628452</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">43600691</article-id>
         <title-group>
            <article-title>Adaptive value of a predatory mouth-form in a dimorphic nematode</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Vahan</given-names>
                  <surname>Serobyan</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Erik J.</given-names>
                  <surname>Ragsdale</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Ralf J.</given-names>
                  <surname>Sommer</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>22</day>
            <month>9</month>
            <year>2014</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">281</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1791</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40143572</issue-id>
         <fpage>1</fpage>
         <lpage>9</lpage>
         <permissions>
            <copyright-statement>Copyright © 2014 The Royal Society</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/43600691"/>
         <abstract>
            <p>Polyphenisms can be adaptations to environments that are heterogeneous in space and time, but to persist they require conditional-specific advantages. The nematode Pristionchus pacificus is a facultative predator that displays an evolutionarily conserved polyphenism of its mouthparts. During development, P. pacificus irreversibly executes either a eurystomatous (Eu) or stenostomatous (St) mouth-form, which differ in the shape and number of movable teeth. The Eu form, which has an additional tooth, is more complex than the St form and is thus more highly derived relative to species lacking teeth. Here, we investigate a putative fitness trade-off for the alternative feeding-structures of P. pacificus. We show that the complex Eu form confers a greater ability to kill prey. When adults were provided with a prey diet, Eu nematodes exhibited greater fitness than St nematodes by several measures, including longevity, offspring survival and fecundity when followed by bacterial feeding. However, the two mouthforms had similar fecundity when fed ad libitum on bacteria, a condition that would confer benefit on the more rapidly developing St form. Thus, the two forms show conditional fitness advantages in different environments. This study provides, to our knowledge, the first functional context for dimorphism in a model for the genetics of plasticity.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d1196e186a1310">
            <label>1</label>
            <mixed-citation id="d1196e193" publication-type="other">
West-Eberhard MJ. 2003 Developmental
plasticity and evolution. Oxford, UK: Oxford
University Press.</mixed-citation>
         </ref>
         <ref id="d1196e206a1310">
            <label>2</label>
            <mixed-citation id="d1196e213" publication-type="other">
West-Eberhard MJ. 2005 Developmental plasticity
and the origin of species differences. Proc. Natl
Acad. Sci. USA 102, 6543-6549. (doi:10.1073/pnas.
0501844102)</mixed-citation>
         </ref>
         <ref id="d1196e229a1310">
            <label>3</label>
            <mixed-citation id="d1196e236" publication-type="other">
Moran NA. 1992 The evolutionary maintenance of
alternative phenotypes. Am. Nat. 139, 971-989.
(doi:10.1086/285369)</mixed-citation>
         </ref>
         <ref id="d1196e249a1310">
            <label>4</label>
            <mixed-citation id="d1196e256" publication-type="other">
Brakefield PM, Reitsma N. 1991 Phenotypic
plasticity, seasonal climate and the population of
Bicyclus butterflies (Satyridae) in Malawi. Ecol.
Entomol. 16, 291-303. (doi:10.1111/j.1365-2311.
1991.tb00220.x)</mixed-citation>
         </ref>
         <ref id="d1196e276a1310">
            <label>5</label>
            <mixed-citation id="d1196e283" publication-type="other">
Stibor H. 1992 Predator induced life-history shifts in
a freshwater dadoceran. Oecologia 92, 162-165.
(doi:10.1007/BF00317358)</mixed-citation>
         </ref>
         <ref id="d1196e296a1310">
            <label>6</label>
            <mixed-citation id="d1196e305" publication-type="other">
Pfennig DW. 1990 The adaptive significance of an
envrionmentally-cued developmental switch in an
anuran tadpole. Oecologia 85, 101-107. (doi:10.
1007/BF00317349)</mixed-citation>
         </ref>
         <ref id="d1196e321a1310">
            <label>7</label>
            <mixed-citation id="d1196e328" publication-type="other">
Ragsdale EJ, Müller MR, Rödelsperger C, Sommer RJ.
2013 A developmental switch coupled to the evolution
of plasticity acts through a sulfatase. Cell 155,
922-933. (doi:10.1016/j.cell.2013.09.054)</mixed-citation>
         </ref>
         <ref id="d1196e344a1310">
            <label>8</label>
            <mixed-citation id="d1196e351" publication-type="other">
West-Eberhard MJ. 1989 Phenotypic plasticity and
the origins of diversity. Annu. Rev. Ecol. Syst. 20,
249-278. (doi:10.1146/annurev.es.20.110189.
001341)</mixed-citation>
         </ref>
         <ref id="d1196e367a1310">
            <label>9</label>
            <mixed-citation id="d1196e374" publication-type="other">
Smith TB, Skúlason S. 1996 Evolutionary significance
of resource polymorphisms in fishes, amphibians,
and birds. Annu. Rev. Ecol. Syst. 27, 111-133.
(doi:10.1146/annurev.ecolsys.27.1.111)</mixed-citation>
         </ref>
         <ref id="d1196e390a1310">
            <label>10</label>
            <mixed-citation id="d1196e397" publication-type="other">
Waddington CH. 1953 Genetic assimilation of an
acquired character. Evolution 7, 118-126. (doi:10.
2307/2405747)</mixed-citation>
         </ref>
         <ref id="d1196e411a1310">
            <label>11</label>
            <mixed-citation id="d1196e418" publication-type="other">
Suzuki Y, Nijhout HF. 2006 Evolution of a
polyphenism by genetic accommodation.
Science 311, 650-652. (doi:10.1126/science.
1118888)</mixed-citation>
         </ref>
         <ref id="d1196e434a1310">
            <label>12</label>
            <mixed-citation id="d1196e441" publication-type="other">
Gomez-Mestre I, Buchholz DR. 2006 Developmental
plasticity mirrors differences among taxa in
spadefoot toads linking plasticity and diversity. Proc.
Natl Acad. Sci. USA 103, 19021-19026. (doi:10.
1073/pnas.0603562103)</mixed-citation>
         </ref>
         <ref id="d1196e460a1310">
            <label>13</label>
            <mixed-citation id="d1196e467" publication-type="other">
Ledon-Rettig CC, Pfennig DW, Nascone-Yoder N.
2008 Ancestral variation and the potential for
genetic accommodation in larval amphibians:
implications for the evolution of novel feeding
strategies. Evol. Dev. 10, 316-325. (doi:10.1111/j.
1525-142X.2008.00240.X)</mixed-citation>
         </ref>
         <ref id="d1196e490a1310">
            <label>14</label>
            <mixed-citation id="d1196e497" publication-type="other">
Pfennig DW, McGee M. 2010 Resource polyphenism
increases species richness: a test of the hypothesis.
Phil. Trans. R. Soc. B 365, 577-591. (doi:10.1098/
rstb.2009.0244)</mixed-citation>
         </ref>
         <ref id="d1196e513a1310">
            <label>15</label>
            <mixed-citation id="d1196e520" publication-type="other">
Sommer RJ, McGaughran A. 2013 The nematode
Pristionchus pacificus as a model system for
integrative studies in evolutionary biology.
Mol. Ecol. 22, 2380-2393. (doi:10.1111/mec.
12286)</mixed-citation>
         </ref>
         <ref id="d1196e539a1310">
            <label>16</label>
            <mixed-citation id="d1196e546" publication-type="other">
Kiontke K, Fitch DHA. 2010 Phenotypic plasticity:
different teeth for different feasts. Curr. Biol. 20,
R710-R712. (doi:10.1016/j.cub.2010.07.009)</mixed-citation>
         </ref>
         <ref id="d1196e560a1310">
            <label>17</label>
            <mixed-citation id="d1196e567" publication-type="other">
Sudhaus W, Fürst von Lieven A. 2003
A phylogenetic classification and catalogue of the
Diplogastridae (Secernentea, Nematoda).
J. Nematode Morphol. Syst. 6, 43-90.</mixed-citation>
         </ref>
         <ref id="d1196e583a1310">
            <label>18</label>
            <mixed-citation id="d1196e590" publication-type="other">
Fürst von Lieven A, Sudhaus W. 2000 Comparative
and functional morphology of the bucal cavity
of Diplogastrina (Nematoda) and a first outline
of the phylogeny of this taxon. J. Zool. Syst. Evol.
Res. 38, 37-63. (doi:10.1046/j.1439-0469.2000.
381125.x)</mixed-citation>
         </ref>
         <ref id="d1196e613a1310">
            <label>19</label>
            <mixed-citation id="d1196e620" publication-type="other">
Bento G, Ogawa A, Sommer RJ. 2010 Co-option of
the hormone-signalling module dafachronic
acid-DAF-12 in nematode evolution. Nature 466,
494-497. (doi:10.1038/nature09164)</mixed-citation>
         </ref>
         <ref id="d1196e636a1310">
            <label>20</label>
            <mixed-citation id="d1196e643" publication-type="other">
Herrmann M, Mayer WE, Sommer RJ. 2006
Nematodes of the genus Pristionchus are
closely associated with scarab beetles and
the Colorado potato beetle in Western Europe.
Zoology 109, 96-108. (doi:10.1016/j.zool.2006.03.
001)</mixed-citation>
         </ref>
         <ref id="d1196e666a1310">
            <label>21</label>
            <mixed-citation id="d1196e673" publication-type="other">
Herrmann M, Mayer WE, Hong RL, Kienle S,
Minasaki R, Sommer RJ. 2007 The nematode
Pristionchus pacificus (Nematoda: Diplogastridae)
is associated with the Oriental beetle Exomala
orientalis (Coleoptera: Scarabaeidae) in Japan.
Zool. Sci. 24, 883-889. (doi:10.2108/zsj.24.883)</mixed-citation>
         </ref>
         <ref id="d1196e696a1310">
            <label>22</label>
            <mixed-citation id="d1196e705" publication-type="other">
Rae R, Riebesell M, Dinckelacker I, Wang Q,
Herrmann M, Weller AM, Dieterich C, Sommer RJ.
2008 Isolation of naturally associated bacteria of
necromenic Pristionchus nematodes and fitness
consequences. J. Exp. Biol. 211, 1927-1936.
(doi:10.1242/jeb.014944)</mixed-citation>
         </ref>
         <ref id="d1196e729a1310">
            <label>23</label>
            <mixed-citation id="d1196e736" publication-type="other">
Weller AM, Mayer WE, Rae R, Sommer RJ. 2010
Quantitative assessment of the nematode fauna
present on Geotrupes dung beetles reveals
species-rich communities with a heterogeneous
distribution. J. Parasitol. 96, 525-531. (doi:10.
1645/GE-2319.1)</mixed-citation>
         </ref>
         <ref id="d1196e759a1310">
            <label>24</label>
            <mixed-citation id="d1196e766" publication-type="other">
Serobyan V, Ragsdale EJ, Müller MR, Sommer RJ.
2013 Feeding plasticity in the nematode
Pristionchus pacificus is influenced by sex and
social context and is linked to developmental
speed. Evol. Dev. 15, 161-170. (doi:10.1111/ede.
12030)</mixed-citation>
         </ref>
         <ref id="d1196e789a1310">
            <label>25</label>
            <mixed-citation id="d1196e796" publication-type="other">
Bose N, Ogawa A, von Reuss SH, Yim JJ, Ragsdale
EJ, Sommer RJ, Schroeder FC. 2012 Complex small-
molecule architectures regulate phenotypic plasticity
in a nematode. Angew. Chem. 51, 12438-12443.
(doi:10.1002/anie.201206797)</mixed-citation>
         </ref>
         <ref id="d1196e815a1310">
            <label>26</label>
            <mixed-citation id="d1196e822" publication-type="other">
Maynard Smith J. 1982 Evolution and the theory
of games. New York, NY: Cambridge University
Press.</mixed-citation>
         </ref>
         <ref id="d1196e835a1310">
            <label>27</label>
            <mixed-citation id="d1196e842" publication-type="other">
Sommer RJ, Carta LK, Kim SY, Sternberg PW. 1996
Morphological, genetic and molecular description of
Pristìonchus pacificus n. sp. (Nematoda:
Neodiplogastridae). Fund. Appl. Nematol. 19, 511-521.</mixed-citation>
         </ref>
         <ref id="d1196e858a1310">
            <label>28</label>
            <mixed-citation id="d1196e865" publication-type="other">
Bongers T. 1999 The Maturity Index, the evolution
of nematode life-history traits, adaptive radiation
and cp-scaling. Plant Soil 212, 13-22. (doi:10.
1023/A:1004571900425)</mixed-citation>
         </ref>
         <ref id="d1196e882a1310">
            <label>29</label>
            <mixed-citation id="d1196e889" publication-type="other">
Scholze VS, Sudhaus W. 2011 A pictorial key to
current genus groups of 'Rhabditidae'. J. Nematode
Morphol. Syst. 14, 105-112.</mixed-citation>
         </ref>
         <ref id="d1196e902a1310">
            <label>30</label>
            <mixed-citation id="d1196e909" publication-type="other">
Sudhaus W. 2011 Phylogenetic systemisation and
catalogue of paraphyletic 'Rhabditidae'
(Secernentea, Nematoda). J. Nematode Morphol.
Syst. 14, 113-178.</mixed-citation>
         </ref>
         <ref id="d1196e925a1310">
            <label>31</label>
            <mixed-citation id="d1196e932" publication-type="other">
Choe A, von Reuss SH, Kogan D, Gasser RB, Platzer
EG, Schroeder FC, Sternberg PW. 2012 Ascaroside
signaling is widely conserved among nematodes.
Curr. Biol. 22, 772-780. (doi:10.1016/j.cub.2012.
03.024)</mixed-citation>
         </ref>
         <ref id="d1196e951a1310">
            <label>32</label>
            <mixed-citation id="d1196e958" publication-type="other">
Sulston J, Hodgkin J. 1988 Methods. In The
nematode Caenorhabditis elegans (ed. WB Wood),
pp. 587-606. Cold Spring Harbor, NY: Cold Spring
Harbor Laboratory Press.</mixed-citation>
         </ref>
         <ref id="d1196e974a1310">
            <label>33</label>
            <mixed-citation id="d1196e981" publication-type="other">
Rae R, latsenko I, Witte H, Sommer RJ. 2010 A
subset of naturally isolated Bacillus strains show
extreme virulence to the free-living nematodes
Caenorhabditis elegans and Pristionchus pacificus.
Environ. Microbiol. 12, 3007-3021. (doi:10.1111/j.
1462-2920.2010.02278.x)</mixed-citation>
         </ref>
         <ref id="d1196e1004a1310">
            <label>34</label>
            <mixed-citation id="d1196e1011" publication-type="other">
latsenko I, Boichenko I, Sommer RJ. 2013 Bacillus
thuringiensis DB27 produces two novel toxins,
Cry21Fa1 and Cry21Ha1, which act synergistically
against nematodes. Appl. Environ. Micrbiol. 80,
3266-3275. (doi:10.1128/AEM.00464-14)</mixed-citation>
         </ref>
         <ref id="d1196e1031a1310">
            <label>35</label>
            <mixed-citation id="d1196e1038" publication-type="other">
Fürst von Lieven A. 2002 Functional
morphology, origin and phylogenetic implications of
the feeding mechanisms of Tylopharynx foetida
(Nematoda: Diplogastrina). Russ. J. Nematol. 10,
11-23.</mixed-citation>
         </ref>
         <ref id="d1196e1057a1310">
            <label>36</label>
            <mixed-citation id="d1196e1064" publication-type="other">
Bumbarger D, Riebesell M, Rödelsperger C, Sommer
RJ. 2013 System-wide rewiring underlies behavioral
difference in predatory and bacterial-feeding
nematodes. Cell 152, 109-119. (doi:10.1016/j.cell.
2012.12.013)</mixed-citation>
         </ref>
         <ref id="d1196e1083a1310">
            <label>37</label>
            <mixed-citation id="d1196e1090" publication-type="other">
Angelo G, Van Gilst MR. 2009 Starvation protects
germline stem cells and extends reproductive
longevity in C. elegans. Science 326, 954-958.
(doi:10.1126/science.1178343)</mixed-citation>
         </ref>
         <ref id="d1196e1106a1310">
            <label>38</label>
            <mixed-citation id="d1196e1113" publication-type="other">
Seidel HS, Kimble J. 2011 The oogenic
germline starvation response in C. elegans.
PLoS ONE 6, e28074. (doi:10.1371/joumal.pone.
0028074)</mixed-citation>
         </ref>
         <ref id="d1196e1129a1310">
            <label>39</label>
            <mixed-citation id="d1196e1136" publication-type="other">
Johnson TE, Mitchell DH, Kline S, Kemal R, Foy J.
1984 Arresting development arrests aging in the
nematode Caenorhabditis elegans. Mech. Ageing
Dev. 28, 23-40. (doi:10.1016/0047-6374
(84)90150-7)</mixed-citation>
         </ref>
         <ref id="d1196e1155a1310">
            <label>40</label>
            <mixed-citation id="d1196e1162" publication-type="other">
Baugh LR, Sternberg PW. 2006 DAF-16/FOXO
regulates transcription of cki-1/Cip/Kip and
repression of lin-4 during C. elegans L1 arrest.
Curr. Biol. 16, 780-785. (doi:10.1016/j.cub.2006.
03.021)</mixed-citation>
         </ref>
         <ref id="d1196e1182a1310">
            <label>41</label>
            <mixed-citation id="d1196e1189" publication-type="other">
Pfennig DW. 1992 Polyphenism in spadefoot toad
tadpoles as a locally adjusted evolutionary stable
strategy. Evolution 46, 1408-1420. (doi:10.2307/
2409946)</mixed-citation>
         </ref>
         <ref id="d1196e1205a1310">
            <label>42</label>
            <mixed-citation id="d1196e1212" publication-type="other">
DeWitt TJ, Sih A, Sloan Wilson D. 1998 Costs
and limits of phenotypic plasticity. Trends Ecol.
Evol. 13, 77-81. (doi:10.1016/S0169-5347(97)
01274-3)</mixed-citation>
         </ref>
         <ref id="d1196e1228a1310">
            <label>43</label>
            <mixed-citation id="d1196e1235" publication-type="other">
Gilbert JJ. 1980 Further observations on
developmental polymorphism and its evolution
in the rotifer Brachionus calyciflorus. Freshw. Biol.
10, 281-294. (doi:10.1111/j.1365-2427.1980.
tb01202.x)</mixed-citation>
         </ref>
         <ref id="d1196e1254a1310">
            <label>44</label>
            <mixed-citation id="d1196e1261" publication-type="other">
Denno RF, Olmstead KL, McCloud ES. 1989
Reproductive cost of flight capability: a comparison
of life history traits in wing dimorphic planthoppers.
Ecol. Entomol. 14, 31-44. (doi:10.1111/j.1365-
2311.1989.tb00751.x)</mixed-citation>
         </ref>
         <ref id="d1196e1280a1310">
            <label>45</label>
            <mixed-citation id="d1196e1287" publication-type="other">
McCollum SA, Van Buskirk J. 1996 Costs
and benefits of a predator-induced
polyphenism in the gray treefrog Hyla
chrysoscelis. Evolution 50, 583-593. (doi:10.2307/
2410833)</mixed-citation>
         </ref>
         <ref id="d1196e1306a1310">
            <label>46</label>
            <mixed-citation id="d1196e1313" publication-type="other">
Miner BG. 2005 Evolution of feeding structure
plasticity in marine invertebrate larvae: a possible
trade-off between arm length and stomach size.
J. Exp. Mar. Biol. Ecol. 315, 117-125. (doi:10.1016/
j.jembe.2004.09.011)</mixed-citation>
         </ref>
         <ref id="d1196e1333a1310">
            <label>47</label>
            <mixed-citation id="d1196e1340" publication-type="other">
Ryals PE, Smith-Somerville HE, Buhse Jr HE.
2002 Phenotype switching in polymorphic
Tetrahymena: a single-cell Jekyll and Hyde. Int. Rev.
Cytol. 212, 209-238. (doi:10.1016/S0074-7696
(01)12006-1)</mixed-citation>
         </ref>
         <ref id="d1196e1359a1310">
            <label>48</label>
            <mixed-citation id="d1196e1366" publication-type="other">
Kopp M, Tollrian R. 2003 Trophic size
polyphenism in Lembadion bullinum: costs
and benefits of an inducible offense. Ecology 84,
641-651. (doi:10.1890/0012-9658(2003)
084[0641:TSPILB]2.0.CO;2)</mixed-citation>
         </ref>
         <ref id="d1196e1385a1310">
            <label>49</label>
            <mixed-citation id="d1196e1394" publication-type="other">
Gilbert JJ. 1973 Induction and ecological
significance of gigantism in the rotifer Asplanchia
sieboldi. Science 181, 63-66. (do¡:10.1126/science.
181.4094.63)</mixed-citation>
         </ref>
         <ref id="d1196e1410a1310">
            <label>50</label>
            <mixed-citation id="d1196e1417" publication-type="other">
Collins JP, Cheek JE. 1983 Effect of food and density
on development of typical and cannibalistic
salamander larvae in Ambystoma tigrinum
nebulosum. Am. Zool. 23, 77-84.</mixed-citation>
         </ref>
         <ref id="d1196e1433a1310">
            <label>51</label>
            <mixed-citation id="d1196e1440" publication-type="other">
Nyman S, Wilkinson RF, Hutcherson JE. 1993
Cannibalism and size relations in a cohort of larval
ringed salamanders (Ambystoma annulatum).
J. Herpetol. 27, 78-84. (doi:10.2307/1564909)</mixed-citation>
         </ref>
         <ref id="d1196e1456a1310">
            <label>52</label>
            <mixed-citation id="d1196e1463" publication-type="other">
Walls SC, Beatty JJ, Tissot BN, Hokit DG, Blaustein
AR. 1993 Morphological variation and annibalism
in a larval salamander (Ambystoma macrodactylum
columbianum). Can. J. Zool. 71, 1543-1551.
(doi:10.1139/z93-218)</mixed-citation>
         </ref>
         <ref id="d1196e1483a1310">
            <label>53</label>
            <mixed-citation id="d1196e1490" publication-type="other">
Moczek AP. 2007 Developmental capacitance, genetic
accommodation, and adaptive evolution. Evol. Dev. 9,
299-305. (doi:10.1111/j.1525-142X.2007.00162.x)</mixed-citation>
         </ref>
         <ref id="d1196e1503a1310">
            <label>54</label>
            <mixed-citation id="d1196e1512" publication-type="other">
Kanzaki N, Ragsdale EJ, Herrmann M, Sommer RJ.
2012 Two new species of Pristionchus (Rhabditida:
Diplogastridae): P. fissidentatus n. sp. from Nepal
and La Réunion Island and P. elegans n. sp. from
Japan. J. Nematol. 44, 80-91.</mixed-citation>
         </ref>
         <ref id="d1196e1531a1310">
            <label>55</label>
            <mixed-citation id="d1196e1538" publication-type="other">
Ragsdale EJ, Kanzaki N, Röseler W, Herrmann M,
Sommer RJ. 2013 Three new species of
Pristionchus (Nematoda: Diplogastridae) show
morphological divergence through evolutionary
intermediates of a novel feeding polymorphism.
Zool. J. Linnean Soc. 168, 671-698. (doi:10.1111/
zoj.12041)</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
