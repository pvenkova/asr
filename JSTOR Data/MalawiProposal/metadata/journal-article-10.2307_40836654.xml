<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">revieconstud</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100024</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>The Review of Economic Studies</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Review of Economic Studies Ltd., Blackwell Publishing</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00346527</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">1467937X</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">40836654</article-id>
         <title-group>
            <article-title>The Long and Short (of) Quality Ladders</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>AMIT</given-names>
                  <surname>KHANDELWAL</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>10</month>
            <year>2010</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">77</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40037284</issue-id>
         <fpage>1450</fpage>
         <lpage>1476</lpage>
         <permissions>
            <copyright-statement>© 2010 The Review of Economic Studies Ltd.</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/40836654"/>
         <abstract>
            <p>Prices are typically used as proxies for countries' export quality. I relax this strong assumption by exploiting both price and quantity information to estimate the quality of products exported to the United States. Higher quality is assigned to products with higher market shares conditional on price. The estimated qualities reveal substantial heterogeneity in product markets' scope for quality differentiation, or their "quality ladders". I use this variation to explain the heterogeneous impact of low-wage competition on US manufacturing employment and output. Markets characterized by relatively short quality ladders are associated with larger employment and output declines resulting from low-wage competition.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d465e217a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d465e224" publication-type="other">
Linder (1961),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d465e230" publication-type="other">
Hallak (2006) and
Verhoogen (2008).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d465e239" publication-type="other">
Hummels and Klenow (2005)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d465e246" publication-type="other">
Schott (2004)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d465e252" publication-type="other">
Baldwin and Harrigan (2007),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d465e258" publication-type="other">
Hallak and Sivadasan (2009),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d465e264" publication-type="other">
Kugler and
Verhoogen (2008)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d465e273" publication-type="other">
Johnson (2009)</mixed-citation>
            </p>
         </fn>
         <fn id="d465e280a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d465e287" publication-type="other">
Kremer (1993)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d465e293" publication-type="other">
Verhoogen, 2008;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d465e299" publication-type="other">
Kugler and Verhoogen, 2008</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d465e306" publication-type="other">
Grossman and Helpman (1991).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d465e312" publication-type="other">
Hausmann and Rodrik (2003),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d465e318" publication-type="other">
Rodrik (2006)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d465e324" publication-type="other">
Hidalgo et al. (2007)</mixed-citation>
            </p>
         </fn>
         <fn id="d465e331a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d465e338" publication-type="other">
Goldberg (1995)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d465e344" publication-type="other">
Irwin
and Pavcnik (2004),</mixed-citation>
            </p>
         </fn>
         <fn id="d465e354a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d465e361" publication-type="other">
Hallak and Schott (2007)</mixed-citation>
            </p>
         </fn>
         <fn id="d465e369a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d465e376" publication-type="other">
Sachs and Shatz
(1994),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d465e385" publication-type="other">
Freeman and Katz (1991)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d465e391" publication-type="other">
Revenga (1992).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d465e398" publication-type="other">
Bernard, Jensen and Schott (2006)</mixed-citation>
            </p>
         </fn>
         <fn id="d465e405a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d465e412" publication-type="other">
Melitz (2003)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d465e418" publication-type="other">
Bernard et al (2007).</mixed-citation>
            </p>
         </fn>
         <fn id="d465e425a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d465e432" publication-type="other">
Anderson et al. (1992),</mixed-citation>
            </p>
         </fn>
         <fn id="d465e439a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d465e446" publication-type="other">
Learner (1987)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d465e452" publication-type="other">
Schott (2003).</mixed-citation>
            </p>
         </fn>
         <fn id="d465e459a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d465e466" publication-type="other">
Sutton (1998).</mixed-citation>
            </p>
         </fn>
         <fn id="d465e473a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d465e480" publication-type="other">
Learner
(1987),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d465e489" publication-type="other">
Davis and Weinstein (2001)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d465e495" publication-type="other">
Schott (2003).</mixed-citation>
            </p>
         </fn>
         <fn id="d465e503a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d465e510" publication-type="other">
Berry (1994),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d465e516" publication-type="other">
Cardell (1997)</mixed-citation>
            </p>
         </fn>
         <fn id="d465e523a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d465e530" publication-type="other">
Bernard, Jensen and Schott (2006)</mixed-citation>
            </p>
         </fn>
         <fn id="d465e537a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d465e544" publication-type="other">
Feenstra (1994)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d465e550" publication-type="other">
Hallak and Schott (2007)</mixed-citation>
            </p>
         </fn>
         <fn id="d465e557a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d465e564" publication-type="other">
Bartelsman, Becker and Gray, (1996)</mixed-citation>
            </p>
         </fn>
         <fn id="d465e571a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d465e578" publication-type="other">
Kugler and Verhoogen (2008),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d465e584" publication-type="other">
Sutton (1998)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d465e590" publication-type="other">
Antras (2003)</mixed-citation>
            </p>
         </fn>
         <fn id="d465e597a1310">
            <label>34</label>
            <p>
               <mixed-citation id="d465e604" publication-type="other">
Bernard, Jensen and Schott (2006)</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>REFERENCES</title>
         <ref id="d465e620a1310">
            <mixed-citation id="d465e624" publication-type="other">
AGHION, P., BLUNDELL, R., GRIFFITH, R., HOWITT, P. and PRANTL, S. (2009), "The Effects of Entry on
Incumbent Innovation and Productivity", The Review of Economics and Statistics, 91, 20-32.</mixed-citation>
         </ref>
         <ref id="d465e634a1310">
            <mixed-citation id="d465e638" publication-type="other">
AMITI, M. and KHANDELWAL, A. (2009), "Competition and Quality Upgrading" (Mimeo, Columbia University).</mixed-citation>
         </ref>
         <ref id="d465e645a1310">
            <mixed-citation id="d465e649" publication-type="other">
ANDERSON, S., DE PALMA, A. and THISSE, J. (1987), "The CES is a Discrete Choice Model?", Economic Letters,
24. 139-140.</mixed-citation>
         </ref>
         <ref id="d465e659a1310">
            <mixed-citation id="d465e663" publication-type="other">
ANDERSON, S., DE PALMA, A. and THISSE, J. (1992), Discrete Choice Theory of Product Differentiation (Cam-
hridcre: MTT Press).</mixed-citation>
         </ref>
         <ref id="d465e674a1310">
            <mixed-citation id="d465e678" publication-type="other">
ANTRAS, P. (2003), "Firms, Contracts, and Trade Structure", Quarterly Journal of Economics, 118, 1375-1418.</mixed-citation>
         </ref>
         <ref id="d465e685a1310">
            <mixed-citation id="d465e689" publication-type="other">
BALDWIN, R. and HARRIGAN, J. (2007), "Zeros, Quality and Space: Trade Theory and Trade Evidence" (NBER
Working Paper 13214).</mixed-citation>
         </ref>
         <ref id="d465e699a1310">
            <mixed-citation id="d465e703" publication-type="other">
BARTELSMAN, E., BECKER, R. and GRAY, W. (1996), "The NBER-CES Manufacturing Industry Database"
(NBER Technical Working Paper No. 205).</mixed-citation>
         </ref>
         <ref id="d465e713a1310">
            <mixed-citation id="d465e717" publication-type="other">
BERNARD, A., JENSEN, B., REDDING, S. and SCHOTT, P. (2007), "Firms in International Trade", Journal of
Economic Perspectives, 21, 105-130.</mixed-citation>
         </ref>
         <ref id="d465e727a1310">
            <mixed-citation id="d465e731" publication-type="other">
BERNARD, A., JENSEN, J. and SCHOTT, P. (2006), "Survival of the Best Fit: Exposure to Low-Wage Countries
and the (Uneven) Growth of U.S. Manufacturing", Journal of International Economics, 68, 219-237.</mixed-citation>
         </ref>
         <ref id="d465e741a1310">
            <mixed-citation id="d465e745" publication-type="other">
BERRY, S. (1994), "Estimating Discrete-Choice Models of Product Differentiation", The RAND Journal of Economics,
25, 242-262.</mixed-citation>
         </ref>
         <ref id="d465e756a1310">
            <mixed-citation id="d465e760" publication-type="other">
BERRY, S., LEVINSOHN, J. and PAKES, A. (1995), "Automobile Prices in Market Equilibrium", Econometrica, 63,
841-890.</mixed-citation>
         </ref>
         <ref id="d465e770a1310">
            <mixed-citation id="d465e774" publication-type="other">
BRAMBILLA, I., KHANDELWAL, A. and SCHOTT, P. (2008), "China's Experience under the Multifiber Arrange-
ment and Agreement on Textile and Clothing" (NBER Working Paper 13346).</mixed-citation>
         </ref>
         <ref id="d465e784a1310">
            <mixed-citation id="d465e788" publication-type="other">
BRESNAHAN, T. (1993), "Competition and Collusion in the American Automobile Industry: The 1955 Price War",
Journal of Industrial Economics, 35, 457-482.</mixed-citation>
         </ref>
         <ref id="d465e798a1310">
            <mixed-citation id="d465e802" publication-type="other">
BRODA, C. and WEINSTEIN, D. (2006), "Globalization and the Gains from Variety", Quarterly Journal of Economics,
121, 541-585.</mixed-citation>
         </ref>
         <ref id="d465e812a1310">
            <mixed-citation id="d465e816" publication-type="other">
CARDELL, N. S. (1997), "Variance Components Structures for the Extreme-Value and Logistic Distributions with
Application to Models of Heterogeneity", Econometric Theory, 13, 185-213.</mixed-citation>
         </ref>
         <ref id="d465e826a1310">
            <mixed-citation id="d465e830" publication-type="other">
CHARI, A. V. and KHANDELWAL, A. (2009), "Quality Specialization and the Dynamics of Trade Policy" (Mimeo,
Columbia University).</mixed-citation>
         </ref>
         <ref id="d465e841a1310">
            <mixed-citation id="d465e845" publication-type="other">
DAVIS, D. and WEINSTEIN, D. (2001), "An Account of Global Factor Trade", American Economic Review, 91,
1423-1454.</mixed-citation>
         </ref>
         <ref id="d465e855a1310">
            <mixed-citation id="d465e859" publication-type="other">
FAJGELBAUM, P., GROSSMAN, G. and HELPMAN, E. (2009), "Income Distribution, Product Quality and Inter-
national Trade" (Mimeo, Princeton University).</mixed-citation>
         </ref>
         <ref id="d465e869a1310">
            <mixed-citation id="d465e873" publication-type="other">
FEENSTRA, R. (1994), "New Product Varieties and the Measurement of International Prices", American Economic
Review, 84, 157-177.</mixed-citation>
         </ref>
         <ref id="d465e883a1310">
            <mixed-citation id="d465e887" publication-type="other">
FEENSTRA, R., ROMALIS, J. and SCHOTT, P. (2002), "U.S. Imports, Exports, and Tariff Data, 1989-2001" (NBER
Working Paper 9387).</mixed-citation>
         </ref>
         <ref id="d465e897a1310">
            <mixed-citation id="d465e901" publication-type="other">
FLAM, H. and HELPMAN, E. (1987), "Vertical Product Differentiation and North-South Trade", American Economic
Review, 77, 810-822.</mixed-citation>
         </ref>
         <ref id="d465e911a1310">
            <mixed-citation id="d465e915" publication-type="other">
FREEMAN, R. and KATZ, L. (1991), "Industrial Wage and Employment Determination in an Open Economy", in
Abowd, J. and Freeman, R. (eds.), Immigration, Trade and Labor Market (Chicago: University of Chicago Press).</mixed-citation>
         </ref>
         <ref id="d465e926a1310">
            <mixed-citation id="d465e930" publication-type="other">
GENERAL ACCOUNTING OFFICE. (1995), "U.S. Imports: Unit Values Vary Widely for Identically Classified
Commodities" (Report GAO/GGD-95-90).</mixed-citation>
         </ref>
         <ref id="d465e940a1310">
            <mixed-citation id="d465e944" publication-type="other">
GOLDBERG, P. (1995), "Product Differentiation and Oligopoly in International Markets: The Case of the U.S.
Automobile Industry", Econometrica, 63, 891-951.</mixed-citation>
         </ref>
         <ref id="d465e954a1310">
            <mixed-citation id="d465e958" publication-type="other">
GOLDBERG, P. and PAVCNIK, N. (2007), "Distributional Effects of Globalization in Developing Countries", Journal
of Economic Literature, 45, 39-82.</mixed-citation>
         </ref>
         <ref id="d465e968a1310">
            <mixed-citation id="d465e972" publication-type="other">
GROSSMAN, G. and HELPMAN, E. (1991), "Quality Ladders and Product Cycles", Quarterly Journal of Economics,
106 S57-586.</mixed-citation>
         </ref>
         <ref id="d465e982a1310">
            <mixed-citation id="d465e986" publication-type="other">
HALLAK, J. and SCHOTT, P. (2007), "Estimating Cross-Country Differences in Product Quality" (Mimeo, Yale
University).</mixed-citation>
         </ref>
         <ref id="d465e996a1310">
            <mixed-citation id="d465e1000" publication-type="other">
HALLAK, J. and SIVADASAN, J. (2009), "Firms' Exporting Behavior under Quality Constraints" (NBER Working
Paper 14928).</mixed-citation>
         </ref>
         <ref id="d465e1011a1310">
            <mixed-citation id="d465e1015" publication-type="other">
HALLAK, J. C. (2006), "Product Quality and the Direction of Trade", Journal of International Economics, 68,
238-265.</mixed-citation>
         </ref>
         <ref id="d465e1025a1310">
            <mixed-citation id="d465e1029" publication-type="other">
HAUSMANN, R. and RODRIK, D. (2003), "Economic Development as Self-Discovery" (Mimeo, Harvard University).</mixed-citation>
         </ref>
         <ref id="d465e1036a1310">
            <mixed-citation id="d465e1040" publication-type="other">
HELPMAN, E., MELITZ, M. and RUBINSTEIN, Y. (2006), "Trading Partners and Trading Volumes" (Mimeo,
Harvard University) .</mixed-citation>
         </ref>
         <ref id="d465e1050a1310">
            <mixed-citation id="d465e1054" publication-type="other">
HIDALGO, C, KLINGER, B., BARABASI, A. and HAUSMANN, R. (2007), "The Product Space Conditions the
Development of Nations", Science, 317, 482-487.</mixed-citation>
         </ref>
         <ref id="d465e1064a1310">
            <mixed-citation id="d465e1068" publication-type="other">
HUMMELS, D. and KLENOW, P. (2005), "The Variety and Quality of a Nation's Exports", American Economic
Review. 95. 704-723.</mixed-citation>
         </ref>
         <ref id="d465e1078a1310">
            <mixed-citation id="d465e1082" publication-type="other">
HUMMELS, D. and SKIBA, A. (2004), "Shipping the Good Apples Out? An Empirical Confirmation of the Alchian-
Allen Conjecture", Journal of Political Economy, 112, 1384-1402.</mixed-citation>
         </ref>
         <ref id="d465e1093a1310">
            <mixed-citation id="d465e1097" publication-type="other">
IRWIN, D. and PAVCNIK, N. (2004), "Airbus versus Boeing Revisited: International Competition in the Aircraft
Market", Journal of International Economics, 64, 223-245.</mixed-citation>
         </ref>
         <ref id="d465e1107a1310">
            <mixed-citation id="d465e1111" publication-type="other">
JOHNSON, R. (2009), "Trade and Prices with Heterogeneous Firms" (Mimeo, University of California at Berkeley).</mixed-citation>
         </ref>
         <ref id="d465e1118a1310">
            <mixed-citation id="d465e1122" publication-type="other">
KAPLAN, D. and VERHOOGEN, E. (2005), "Exporting and Individual Wage Premia: Evidence from Mexican
Employer-Employee Data" (Mimeo, Columbia University).</mixed-citation>
         </ref>
         <ref id="d465e1132a1310">
            <mixed-citation id="d465e1136" publication-type="other">
KREMER, M. (1993), "The O-Ring Theory of Economic Development", Quarterly Journal of Economics, 108,
551-575.</mixed-citation>
         </ref>
         <ref id="d465e1146a1310">
            <mixed-citation id="d465e1150" publication-type="other">
KRUGMAN, P. (1980), "Scale Economies, Product Differentiation and the Pattern of Trade", American Economic
Review. 70. 950-959.</mixed-citation>
         </ref>
         <ref id="d465e1160a1310">
            <mixed-citation id="d465e1164" publication-type="other">
KUGLER, M. and VERHOOGEN, E. (2008), "The Quality-Complementarity Hypothesis: Theory and Evidence from
Columbia" (NBER Working Paper 14418).</mixed-citation>
         </ref>
         <ref id="d465e1175a1310">
            <mixed-citation id="d465e1179" publication-type="other">
LEAMER, E. (1987), "Paths of Development in the Three-Factor, n-Good General Equilibrium Model", Journal of
Political Economy, 95, 961-999.</mixed-citation>
         </ref>
         <ref id="d465e1189a1310">
            <mixed-citation id="d465e1193" publication-type="other">
LEAMER, E. (2000), "What's the Use of Factor Contents?", Journal of International Economics, 50, 17-49.</mixed-citation>
         </ref>
         <ref id="d465e1200a1310">
            <mixed-citation id="d465e1204" publication-type="other">
LEAMER, E. (2006), "A Flat World, A Level Playing Field, A Small World After All, or None of the Above?"
(Mimeo, UCLA).</mixed-citation>
         </ref>
         <ref id="d465e1214a1310">
            <mixed-citation id="d465e1218" publication-type="other">
LINDER, S. (1961), "An Essav on Trade and Transformation" (Stockholm: Almqvist &amp; Wiksell).</mixed-citation>
         </ref>
         <ref id="d465e1225a1310">
            <mixed-citation id="d465e1229" publication-type="other">
MELITZ, M. (2003), "The Impact of Trade on Intra-Industry Reallocations and Aggregate Industry Productivity",
Econometrica, 71, 1695-1725.</mixed-citation>
         </ref>
         <ref id="d465e1239a1310">
            <mixed-citation id="d465e1243" publication-type="other">
RAUCH, J. (1999), "Networks versus Markets in International Trade", Journal of International Economics, 48, 7-35.</mixed-citation>
         </ref>
         <ref id="d465e1251a1310">
            <mixed-citation id="d465e1255" publication-type="other">
REVENGA, A. (1992), "Exporting Jobs? The Impact of Import Competition on Employment and Wages in U.S.
Manufacturing", Quarterly Journal of Economics, 107, 255-284.</mixed-citation>
         </ref>
         <ref id="d465e1265a1310">
            <mixed-citation id="d465e1269" publication-type="other">
RODRIK, D. (2006), "What's So Special about China's Exports?" (NBER Working Paper 11947).</mixed-citation>
         </ref>
         <ref id="d465e1276a1310">
            <mixed-citation id="d465e1280" publication-type="other">
SACHS, J. and SHATZ, H. (1994), "Trade and Jobs in U.S. Manufacturing", Brookings Papers on Economic Activity,
1994(1), 1-69.</mixed-citation>
         </ref>
         <ref id="d465e1290a1310">
            <mixed-citation id="d465e1294" publication-type="other">
SCHOTT, P. (2003), "One Size Fits All? Hecksher-Ohlin Specialization in Global Production", American Economic
Rpvípw 93 686-708</mixed-citation>
         </ref>
         <ref id="d465e1304a1310">
            <mixed-citation id="d465e1308" publication-type="other">
SCHOTT, P. (2004), "Across-Product versus Within-Product Specialization in International Trade", Quarterly Journal
of Economics, 119, 647-678.</mixed-citation>
         </ref>
         <ref id="d465e1318a1310">
            <mixed-citation id="d465e1322" publication-type="other">
SUTTON, J. (1991), Price Competition, Advertising, and the Evolution of Concentration (Cambridge: The MIT Press).</mixed-citation>
         </ref>
         <ref id="d465e1330a1310">
            <mixed-citation id="d465e1334" publication-type="other">
SUTTON, J. (1998), Technology and Market Structure: Theory and History (Cambridge: The MIT Press).
UNIDO. (2005), "Industrial Statistics Database".</mixed-citation>
         </ref>
         <ref id="d465e1344a1310">
            <mixed-citation id="d465e1348" publication-type="other">
VERHOOGEN, E. (2008), "Trade, Quality Upgrading, and Wage Inequality in the Mexican Manufacturing Sector",
Quarterly Journal of Economics, 123, 489-530.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
