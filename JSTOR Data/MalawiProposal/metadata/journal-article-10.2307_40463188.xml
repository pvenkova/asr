<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">anthropos</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000621</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Anthropos</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Paulusdruckerei</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">02579774</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">40463188</article-id>
         <title-group>
            <article-title>Masks, Society, and Hierarchy among the Chewa of Central Malawi</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Laurel Birch</given-names>
                  <surname>de Aguilar</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>1</month>
            <year>1995</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">90</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4/6</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40020062</issue-id>
         <fpage>407</fpage>
         <lpage>421</lpage>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/40463188"/>
         <abstract>
            <p>In the course of field research various hierarchical relationships between masks became consistently evident throughout the region of study. The same named mask forms existed from one village area to the next, and each form shared the same specific rhythm of drumming for performances, similar dance steps, and the same kinds of roles and characterizations. Once consistency in mask forms became evident, differences between these mask forms also became apparent. Differences included the relative importance of one mask form over another, depending on the various values associated with the mask forms. This article is about these relationships and the hierarchy of Chewa masks in relation to Chewa society.</p>
         </abstract>
         <kwd-group>
            <kwd>Malawi</kwd>
            <kwd>Chewa</kwd>
            <kwd>hierarchy</kwd>
            <kwd>masks</kwd>
            <kwd>relation of masks and society</kwd>
         </kwd-group>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d1515e226a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1515e233" publication-type="other">
Stannus 1910,</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1515e239" publication-type="other">
Hodgson 1933,</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1515e245" publication-type="other">
Rangeley 1949/50,</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1515e252" publication-type="other">
Schoffeleers 1968, 1975, 1992,</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1515e258" publication-type="other">
Blackmun and Schoffeleers
1972,</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1515e267" publication-type="other">
Schoffeleers and Roscoe 1985,</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1515e273" publication-type="other">
Breughel 1985,</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1515e279" publication-type="other">
Birch Faulkner 1988,</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1515e285" publication-type="other">
Kamlongera et. al. 1992,</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1515e292" publication-type="other">
Yoshida
1993,</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1515e301" publication-type="other">
Birch de Aguilar 1993.</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References Cited</title>
         <ref id="d1515e317a1310">
            <mixed-citation id="d1515e321" publication-type="other">
Birch Faulkner, Laurel
1988 Basketry Masks of the Chewa. African Arts 21: 28-31,
86.</mixed-citation>
         </ref>
         <ref id="d1515e334a1310">
            <mixed-citation id="d1515e338" publication-type="other">
Birch de Aguilar, Laurel
1993 Reconstructing Historical Experience in Chewa Masks
of Malawi. London. [Unpublished paper for African
History Seminar, SOAS, November 2]</mixed-citation>
         </ref>
         <ref id="d1515e354a1310">
            <mixed-citation id="d1515e358" publication-type="other">
Blackmun, Barbara, and J. Matthew Schoffeleers
1972 Masks of Malawi. African Arts 5/4: 36-41, 69.</mixed-citation>
         </ref>
         <ref id="d1515e368a1310">
            <mixed-citation id="d1515e372" publication-type="other">
van Breugel, J. W. M.
1985 The Religious Significance of the Nyao among the Che-
wa of Malawi. Cultures et développement 17: 487-517.</mixed-citation>
         </ref>
         <ref id="d1515e386a1310">
            <mixed-citation id="d1515e390" publication-type="other">
Hodgson, A. G. O.
1933 Notes on the Achewa and Angoni of the Dowa District
of the Nyasaland Protectorate. The Journal of the Royal
Anthropological Institute of Great Britain and Ireland
63: 123-164.</mixed-citation>
         </ref>
         <ref id="d1515e409a1310">
            <mixed-citation id="d1515e413" publication-type="other">
Kamlongera, Christopher et al.
1992 Kubvina: An Introduction to Malawi Dance and The-
atre. Zomba: University of Malawi.</mixed-citation>
         </ref>
         <ref id="d1515e426a1310">
            <mixed-citation id="d1515e430" publication-type="other">
Kasfir, Sydney L.
1988 Introduction: Masquerading as a Cultural System. In:
Sydney L. Kasfir (ed.), West African Masks and Cul-
tural Systems; pp. 1-16. Tervuren: Musée royal de
l'Afrique centrale. (Sciences Humaines, 126)</mixed-citation>
         </ref>
         <ref id="d1515e449a1310">
            <mixed-citation id="d1515e453" publication-type="other">
Rangeley, W. H. J.
1949/50 Nyau in Kotakota District. The Nyasaland Journal
2/2: 35-49; 3/2: 19-33.</mixed-citation>
         </ref>
         <ref id="d1515e466a1310">
            <mixed-citation id="d1515e470" publication-type="other">
Schoffeleers, J. Matthew
1968 Social and Symbolic Aspects of Mang'anja Religion.
Oxford. [D. Phil, diss., Oxford University]</mixed-citation>
         </ref>
         <ref id="d1515e483a1310">
            <mixed-citation id="d1515e487" publication-type="other">
1975 The Nyau Societies: Our Present Understanding. Blan-
tyre: Society of Malawi.</mixed-citation>
         </ref>
         <ref id="d1515e498a1310">
            <mixed-citation id="d1515e502" publication-type="other">
1992 River of Blood: The Genesis of a Martyr Cult in South-
ern Malawi, c. A.D. 1600. Madison: The University of
Wisconsin Press.</mixed-citation>
         </ref>
         <ref id="d1515e515a1310">
            <mixed-citation id="d1515e519" publication-type="other">
Schoffeleers, J. Matthew, and A. A. Roscoe
1985 Land of Fire. Lilongwe: Popular Publications.</mixed-citation>
         </ref>
         <ref id="d1515e529a1310">
            <mixed-citation id="d1515e533" publication-type="other">
Skorupski, John
1976 Symbol and Theory: A Philosophical Study of Theories
of Religion in Social Anthropology. Cambridge: Cam-
bridge University Press.</mixed-citation>
         </ref>
         <ref id="d1515e549a1310">
            <mixed-citation id="d1515e553" publication-type="other">
Stannus, H.
1910 Notes on Some Tribes of British Central Africa. The
Journal of the Royal Anthropological Institute of Great
Britain and Ireland 40: 285-335.</mixed-citation>
         </ref>
         <ref id="d1515e569a1310">
            <mixed-citation id="d1515e573" publication-type="other">
White, C. M. N.
1961 Elements in Luvale Beliefs and Rituals. Manchester:
Manchester University Press.</mixed-citation>
         </ref>
         <ref id="d1515e586a1310">
            <mixed-citation id="d1515e590" publication-type="other">
Yoshida, K.
1993 Masks and Secrecy among the Chewa. African Arts 26:
34-45.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
