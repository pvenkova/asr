<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt1gsmx3r</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1gxpfd6</book-id>
      <subj-group>
         <subject content-type="call-number">HG173.A378 2014</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Finance</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Financial crises</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Finance</subject>
         <subj-group>
            <subject content-type="lcsh">Developing countries</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Economic development</subject>
         <subj-group>
            <subject content-type="lcsh">Developing countries</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Business</subject>
         <subject>Economics</subject>
         <subject>Finance</subject>
      </subj-group>
      <book-title-group>
         <book-title>Liberalization, Financial Instability and Economic Development</book-title>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Akyüz</surname>
               <given-names>Yilmaz</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>01</day>
         <month>05</month>
         <year>2014</year>
      </pub-date>
      <isbn content-type="ppub">9781783082292</isbn>
      <isbn content-type="epub">9781783082407</isbn>
      <publisher>
         <publisher-name>Anthem Press</publisher-name>
         <publisher-loc>LONDON; NEW YORK; DELHI</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2014</copyright-year>
         <copyright-holder>South Centre</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt1gxpfd6"/>
      <abstract abstract-type="short">
         <p>Weighing up the costs and benefits of economic interdependence in a finance-driven world, this book argues that globalization has been oversold to the Global South, and that the South should be as selective about globalization as the North.</p>
      </abstract>
      <counts>
         <page-count count="338"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxpfd6.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxpfd6.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxpfd6.3</book-part-id>
                  <title-group>
                     <title>INTRODUCTION</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>Globalization is understood and promoted as absolute freedom for all forms of capital, above all financial capital, while restrictions continue to shape the markets for goods, labor and technology. This has reduced the power of nations to regulate and control their own economic space, shifted the playing field against labor and industry, and led to growing financialization of the world economy. Two important consequences are increased instability and inequality. Moreover, for developing countries (DCs) the benefits of unleashed finance have proved to be highly elusive. It has not only meant loss of crucial policy tools for industrialization and development, but</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxpfd6.4</book-part-id>
                  <title-group>
                     <label>Chapter I</label>
                     <title>FINANCIAL LIBERALIZATION:</title>
                     <subtitle>THE KEY ISSUES</subtitle>
                  </title-group>
                  <fpage>19</fpage>
                  <abstract>
                     <p>In recent years financial policies in both industrial and developing countries have put increased emphasis on the market mechanism. Liberalization was partly a response to developments in the financial markets themselves: as these markets innovated to get round the restrictions placed on them, governments chose to throw in the towel. More importantly, however, governments embraced liberalization as a doctrine.</p>
                     <p>In developing countries, the main impulse behind liberalization has been the belief, based on the notion that interventionist financial policies were one of the main causes of the crisis of the 1980s, that liberalization would help to restore growth and stability</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxpfd6.5</book-part-id>
                  <title-group>
                     <label>Chapter II</label>
                     <title>MANAGING FINANCIAL INSTABILITY IN EMERGING MARKETS:</title>
                     <subtitle>A KEYNESIAN PERSPECTIVE</subtitle>
                  </title-group>
                  <fpage>51</fpage>
                  <abstract>
                     <p>With widespread deregulation and rapid growth of financial wealth, business cycles in both advanced economies and emerging markets are increasingly dominated by the financial system.</p>
                     <p>It is true that there is not always a one-to-one correspondence between real and financial cycles, and recessions do not always go in tandem with financial crises. Nevertheless, the response of the financial system to impulses emanating from the real economy has become increasingly procyclical, and this tends to reinforce expansionary and contractionary forces and amplify swings in investment, output and employment, creating new dilemmas for macroeconomic policy.</p>
                     <p>With rapid liberalization of the capital account,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxpfd6.6</book-part-id>
                  <title-group>
                     <label>Chapter III</label>
                     <title>FROM LIBERALIZATION TO INVESTMENT AND JOBS:</title>
                     <subtitle>LOST IN TRANSLATION</subtitle>
                  </title-group>
                  <fpage>73</fpage>
                  <abstract>
                     <p>The past two decades have seen an increased global integration of labor markets to a degree unprecedented in recent history despite continued barriers to labor mobility, particularly for low-skilled and unskilled workers. This has been driven by a rapid opening to and expansion of international trade and capital flows, and a growing spread of global production networks, outsourcing and offshoring. The total number of workers producing for international markets in goods alone rose from around 300 million in 1980 to almost 800 million at the turn of the millennium. This has been associated with a significant increase in the share</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxpfd6.7</book-part-id>
                  <title-group>
                     <label>Chapter IV</label>
                     <title>EXCHANGE RATE MANAGEMENT, GROWTH AND STABILITY:</title>
                     <subtitle>NATIONAL AND REGIONAL POLICY OPTIONS IN ASIA</subtitle>
                  </title-group>
                  <fpage>117</fpage>
                  <abstract>
                     <p>The exchange rate has become a growing focus of attention in the recent policy debate in developing countries. This is due mainly to two reasons. First, with increased emphasis placed on export-led growth and the dismantling of tariff and nontariff barriers, the role of the exchange rate in growth and development has gained added importance. Drawing on the experience of late industrializers in East Asia, competitive and stable exchange rates have come to be seen as a key ingredient of successful industrialization.</p>
                     <p>Second, with rapid liberalization of the capital account in developing countries and the growing size and speed of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxpfd6.8</book-part-id>
                  <title-group>
                     <label>Chapter V</label>
                     <title>REFORMING THE IMF:</title>
                     <subtitle>BACK TO THE DRAWING BOARD</subtitle>
                  </title-group>
                  <fpage>183</fpage>
                  <abstract>
                     <p>There have been widespread misgivings about international economic cooperation in recent years even as the need for global collective action has grown because of recurrent financial crises in emerging markets, the increased gap between the rich and the poor, and the persistence of extreme poverty in many countries in the developing world. Perhaps more than any other international organization the International Monetary Fund (IMF) has been the focus of these misgivings.</p>
                     <p>Several observers including former treasury secretaries of the United States, a Nobel Prize–winning economist and many NGOs have called for its abolition on the grounds that it is</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxpfd6.9</book-part-id>
                  <title-group>
                     <label>Chapter VI</label>
                     <title>THE CURRENT GLOBAL FINANCIAL TURMOIL AND ASIAN DEVELOPING COUNTRIES</title>
                  </title-group>
                  <fpage>223</fpage>
                  <abstract>
                     <p>After almost six years of exceptional performance, the world economy has now entered a period of uncertainty due to a financial turmoil triggered by the subprime mortgage crisis in the United States. World economic growth and stability in the next few years will depend crucially on the impact of the crisis on the United States economy and its global spillovers. The resilience of emerging markets to direct and indirect shocks from the crisis will no doubt play an important role, since much of global growth in recent years has been due to expansion in these economies, notably in Asia. The</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxpfd6.10</book-part-id>
                  <title-group>
                     <label>Chapter VII</label>
                     <title>THE GLOBAL ECONOMIC CRISIS AND ASIAN DEVELOPING COUNTRIES:</title>
                     <subtitle>IMPACT, POLICY RESPONSE AND MEDIUM-TERM PROSPECTS</subtitle>
                  </title-group>
                  <fpage>259</fpage>
                  <abstract>
                     <p>After several years of impressive growth, the world economy encountered an equally impressive downturn starting in the third quarter of 2008, triggered by financial fragility and imbalances generated by speculative lending and investment and debt-driven spending in major advanced economies (AEs), notably the United States. Initially, there was widespread optimism that growth in developing and emerging economies (DEEs) of East Asia² would be decoupled from the difficulties that pervaded AEs and the region would continue to surge ahead as an autonomous growth pole. Sound balance of payments positions and self-insurance provided by large international reserves accumulated from current account surpluses</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxpfd6.11</book-part-id>
                  <title-group>
                     <label>Chapter VIII</label>
                     <title>THE STAGGERING RISE OF THE SOUTH?</title>
                  </title-group>
                  <fpage>293</fpage>
                  <abstract>
                     <p>In the early days of the global economic crisis, growth in developing and emerging economies (DEEs) was widely expected to be decoupled from the difficulties facing advanced economies (AEs). Strong growth that many DEEs had been enjoying since the early years of the millennium was expected to continue with some moderation and prevent AEs and the world economy from falling into recession. In the event, however, growth in DEEs slowed considerably in 2009 as a result of contraction of exports and financial contagion. AEs fell into recession and world income declined for the first time in several decades.</p>
                     <p>Nevertheless DEEs</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
