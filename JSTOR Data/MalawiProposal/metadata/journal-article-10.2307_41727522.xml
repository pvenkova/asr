<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">procbiolscie</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100837</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Proceedings: Biological Sciences</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>The Royal Society</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">09628452</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">41727522</article-id>
         <article-categories>
            <subj-group>
               <subject>Review</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Sexual dichromatism in frogs: natural selection, sexual selection and unexpected diversity</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Rayna C.</given-names>
                  <surname>Bell</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Kelly R.</given-names>
                  <surname>Zamudio</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>7</day>
            <month>12</month>
            <year>2012</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">279</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1748</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40081210</issue-id>
         <fpage>4687</fpage>
         <lpage>4693</lpage>
         <permissions>
            <copyright-statement>Copyright © 2012 The Royal Society</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/41727522"/>
         <abstract>
            <p>Sexual dichromatism, a form of sexual dimorphism in which males and females differ in colour, is widespread in animals but has been predominantly studied in birds, fishes and butterflies. Moreover, although there are several proposed evolutionary mechanisms for sexual dichromatism in vertebrates, few studies, have examined this phenomenon outside the context of sexual selection. Here, we describe unexpectedly high diversity of sexual dichromatism in frogs and create a comparative framework to guide future analyses of the evolution of these sexual colour differences. We review what is known about evolution of colour dimorphism in frogs, highlight alternative mechanisms that may contribute to the evolution of sexual colour differences, and compare them to mechanisms active in other major groups of vertebrates. In frogs, sexual dichromatism can be dynamic (temporary colour change in males) or ontogenetic (permanent colour change in males or females). The degree and the duration of sexual colour differences vary greatly across lineages, and we do not detect phylogenetic signal in the distribution of this trait, therefore frogs provide an opportunity to investigate the roles of natural and sexual selection across multiple independent derivations of sexual dichromatism.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>REFERENCES</title>
         <ref id="d836e169a1310">
            <label>1</label>
            <mixed-citation id="d836e176" publication-type="other">
Kimball, R. T. &amp; Ligon, J. D. 1999 Evolution of avian
plumage dichromatism from a proximate perspective.
Am. Nat. 154, 182-193. (doi:10.1 086/303228)</mixed-citation>
         </ref>
         <ref id="d836e189a1310">
            <label>2</label>
            <mixed-citation id="d836e198" publication-type="other">
Badyaev, A. V. &amp; Hill, G. E. 2003 Avian sexual dichroma-
tism in relation to phylogeny and ecology. Annu. Rev.
Ecol. Evol. Syst. 34, 27-49. (doi:10.1146/annurev.
ecolsys.34.011802.132441)</mixed-citation>
         </ref>
         <ref id="d836e214a1310">
            <label>3</label>
            <mixed-citation id="d836e221" publication-type="other">
Kodric-Brown, A. 1998 Sexual dichromatism and
temporary color changes in the reproduction of fishes.
Am. Zool. 31, 70-81. (doi:10.1093/icb/38.1.70)</mixed-citation>
         </ref>
         <ref id="d836e234a1310">
            <label>4</label>
            <mixed-citation id="d836e241" publication-type="other">
Allen, C. E., Zwaan, B. J. &amp; Brakefield, R M. 2011
Evolution of sexual dimorphism in the Lepidoptera.
Annu. Rev. Entomol. 56, 445-464. (doi:10.1146/
annurev-ento-120709-144828)</mixed-citation>
         </ref>
         <ref id="d836e258a1310">
            <label>5</label>
            <mixed-citation id="d836e265" publication-type="other">
Darwin, C. 1874 The descent of man , and selection in
relation to sex. London, UK: John Murray.</mixed-citation>
         </ref>
         <ref id="d836e275a1310">
            <label>6</label>
            <mixed-citation id="d836e282" publication-type="other">
Salthe, S. N. &amp; Duellman, W. 1973 Quantitative constrains
associated with reproductive mode in anurans. In
Evolutionary biology of the anurans (ed. J. L. Vial),
pp. 229-249. Columbia, SC: University of Missouri Press.</mixed-citation>
         </ref>
         <ref id="d836e298a1310">
            <label>7</label>
            <mixed-citation id="d836e305" publication-type="other">
Shine, R. 1979 Sexual selection and sexual dimorphism in
the amphibia. Copeia 2, 297-306. (doi:10.2307/1443418)</mixed-citation>
         </ref>
         <ref id="d836e315a1310">
            <label>8</label>
            <mixed-citation id="d836e322" publication-type="other">
Hoffman, E. A. &amp; Blouin, M. S. 2000 A review of colour and
pattern polymorphism in anurans. Biol. J. Linnean Soc.
70, 633-665. (doi:10.1111/j.1095-8312.2000.tb0022 1.x)</mixed-citation>
         </ref>
         <ref id="d836e335a1310">
            <label>9</label>
            <mixed-citation id="d836e342" publication-type="other">
Doucet, S. M. &amp; Mennill, D. J. 2010 Dynamic sexual
dichromatism in an explosively breeding neotropical
toad. Biol. Lett. 6, 63-66. (doi:10.1098/rsbl.2009.0604)</mixed-citation>
         </ref>
         <ref id="d836e355a1310">
            <label>10</label>
            <mixed-citation id="d836e362" publication-type="other">
Hedengren, I. 1987 Selection on body size, arm length
and colour in male and female moor frogs (Rana arvalis).
MSc thesis, Section of Ethology, Department of Zoology,
University of Stockholm, Stockholm, Sweden.</mixed-citation>
         </ref>
         <ref id="d836e379a1310">
            <label>11</label>
            <mixed-citation id="d836e386" publication-type="other">
Glaw, F. &amp; Vences, M. 1994 A field guide to the amphibians
and reptiles of Madagascar. Leverkusen &amp; Koln, Germany:
Moos Druck/FARBO.</mixed-citation>
         </ref>
         <ref id="d836e399a1310">
            <label>12</label>
            <mixed-citation id="d836e406" publication-type="other">
Stewart, M. M. 1967 Amphibians of Malawi. Albany, NY:
State University of New York Press.</mixed-citation>
         </ref>
         <ref id="d836e416a1310">
            <label>13</label>
            <mixed-citation id="d836e423" publication-type="other">
Pyron, R. A. &amp; Wiens, J. J. 2011 A large-scale phylogeny
of Amphibia including over 2800 species, and a revised
classification of extant frogs, salamanders and caecilians.
Mol. Phylogenet. Evol. 61, 543-583. (doi:10.1016/j.
ympev.2011.06.012)</mixed-citation>
         </ref>
         <ref id="d836e442a1310">
            <label>14</label>
            <mixed-citation id="d836e449" publication-type="other">
Sanderson, M. J. 2002 Estimating absolute rates of
molecular evolution and divergence times: a penalized
likelihood approach. Mol. Biol. Evol. 19, 101-109.
(doi:10.1093/oxfordjournals.molbev.a003974)</mixed-citation>
         </ref>
         <ref id="d836e465a1310">
            <label>15</label>
            <mixed-citation id="d836e472" publication-type="other">
Pagel, M. 1999 Inferring the historical patterns of biological
evolution. Nature 401, 877-884. (doi:10.1038/44766)</mixed-citation>
         </ref>
         <ref id="d836e482a1310">
            <label>16</label>
            <mixed-citation id="d836e489" publication-type="other">
Harmon, L., Weir, J., Brock, C., Glor, R., Wendell, C. &amp;
Hunt, G. 2009 geiger: analysis of evolutionary diversifi-
cation. R package v. 1.3-1. See http://CRAN.R-project.
org/package=geiger.</mixed-citation>
         </ref>
         <ref id="d836e506a1310">
            <label>17</label>
            <mixed-citation id="d836e513" publication-type="other">
Andersson, M. 1982 Sexual selection, natural selection
and quality advertisement. Biol. J. Linnean Soc. 17,
375-393. (doi:10.1111/j.1095-8312.1982.tb02028.x)</mixed-citation>
         </ref>
         <ref id="d836e526a1310">
            <label>18</label>
            <mixed-citation id="d836e533" publication-type="other">
Kodric-Brown, A. &amp; Brown, J. H. 1984 Truth in advertis-
ing: the kinds of traits favored by sexual selection. Am.
Nat. 124, 309-323. (doi:10.1086/284275)</mixed-citation>
         </ref>
         <ref id="d836e546a1310">
            <label>19</label>
            <mixed-citation id="d836e553" publication-type="other">
Olsson, M. 1992 Sexual selection and reproductive
strategies in the sand lizard (Lacerta agilis). PhD disser-
tation, University of Göteborg, Göteborg, Sweden.</mixed-citation>
         </ref>
         <ref id="d836e566a1310">
            <label>20</label>
            <mixed-citation id="d836e573" publication-type="other">
Wiens, J. J., Reeder, T. W. &amp; De Oca, A. N. M. 1999
Molecular phylognetics and evolution of sexual
dichroamtism among populations of the Yarrow's spiny
lizard (Sceloporus jarrovii). Evolution 53, 1884-1897.
(doi: 10.2307/2640448)</mixed-citation>
         </ref>
         <ref id="d836e592a1310">
            <label>21</label>
            <mixed-citation id="d836e599" publication-type="other">
Moll, E. O., Matson, K. E. &amp; Krehbiel, E. B. 1981
Sexual and seasonal dichromatism in the Asian river
turtle Callagur borneoensis. Herpetologica 37, 181-194.</mixed-citation>
         </ref>
         <ref id="d836e612a1310">
            <label>22</label>
            <mixed-citation id="d836e619" publication-type="other">
Salthe, S. N. 1967 Courtship patterns and the phylogeny
of urodeles. Copeia 1967, 100-117. (doi:10.2307/
1442181)</mixed-citation>
         </ref>
         <ref id="d836e633a1310">
            <label>23</label>
            <mixed-citation id="d836e640" publication-type="other">
Todd, B. D. &amp; Davis, A. K. 2007 Sexual dichromatism in
the marbled salamander, Ambystoma opacum. Can. J. Zool.
85, 1008-1013. (doi: 10.1139/Z07-082)</mixed-citation>
         </ref>
         <ref id="d836e653a1310">
            <label>24</label>
            <mixed-citation id="d836e660" publication-type="other">
Cooper, V. J. &amp; Hosey, G. R. 2003 Sexual dichromatism
and female preference in Eulemur fulvus subspecies.
Int. J. Primatol. 24, 1177-1188. (doi:10.1023/B:IJOP.
0000005986.21477.ad)</mixed-citation>
         </ref>
         <ref id="d836e676a1310">
            <label>25</label>
            <mixed-citation id="d836e683" publication-type="other">
Caro, T. 2009 Contrasting coloration in terrestrial mam-
mals. Phil. Trans. R. Soc. B 364, 537-548. (doi:10.1098/
rstb.2008.0221)</mixed-citation>
         </ref>
         <ref id="d836e696a1310">
            <label>26</label>
            <mixed-citation id="d836e703" publication-type="other">
Andersson, M. 1994 Sexual selection. Princeton, NJ:
Princeton University Press.</mixed-citation>
         </ref>
         <ref id="d836e713a1310">
            <label>27</label>
            <mixed-citation id="d836e720" publication-type="other">
Hanssen, S. A., Folstad, I. &amp; Erikstad, K. E. 2006 White
plumage reflects individual quality in female eiders.
Anim. Behav. 71, 337-343. (doi:10.1016/j.anbehav.
2005.04.021)</mixed-citation>
         </ref>
         <ref id="d836e736a1310">
            <label>28</label>
            <mixed-citation id="d836e743" publication-type="other">
Feduccia, A. &amp; Slaughter, B. H. 1973 Sexual dimor-
phism in skates (Rajidae) and its possible role in
differential niche utilization. Evolution 28, 164-168.
(doi:10.2307/2407249)</mixed-citation>
         </ref>
         <ref id="d836e760a1310">
            <label>29</label>
            <mixed-citation id="d836e767" publication-type="other">
Partridge, L. &amp; Green, P. 1985 Intraspecific feeding
specializations and population dynamics. In Behavioral
ecology: ecological consequences of adaptive behavior
(eds R. M. Sibly &amp; R. H. Smith), pp. 207-226.
Oxford, UK: Blackwell Scientific Publications.</mixed-citation>
         </ref>
         <ref id="d836e786a1310">
            <label>30</label>
            <mixed-citation id="d836e793" publication-type="other">
Shine, R. 1989 Ecological causes for the evolution of
sexual dimorphism: a review of the evidence. Q. Rev.
Biol. 64, 419-461. (doi:10.1086/416458)</mixed-citation>
         </ref>
         <ref id="d836e806a1310">
            <label>31</label>
            <mixed-citation id="d836e813" publication-type="other">
Heinsohn, R., Legge, S. &amp; Endler, J. A. 2005 Extreme
reversed sexual dichromatism in a bird without sex role
reversal. Science 309, 617-619. (doi:10.1126/science.
1112774)</mixed-citation>
         </ref>
         <ref id="d836e829a1310">
            <label>32</label>
            <mixed-citation id="d836e836" publication-type="other">
Hettyey, A., Herczeg, G. &amp; Hoi, H. 2009 Testing the
phenotype-linked fertility hypothesis in male moor frogs
(Rana arvalis ) exhibiting a conspicuous nuptial colour-
ation. Amphibia-Reptilia 30, 581-586. (doi:10.1163/
156853809789647086)</mixed-citation>
         </ref>
         <ref id="d836e855a1310">
            <label>33</label>
            <mixed-citation id="d836e862" publication-type="other">
Taylor, R. C., Buchana, B. W. &amp; Doherty, J. L. 2007
Sexual selection in the squirrel treefrog Hyla squirella:
the role of multi-modal cue assessment in female
choice. Anim. Behav. 74, 1753-1763. (doi:10.101 6/j.
anbehav.2007.03.010)</mixed-citation>
         </ref>
         <ref id="d836e881a1310">
            <label>34</label>
            <mixed-citation id="d836e888" publication-type="other">
Hamilton, W. D. &amp; Zuk, M. 1982 Heritable true fitness
and bright birds: a role for parasites? Science 218,
384-387. (doi:10.1126/science.7123238)</mixed-citation>
         </ref>
         <ref id="d836e902a1310">
            <label>35</label>
            <mixed-citation id="d836e909" publication-type="other">
Martin, J. &amp; López, P. 2010 Multimodal sexual signals in
male ocellated lizards Lacerta lepida: vitamin E in scent
and green coloration may signal male quality in different
sensory channels. Naturwissenschaften 97, 545-553.
(doi:10.1007/s00 114-010-0669-8)</mixed-citation>
         </ref>
         <ref id="d836e928a1310">
            <label>36</label>
            <mixed-citation id="d836e935" publication-type="other">
Vásquez, T. &amp; Pfennig, K. S. 2007 Looking on the bright
side: females prefer coloration indicative of male size and
condition in the sexually dichromatic spadefoot toad,
Scaphiopus couchii. Behav. Ecol. Sociobiol. 62, 127-135.
(doi:10.1007/s00265-007-0446-7)</mixed-citation>
         </ref>
         <ref id="d836e954a1310">
            <label>37</label>
            <mixed-citation id="d836e961" publication-type="other">
Hill, G. E. 1991 Plumage coloration is a sexually selected
indicator of male quality. Nature 350, 337-339. (doi: 10.
1038/350337a0)</mixed-citation>
         </ref>
         <ref id="d836e974a1310">
            <label>38</label>
            <mixed-citation id="d836e981" publication-type="other">
Pfennig, K. S. &amp; Tinsley, R. C. 2002 Different mate pre-
ferences by parasitized and unparasitized females
potentially reduces sexual selection. J. Evol. Biol. 15,
399-406. (doi:10.1046/j.1420-9101.2002.00406.x)</mixed-citation>
         </ref>
         <ref id="d836e997a1310">
            <label>39</label>
            <mixed-citation id="d836e1004" publication-type="other">
Friedman, N. R., Hofmann, C. M., Kondo, B. &amp;
Omland, K. E. 2009 Correlated evolution of migration
and sexual dichromatism in the new world orioles
(Icterus). Evolution 63, 3269-3274. (doi:10.1111/j.
1558-5646. 2009. 00792.x)</mixed-citation>
         </ref>
         <ref id="d836e1023a1310">
            <label>40</label>
            <mixed-citation id="d836e1030" publication-type="other">
Wells, H. D. 1977 The social behaviour of anuran amphi-
bians. Anim. Behav. 25, 666-693. (doi:10.1016/0003-
3472(77)901 18-X)</mixed-citation>
         </ref>
         <ref id="d836e1044a1310">
            <label>41</label>
            <mixed-citation id="d836e1051" publication-type="other">
Lyerla, T. A. &amp; Jameson, D. L. 1968 Development of
color in chimeras of pacific tree frogs. Copeia 1968,
113-128. (doi:10.2307/1441558)</mixed-citation>
         </ref>
         <ref id="d836e1064a1310">
            <label>42</label>
            <mixed-citation id="d836e1071" publication-type="other">
Bagnara, J. T. 1998 Comparative anatomy and physiology
of pigment cells in non-mammalian tissues. In The
pigmentary system: physiology and pathophysiology (eds J.
J. Nordlund, R. E. Boissy, V. J. Hearing, R. A. King &amp;
J. -P. Ortonne), pp. 9-40. New York, NY: Oxford
University Press.</mixed-citation>
         </ref>
         <ref id="d836e1094a1310">
            <label>43</label>
            <mixed-citation id="d836e1101" publication-type="other">
Frost, S. K. &amp; Robinson, S. J. 1984 Pigment cell differen-
tiation in the fire-bellied toad, Bombina orientalis.
I. Structural, chemical, and physical aspects of the
adult pigment pattern. J. Morphol. 179, 229-242.
(doi:10.1002/jmor.1051790303)</mixed-citation>
         </ref>
         <ref id="d836e1120a1310">
            <label>44</label>
            <mixed-citation id="d836e1127" publication-type="other">
Frost-Mason, S., Morrison, R. &amp; Masok, K. 1994
Pigmentation. In Amphibian biology (ed. H. Heatwole).
New South Wales, Australia: Surrey Beatty &amp; Sons.</mixed-citation>
         </ref>
         <ref id="d836e1140a1310">
            <label>45</label>
            <mixed-citation id="d836e1147" publication-type="other">
Bagnara, J. T. 1976 Color change. In Physiology of the
amphibia , vol. 3 (ed. B. Lofts), pp. 1-44. New York,
NY: Academic Press.</mixed-citation>
         </ref>
         <ref id="d836e1160a1310">
            <label>46</label>
            <mixed-citation id="d836e1167" publication-type="other">
Baker, A. S. 1951 A study of the expression of the burnsi
gene in adult Rana pipiens. J. Exp. Zool. 116, 191-229.
(doi:10.1002/jez.1401160202)</mixed-citation>
         </ref>
         <ref id="d836e1181a1310">
            <label>47</label>
            <mixed-citation id="d836e1188" publication-type="other">
Mann, M. E. &amp; Cummings, M. E. 2009 Sexual dimorph-
ism and directional sexual selection on aposematic
signals in a poison frog. Proc. Natl Acad. Sci. USA 106,
19 072-19 077. (doi:10.1073/pnas.0903327106)</mixed-citation>
         </ref>
         <ref id="d836e1204a1310">
            <label>48</label>
            <mixed-citation id="d836e1211" publication-type="other">
Veith, M., Kosuch, J., Rödel, M. O., Hillers, A., Schmitz,
A., Burger, M. &amp; Lötters, S. 2009 Multiple evolution of
sexual dichromatism in African reed frogs. Mol. Phylogenet.
Evol. 51, 388-393. (doi:10.1016/j.ympev.2008.12.022)</mixed-citation>
         </ref>
         <ref id="d836e1227a1310">
            <label>49</label>
            <mixed-citation id="d836e1234" publication-type="other">
Hayes, T. B. 1997 Hormonal mechanisms as potential
constraints on evolution: examples from the Anura. Am.
Zool 37, 482-490. (doi:10.1093/icb/37.6.482)</mixed-citation>
         </ref>
         <ref id="d836e1247a1310">
            <label>50</label>
            <mixed-citation id="d836e1254" publication-type="other">
Murphy, T. G., Hernández-Muciño, D., Osorio-
Beristain, M., Montgomerie, R. &amp; Omland, K. 2009
Carotenoid-based status signaling by females in the tropi-
cal streak-backed oriole. Behav. Ecol. 2, 1000-1006.
(doi:10.1093/beheco/arp089)</mixed-citation>
         </ref>
         <ref id="d836e1273a1310">
            <label>51</label>
            <mixed-citation id="d836e1280" publication-type="other">
Jackson, J. F., Ingram, W. &amp; Campbell, H. W. 1976 The
dorsal pigmentation pattern of snakes as an antipredator
strategy: a multivariate approach. Am. Nat. 110,
1029-1053. (doi:10.1086/283125)</mixed-citation>
         </ref>
         <ref id="d836e1296a1310">
            <label>52</label>
            <mixed-citation id="d836e1303" publication-type="other">
Pough, F. H. 1976 Multiple cryptic effects of cross-
banded and ringed patterns of snakes. Copeia 1976,
834-836. (doi:10.2307/1443481)</mixed-citation>
         </ref>
         <ref id="d836e1317a1310">
            <label>53</label>
            <mixed-citation id="d836e1324" publication-type="other">
Shine, R. &amp; Madsen, T. 1994 Sexual dichromatism in
snakes of the genus Vipera: a review and a new evolution-
ary hypothesis. J. Herpetol. 28, 114-117. (doi:10.2307/
1564692)</mixed-citation>
         </ref>
         <ref id="d836e1340a1310">
            <label>54</label>
            <mixed-citation id="d836e1347" publication-type="other">
Lindeil, L. E. &amp; Forsman, A. 1996 Sexual dichromatism
in snakes: support for the flicker-fusion hypothesis.
Can. J. Zool. 74, 2254-2256. (doi:10.1139/z96-256)</mixed-citation>
         </ref>
         <ref id="d836e1360a1310">
            <label>55</label>
            <mixed-citation id="d836e1367" publication-type="other">
Martin, T. E. 1996 Life history evolution in tropical and
south temperate birds: what do we really know? J. Avian
Biol. 27, 263-272. (doi:10.2307/3677257)</mixed-citation>
         </ref>
         <ref id="d836e1380a1310">
            <label>56</label>
            <mixed-citation id="d836e1387" publication-type="other">
Wiens, J. J. 2007 Global patterns of diversification
and species richness in amphibians. Am. Nat. 170,
S86-S106. (doi:10.1086/519396)</mixed-citation>
         </ref>
         <ref id="d836e1400a1310">
            <label>57</label>
            <mixed-citation id="d836e1407" publication-type="other">
Badyaev, A. V. &amp; Ghalambor, C. K. 1998 Does a trade-
off exist between sexual ornamentation and ecological
plasticity? Sexual dichromatism and occupied elevational
range in finches. Oikos 82, 319-24. (doi:10.2307/
3546972)</mixed-citation>
         </ref>
         <ref id="d836e1426a1310">
            <label>58</label>
            <mixed-citation id="d836e1433" publication-type="other">
Price, T. 1998 Sexual selection and natural selection
in bird speciation. Phil. Trans. R. Soc. Lond. B 353,
251-260. (doi:10.1098/rstb.1998.0207)</mixed-citation>
         </ref>
         <ref id="d836e1447a1310">
            <label>59</label>
            <mixed-citation id="d836e1454" publication-type="other">
Peterson, A. T. 1996 Geographic variation in sexual dichro-
matism in birds. Bull. Br. Ornithol. Club 116, 156-172.</mixed-citation>
         </ref>
         <ref id="d836e1464a1310">
            <label>60</label>
            <mixed-citation id="d836e1471" publication-type="other">
Figuerola, J. &amp; Green, A. J. 2000 The evolution of sexual
dimorphism in relation to mating patterns, cavity nesting,
insularity, and sympatry in the Anseriformes. Fund. Ecol.
14, 701-710. (doi:10.1046/j.1365-2435.2000.00474.x)</mixed-citation>
         </ref>
         <ref id="d836e1487a1310">
            <label>61</label>
            <mixed-citation id="d836e1494" publication-type="other">
Hebets, E. A. &amp; Papaj, D. R. 2005 Complex signal func-
tion: developing a framework of testable hypotheses.
Behav. Ecol. Sociobiol. 57, 197-214. (doi:10.1007/
s00265-004-0865-7)</mixed-citation>
         </ref>
         <ref id="d836e1510a1310">
            <label>62</label>
            <mixed-citation id="d836e1517" publication-type="other">
Bagnara, J. T. 1960 Pineal regulation of the body
lightening reaction in amphibian larvae. Science 132,
1481-1483. (doi:10.1126/science.132.3438.1481-a)</mixed-citation>
         </ref>
         <ref id="d836e1530a1310">
            <label>63</label>
            <mixed-citation id="d836e1537" publication-type="other">
Bagnara, J. T., Frost, S. K. &amp; Matsumoto, J. 1978 On the
development of pigment patterns in amphibians. Am.
Zool. 18, 301-312. (doi:10.1093/icb/18.2.301)</mixed-citation>
         </ref>
         <ref id="d836e1550a1310">
            <label>64</label>
            <mixed-citation id="d836e1557" publication-type="other">
Hailman, J. P. &amp; Jaeger, R. G. 1974 Phototactic responses
to spectrally dominant stimuli and use of colour vision
by adult anuran amphibians: a comparative study.
Anim. Behav. 22, 757-795. (doi:10.1016/0003-3472
(74)90002-5)</mixed-citation>
         </ref>
         <ref id="d836e1577a1310">
            <label>65</label>
            <mixed-citation id="d836e1584" publication-type="other">
Kondrashev, S. L., Gnyubkin, V. F. &amp; Dimentman, A. M.
1976 Role of visual stimuli in the breeding behavior of
males of the common frog Rana temporaria the
common toad Bufo bufo and the green toad Bufo viridis.
Zoologicheskii Zhurnal 55, 1027 -1037.</mixed-citation>
         </ref>
         <ref id="d836e1603a1310">
            <label>66</label>
            <mixed-citation id="d836e1610" publication-type="other">
Siddiqi, A., Cronin, T. W, Loew, E. R, Vorobyev, M. &amp;
Summers, K. 2004 Interspecific and intraspecific
views of color signals in the strawberry poison frog
Dendrobates pumilio. J. Exp. Biol. 207, 2471-2485.
(doi:10.1242/jeb.01047)</mixed-citation>
         </ref>
         <ref id="d836e1629a1310">
            <label>67</label>
            <mixed-citation id="d836e1636" publication-type="other">
Gomez, D., Richardson, C., Lengagne, T., Derex, M.,
Plenet, S., Joly, P., Léna, J. P. &amp; Théry, M. 2010 Support
for a role of colour vision in mate choice in the nocturnal
European treefrog (Hyla arborea). Behaviour 147,
1753-1 768. (doi:10.1163/000579510X534227)</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
