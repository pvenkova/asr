<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">scietechhumavalu</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100550</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Science, Technology, &amp; Human Values</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Sage Publications</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">01622439</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15528251</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">27786177</article-id>
         <title-group>
            <article-title>Between the Practical and the Academic: The Relation of Mode 1 and Mode 2 Knowledge Production in a Developing Country</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Dana G.</given-names>
                  <surname>Holland</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>9</month>
            <year>2009</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">34</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">5</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i27786175</issue-id>
         <fpage>551</fpage>
         <lpage>572</lpage>
         <permissions>
            <copyright-statement>Copyright © 2009 SAGE Publications</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/27786177"/>
         <abstract>
            <p>Growing expectation that research addresses problems in the context of application has spurred theorization about a "new mode" of production, Mode 2, which contrasts with Mode 1 or discipline-based research production in terms of animating questions, organization, and evaluation criteria. This article examines how the proposed Mode 2 form of research production and the practical role of the intellectual that it promotes align with the career trajectories and identities of academics who simultaneously engage in Mode 1 work. It focuses on a setting that is particularly susceptible to global shifts in knowledge production: a developing country and longtime object of external intervention, Malawi. Numerous contradictions are found between Mode 1 and 2 production as well as impediments to the conversion of products generated in Mode 2 into scholarly contradictions to Mode 1 development. The evidence draws from 42 interviews conducted with academics and independent researchers in Malawi during 2003 and 2004 as well as historical documents.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Notes</title>
         <ref id="d525e116a1310">
            <label>1</label>
            <mixed-citation id="d525e123" publication-type="other">
United Nations 2006 Human Development Report, Malawi
ranked 166.</mixed-citation>
         </ref>
         <ref id="d525e133a1310">
            <label>3</label>
            <mixed-citation id="d525e140" publication-type="other">
Carver (1990)</mixed-citation>
            <mixed-citation id="d525e146" publication-type="other">
Kerr and Mapanje (2002)</mixed-citation>
            <mixed-citation id="d525e152" publication-type="other">
Lwanda (1993)</mixed-citation>
            <mixed-citation id="d525e159" publication-type="other">
Zeleza (2002).</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>References</title>
         <ref id="d525e175a1310">
            <mixed-citation id="d525e179" publication-type="other">
Ajayi, J. F. Ade, Lameck K. G. Goma, and G. Ampah Johnson. 1996. The African experience
with higher education. Accra: Association of African Universities.</mixed-citation>
         </ref>
         <ref id="d525e189a1310">
            <mixed-citation id="d525e193" publication-type="other">
American Council on Education. 1964. Education for development: Report of the survey team
on education in Malawi. Washington, DC: American Council on Education.</mixed-citation>
         </ref>
         <ref id="d525e203a1310">
            <mixed-citation id="d525e207" publication-type="other">
Ashby, Eric. 1966. Universities: British, Indian, African. A study in the ecology of higher edu-
cation. Cambridge, MA: Harvard University Press.</mixed-citation>
         </ref>
         <ref id="d525e217a1310">
            <mixed-citation id="d525e221" publication-type="other">
Bourdieu, Pierre. 1975. The specificity of the scientific field and the social conditions of the
progress of reason. Social Science Information 14:20-47.</mixed-citation>
         </ref>
         <ref id="d525e232a1310">
            <mixed-citation id="d525e236" publication-type="other">
Carver, Richard. 1990. Where silence rules: The suppression of dissent in Malawi. New York,
NY: Human Rights Watch.</mixed-citation>
         </ref>
         <ref id="d525e246a1310">
            <mixed-citation id="d525e250" publication-type="other">
Chipeta, C. 1983. Maximizing resources utilization in institutions of higher learning in Africa:
A case study of Chancellor College, University of Malawi. Addis Ababa: United Nations
Economic Commission for Africa.</mixed-citation>
         </ref>
         <ref id="d525e263a1310">
            <mixed-citation id="d525e267" publication-type="other">
Collins, Randall. 1975. Conflict sociology: Toward an explanatory science. New York:
Academic Press.</mixed-citation>
         </ref>
         <ref id="d525e277a1310">
            <mixed-citation id="d525e281" publication-type="other">
Court, David. 1979. The idea of social science in East Africa: An aspect of the development of
higher education. Minerva 17:110-32.</mixed-citation>
         </ref>
         <ref id="d525e291a1310">
            <mixed-citation id="d525e295" publication-type="other">
Denzin, Norman K. 1989. Interpretive biography. Newbury Park: Sage Publications.</mixed-citation>
         </ref>
         <ref id="d525e302a1310">
            <mixed-citation id="d525e306" publication-type="other">
Dubbey, John M. 1994. Warm hearts, white hopes. Pretoria West: Penrose Book Printers.</mixed-citation>
         </ref>
         <ref id="d525e314a1310">
            <mixed-citation id="d525e318" publication-type="other">
Dudley, CO., and Kings M. Phiri. 1978. Report of the research and publications committee of
the University of Malawi. Zomba: University of Malawi.</mixed-citation>
         </ref>
         <ref id="d525e328a1310">
            <mixed-citation id="d525e332" publication-type="other">
Duque, Ricardo B., Marcus Ynalvez, R. Sooryamoorthy, Paul Mbatia, Dan-Bright S. Dzorgbo,
and Wesley Shrum. 2005. Collaboration paradox: Scientific productivity, the Internet, and
problems of research in developing areas. Social Studies of Science 35:755-85.</mixed-citation>
         </ref>
         <ref id="d525e345a1310">
            <mixed-citation id="d525e349" publication-type="other">
Gibbons, Michael. 1998. Higher education relevance in the 21st century. Washington, DC:
World Bank.</mixed-citation>
         </ref>
         <ref id="d525e359a1310">
            <mixed-citation id="d525e363" publication-type="other">
Gibbons, Michael, Camille Limoges, Helga Nowotny, Schwartzman Simon, Peter Scott, and
Martin Trow. 1994. The new production of knowledge: The dynamics of science and
research in contemporary societies. London: Sage.</mixed-citation>
         </ref>
         <ref id="d525e376a1310">
            <mixed-citation id="d525e380" publication-type="other">
Gieryn, Thomas, F. 1999. Cultural boundaries of science: Credibility on the line. Chicago:
University of Chicago Press.</mixed-citation>
         </ref>
         <ref id="d525e390a1310">
            <mixed-citation id="d525e394" publication-type="other">
Gille, Zsuzsa, and Sean O Riain. 2002. Global ethnography. Annual Review of Sociology
28:271-95.</mixed-citation>
         </ref>
         <ref id="d525e405a1310">
            <mixed-citation id="d525e409" publication-type="other">
Gondwe, G. E. 2004. 2004/5 Budget Statement Delivered in the National Assembly of Malawi.
Lilongwe: Ministry of Finance.</mixed-citation>
         </ref>
         <ref id="d525e419a1310">
            <mixed-citation id="d525e423" publication-type="other">
Harrigan, Jane. 2001. From dictatorship to democracy: Economic policy in Malawi 1964-
2000. Aldershot: Ashgate.</mixed-citation>
         </ref>
         <ref id="d525e433a1310">
            <mixed-citation id="d525e437" publication-type="other">
Heyneman, Stephen P. 2003. the history and problems in the making of education policy at the
World Bank, 1960-2000. International Journal of Educational Development 23:315-37.</mixed-citation>
         </ref>
         <ref id="d525e447a1310">
            <mixed-citation id="d525e451" publication-type="other">
Hunnings, Gordon. 1975. The realization of a dream: The University of Malawi 1964-1974.
Zomba: Government of Malawi.</mixed-citation>
         </ref>
         <ref id="d525e461a1310">
            <mixed-citation id="d525e465" publication-type="other">
Kerr, David, and Jack Mapanje. 2002. Academic freedom and the University of Malawi. Afri-
can Studies Review 45:73-92.</mixed-citation>
         </ref>
         <ref id="d525e475a1310">
            <mixed-citation id="d525e479" publication-type="other">
King, Kenneth, and Simon McGrath. 2004. Knowledge for development? Comparing British,
Japanese, Swedish and World Bank Aid. London: Zed Books, Ltd.</mixed-citation>
         </ref>
         <ref id="d525e490a1310">
            <mixed-citation id="d525e494" publication-type="other">
Lancaster, Carol. 1999. Aid to Africa: So much to do, so little done. Chicago: University of
Chicago Press.</mixed-citation>
         </ref>
         <ref id="d525e504a1310">
            <mixed-citation id="d525e508" publication-type="other">
Latham, Michael E. 2000. Modernization as ideology: American Social Science and 'Nation
Building' in the Kennedy Era. Chapel Hill: The University of North Carolina Press.</mixed-citation>
         </ref>
         <ref id="d525e518a1310">
            <mixed-citation id="d525e522" publication-type="other">
Lwanda, John L. 1993. Kamuzu Banda of Malawi: A study in promise, power and paralysis.
Glasgow, Scotland: Dudu Nsomba Publications.</mixed-citation>
         </ref>
         <ref id="d525e532a1310">
            <mixed-citation id="d525e536" publication-type="other">
Marcus, George E. 1998. Ethnography through Thick &amp; Thin. Princeton, NJ: Princeton
University Press.</mixed-citation>
         </ref>
         <ref id="d525e546a1310">
            <mixed-citation id="d525e550" publication-type="other">
McCracken, John. 2002. Special issue on Malawi: Introduction. Journal of Southern African
Studies 28: 5-7.</mixed-citation>
         </ref>
         <ref id="d525e560a1310">
            <mixed-citation id="d525e564" publication-type="other">
Mkandawire, Thandika. 1997. The social sciences in Africa: Breaking local barriers and
negotiating international presence. African Studies Review 40:15-36.</mixed-citation>
         </ref>
         <ref id="d525e575a1310">
            <mixed-citation id="d525e579" publication-type="other">
Mkandawire, Thandika. 2002. Incentives, governance, and capacity development in Africa.
African Issues 30:15-20.</mixed-citation>
         </ref>
         <ref id="d525e589a1310">
            <mixed-citation id="d525e593" publication-type="other">
Morton, Kathryn. 1975. Aid and dependence: British aid to Malawi. London: Croom Helm.</mixed-citation>
         </ref>
         <ref id="d525e600a1310">
            <mixed-citation id="d525e604" publication-type="other">
Platt, Jennifer. 1981. On interviewing one's peers. The British Journal of Sociology 32:75-91.</mixed-citation>
         </ref>
         <ref id="d525e611a1310">
            <mixed-citation id="d525e615" publication-type="other">
Sail, Ebrima. 2002. Working draft: The social science in Sub-Saharan Africa. Uppsala: The
Nordic Africa Institute and the Social Science Research Council.</mixed-citation>
         </ref>
         <ref id="d525e625a1310">
            <mixed-citation id="d525e629" publication-type="other">
Shrum, Wesley. 2005. Reagency of the Internet, or, "How I Became a Guest for Science."
Social Studies of Science 35:723-54.</mixed-citation>
         </ref>
         <ref id="d525e639a1310">
            <mixed-citation id="d525e643" publication-type="other">
Shrum, Wesley, and Yehouda Shenhav. 1995. Science and technology in less developed coun-
tries. In Handbook of Science, Technology, and Society, ed. Sheila Jasanoff, Gerry Markle,
James Peterson, and Trevor Pinch. Newbury Park: Sage.</mixed-citation>
         </ref>
         <ref id="d525e657a1310">
            <mixed-citation id="d525e661" publication-type="other">
Slaughter, Sheila, and Larry L. Leslie. 1997. Academic Capitalism: Politics, policies and the
entrepreneurial university. Baltimore: The Johns Hopkins University Press.</mixed-citation>
         </ref>
         <ref id="d525e671a1310">
            <mixed-citation id="d525e675" publication-type="other">
Sorbo, Gunnar M. 2001. Aid and academia: An uneasy relationship. Forum for Development
Studies 2:310-27.</mixed-citation>
         </ref>
         <ref id="d525e685a1310">
            <mixed-citation id="d525e689" publication-type="other">
Stifel, Laurence D., Ralph K. Davidson, and James S. Coleman, eds. 1982. Social sciences and
public policy in the developing world. Lexington, Massachusetts: D.C. Heath and Co.</mixed-citation>
         </ref>
         <ref id="d525e699a1310">
            <mixed-citation id="d525e703" publication-type="other">
Stutley, P. W. 1980. Some aspects of the contemporary scene. In Experts in Africa:
Proceedings of a Colloquium at the University of Aberdeen, ed. J. C. Stone, 76-95.
Aberdeen: Aberdeen University African Studies Group.</mixed-citation>
         </ref>
         <ref id="d525e716a1310">
            <mixed-citation id="d525e720" publication-type="other">
United States Agency for International Development (USAID). 1982. Country Development
Strategy Statement, Malawi, FY1984. Washington, DC: USAID.</mixed-citation>
         </ref>
         <ref id="d525e730a1310">
            <mixed-citation id="d525e734" publication-type="other">
Whitley, Richard. 2003. The intellectual and social organization of the sciences. 2nd ed.
Oxford: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d525e745a1310">
            <mixed-citation id="d525e749" publication-type="other">
Zeleza, Paul Tiyambe. 2002. The politics of historical and social science research in Africa.
Journal of Southern African Studies 28:9-23.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
