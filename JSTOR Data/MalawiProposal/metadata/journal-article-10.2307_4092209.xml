<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">britjpoliscie</journal-id>
         <journal-id journal-id-type="jstor">j100108</journal-id>
         <journal-title-group>
            <journal-title>British Journal of Political Science</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Cambridge University Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">00071234</issn>
         <issn pub-type="epub">14692112</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">4092209</article-id>
         <title-group>
            <article-title>When Are Monetary Commitments Credible? Parallel Agreements and the Sustainability of Currency Unions</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>David</given-names>
                  <surname>Stasavage</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Dominique</given-names>
                  <surname>Guillaume</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>1</month>
            <year>2002</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">32</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id>i378344</issue-id>
         <fpage>119</fpage>
         <lpage>146</lpage>
         <page-range>119-146</page-range>
         <permissions>
            <copyright-statement>Copyright 2002 Cambridge University Press</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/4092209"/>
         <abstract>
            <p>This article investigates the conditions which make it costly for governments to renege on institutional commitments governing monetary policy. Focusing on one such type of commitment - monetary integration - we develop and test a hypothesis which suggests that the presence of parallel international agreements plays an important role in raising the costs of exit for states which might otherwise withdraw from a monetary union. While existing political economy work on credible commitments in the area of monetary policy has had a heavy focus on countries in the European Union, we broaden the inquiry, using quantitative and qualitative evidence from the numerous African countries which have participated in monetary unions over the last forty years. Our results provide strong support for the parallel agreements hypothesis.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d274e178a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d274e185" publication-type="journal">
Kenneth
Rogoff, 'The Optimal Commitment to an Intermediate Monetary Target', Quarterly Journal of
Economics, 100 (1985), 1169-90.<object-id pub-id-type="doi">10.2307/1885679</object-id>
                  <fpage>1169</fpage>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d274e203" publication-type="journal">
Francesco Giavazzi and Marco Pagano, 'The Advantage of Tying One's Hands: EMS
Discipline and Central Bank Credibility', European Economic Review, 32 (1988), 1055-82.<person-group>
                     <string-name>
                        <surname>Giavazzi</surname>
                     </string-name>
                  </person-group>
                  <fpage>1055</fpage>
                  <volume>32</volume>
                  <source>European Economic Review</source>
                  <year>1988</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d274e235a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d274e242" publication-type="book">
Benjamijn Cohen, 'Beyond
EMU: The Problem of Sustainability', in Barry Eichengreen and Jeffrey Frieden, eds, The Political
Economy of European Monetary Unification (Boulder, Colo.: Westview, 1994), pp. 187-203.<person-group>
                     <string-name>
                        <surname>Cohen</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">Beyond EMU: The Problem of Sustainability</comment>
                  <fpage>187</fpage>
                  <source>The Political Economy of European Monetary Unification</source>
                  <year>1994</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d274e277a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d274e284" publication-type="journal">
Douglas Bernheim and
M. D. Whinston, 'Multimarket Contact and Collusive Behavior', Rand Journal of Economics, 21
(1990), 1-26.<object-id pub-id-type="doi">10.2307/2555490</object-id>
                  <fpage>1</fpage>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d274e302" publication-type="journal">
'Parallel and Overlapping Games: Theory and an Application to the
European Gas Trade', Economics and Politics, 1 (1989), 119-44.<fpage>119</fpage>
                  <volume>1</volume>
                  <source>Economics and Politics</source>
                  <year>1989</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d274e324" publication-type="journal">
'Linkage
Politics', Journal of Conflict Resolution, 41 (1997), 38-67.<object-id pub-id-type="jstor">10.2307/174486</object-id>
                  <fpage>38</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d274e340a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d274e347" publication-type="other">
Lisa Martin, Geoffrey Garrett and Jeffry Frieden in Eichengreen and
Frieden, eds, The Political Economy of European Monetary Unification</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d274e356" publication-type="journal">
Lisa Martin, 'Heterogeneity,
Linkage and Commons Problems', Journal of Theoretical Politics, 6 (1994), 473-93.<person-group>
                     <string-name>
                        <surname>Martin</surname>
                     </string-name>
                  </person-group>
                  <fpage>473</fpage>
                  <volume>6</volume>
                  <source>Journal of Theoretical Politics</source>
                  <year>1994</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d274e389a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d274e396" publication-type="journal">
Finn Kydland and Edward Prescott, 'Rules rather than Discretion: The Inconsistency of
Optimal Plans', Journal of Political Economy, 85 (1977), 473-91<object-id pub-id-type="jstor">10.2307/1830193</object-id>
                  <fpage>473</fpage>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d274e411" publication-type="book">
Alex Cukierman, Central Bank
Strategy, Credibility and Independence (Cambridge, Mass.: MIT Press, 1992)<person-group>
                     <string-name>
                        <surname>Cukierman</surname>
                     </string-name>
                  </person-group>
                  <source>Central Bank Strategy, Credibility and Independence</source>
                  <year>1992</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d274e436a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d274e443" publication-type="journal">
Alberto Alesina, 'Macroeconomic Policy in a Two-Party System as a Repeated Game',
Quarterly Journal of Economics, 102 (1987), 651-78.<object-id pub-id-type="doi">10.2307/1884222</object-id>
                  <fpage>651</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d274e459a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d274e466" publication-type="journal">
Alex Cukierman, Sebastian Edwards
and Guido Tabellini, 'Seignorage and Political Instability', American Economic Review, 82 (1992),
537-55<object-id pub-id-type="jstor">10.2307/2117320</object-id>
                  <fpage>537</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d274e485a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d274e492" publication-type="journal">
Robert Barro and Douglas Gordon, 'Rules, Discretion, and Reputation in a Model of Monetary
Policy', Journal of Monetary Economics, 12 (1983), 101-20.<person-group>
                     <string-name>
                        <surname>Barro</surname>
                     </string-name>
                  </person-group>
                  <fpage>101</fpage>
                  <volume>12</volume>
                  <source>Journal of Monetary Economics</source>
                  <year>1983</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d274e524a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d274e531" publication-type="other">
Rogoff, 'The Optimal Degree of Commitment'.</mixed-citation>
            </p>
         </fn>
         <fn id="d274e538a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d274e545" publication-type="journal">
Carl Walsh, 'Optimal Contracts for Central Bankers', American Economic Review, 85 (1995),
150-67.<object-id pub-id-type="jstor">10.2307/2118001</object-id>
                  <fpage>150</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d274e562a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d274e569" publication-type="journal">
Susanne Lohmann, 'Federalism and Central Bank Independence: The Politics of German
Monetary Policy, 1957-1992', World Politics, 50 (1998), 401-46<person-group>
                     <string-name>
                        <surname>Lohmann</surname>
                     </string-name>
                  </person-group>
                  <fpage>401</fpage>
                  <volume>50</volume>
                  <source>World Politics</source>
                  <year>1998</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d274e600" publication-type="journal">
Peter Moser, 'Checks and
Balances, and the Supply of Central Bank Independence', European Economic Review, 43 (1999),
1569-93<person-group>
                     <string-name>
                        <surname>Moser</surname>
                     </string-name>
                  </person-group>
                  <fpage>1569</fpage>
                  <volume>43</volume>
                  <source>European Economic Review</source>
                  <year>1999</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d274e634" publication-type="book">
Philip Keefer and David Stasavage, 'Bureaucratic Delegation and Political Institutions:
When are Independent Central Banks Irrelevant?' (World Bank Policy Research Working Paper, no.
2356 (2000).<person-group>
                     <string-name>
                        <surname>Keefer</surname>
                     </string-name>
                  </person-group>
                  <source>Bureaucratic Delegation and Political Institutions: When are Independent Central Banks Irrelevant?</source>
                  <year>2000</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d274e663a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d274e670" publication-type="book">
Alex Cukierman, Miguel Kiguel and Nissan Liviatan, 'How Much to Commit to an Exchange
Rate Rule: Balancing Credibility and Flexibility', in Pierre Siklos, ed., Varieties of Monetary
Reforms: Lessons and Experiences on the Road to Monetary Union (Boston, Mass.: Kluwer, 1994),
pp. 73-94.<person-group>
                     <string-name>
                        <surname>Cukierman</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">How Much to Commit to an Exchange Rate Rule: Balancing Credibility and Flexibility</comment>
                  <fpage>73</fpage>
                  <source>Varieties of Monetary Reforms: Lessons and Experiences on the Road to Monetary Union</source>
                  <year>1994</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d274e708a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d274e715" publication-type="journal">
Michael Bordo and Hugh Rockoff, 'The Gold Standard as a "Good Housekeeping Seal of
Approval" ', Journal of Economic History, 56 (1996), 389-428.<object-id pub-id-type="jstor">10.2307/2123971</object-id>
                  <fpage>389</fpage>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d274e730" publication-type="journal">
Michael Bordo and Finn
Kydland, 'The Gold Standard as a Rule: An Essay in Exploration', Explorations in Economic History,
32 (1995), 423-64.<person-group>
                     <string-name>
                        <surname>Bordo</surname>
                     </string-name>
                  </person-group>
                  <fpage>423</fpage>
                  <volume>32</volume>
                  <source>Explorations in Economic History</source>
                  <year>1995</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d274e765a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d274e772" publication-type="other">
Giavazzi and Pagano, 'The Advantage of Tying One's Hands'.</mixed-citation>
            </p>
         </fn>
         <fn id="d274e779a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d274e786" publication-type="other">
Bernheim and Whinston, 'Multimarket Contact and Collusive Behavior'.</mixed-citation>
            </p>
         </fn>
         <fn id="d274e793a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d274e802" publication-type="book">
J. B. de Loynes, A History of the West African Currency Board (London: West African
Currency Board, 1974)<person-group>
                     <string-name>
                        <surname>de Loynes</surname>
                     </string-name>
                  </person-group>
                  <source>A History of the West African Currency Board</source>
                  <year>1974</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d274e826" publication-type="book">
C. Harvey 'Monetary Independence: The Contrasting Strategies of Botswana
and Swaziland', in J. S. Salkin et al, eds, Aspects of the Botswana Economy: Selected Papers
(London: James Currey, 1997)<person-group>
                     <string-name>
                        <surname>Harvey</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">Monetary Independence: The Contrasting Strategies of Botswana (F'note continued) and Swaziland</comment>
                  <source>Aspects of the Botswana Economy: Selected Papers</source>
                  <year>1997</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d274e858" publication-type="other">
IMF, Surveys of African Economies</mixed-citation>
            </p>
         </fn>
         <fn id="d274e866a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d274e873" publication-type="book">
Barry Eichengreen, International Monetary
Arrangements for the 21st Century (Washington, DC: Brookings Institution, 1994).<person-group>
                     <string-name>
                        <surname>Eichengreen</surname>
                     </string-name>
                  </person-group>
                  <source>International Monetary Arrangements for the 21st Century</source>
                  <year>1994</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d274e898a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d274e905" publication-type="journal">
Tamim Bayoumi and Jonathan Ostry, 'Macroeconomic Shocks
and Trade Flows within Sub-Saharan Africa: Implications for Optimum Currency Arrangements',
Journal of African Economies, 6 (1997) 412-44<person-group>
                     <string-name>
                        <surname>Bayoumi</surname>
                     </string-name>
                  </person-group>
                  <fpage>412</fpage>
                  <volume>6</volume>
                  <source>Journal of African Economies</source>
                  <year>1997</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d274e940a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d274e947" publication-type="book">
Sebastian Edwards, 'The Determinants of the Choice between Fixed and
Flexible Exchange-Rate Regimes', NBER Working Paper no. 5756 (1996)<person-group>
                     <string-name>
                        <surname>Edwards</surname>
                     </string-name>
                  </person-group>
                  <source>The Determinants of the Choice between Fixed and Flexible Exchange-Rate Regimes</source>
                  <year>1996</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d274e971" publication-type="book">
Beth Simmons, Who
Adjusts? (Princeton, NJ: Princeton University Press, 1996)<person-group>
                     <string-name>
                        <surname>Simmons</surname>
                     </string-name>
                  </person-group>
                  <source>Who Adjusts?</source>
                  <year>1996</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d274e996a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d274e1003" publication-type="other">
Arthur Banks, 'Cross-National Time-Series Data Archive', State University of New York,
Binghampton.</mixed-citation>
            </p>
         </fn>
         <fn id="d274e1013a1310">
            <label>32</label>
            <p>
               <mixed-citation id="d274e1020" publication-type="journal">
D. Pregibon, 'Logistic Regression Analysis', Annals of Statistics, 9(1981), 705-24<object-id pub-id-type="jstor">10.2307/2240841</object-id>
                  <fpage>705</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d274e1033a1310">
            <label>33</label>
            <p>
               <mixed-citation id="d274e1040" publication-type="book">
Gary King, Michael Tomz and Jason Wittenberg, 'Making the Most of Statistical Analyses:
Improving Interpretation and Presentation' (paper presented to the Annual Meetings of the American
Political Science Association, Boston, 1998).<person-group>
                     <string-name>
                        <surname>King</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">Making the Most of Statistical Analyses: Improving Interpretation and Presentation</comment>
                  <source>Annual Meetings of the American Political Science Association, Boston</source>
                  <year>1998</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d274e1073a1310">
            <label>35</label>
            <p>
               <mixed-citation id="d274e1080" publication-type="other">
exit = 1 and exit = 0 sub-samples</mixed-citation>
            </p>
         </fn>
         <fn id="d274e1087a1310">
            <label>38</label>
            <p>
               <mixed-citation id="d274e1094" publication-type="journal">
Joachim Kratz, 'The
East African Currency Board', IMF Staff Papers, 13 (1966), 229-55.<person-group>
                     <string-name>
                        <surname>Kratz</surname>
                     </string-name>
                  </person-group>
                  <fpage>229</fpage>
                  <volume>13</volume>
                  <source>IMF Staff Papers</source>
                  <year>1966</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d274e1126a1310">
            <label>45</label>
            <p>
               <mixed-citation id="d274e1133" publication-type="other">
de Loynes papers, OV7/63 609/5, OV7/64 610/1</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d274e1139" publication-type="other">
de Loynes papers, 22 April 1964, Bank of England Archive, OV7/85 615/3</mixed-citation>
            </p>
         </fn>
         <fn id="d274e1146a1310">
            <label>46</label>
            <p>
               <mixed-citation id="d274e1153" publication-type="other">
De Loynes papers, 14 May 1963, OV7/61 609/3.</mixed-citation>
            </p>
         </fn>
         <fn id="d274e1160a1310">
            <label>47</label>
            <p>
               <mixed-citation id="d274e1167" publication-type="book">
IMF, Surveys of African Economies, 1969.<person-group>
                     <string-name>
                        <surname>IMF</surname>
                     </string-name>
                  </person-group>
                  <source>Surveys of African Economies</source>
                  <year>1969</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d274e1189a1310">
            <label>49</label>
            <p>
               <mixed-citation id="d274e1196" publication-type="book">
Phares Mutibwa, Uganda Since Independence (London: Hurst, 1992).<person-group>
                     <string-name>
                        <surname>Mutibwa</surname>
                     </string-name>
                  </person-group>
                  <source>Uganda Since Independence</source>
                  <year>1992</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d274e1219a1310">
            <label>51</label>
            <p>
               <mixed-citation id="d274e1226" publication-type="book">
Arthur Hazlewood, Economic Integration: The East African Experience (London: Heinemann,
1975).<person-group>
                     <string-name>
                        <surname>Hazlewood</surname>
                     </string-name>
                  </person-group>
                  <source>Economic Integration: The East African Experience</source>
                  <year>1975</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d274e1251a1310">
            <label>52</label>
            <p>
               <mixed-citation id="d274e1258" publication-type="book">
Minutes of the meeting of the
Ministers for Commerce of Tanganyika, Uganda and Kenya, Kampala, 1964, Rhodes House Library,
Oxford<source>Minutes of the meeting of the Ministers for Commerce of Tanganyika, Uganda and Kenya, Kampala, 1964</source>
                  <year>1964</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d274e1277a1310">
            <label>53</label>
            <p>
               <mixed-citation id="d274e1284" publication-type="other">
Minutes of the Kampala Agreement
of 1964, Rhodes House Library, Oxford</mixed-citation>
            </p>
         </fn>
         <fn id="d274e1294a1310">
            <label>54</label>
            <p>
               <mixed-citation id="d274e1301" publication-type="book">
Hadley Smith, Readings on Economic Development and
Administration in Tanzania (Dar es Salaam: Institute of Public Administration, 1967).<person-group>
                     <string-name>
                        <surname>Smith</surname>
                     </string-name>
                  </person-group>
                  <source>Readings on Economic Development and Administration in Tanzania</source>
                  <year>1967</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d274e1326a1310">
            <label>55</label>
            <p>
               <mixed-citation id="d274e1333" publication-type="other">
Hazlewood, Economic Integration.</mixed-citation>
            </p>
         </fn>
         <fn id="d274e1340a1310">
            <label>57</label>
            <p>
               <mixed-citation id="d274e1347" publication-type="other">
'Statuts de la Banque Centrale des Etats de l'Afrique de l'Ouest', 1973</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d274e1353" publication-type="other">
'Statuts de laBanque des Etats del'Afrique Centrale', 1972</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d274e1359" publication-type="journal">
IMF, 'The CFA Franc System',
IMF Staff Papers, 10 (1963)<person-group>
                     <string-name>
                        <surname>IMF</surname>
                     </string-name>
                  </person-group>
                  <volume>10</volume>
                  <source>IMF Staff Papers</source>
                  <year>1963</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d274e1388" publication-type="journal">
IMF, 'Financial Arrangements of Countries Using the CFA Franc',
IMF Staff Papers (1969)<person-group>
                     <string-name>
                        <surname>IMF</surname>
                     </string-name>
                  </person-group>
                  <source>IMF Staff Papers</source>
                  <year>1969</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d274e1414a1310">
            <label>58</label>
            <p>
               <mixed-citation id="d274e1421" publication-type="other">
'Statuts', 1973</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d274e1427" publication-type="other">
'Statuts', 1972</mixed-citation>
            </p>
         </fn>
         <fn id="d274e1434a1310">
            <label>59</label>
            <p>
               <mixed-citation id="d274e1441" publication-type="journal">
Rattan Bhatia, 'The West African Monetary Union: An Analytical Review', IMF Occasional
Paper, 35 (1985).<person-group>
                     <string-name>
                        <surname>Bhatia</surname>
                     </string-name>
                  </person-group>
                  <volume>35</volume>
                  <source>IMF Occasional Paper</source>
                  <year>1985</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d274e1469" publication-type="other">
the French government report, 'Les Problèmes monétaires de la Zone
Franc' (Paris: Conseil Economique et Social, 1970).</mixed-citation>
            </p>
         </fn>
         <fn id="d274e1479a1310">
            <label>60</label>
            <p>
               <mixed-citation id="d274e1486" publication-type="other">
IMF, 'Financial Arrangements'</mixed-citation>
            </p>
         </fn>
         <fn id="d274e1493a1310">
            <label>61</label>
            <p>
               <mixed-citation id="d274e1500" publication-type="book">
Africa Contemporary Record: Annual Survey and Documents, 1972-73 (New York: Africana
Publishing Company, 1974), pp. B179-B180.<fpage>B179</fpage>
                  <source>Africa Contemporary Record: Annual Survey and Documents, 1972-73</source>
                  <year>1974</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d274e1519a1310">
            <label>62</label>
            <p>
               <mixed-citation id="d274e1526" publication-type="journal">
Le Monde, 25 November 1972<issue>25 November</issue>
                  <source>Le Monde</source>
                  <year>1972</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d274e1541" publication-type="journal">
Le Monde,
23 November 1972<issue>23 November</issue>
                  <source>Le Monde</source>
                  <year>1972</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d274e1559" publication-type="other">
Africa Contemporary Record 1972-73, pp. 65-72</mixed-citation>
            </p>
         </fn>
         <fn id="d274e1566a1310">
            <label>63</label>
            <p>
               <mixed-citation id="d274e1573" publication-type="book">
Robert Julienne, Vingt Ans
d'institutions monétaires ouest-africaines: 1955-1975 (Paris: Harmattan, 1987)<person-group>
                     <string-name>
                        <surname>Julienne</surname>
                     </string-name>
                  </person-group>
                  <source>Vingt Ans d'institutions monétaires ouest-africaines: 1955-1975</source>
                  <year>1987</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d274e1597" publication-type="other">
French government report, 'Les Problèmes monétaires de la Zone Franc'.</mixed-citation>
            </p>
         </fn>
         <fn id="d274e1605a1310">
            <label>64</label>
            <p>
               <mixed-citation id="d274e1612" publication-type="book">
Serge Michailof, ed., La France et l'Afrique: Vade Mecum pour un
Nouveau Voyage (Paris: Karthala, 1993)<person-group>
                     <string-name>
                        <surname>Michailof</surname>
                     </string-name>
                  </person-group>
                  <source>La France et l'Afrique: Vade Mecum pour un Nouveau Voyage</source>
                  <year>1993</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d274e1636" publication-type="book">
Teresa Hayter, French
Aid (London: Overseas Development Institute, 1966)<person-group>
                     <string-name>
                        <surname>Hayter</surname>
                     </string-name>
                  </person-group>
                  <source>French Aid</source>
                  <year>1966</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d274e1661a1310">
            <label>65</label>
            <p>
               <mixed-citation id="d274e1668" publication-type="other">
Cameroon 1960, 1961</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d274e1674" publication-type="other">
Congo 1960</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d274e1680" publication-type="other">
Gabon-Congo, 1960</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d274e1687" publication-type="other">
Mauritania 1961</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d274e1693" publication-type="other">
Chad 1963, 1968,
1969-75, 1978</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d274e1702" publication-type="other">
Niger 1963</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d274e1708" publication-type="other">
Gabon 1964</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d274e1714" publication-type="other">
Brigitte Nouaille-Degorce, 'La Politique française
de coopération avec les états africains et malgaches au sud du Sahara, 1958-1978' (doctoral
dissertation, University of Bordeaux, 1982)</mixed-citation>
            </p>
         </fn>
         <fn id="d274e1727a1310">
            <label>66</label>
            <p>
               <mixed-citation id="d274e1734" publication-type="other">
Interview with Didier Ratsiraka</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d274e1740" publication-type="journal">
Philip Leymarie, 'Interview with Didier Ratsiraka', Revue
Française d'Etudes Politiques Africaines (January 1974), 29-40.<person-group>
                     <string-name>
                        <surname>Leymarie</surname>
                     </string-name>
                  </person-group>
                  <issue>January</issue>
                  <fpage>29</fpage>
                  <source>Revue Française d'Etudes Politiques Africaines</source>
                  <year>1974</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d274e1772a1310">
            <label>67</label>
            <p>
               <mixed-citation id="d274e1779" publication-type="other">
Nouaille-Degorce, 'Politique française de
coopération'.</mixed-citation>
            </p>
         </fn>
         <fn id="d274e1789a1310">
            <label>68</label>
            <p>
               <mixed-citation id="d274e1796" publication-type="other">
Julienne, Vingt ans d'institutions monétaires.</mixed-citation>
            </p>
         </fn>
         <fn id="d274e1803a1310">
            <label>69</label>
            <p>
               <mixed-citation id="d274e1810" publication-type="book">
William Foltz, From French West Africa to Mali Federation (New
Haven, Conn.: Yale University Press, 1965)<person-group>
                     <string-name>
                        <surname>Foltz</surname>
                     </string-name>
                  </person-group>
                  <source>From French West Africa to Mali Federation</source>
                  <year>1965</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d274e1834" publication-type="book">
Ruth Schachter-Morgenthau, Political Parties in
French-Speaking West Africa (Oxford: Clarendon Press, 1964)<person-group>
                     <string-name>
                        <surname>Schachter-Morgenthau</surname>
                     </string-name>
                  </person-group>
                  <source>Political Parties in French-Speaking West Africa</source>
                  <year>1964</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d274e1860a1310">
            <label>70</label>
            <p>
               <mixed-citation id="d274e1867" publication-type="other">
interviews with Jacques Foccart in P. Gaillard, Foccart Parle (Paris:
Jeune Afrique, 1995)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d274e1876" publication-type="journal">
Valerie Plave Bennett, 'Military Government in Mali', Journal of
Modern African Studies, 13 (1975), 249-66.<object-id pub-id-type="jstor">10.2307/160192</object-id>
                  <fpage>249</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d274e1892a1310">
            <label>71</label>
            <p>
               <mixed-citation id="d274e1899" publication-type="journal">
Le Monde, 30 November 1972.<issue>30 November</issue>
                  <source>Le Monde</source>
                  <year>1972</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d274e1915a1310">
            <label>72</label>
            <p>
               <mixed-citation id="d274e1922" publication-type="other">
interviews with Jacques Foccart by Philippe Gaillard in Foccart Parle</mixed-citation>
            </p>
         </fn>
         <fn id="d274e1929a1310">
            <label>73</label>
            <p>
               <mixed-citation id="d274e1936" publication-type="other">
Nouaille-Degorce, 'La Politique française de coopération</mixed-citation>
            </p>
         </fn>
         <fn id="d274e1943a1310">
            <label>74</label>
            <p>
               <mixed-citation id="d274e1950" publication-type="other">
David Stasavage, 'The Political Economy of Monetary Union: Evolution of the African Franc
Zone 1945-1994' (doctoral dissertation, Harvard University, 1995).</mixed-citation>
            </p>
         </fn>
         <fn id="d274e1960a1310">
            <label>75</label>
            <p>
               <mixed-citation id="d274e1967" publication-type="other">
Stasavage, 'Political Economy of Monetary Union'</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d274e1973" publication-type="journal">
David Stasavage, 'The CFA Franc
Zone and Fiscal Discipline', Journal of African Economies, 6 (1997), 132-67<person-group>
                     <string-name>
                        <surname>Stasavage</surname>
                     </string-name>
                  </person-group>
                  <fpage>132</fpage>
                  <volume>6</volume>
                  <source>Journal of African Economies</source>
                  <year>1997</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d274e2004" publication-type="book">
J. F. Bayart,
La Politique africaine de Franôois Mitterrand (Paris: Karthala, 1984)<person-group>
                     <string-name>
                        <surname>Bayart</surname>
                     </string-name>
                  </person-group>
                  <source>La Politique africaine de Franôois Mitterrand</source>
                  <year>1984</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d274e2029" publication-type="journal">
Nicolas van de Walle,
'The Decline of the Franc Zone: Monetary Politics in Francophone Africa', African Affairs, 90
(1991), 383-405.<object-id pub-id-type="jstor">10.2307/722938</object-id>
                  <fpage>383</fpage>
               </mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
