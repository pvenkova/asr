<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">jinfedise</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000007</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>The Journal of Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Oxford University Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00221899</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">41230461</article-id>
         <article-categories>
            <subj-group>
               <subject>REGIONAL AND COUNTRY EXPERIENCES</subject>
               <subj-group>
                  <subject>AFRICAN REGION</subject>
               </subj-group>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Measles Mortality Reduction and Pre-Elimination in the African Region, 2001-2009</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Balcha G.</given-names>
                  <surname>Masresha</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Amadou</given-names>
                  <surname>Fall</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Messeret</given-names>
                  <surname>Eshetu</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Steve</given-names>
                  <surname>Sosler</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Mary</given-names>
                  <surname>Alleman</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>James L.</given-names>
                  <surname>Goodson</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Reggis</given-names>
                  <surname>Katsande</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Deogratias</given-names>
                  <surname>Nshimirimana</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>15</day>
            <month>7</month>
            <year>2011</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">204</volume>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40055456</issue-id>
         <fpage>S198</fpage>
         <lpage>S204</lpage>
         <permissions>
            <copyright-statement>Copyright © 2011 Oxford University Press on behalf of the Infectious Diseases Society of America</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:href="http://dx.doi.org/10.1093/infdis/jir116"
                   xlink:title="an external site"/>
         <abstract>
            <p>Introduction. In 2001, countries in the African region adopted the measles-associated mortality reduction strategy recommended by the World Health Organization and the United Nations Children's Fund. With support from partners, these strategies were implemented during 2001-2009. Methods. To assess implementation, estimates of the first dose of measles vaccination through routine services (MCVI) and reported coverage for measles supplemental immunization activities (SIAs) were reviewed. Measles surveillance data were analyzed. Results. During 2001-2009, regional MCVI coverage increased from 56% to 69%, and &gt; 425 million children received measles vaccination through 125 SIAs. Measles case-based surveillance was established in 40 of 46 countries; the remaining 6 have aggregated case reporting. From 2001 through 2008, reported measles cases decreased by 92%, from 492,116 to 37,010; however, in 2009, cases increased to 83,625. Conclusions. The implementation of the recommended strategies led to a marked decrease in measles cases in the region; however, the outbreaks occurring since 2008 indicate suboptimal vaccination coverage. To achieve high MCVI coverage, provide a second dose through either periodic SIAs or routine services, and to ensure further progress toward attaining the regional measles pre-elimination goal by 2012, a renewed commitment from implementing partners and donors is needed.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d989e243a1310">
            <label>1</label>
            <mixed-citation id="d989e250" publication-type="other">
Guyer В, McBean AM. The epidemiology and control of measles in
Yaounde, Cameroun, 1968-1975. Int J Epidemiol 1981; 10:263-9.</mixed-citation>
         </ref>
         <ref id="d989e260a1310">
            <label>2</label>
            <mixed-citation id="d989e267" publication-type="other">
Cutts F, Henderson R, Clements C, Chen R, Patriarca P. Principles of
measles control. Bull World Health Organ 1991; 69:1-7.</mixed-citation>
         </ref>
         <ref id="d989e277a1310">
            <label>3</label>
            <mixed-citation id="d989e284" publication-type="other">
Bele O, Barakamhtiye DG. The Expanded Program on Immunization
in the WHO African region: current situation and implementation
constraints. Sante 1994; 4:137-42.</mixed-citation>
         </ref>
         <ref id="d989e297a1310">
            <label>4</label>
            <mixed-citation id="d989e304" publication-type="other">
Centers for Disease Control and Prevention. Progress toward measles
control—African region, 2001-2008. MMWR Morb Mortal Wkly Rep
2009; 58:1036-41.</mixed-citation>
         </ref>
         <ref id="d989e318a1310">
            <label>5</label>
            <mixed-citation id="d989e325" publication-type="other">
World Health Organization. WHO/UNICEF estimates of national
immunization coverage, WHO-UNICEF estimates of MCV coverage.
http://www.who.int/immunization_monitoring/routine/im-
munization_coverage/en/index.html. Accessed 19 August 2010.</mixed-citation>
         </ref>
         <ref id="d989e341a1310">
            <label>6</label>
            <mixed-citation id="d989e348" publication-type="other">
World Health Organization. Expanded programme on immunization
(EPI), measles control in the WHO African region. Wkly Epidemiol
Rec 1996; 71:201-3.</mixed-citation>
         </ref>
         <ref id="d989e361a1310">
            <label>7</label>
            <mixed-citation id="d989e368" publication-type="other">
Biellik R, Madema S, Taole A, et al. First 5 years of measles elimination
in southern Africa: 1996-2000. Lancet 2002; 359:1564-8.</mixed-citation>
         </ref>
         <ref id="d989e378a1310">
            <label>8</label>
            <mixed-citation id="d989e385" publication-type="other">
Otten M, Kezaala R, Fall A, et al. Public-health impact of accelerated
measles control in the WHO African Region 2000-03. Lancet 2005;
366:832-9.</mixed-citation>
         </ref>
         <ref id="d989e398a1310">
            <label>9</label>
            <mixed-citation id="d989e405" publication-type="other">
World Health Organization. Progress towards measles control in
WHO's African Region, 2001-2008. Wkly Epidemiol Ree 2009;
84:397-404.</mixed-citation>
         </ref>
         <ref id="d989e418a1310">
            <label>10</label>
            <mixed-citation id="d989e425" publication-type="other">
Wolfson LJ, Strebel PM, Gacic-Dobo M, Hoekstra EJ, McFarland JW,
Hersh BS. Has the 2005 measles mortality reduction goal been ach-
ieved? A natural history modelling study. Lancet 2007; 369:191-200.</mixed-citation>
         </ref>
         <ref id="d989e439a1310">
            <label>11</label>
            <mixed-citation id="d989e446" publication-type="other">
World Health Organization/United Nations Children's Fund. Global
immunization vision and strategy 2006-2015. http://www.who.int/
immunization/givs/en/index.html. Accessed 19 August 2010.</mixed-citation>
         </ref>
         <ref id="d989e459a1310">
            <label>12</label>
            <mixed-citation id="d989e466" publication-type="other">
Centers for Disease Control and Prevention. Progress in global measles
control and mortality reduction, 2000-2007. MMWR Morb Mortal
Wkly Rep 2008; 57:1303-6.</mixed-citation>
         </ref>
         <ref id="d989e479a1310">
            <label>13</label>
            <mixed-citation id="d989e486" publication-type="other">
Centers for Disease Control and Prevention. Global measles mortality,
2000-2008. MMWR Morb Mortal Wkly Rep 2009; 58:1321-6.</mixed-citation>
         </ref>
         <ref id="d989e496a1310">
            <label>14</label>
            <mixed-citation id="d989e503" publication-type="other">
World Health Organization Regional Office for Africa. Report of the second
meeting of the African regional measles technical advisory group 2008. http://
www.afro.who.int/index.php?option=com_content&amp;view=article8dd=2383:
publications-measles&amp;catid=1974&amp;Itemid=2734. Accessed 16 March 2011.</mixed-citation>
         </ref>
         <ref id="d989e519a1310">
            <label>15</label>
            <mixed-citation id="d989e526" publication-type="other">
World Health Organization, Regional Office for Africa. WHO AFRO—
IVD Programme Task Force on Immunization (TFI) Recommendations.
2008. http://www.who.int/immunization/sage/TFI_recs_AFRO_2008.pdf.
Accessed 19 August 2010.</mixed-citation>
         </ref>
         <ref id="d989e542a1310">
            <label>16</label>
            <mixed-citation id="d989e549" publication-type="other">
World Health Organization. Measles vaccines: WHO position paper.
Wkly Epidemiol Rec 2009; 84:349-60.</mixed-citation>
         </ref>
         <ref id="d989e560a1310">
            <label>17</label>
            <mixed-citation id="d989e567" publication-type="other">
Strebel PM, Papania MJ, Dayan GH, Halsey NA. Measles vaccine. In
Plotkin SA, Orenstein WA, Offit PA eds: Vaccines, 5th ed. Philadelphia:
Saunders Elsevier, 2008: 353-98.</mixed-citation>
         </ref>
         <ref id="d989e580a1310">
            <label>18</label>
            <mixed-citation id="d989e587" publication-type="other">
World Health Organization. Regional Office for Africa. Measles SIA
Field Guide, 2006. http://www.afro.who.int/index.php?option=com_
content&amp;view=article&amp;id=1939:measles-supplemental-immunization-
activities-sias-&amp;catid=1974&amp;Itemid=2734. Accessed 16 March 2011.</mixed-citation>
         </ref>
         <ref id="d989e603a1310">
            <label>19</label>
            <mixed-citation id="d989e610" publication-type="other">
World Health Organization. WHO/UNICEF Joint reporting process,
2010. http://www.who.int/immunization_monitoring/routine/joint_
reporting/en/index.html. Accessed 19 August 2010.</mixed-citation>
         </ref>
         <ref id="d989e623a1310">
            <label>20</label>
            <mixed-citation id="d989e630" publication-type="other">
Burton A, Monasch R, Lautenbach В, et al. WHO and UNICEF esti-
mates of national infant immunization coverage: methods and pro-
cesses. Bull World Health Organ 2009; 87:535-41.</mixed-citation>
         </ref>
         <ref id="d989e643a1310">
            <label>21</label>
            <mixed-citation id="d989e650" publication-type="other">
World Health Organization Regional Office for Africa. Guidelines for
measles surveillance, 2004. http://www.afro.who.int/index.php?option=
com_content&amp;view=article8dd=2383:publications-measles&amp;ccatid=1974&amp;
Itemid=2734. Accessed 16 March 2011.</mixed-citation>
         </ref>
         <ref id="d989e666a1310">
            <label>22</label>
            <mixed-citation id="d989e673" publication-type="other">
World Health Organization. Response to measles outbreaks in measles
mortality reduction settings, 2009. http://libdoc.who.int/hq/2009/
WHO_IVB_09.03_eng.pdf. Accessed 18 March 2011.</mixed-citation>
         </ref>
         <ref id="d989e687a1310">
            <label>23</label>
            <mixed-citation id="d989e694" publication-type="other">
World Health Organization Regional Office for Africa. Case-based
measles surveillance with lab confirmation, 2010. http://www.
afro.who.int/index.php?option=com_content&amp;view=article8cid=1938:
case-based-measles-surveillance-with-lab-confirmation-&amp;catid=1974&amp;
Itemid=2734. Accessed 18 March 2011.</mixed-citation>
         </ref>
         <ref id="d989e713a1310">
            <label>24</label>
            <mixed-citation id="d989e720" publication-type="other">
World Health Organization. Measles reported cases, http://www.wno.int/
immunization_monitoring/en/globalsummary/timeseries/tsincidencemea.
htm. Accessed 19 August 2010.</mixed-citation>
         </ref>
         <ref id="d989e733a1310">
            <label>25</label>
            <mixed-citation id="d989e740" publication-type="other">
Ryman T, Macauley R, Nshimirimana D. Reaching every district (RED)
approach to strengthen routine immunization services: evaluation in
the African region, 2005. J Public Health 2009; 32:18-25.</mixed-citation>
         </ref>
         <ref id="d989e753a1310">
            <label>26</label>
            <mixed-citation id="d989e760" publication-type="other">
World Health Organization, Regional Office for Africa. Implementing
the reach every district approach, a guide for district health manage-
ment teams. 2008. http://www.afro.who.int/en/clusters-a-programmes/
ard/immunization-and-vaccines-development/ivd-publications.html.
Accessed 19 August 2010.</mixed-citation>
         </ref>
         <ref id="d989e779a1310">
            <label>27</label>
            <mixed-citation id="d989e786" publication-type="other">
World Health Organization Regional Office for Africa. Evaluation
guidelines for measles supplemental immunization activities, 2006.
http://www.afro.who.int/index.php?option=com_content&amp;view=
article8rid=2383:publications-measles&amp;catid=1974&amp;Itemid=2734.
Accessed 16 March 2011.</mixed-citation>
         </ref>
         <ref id="d989e805a1310">
            <label>28</label>
            <mixed-citation id="d989e812" publication-type="other">
The Measles Initiative. The Measles Initiative, 2001. http://www.
measlesinitiative.org/. Accessed 19 August 2010.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
