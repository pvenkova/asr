<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xlink="http://www.w3.org/1999/xlink"
         article-type="research-article"
         dtd-version="1.0"
         xml:lang="en">
   <front>
      <journal-meta>
         <journal-id journal-id-type="publisher-id">africatoday</journal-id>
         <journal-title-group>
            <journal-title content-type="full">Africa Today</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Indiana University Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">00019887</issn>
         <issn pub-type="epub">15271978</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="publisher-id">africatoday.58.4.45</article-id>
         <article-id pub-id-type="doi">10.2979/africatoday.58.4.45</article-id>
         <article-categories>
            <subj-group subj-group-type="subject">
               <subject>African studies</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Studying Life Strategies of AIDS Orphans in Rural Kenya</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author" equal-contrib="yes">
               <string-name>
                  <surname>Prazak</surname>
                  <given-names>Miroslava</given-names>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date pub-type="ppub">
            <day>1</day>
            <month>6</month>
            <year>2012</year>
            <string-date>Summer 2012</string-date>
         </pub-date>
         <volume>58</volume>
         <issue>4</issue>
         <issue-id>africatoday.58.issue-4</issue-id>
         <fpage>45</fpage>
         <lpage>64</lpage>
         <permissions>
            <copyright-statement>©2012 Indiana University Press</copyright-statement>
            <copyright-year>2012</copyright-year>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/10.2979/africatoday.58.4.45"/>
         <abstract>
            <p>This paper offers an ethnographic account of the impact of the AIDS epidemic on families in rural Nyanza Province, Kenya, and life strategies of orphaned children as they negotiate their survival options in a lineage-based, agrarian society incorporated into the margins of the national and global economic system. Faced with the secrecy and shame that shroud HIV/AIDS, orphaned children face bereavement along with a drastic change in expectations and access to resources and opportunities. This article examines transformations in domestic groups necessitated by ongoing underdevelopment and economic marginalization. The orphans, often from educated, employed parents, face privation and diminished opportunities and access to resources, and depend on neighbors and kin for their basic subsistence, as well as to hold onto or gain access to their patrimony.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>en</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="parsed-citations">
         <title>NOTES</title>
         <ref id="ref1">
            <label>1.</label>
            <mixed-citation publication-type="other">Research on which this paper is based was funded by the Wenner-Gren Foundation for Anthropological Research, grant number 7644. Longitudinal research was made possible by funding from the Social Science Research Council (the American Council of Learned Societies and the Flora Hewett Foundation), the National Science Foundation, the Yale Center for International and Area Studies, the A.W. Mellon Foundation, and Bennington College Faculty Grants. I thank Jennifer Coffman, Karen Gover, and Carol Pal for their astute comments on previous drafts of the article, as well as the comments of four anonymous reviewers from<italic>Africa Today</italic>.</mixed-citation>
         </ref>
         <ref id="ref2">
            <label>2.</label>
            <mixed-citation publication-type="other">The disparity between Kenyan government and international agency figures was noted earlier. For example, in 1998 the government was estimating that around 8 percent of the country's adult population had HIV, while international health experts put the figure at twice that.</mixed-citation>
         </ref>
         <ref id="ref3">
            <label>3.</label>
            <mixed-citation publication-type="other">Though the KDHS published knowledge, attitude, and practice data on HIV, it did not include any data on prevalence.</mixed-citation>
         </ref>
         <ref id="ref4">
            <label>4.</label>
            <mixed-citation publication-type="other">A team of enumerators visited the homes and filled out questionnaires that were checked by field supervisors for completeness and internal consistency. The data were entered into the computer after leaving the field.</mixed-citation>
         </ref>
         <ref id="ref5">
            <label>5.</label>
            <mixed-citation publication-type="other">Research was interrupted midstream by the election violence of 2007–2008. A serious shortcoming to these data is that for members of the community, the orphans they were aware of were offspring of prior residents in the community, so the presence of orphaned children coming from outside the community is undervalued, as orphans taken in to perform domestic or farm work were often described as workers, maids, farm laborers, nursemaids, or babysitters. Because of the large number of workers reported in the 574 homesteads and the interruption of research, follow-up visits to determine the status of these workers were impossible to arrange.</mixed-citation>
         </ref>
         <ref id="ref6">
            <label>6.</label>
            <mixed-citation publication-type="other">Open-ended initial interviews were all carried out by the principal investigator; subsequent interviews using a schedule of questions and topics were carried out with two additional interviewers. Names of all informants have been changed to protect their identities.</mixed-citation>
         </ref>
         <ref id="ref7">
            <label>7.</label>
            <mixed-citation publication-type="other">Such a technique is advocated by several authors, especially<person-group person-group-type="author">
                  <string-name>Cook, Fritz</string-name>
               </person-group>, and<person-group person-group-type="author">
                  <string-name>Mwonya</string-name>
               </person-group>(<year>2003</year>) and<person-group person-group-type="author">
                  <string-name>Pillay</string-name>
               </person-group>(<year>2003</year>) in the collection of essays edited by<person-group person-group-type="author">
                  <string-name>Singhal</string-name>
               </person-group>and<person-group person-group-type="author">
                  <string-name>Howard</string-name>
               </person-group>(<year>2003</year>)</mixed-citation>
         </ref>
         <ref id="ref8">
            <label>8.</label>
            <mixed-citation publication-type="other">Care was taken to keep within the same communal boundaries, and in all cases those were ascertained with the<italic>abagaaka ba inyumba ikumi</italic>responsible for those communities. In many cases, the same men were in that position of responsibility throughout the decades of research.</mixed-citation>
         </ref>
         <ref id="ref9">
            <label>9.</label>
            <mixed-citation publication-type="other">The four formal parameters by which the data were disaggregated are the size of the domestic group, its development cycle stage, the type of family, and its demographic composition.</mixed-citation>
         </ref>
         <ref id="ref10">
            <label>10.</label>
            <mixed-citation publication-type="other">
               <person-group person-group-type="author">
                  <string-name>Epstein</string-name>
               </person-group>(<year>2007</year>) elaborates a model that shows the importance of concurrency of sexual partners in the spread of HIV in Africa.</mixed-citation>
         </ref>
         <ref id="ref11">
            <label>11.</label>
            <mixed-citation publication-type="other">Though primary education is free, there are still monetary requirements that pupils and their families must meet, including the purchase of a school uniform, and various activity fees and testing fees.</mixed-citation>
         </ref>
         <ref id="ref12">
            <label>12.</label>
            <mixed-citation publication-type="other">What became apparent is that this is typical for this area. The highest HIV/AIDS prevalence rate is among women aged 40–45; in many cases, women of that age have completed most of their childbearing (<collab>Kenya National Bureau of Statistics</collab>and<collab>ICF Macro</collab>
               <year>2010</year>:<fpage>214</fpage>–<lpage>215</lpage>).</mixed-citation>
         </ref>
      </ref-list>
      <ref-list content-type="parsed-citations">
         <title>REFERENCES CITED</title>
         <ref id="ref13">
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Bandawe, Chiwoza R.</string-name>
               </person-group>, and<person-group person-group-type="author">
                  <string-name>Johann Louw</string-name>
               </person-group>.<year>1997</year>.<article-title>The Experience of Family Foster Care in Malawi: A Preliminary Investigation</article-title>.<source>
                  <italic>Child Welfare</italic>
               </source>
               <volume>76</volume>(<issue>4</issue>):<fpage>535</fpage>–<lpage>548</lpage>.</mixed-citation>
         </ref>
         <ref id="ref14">
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Barnett, Tony</string-name>
               </person-group>.<year>2004</year>.<article-title>Editorial: The Cost of an HIV/AIDS Epidemic</article-title>.<source>
                  <italic>Tropical Medicine &amp; International Health</italic>
               </source>
               <volume>9</volume>(<issue>3</issue>):<fpage>315</fpage>–<lpage>318</lpage>.</mixed-citation>
         </ref>
         <ref id="ref15">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Booth, Karen</string-name>
               </person-group>.<year>2004</year>.<source>
                  <italic>Local Women, Global Science: Fighting AIDS in Kenya</italic>
               </source>.:<publisher-name>Indiana University Press</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref16">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Bradley, Candice</string-name>
               </person-group>.<year>1995</year>.<article-title>Women's Empowerment and Fertility Decline in Western Kenya</article-title>. In<source>
                  <italic>Situating Fertility: Anthropology and Demographic Enquiry</italic>
               </source>, edited by<person-group person-group-type="editor">
                  <string-name>Susan Greenhalgh</string-name>
               </person-group>.:<publisher-name>Cambridge University Press</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref17">
            <mixed-citation publication-type="book">
               <person-group person-group-type="editor">
                  <string-name>Brass, William</string-name>
               </person-group>, and<person-group person-group-type="editor">
                  <string-name>Carole Jolly</string-name>
               </person-group>, eds.<year>1993</year>.<source>
                  <italic>Population Dynamics of Kenya</italic>
               </source>.:<publisher-name>National Academy Press</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref18">
            <mixed-citation publication-type="book">
               <collab>Central Bureau of Statistics (CBS) (Kenya), Ministry of Health (MOH) (Kenya) and ORC Macro</collab>.<year>2004</year>.<source>
                  <italic>Kenya Demographic and Health Survey 2003</italic>
               </source>.:<publisher-name>CBS, MOH, and ORC Macro</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref19">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Cook, Alicia Skinner</string-name>
               </person-group>,<person-group person-group-type="author">
                  <string-name>Janet Julia Fritz</string-name>
               </person-group>, and<person-group person-group-type="author">
                  <string-name>Rose Mwonya</string-name>
               </person-group>.<year>2003</year>.<article-title>Understanding the Psychological and Emotional Needs of AIDS Orphans in Africa</article-title>. In<source>
                  <italic>The Children of Africa Confront AIDS: From Vulnerabilityto Possibility</italic>
               </source>, edited by<person-group person-group-type="editor">
                  <string-name>Arvind Singhal</string-name>
               </person-group>and<person-group person-group-type="editor">
                  <string-name>W. Stephen Howard</string-name>
               </person-group>.:<publisher-name>Ohio University Press</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref20">
            <mixed-citation publication-type="other">
               <person-group person-group-type="author">
                  <string-name>Cooper, Elizabeth</string-name>
               </person-group>.<year>2008</year>.<article-title>Children's Homes to Children's Villages</article-title>.<source>
                  <italic>Anthropology News</italic>
               </source>
               <month>March</month>:<day>26</day>–<day>27</day>.</mixed-citation>
         </ref>
         <ref id="ref21">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Epstein, Helen</string-name>
               </person-group>.<year>2007</year>.<source>
                  <italic>The Invisible Cure: Africa, the West, and the Fight against AIDS</italic>
               </source>.:<publisher-name>Farrar, Straus and Giroux</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref22">
            <mixed-citation publication-type="book">
               <person-group person-group-type="editor">
                  <string-name>Evans-Pritchard</string-name>
               </person-group>,<person-group person-group-type="editor">
                  <string-name>Edward E.</string-name>
               </person-group>, and<person-group person-group-type="editor">
                  <string-name>Meyer Fortes</string-name>
               </person-group>, eds.<year>1940</year>.<source>
                  <italic>African Political Systems</italic>
               </source>.:<publisher-name>Oxford University Press</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref23">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Floyd, Sian</string-name>
               </person-group>, et al.<year>2003</year>.<article-title>The Impact of HIV on Household Structure in Rural Malawi</article-title>. Paper presented at the<conf-name>scientific meeting on Empirical Evidence for Demographic and Socio-Economic Impact of AIDS, hosted by the Health Economics and HIV/AIDS Research Division</conf-name>,<conf-loc>Durban</conf-loc>,<conf-date>26–28 March</conf-date>. Cited and summarized in<collab>United Nations Population Division, Department of Economic and Social Affairs</collab>,<source>
                  <italic>The Impact of AIDS</italic>
               </source>(:<publisher-name>The United Nations</publisher-name>).<uri xlink:href="http://www.un.org/esa/population/publications/AIDSimpact">http://www.un.org/esa/population/publications/AIDSimpact</uri>.</mixed-citation>
         </ref>
         <ref id="ref24">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Forsythe, Steven</string-name>
               </person-group>, and<person-group person-group-type="author">
                  <string-name>Bill Rau</string-name>
               </person-group>.<year>1996</year>.<source>
                  <italic>AIDS in Kenya: Socioeconomic Impact and Policy Implications</italic>
               </source>.:<publisher-name>U.S. Agency for International Development</publisher-name>,<collab>AIDSCAP/Family Health International</collab>.</mixed-citation>
         </ref>
         <ref id="ref25">
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Foster, Geoff</string-name>
               </person-group>,<person-group person-group-type="author">
                  <string-name>Choice Makufa</string-name>
               </person-group>,<person-group person-group-type="author">
                  <string-name>Roger Drew</string-name>
               </person-group>, and<person-group person-group-type="author">
                  <string-name>Etta Kralovec</string-name>
               </person-group>.<year>1997</year>.<article-title>Factors Leading to the Establishment of Child-Headed Households: The Case of Zimbabwe</article-title>.<source>
                  <italic>Health Transition Review</italic>
               </source>
               <volume>7</volume>(supplement 2):<fpage>155</fpage>–<lpage>168</lpage>.</mixed-citation>
         </ref>
         <ref id="ref26">
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Fox, Matthew P.</string-name>
               </person-group>, et al.<year>2004</year>.<article-title>The Impact of HIV/AIDS on Labour Productivity in Kenya</article-title>.<source>
                  <italic>Tropical Medicine &amp; International Health</italic>
               </source>
               <volume>9</volume>(<issue>3</issue>):<fpage>318</fpage>–<lpage>325</lpage>.</mixed-citation>
         </ref>
         <ref id="ref27">
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Glynn, Judith R.</string-name>
               </person-group>, et al.<year>2004</year>.<article-title>Does Increased General Schooling Protect against HIV Infection? A Study of Four African Cities</article-title>.<source>
                  <italic>Tropical Medicine &amp; International Health</italic>
               </source>
               <volume>9</volume>(<issue>1</issue>):<fpage>4</fpage>–<lpage>15</lpage>.</mixed-citation>
         </ref>
         <ref id="ref28">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Guest, Emma</string-name>
               </person-group>.<year>2003</year>.<source>
                  <italic>Children of AIDS: Africa's Orphan Crisis</italic>
               </source>. Second edition.:<publisher-name>Pluto Press</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref29">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Hall, James</string-name>
               </person-group>.<year>2004</year>.<source>
                  <italic>About Us (Ngatsi)</italic>
               </source>.:<publisher-name>UNICEF</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref30">
            <mixed-citation publication-type="other">
               <person-group person-group-type="author">
                  <string-name>Kates, Jennifer</string-name>
               </person-group>, and<person-group person-group-type="author">
                  <string-name>Alyssa Wilson Leggoe</string-name>
               </person-group>.<year>2005</year>.<article-title>The HIV/AIDS Epidemic in Kenya</article-title>.<month>October</month>
               <year>2005</year>.<person-group person-group-type="author">
                  <string-name>Henry J. Kaiser</string-name>
               </person-group>
               <collab>Family Foundation HIV/AIDS Policy Fact Sheet</collab>.<uri xlink:href="http://www.kff.org/hivaids/upload7356.pdf">http://www.kff.org/hivaids/upload7356.pdf</uri>.</mixed-citation>
         </ref>
         <ref id="ref31">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Keim, Curtis</string-name>
               </person-group>.<year>2009</year>.<source>
                  <italic>Mistaking Africa: Curiosities and Inventions of the American Mind</italic>
               </source>. 2nd ed.:<publisher-name>Westview</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref32">
            <mixed-citation publication-type="book">
               <collab>Kenya National Bureau of Statistics and ICF Macro</collab>.<year>2010</year>.<source>
                  <italic>Kenya Demographic and Health Survey 2008–09</italic>
               </source>.:<publisher-name>KNBS and ICF Macro</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref33">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Kilbride, Philip L.</string-name>
               </person-group>, and<person-group person-group-type="author">
                  <string-name>Janet C. Kilbride</string-name>
               </person-group>.<year>1990</year>.<source>
                  <italic>Changing Family Life in East Africa: Women and Children at Risk</italic>
               </source>.:<publisher-name>The Pennsylvania State University Press</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref34">
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Kuper, Adam</string-name>
               </person-group>.<year>1982</year>.<article-title>Lineage Theory: A Critical Retrospect</article-title>.<source>
                  <italic>Annual Review of Anthropology</italic>
               </source>
               <volume>11</volume>:<fpage>71</fpage>–<lpage>95</lpage>.</mixed-citation>
         </ref>
         <ref id="ref35">
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Lindblade, Kim A.</string-name>
               </person-group>,<person-group person-group-type="author">
                  <string-name>Frank Odhiambo</string-name>
               </person-group>,<person-group person-group-type="author">
                  <string-name>Daniel H. Rosen</string-name>
               </person-group>, and<person-group person-group-type="author">
                  <string-name>Kevin DeCock</string-name>
               </person-group>.<year>2003</year>.<article-title>Health and Nutritional Status of Orphans &lt;6 Years Old Cared for by Relatives in Western Kenya</article-title>.<source>
                  <italic>Tropical Medicine &amp;International Health</italic>
               </source>
               <volume>8</volume>(<issue>1</issue>):<fpage>67</fpage>–<lpage>72</lpage>.</mixed-citation>
         </ref>
         <ref id="ref36">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Lindholm, Charles</string-name>
               </person-group>.<year>1982</year>.<source>
                  <italic>Generosity and Jealousy: The Swat Pukhtun of Northern Pakistan</italic>
               </source>.:<publisher-name>Columbia University Press</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref37">
            <mixed-citation publication-type="book">
               <person-group person-group-type="editor">
                  <string-name>Middleton, John</string-name>
               </person-group>, and<person-group person-group-type="editor">
                  <string-name>David Tait</string-name>
               </person-group>, eds.<year>1958</year>.<source>
                  <italic>Tribes without Rulers: Studies in African Segmentary Systems</italic>
               </source>.:<publisher-name>Routledge &amp; Kegan Paul</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref38">
            <mixed-citation publication-type="other">
               <person-group person-group-type="author">
                  <string-name>Ngome, James</string-name>
               </person-group>.<year>2003</year>.<article-title>Reporting on HIV/AIDS in Kenya</article-title>.<source>
                  <italic>Nieman Reports</italic>
               </source>
               <year>2003</year>(<season>spring</season>):<fpage>44</fpage>–<lpage>45</lpage>.</mixed-citation>
         </ref>
         <ref id="ref39">
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Ntozi, James P. M.</string-name>
               </person-group>
               <year>1997</year>.<article-title>Effect of AIDS on Children: The Problem of Orphans in Uganda</article-title>.<source>
                  <italic>Health Transition Review</italic>
               </source>
               <volume>7</volume>(supplement 2):<fpage>23</fpage>–<lpage>40</lpage>.</mixed-citation>
         </ref>
         <ref id="ref40">
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Nyambedha, Erick O.</string-name>
               </person-group>,<person-group person-group-type="author">
                  <string-name>Simiyu Wandibba</string-name>
               </person-group>, and<person-group person-group-type="author">
                  <string-name>Jens Aagaard-Hansen</string-name>
               </person-group>.<year>2003</year>.<article-title>“Retirement Lost”—The New Rise of the Elderly as Caretakers for Orphans in Western Kenya</article-title>.<source>
                  <italic>Journal of Cross-Cultural Gerontology</italic>
               </source>
               <volume>18</volume>:<fpage>3</fpage>–<lpage>52</lpage>.</mixed-citation>
         </ref>
         <ref id="ref41">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Pillay, Yegan</string-name>
               </person-group>.<year>2003</year>.<article-title>Storytelling as a Psychological Intervention for AIDS Orphans in Africa</article-title>. In<source>
                  <italic>The Children of Africa Confront AIDS: From Vulnerability to Possibility</italic>
               </source>, ed.<person-group person-group-type="editor">
                  <string-name>Arvind Singhal</string-name>
               </person-group>and<person-group person-group-type="editor">
                  <string-name>W. Stephen Howard</string-name>
               </person-group>.:<publisher-name>Ohio University Press</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref42">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Prazak, Miroslava</string-name>
               </person-group>.<year>1992</year>.<article-title>Cultural Expression of Socioeconomic Differentiation in Rural Kenya</article-title>.<source>Ph.D. thesis</source>,<publisher-name>Yale University</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref43">
            <mixed-citation publication-type="other">
               <person-group person-group-type="author">
                  <string-name>Prazak, Miroslava</string-name>
               </person-group>, and<person-group person-group-type="author">
                  <string-name>Timothy Voice</string-name>
               </person-group>.<year>2010</year>.<article-title>Population and Development in Rural Kenya: Tracking Change through Domestic Groups</article-title>. Manuscript.</mixed-citation>
         </ref>
         <ref id="ref44">
            <mixed-citation publication-type="book">
               <collab>Republic of Kenya</collab>.<year>1997</year>.<source>
                  <italic>Sessional Paper No. 4 of 1997 on AIDS in Kenya</italic>
               </source>.:<publisher-name>Republic of Kenya, Ministry of Health</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref45">
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Robalino, David A.</string-name>
               </person-group>,<person-group person-group-type="author">
                  <string-name>Albertus Voetberg</string-name>
               </person-group>, and<person-group person-group-type="author">
                  <string-name>Oscar Picazo</string-name>
               </person-group>.<year>2002</year>.<article-title>The Macroeconomic Impacts of AIDS in Kenya Estimating Optimal Reduction Targets for the HIV/AIDS Incidence Rate</article-title>.<source>
                  <italic>Journal of Policy Modeling</italic>
               </source>
               <volume>24</volume>:<fpage>195</fpage>–<lpage>218</lpage>.</mixed-citation>
         </ref>
         <ref id="ref46">
            <mixed-citation publication-type="other">
               <person-group person-group-type="author">
                  <string-name>Ruel, Malcolm</string-name>
               </person-group>.<year>1958</year>.<article-title>The Social Organization of the Kuria</article-title>. Manuscript.</mixed-citation>
         </ref>
         <ref id="ref47">
            <mixed-citation publication-type="book">
               <person-group person-group-type="editor">
                  <string-name>Rwomire, Apollo</string-name>
               </person-group>, ed.<year>2001</year>.<source>
                  <italic>African Women and Children: Crisis and Response</italic>
               </source>.:<publisher-name>Praeger</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref48">
            <mixed-citation publication-type="book">
               <person-group person-group-type="editor">
                  <string-name>Singhal, Arvind</string-name>
               </person-group>, and<person-group person-group-type="editor">
                  <string-name>W. Stephen Howard</string-name>
               </person-group>, eds.<year>2003</year>.<source>
                  <italic>The Children of Africa Confront AIDS: From Vulnerability to Possibility</italic>
               </source>.:<publisher-name>Ohio University Press</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref49">
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Siringi, Samuel</string-name>
               </person-group>.<year>2002</year>.<article-title>Kenya Issues New AIDS Bill</article-title>.<source>
                  <italic>Lancet Infectious Diseases</italic>
               </source>
               <volume>2</volume>(<issue>9</issue>):<fpage>516</fpage>.</mixed-citation>
         </ref>
         <ref id="ref50">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Tobisson, Eva</string-name>
               </person-group>.<year>1986</year>.<source>
                  <italic>Family Dynamics among the Kuria</italic>
               </source>.:<publisher-name>Acta Universiatis Gothoburgensis</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref51">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Topouzis, D.</string-name>
               </person-group>
               <year>1994</year>.<source>
                  <italic>The Socio-Economic Impact of HIV/AIDS on Rural Families with an Emphasis on Youth</italic>
               </source>.:<publisher-name>FAO</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref52">
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Townsend, Nicholas W.</string-name>
               </person-group>
               <year>1997</year>.<article-title>Men, Migration, and Households in Botswana: An Exploration of Connections over Time and Space</article-title>.<source>
                  <italic>Journal of Southern African Studies</italic>
               </source>
               <volume>25</volume>(<issue>3</issue>):<fpage>405</fpage>–<lpage>420</lpage>.</mixed-citation>
         </ref>
         <ref id="ref53">
            <mixed-citation publication-type="book">
               <collab>United Nations, Population Division, Department of Economic and Social Affairs</collab>.<year>2003</year>.<source>
                  <italic>The Impact of AIDS</italic>
               </source>.:<publisher-name>United Nations</publisher-name>.<uri xlink:href="http://www.un.org/esa/population/publications/AIDSimpact">http://www.un.org/esa/population/publications/AIDSimpact</uri>.</mixed-citation>
         </ref>
         <ref id="ref54">
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Urassa, Mark, J.</string-name>
               </person-group>,<person-group person-group-type="author">
                  <string-name>Ties Boerman</string-name>
               </person-group>,<person-group person-group-type="author">
                  <string-name>Japheth Z. L. Ng'weshemi</string-name>
               </person-group>,<person-group person-group-type="author">
                  <string-name>Raphael Isingo</string-name>
               </person-group>,<person-group person-group-type="author">
                  <string-name>Dick Schapink</string-name>
               </person-group>, and<person-group person-group-type="author">
                  <string-name>Yusufu Kumogola</string-name>
               </person-group>.<year>1997</year>.<article-title>Orphanhood, Child Fostering and the AIDS Epidemic in Rural Tanzania</article-title>.<source>
                  <italic>Health Transition Review</italic>
               </source>
               <volume>7</volume>(supplement 2):<fpage>141</fpage>–<lpage>153</lpage>.</mixed-citation>
         </ref>
         <ref id="ref55">
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Urassa, Mark, J.</string-name>
               </person-group>,<person-group person-group-type="author">
                  <string-name>Ties Boerma</string-name>
               </person-group>,<person-group person-group-type="author">
                  <string-name>Raphael Isingo</string-name>
               </person-group>,<person-group person-group-type="author">
                  <string-name>Juliana Ngalula</string-name>
               </person-group>,<person-group person-group-type="author">
                  <string-name>Japheth Ng'weshemi</string-name>
               </person-group>,<person-group person-group-type="author">
                  <string-name>Gabriel Mwaluko</string-name>
               </person-group>, and<person-group person-group-type="author">
                  <string-name>Basia Zaba</string-name>
               </person-group>.<year>2001</year>.<article-title>The Impact of HIV/AIDS on Mortality and Household Mobility in Rural Tanzania</article-title>.<source>
                  <italic>AIDS</italic>
               </source>
               <volume>15</volume>:<fpage>2017</fpage>–<lpage>2023</lpage>.</mixed-citation>
         </ref>
         <ref id="ref56">
            <mixed-citation publication-type="book">
               <person-group person-group-type="editor">
                  <string-name>Weisner, Thomas S.</string-name>
               </person-group>,<person-group person-group-type="editor">
                  <string-name>Candice Bradley</string-name>
               </person-group>, and<person-group person-group-type="editor">
                  <string-name>Philip Kilbride</string-name>
               </person-group>, eds.<year>1997</year>.<source>
                  <italic>African Families and the Crisis of Social Change</italic>
               </source>.:<publisher-name>Bergin and Garvey</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref57">
            <mixed-citation publication-type="other">
               <person-group person-group-type="author">
                  <string-name>Were, Maureen</string-name>
               </person-group>And<person-group person-group-type="author">
                  <string-name>Nancy N. Nafula</string-name>
               </person-group>.<year>2003</year>.<article-title>An Assessment of the Impact of HIV/AIDS on Economic Growth: The Case of Kenya</article-title>.<source>CESifo Working Paper 1034</source>. Presented at<conf-name>CESifo Conference on Health and Economic Policy</conf-name>,<conf-date>June 2003</conf-date>. Retrieved from<uri xlink:href="http://ssrn.com/abstract=449241,4/1/06">http://ssrn.com/abstract=449241,4/1/06</uri>.</mixed-citation>
         </ref>
         <ref id="ref58">
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Yamano, Takashi</string-name>
               </person-group>, and<person-group person-group-type="author">
                  <string-name>T. S. Jayne</string-name>
               </person-group>.<year>2004</year>.<article-title>Measuring the Impacts of Working-Age Adult Mortality on Small-Scale Farm Households in Kenya</article-title>.<source>
                  <italic>World Development</italic>
               </source>
               <volume>32</volume>(<issue>1</issue>):<fpage>91</fpage>–<lpage>120</lpage>.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
