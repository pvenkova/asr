<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">landeconomics</journal-id>
         <journal-id journal-id-type="jstor">j100261</journal-id>
         <journal-title-group>
            <journal-title>Land Economics</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Wisconsin Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">00237639</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="doi">10.2307/3146638</article-id>
         <title-group>
            <article-title>Deforestation and the Rule of Law in a Cross-Section of Countries</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Robert T.</given-names>
                  <surname>Deacon</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>11</month>
            <year>1994</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">70</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id>i357988</issue-id>
         <fpage>414</fpage>
         <lpage>430</lpage>
         <page-range>414-430</page-range>
         <permissions>
            <copyright-statement>Copyright 1994 Board of Regents of the University of Wisconsin System</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/3146638"/>
         <abstract>
            <p>Relationships between deforestation and population pressure, income growth, and insecure property rights are examined with data from 120 countries. Insecure property rights are hypothesized to arise from two sources: government instability or inability to enforce ownership and an absence of government accountability. The former source is captured by measures of general lawlessness such as guerrilla warfare, revolution, and frequent constitutional change. The latter is proxied by variables indicating the type of government executive, frequency of political purges, and the existence of an elected legislature. General support is indicated for the property rights hypothesis and for the effects of population growth.</p>
         </abstract>
         <kwd-group>
            <kwd>Q23</kwd>
            <kwd>O13</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d990e193a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d990e200" publication-type="journal">
Deacon (forthcoming  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d990e208" publication-type="book">
Mendelsohn (1991)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d990e216" publication-type="book">
Sedjo (1991)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d990e225" publication-type="book">
Hyde, Mendelsohn, and Sedjo
(1990)  </mixed-citation>
            </p>
         </fn>
         <fn id="d990e237a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d990e244" publication-type="other">
Libecap, and Schneider (1994)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d990e250" publication-type="journal">
Anderson and Lueck (1992)  </mixed-citation>
            </p>
         </fn>
         <fn id="d990e259a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d990e266" publication-type="book">
Romer (1989)  </mixed-citation>
            </p>
         </fn>
         <fn id="d990e275a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d990e282" publication-type="book">
World Resources Institute (1992)  </mixed-citation>
            </p>
         </fn>
         <fn id="d990e292a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d990e299" publication-type="book">
World Resources Institute (1992, 292)  <fpage>292</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d990e311a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d990e318" publication-type="book">
Panayotou and Sungsuwan 1989  </mixed-citation>
            </p>
         </fn>
         <fn id="d990e327a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d990e334" publication-type="journal">
Pin-
dyck (1991)  </mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d990e355a1310">
            <mixed-citation id="d990e361" publication-type="book">
Alesina, Alberto, Sule Ozler, Nouriel Roubini,
and Philip Swagel. 1991. "Political Instability
and Economic Growth." Cambridge, MA:
National Bureau of Economic Research.<person-group>
                  <string-name>
                     <surname>Alesina</surname>
                  </string-name>
               </person-group>
               <source>Political Instability and Economic Growth</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d990e393a1310">
            <mixed-citation id="d990e397" publication-type="journal">
Allen, Julia C., and Douglas F. Barnes. 1985.
"The Causes of Deforestation in Develop-
ing Countries." Annals of the Associa-
tion of American Geographers 75 (2):163-
84.<object-id pub-id-type="jstor">10.2307/2562560</object-id>
               <fpage>163</fpage>
            </mixed-citation>
         </ref>
         <ref id="d990e423a1310">
            <mixed-citation id="d990e427" publication-type="book">
Alston, Lee J., Gary D. Libecap, and Robert
Schneider. 1994. "An Analysis of Property
Rights, Land Value, and Agricultural Invest-
ment on Two Frontiers in Brazil." Depart-
ment of Economics, University of Illinois,
Urbana. Mimeo.<person-group>
                  <string-name>
                     <surname>Alston</surname>
                  </string-name>
               </person-group>
               <source>An Analysis of Property Rights, Land Value, and Agricultural Investment on Two Frontiers in Brazil</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d990e465a1310">
            <mixed-citation id="d990e469" publication-type="journal">
Anderson, Terry L., and Dean Lueck. 1992.
"Land Tenure and Agricultural Productivity
on Indian Reservations." Journal of Law and
Economics 35 (Oct.):427-54.<object-id pub-id-type="jstor">10.2307/725547</object-id>
               <fpage>427</fpage>
            </mixed-citation>
         </ref>
         <ref id="d990e493a1310">
            <mixed-citation id="d990e497" publication-type="book">
Banks, A. S. 1990. "Cross-National Time-Series
Data Archive." Center for Social Analysis,
State University of New York, Binghampton,
September 1979 (updated to 1990).<person-group>
                  <string-name>
                     <surname>Banks</surname>
                  </string-name>
               </person-group>
               <source>Cross-National Time-Series Data Archive</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d990e529a1310">
            <mixed-citation id="d990e533" publication-type="journal">
Barro, Robert. 1991. "Economic Growth in a
Cross Section of Countries." Quarterly Jour-
nal of Economics 61 (May):407-44.<object-id pub-id-type="doi">10.2307/2937943</object-id>
               <fpage>407</fpage>
            </mixed-citation>
         </ref>
         <ref id="d990e552a1310">
            <mixed-citation id="d990e556" publication-type="book">
Boado, Eufresina L. 1988. "Incentive Policies
and Forest Use in the Philippines." In Public
Policies and the Misuse of Forest Resources,
ed. R. Repetto and M. Gillis. Cambridge:
Cambridge University Press.<person-group>
                  <string-name>
                     <surname>Boado</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Incentive Policies and Forest Use in the Philippines</comment>
               <source>Public Policies and the Misuse of Forest Resources</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d990e594a1310">
            <mixed-citation id="d990e598" publication-type="journal">
Deacon, Robert T. Forthcoming. "Assessing
the Relationship Between Government Pol-
icy and Deforestation." Journal of Environ-
mental Economics and Management.<person-group>
                  <string-name>
                     <surname>Deacon</surname>
                  </string-name>
               </person-group>
               <source>Journal of Environmental Economics and Management</source>
            </mixed-citation>
         </ref>
         <ref id="d990e627a1310">
            <mixed-citation id="d990e631" publication-type="book">
Fisher, Anthony C., and W. Michael Hane-
mann. 1991. "Valuation of Tropical For-
ests." Working Paper No. 576, Department
of Agricultural and Resource Economics,
University of California, Berkeley.<person-group>
                  <string-name>
                     <surname>Fisher</surname>
                  </string-name>
               </person-group>
               <source>Valuation of Tropical Forests</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d990e666a1310">
            <mixed-citation id="d990e670" publication-type="book">
Food and Agriculture Organization (FAO).
1988. An Interim Report on the State of the
Forest Resources in the Developing Coun-
tries. Rome: United Nations.<person-group>
                  <string-name>
                     <surname>Food and Agriculture Organization (FAO)</surname>
                  </string-name>
               </person-group>
               <source>An Interim Report on the State of the Forest Resources in the Developing Countries</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d990e703a1310">
            <mixed-citation id="d990e707" publication-type="book">
Gullison, Ramond E., and Elizabeth Losos.
1992. "The Role of Foreign Debt in Defores-
tation in Latin America." Department of
Ecology and Evolutionary Biology, Prince-
ton University. Mimeo.<person-group>
                  <string-name>
                     <surname>Gullison</surname>
                  </string-name>
               </person-group>
               <source>The Role of Foreign Debt in Deforestation in Latin America</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d990e742a1310">
            <mixed-citation id="d990e746" publication-type="book">
Hyde, William, Robert Mendelsohn, and Roger
Sedjo. 1990. "The Applied Economics of
Tropical Deforestation." Yale School of For-
estry and Environmental Studies. Mimeo.<person-group>
                  <string-name>
                     <surname>Hyde</surname>
                  </string-name>
               </person-group>
               <source>The Applied Economics of Tropical Deforestation</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d990e778a1310">
            <mixed-citation id="d990e782" publication-type="book">
Kahn, James R., and Judith A. McDonald. 1991.
"Third World Debt and Tropical Deforesta-
tion." Department of Economics, University
of Tennessee. Mimeo.<person-group>
                  <string-name>
                     <surname>Kahn</surname>
                  </string-name>
               </person-group>
               <source>Third World Debt and Tropical Deforestation</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d990e814a1310">
            <mixed-citation id="d990e820" publication-type="book">
Lopez, Ramon. 1992. "Resource Degradation,
Community Controls, and Agricultural Pro-
ductivity in Tropical Areas." Department of
Economics, University of Maryland. Mimeo.<person-group>
                  <string-name>
                     <surname>Lopez</surname>
                  </string-name>
               </person-group>
               <source>Resource Degradation, Community Controls, and Agricultural Productivity in Tropical Areas</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d990e852a1310">
            <mixed-citation id="d990e856" publication-type="book">
Mendelsohn, Robert. 1991. "Property Rights
and Tropical Deforestation." Yale School of
Forestry and Environmental Studies. Mimeo.<person-group>
                  <string-name>
                     <surname>Mendelsohn</surname>
                  </string-name>
               </person-group>
               <source>Property Rights and Tropical Deforestation</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d990e885a1310">
            <mixed-citation id="d990e889" publication-type="book">
Ozler, Sule, and Dani Rodrik. 1992. "External
Shocks, Politics, and Private Investment:
Some Theory and Empirical Evidence."
Working Paper No. 3960, National Bureau of
Economic Research, Cambridge, MA.<person-group>
                  <string-name>
                     <surname>Ozler</surname>
                  </string-name>
               </person-group>
               <source>External Shocks, Politics, and Private Investment: Some Theory and Empirical Evidence</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d990e925a1310">
            <mixed-citation id="d990e929" publication-type="book">
Panayotou, Theodore. 1990. "The Economics
of Environmental Degradation: Problems,
Causes, and Responses." Harvard Institute
for International Development. Mimeo.<person-group>
                  <string-name>
                     <surname>Panayotou</surname>
                  </string-name>
               </person-group>
               <source>The Economics of Environmental Degradation: Problems, Causes, and Responses</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d990e961a1310">
            <mixed-citation id="d990e965" publication-type="book">
Panayotou, Theodore, and Somthawin Sungsu-
wan. 1989. "An Econometric Study of the
Causes of Tropical Deforestation: The Case
of Northeast Thailand." Discussion Paper
No. 284, Harvard Institute for International
Development.<person-group>
                  <string-name>
                     <surname>Panayotou</surname>
                  </string-name>
               </person-group>
               <source>An Econometric Study of the Causes of Tropical Deforestation: The Case of Northeast Thailand</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d990e1003a1310">
            <mixed-citation id="d990e1007" publication-type="book">
Pearce, David, and Camille Bann. 1993. "North-
South Transfers and the Capture of Global
Environmental Value." Center for Social and
Economic Research on the Global Environ-
ment, University College, London. Mimeo.<person-group>
                  <string-name>
                     <surname>Pearce</surname>
                  </string-name>
               </person-group>
               <source>NorthSouth Transfers and the Capture of Global Environmental Value</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d990e1042a1310">
            <mixed-citation id="d990e1046" publication-type="book">
Persson, Torsten, and Guido Tabellini. 1990. "Is
Inequality Harmful for Growth?" Working
Paper, Department of Economics, University
of California, Los Angeles.<person-group>
                  <string-name>
                     <surname>Persson</surname>
                  </string-name>
               </person-group>
               <source>Is Inequality Harmful for Growth?</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d990e1078a1310">
            <mixed-citation id="d990e1082" publication-type="journal">
Peters, Charles M., Alwyn J. Gentry, and Rob-
ert 0. Mendelsohn. 1989. "Valuation of an
Amazonian Rainforest." Nature 399 (June
29):655-56.<person-group>
                  <string-name>
                     <surname>Peters</surname>
                  </string-name>
               </person-group>
               <issue>June 29</issue>
               <fpage>655</fpage>
               <volume>399</volume>
               <source>Nature</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d990e1123a1310">
            <mixed-citation id="d990e1127" publication-type="journal">
Pindyck, Robert S. 1991. "Irreversibility, Un-
certainty, and Investment." Journal of Eco-
nomic Literature 29 (Sept.): 1110-48.<object-id pub-id-type="jstor">10.2307/2727613</object-id>
               <fpage>1110</fpage>
            </mixed-citation>
         </ref>
         <ref id="d990e1147a1310">
            <mixed-citation id="d990e1151" publication-type="book">
Repetto, Robert, William Magrath, Michael
Wells, Christine Beer, and Fabrizio Rossini.
1989. Wasting Assets: Natural Resources in
the National Income Accounts. Washington,
DC: World Resources Institute.<person-group>
                  <string-name>
                     <surname>Repetto</surname>
                  </string-name>
               </person-group>
               <source>Wasting Assets: Natural Resources in the National Income Accounts</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d990e1186a1310">
            <mixed-citation id="d990e1190" publication-type="journal">
Reynolds, Lloyd G. 1983. "The Spread of Eco-
nomic Growth to the Third World: 1850-
1980." Journal of Economic Literature 21
(Sept.):941-80.<object-id pub-id-type="jstor">10.2307/2724912</object-id>
               <fpage>941</fpage>
            </mixed-citation>
         </ref>
         <ref id="d990e1213a1310">
            <mixed-citation id="d990e1217" publication-type="book">
Romer, Paul. 1989. "Capital Accumulation in
the Theory of Long-Run Growth." In Mod-
ern Business Cycle Theory, ed. R. Barro.
Cambridge: Harvard University Press.<person-group>
                  <string-name>
                     <surname>Romer</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Capital Accumulation in the Theory of Long-Run Growth</comment>
               <source>Modern Business Cycle Theory</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d990e1252a1310">
            <mixed-citation id="d990e1256" publication-type="book">
Sedjo, Roger A. 1991. "Tropical Forests, Land
Use and Environmental Values: Economic
Concepts and Real World Complexities."
Resources for the Future, Washington, DC.
Mimeo.<person-group>
                  <string-name>
                     <surname>Sedjo</surname>
                  </string-name>
               </person-group>
               <source>Tropical Forests, Land Use and Environmental Values: Economic Concepts and Real World Complexities</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d990e1291a1310">
            <mixed-citation id="d990e1295" publication-type="journal">
---. 1992. "Property Rights, Genetic Re-
sources, and Biotechnological Change."
Journal of Law and Economics 35 (Apr.):
199-213.<object-id pub-id-type="jstor">10.2307/725560</object-id>
               <fpage>199</fpage>
            </mixed-citation>
         </ref>
         <ref id="d990e1318a1310">
            <mixed-citation id="d990e1322" publication-type="book">
Solorzano, Raul, Ronnie de Camino, Richard
Woodward, Joseph Tosi, Vincente Watson,
Alexis Vasquez, Carlos Villalobos, Jorge Ji-
menez, Robert Repetto, and Wilfredo Cruz.
1991. Accounts Overdue: Natural Resource
Depreciation in Costa Rica. Washington,
DC: World Resources Institute.<person-group>
                  <string-name>
                     <surname>Solorzano</surname>
                  </string-name>
               </person-group>
               <source>Accounts Overdue: Natural Resource Depreciation in Costa Rica</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d990e1364a1310">
            <mixed-citation id="d990e1368" publication-type="journal">
Southgate, Douglas, Rodrigo Sierra, and Law-
rence Brown. 1991. "The Causes of Tropical
Deforestation in Ecuador: A Statistical Anal-
ysis." World Development 19 (9):1145-51.<person-group>
                  <string-name>
                     <surname>Southgate</surname>
                  </string-name>
               </person-group>
               <issue>9</issue>
               <fpage>1145</fpage>
               <volume>19</volume>
               <source>World Development</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d990e1409a1310">
            <mixed-citation id="d990e1413" publication-type="journal">
Summers, Robert, and Alan Heston. 1991. "The
Penn World Table Mark V." Quarterly Jour-
nal of Economics 61 (May):225-39.<person-group>
                  <string-name>
                     <surname>Summers</surname>
                  </string-name>
               </person-group>
               <issue>May</issue>
               <fpage>225</fpage>
               <volume>61</volume>
               <source>Quarterly Journal of Economics</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d990e1451a1310">
            <mixed-citation id="d990e1455" publication-type="book">
World Resources Institute. 1992. World Re-
sources, 1992-93. New York: Oxford Uni-
versity Press.<person-group>
                  <string-name>
                     <surname>World Resources Institute</surname>
                  </string-name>
               </person-group>
               <source>World Resources, 1992-93</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d990e1484a1310">
            <mixed-citation id="d990e1488" publication-type="book">
World Bank. 1992. World Development Report
1992: Development and the Environment.
Oxford: Oxford University Press.<person-group>
                  <string-name>
                     <surname>World Bank</surname>
                  </string-name>
               </person-group>
               <source>World Development Report 1992: Development and the Environment</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d990e1517a1310">
            <mixed-citation id="d990e1521" publication-type="book">
World Rainforest Movement. 1990. Rainforest
Destruction: Causes, Effects, and False So-
lutions. Penang, Malaysia: World Rainforest
Movement.<person-group>
                  <string-name>
                     <surname>World Rainforest Movement</surname>
                  </string-name>
               </person-group>
               <source>Rainforest Destruction: Causes, Effects, and False Solutions</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
