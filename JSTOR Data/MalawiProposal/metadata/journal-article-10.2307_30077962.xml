<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">jinfedise</journal-id>
         <journal-id journal-id-type="jstor">j50000007</journal-id>
         <journal-title-group>
            <journal-title>The Journal of Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Chicago Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">00221899</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">30077962</article-id>
         <article-categories>
            <subj-group>
               <subject>Major Articles</subject>
               <subj-group>
                  <subject>HIV/AIDS</subject>
               </subj-group>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Association of Levels of HIV-1-Infected Breast Milk Cells and Risk of Mother-to-Child Transmission</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Christine M.</given-names>
                  <surname>Rousseau</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Ruth W.</given-names>
                  <surname>Nduati</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Barbra A.</given-names>
                  <surname>Richardson</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Grace C.</given-names>
                  <surname>John-Stewart</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Dorothy A.</given-names>
                  <surname>Mbori-Ngacha</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Joan K.</given-names>
                  <surname>Kreiss</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Julie</given-names>
                  <surname>Overbaugh</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>15</day>
            <month>11</month>
            <year>2004</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">190</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">10</issue>
         <issue-id>i30077939</issue-id>
         <fpage>1880</fpage>
         <lpage>1888</lpage>
         <permissions>
            <copyright-statement>Copyright 2004 Infectious Diseases Society of America</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/30077962"/>
         <abstract>
            <p>Understanding how the level of human immunodeficiency virus type 1 (HlV-l)-infected breast milk cells (BMCs) affects HIV transmission via breast-feeding can shed light on the mechanism of infection and aid in establishing effective interventions. The proportion of infected cells to total cells was measured in serial breast milk samples collected from 291 HIV-1-infected women in Nairobi, Kenya, by use of real-time DNA polymerase chain reaction amplification of BMCs. The number of infected BMCs per million cells was associated with levels of cell-free viral RNA in breast milk (R = .144; P = .032), levels of cell-free virus in blood plasma (R = .365; P&lt;.001), and the detection of proviral DNA in cervical and vaginal secretions (P&lt;.001 and P = .030, respectively). The number of infected BMCs per million cells was lower in colostrum or early milk than in mature milk (P&lt;.001). Previous studies demonstrated that the concentration of BMCs varies throughout lactation, and we used these data to transform infected BMCs per million cells to infected BMCs per milliliter. The estimated concentration of infected BMCs per milliliter was higher in colostrum or early milk than in mature milk (P&lt;OOl). Each log10 increase in infected BMCs per milliliter was associated with a 3.19-foldincreased risk of transmission (P = .002), after adjustment for cell-free virus in plasma (hazard ratio [HR], 2.09; P = .03) and breast milk (HR, 1.01; P = 1.00). This suggests that infected BMCs may play a more important role in transmission of HIV via breast-feeding than does cell-free virus.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d470e248a1310">
            <label>1</label>
            <mixed-citation id="d470e255" publication-type="other">
Nduati R, John G, Mbori-Ngacha D, et al. Effect of breastfeeding and
formula feeding on transmission of HIV-1: a randomized clinical trial.
JAMA 2000;283:1167-74.</mixed-citation>
         </ref>
         <ref id="d470e268a1310">
            <label>2</label>
            <mixed-citation id="d470e275" publication-type="other">
Nduati RW, John GC, Richardson BA, et al. Human immunodeficiency
virus type 1-infected cells in breast milk: association with immuno-
suppression and vitamin A deficiency. J Infect Dis 1995; 172:1461-8.</mixed-citation>
         </ref>
         <ref id="d470e288a1310">
            <label>3</label>
            <mixed-citation id="d470e295" publication-type="other">
Rousseau CM, Nduati RW, Richardson BA, et al. Longitudinal analysis
of human immunodeficiency virus type 1 RNA in breast milk and of
its relationship to infant infection and maternal disease. J Infect Dis
2003; 187:741-7.</mixed-citation>
         </ref>
         <ref id="d470e311a1310">
            <label>4</label>
            <mixed-citation id="d470e318" publication-type="other">
Lewis P, Nduati R, Kreiss JK, et al. Cell-free human immunodeficiency
virus type 1 in breast milk. J Infect Dis 1998; 177:34-9.</mixed-citation>
         </ref>
         <ref id="d470e329a1310">
            <label>5</label>
            <mixed-citation id="d470e336" publication-type="other">
Pillay K, Coutsoudis A, York D, Kuhn L, Coovadia HM. Cell-free virus
in breast milk of HIV- 1-seropositive women. J Acquir Immune Defic
Syndr 2000; 24:330-6.</mixed-citation>
         </ref>
         <ref id="d470e349a1310">
            <label>6</label>
            <mixed-citation id="d470e358" publication-type="other">
Van de Perre P, Simonon A, Hitimana DG, et al. Infective and anti-
infective properties of breastmilk from HIV-1-infected women. Lancet
1993;341:914-8.</mixed-citation>
         </ref>
         <ref id="d470e371a1310">
            <label>7</label>
            <mixed-citation id="d470e378" publication-type="other">
Janoff EN, Smith PD. Emerging concepts in gastrointestinal aspects of
HIV-1 pathogenesis and management. Gastroenterology 2001; 120:
607-21.</mixed-citation>
         </ref>
         <ref id="d470e391a1310">
            <label>8</label>
            <mixed-citation id="d470e398" publication-type="other">
Bomsel M. Transcytosis of infectious human immunodeficiency virus
across a tight human epithelial cell line barrier. Nat Med 1997; 3:42-7.</mixed-citation>
         </ref>
         <ref id="d470e408a1310">
            <label>9</label>
            <mixed-citation id="d470e415" publication-type="other">
Lagaye S, Derrien M, Menu E, et al. Cell-to-cell contact results in a
selective translocation of maternal human immunodeficiency virus type
1 quasispecies across a trophoblastic barrier by both transcytosis and
infection. J Virol 2001;75:4780-91.</mixed-citation>
         </ref>
         <ref id="d470e431a1310">
            <label>10</label>
            <mixed-citation id="d470e438" publication-type="other">
Hocini H, Becquart P, Bouhlal H, et al. Active and selective transcytosis
of cell-free human immunodeficiency virus through a tight polarized
monolayer of human endometrial cells. J Virol 2001;75:5370-4.</mixed-citation>
         </ref>
         <ref id="d470e452a1310">
            <label>11</label>
            <mixed-citation id="d470e459" publication-type="other">
Meng G, Wei X, Wu X, et al. Primary intestinal epithelial cells selectively
transfer R5 HIV-1 to  cells. Nat Med 2002;8:150-6.</mixed-citation>
         </ref>
         <ref id="d470e472a1310">
            <label>12</label>
            <mixed-citation id="d470e479" publication-type="other">
Stahl-Hennig C, Steinman RM, Tenner-Racz K, et al. Rapid infection
of oral mucosal-associated lymphoid tissue with simian immunode-
ficiency virus. Science 1999;285:1261-5.</mixed-citation>
         </ref>
         <ref id="d470e492a1310">
            <label>13</label>
            <mixed-citation id="d470e499" publication-type="other">
Baba TW, Trichel AM, An L, et al. Infection and AIDS in adult ma-
caques after nontraumatic oral exposure to cell-free SIV. Science 1996;
272:1486-9.</mixed-citation>
         </ref>
         <ref id="d470e512a1310">
            <label>14</label>
            <mixed-citation id="d470e519" publication-type="other">
Ruprecht RM, Baba TW, Liska V, et al. Oral SIV, SHIV, and HIV type
1 infection. AIDS Res Hum Retroviruses 1998; 14(Suppl 1):S97-103.</mixed-citation>
         </ref>
         <ref id="d470e529a1310">
            <label>15</label>
            <mixed-citation id="d470e536" publication-type="other">
Girard M, Mahoney J, Wei Q, et al. Genital infection of female chim-
panzees with human immunodeficiency virus type 1. AIDS Res Hum
Retroviruses 1998;14:1357-67.</mixed-citation>
         </ref>
         <ref id="d470e549a1310">
            <label>16</label>
            <mixed-citation id="d470e556" publication-type="other">
Hu J, Gardner MB, Miller CJ. Simian immunodeficiency virus rapidly
penetrates the cervicovaginal mucosa after intravaginal inoculation and
infects intraepithelial dendritic cells. J Virol 2000; 74:6087-95.</mixed-citation>
         </ref>
         <ref id="d470e570a1310">
            <label>17</label>
            <mixed-citation id="d470e577" publication-type="other">
Johansson BG. Isolation of crystalline lactoferrin from human milk.
Acta Chem Scand 1969;23:683-4.</mixed-citation>
         </ref>
         <ref id="d470e587a1310">
            <label>18</label>
            <mixed-citation id="d470e594" publication-type="other">
Michie CA, Tantscher E, Schall T, Rot A. Physiological secretion of
chemokines in human breast milk. Eur Cytokine Netw 1998;9:123-9.</mixed-citation>
         </ref>
         <ref id="d470e604a1310">
            <label>19</label>
            <mixed-citation id="d470e611" publication-type="other">
Wahl SM, McNeely TB, Janoff EN, et al. Secretory leukocyte protease
inhibitor (SLPI) in mucosal fluids inhibits HIV-I. Oral Dis 1997;3(Suppl
l):S64-9.</mixed-citation>
         </ref>
         <ref id="d470e624a1310">
            <label>20</label>
            <mixed-citation id="d470e631" publication-type="other">
Hocini H, Becquart P, Bouhlal H, Adle-Biassette H, Kazatchkine MD,
Belec L. Secretory leukocyte protease inhibitor inhibits infection of
monocytes and lymphocytes with human immunodeficiency virus type
1 but does not interfere with transcytosis of cell-associated virus across
tight epithelial barriers. Clin Diagn Lab Immunol 2000;7:515-8.</mixed-citation>
         </ref>
         <ref id="d470e650a1310">
            <label>21</label>
            <mixed-citation id="d470e657" publication-type="other">
Verani A, Lusso P. Chemokines as natural HIV antagonists. Curr Mol
Med 2002;2:691-702.</mixed-citation>
         </ref>
         <ref id="d470e667a1310">
            <label>22</label>
            <mixed-citation id="d470e674" publication-type="other">
van der Strate BW, Beljaars L, Molema G, Harmsen MC, Meijer DK.
Antiviral activities of lactoferrin. Antiviral Res 2001;52:225-39.</mixed-citation>
         </ref>
         <ref id="d470e685a1310">
            <label>23</label>
            <mixed-citation id="d470e692" publication-type="other">
Semba RD, Kumwenda N, Hoover DR, et al. Human immunodeficiency
virus load in breast milk, mastitis, and mother-to-child transmission of
human immunodeficiency virus type 1. J Infect Dis 1999; 180:93-8.</mixed-citation>
         </ref>
         <ref id="d470e705a1310">
            <label>24</label>
            <mixed-citation id="d470e712" publication-type="other">
Coutsoudis A, Pillay K, Kuhn L, Spooner E, Tsai WY, Coovadia HM.
Method of feeding and transmission of HIV-1 from mothers to children
by 15 months of age: prospective cohort study from Durban, South
Africa. AIDS 2001; 15:379-87.</mixed-citation>
         </ref>
         <ref id="d470e728a1310">
            <label>25</label>
            <mixed-citation id="d470e735" publication-type="other">
Miotti PG, Taha TE, Kumwenda NI, et al. HIV transmission through
breastfeeding: a study in Malawi. JAMA 1999;282:744-9.</mixed-citation>
         </ref>
         <ref id="d470e745a1310">
            <label>26</label>
            <mixed-citation id="d470e752" publication-type="other">
Efficacy of three short-course regimens of zidovudine and lamivudine
in preventing early and late transmission of HIV-1 from mother to
child in Tanzania, South Africa, and Uganda (Petra study): a random-
ised, double-blind, placebo-controlled trial. Lancet 2002;359:1178-86.</mixed-citation>
         </ref>
         <ref id="d470e768a1310">
            <label>27</label>
            <mixed-citation id="d470e775" publication-type="other">
Leroy V, Newell ML, Dabis F, et al. International multicentre pooled
analysis of late postnatal mother-to-child transmission of HIV-1 in-
fection. Ghent International Working Group on Mother-to-Child
Transmission of HIV. Lancet 1998;352:597-600.</mixed-citation>
         </ref>
         <ref id="d470e791a1310">
            <label>28</label>
            <mixed-citation id="d470e798" publication-type="other">
John GC, Nduati RW, Mbori-Ngacha D, et al. Genital shedding of
human immunodeficiency virus type 1 DNA during pregnancy: as-
sociation with immunosuppression, abnormal cervical or vaginal dis-
charge, and severe vitamin A deficiency. J Infect Dis 1997; 175:57-62.</mixed-citation>
         </ref>
         <ref id="d470e815a1310">
            <label>29</label>
            <mixed-citation id="d470e822" publication-type="other">
Neilson JR, John GC, Carr JK, et al. Subtypes of human immunodefi-
ciency virus type 1 and disease stage among women in Nairobi, Kenya.
J Virol 1999;73:4393-403.</mixed-citation>
         </ref>
         <ref id="d470e835a1310">
            <label>30</label>
            <mixed-citation id="d470e842" publication-type="other">
John GC, Nduati RW, Mbori-Ngacha DA, et al. Correlates of mother-
to-child human immunodeficiency virus type 1 (HIV-1) transmission:
association with maternal plasma HIV-1 RNA load, genital HIV-1 DNA
shedding, and breast infections. J Infect Dis 2001; 183:206-12.</mixed-citation>
         </ref>
         <ref id="d470e858a1310">
            <label>31</label>
            <mixed-citation id="d470e865" publication-type="other">
Clouse KA, Powell D, Washington I, et al. Monokine regulation of
human immunodeficiency virus-1 expression in a chronically infected
human T cell clone. J Immunol 1989; 142:431-8.</mixed-citation>
         </ref>
         <ref id="d470e878a1310">
            <label>32</label>
            <mixed-citation id="d470e885" publication-type="other">
Heid CA, Stevens J, Livak KJ, Williams PM. Real time quantitative
PCR. Genome Res 1996;6:986-94.</mixed-citation>
         </ref>
         <ref id="d470e895a1310">
            <label>33</label>
            <mixed-citation id="d470e902" publication-type="other">
Burns CC, Gleason LM, Mozaffarian A, Giachetti C, Carr JK, Over-
baugh J. Sequence variability of the integrase protein from a diverse
collection of HIV type 1 isolates representing several subtypes. AIDS
Res Hum Retroviruses 2002; 18:1031-41.</mixed-citation>
         </ref>
         <ref id="d470e918a1310">
            <label>34</label>
            <mixed-citation id="d470e925" publication-type="other">
Poss M, Overbaugh J. Variants from the diverse virus population iden-
tified at seroconversion of a clade A human immunodeficiency virus
type 1-infected woman have distinct biological properties. J Virol 1999;
73:5255-64.</mixed-citation>
         </ref>
         <ref id="d470e942a1310">
            <label>35</label>
            <mixed-citation id="d470e949" publication-type="other">
Moss GB, Overbaugh J, Welch M, et al. Human immunodeficiency
virus DNA in urethral secretions in men: association with gonococcal
urethritis and CD4 cell depletion. J Infect Dis 1995; 172:1469-74.</mixed-citation>
         </ref>
         <ref id="d470e962a1310">
            <label>36</label>
            <mixed-citation id="d470e971" publication-type="other">
Panteleeff DD, John G, Nduati R, et al. Rapid method for screening
dried blood samples on filter paper for human immunodeficiency virus
type 1 DNA. J Clin Microbiol 1999;37:350-3.</mixed-citation>
         </ref>
         <ref id="d470e984a1310">
            <label>37</label>
            <mixed-citation id="d470e991" publication-type="other">
Goldman AS, Garza C, Nichols BL, Goldblum RM. Immunologic fac-
tors in human milk during the first year of lactation. J Pediatr 1982;
100:563-7.</mixed-citation>
         </ref>
         <ref id="d470e1004a1310">
            <label>38</label>
            <mixed-citation id="d470e1011" publication-type="other">
Garcia PM, Kalish LA, Pitt J, et al. Maternal levels of plasma human
immunodeficiency virus type 1 RNA and the risk of perinatal trans-
mission. Women and Infants Transmission Study Group. N Engl J Med
1999;341:394-402.</mixed-citation>
         </ref>
         <ref id="d470e1027a1310">
            <label>39</label>
            <mixed-citation id="d470e1034" publication-type="other">
Leroy V, Montcho C, Manigart O, et al. Maternal plasma viral load,
zidovudine, and mother-to-child transmission of HIV-1 in Africa:
DITRAME ANRS 049a trial. AIDS 2001; 15:517-22.</mixed-citation>
         </ref>
         <ref id="d470e1047a1310">
            <label>40</label>
            <mixed-citation id="d470e1054" publication-type="other">
Semba RD, Kumwenda N, Taha TE, et al. Mastitis and immunological
factors in breast milk of lactating women in Malawi. Clin Diagn Lab
Immunol 1999;6:671-4. 40.</mixed-citation>
         </ref>
         <ref id="d470e1068a1310">
            <label>41</label>
            <mixed-citation id="d470e1075" publication-type="other">
Iversen AK, Larsen AR, Jensen T, et al. Distinct determinants of human
immunodeficiency virus type 1 RNA and DNA loads in vaginal and
cervical secretions. J Infect Dis 1998;177:1214-20.</mixed-citation>
         </ref>
         <ref id="d470e1088a1310">
            <label>42</label>
            <mixed-citation id="d470e1095" publication-type="other">
Ball JK, Curran R, Irving WL, Dearden AA. HIV-1 in semen: deter-
mination of proviral and viral titres compared to blood, and quanti-
fication of semen leukocyte populations. J Med Virol 1999; 59:356-63.</mixed-citation>
         </ref>
         <ref id="d470e1108a1310">
            <label>43</label>
            <mixed-citation id="d470e1115" publication-type="other">
Verhofstede C, Reniers S, Van Wanzeele F, Plum J. Evaluation of pro-
viral copy number and plasma RNA level as early indicators of pro-
gression in HIV-1 infection: correlation with virological and immu-
nological markers of disease. AIDS 1994;8:1421-7.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
