<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">mycologia</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100297</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Mycologia</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Mycological Society of America</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00275514</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15572536</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="doi">10.2307/3761435</article-id>
         <article-categories>
            <subj-group>
               <subject>Plant Pathogens</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Identification of the Causal Agent of Armillaria Root Rot of Pinus Species in South Africa</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>Martin P. A. Coetzee</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Brenda D.</given-names>
                  <surname>Wingfield</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Teresa A.</given-names>
                  <surname>Coutinho</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Michael J.</given-names>
                  <surname>Wingfield</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>7</month>
            <year>2000</year>
      
            <day>1</day>
            <month>8</month>
            <year>2000</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">92</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i290168</issue-id>
         <fpage>777</fpage>
         <lpage>785</lpage>
         <page-range>777-785</page-range>
         <permissions>
            <copyright-statement>Copyright 2000 The Mycological Society of America</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/3761435"/>
         <abstract>
            <p>Armillaria root rot was reported on economically important Pinus and Eucalyptus species grown in plantations in South Africa since the early 1900s. Armillaria species have been well studied in North America and Europe, but have received minimal attention in South Africa. Most reports of Armillaria root rot in South Africa suggest that A. mellea is the causal agent. The name A. heimii has also been used in more recent reports, although these have not been based on mycological studies. The taxonomic disposition of Armillaria in South Africa, therefore, remains unknown. The aim of this study was to identify and characterize Armillaria isolates from forest plantations in South Africa based on morphology, PCR-RFLP profiles and sequence data. Analysis of these characters revealed that the isolates originating from the plantations in South Africa are distinct from A. mellea and A. heimii. We believe that they represent A. fuscipes.</p>
         </abstract>
         <kwd-group>
            <kwd>Armillaria fuscipes</kwd>
            <kwd>PCR-RFLP</kwd>
            <kwd>IGS-1</kwd>
            <kwd>Phylogeny</kwd>
            <kwd>rRNA</kwd>
         </kwd-group>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Literature Cited</title>
         <ref id="d267e250a1310">
            <mixed-citation id="d267e254" publication-type="journal">
Anderson JB, Bailey SS, Pukkila PJ. 1989. Variation in ri¬
bosomal DNA among biological species of Armillaria,
a genus of root-infecting fungi. Evolution43:1652-
1662<object-id pub-id-type="doi">10.2307/2409381</object-id>
               <fpage>1652</fpage>
            </mixed-citation>
         </ref>
         <ref id="d267e277a1310">
            <mixed-citation id="d267e281" publication-type="journal">
—, Korhonen K, Ullrich RC. 1980. Relationships be¬
tween European and North American biological spe¬
cies of Armillaria mellea. Exp Mycol4:87-95<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Anderson</surname>
                  </string-name>
               </person-group>
               <fpage>87</fpage>
               <volume>4</volume>
               <source>Exp Mycol</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
         <ref id="d267e316a1310">
            <mixed-citation id="d267e320" publication-type="journal">
-, Stasovski E. 1992. Molecular phylogeny of Northern
Hemisphere species of Armillaria. Mycologia84:505-
516<object-id pub-id-type="doi">10.2307/3760315</object-id>
               <fpage>505</fpage>
            </mixed-citation>
         </ref>
         <ref id="d267e339a1310">
            <mixed-citation id="d267e343" publication-type="journal">
-, Ullrich RC. 1979. Biological species of Armillaria
mellea in North America. Mycologia71:402-414<object-id pub-id-type="doi">10.2307/3759160</object-id>
               <fpage>402</fpage>
            </mixed-citation>
         </ref>
         <ref id="d267e360a1310">
            <mixed-citation id="d267e364" publication-type="journal">
Augustain A, Mohammed C, Guillaumin J-J, Botton B. 1994.
Discrimination of some African Armillaria species by
isozyme electrophoretic analysis. New Phytol128:135-
143<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Augustain</surname>
                  </string-name>
               </person-group>
               <fpage>135</fpage>
               <volume>128</volume>
               <source>New Phytol</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d267e402a1310">
            <mixed-citation id="d267e406" publication-type="journal">
Bonde MR, MicalesJA, Peterson GL. 1993. The use of iso¬
zyme analysis for identification of plant-pathogenic
fungi. Pl Dis77:961-968<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Bonde</surname>
                  </string-name>
               </person-group>
               <fpage>961</fpage>
               <volume>77</volume>
               <source>Pl Dis</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d267e441a1310">
            <mixed-citation id="d267e445" publication-type="journal">
Bottomley AM. 1937. Some of the more important diseases
affecting timber plantations in the Transvaal. S African
J Sci33:373-376<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Bottomley</surname>
                  </string-name>
               </person-group>
               <fpage>373</fpage>
               <volume>33</volume>
               <source>S African J Sci</source>
               <year>1937</year>
            </mixed-citation>
         </ref>
         <ref id="d267e480a1310">
            <mixed-citation id="d267e484" publication-type="journal">
Chandra A, Watling R. 1981. Studies in Indian Armillaria
(Fries per Fries) Staude (Basidiomycotina). Kavaka10:
63-84<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Chandra</surname>
                  </string-name>
               </person-group>
               <fpage>63</fpage>
               <volume>10</volume>
               <source>Kavaka</source>
               <year>1981</year>
            </mixed-citation>
         </ref>
         <ref id="d267e519a1310">
            <mixed-citation id="d267e523" publication-type="journal">
Coetzee MPA, Wingfield BD, Harrington TC, Dalevi D, Cou-
tinho TA, Wingfield MJ. 2000. Geographical diversity
of Armillaria mellea s. s. based on phylogenetic analysis.
Mycologia92:105-113<object-id pub-id-type="doi">10.2307/3761454</object-id>
               <fpage>105</fpage>
            </mixed-citation>
         </ref>
         <ref id="d267e546a1310">
            <mixed-citation id="d267e550" publication-type="book">
—, -, Wingfield MJ, Coutinho TA. 1997. Char-
acteristics of South African Armillaria isolates using
RFLPs. Proceedings of the 35th Congress of the South-
ern African Society for Plant Pathology, Badplaas, 19-
22 January 1997<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Coetzee</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Characteristics of South African Armillaria isolates using RFLPs</comment>
               <source>Proceedings of the 35th Congress of the Southern African Society for Plant Pathology, Badplaas, 19-22 January 1997</source>
               <year>1997</year>
            </mixed-citation>
            <mixed-citation id="d267e587" publication-type="journal">
S African J Sci93:vi (abstract)<fpage>vi</fpage>
               <volume>93</volume>
               <source>S African J Sci</source>
            </mixed-citation>
         </ref>
         <ref id="d267e604a1310">
            <mixed-citation id="d267e610" publication-type="book">
—,-,-,-. 1998. Identification of the
causal agent of Armillaria root rot in South African
forest plantations. In: Delatour C, Guillaumin JJ, Lung-
Escarmant B, Marcais B, eds. Root and butt rots of for-
est trees, Carcans-Maubuisson (France), 1-7 Septem-
ber 1997. Paris: INRA (Les Collogues no. 89). p 49-61<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Coetzee</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Identification of the causal agent of Armillaria root rot in South African forest plantations</comment>
               <fpage>49</fpage>
               <source>Root and butt rots of forest trees, Carcans-Maubuisson (France), 1-7 September 1997</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d267e654a1310">
            <mixed-citation id="d267e658" publication-type="journal">
Duschesne LC, Anderson JB. 1990. Location and direction
of transcription of the 5S rRNA gene in Armillaria. My-
col Res94:266-269<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Duschesne</surname>
                  </string-name>
               </person-group>
               <fpage>266</fpage>
               <volume>94</volume>
               <source>Mycol Res</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d267e693a1310">
            <mixed-citation id="d267e697" publication-type="book">
Guillaumin J-J, Mohammed C, Abomo-Ndongo S. 1993a.
Vegetative incompatibility and sexual systems of Armil-
laria isolates from tropical Africa. In: Johansson M,
Stenlid J, eds. Proceedings of the 8th IUFRO Confer-
ence on Root and Butt Rots. Uppsala: Swedish Univ.,
Agricultural Science. p 349-355<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Guillaumin</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Vegetative incompatibility and sexual systems of Armillaria isolates from tropical Africa</comment>
               <fpage>349</fpage>
               <source>Proceedings of the 8th IUFRO Conference on Root and Butt Rots</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d267e741a1310">
            <mixed-citation id="d267e745" publication-type="journal">
—,-, Lung B, Marxmuller H, Anselmi N, Cour-
tecuisse R, Gregory SC, Holdenrieder O, Intini M,
Lung B, et al. 1993b. Geographical distribution and
ecology of the Armillaria species in Western Europe.
Eur J For Pathol23:321-341<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Guillaumin</surname>
                  </string-name>
               </person-group>
               <fpage>321</fpage>
               <volume>23</volume>
               <source>Eur J For Pathol</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d267e786a1310">
            <mixed-citation id="d267e790" publication-type="journal">
Harrington TC, Wingfield BD. 1995. A PCR-based identifi¬
cation method for species of Armillaria. Mycologia87:
280-288<object-id pub-id-type="doi">10.2307/3760915</object-id>
               <fpage>280</fpage>
            </mixed-citation>
         </ref>
         <ref id="d267e809a1310">
            <mixed-citation id="d267e813" publication-type="book">
—, Worrall JJ, Baker FA. 1992. Armillaria. In: LL Sin-
gleton, Mihail JD, Rush C, eds. Methods for research
on soilborne phytopathogenic fungi. St. Paul, Minne-
sota: American Phytopathological Society Press. p 81-
85<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Harrington</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Armillaria</comment>
               <fpage>81</fpage>
               <source>Methods for research on soilborne phytopathogenic fungi</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d267e855a1310">
            <mixed-citation id="d267e859" publication-type="journal">
Hasegawa M, Kishino H, Yano T-A. 1985. Dating of the hu-
man-ape splitting by a molecular clock of mitochon¬
drial DNA. J Mol Evol22:160-174<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Hasegawa</surname>
                  </string-name>
               </person-group>
               <fpage>160</fpage>
               <volume>22</volume>
               <source>J Mol Evol</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d267e894a1310">
            <mixed-citation id="d267e898" publication-type="journal">
Heim R. 1963. VArmillariella elegans Heim. Rev Mycol (Par-
is) 28:88-94<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Heim</surname>
                  </string-name>
               </person-group>
               <fpage>88</fpage>
               <volume>28</volume>
               <source>Rev Mycol (Paris)</source>
               <year>1963</year>
            </mixed-citation>
         </ref>
         <ref id="d267e930a1310">
            <mixed-citation id="d267e934" publication-type="journal">
Hori H, Higo K, Osawa S. 1977. The rates of evolution in
some ribosomal components. J Mol Evol9:191-201<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Hori</surname>
                  </string-name>
               </person-group>
               <fpage>191</fpage>
               <volume>9</volume>
               <source>J Mol Evol</source>
               <year>1977</year>
            </mixed-citation>
         </ref>
         <ref id="d267e966a1310">
            <mixed-citation id="d267e970" publication-type="book">
Hsiau PT-W. 1996. The taxonomy and phylogeny of the my-
cangial fungi from Dendroctonus brevicomis and D. fron-
talis (Coleoptera: Scolytidae) [PhD Thesis]. Ames,
Iowa: Iowa State Univ. 92 p.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Hsiau</surname>
                  </string-name>
               </person-group>
               <fpage>92</fpage>
               <source>The taxonomy and phylogeny of the mycangial fungi from Dendroctonus brevicomis and D. frontalis (Coleoptera: Scolytidae)</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d267e1005a1310">
            <mixed-citation id="d267e1009" publication-type="book">
Ivory MH. 1987. Diseases and disorders of pines in the trop-
ics. A field and laboratory manual. Overseas Research
Publication Number 31. Oxfordshire: Burgess &amp; Son
(Abingdon) Ltd. 92 p.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Ivory</surname>
                  </string-name>
               </person-group>
               <fpage>92</fpage>
               <source>Diseases and disorders of pines in the tropics. A field and laboratory manual</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d267e1044a1310">
            <mixed-citation id="d267e1048" publication-type="journal">
Kile GA, Watling R. 1988. Identification and occurrence of
Australian Armillaria species, including A. pallidula sp.
nov. and comparative studies between them and non-
Australian tropical and Indian Armillaria. Trans Br My-
col Soc91:305-315<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Kile</surname>
                  </string-name>
               </person-group>
               <fpage>305</fpage>
               <volume>91</volume>
               <source>Trans Br Mycol Soc</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d267e1090a1310">
            <mixed-citation id="d267e1094" publication-type="book">
Kotzé JJ. 1935. Forest fungi: the position in South Africa.
In: Papers and statements on exotics: 4th British Em-
pire Forestry Conference. Pretoria: The Government
Printer. 12 p.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Kotzé</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Forest fungi: the position in South Africa</comment>
               <fpage>12</fpage>
               <source>Papers and statements on exotics: 4th British Empire Forestry Conference</source>
               <year>1935</year>
            </mixed-citation>
         </ref>
         <ref id="d267e1132a1310">
            <mixed-citation id="d267e1136" publication-type="book">
Largent DL. 1977. How to identify mushrooms to genus I:
macroscopic features. Eureka, California: Mad River
Press Inc. 86 p.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Largent</surname>
                  </string-name>
               </person-group>
               <fpage>86</fpage>
               <source>How to identify mushrooms to genus I: macroscopic features</source>
               <year>1977</year>
            </mixed-citation>
         </ref>
         <ref id="d267e1168a1310">
            <mixed-citation id="d267e1172" publication-type="book">
—, Johnson D, Watling R. 1977. How to identify mush-
rooms to genus III: microscopic features. Eureka, Cal¬
ifornia: Mad River Press Inc. 148 p.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Largent</surname>
                  </string-name>
               </person-group>
               <fpage>148</fpage>
               <source>How to identify mushrooms to genus III: microscopic features</source>
               <year>1977</year>
            </mixed-citation>
         </ref>
         <ref id="d267e1204a1310">
            <mixed-citation id="d267e1208" publication-type="book">
Lückhoff HA. 1964. Diseases of exotic plantation trees in
the Republic of South Africa. In: FAO/IUFRO Sym-
posium on Internationally Dangerous Forest Diseases
and Insects. Oxford, UK: Food and Agricultural Orga-
nization; International Union of Forestry Research Or-
ganizations. 4 p.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Lückhoff</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Diseases of exotic plantation trees in the Republic of South Africa</comment>
               <fpage>4</fpage>
               <source>FAO/IUFRO Symposium on Internationally Dangerous Forest Diseases and Insects</source>
               <year>1964</year>
            </mixed-citation>
         </ref>
         <ref id="d267e1252a1310">
            <mixed-citation id="d267e1256" publication-type="journal">
LundquistJE. 1987. A history of five forest diseases in South
Africa. S African J Forest140:51-59<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Lundquist</surname>
                  </string-name>
               </person-group>
               <fpage>51</fpage>
               <volume>140</volume>
               <source>S African J Forest</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d267e1288a1310">
            <mixed-citation id="d267e1292" publication-type="book">
Mohammed C. 1994. The detection and species identifica-
tion of African Armillaria. In: Schots A, Dewey FM, Ol-
iver RP, eds. Modern assays for plant pathogenic fungi,
identification, detection and quantification. Oxford:
CAB International. p 141-147<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Mohammed</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The detection and species identification of African Armillaria</comment>
               <fpage>141</fpage>
               <source>Modern assays for plant pathogenic fungi, identification, detection and quantification</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d267e1334a1310">
            <mixed-citation id="d267e1338" publication-type="book">
—, Guillaumin J-J, Botton B, Intini M. 1993. Species of
Armillaria in tropical Africa. In: Johansson M, Stenlid
J, eds. Proceedings of the 8th IUFRO Conference on
root and butt rots. Uppsala: Swedish Univ. of Agricul¬
tural Science. p 402-410<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Mohammed</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Species of Armillaria in tropical Africa</comment>
               <fpage>402</fpage>
               <source>Proceedings of the 8th IUFRO Conference on root and butt rots</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d267e1379a1310">
            <mixed-citation id="d267e1383" publication-type="journal">
Morrison DJ, Thomson AJ, Chu D, Peet FG, Sahota TS.
1985. Isozyme patterns of Armillaria intersterility
groups occurring in British Columbia. Can J Microbiol
31:651-653<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Morrison</surname>
                  </string-name>
               </person-group>
               <fpage>651</fpage>
               <volume>31</volume>
               <source>Can J Microbiol</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d267e1421a1310">
            <mixed-citation id="d267e1425" publication-type="journal">
Mwangi LM, Lin D, Hubbes M. 1989. Identification of Ken-
yan Armillaria isolates by cultural morphology, interste¬
rility tests and analysis of isozyme profiles. Eur J For
Pathol19:399-406<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Mwangi</surname>
                  </string-name>
               </person-group>
               <fpage>399</fpage>
               <volume>19</volume>
               <source>Eur J For Pathol</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d267e1463a1310">
            <mixed-citation id="d267e1467" publication-type="book">
—, Mwenje E, Makambila C, Chanakira-Nyahwa F, Guil¬
laumin J-J, Mohammed C. 1993. Ecology and patho-
genicity of Armillaria in Kenya, Zimbabwe and the
Congo. In: Johansson M, Stenlid J, eds. Proceedings of
the 8th IUFRO Conference on Root and Butt Rots.
Uppsala: Swedish Univ. of Agricultural Science. p 34-
44<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Mwangi</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Ecology and pathogenicity of Armillaria in Kenya, Zimbabwe and the Congo</comment>
               <fpage>34</fpage>
               <source>Proceedings of the 8th IUFRO Conference on Root and Butt Rots</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d267e1515a1310">
            <mixed-citation id="d267e1519" publication-type="book">
Mwenje E, Ride JP. 1993. Characterisation of Zimbabwean
Armillaria by morphological features and isozymes
analysis of cell wall degrading enzymes. In: Johansson
M, Stenlid J, eds. Proceedings of the 8th IUFRO Con-
ference on Root and Butt Rots. Uppsala: Swedish Univ.
of Agricultural Science. p. 393-401<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Mwenje</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Characterisation of Zimbabwean Armillaria by morphological features and isozymes analysis of cell wall degrading enzymes</comment>
               <fpage>393</fpage>
               <source>Proceedings of the 8th IUFRO Conference on Root and Butt Rots</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d267e1563a1310">
            <mixed-citation id="d267e1569" publication-type="journal">
—,-. 1996. Morphological and biochemical char-
acterisation of Armillaria isolates from Zimbabwe. Pl
Pathol45:1036-1051<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Mwenje</surname>
                  </string-name>
               </person-group>
               <fpage>1036</fpage>
               <volume>45</volume>
               <source>Pl Pathol</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d267e1605a1310">
            <mixed-citation id="d267e1609" publication-type="journal">
Pegler DN. 1977. A preliminary agaric flora of East Africa.
Kew Bull Addit Ser6:91-95<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Pegler</surname>
                  </string-name>
               </person-group>
               <fpage>91</fpage>
               <volume>6</volume>
               <source>Kew Bull Addit Ser</source>
               <year>1977</year>
            </mixed-citation>
         </ref>
         <ref id="d267e1641a1310">
            <mixed-citation id="d267e1645" publication-type="journal">
Petch T1909. New Ceylon fungi. Ann Roy Bot Gard Pera-
deniyae4:299-386<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Petch</surname>
                  </string-name>
               </person-group>
               <fpage>299</fpage>
               <volume>4</volume>
               <source>Ann Roy Bot Gard Peradeniyae</source>
               <year>1909</year>
            </mixed-citation>
         </ref>
         <ref id="d267e1677a1310">
            <mixed-citation id="d267e1681" publication-type="journal">
Pole Evans IB. 1933. Safe guarding the soil products of the
Union. Farming South Africa8:486-493<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Pole Evans</surname>
                  </string-name>
               </person-group>
               <fpage>486</fpage>
               <volume>8</volume>
               <source>Farming South Africa</source>
               <year>1933</year>
            </mixed-citation>
         </ref>
         <ref id="d267e1713a1310">
            <mixed-citation id="d267e1717" publication-type="book">
Rayner RW. 1970. A mycological colour chart. Kew, Surrey,
UK Commonwealth Mycological Institute and British
Mycological Society. 34 p.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Rayner</surname>
                  </string-name>
               </person-group>
               <fpage>34</fpage>
               <source>A mycological colour chart</source>
               <year>1970</year>
            </mixed-citation>
         </ref>
         <ref id="d267e1749a1310">
            <mixed-citation id="d267e1753" publication-type="journal">
Rishbeth J. 1978. Effects of soil temperature and atmo-
sphere on growth of Armillaria rhizomorphs. Trans Br
Mycol Soc70:213-220<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Rishbeth</surname>
                  </string-name>
               </person-group>
               <fpage>213</fpage>
               <volume>70</volume>
               <source>Trans Br Mycol Soc</source>
               <year>1978</year>
            </mixed-citation>
         </ref>
         <ref id="d267e1788a1310">
            <mixed-citation id="d267e1792" publication-type="journal">
Roll-Hansen F. 1985. The Armillaria species in Europe. Eur
J For Pathol15:22-31<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Roll-Hansen</surname>
                  </string-name>
               </person-group>
               <fpage>22</fpage>
               <volume>15</volume>
               <source>Eur J For Pathol</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d267e1825a1310">
            <mixed-citation id="d267e1829" publication-type="journal">
Smith ML, Anderson JB. 1989. Restriction fragment length
polymorphisms in mitochondrial DNAs of Armillaria:
identification of North American biological species.
Mycol Res93:247-256<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Smith</surname>
                  </string-name>
               </person-group>
               <fpage>247</fpage>
               <volume>93</volume>
               <source>Mycol Res</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d267e1867a1310">
            <mixed-citation id="d267e1871" publication-type="journal">
Swift MJ. 1968. Inhibition of rhizomorph development by
Armillaria mellea in Rhodesian forest soils. Trans Br
Mycol Soc51:241-247<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Swift</surname>
                  </string-name>
               </person-group>
               <fpage>241</fpage>
               <volume>51</volume>
               <source>Trans Br Mycol Soc</source>
               <year>1968</year>
            </mixed-citation>
         </ref>
         <ref id="d267e1906a1310">
            <mixed-citation id="d267e1910" publication-type="book">
Swofford DL. 1998. PAUP*: Phylogenetic Analyis Using Par-
simony (* and other methods). vers: 4. Sunderland,
Massachusetts: Sinauer Associates<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Swofford</surname>
                  </string-name>
               </person-group>
               <source>PAUP*: Phylogenetic Analyis Using Parsimony (* and other methods). vers: 4</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d267e1939a1310">
            <mixed-citation id="d267e1943" publication-type="journal">
Veldman GM, Klootwijk J, de Regt VCHF, Rudi RJ. 1981.
The primary and secondary structure of yeasts 26S
rRNA. Nucl Acids Res9:6935-6952<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Veldman</surname>
                  </string-name>
               </person-group>
               <fpage>6935</fpage>
               <volume>9</volume>
               <source>Nucl Acids Res</source>
               <year>1981</year>
            </mixed-citation>
         </ref>
         <ref id="d267e1978a1310">
            <mixed-citation id="d267e1982" publication-type="book">
Volk TJ, Burdsall HH. 1995. A nomenclatural study of Ar-
millaria and Armillariella species. Synopsis Fungorum
8. Oslo: Fungiflora. 121 p.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Volk</surname>
                  </string-name>
               </person-group>
               <fpage>121</fpage>
               <source>A nomenclatural study of Armillaria and Armillariella species. Synopsis Fungorum 8</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d267e2014a1310">
            <mixed-citation id="d267e2018" publication-type="journal">
Wingfield MJ, Knox-Davies PS. 1980. Observations on dis¬
eases in pine and eucalyptus plantations in South Af¬
rica. Phytophylactica12:57-63<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Wingfield</surname>
                  </string-name>
               </person-group>
               <fpage>57</fpage>
               <volume>12</volume>
               <source>Phytophylactica</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
