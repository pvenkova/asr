<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">clininfedise</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j101405</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Clinical Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Chicago Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">10584838</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15376591</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">40309372</article-id>
         <article-id pub-id-type="pub-doi">10.1086/598977</article-id>
         <article-categories>
            <subj-group>
               <subject>HIV/AIDS</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Tuberculosis Treatment and Risk of Stavudine Substitution in First-Line Antiretroviral Therapy</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Daniel J.</given-names>
                  <surname>Westreich</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Ian</given-names>
                  <surname>Sanne</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Mhairi</given-names>
                  <surname>Maskew</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Babatyi</given-names>
                  <surname>Malope-Kgokong</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Francesca</given-names>
                  <surname>Conradie</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Pappie</given-names>
                  <surname>Majuba</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Michele Jonsson</given-names>
                  <surname>Funk</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jay S.</given-names>
                  <surname>Kaufman</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Annelies</given-names>
                  <surname>Van Rie</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Patrick</given-names>
                  <surname>MacPhail</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>6</month>
            <year>2009</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">48</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">11</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40012571</issue-id>
         <fpage>1617</fpage>
         <lpage>1623</lpage>
         <permissions>
            <copyright-statement>Copyright 2009 Infectious Diseases Society of America</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/40309372"/>
         <abstract>
            <p>Background. Treatment for tuberculosis (TB) is common among individuals receiving stavudine-containing highly active antiretroviral therapy (HAART), but the effect of TB treatment on stavudine toxicity has received little attention. We estimated the effect of TB treatment on risk of stavudine substitution among individuals receiving first-line HAART. Methods. We evaluated a cohort of 7066 patients who initiated HAART from April 2004 through March 2007 in Johannesburg, South Africa. Three exposure categories were considered: ongoing TB treatment at HAART initiation, concurrent initiation of TB treatment and HAART, and incident TB treatment after HAART initiation. The outcome was single-drug stavudine substitution. Adjusted hazard ratios (aHRs) were estimated using marginal structural models to control for confounding, loss to follow-up, and competing risks. Results. Individuals with ongoing and concurrent TB treatment were at increased risk of stavudine substitution, irrespective of stavudine dosage. For ongoing TB treatment, aHR was 3.18 (95% confidence interval [CI], 1.82-5.56) in the first 2 months of HAART, 2.51 (95% CI, 1.77-3.54) in months 3-6, and 1.19 (95% CI, 0.94-1.52) thereafter. For concurrent TB treatment, aHR was 6.60 (95% CI, 3.03-14.37) in the first 2 months, 1.88 (95% CI, 0.87-4.09) in months 3-6, and 1.07 (95% CI, 0.65-1.76) thereafter. There was no effect of incident TB on stavudine substitution risk. Conclusions. Risk of stavudine substitution was increased among patients who received TB treatment and was especially elevated during the period soon after HAART initiation. In settings in which alternative antiretroviral drugs are available, initiation of stavudine therapy in patients receiving TB treatment may need to be reconsidered.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d3139e274a1310">
            <label>1</label>
            <mixed-citation id="d3139e281" publication-type="other">
World Health Organization (WHO). Towards universal access: scaling
up priority HIV/AIDS interventions in the health sector. Progress re-
port, April 2007. Geneva, Switzerland: WHO, 2007. Available at: http:
//www.who.int/hiv/mediacentre/universal_access_progress_report_en
.pdf. Accessed 14 September 2007.</mixed-citation>
         </ref>
         <ref id="d3139e300a1310">
            <label>2</label>
            <mixed-citation id="d3139e307" publication-type="other">
Bolhaar MG, Karstaedt AS. A high incidence of lactic acidosis and
symptomatic hyperlactatemia in women receiving highly active anti-
retroviral therapy in Soweto, South Africa. Clin Infect Dis 2007;45:
254-60.</mixed-citation>
         </ref>
         <ref id="d3139e323a1310">
            <label>3</label>
            <mixed-citation id="d3139e330" publication-type="other">
Boulle A, Orrell C, Kaplan R, et al. Substitutions due to antiretroviral
toxicity or contraindication in the first 3 years of antiretroviral therapy
in a large South African cohort. Antivir Ther 2007;12:753-60.</mixed-citation>
         </ref>
         <ref id="d3139e343a1310">
            <label>4</label>
            <mixed-citation id="d3139e350" publication-type="other">
Ferradini L, Jeannin A, Pinoges L, et al. Scaling up of highly active
antiretroviral therapy in a rural district of Malawi: an effectiveness
assessment. Lancet 2006;367:1335-42.</mixed-citation>
         </ref>
         <ref id="d3139e364a1310">
            <label>5</label>
            <mixed-citation id="d3139e371" publication-type="other">
Stringer JS, Zulu I, Levy J, et al. Rapid scale-up of antiretroviral therapy
at primary care sites in Zambia: feasibility and early outcomes. JAMA
2006;296:782-93.</mixed-citation>
         </ref>
         <ref id="d3139e384a1310">
            <label>6</label>
            <mixed-citation id="d3139e391" publication-type="other">
Colebunders R, Kamya MR, Laurence J, et al. First-line antiretroviral
therapy in Africa: how evidence-based are our recommendations? AIDS
Rev 2005;7:148-54.</mixed-citation>
         </ref>
         <ref id="d3139e404a1310">
            <label>7</label>
            <mixed-citation id="d3139e411" publication-type="other">
Currier J. Management of metabolic complications of therapy. AIDS
2002;16(Suppl 4):S171-6.</mixed-citation>
         </ref>
         <ref id="d3139e421a1310">
            <label>8</label>
            <mixed-citation id="d3139e428" publication-type="other">
Currier JS. Sex differences in antiretroviral therapy toxicity: lactic ac-
idosis, stavudine, and women. Clin Infect Dis 2007;45:261-2.</mixed-citation>
         </ref>
         <ref id="d3139e438a1310">
            <label>9</label>
            <mixed-citation id="d3139e445" publication-type="other">
Currier JS, Havlir DV. Complications of HIV disease and antiretroviral
therapy. Top HIV Med 2005;13:16-23.</mixed-citation>
         </ref>
         <ref id="d3139e455a1310">
            <label>10</label>
            <mixed-citation id="d3139e462" publication-type="other">
Subbaraman R, Chaguturu SK, Mayer KH, Flanigan TP, Kumarasamy
N. Adverse effects of highly active antiretroviral therapy in developing
countries. Clin Infect Dis 2007;45:1093-101.</mixed-citation>
         </ref>
         <ref id="d3139e476a1310">
            <label>11</label>
            <mixed-citation id="d3139e483" publication-type="other">
Falco V, Rodriguez D, Ribera E, et al. Severe nudeoside-associated
lactic acidosis in human immunodeficiency virus-infected patients:
report of 12 cases and review of the literature. Clin Infect Dis 2002;
34:838-46.</mixed-citation>
         </ref>
         <ref id="d3139e499a1310">
            <label>12</label>
            <mixed-citation id="d3139e508" publication-type="other">
Parienti JJ, Massari V, Descamps D, et al. Predictors of virologie failure
and resistance in HIV-infected patients treated with nevirapine-or
efavirenz-based antiretroviral therapy. Clin Infect Dis 2004;38:1311-6.</mixed-citation>
         </ref>
         <ref id="d3139e521a1310">
            <label>13</label>
            <mixed-citation id="d3139e528" publication-type="other">
Forna F, Liechty CA, Solberg P, et al. Clinical toxicity of highly active
antiretroviral therapy in a home-based AIDS care program in rural
Uganda. J Acquir Immune Defic Syndr 2007;44:456-62.</mixed-citation>
         </ref>
         <ref id="d3139e541a1310">
            <label>14</label>
            <mixed-citation id="d3139e548" publication-type="other">
Hawkins C, Achenbach C, Fryda W, Ngare D, Murphy R. Antiretroviral
durability and tolerabiliry in HIV-infected adults living in urban Kenya.
J Acquir Immune Defic Syndr 2007;45:304-10.</mixed-citation>
         </ref>
         <ref id="d3139e561a1310">
            <label>15</label>
            <mixed-citation id="d3139e568" publication-type="other">
World Health Organization (WHO). TB/HIV: a clinical manual. 2nd
ed. Accessed 31 March 2008. Geneva, Switzerland: WHO, 2004. Avail-
able at: http://whqlibdoc.who.int/publications/2004/9241546344.pdf.
Accessed 13 April 2009.</mixed-citation>
         </ref>
         <ref id="d3139e584a1310">
            <label>16</label>
            <mixed-citation id="d3139e591" publication-type="other">
South African National Department of Health. National antiretroviral
treatment guidelines. HIV and AIDS Policy Guideline, 2004. South
Africa: Jacana, 2004. Available at: http://www.doh.gov.za/docs/fact-
sheets/suidelines/arteuidelinesO4/. Accessed 13 April 2009.</mixed-citation>
         </ref>
         <ref id="d3139e608a1310">
            <label>17</label>
            <mixed-citation id="d3139e615" publication-type="other">
Amoroso A, Sheneberger R, Fielder AEJ, Etienne M, Stafford K. ART-
associated toxicities leading to a switch in medication: experience in
Uganda, Kenya, and Zambia [abstract 789]. In: Program and abstracts
of the 14th Conference on Retroviruses and Opportunistic Infections
(Los Angeles, CA). Alexandria, VA: Foundation for Retroviology and
Human Health, 2007.</mixed-citation>
         </ref>
         <ref id="d3139e638a1310">
            <label>18</label>
            <mixed-citation id="d3139e645" publication-type="other">
Moyle GJ, Sadler M. Peripheral neuropathy with nucleoside antiret-
rovirals: risk factors, incidence and management. Drug Saf 1998;19:
481-94.</mixed-citation>
         </ref>
         <ref id="d3139e658a1310">
            <label>19</label>
            <mixed-citation id="d3139e665" publication-type="other">
Saarto T, Wiffen PJ. Antidepressants for neuropathic pain. Cochrane
Database Syst Rev 2007:CD005454.</mixed-citation>
         </ref>
         <ref id="d3139e675a1310">
            <label>20</label>
            <mixed-citation id="d3139e682" publication-type="other">
World Health Organization (WHO). Antiretroviral therapy for HIV
infection in adults and adolescents: recommendations for a public
health approach, 2006 revision. Available at: http://www.who.int/hiv/
pub/guidelines/artadultguidelines.pdf. Accessed 14 October 2007. Ge-
neva, Switzerland: WHO, 2006.</mixed-citation>
         </ref>
         <ref id="d3139e701a1310">
            <label>21</label>
            <mixed-citation id="d3139e708" publication-type="other">
Cole SR, Hernán MA, Anastos K, Jamieson BD, Robins JM. Deter-
mining the effect of highly active antiretroviral therapy on changes in
human immunodeficiency virus type 1 RNA viral load using a margin-
al structural left-censored mean model. Am J Epidemiol 2007;166:
219-27.</mixed-citation>
         </ref>
         <ref id="d3139e727a1310">
            <label>22</label>
            <mixed-citation id="d3139e734" publication-type="other">
Hernan MA, Brumback B, Robins JM. Marginal structural models to
estimate the causal effect of zidovudine on the survival of HIV-positive
men. Epidemiology 2000;11:561-70.</mixed-citation>
         </ref>
         <ref id="d3139e748a1310">
            <label>23</label>
            <mixed-citation id="d3139e755" publication-type="other">
Robins JM, Hernân MA, Brumback B. Marginal structural models and
causal inference in epidemiology. Epidemiology 2000;11:550-60.</mixed-citation>
         </ref>
         <ref id="d3139e765a1310">
            <label>24</label>
            <mixed-citation id="d3139e772" publication-type="other">
Hernán MA, Brumback B, Robins JM. Marginal structural models to
estimate the causal effect of zidovudine on the survival of HIV-positive
men. Epidemiology 2000;11:561-70.</mixed-citation>
         </ref>
         <ref id="d3139e785a1310">
            <label>25</label>
            <mixed-citation id="d3139e792" publication-type="other">
Matsuyama Y, Yamaguchi T. Estimation of the marginal survival time
in the presence of dependent competing risks using inverse probability
of censorine weighted (IPCW) methods. Pharm Stat 2008;7:202-14.</mixed-citation>
         </ref>
         <ref id="d3139e805a1310">
            <label>26</label>
            <mixed-citation id="d3139e812" publication-type="other">
Breen RA, Lipman MC, Johnson MA. Increased incidence of peripheral
neuropathy with co-administration of stavudine and isoniazid in HIV-
infected individuals. AIDS 2000;14:615.</mixed-citation>
         </ref>
         <ref id="d3139e825a1310">
            <label>27</label>
            <mixed-citation id="d3139e832" publication-type="other">
Dean GL, Edwards SG, Ives NJ, et al. Treatment of tuberculosis in
HIV-infected persons in the era of highly active antiretroviral therapy.
AIDS 2002;16:75-83.</mixed-citation>
         </ref>
         <ref id="d3139e845a1310">
            <label>28</label>
            <mixed-citation id="d3139e852" publication-type="other">
Villamor E, Mugusi F, Urassa W, et al. A trial of the effect of micro-
nutrient supplementation on treatment outcome, T cell counts, mor-
bidity, and mortality in adults with pulmonary tuberculosis. J Infect
Dis 2008;197:1499-505.</mixed-citation>
         </ref>
         <ref id="d3139e869a1310">
            <label>29</label>
            <mixed-citation id="d3139e876" publication-type="other">
Hernan MA, Robins JM. Estimating causal effects from epidemiological
data. J Epidemiol Community Health 2006;60:578-86.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
