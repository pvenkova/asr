<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt2jbv7d</book-id>
      <subj-group>
         <subject content-type="call-number">D531.S73 2011</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">World War, 1914–1918</subject>
         <subj-group>
            <subject content-type="lcsh">Campaigns</subject>
            <subj-group>
               <subject content-type="lcsh">Western Front</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>History</subject>
      </subj-group>
      <book-title-group>
         <book-title>With Our Backs to the Wall</book-title>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>STEVENSON</surname>
               <given-names>DAVID</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>12</day>
         <month>12</month>
         <year>2011</year>
      </pub-date>
      <isbn content-type="ppub">9780674062269</isbn>
      <isbn content-type="epub">9780674063198</isbn>
      <publisher>
         <publisher-name>Harvard University Press</publisher-name>
         <publisher-loc>Cambridge, Massachusetts</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2011</copyright-year>
         <copyright-holder>David Stevenson</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt2jbv7d"/>
      <abstract abstract-type="short">
         <p>Why did World War I end with a whimper—an arrangement between two weary opponents to suspend hostilities? Why did the Allies reject the option of advancing into Germany and taking Berlin? Most histories of the Great War focus on the avoidability of its beginning. This book focuses on Germany’s inconclusive defeat and its ominous ramifications.</p>
      </abstract>
      <counts>
         <page-count count="752"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jbv7d.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jbv7d.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jbv7d.3</book-part-id>
                  <title-group>
                     <title>List of Illustrations</title>
                  </title-group>
                  <fpage>vi</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jbv7d.4</book-part-id>
                  <title-group>
                     <title>List of Maps</title>
                  </title-group>
                  <fpage>viii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jbv7d.5</book-part-id>
                  <title-group>
                     <title>List of Tables</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jbv7d.6</book-part-id>
                  <title-group>
                     <title>Abbreviations</title>
                  </title-group>
                  <fpage>x</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jbv7d.7</book-part-id>
                  <title-group>
                     <title>Note on Military and Naval Terminology</title>
                  </title-group>
                  <fpage>xiv</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jbv7d.8</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Stevenson</surname>
                           <given-names>David</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>xvi</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jbv7d.9</book-part-id>
                  <title-group>
                     <title>[Maps]</title>
                  </title-group>
                  <fpage>xxii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jbv7d.10</book-part-id>
                  <title-group>
                     <title>Prologue:</title>
                     <subtitle>Deadlock, 1914–1917</subtitle>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>The eleventh hour of the eleventh day of the eleventh month of 1918 was an exceptional juncture. It remains imprinted in the Western calendar. As our distance from it lengthens, it has grown more emblematic of war in general, and it has become impossible to view the armistice without irony, as a forlorn expression of the hope that such a catastrophe could never happen again. Even on the day itself Clemenceau’s daughter pleaded with him: ‘Tell me Papa that you are happy.’ He responded that ‘I cannot say it because I am not.<italic>It will all be useless.</italic>’³</p>
                     <p>The ceasefire</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jbv7d.11</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>On the Defensive, March–July 1918</title>
                  </title-group>
                  <fpage>30</fpage>
                  <abstract>
                     <p>For the Allies during 1917 almost every aspect of the fighting had gone badly. At the end of the year their leaders were unsure what sort of victory, if any, was obtainable, and were preparing for a long haul. Their forebodings proved unjustified because the Germans attacked. Between March and July 1918 the OHL launched five major Western Front offensives: the first (codenamed ‘Michael’), from 21 March to 5 April; the Battle of the Lys (‘Georgette’) from 9 to 29 April; the Battle of the Chemin des Dames (‘Blücher–Yorck’) from 27 May to 4 June; the Battle of the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jbv7d.12</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>On the Attack:</title>
                     <subtitle>July–November 1918</subtitle>
                  </title-group>
                  <fpage>112</fpage>
                  <abstract>
                     <p>During 1918 the Central Powers tried first to break out of their fortress and then were driven back into it. In the first half of the year the Allies withstood a succession of all-out assaults. Well into the summer, the outcome seemed to many observers to hang in the balance. Yet during these same months the German and Austrian armies lost much of their edge, and once the Allies returned to the offensive they found their task far easier than in 1916 or 1917. Experience had so traumatized them, however, that most in authority continued to anticipate a final victory</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jbv7d.13</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>The New Warfare:</title>
                     <subtitle>Intelligence, Technology, and Logistics</subtitle>
                  </title-group>
                  <fpage>170</fpage>
                  <abstract>
                     <p>The Great War has been described as ‘the birth of the modern style of warfare’. By 1918 both sides were attacking in three dimensions, from the air as well as on the surface. Intelligence-led targeting and all-arms co-ordination made possible sudden paralysing blows delivered deep behind the enemy front in a manner more resembling France in 1940 or Kuwait in 1991 than the short-range artillery and infantry encounters of 1914.² During the war’s middle years British troops had dubbed the Western Front ‘The Great Sausage Machine’: mincing both sides’ manpower while remaining stubbornly in place.³ But during 1918 it first</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jbv7d.14</book-part-id>
                  <title-group>
                     <title>[Illustrations]</title>
                  </title-group>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jbv7d.15</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>The Human Factor:</title>
                     <subtitle>Manpower and Morale</subtitle>
                  </title-group>
                  <fpage>244</fpage>
                  <abstract>
                     <p>Fundamental to the Allies’ final successes was that they no longer faced the same enemies. The Turkish resistance that had halted them at Gallipoli; the Bulgarian in the hills above Salonika; the Austro-Hungarian on the Isonzo; and above all the German resistance on the Western Front had ceased to be so formidable. Once their defensive crust was broken, the Central Powers’ armies crumbled into mass desertion and surrender, whereas the Allied armies – however tested – emerged intact. The usual (and entirely understandable) question raised about combat motivation in the war is that of why the troops fought. Foch’s general-ship would have</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jbv7d.16</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Securing the Seas:</title>
                     <subtitle>Submarines and Shipping</subtitle>
                  </title-group>
                  <fpage>311</fpage>
                  <abstract>
                     <p>The Allies’ mastery of global resources depended on command of the seas. As 1918 began, neither side saw that command as being assured. Admiral Henning von Holtzendorff, the Chief of the Admiralty Staff in Berlin and the architect of the unrestricted submarine campaign, advised Wilhelm that ‘decisive consequences are to be expected from U-boat warfare in 1918’.¹ The Allies’ shipping losses still exceeded new construction, and their experts forecast worsening tonnage shortages.² Admiral Sir David Beatty, the commander of the British Grand Fleet, feared his margin of superiority was so fine that his best policy was simply to contain his</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jbv7d.17</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>The War Economies:</title>
                     <subtitle>Money, Guns, and Butter</subtitle>
                  </title-group>
                  <fpage>350</fpage>
                  <abstract>
                     <p>The scene now turns to the war between the workshops: to Pittsburgh, Birmingham, Paris, and Turin; and to Wiener Neustadt and Essen. The Allies’ economic advantage was indispensable to their triumph, though at the time that advantage seemed much less certain than in retrospect, and a mere superiority in resources meant little unless managed and directed to the tasks in hand. Not only had weaponry to be manufactured – and plant, raw materials, and labour earmarked for that purpose – but also the civilian population had to be supplied and fed, and transport and finance provided. So far from the Allies enjoying</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jbv7d.18</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>The Home Fronts:</title>
                     <subtitle>Gender, Class, and Nation</subtitle>
                  </title-group>
                  <fpage>439</fpage>
                  <abstract>
                     <p>In 1918 the Central Powers were not only defeated militarily: they were also convulsed politically. Revolution overthrew the Hohenzollern monarchy, Austria-Hungary broke up, King Ferdinand of Bulgaria abdicated, and the Young Turk party left government and dissolved itself. Military defeat set the collapse in motion, but internal upheaval completed it. Yet when the war began it had been far from obvious that the Allies could better withstand a prolonged struggle. Italy was socially polarized, and the Catholic Church as well as the Left and liberal progressives opposed its intervention. Pre-1914 France had been riven by the Dreyfus Affair and over</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jbv7d.19</book-part-id>
                  <title-group>
                     <title>[Illustrations]</title>
                  </title-group>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jbv7d.20</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>Armistice and After</title>
                  </title-group>
                  <fpage>509</fpage>
                  <abstract>
                     <p>The process that began the First World War had started in the Balkans: so did the process that ended it. The Allied offensive in Macedonia launched on 15 September 1918 led to a ceasefire with Bulgaria on the 29th, and that ceasefire led on to Germany’s request to Woodrow Wilson on 4 October to terminate hostilities. From there the steps towards the armistice were Wilson’s decision to recommend one, his partners’ decision to acquiesce, and that of the Central Powers to accept the victors’ demands. Consent from all the warring powers was needed, and while governments deliberated the fighting proceeded</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jbv7d.21</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>547</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jbv7d.22</book-part-id>
                  <title-group>
                     <title>Bibliography</title>
                  </title-group>
                  <fpage>629</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jbv7d.23</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>659</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
