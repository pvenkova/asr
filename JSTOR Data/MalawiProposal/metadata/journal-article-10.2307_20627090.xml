<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">jvertpale</journal-id>
         <journal-id journal-id-type="jstor">j101365</journal-id>
         <journal-title-group>
            <journal-title>Journal of Vertebrate Paleontology</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Society of Vertebrate Paleontology</publisher-name>
         </publisher>
         <issn pub-type="ppub">02724634</issn>
         <issn pub-type="epub">19372809</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">20627090</article-id>
         <title-group>
            <article-title>A Re-evaluation of Brachiosaurus altithorax Riggs 1903 (Dinosauria, Sauropoda) and Its Generic Separation from Giraffatitan brancai (Janensch 1914)</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Michael P.</given-names>
                  <surname>Taylor</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>12</day>
            <month>9</month>
            <year>2009</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">29</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id>i20627077</issue-id>
         <fpage>787</fpage>
         <lpage>806</lpage>
         <permissions>
            <copyright-statement>Copyright 2008 Society of Vertebrate Paleontology</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/20627090"/>
         <abstract>
            <p>Although the macronarian sauropod Brachiosaurus is one of the most iconic dinosaurs, its popular image is based almost entirely on the referred African species Brachiosaurus brancai rather than the North American type species Brachiosaurus altithorax. Reconsideration of Janensch's referral of the African species to the American genus shows that it was based on only four synapomorphies and would not be considered a convincing argument today. Detailed study of the bones of both species show that they are distinguished by at least 26 characters of the dorsal and caudal vertebrae, coracoids, humeri, ilia, and femora, with the dorsal vertebrae being particularly different between the two species. These animals must therefore be considered generically separate, and the genus name Giraffatitan Paul 1988 must be used for "Brachiosaurus" brancai, in the combination Giraffatitan brancai. A phylogenetic analysis treating the two species as separate OTUs nevertheless recovers them as sister taxa in all most parsimonious trees, reaffirming a monophyletic Brachiosauridae, although only one additional step is required for Giraffatitan to clade among somphospondylians to the exclusion of Brachiosaurus. The American Brachiosaurus is shown to be somewhat different from Giraffatitan in overall bodily proportions: it had a longer and deeper trunk and probably a longer and taller tail, carried a greater proportion of its mass on the forelimbs, and may have had somewhat sprawled forelimbs. Even though it was overall a larger animal than the Giraffatitan lectotype, the Brachiosaurus holotype was probably immature, as its coracoids were not fused to its scapulae.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Literature Cited</title>
         <ref id="d2509e180a1310">
            <mixed-citation id="d2509e184" publication-type="other">
Alexander, R. M. 1985. Mechanics of posture and gait of some large
dinosaurs. Zoological Journal of the Linnean Society 83:1-25.</mixed-citation>
         </ref>
         <ref id="d2509e194a1310">
            <mixed-citation id="d2509e198" publication-type="other">
Anonymous. 1959. Brachiosaurus exhibit at the Smithsonian Institution.
Nature 183:649-650.</mixed-citation>
         </ref>
         <ref id="d2509e208a1310">
            <mixed-citation id="d2509e212" publication-type="other">
Antunes, M. T., and O. Mateus. 2003. Dinosaurs of Portugal. Comptes
Rendus Palevol 2:77-95.</mixed-citation>
         </ref>
         <ref id="d2509e222a1310">
            <mixed-citation id="d2509e226" publication-type="other">
Bonaparte, J. F. 1986. Les dinosaures (Carnosaures, Allosauridés, Saur-
opodes, Cétiosauridés) du Jurassique moyen de Cerro Condor
(Chubut, Argentina). Annales de Paléontologie 72:325-386.</mixed-citation>
         </ref>
         <ref id="d2509e240a1310">
            <mixed-citation id="d2509e244" publication-type="other">
Bonnan, M. F., and M. J. Wedel. 2004. First occurrence of Brachiosaurus
(Dinosauria: Sauropoda) from the Upper Jurassic Morrison Forma-
tion of Oklahoma. PaleoBios 24:13-21.</mixed-citation>
         </ref>
         <ref id="d2509e257a1310">
            <mixed-citation id="d2509e261" publication-type="other">
Borsuk-Bialynicka, M. 1977. A new camarasaurid sauropod Opisthocoe-
licaudia skarzynskii, gen. n., sp. n., from the Upper Cretaceous of
Mongolia. Palaeontologica Polonica 37:5-64.</mixed-citation>
         </ref>
         <ref id="d2509e274a1310">
            <mixed-citation id="d2509e278" publication-type="other">
Cantino, P. D., and K. de Queiroz. 2006. PhyloCode: A Phylogenetic
Code of Biological Nomenclature (Version 4b, September 12,2007).
http://www.ohiou.edu/phylocode/Phyl0Code4b.pdf.</mixed-citation>
         </ref>
         <ref id="d2509e291a1310">
            <mixed-citation id="d2509e295" publication-type="other">
Carpenter, K., and J. S. Mclntosh. 1994. Upper Jurassic sauropod babies
from the Morrison Formation; pp. 265-278 in K. Carpenter,
K. F. Hirsch, and J. R. Horner (eds.), Dinosaur Eggs and Babies.
Cambridge University Press, Cambridge.</mixed-citation>
         </ref>
         <ref id="d2509e311a1310">
            <mixed-citation id="d2509e315" publication-type="other">
Carpenter, K., and V. Tidwell. 1998. Preliminary description of a Bra-
chiosaurus skull from Felch Quarry 1, Garden Park, Colorado. Mod-
ern Geology 23:69-84.</mixed-citation>
         </ref>
         <ref id="d2509e328a1310">
            <mixed-citation id="d2509e332" publication-type="other">
Casanovas, M. L., J. V. Santafé, and J. L. Sanz. 2001. Losillasaurus
giganteus, un neuvo saurópodo del tránsito Jurásico-Cretácico de la
cuenca de "Los Serranos" (Valencia, España). Paleontologia i Evo-
lució 32-33:99-122.</mixed-citation>
         </ref>
         <ref id="d2509e349a1310">
            <mixed-citation id="d2509e353" publication-type="other">
Cope, E. D. 1877. On a gigantic saurian from the Dakota epoch of
Colorado. Paleontology Bulletin 25:5-10.</mixed-citation>
         </ref>
         <ref id="d2509e363a1310">
            <mixed-citation id="d2509e367" publication-type="other">
Curry Rogers, K. 2001. The evolutionary history of the Titanosauria.
(Ph.D. dissertation). State University of New York, Stony Brook,
573 pp.</mixed-citation>
         </ref>
         <ref id="d2509e380a1310">
            <mixed-citation id="d2509e384" publication-type="other">
Curtice, B. D. 1995. A description of the anterior caudal vertebrae of
Supersaurus vivianae. Journal of Vertebrate Paleontology 15:3-25 A.</mixed-citation>
         </ref>
         <ref id="d2509e394a1310">
            <mixed-citation id="d2509e398" publication-type="other">
Curtice, B. D., and K. L. Stadtman. 2001. The demise of Dystylosaurus
edwini and a revision of Supersaurus vivianae. Western Association
of Vertebrate Paleontologists and Mesa Southwest Paleontological
Symposium, Mesa Southwest Museum Bulletin 8:33-40.</mixed-citation>
         </ref>
         <ref id="d2509e414a1310">
            <mixed-citation id="d2509e418" publication-type="other">
Curtice, B. D., K. L. Stadtman, and L. J. Curtice. 1996. A reassessment of
Ultrasauros macintoshi (Jensen, 1985). Museum of Northern Ari-
zona Bulletin 60:87-95.</mixed-citation>
         </ref>
         <ref id="d2509e431a1310">
            <mixed-citation id="d2509e435" publication-type="other">
Foster, J. R. 2003. Paleoecological analysis of the vertebrate fauna of
the Morrison Formation (Upper Jurassic), Rocky Mountain Region,
U.S.A. (NMMNHS bulletin 23). New Mexico Museum of Natural
History and Science, Albuquerque, New Mexico, 95 pp.</mixed-citation>
         </ref>
         <ref id="d2509e452a1310">
            <mixed-citation id="d2509e456" publication-type="other">
Harris, J. D. 2006. The significance of Suuwassea emiliae (Dinosauria:
Sauropoda) for flagellicaudatan intrarelationships and evolution.
Journal of Systematic Palaeontology 4:185-198.</mixed-citation>
         </ref>
         <ref id="d2509e469a1310">
            <mixed-citation id="d2509e473" publication-type="other">
Hatcher, J. B. 1903a. A new name for the dinosaur Haplocanthus Hatch-
er. Proceedings of the Biological Society of Washington 16:100.</mixed-citation>
         </ref>
         <ref id="d2509e483a1310">
            <mixed-citation id="d2509e487" publication-type="other">
Hatcher, J. B. 1903b. Osteology of Haplocanthosaurus with description
of a new species, and remarks on the probable habits of the Saur-
opoda and the age and origin of the Atlantosaurus beds. Memoirs of
the Carnegie Museum 2:1-72.</mixed-citation>
         </ref>
         <ref id="d2509e503a1310">
            <mixed-citation id="d2509e507" publication-type="other">
Heinrich, W.-D. 1999. The taphonomy of dinosaurs from the Upper Juras-
sic of Tendaguru (Tanzania) based on field sketches of the German
Tendaguru Expedition (1909-1913). Mitteilungen aus dem Museum
für Naturkunde, Berlin, Geowissenschaften, Reihe 2:25-61.</mixed-citation>
         </ref>
         <ref id="d2509e523a1310">
            <mixed-citation id="d2509e527" publication-type="other">
Henderson, D. M. 2004. Tipsy punters: sauropod dinosaur pneumaticity,
buoyancy and aquatic habits. Proceedings of the Royal Society of
London B, 271(Suppl. 4):S180-S183.</mixed-citation>
         </ref>
         <ref id="d2509e540a1310">
            <mixed-citation id="d2509e544" publication-type="other">
Hurlburt, G. R. 1999. Comparison of body mass estimation techniques,
using Recent reptiles and the pelycosaur Edaphosaurus boanerges.
Journal of Vertebrate Paleontology 19:338-350.</mixed-citation>
         </ref>
         <ref id="d2509e558a1310">
            <mixed-citation id="d2509e562" publication-type="other">
Ikejiri, T., V. Tidwell, and D. L. Trexler. 2005. new adult specimens of
Camarasaurus lentus highlight ontogenetic variation within the spe-
cies; pp. 154-179 in V. Tidwell, and K. Carpenter (eds.), Thunder
Lizards: the Sauropodomorph Dinosaurs. Indiana University Press,
Bloomington, Indiana.</mixed-citation>
         </ref>
         <ref id="d2509e581a1310">
            <mixed-citation id="d2509e585" publication-type="other">
Jacobs, L. L., D. A. Winkler, W. R. Downs, and E. M. Gomani. 1993.
New material of an Early Cretaceous titanosaurid sauropod dino-
saur from Malawi. Palaeontology 36:523-534.</mixed-citation>
         </ref>
         <ref id="d2509e598a1310">
            <mixed-citation id="d2509e602" publication-type="other">
Janensch, W. 1914. Übersicht über der Wirbeltierfauna der Tendaguru-
Schichten nebst einer kurzen Charakterisierung der neu aufgefuhr-
ten Arten von Sauropoden. Archiv fur Biontologie 3:81-110.</mixed-citation>
         </ref>
         <ref id="d2509e615a1310">
            <mixed-citation id="d2509e619" publication-type="other">
Janensch, W. 1922. Das Handskelett von Gigantosaurus robustus u. Bra-
chiosaurus Brancai aus den Tendaguru-Schichten Deutsch-Ostafri-
kas. Centraiblatt für Mineralogie, Geologie und Paläontologie
15:464-480.</mixed-citation>
         </ref>
         <ref id="d2509e635a1310">
            <mixed-citation id="d2509e639" publication-type="other">
Janensch, W. 1929. Material und Formengehalt der Sauropoden in der
Ausbeute der Tendaguru-Expedition. Palaeontographica (Suppl. 7)
2:1-34.</mixed-citation>
         </ref>
         <ref id="d2509e652a1310">
            <mixed-citation id="d2509e656" publication-type="other">
Janensch, W. 1935-1936. Die Schädel der Sauropoden Brachiosaurus,
Barosaurus und Dicraeosaurus aus den Tendaguru-Schichten
Deutsch-Ostafrikas. Palaeontographica (Suppl. 7) 2:147-298.</mixed-citation>
         </ref>
         <ref id="d2509e670a1310">
            <mixed-citation id="d2509e674" publication-type="other">
Janensch, W. 1947. Pneumatizitat bei Wirbeln von Sauropoden und
anderen Saurischien. Palaeontographica (Suppl. 7) 3:1-25.</mixed-citation>
         </ref>
         <ref id="d2509e684a1310">
            <mixed-citation id="d2509e688" publication-type="other">
Janensch, W. 1950a. Die Wirbelsaule von Brachiosaurus brancai.
Palaeontographica (Suppl. 7) 3:27-93.</mixed-citation>
         </ref>
         <ref id="d2509e698a1310">
            <mixed-citation id="d2509e702" publication-type="other">
Janensch, W. 1950b. Die Skelettrekonstruktion von Brachiosaurus bran-
cai. Palaeontographica (Suppl. 7) 3:97-103.</mixed-citation>
         </ref>
         <ref id="d2509e712a1310">
            <mixed-citation id="d2509e716" publication-type="other">
Janensch, W. 1961. Die Gliedmaszen und Gliedmaszengürtel der Sau-
ropoden der Tendaguru-Schichten. Palaeontographica (Suppl. 7)
3:177-235.</mixed-citation>
         </ref>
         <ref id="d2509e729a1310">
            <mixed-citation id="d2509e733" publication-type="other">
Jensen, J. A. 1985. Three new sauropod dinosaurs from the Upper Juras-
sic of Colorado. Great Basin Naturalist 45:697-709.</mixed-citation>
         </ref>
         <ref id="d2509e743a1310">
            <mixed-citation id="d2509e747" publication-type="other">
Jensen, J. A. 1987. New brachiosaur material from the Late Jurassic of
Utah and Colorado. Great Basin Naturalist 47:592-608.</mixed-citation>
         </ref>
         <ref id="d2509e758a1310">
            <mixed-citation id="d2509e762" publication-type="other">
Jerison, H. J. 1973. Evolution of the brain and intelligence. Academic
Press, New York, 482 pp.</mixed-citation>
         </ref>
         <ref id="d2509e772a1310">
            <mixed-citation id="d2509e776" publication-type="other">
Kim, H. M. 1983. Cretaceous dinosaurs from Korea. Journal of the
Geology Society of Korea 19:115-126.</mixed-citation>
         </ref>
         <ref id="d2509e786a1310">
            <mixed-citation id="d2509e790" publication-type="other">
Lapparent, A. F. d., and G. Zbyszewski. 1957. Memoire no. 2 (nouvelle
série): les dinosauriens du Portugal. Services Geologiques du Portu-
gal, Lisbon, Portugal, 63 pp.</mixed-citation>
         </ref>
         <ref id="d2509e803a1310">
            <mixed-citation id="d2509e807" publication-type="other">
Lavocat, R. 1954. Sur les Dinosauriens du continental intercalaire des
Kem-Kem de la Daoura. Comptes Rendus 19th Intenational Geo-
logical Congress 1952, 1:65-68.</mixed-citation>
         </ref>
         <ref id="d2509e820a1310">
            <mixed-citation id="d2509e824" publication-type="other">
Lull, R. S. 1919. The sauropod dinosaur Barosaurus Marsh. Memoirs of
the Connecticut Academy of Arts and Sciences 6:1-42.</mixed-citation>
         </ref>
         <ref id="d2509e834a1310">
            <mixed-citation id="d2509e838" publication-type="other">
Maier, G. 2003. African Dinosaurs Unearthed: The Tendaguru Expeditions.
Indiana University Press, Bloomington and Indianapolis, 380 pp.</mixed-citation>
         </ref>
         <ref id="d2509e849a1310">
            <mixed-citation id="d2509e853" publication-type="other">
Marsh, O. C. 1877. Notice of new dinosaurian reptiles from the Jurassic
formation. American Journal of Science and Arts 14:514-516.</mixed-citation>
         </ref>
         <ref id="d2509e863a1310">
            <mixed-citation id="d2509e867" publication-type="other">
Marsh, O. C. 1878. Principal characters of American Jurassic dinosaurs.
Part I. American Journal of Science, Series 3, 16:411-416.</mixed-citation>
         </ref>
         <ref id="d2509e877a1310">
            <mixed-citation id="d2509e881" publication-type="other">
Marsh, O. C. 1879. Notice of new Jurassic reptiles. American Journal of
Science, Series 3, 18:501-505.</mixed-citation>
         </ref>
         <ref id="d2509e891a1310">
            <mixed-citation id="d2509e895" publication-type="other">
Marsh, O. C. 1890. Description of new dinosaurian reptiles. American
Journal of Science, Series 3, 39:81-86 and plate I.</mixed-citation>
         </ref>
         <ref id="d2509e905a1310">
            <mixed-citation id="d2509e909" publication-type="other">
Marsh, O. C 1891. Restoration of Triceratops. American Journal of
Science, Series 3, 41:339-342.</mixed-citation>
         </ref>
         <ref id="d2509e919a1310">
            <mixed-citation id="d2509e923" publication-type="other">
Martin, J., V. Martin-Rolland, and E. Frey. 1998. Not cranes or
masts, but beams: the biomechanics of sauropod necks. Oryctos
1:113-120.</mixed-citation>
         </ref>
         <ref id="d2509e937a1310">
            <mixed-citation id="d2509e941" publication-type="other">
Matthew, W. D. 1915. Dinosaurs, with special reference to the American
Museum collections. American Museum of Natural History, New
York, 164 pp.</mixed-citation>
         </ref>
         <ref id="d2509e954a1310">
            <mixed-citation id="d2509e958" publication-type="other">
Mclntosh, J. S. 1990a. Sauropoda; pp. 345-401 in D. B. Weishampel,
P. Dodson, and H. Osmólska (eds.), The Dinosauria. University of
California Press, Berkeley and Los Angeles.</mixed-citation>
         </ref>
         <ref id="d2509e971a1310">
            <mixed-citation id="d2509e975" publication-type="other">
Mclntosh, J. S. 1990b. Species determination in sauropod dinosaurs
with tentative suggestions for the their classification; pp. 53-69 in
K. Carpenter, and P. J. Currie (eds.), Dinosaur Systematics: Approaches
and Perspectives. Cambridge University Press, Cambridge.</mixed-citation>
         </ref>
         <ref id="d2509e991a1310">
            <mixed-citation id="d2509e995" publication-type="other">
McIntosh, J. S., and D. S. Berman. 1975. Description of the palate and
lower jaw of the sauropod dinosaur Diplodocus (Reptilia: Saur-
ischia) with remarks on the nature of the skull of Apatosaurus.
Journal of Paleontology 49:187-199.</mixed-citation>
         </ref>
         <ref id="d2509e1011a1310">
            <mixed-citation id="d2509e1015" publication-type="other">
Migeod, F. W. H. 1931. British Museum East Africa Expedition: Ac-
count of the work done in 1930. Natural History Magazine 3:87-103.</mixed-citation>
         </ref>
         <ref id="d2509e1025a1310">
            <mixed-citation id="d2509e1029" publication-type="other">
Murray, P. F., and Vickers-Rich, P. 2004. Magnificent mihirungs. Indiana
University Press, Bloomington, Indiana, 410 pp.</mixed-citation>
         </ref>
         <ref id="d2509e1040a1310">
            <mixed-citation id="d2509e1044" publication-type="other">
Naish, D., D. M. Martill, D. Cooper, and K. A. Stevens. 2004. Europe's
largest dinosaur? A giant brachiosaurid cervical vertebra from the
Wessex Formation (Early Cretaceous) of southern England. Creta-
ceous Research 25:787-795.</mixed-citation>
         </ref>
         <ref id="d2509e1060a1310">
            <mixed-citation id="d2509e1064" publication-type="other">
Olshevsky, G. 1991. A revision of the parainfraclass Archosauria Cope,
1869, excluding the advanced Crocodylia. Mesozoic Meanderings
2:1-196.</mixed-citation>
         </ref>
         <ref id="d2509e1077a1310">
            <mixed-citation id="d2509e1081" publication-type="other">
Osborn, H. F., and C. C. Mook. 1921. Camarasaurus, Amphicoelias and
other sauropods of Cope. Memoirs of the American Museum of
Natural History, n.s. 3:247-387.</mixed-citation>
         </ref>
         <ref id="d2509e1094a1310">
            <mixed-citation id="d2509e1098" publication-type="other">
Owen, R. 1842. Report on British fossil reptiles, Part II. Reports of
the British Association for the Advancement of Sciences 11:
60-204.</mixed-citation>
         </ref>
         <ref id="d2509e1111a1310">
            <mixed-citation id="d2509e1115" publication-type="other">
Paul, G. S. 1988. The brachiosaur giants of the Morrison and Tendaguru
with a description of a new subgenus, Giraffatitan, and a comparison
of the world's largest dinosaurs. Hunteria 2:1-14.</mixed-citation>
         </ref>
         <ref id="d2509e1128a1310">
            <mixed-citation id="d2509e1132" publication-type="other">
Paul, G. S. 1994. Dinosaur reproduction in the fast lane: implications for
size, success and extinction; pp. 244-255 in K. Carpenter,
K. F. Hirsch, and J. R. Horner (eds.), Dinosaur Eggs and Babies.
Cambridge University Press, Cambridge.</mixed-citation>
         </ref>
         <ref id="d2509e1149a1310">
            <mixed-citation id="d2509e1153" publication-type="other">
Paul, G. S. 1998. Terramegathermy and Cope's rule in the land of titans.
Modern Geology 23:179-217.</mixed-citation>
         </ref>
         <ref id="d2509e1163a1310">
            <mixed-citation id="d2509e1167" publication-type="other">
Paul, G. S. 2000. Restoring the life appearances of dinosaurs; pp. 78-106
in G. S. Paul (ed.), The Scientific American book of dinosaurs. St.
Martin's Press, New York.</mixed-citation>
         </ref>
         <ref id="d2509e1180a1310">
            <mixed-citation id="d2509e1184" publication-type="other">
Powell, J. E. 1992. Osteología de Saltasaurus loricatus (Sauropoda-
Titanosauridae) del Cretácico Superior del Noroeste Argentino; pp.
165-230 in J. L. Sanz, and A. D. Buscalioni (eds.), Los Dinosaurios y
su Entorno Biotico. Actas del Segundo Curso de Paleontologia en
Cuenca. Instituto Juan de Valdés, Ayuntamiento de Cuenca.</mixed-citation>
         </ref>
         <ref id="d2509e1203a1310">
            <mixed-citation id="d2509e1207" publication-type="other">
Riggs, E. S. 1901. The largest known dinosaur. Science 13:549-550.</mixed-citation>
         </ref>
         <ref id="d2509e1214a1310">
            <mixed-citation id="d2509e1218" publication-type="other">
Riggs, E. S. 1903. Brachiosaurus altithorax, the largest known dinosaur.
American Journal of Science 15:299-306.</mixed-citation>
         </ref>
         <ref id="d2509e1228a1310">
            <mixed-citation id="d2509e1232" publication-type="other">
Riggs, E. S. 1904. Structure and relationships of opisthocoelian dino-
saurs. Part II, the Brachiosauridae. Field Columbian Museum, Geo-
logical Series 2, 6:229-247.</mixed-citation>
         </ref>
         <ref id="d2509e1246a1310">
            <mixed-citation id="d2509e1250" publication-type="other">
Romer, A. S. 1956. Osteology of the Reptiles. University of Chicago
Press, Chicago, 772 pp.</mixed-citation>
         </ref>
         <ref id="d2509e1260a1310">
            <mixed-citation id="d2509e1264" publication-type="other">
Saigado, L., and J. O. Calvo. 1997. Evolution of titanosaurid sauropods.
II: the cranial evidence. Ameghiniana 34:33-48.</mixed-citation>
         </ref>
         <ref id="d2509e1274a1310">
            <mixed-citation id="d2509e1278" publication-type="other">
Saigado, L., R. A. Coria, and J. O. Calvo. 1997. Evolution of titanosaurid
sauropods. I: Phylogenetic analysis based on the postcranial evi-
dence. Ameghiniana 34:3-32.</mixed-citation>
         </ref>
         <ref id="d2509e1291a1310">
            <mixed-citation id="d2509e1295" publication-type="other">
Saigado, L., A. Garrido, S. E. Cocca, and J. R. Cocca. 2004. Lower
Cretaceous rebbachisaurid sauropods from Cerro Aguada Del
Leon (Lohan Cura Formation), Neuquen Province, Northwestern
Patagonia, Argentina. Journal of Vertebrate Paleontology 24:
903-912.</mixed-citation>
         </ref>
         <ref id="d2509e1314a1310">
            <mixed-citation id="d2509e1318" publication-type="other">
Seeley, H. G. 1882. On a remarkable dinosaurian coracoid from the
Wealden of Brook in the Isle of Wight, preserved in the Wood-
wardian Museum of Cambridge, probably referable to Ornithop-
sis. Quarterly Journal of the Geological Society, London 38:
367-371.</mixed-citation>
         </ref>
         <ref id="d2509e1337a1310">
            <mixed-citation id="d2509e1341" publication-type="other">
Seeley, H. G. 1888. On the classification of the fossil animals commonly
named Dinosauria. Proceedings of the Royal Society of London
43:165-171.</mixed-citation>
         </ref>
         <ref id="d2509e1355a1310">
            <mixed-citation id="d2509e1359" publication-type="other">
Sereno, P. C. 1998. A rationale for phylogenetic definitions, with appli-
cation to the higher-level taxonomy of Dinosauria. Neues Jahrbuch
für Geologie und Paläontologie, Abhandlungen 210:41-83.</mixed-citation>
         </ref>
         <ref id="d2509e1372a1310">
            <mixed-citation id="d2509e1376" publication-type="other">
Sereno, P. C., A. L. Beck, D. B. Dutheil, H. C. E. Larsson, G. H. Lyon,
B. Moussa, R. W. Sadleir, C. A. Sidor, D. J. Varricchio,
G. P. Wilson, and J. A. Wilson. 1999. Cretaceous sauropods from
the Sahara and the uneven rate of skeletal evolution among dino-
saurs. Science 282:1342-1347.</mixed-citation>
         </ref>
         <ref id="d2509e1395a1310">
            <mixed-citation id="d2509e1399" publication-type="other">
Swofford, D. L. 2002. PAUP*: Phylogenetic Analysis Using Parsi-
mony (* and Other Methods). Sinauer Associates, Sunderland,
Massachusetts.</mixed-citation>
         </ref>
         <ref id="d2509e1412a1310">
            <mixed-citation id="d2509e1416" publication-type="other">
Taylor, M. P. 2005. Sweet seventy-five and never been kissed: the
Natural History Museum's Tendaguru brachiosaur; pp. 25-25 in
P. M. Barrett (ed.), Abstracts volume for 53rd Symposium of Ver-
tebrae Palaeontology and Comparative Anatomy. The Natural His-
tory Museum, London.</mixed-citation>
         </ref>
         <ref id="d2509e1435a1310">
            <mixed-citation id="d2509e1439" publication-type="other">
Taylor, M. P. 2007. Phylogenetic definitions in the pre-PhyloCode era;
implications for naming clades under the PhyloCode. PaleoBios
27:1-6.</mixed-citation>
         </ref>
         <ref id="d2509e1452a1310">
            <mixed-citation id="d2509e1456" publication-type="other">
Taylor, M. P., and D. Naish. 2007. An unusual new neosauropod dino-
saur from the Lower Cretaceous Hastings Beds Group of East Sus-
sex, England. Palaeontology 50:1547-1564.</mixed-citation>
         </ref>
         <ref id="d2509e1470a1310">
            <mixed-citation id="d2509e1474" publication-type="other">
Tidwell, V., and D. R. Wilhite. 2005. Ontogenetic variation and isometric
growth in the forelimb of the Early Cretaceous sauropod Veneno-
saurus; pp. 187-198 in V. Tidwell, and K. Carpenter (eds.), Thunder
Lizards: the Sauropodomorph Dinosaurs. Indiana University Press,
Bloomington, Indiana.</mixed-citation>
         </ref>
         <ref id="d2509e1493a1310">
            <mixed-citation id="d2509e1497" publication-type="other">
Tidwell, V., K. Carpenter, and S. Meyer. 2001. New Titanosauriform
(Sauropoda) from the Poison Strip Member of the Cedar Mountain
Formation (Lower Cretaceous), Utah; pp. 139-165 in D. H. Tanke,
and K. Carpenter (eds.), Mesozoic Vertebrate Life: New Research
inspired by the Paleontology of Philip J. Currie. Indiana University
Press, Bloomington and Indianapolis, Indiana.</mixed-citation>
         </ref>
         <ref id="d2509e1520a1310">
            <mixed-citation id="d2509e1524" publication-type="other">
Turner, C. E., and F. Peterson. 1999. Biostratigraphy of dinosaurs in the
Upper Jurassic Morrison Formation of the Western Interior, U.S.A;
pp. 77-114 in D. D. Gillette (ed.), Vertebrate Paleontology in Utah
(Utah Geological Survey Miscellaneous Publication 99-1). Utah
Geological Survey, Salt Lake City, Utah.</mixed-citation>
         </ref>
         <ref id="d2509e1543a1310">
            <mixed-citation id="d2509e1547" publication-type="other">
Upchurch, P. 1995. The evolutionary history of sauropod dinosaurs. Phil-
osophical Transactions of the Royal Society of London, Series B
349:365-390.</mixed-citation>
         </ref>
         <ref id="d2509e1560a1310">
            <mixed-citation id="d2509e1564" publication-type="other">
Upchurch, P. 1998. The phylogenetic relationships of sauropod dino-
saurs. Zoological Journal of the Linnean Society 124:43-103.</mixed-citation>
         </ref>
         <ref id="d2509e1574a1310">
            <mixed-citation id="d2509e1578" publication-type="other">
Upchurch, P., and J. A. Wilson. 2007. Euhelopus zdanksyi and its bearing
on the evolution of East Asian sauropod dinosaurs; pp. 30-30 in
J. Liston (ed.), Abstracts volume for 55th Symposium of Vertebrae
Palaeontology and Comparative Anatomy. University of Glasgow,
Glasgow.</mixed-citation>
         </ref>
         <ref id="d2509e1598a1310">
            <mixed-citation id="d2509e1602" publication-type="other">
Upchurch, P., P. M. Barrett, and P. Dodson. 2004. Sauropoda; pp.
259-322 in D. B. Weishampel, P. Dodson, and H. Osmólska (eds.),
The Dinosauria. 2nd edition. University of California Press, Berke-
ley and Los Angeles.</mixed-citation>
         </ref>
         <ref id="d2509e1618a1310">
            <mixed-citation id="d2509e1622" publication-type="other">
Wedel, M. J. 2000. Reconstructing Brachiosaurus. Prehistoric Times 42:47.</mixed-citation>
         </ref>
         <ref id="d2509e1629a1310">
            <mixed-citation id="d2509e1633" publication-type="other">
Wedel, M. J. 2005. Postcranial skeletal pneumaticity in sauropods and its
implications for mass estimates; pp. 201-228 in J. A. Wilson, and
K. Curry-Rogers (eds.), The Sauropods: Evolution and Paleobiolo-
gy. University of California Press, Berkeley.</mixed-citation>
         </ref>
         <ref id="d2509e1649a1310">
            <mixed-citation id="d2509e1653" publication-type="other">
Wedel, M. J. 2006. Pneumaticity, neck length, and body size in sauro-
pods. Journal of Vertebrate Paleontology 26:3-137A.</mixed-citation>
         </ref>
         <ref id="d2509e1663a1310">
            <mixed-citation id="d2509e1667" publication-type="other">
Wedel, M. J., R. L. Cifelli, and R. K. Sanders. 2000. Osteology, paleobi-
ology, and relationships of the sauropod dinosaur Sauroposeidon.
Acta Palaeontologica Polonica 45:343-388.</mixed-citation>
         </ref>
         <ref id="d2509e1680a1310">
            <mixed-citation id="d2509e1684" publication-type="other">
Wilson, J. A. 1999. A nomenclature for vertebral laminae in sauropods
and other saurischian dinosaurs. Journal of Vertebrate Paleontology
19:639-653.</mixed-citation>
         </ref>
         <ref id="d2509e1698a1310">
            <mixed-citation id="d2509e1702" publication-type="other">
Wilson, J. A. 2002. Sauropod dinosaur phylogeny: critique and cladistic
analysis. Zoological Journal of the Linnean Society 136:217-276.</mixed-citation>
         </ref>
         <ref id="d2509e1712a1310">
            <mixed-citation id="d2509e1716" publication-type="other">
Wilson, J. A., and P. C. Sereno. 1998. Early evolution and higher-level
phylogeny of sauropod dinosaurs. Society of Vertebrate Paleontolo-
gy Memoir 5:1-68.</mixed-citation>
         </ref>
         <ref id="d2509e1729a1310">
            <mixed-citation id="d2509e1733" publication-type="other">
Young, C.-C. 1958. New sauropods from China. Vertebrata Palasiatica
2:1-28.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
