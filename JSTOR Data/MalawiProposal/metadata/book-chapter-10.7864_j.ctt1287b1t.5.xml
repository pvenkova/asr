<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1287b1t</book-id>
      <subj-group>
         <subject content-type="call-number">HG1725.F88 2005</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Banks and banking</subject>
         <subj-group>
            <subject content-type="lcsh">Government ownership</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Financial institutions</subject>
         <subj-group>
            <subject content-type="lcsh">Government ownership</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Privatization</subject>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Business</subject>
         <subject>Finance</subject>
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>The Future of State-Owned Financial Institutions</book-title>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="editor" id="contrib1">
            <name name-style="western">
               <surname>CAPRIO</surname>
               <given-names>GERARD</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib2">
            <name name-style="western">
               <surname>FIECHTER</surname>
               <given-names>JONATHAN L.</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib3">
            <name name-style="western">
               <surname>LITAN</surname>
               <given-names>ROBERT E.</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib4">
            <name name-style="western">
               <surname>POMERLEANO</surname>
               <given-names>MICHAEL</given-names>
            </name>
         </contrib>
         <role content-type="editor">Editors</role>
      </contrib-group>
      <pub-date>
         <day>01</day>
         <month>12</month>
         <year>2010</year>
      </pub-date>
      <isbn content-type="epub">9780815717065</isbn>
      <isbn content-type="epub">0815717067</isbn>
      <publisher>
         <publisher-name>Brookings Institution Press</publisher-name>
         <publisher-loc>Washington, D.C.</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2004</copyright-year>
         <copyright-holder>THE BROOKING INSTITUTION</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7864/j.ctt1287b1t"/>
      <abstract abstract-type="short">
         <p>Research suggests that if the majority of a country's financial institutions are owned by the state, that country will experience slower financial development, less efficient financial systems, less private sector credit, and slower GDP growth. Yet more than 40 percent of the world's population live in countries in which public sector institutions dominate the banking system. In<italic>The Role of State-Owned Financial Institutions: Policy and Practice</italic>noted experts discuss the challenges presented by state-owned financial institutions and offer cross-disciplinary solutions for policymakers and banking regulators. The issues include: methods for effectively managing, reforming, and privatizing state-owned banks; the fiscal costs and contingent liabilities of state-owned banks; macroeconomic implications and the impact of state-owned banking on access to credit in an economy; guidance for effective supervision of state-owned banks; managerial perspectives on improving products, human resources, and risk; management case studies of different methods of privatization, such as initial public offerings, employee stock ownership plans, and strategic investors Contributors include David Binns (Beyster Institute), Robert Cull (World Bank), Ron Gilbert (ESOP Services), James A. Hanson (World Bank), Richard Hemming (International Monetary Fund), Fred Huibers (ING Research), Arminio Fraga (formerly Central Bank of Brazil), Nicholas Lardy (Institute for International Economics), David Marston (International Monetary Fund), Moody's Global Investor Service, Herman Mulder (ABN-Amro), William Nichol (Deutsche Bank AG), Urjit Patel (Infrastructure Development Finance Company, India), and P. S. Srinivas (World Bank).</p>
      </abstract>
      <counts>
         <page-count count="382"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1287b1t.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1287b1t.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1287b1t.3</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Caprio</surname>
                           <given-names>Gerard</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Fiechter</surname>
                           <given-names>Jonathan L.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib3" xlink:type="simple">
                        <name name-style="western">
                           <surname>Litan</surname>
                           <given-names>Robert E.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib4" xlink:type="simple">
                        <name name-style="western">
                           <surname>Pomerleano</surname>
                           <given-names>Michael</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1287b1t.4</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Introduction</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>CAPRIO</surname>
                           <given-names>GERARD</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>FIECHTER</surname>
                           <given-names>JONATHAN L.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib3" xlink:type="simple">
                        <name name-style="western">
                           <surname>LITAN</surname>
                           <given-names>ROBERT E.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib4" xlink:type="simple">
                        <name name-style="western">
                           <surname>POMERLEANO</surname>
                           <given-names>MICHAEL</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>It was only a short time ago that it was fashionable for some to speak of “the end of history”—or the conversion of most of the world’s economies to various forms of democratic capitalism. The tragic events of September 11, 2001, and the terrorism leading up to and following the Iraq war clearly demonstrate that democracy has not yet taken root in much of the world. Meanwhile, on the economic front—even outside former planned economies (such as China)—neither has capitalism completed its triumph. The state, it turns out, still is alive and well in owning a key</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1287b1t.5</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>The Transformation of State-Owned Banks</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>HANSON</surname>
                           <given-names>JAMES A.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>13</fpage>
                  <abstract>
                     <p>State-owned banks often have high nonperforming assets and high costs and make only a limited contribution to development. Improving their performance is important because they still dominate banking for the majority of people in developing countries, despite the rash of bank privatizations in the 1990s. In 2002 these banks held 60 percent or more of bank assets in Algeria, Bangladesh, China, Egypt, Ethiopia, India, Indonesia, Iran, and Vietnam (table 2-1).¹ In Latin America, privatizations have reduced the role of state-owned banks, but they remain important in Argentina, Brazil, Ecuador, and Uruguay. In the transition countries, the role of state-owned</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1287b1t.6</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Observations from an International Monetary Fund Survey</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>MARSTON</surname>
                           <given-names>DAVID</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>NARAIN</surname>
                           <given-names>ADITYA</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>51</fpage>
                  <abstract>
                     <p>The state has a major presence in the financial sector of many countries around the world. This is particularly visible in banking, where despite several privatization initiatives over the last decade, public sector banks are still estimated to account for a significant portion of total banking sector assets.¹</p>
                     <p>State intervention, however, is not confined to ownership in banking but also extends, albeit in a lesser degree, to insurance and contractual savings schemes. Further, though state intervention is often more prominent in the developing world, it can also play an important role in the developed world, taking a variety of different</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1287b1t.7</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Fiscal Transparency and State-Owned Banks</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>FOUAD</surname>
                           <given-names>MANAL</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>HEMMING</surname>
                           <given-names>RICHARD</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib3" xlink:type="simple">
                        <name name-style="western">
                           <surname>LOMBARDO</surname>
                           <given-names>DAVIDE</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib4" xlink:type="simple">
                        <name name-style="western">
                           <surname>MALISZEWSKI</surname>
                           <given-names>WOJCIECH</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>75</fpage>
                  <abstract>
                     <p>Insufficient attention has been paid to the activities of public financial institutions in discussions of fiscal transparency, although the benefits of fiscal transparency are widely recognized. These benefits are highlighted in the International Monetary Fund’s<italic>Manual on Fiscal Transparency,</italic>which opens by referring to “a clear consensus that good governance is of central importance to achieving macroeconomic stability and high-quality growth, and that fiscal transparency is a key aspect of good governance.”¹ An important requirement of fiscal transparency is the provision of information to the legislature and the public about all fical activities, no matter where in the public</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1287b1t.8</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>State-Owned Banks in China</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>LARDY</surname>
                           <given-names>NICHOLAS R.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>93</fpage>
                  <abstract>
                     <p>China’s transition to a market economy has proceeded gradually over the past twenty-five years. Perhaps it is not surprising that progress across product, labor, and capital markets has been highly differentiated. As outlined in this chapter, in sharp contrast to the situation twenty-five years ago, when only a tiny fraction of farm output was sold at market prices outside of state procurement channels, transactions in product markets are now almost entirely at market-determined prices. This is true not only for agricultural output and consumer goods but even for steel, machinery, and other investment goods. The system of lifetime employment</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1287b1t.9</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>State-Owned Banks in Indonesia</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>SRINIVAS</surname>
                           <given-names>P. S.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>SITORUS</surname>
                           <given-names>DJAUHARI</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>123</fpage>
                  <abstract>
                     <p>As in much of the rest of the developing world, state-owned banks in Indonesia originated with the government’s objectives to channel resources to “priority” sectors of the economy as well as to provide financial services to underserved parts of a widely dispersed country. Each of the banks was also established with a mandate to finance a specific sector of the economy. For much of the country’s history, these banks have played a major role in the country’s economy—at times controlling over three-fourths of deposits and assets of the banking system—and they continue to control almost half the assets</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1287b1t.10</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Role of State-Owned Financial Institutions in India:</title>
                     <subtitle>Should the Government “Do” or “Lead”?</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>PATEL</surname>
                           <given-names>URJIT R.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>181</fpage>
                  <abstract>
                     <p>A deep and efficient financial sector is necessary for the optimal allocation of resources. Governments have been involved in the financial sectors—in intermediation, if not directly as the owners of intermediaries—of many countries, even currently developed ones, during various stages of their growth. In many countries, development financial institutions have been major conduits for channeling funds to particular firms, industries, and sectors during their development. Many studies (more recently by Allen and Gale, and by Levine) identify the importance of development financial institutions in the South Korean and Japanese process of industrialization.¹ In some developed countries, such as</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1287b1t.11</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>Lessons from Southern Africa</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>MUSASIKE</surname>
                           <given-names>LEWIS</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>STILWELL</surname>
                           <given-names>TED</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib3" xlink:type="simple">
                        <name name-style="western">
                           <surname>MAKHURA</surname>
                           <given-names>MORAKA</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib4" xlink:type="simple">
                        <name name-style="western">
                           <surname>JACKSON</surname>
                           <given-names>BARRY</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib5" xlink:type="simple">
                        <name name-style="western">
                           <surname>KIRSTEN</surname>
                           <given-names>MARIE</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>211</fpage>
                  <abstract>
                     <p>The state has always played a role in the economies of both developed and developing nations, although the dominance of that role has changed over time in response to globalization, privatization, and the perceived efficiencies of the private sector. The establishment of the Bretton Woods institutions was a landmark in the reshaping of the international financial system—the development financial system in particular.</p>
                     <p>In the 1960s, as former colonies gained nationhood and independence from western nations, regional economic and political groups established their own development institutions, such as the African Development Bank, the Asian Development Bank, and the Inter-American</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1287b1t.12</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>Lessons from Indonesia</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>RUDJITO</surname>
                           <given-names>PAK</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>TRANGGANA</surname>
                           <given-names>HENDRAWAN</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>229</fpage>
                  <abstract>
                     <p>Indonesian state-owned enterprises are major actors in national economic development. The activities managed by these enterprises cover almost all sectors in Indonesia, and almost all people must deal with them in one way or another.</p>
                     <p>The economic crisis in Indonesia in 1997–98 caused the financial performance of many state-owned enterprises with a strong position in the national economy to severely drop, with a lessening of the contribution of these enterprises to the country’s revenue. As an illustration, the contribution (taxes and dividends) of fourteen of the top state–owned enterprises in 2000 was only Rp 13.6 trillion,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1287b1t.13</book-part-id>
                  <title-group>
                     <label>10</label>
                     <title>Principles for Supervision</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>FIECHTER</surname>
                           <given-names>JONATHAN L.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>KUPIEC</surname>
                           <given-names>PAUL H.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>241</fpage>
                  <abstract>
                     <p>When policy discussions are focused on state-owned financial institutions, economists often suggest privatization as a means for improving economic efficiency and social welfare. In a wide range of settings, competitive market forces promote an efficient allocation of resources. Theory and experience suggest that public ownership and control of productive activities that could be undertaken by privately held competitive enterprises often leads to inefficiencies, market distortions, and governance arrangements that may facilitate the expropriation of government resources for the benefit of specific political interests.</p>
                     <p>While there may be cases in which specific financial sector needs are chronically undersupplied by the private</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1287b1t.14</book-part-id>
                  <title-group>
                     <label>11</label>
                     <title>Lessons from Pakistan</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>HUSAIN</surname>
                           <given-names>ISHRAT</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>259</fpage>
                  <abstract>
                     <p>Pakistan’s financial sector privatization, which began in the early 1990s and continues today, offers valuable lessons for policymakers in other emerging economies. Pakistan successfully reduced state ownership in the banking sector from 92 percent of assets in 1990 to 18.6 percent in 2004. The government also pushed through a number of very tough policies dealing with overstaffing, overbranching, and nonperforming loans and managed to attract top managers from international banks to bring an infusion of new skills to the sector. This chapter lays out the hurdles Pakistan faced and the measures taken to privatize the banking system.</p>
                     <p>In 1974, when</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1287b1t.15</book-part-id>
                  <title-group>
                     <label>12</label>
                     <title>Lessons from Uganda</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>KASEKENDE</surname>
                           <given-names>LOUIS</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>269</fpage>
                  <abstract>
                     <p>The privatization of the Uganda Commercial Bank (UCB) occurred during a period of comprehensive reforms aimed at introducing market-based mechanisms for resource allocation and price determination, moving away from controls of the 1960s and 1970s. The government recognized early in the process that the reforms were unlikely to achieve their objectives if the banking system remained insolvent and inefficient. This was also likely to undermine the use of open-market operations by the central bank in the context of a market-based monetary policy framework, something the government welcomed in order to ease the burden placed on fiscal policy in the pursuit</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1287b1t.16</book-part-id>
                  <title-group>
                     <label>13</label>
                     <title>Empirical Studies</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>CLARKE</surname>
                           <given-names>GEORGE R. G.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>CULL</surname>
                           <given-names>ROBERT</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib3" xlink:type="simple">
                        <name name-style="western">
                           <surname>SHIRLEY</surname>
                           <given-names>MARY</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>279</fpage>
                  <abstract>
                     <p>State ownership of large parts of the banking system is relatively rare in developed countries but widespread in less developed countries (table 13-1). In 1999 governments held controlling shares in banks representing more than 30 percent of banking sector assets in 25 percent of the seventy-eight less developed countries for which we have information, compared to only two (10 percent) of the twenty developed countries for which we have data.</p>
                     <p>The importance of state-owned banks in these developing countries contrasts worryingly with research findings that state ownership has pernicious effects in general and for banks in particular. A large</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1287b1t.17</book-part-id>
                  <title-group>
                     <label>14</label>
                     <title>Initial Public Offerings</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>HUIBERS</surname>
                           <given-names>FRED E.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>315</fpage>
                  <abstract>
                     <p>Since the early 1980s, a growing number of state-owned enterprises have been privatized, with the total value of privatization transactions in the period 1982–2000 amounting to more than $1 trillion.¹ After privatizing industrial firms, governments increasingly began to dispose of companies from regulated industries such as telecommunications, utilities, and banks.</p>
                     <p>Bank privatization has been prevalent throughout the world. Of the 101 countries that conducted privatization transactions between 1982 and 2000, about half privatized state-owned banks. Overall, privatization of state-owned banks accounted for approximately 11 percent of the number and 10 percent of the value of transactions. Bank privatization has</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1287b1t.18</book-part-id>
                  <title-group>
                     <label>15</label>
                     <title>Employee Stock Ownership Plans</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>BINNS</surname>
                           <given-names>DAVID M.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>GILBERT</surname>
                           <given-names>RONALD J.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>345</fpage>
                  <abstract>
                     <p>International competitive forces are accelerating the movement toward globalization of financial markets. As a result of lowered barriers to communications and information technology, political borders for financial services often include a much larger territory than do market areas for most goods and services. The rapid development of international financial markets has therefore increased the importance of restructuring financial institutions in developing countries to better facilitate their ability to provide the financial services that are key to supporting economic development.</p>
                     <p>Trade, the efficient use of resources, savings, and risk taking are the cornerstones of a growing economy; the biggest difference between</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1287b1t.19</book-part-id>
                  <title-group>
                     <title>Contributors</title>
                  </title-group>
                  <fpage>373</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1287b1t.20</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>375</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1287b1t.21</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>383</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
