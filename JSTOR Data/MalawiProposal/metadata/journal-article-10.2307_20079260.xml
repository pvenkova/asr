<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">jworldhistory</journal-id>
         <journal-id journal-id-type="jstor">j50000133</journal-id>
         <journal-title-group>
            <journal-title>Journal of World History</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Hawaii Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">10456007</issn>
         <issn pub-type="epub">15278050</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">20079260</article-id>
         <title-group>
            <article-title>Contested Hegemony: The Great War and the Afro-Asian Assault on the Civilizing Mission Ideology</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Michael</given-names>
                  <surname>Adas</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>3</month>
            <year>2004</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">15</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id>i20079256</issue-id>
         <fpage>31</fpage>
         <lpage>63</lpage>
         <permissions>
            <copyright-statement>Copyright 2004 University of Hawai'i Press</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/20079260"/>
         <abstract>
            <p>Perhaps the most fundamental and enduring effects of the worldwide crisis of the European colonial order brought on by the Great War of 1914-1918 resulted from the challenges it provoked on the part of Asian and African novelists, poets, philosophers, and emerging political leaders. Four years of indecisive, mechanized slaughter on the Western Front gave rise to spirited and widely publicized critiques of the civilizing mission ideology that had long been invoked to justify European dominance. Since at least the early nineteenth century, the credibility of the civilizing mission credo for European colonizers as well as subject peoples depended increasingly on its emphasis on the unprecedented superiority that Europeans had attained in science and technology over all other peoples and cultures. Some Indian and African, and indeed also European, intellectuals had challenged these gauges of European racial and historical preeminence in the decades before 1914. But the appalling uses to which European discoveries and inventions were put in the First World War raised profound doubts among intellectuals across four continents about the progressive nature of industrial civilization and its potential as the model for all of humanity to emulate. The highly contentious exchanges that these questions gave rise to in the postwar decades soon coalesced into arguably the first genuinely worldwide discourse and proved a critical prelude to the struggles for decolonization that followed.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d1310e114a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1310e121" publication-type="other">
Martine
Loutfi's discussion of the views of Jean Jaures in Littérature et colonialisme (Paris: Mouton,
1971), p. 119</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e133" publication-type="other">
Raoul Giradet, L'Idée coloniale en France, 1871-1962 (Paris: La Table
Ronde, 1972), pp. 96-98, 104-111</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e143a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1310e150" publication-type="other">
John Crawfurd, "On the Physical
and Mental Characteristics of the European and Asiatic Races," Transactions of the Ethno-
logical Society of London 5 (1867)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e162" publication-type="other">
Robert Knox, The Races of Men: A Philosophical
Enquiry into the Influence of Race over the Destinies of Nations (London, 1862)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e171" publication-type="other">
Gustave Le Bon, The Psychology of Peoples (London,
1899)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e181" publication-type="other">
C. S. Wake, "The Psychological Unity of Mankind," Memoirs Read before the
Anthropological Society of London 3 (1867-1868)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e190" publication-type="other">
Jacques Novicov [L'Avenir de la race blanche (Paris, 1897)]</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e196" publication-type="other">
Henry Maine [Short Essays and Reviews on the Educational Policy of the Government of India
from the "Englishman" (Calcutta, 1866)]</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e206a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1310e213" publication-type="other">
"Aptitudes of the Races," Transactions of the Ethnological Society of London 5 (1867),
p. 120</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e222" publication-type="other">
Alfred Russel Wallace, "The Development of Human Races under the Law of Nat-
ural Selection," Anthropological Review (1865)</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e232a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1310e239" publication-type="other">
Helen Callaway, Gender, Culture and Empire: European Women in Colonial Nigeria
(Urbana: University of Illinois Press, 1987), esp. pp. 139-145</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e250a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1310e257" publication-type="other">
Leila Ahmed, Women and Gender in Islam (New Haven, Conn.: Yale University Press,
1992), pp. 153-154</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e267a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d1310e274" publication-type="other">
"On Clive," in Macaulay, Poetry and Prose, ed. by G. M. Young (Cam-
bridge, Mass.: Harvard University Press, 1970)</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e284a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d1310e291" publication-type="other">
Arthur de Gobineau, Essai sur Vinégalité des
races humaines (Paris, 1853), vol. 1, pp. 150-152</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e300" publication-type="other">
James Hunt, "On the Negro's Place in
Nature," Memoirs Read before the Anthropological Society of London (1863-1864), p. 10</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e309" publication-type="other">
Le Bon, Psychology of Peoples, pp. 35-36</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e316a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d1310e323" publication-type="other">
Women's Role in Economic Development (New York: St. Martin's Press, 1979), chap-
ter 3</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e333a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d1310e340" publication-type="other">
Thomas Haskell's essays on "Capitalism and the Origins of the Humanitarian Sensibility,"
American Historical Review 90, nos. 3 and 4 (1985)</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e350a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d1310e357" publication-type="other">
John C. Greene, Science, Ideology and World View (Berke-
ley: University of California Press, 1981), p. 108</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e368a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d1310e375" publication-type="other">
Benjamin Kidd, The Control of the Tropics (London: Macmillan,
1898), pp. 14, 39, 52-55, 58, 83-84, 88-90</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e384" publication-type="other">
H. H. Johnson, "British West Africa and the
Trade of the Interior," Proceedings of the Royal Colonial Institute 20 (1888-1889), p. 91</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e393" publication-type="other">
Arthur Girault, Principes de colonisation et de legislation coloniale (Paris: Larose, 1895), p. 31</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e400a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d1310e407" publication-type="other">
Selections from Prison Notebooks, ed. and trans. Q. Hoare and G. Smith (New York,
1971), pp. 12-14, 55-63, 275-276</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e417a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d1310e424" publication-type="other">
Ira Klein, "Indian Nationalism and Anti-Industrialization: The
Roots of Gandhian Economics," South Asia 3 (1973)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e433" publication-type="other">
Bade Onimode, A Political Econ-
omy of the African Crisis (London: Zed, 1988), pp. 14-22 and chapters 6, 9, and 11</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e443a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d1310e450" publication-type="other">
"The Proper Place of Oriental Literature in Indian Collegiate Education," Proceed-
ings of the Bethune Society (February 1868), pp. 149, 154</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e460a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d1310e467" publication-type="other">
"Asia's Message to Europe," in Keshub Sen's Chunder Lectures in India (London: Cas-
sell, 1901) vol. 2, pp. 51, 61</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e477a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d1310e484" publication-type="other">
"Social Contest of Bengal Renaissance," in David Kopf and Joaarder Safiuddin, eds.,
Reflections on the Bengal Renaissance (Dacca: Institute of Bangladesh Studies, 1977), p. 140</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e495a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d1310e502" publication-type="other">
Floyd and Lillian Dotson, The Indian Minority of Zambia, Rhodesia, and Malawi (New
Haven, Conn.: Yale University Press, 1968), pp. 262-268, 320</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e512a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d1310e519" publication-type="other">
African Nationalism (London, Oxford University Pifess, 1969), p. 157</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e526a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d1310e533" publication-type="other">
Michael Adas, Machines as the Measure of Men: Science, Technology
and Ideologies of Western Dominance (Ithaca, N.Y.: 1989), chapter 5</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e543a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d1310e550" publication-type="other">
Esquisses sénégalaises (Paris: Bertrand, 1853), pp. 9-13, 478</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e556" publication-type="other">
André Villard, His-
toire du Sénégal (Dakar, 1943), p. 98</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e566a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d1310e573" publication-type="other">
West African Countries and Peoples (London: W. J. Johnson, 1868), pp. 1-4</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e579" publication-type="other">
Let-
ters on the Political Condition of the Gold Coast (London: W. J. Johnson, 1870), pp. i-iii</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e589a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d1310e596" publication-type="other">
"Hope for Africa: A Discourse," Colonization Journal (August 1861), pp. 7-8</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e602" publication-type="other">
"The Negro in Ancient History," in The People of Africa (New York, 1871), pp. 23-24, 34</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e610a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d1310e617" publication-type="other">
Michael Adas, Machines as the Measure of Men: Science, Technology and
Ideologies of Western Dominance (Ithaca, N.Y.: 1989), pp. 345-365</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e627a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d1310e634" publication-type="other">
"Reflections on War and Death," (1915)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e640" publication-type="other">
Philip Reiff, ed., Character
and Culture (New York: Collier, 1963), p. 118</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e649" publication-type="other">
Adas, Machines as the Measure of Men, chapter 6</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e656a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d1310e663" publication-type="other">
Christopher M.
Andrew and A. S. Kanya-Forstner, France Overseas: The Great War and the Climax of French
Imperialism (London: Thames and Hudson, 1981)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e675" publication-type="other">
John Darwin, The British in the Middle East 1918-1922 (London: Macmillan, 1981)</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e682a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d1310e689" publication-type="other">
Hugh Tinker, "Structure of the British Imperial Heritage," in Ralph Braibanti, ed.,
Asian Bureaucratic Systems Emergent from the British Imperial Tradition (Durham: Duke Uni-
versity Press, 1966), pp. 61-63</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e701" publication-type="other">
Henri Massis, Defense of the West (London: Faber,
1927), pp. 6, 9, 134</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e710" publication-type="other">
Robert Delavignette, Freedom and
Authority in West Africa (London, 1965), pp. 149-150</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e720a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d1310e727" publication-type="other">
Raoul Girardet, L'Idée coloniale en France, 1871-1962 (Paris: La Table Ronde, 1972),
pp. 117-132 and chapter 5</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e736" publication-type="other">
Thomas August, The Selling of Empire: British and French
Imperialist Propaganda, 1890-1940 (Westport: Greenwood, 1985), pp. 126-140</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e745" publication-type="other">
Frederick Cooper,
Decolonization and African Society: The Labor Question in British and French Africa (Cam-
bridge, 1996)</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e758a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d1310e765" publication-type="other">
Michael Adas, Machines as the Measure
of Men, chapter 6</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e776a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d1310e783" publication-type="other">
Duhamel, Civilisation, pp. 257-258, 268-269</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e789" publication-type="other">
Entretien sur l'esprit européen (Paris:
Mercure de France, 1928), pp. 17-18, 36-39</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e798" publication-type="other">
La Pensée âes âmes, (Paris: Mercure de France,
1949)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e808" publication-type="other">
La possession du monde (Paris: Mercure de France, 1919), pp. 18-19, 140,
242-245</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e818a1310">
            <label>31</label>
            <p>
               <mixed-citation id="d1310e825" publication-type="other">
"Letters from France I: The Spiritual Crisis," The Athenaeum (11 April 1919), p. 182</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e832a1310">
            <label>32</label>
            <p>
               <mixed-citation id="d1310e839" publication-type="other">
Civilisation, pp. 11-12</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e846a1310">
            <label>33</label>
            <p>
               <mixed-citation id="d1310e853" publication-type="other">
Possession du monde, p. 99</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e859" publication-type="other">
Journey
to the End of Night [New York: New Directions, 1960], p. 53</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e868" publication-type="other">
All Quiet on the Western Front [New York: Fawcett Crest, 1975], p. 61</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e875a1310">
            <label>34</label>
            <p>
               <mixed-citation id="d1310e882" publication-type="other">
Civiliza-
tion 1914-1918, chapter 16</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e892a1310">
            <label>35</label>
            <p>
               <mixed-citation id="d1310e899" publication-type="other">
"Soldier's Heart: Literary Men, Literary Women, and the Great War," Signs 8, no. 3
(1983): 435</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e910a1310">
            <label>36</label>
            <p>
               <mixed-citation id="d1310e917" publication-type="other">
Civilisation 1914-1918, pp. 282-283</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e924a1310">
            <label>37</label>
            <p>
               <mixed-citation id="d1310e931" publication-type="other">
Mary Louise Pratt, Imperial Eyes: Travel Writing and Transcultura-
tion (London: Routledge, 1992)</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e941a1310">
            <label>38</label>
            <p>
               <mixed-citation id="d1310e948" publication-type="other">
Roland Rorgelès, Les croix de Bois (Paris:
Albin Michel, 1919), pp. 62, 113</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e957" publication-type="other">
Henri Barbusse, Under Fire: The Story of a Squad (Lon-
don: 1916)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e966" publication-type="other">
Remarque, All Quiet, pp. 103-104, 236-237</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e973" publication-type="other">
Richard Aldington, The
Death of a Hero (London: 1984), pp. 255, 264, 267</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e983a1310">
            <label>39</label>
            <p>
               <mixed-citation id="d1310e990" publication-type="other">
Manning's Middle Parts of Fortune (New York: St. Martins,
1972), pp. 8, 12, 39-40</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e999" publication-type="other">
Remarque, All Quiet, pp. 12-13, 124-125,
236-237</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e1008" publication-type="other">
Aldington, Death of a Hero, p. 362</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e1015" publication-type="other">
Ludwig Renn, War (London: Antony
Mott, 1984), pp. 110-111</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e1025a1310">
            <label>40</label>
            <p>
               <mixed-citation id="d1310e1032" publication-type="other">
Léon-François Hoffman, Le nègre romantique (Paris, 1973)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e1038" publication-type="other">
Eric J. Leed, No
Man's Land: Combat and Identity in World War I (Cambridge: Cambridge University Press,
1979), pp. 126-127</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e1051a1310">
            <label>41</label>
            <p>
               <mixed-citation id="d1310e1058" publication-type="other">
Robert Wohl, The Generation of 1Q14 (Cambridge, Mass.: Harvard University Press,
1979), pp. 12-13</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e1069a1310">
            <label>42</label>
            <p>
               <mixed-citation id="d1310e1076" publication-type="other">
Civilisation 1914-1918, p. 268</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e1082" publication-type="other">
Civilization 1914-1918, p. 272</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e1089a1310">
            <label>43</label>
            <p>
               <mixed-citation id="d1310e1096" publication-type="other">
La Possession du
monde, esp. pp. 140, 242-246, 254-256, 264-265</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e1105" publication-type="other">
Entretien sur l'esprit européen, pp.
17-18, 20-22, 29-37, 40-46, 50</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e1115a1310">
            <label>44</label>
            <p>
               <mixed-citation id="d1310e1122" publication-type="other">
Les espoirs et les épreuves, (Paris: Mercure de France, 1953), pp.
135-136, 186-187</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e1132a1310">
            <label>45</label>
            <p>
               <mixed-citation id="d1310e1139" publication-type="other">
America the Menace: Scenes from the Life of the Future
(London: Allen and Unwin, 1931)</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e1149a1310">
            <label>46</label>
            <p>
               <mixed-citation id="d1310e1156" publication-type="other">
Mal, Dayanand (1962), pp. 66-68, 73, 216</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e1162" publication-type="other">
Dayanda, Satyarth Prakash, pp.
292-3</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e1172a1310">
            <label>47</label>
            <p>
               <mixed-citation id="d1310e1179" publication-type="other">
Collected Works, vol. 1, pp. 13, 134</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e1185" publication-type="other">
vol. 2, pp. 124, 140-141</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e1193a1310">
            <label>48</label>
            <p>
               <mixed-citation id="d1310e1200" publication-type="other">
Vivekananda, Collected Works, vol. 1, pp. 121, 365</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e1206" publication-type="other">
The Crisis of the Modern
World (London, 1924), pp. 24-26, 66-67, 125-126</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e1215" publication-type="other">
East and West (London, 1941),
pp. 23-26, 36-39, 43-44, 57-62, 68</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e1225a1310">
            <label>49</label>
            <p>
               <mixed-citation id="d1310e1234" publication-type="other">
Vivekananda, Collected Works, vol. 2, pp. 410-411</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e1240" publication-type="other">
The
Dance of Shiva (London: Sunrise Turn Press, 1924)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e1250" publication-type="other">
E. P. Thompson, William Morris: Romantic to Revolutionary (London:
Lawrence and Wisehart, 1955)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e1259" publication-type="other">
Peter Stansky, Redesigning the World: William Morris,
the 1880s, and the Arts and Crafts (Princeton, N.J.: Princeton University Press, 1985)</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e1269a1310">
            <label>50</label>
            <p>
               <mixed-citation id="d1310e1276" publication-type="other">
V. S. Narvane, Modern
Indian Thought (Bombay: Asia Publishing House, 1964), p. 106</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e1285" publication-type="other">
Collected Works,
vol. 4, pp. 410-411</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e1295a1310">
            <label>51</label>
            <p>
               <mixed-citation id="d1310e1302" publication-type="other">
Mary M. Lago, ed., Imperfect Encounter: Letters of William Rothenstein and Rabindra-
nath Tagore (New Haven, Conn.: Yale University Press, 1972), pp. 189, 191</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e1312a1310">
            <label>52</label>
            <p>
               <mixed-citation id="d1310e1319" publication-type="other">
Carolyn Merchant has convincingly demonstrated in The Death of Nature (New
York: Harper and Row, 1983)</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e1329a1310">
            <label>53</label>
            <p>
               <mixed-citation id="d1310e1336" publication-type="other">
Diary of a Westward Voyage (Bombay: Asia Publishing House, 1962), pp. 68-69, 71-74,
96-97</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e1345" publication-type="other">
Letters from Abroad (Madras: Ganesan, 1924), pp. 18, 56, 66, 83-85, 130</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e1351" publication-type="other">
Person-
ality (London: Macmillan, 1917), pp. 50, 52, 169-175, 181-182</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e1361" publication-type="other">
Nationalism (London,
Macmillan, 1917), pp. 33, 37, 44-45, 77, 91-92</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e1372a1310">
            <label>54</label>
            <p>
               <mixed-citation id="d1310e1379" publication-type="other">
Gandhi's writings in Young India</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e1386a1310">
            <label>55</label>
            <p>
               <mixed-citation id="d1310e1393" publication-type="other">
Adas, Machines as the Measure of Men, pp. 221-236</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e1400a1310">
            <label>56</label>
            <p>
               <mixed-citation id="d1310e1407" publication-type="other">
War
and Self-Determination [Calcutta: Sarojini Chose, n.d. (c. 1924)]</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e1416" publication-type="other">
After the War (Pondi-
cherry: Shri Aurobindo Ashram, 1949)</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e1426a1310">
            <label>57</label>
            <p>
               <mixed-citation id="d1310e1433" publication-type="other">
"After the War," pp. 10, 13</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e1440a1310">
            <label>58</label>
            <p>
               <mixed-citation id="d1310e1447" publication-type="other">
The Story of My Experiments with Truth: The Autobiography of Mahatma Gandhi (Bos-
ton: Beacon, 1957 ed.), pp. 346-348</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e1456" publication-type="other">
Louis Fischer, The Life of Mahatma Gandhi (New
York: Collier, 1962), pp. 133, 164-165, 180, 288-290</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e1466a1310">
            <label>59</label>
            <p>
               <mixed-citation id="d1310e1473" publication-type="other">
Young India, December 8, 1927</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e1481a1310">
            <label>60</label>
            <p>
               <mixed-citation id="d1310e1488" publication-type="other">
Young India, October 7, 1926</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e1495a1310">
            <label>61</label>
            <p>
               <mixed-citation id="d1310e1502" publication-type="other">
Michael Crowder, "Senegal: A Study in French Assimilationist Policy" (Lon-
don: Methuen, 1967), chapters one and two</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e1512a1310">
            <label>62</label>
            <p>
               <mixed-citation id="d1310e1519" publication-type="other">
John A. Harris, "The 'New Attitude' of the African," Fortnightly
Review 108 (1920), pp. 953-960</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e1528" publication-type="other">
G. St. John Orde-Browne, The Vanishing Tribes of
Kenya (London: Seeley, Service, 1925), p. 271</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e1538a1310">
            <label>63</label>
            <p>
               <mixed-citation id="d1310e1545" publication-type="other">
Batouala, trans. Barbara Beck and Alexandre Mboukou (London: Heinemann,
1973), p. 74</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e1555a1310">
            <label>64</label>
            <p>
               <mixed-citation id="d1310e1562" publication-type="other">
Ibid., pp. 29-31, 47-50, 75-76</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e1569a1310">
            <label>65</label>
            <p>
               <mixed-citation id="d1310e1576" publication-type="other">
The Negroes of Africa, first published in
1921</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e1585" publication-type="other">
Gérard Leclerc, Anthropologie et colonialisme: Essai sur l'histoire de
l'africainisme (Paris, 1972), pp. 43-52</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e1594" publication-type="other">
Girardet, L'Idée coloniale, pp. 158-164</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e1602a1310">
            <label>66</label>
            <p>
               <mixed-citation id="d1310e1609" publication-type="other">
S. Okechukwu Megu, Leopold Sedar Senghor et la défense
et illustration de la. civilisation noire (Paris, 1968), especially pp. 32-36</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e1619a1310">
            <label>67</label>
            <p>
               <mixed-citation id="d1310e1626" publication-type="other">
Crowder, Colonial West Africa, pp. 408-412</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e1633a1310">
            <label>68</label>
            <p>
               <mixed-citation id="d1310e1640" publication-type="other">
Dorothy S. Blair, African Literature in French (Cambridge: Cambridge University
Press, 1976), pp. 18-20</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e1650a1310">
            <label>69</label>
            <p>
               <mixed-citation id="d1310e1657" publication-type="other">
Ce que je crois (Paris: 1988), pp. 136-152</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e1664a1310">
            <label>70</label>
            <p>
               <mixed-citation id="d1310e1671" publication-type="other">
"Snow upon Paris," by
John Reed and Clive Wake in Senghor, Selected Poems (Oxford: 1964)</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e1681a1310">
            <label>71</label>
            <p>
               <mixed-citation id="d1310e1688" publication-type="other">
"Solde," published in Pigments (Paris: 1962)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1310e1694" publication-type="other">
Abiola Irele,
"Négritude or Black Cultural Nationalism," The Journal of Modern African Studies 3, no. 3
(1965): 503</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e1708a1310">
            <label>72</label>
            <p>
               <mixed-citation id="d1310e1715" publication-type="other">
Cahier d'un retour au pays natal, translated as Return to My Native Land by John
Berger and Anna Bostock (Harmondsworth, 1969), p. 75</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e1725a1310">
            <label>73</label>
            <p>
               <mixed-citation id="d1310e1732" publication-type="other">
Paul Sorum in his treatment of the origins of Négri-
tude in Intellectuals and Decolonization in France (Chapel Hill, N.C. University of North
Carolina Press, 1977), pp. 213-214</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e1745a1310">
            <label>74</label>
            <p>
               <mixed-citation id="d1310e1752" publication-type="other">
Talal Asad, "Two European Images of
Non-European Rule," in Asad, Anthropology and the Colonial Encounter (New York: Human-
ities Press, 1973), pp. 103-118</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e1765a1310">
            <label>75</label>
            <p>
               <mixed-citation id="d1310e1772" publication-type="other">
Decolonising the Mind: The Politics of Language in African Literature (London: Heine-
mann, 1986)</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e1782a1310">
            <label>76</label>
            <p>
               <mixed-citation id="d1310e1789" publication-type="other">
"The Decline of the City of Mahagonny," The New Republic (28 June 1990), pp.
27-28</mixed-citation>
            </p>
         </fn>
         <fn id="d1310e1799a1310">
            <label>77</label>
            <p>
               <mixed-citation id="d1310e1806" publication-type="other">
The Intimate Enemy: Loss and Recovery of Self under Colonialism (Delhi: Oxford Uni-
versity Press, 1983), esp. pp. xi-xiii</mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
