<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">popudeverevi</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100511</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Population and Development Review</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Blackwell Publishing</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00987921</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">17284457</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">25434704</article-id>
         <title-group>
            <article-title>Income per Natural: Measuring Development for People Rather than Places</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Michael A.</given-names>
                  <surname>Clemens</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Lant</given-names>
                  <surname>Pritchett</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>9</month>
            <year>2008</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">34</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i25434702</issue-id>
         <fpage>395</fpage>
         <lpage>434</lpage>
         <permissions>
            <copyright-statement>Copyright 2008 The Population Council, Inc.</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/25434704"/>
         <abstract>
            <p>It is easy to learn the average income of a resident of El Salvador or Albania. But there is no systematic source of information on the average income of a Salvadoran or Albanian. We estimate a new statistic: income per natural-the mean annual income of all people born in a given country, regardless of where those people now reside. Income per natural often differs substantially from income per resident, both in its mean and in its distribution. A large part of this difference is caused by movement across borders. Indeed, for people from a number of developing countries, departing their country of birth is one of the most important sources of poverty reduction and material advancement. If economic development is that which raises human well-being, then crossing international borders is not an alternative to economic development; it is a form of economic development.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Notes</title>
         <ref id="d1478e162a1310">
            <label>6</label>
            <mixed-citation id="d1478e171" publication-type="other">
Bound and Krueger (1991)</mixed-citation>
         </ref>
         <ref id="d1478e178a1310">
            <label>7</label>
            <mixed-citation id="d1478e185" publication-type="other">
Dumont and Lemaître (2005)</mixed-citation>
            <mixed-citation id="d1478e191" publication-type="other">
Docquier and
Marfouk (2005).</mixed-citation>
         </ref>
         <ref id="d1478e201a1310">
            <label>8</label>
            <mixed-citation id="d1478e208" publication-type="other">
Cutler,
Glaeser, and Vigdor 2007</mixed-citation>
         </ref>
         <ref id="d1478e218a1310">
            <label>12</label>
            <mixed-citation id="d1478e225" publication-type="other">
OECD (2007)</mixed-citation>
         </ref>
         <ref id="d1478e233a1310">
            <label>15</label>
            <mixed-citation id="d1478e242" publication-type="other">
Pritchett
2006a</mixed-citation>
         </ref>
         <ref id="d1478e252a1310">
            <label>16</label>
            <mixed-citation id="d1478e259" publication-type="other">
Beegle, De Weerdt, and Dercon (2008:
Table 4)</mixed-citation>
         </ref>
         <ref id="d1478e269a1310">
            <label>17</label>
            <mixed-citation id="d1478e276" publication-type="other">
Chiquiar and Hanson (2005)</mixed-citation>
         </ref>
         <ref id="d1478e283a1310">
            <label>18</label>
            <mixed-citation id="d1478e292" publication-type="other">
CDC (1995)</mixed-citation>
            <mixed-citation id="d1478e298" publication-type="other">
CDC 2007: Table 23</mixed-citation>
            <mixed-citation id="d1478e305" publication-type="other">
US Census Bureau 2007</mixed-citation>
            <mixed-citation id="d1478e311" publication-type="other">
CDC (2007: Table 23)</mixed-citation>
            <mixed-citation id="d1478e317" publication-type="other">
CDC (2007: Table
23)</mixed-citation>
         </ref>
         <ref id="d1478e327a1310">
            <label>20</label>
            <mixed-citation id="d1478e334" publication-type="other">
Ottaviano and Peri (2008)</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>References</title>
         <ref id="d1478e350a1310">
            <mixed-citation id="d1478e354" publication-type="other">
Amuedo-Dorantes, Catalina and Susan Pozo. 2006. "Migration, remittances, and male and
female employment patterns," American Economic Review 96(2): 222-226.</mixed-citation>
         </ref>
         <ref id="d1478e364a1310">
            <mixed-citation id="d1478e368" publication-type="other">
Anson, Jon. 2004. "The migrant mortality advantage: A 70 month follow-up of the Brussels
population, " European Journal of Population 20(3): 191-218.</mixed-citation>
         </ref>
         <ref id="d1478e378a1310">
            <mixed-citation id="d1478e382" publication-type="other">
Batista, Catia, Aitor Lacuesta, and Pedro C. Vicente. 2007, "Brain drain or brain gain? Micro
evidence from an African success story," Discussion Paper 343. Oxford, UK: Oxford Uni-
versity Department of Economics.</mixed-citation>
         </ref>
         <ref id="d1478e395a1310">
            <mixed-citation id="d1478e399" publication-type="other">
Beegle, Kathleen, Joachim De Weerdt, and Stefan Dercon. 2008. "Migration and economic
mobility in Tanzania: Evidence from a tracking survey," working paper (forthcoming).
Washington, DC: World Bank.</mixed-citation>
         </ref>
         <ref id="d1478e413a1310">
            <mixed-citation id="d1478e417" publication-type="other">
Borjas, George J. 2008. "Labor outflows and labor inflows in Puerto Rico," Journal of Human
Capital2(1): 32-68.</mixed-citation>
         </ref>
         <ref id="d1478e427a1310">
            <mixed-citation id="d1478e431" publication-type="other">
Bound, John and Alan B. Krueger. 1991. "The extent of measurement error in longitudinal
earnings data: Do two wrongs make a right?," Journal of Labor Economics 9(1): 1-24.</mixed-citation>
         </ref>
         <ref id="d1478e441a1310">
            <mixed-citation id="d1478e445" publication-type="other">
CDC. 1995. "Poverty and infant mortality—United States, 1988," Morbidity and Mortality Weekly
Report 44(49): 923-927.</mixed-citation>
         </ref>
         <ref id="d1478e455a1310">
            <mixed-citation id="d1478e459" publication-type="other">
-. 2007. Health, United States, 2006 edition. Atlanta: Centers for Disease Control and
Prevention.</mixed-citation>
         </ref>
         <ref id="d1478e469a1310">
            <mixed-citation id="d1478e473" publication-type="other">
Chiquiar, Daniel and Gordon H. Hanson. 2005. "International migration, self-selection, and the
distribution of wages: Evidence from Mexico and the United States," Journal of Political
Economy 113(2): 239-281.</mixed-citation>
         </ref>
         <ref id="d1478e486a1310">
            <mixed-citation id="d1478e490" publication-type="other">
Commander, Simon, Rupa Chanda, Mari Kangasniemi, and L. Alan Winters. 2004. "Must
skilled migration be a brain drain? Evidence from the Indian software industry," Discus-
sion Paper 1422. Bonn: Forschungsinstitut zur Zukunft der Arbeit (IZA).</mixed-citation>
         </ref>
         <ref id="d1478e504a1310">
            <mixed-citation id="d1478e508" publication-type="other">
Cutler, David M., Edward L. Glaeser, and Jacob L. Vigdor. 2007. "When are ghettos bad? Les-
sons from immigrant segregation in the United States," Working Paper 13082. Cambridge,
MA: National Bureau of Economic Research.</mixed-citation>
         </ref>
         <ref id="d1478e521a1310">
            <mixed-citation id="d1478e525" publication-type="other">
de Haas, Hein. 2006. "Migration, remittances and regional development in Morocco," Geoforum
37(4): 565-580.</mixed-citation>
         </ref>
         <ref id="d1478e535a1310">
            <mixed-citation id="d1478e539" publication-type="other">
Docquier, Frédéric and Abdeslam Marfouk. 2005. "International migration by educational at-
tainment, 1990-2000," in Maurice Schiff and Çağlar Özden (eds.), International Migration,
Remittances, and the Brain Drain. Washington, DC: World Bank, pp. 151-200.</mixed-citation>
         </ref>
         <ref id="d1478e552a1310">
            <mixed-citation id="d1478e558" publication-type="other">
Donato, Katharine M., Shawn Malia Kanaiaupuni, and Melissa Stainback. 2001. "The effects of
migration, household income, and gender on Mexican child health," CDE Working Paper
2001-10. University of Wisconsin, Madison: Center for Demography and Ecology.</mixed-citation>
         </ref>
         <ref id="d1478e571a1310">
            <mixed-citation id="d1478e575" publication-type="other">
Dumont, Jean-Christophe and Georges Lemaître. 2005. "Counting immigrants and expatri-
ates in OECD countries: A new perspective," OECD Social Employment and Migration
Working Papers, No. 25. Paris: OECD Publishing.</mixed-citation>
         </ref>
         <ref id="d1478e588a1310">
            <mixed-citation id="d1478e592" publication-type="other">
Fernández-Huertas Moraga, Jesús. 2006. "New evidence on immigrant selection," Job Market
Paper. New York: Columbia University.</mixed-citation>
         </ref>
         <ref id="d1478e603a1310">
            <mixed-citation id="d1478e607" publication-type="other">
Frank, Reanne. 2005. "International migration and infant health in Mexico," Journal of Im-
migrant Health 7 (1): 11-22.</mixed-citation>
         </ref>
         <ref id="d1478e617a1310">
            <mixed-citation id="d1478e621" publication-type="other">
Hanson, Gordon. 2007. "Emigration, remittances, and labor force participation in Mexico,"
Integration and Trade Journal 27(July—December): 73-103.</mixed-citation>
         </ref>
         <ref id="d1478e631a1310">
            <mixed-citation id="d1478e635" publication-type="other">
Hendricks, Lutz. 2002. "How important is human capital for development? Evidence from
immigrant earnings, " American Economic Review 92(1): 198-219.</mixed-citation>
         </ref>
         <ref id="d1478e645a1310">
            <mixed-citation id="d1478e649" publication-type="other">
Hummer, Robert A., Daniel A. Powers, Starling G. Pullum, Ginger L. Gossman, and W Parker
Frisbie. 2007. "Paradox found (again): Infant mortality among the Mexican-origin popu-
lation in the United States," Demography 44(3): 441-457.</mixed-citation>
         </ref>
         <ref id="d1478e662a1310">
            <mixed-citation id="d1478e666" publication-type="other">
Husted, Leif, Helena Skyt Nielsen, Michael Rosholm, and Nina Smith. 2000. "Employment
and wage assimilation of male first generation immigrants in Denmark," Working Paper
00-01. Aarhus, Denmark: Centre for Labour Market and Social Research.</mixed-citation>
         </ref>
         <ref id="d1478e679a1310">
            <mixed-citation id="d1478e683" publication-type="other">
Hyman, llene. 2001. "Immigration and health," Working Paper 01-05. Ottawa: Health
Canada.</mixed-citation>
         </ref>
         <ref id="d1478e694a1310">
            <mixed-citation id="d1478e698" publication-type="other">
Landale, Nancy S., R. S. Oropesa, and Bridget K. Gorman. 2000. "Migration and infant death:
Assimilation or selective migration among Puerto Ricans?," American Sociological Review
65(6): 888-909.</mixed-citation>
         </ref>
         <ref id="d1478e711a1310">
            <mixed-citation id="d1478e715" publication-type="other">
Lucas, Robert E. B. 2005. International Migration and Economic Development: Lessons from Low-
Income Countries. Northampton, MA: Edward Elgar.</mixed-citation>
         </ref>
         <ref id="d1478e725a1310">
            <mixed-citation id="d1478e729" publication-type="other">
Markides, K. S. and J. Coreil. 1986. "The health of Hispanics in the southwestern United States:
An epidemiologic paradox," Public Health Reports 101(3): 253-265.</mixed-citation>
         </ref>
         <ref id="d1478e739a1310">
            <mixed-citation id="d1478e743" publication-type="other">
McKenzie, David, John Gibson, and Steven Stillman. 2006. "How important is selection?
Experimental versus non-experimental measures of the income gains from migration,"
Policy Research Working Paper 3096. Washington, DC: World Bank.</mixed-citation>
         </ref>
         <ref id="d1478e756a1310">
            <mixed-citation id="d1478e760" publication-type="other">
Mishra, Prachi. 2007. "Emigration and wages in source countries: Evidence from Mexico,"
Journal of Development Economics 82 (1) : 180-199.</mixed-citation>
         </ref>
         <ref id="d1478e770a1310">
            <mixed-citation id="d1478e774" publication-type="other">
OECD. 2007. OECD Fadbook 2007: Economic, Environmental and Social Statistics. Paris: Organisation
for Economic Co-operation and Development, «http://www.sourceoecd.org», accessed
1 August 2008.</mixed-citation>
         </ref>
         <ref id="d1478e788a1310">
            <mixed-citation id="d1478e792" publication-type="other">
O'Rourke, Kevin H. 1994. "The economic impact of the famine in the short and long run,"
American Economic Review 84(2) : 309-313.</mixed-citation>
         </ref>
         <ref id="d1478e802a1310">
            <mixed-citation id="d1478e806" publication-type="other">
Ottaviano, Gianmarco I. P. and Giovanni Peri. 2008. "Immigration and national wages: Clari-
fying the theory and empirics," Working Paper, Department of Economics. Davis, CA:
University of California at Davis.</mixed-citation>
         </ref>
         <ref id="d1478e819a1310">
            <mixed-citation id="d1478e823" publication-type="other">
Parsons, Christopher R., Ronald Skeldon, Terrie L. Walmsley, and L. Alan Winters. 2007.
"Quantifying international migration: A database of bilateral migrant stocks," in Çağlar
Özden and Maurice Schiff (eds.), International Migration, Economic Development, and Policy.
Washington, DC: World Bank, pp. 17-58.</mixed-citation>
         </ref>
         <ref id="d1478e839a1310">
            <mixed-citation id="d1478e843" publication-type="other">
Pritchett, Lant. 2001. "Where has all the education gone?," World Bank Economic Review 15 (3):
367-391.</mixed-citation>
         </ref>
         <ref id="d1478e853a1310">
            <mixed-citation id="d1478e857" publication-type="other">
-. 2006a. "Who is not poor? Dreaming of a world truly free of poverty," World Bank
Research Observer 21(1): 1-23.</mixed-citation>
         </ref>
         <ref id="d1478e867a1310">
            <mixed-citation id="d1478e871" publication-type="other">
-. 2006b. "Does learning to add up add up? The returns to schooling in aggregate data,"
in Erik Hanushek and F. Welch (eds.), Handbook of Education Economics. New York: North-
Holland.</mixed-citation>
         </ref>
         <ref id="d1478e885a1310">
            <mixed-citation id="d1478e889" publication-type="other">
-. 2006c. Let Their People Come. Washington, DC: Center for Global Development.</mixed-citation>
         </ref>
         <ref id="d1478e896a1310">
            <mixed-citation id="d1478e900" publication-type="other">
Ray, Joel G., Marian J. Vermeulen, Michael J. Schull, Gita Singh, Rajiv Shah, and Donald A.
Redelmeier. 2007. "Results of the Recent Immigrant Pregnancy and Perinatal Long-term
Evaluation Study (RIPPLES)/ Canadian Medical Association Journal 176(10): 1419-1426.</mixed-citation>
         </ref>
         <ref id="d1478e913a1310">
            <mixed-citation id="d1478e917" publication-type="other">
Saxenian, Annalee. 2002. "Transnational communities and the evolution of global production
networks: Taiwan, China and India," Industry and Innovation 9(3): 183-202.</mixed-citation>
         </ref>
         <ref id="d1478e927a1310">
            <mixed-citation id="d1478e931" publication-type="other">
Scott, James C. 1998. Seeing Like a State: How Certain Schemes to Improve the Human Condition Have
Failed. New Haven, CT: Yale University Press.</mixed-citation>
         </ref>
         <ref id="d1478e941a1310">
            <mixed-citation id="d1478e945" publication-type="other">
Singh, Gopal K. and Stella M. Yu. 1996. "Adverse pregnancy outcomes: Difference between
US- and foreign-born women in major US racial and ethnic groups," American Journal of
Public Health 86(6): 837-843.</mixed-citation>
         </ref>
         <ref id="d1478e958a1310">
            <mixed-citation id="d1478e962" publication-type="other">
Taylor, J. Edward and Alejandro López-Feldman. 2007. "Does migration make rural house-
holds more productive? Evidence from Mexico," ESA Working Paper 07-10, Agricultural
Development Economics Division. Rome: The Food and Agriculture Organization of the
United Nations.</mixed-citation>
         </ref>
         <ref id="d1478e979a1310">
            <mixed-citation id="d1478e983" publication-type="other">
UNDP. 2007. Human Development Report 2006. New York: United Nations.</mixed-citation>
         </ref>
         <ref id="d1478e990a1310">
            <mixed-citation id="d1478e994" publication-type="other">
US Census Bureau. 2007. Historical Income Inequality Tables—Selected Measures of Household In-
come Dispersion: 1967-2005, «http://www.census.gov/hhes/www/income/histinc/ineqtoc.
html», accessed 12 November 2007.</mixed-citation>
         </ref>
         <ref id="d1478e1007a1310">
            <mixed-citation id="d1478e1011" publication-type="other">
World Bank. 2007. World Development Indicators 2007. Washington, DC: World Bank.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
