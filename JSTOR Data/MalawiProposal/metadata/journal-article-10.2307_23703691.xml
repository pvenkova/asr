<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">culturesconflits</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50009870</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Cultures et Conflits</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>L'Harmattan</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">1157996X</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">17775345</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">23703691</article-id>
         <title-group>
            <article-title>Entre survivance des ONG et mise en mouvement : pratiques et débats des féminismes nicaraguayens à l'heure de la globalisation du genre</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Delphine</given-names>
                  <surname>LACOMBE</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>10</month>
            <year>2011</year>
         </pub-date>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">83</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i23701742</issue-id>
         <fpage>15</fpage>
         <lpage>37</lpage>
         <permissions>
            <copyright-statement>© L'Harmattan décembre 2011</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/23703691"/>
         <abstract>
            <p>L'article propose de déconstruire un angle de vue dichotomique sur l'action collective féministe latino-américaine et caribéenne, selon lequel cette dernière serait divisée en deux courants : l'un autonome-libertaire refusant de s'inscrire dans les politiques onusiennes et de développement sur le genre, l'autre institutionnalisé, constitué autour d'ONG, mobilisant les financements des bailleurs de fonds internationaux pour mettre en œuvre des programmes à l'adresse des femmes pauvres. Selon le courant autonome, ≪ l'ONGisation ≫ serait facteur de dépolitisation du féminisme et de détournement de ses visées subversives. Or, il s'agit de montrer, à l'appui d'une enquête menée au Nicaragua sur les débats et pratiques des féminismes post-sandinistes (1990-2010), que la militance institutionnalisée œuvre plutôt dans un continuum fragile entre politique de résistance féministe et adaptabilités stratégiques aux ressources de l'aide au développement. La construction de marges de manœuvre et d'actions autonomes et les débats qu'elle alimente se posent en contrepoint des impasses politiques nationales. Elle témoigne d'une capacité réelle, quoique sous contraintes nationales et internationales, à arbitrer des tensions entre survivance des ONG et mise en mouvement féministe. This article will attempt to deconstruct the dichotomous perspective on LatinAmerican and Caribbean feminist collective action, that presents it as divided into two streams: a libertarian autonomous one refusing to be part of the United Nations and development gender policies, and the other institutionalized, organized around NGOs, and mobilizing funding from international donors in order to implement programs directed toward poor women. According to the autonomist stream, this "NGO-ization" is a factor in the depoliticization of feminism and in the diversion of its subversive aims. Yet, we propose to demonstrate, based on fieldwork conducted in Nicaragua on post-sandinist (1990-2010) feminism practices and debates, that the institutionalized militancy actually operates in a fragile continuum between feminist resistance politics and strategic adaptabilities to development aid resources. The establishment of maneuvering spaces for autonomous actions and the debates that this fosters, arises as a counterpoint to national political deadlocks. This testifies to a real capacity, albeit under national and international constraints, to shelter the tensions between NGO survival and feminist mobilization.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>fre</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d1086e114a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1086e121" publication-type="other">
Sternbach Saporta N., Navarro-Aranguren M., Chuchryk P., Alvarez. E. S., "Feminisms
in Latin America: From Bogota to San Bernardo", in Escobar A. et Alvarez. E. S., The
Making of Social Movements in Latin America: Identity, Strategy, and Democracy, Boulder,
CO, Westview Press, 1992, pp. 205-239.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1086e136" publication-type="other">
Falquet J., « Panorama du mouvement après la
Sixième Rencontre féministe latino-américaine et des Caraïbes, novembre 1993 », Cahiers du
GEDISST, n°9-10, 1994, pp. 133-146.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1086e148" publication-type="other">
Falquet J., « De l'institutionnalisation du féminisme
latino-américain et des Caraïbes », Cahiers du GEDISST, n°20,1998, pp. 131-147.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1086e158" publication-type="other">
Falquet J.,
« Un mouvement désorienté, la 8l Rencontre féministe latino-américaine et des Caraïbes »,
Nouvelles Questions Féministes, « Féminismes d'Amérique latine et des Caraïbes », 20, 3,
1999, pp. 5-38.</mixed-citation>
            </p>
         </fn>
         <fn id="d1086e174a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1086e181" publication-type="other">
Debate Feminista, año 8, vol. 15, avril 1997.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1086e187" publication-type="other">
Gargallo F., dans « Féminismes multiples : pratiques
et idées féministes en Amérique Latine » in Verschuur C. (éd.), «Genre, postcolonialisme et
diversité des mouvements de femmes», Cahiers Genre et Développement, n°7, pp. 277-295,
Paris, L'Harmattan.</mixed-citation>
            </p>
         </fn>
         <fn id="d1086e203a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1086e210" publication-type="other">
Curiel O., « El 7mo encuentro lésbico feminista : trascendente e histórico », Bogota, 14
février 2007, en ligne : http://www.feministastramando.cl/
index.php?option=com_content&amp;task=view&amp;id=444&amp;Itemid=62</mixed-citation>
            </p>
         </fn>
         <fn id="d1086e223a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1086e232" publication-type="other">
J. Falquet a sélectionnées pour
un article à paraître dans Recherches Féministes, « Pour un autre imaginaire politique : vingt
ans de réflexions et de pratiques "féministes autonomes" latino-américaines et caribéennes »
issues d'un texte de Mujeres Creando intitulé « Dignité et Autonomie » (1994, pp. 46-51).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1086e247" publication-type="other">
Curiel O., Falquet J., Masson S.
(éd.), « Féminismes dissidents en Amérique Latine et aux Caraïbes », Nouvelles Questions
Féministes, 24,2,2005.</mixed-citation>
            </p>
         </fn>
         <fn id="d1086e261a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d1086e268" publication-type="other">
Stoffel S., « L'institutionnalisation au service de
l'autonomie des féministes chiliennes », Recherches Féministes, vol.20, n°2, 2007, pp. 37-59.</mixed-citation>
            </p>
         </fn>
         <fn id="d1086e278a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d1086e285" publication-type="other">
Falquet J., De gré ou de force, Les femmes dans la mondialisation, Paris,
La Dispute, le Genre du Monde, 2008.</mixed-citation>
            </p>
         </fn>
         <fn id="d1086e295a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d1086e302" publication-type="other">
Amalia Fischer dans le numéro de Nouvelles
Questions Féministes</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1086e311" publication-type="other">
Fischer A., « Les chemins complexes de
l'autonomie », Nouvelles Questions Féministes, op. cit., pp. 65-85.</mixed-citation>
            </p>
         </fn>
         <fn id="d1086e321a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d1086e328" publication-type="other">
de Falquet, Curiel et Masson dans Nouvelles Questions Féministes, op. cit.</mixed-citation>
            </p>
         </fn>
         <fn id="d1086e335a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d1086e342" publication-type="other">
Morgan M., "Founding Mothers, Women's Voices and Stories in
the 1987 Nicaraguan Constitution", Boston University Law Review, 70(1), 1990, p. 38.</mixed-citation>
            </p>
         </fn>
         <fn id="d1086e352a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d1086e359" publication-type="other">
Molyneux M., "Mobilization without
Emancipation? Women's Interests, The State and Revolution", in Fagan R., Deere C. D.,
Coraggio J. L. (eds.), Transition and Development: Problems of Third World Socialism,
Berkeley, Monthly Review Press and Center for Study of the Americas, 1986, pp. 280-302.</mixed-citation>
            </p>
         </fn>
         <fn id="d1086e376a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d1086e383" publication-type="other">
Murguialday C., Nicaragua Revolución feminismo (1977-1990), Editorial Revolución.
Madrid, 1990.</mixed-citation>
            </p>
         </fn>
         <fn id="d1086e393a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d1086e400" publication-type="other">
Isbester K., Still Fighting, The Nicaraguan Women's Movement, 1977-
2000, Pittsburgh, University of Pittsburgh Press, 2000.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1086e409" publication-type="other">
Molyneux M., "Family Reform in
socialist states: the hidden agendas", Feminist Review, 21, 1985, pp. 47-66.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1086e418" publication-type="other">
Randall M., Las
hijas de Sandino, una historia abierta, Managua, Nicaragua, Ediciones Centroamericanas,
1999.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1086e431" publication-type="other">
Collinson, H. (ed.) Women and Revolution in Nicaragua, New Jersey, Zed books,
1990.</mixed-citation>
            </p>
         </fn>
         <fn id="d1086e441a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d1086e448" publication-type="other">
Serra V., Héctor L., La
Sociedad Civil Nicaragüense, sus organizaciones y sus relaciones con el Estado, Centro de
Análisis Socio-Cultural, Universidad Centroamericana, 2007, p. 45.</mixed-citation>
            </p>
         </fn>
         <fn id="d1086e461a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d1086e468" publication-type="other">
Bataillon G., « Nicaragua : La présidence Chamorro, L'instauration d'un régime démocra-
tique désenchanté », Problèmes d'Amérique Latine, n" 30, juillet-sept 1998, pp. 71-92.</mixed-citation>
            </p>
         </fn>
         <fn id="d1086e478a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d1086e485" publication-type="other">
Serra V., op. cit., p. 45</mixed-citation>
            </p>
         </fn>
         <fn id="d1086e492a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d1086e499" publication-type="other">
Varela BH
1998, "Las ONG en Nicaragua : limitaciones y tendencias en su relación con la cooperación
internacional" Managua FONG-INIES-CON-SNV cité dans Serra Vásquez, ibid., p. 44.</mixed-citation>
            </p>
         </fn>
         <fn id="d1086e513a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d1086e520" publication-type="other">
CAPRI, 2004, Centro de Apoyo a Programas y Proyectos, Directorio ONG de Nicaragua.</mixed-citation>
            </p>
         </fn>
         <fn id="d1086e527a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d1086e534" publication-type="other">
Gosparini et alii, Evaluación
conjunta del apoyo presupuestario general 1994-2004, Burkina Faso, Malawi, Mozambique,
Nicaragua, Ruanda, Uganda, Vietnam, Informe de País Nicaragua, mayo 2006, International
Development Department School of Public Policy University of Birmingham.</mixed-citation>
            </p>
         </fn>
         <fn id="d1086e550a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d1086e557" publication-type="other">
Serra V., op. cit., p. 84.</mixed-citation>
            </p>
         </fn>
         <fn id="d1086e564a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d1086e571" publication-type="other">
Montenegro S. et alii., Movimiento de mujeres en Centroamérica, Managua, Centro Editorial
de la Mujer (CEM), 1997.</mixed-citation>
            </p>
         </fn>
         <fn id="d1086e581a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d1086e588" publication-type="other">
Sonia Alvarez : "Latin
American feminisms 'go global': trends of the 1990s and challenges for the new millenium" in
Alvarez S., Dagnino E., Escobar A. (eds), Cultures of Politics/Politics of Culture: Revisioning
Latin American Social Movements, Boulder, Coo., Westview Press, 1998, pp. 293-324.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1086e603" publication-type="other">
"Advocating Feminism: The Latin American feminist NGO 'boom'", International Feminist
Journal of Politics, 1(2).1999, pp. 181-209</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1086e612" publication-type="other">
"Beyond NGO-ization? Reflections from Latin
America", Development, 52(2), 2009, pp. 175-184.</mixed-citation>
            </p>
         </fn>
         <fn id="d1086e622a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d1086e629" publication-type="other">
Encuentro Nacional de Mujeres Por la unidad en la diversidad, 1992, Managua, Centro
Editorial de la Mujer, CEM.</mixed-citation>
            </p>
         </fn>
         <fn id="d1086e640a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d1086e647" publication-type="other">
Murdock D., When Women Have Wings: Feminism and Development in
Medellin, Colombia, Ann Arbor, MI, University of Michigan Press, 2008</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1086e656" publication-type="other">
Alvarez,
S., op. cit., 2009, p.177.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1086e665" publication-type="other">
McCarthy et Zald, McCarthy J.D., Zald M. "Resource Mobilization
and Social Movements: a Partial Theory", American Journal of Sociology, vol. 82, 1977,
p. 1212-1241.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1086e678" publication-type="other">
Hély M., Les métamorphoses du monde associatif, Paris, PUF, 2009.</mixed-citation>
            </p>
         </fn>
         <fn id="d1086e685a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d1086e692" publication-type="other">
Keck M., Sikkink K., (eds), Activists Beyond Borders: Advocacy Networks in International
Politics, Ithaca, NY, Cornell University Press, 1998</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1086e701" publication-type="other">
Joachim J., "Shaping the Human
Rights Agenda: the case of violence against women" in Meyer M., Priigl E., (eds), Gender
Politics in Global Governance, Oxford, Rowman &amp; Littlefield Publishers, 1999 pp. 142-160.</mixed-citation>
            </p>
         </fn>
         <fn id="d1086e714a1310">
            <label>31</label>
            <p>
               <mixed-citation id="d1086e721" publication-type="other">
http://www.cidh.oas.org/Basicos/French/rn.femme.htm</mixed-citation>
            </p>
         </fn>
         <fn id="d1086e728a1310">
            <label>32</label>
            <p>
               <mixed-citation id="d1086e735" publication-type="other">
Nadine Jubb :
"Gender, Funding, and the Social Order: Contradictions among the State, the Women's
Movement, and Donors regarding the Nicaragua Women's and Children's Police Stations",
Prepared for delivery at the 2006 congress of the Canadian Political Science Association,
Toronto, June 1-3. 2006.</mixed-citation>
            </p>
         </fn>
         <fn id="d1086e754a1310">
            <label>34</label>
            <p>
               <mixed-citation id="d1086e761" publication-type="other">
Jubb, N.,
op. cit.</mixed-citation>
            </p>
         </fn>
         <fn id="d1086e771a1310">
            <label>35</label>
            <p>
               <mixed-citation id="d1086e778" publication-type="other">
Ewig C., "The Strengths and Limits of the NGO Women's Movement Model: Shaping
Nicaragua's democratic institutions", Latin American Research Review, 34(3), 1999, pp. 75-
102.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1086e790" publication-type="other">
Delia
Porta D., Diani M., Social Movements, an Introduction, Oxford, Blackwell Publishing, 1999,
p.16.</mixed-citation>
            </p>
         </fn>
         <fn id="d1086e804a1310">
            <label>37</label>
            <p>
               <mixed-citation id="d1086e811" publication-type="other">
Lacombe D., « L'affaire Zoilamérica Narvâez contre Daniel Ortega ou la caducité de
"l'homme nouveau" », Problèmes d'Amérique Latine, n° 73, été 2009, pp. 73-100.</mixed-citation>
            </p>
         </fn>
         <fn id="d1086e821a1310">
            <label>39</label>
            <p>
               <mixed-citation id="d1086e828" publication-type="other">
Molyneux M., op. cit.</mixed-citation>
            </p>
         </fn>
         <fn id="d1086e835a1310">
            <label>40</label>
            <p>
               <mixed-citation id="d1086e842" publication-type="other">
In Feminismo y globalización: Apuntes para un análisis político desde el Movimiento,
[Convención Feminista, volver al escándalo y la transgresión, Por una agenda propia y autó-
noma Comité Nacional Feminista, 2003, p.50.</mixed-citation>
            </p>
         </fn>
         <fn id="d1086e855a1310">
            <label>42</label>
            <p>
               <mixed-citation id="d1086e862" publication-type="other">
Kampwirth K., "Amoldo Alemán takes on the NGO's:
Antifeminism and the new populism in Nicaragua", Latin American Politics and Society,
summer 2003.</mixed-citation>
            </p>
         </fn>
         <fn id="d1086e875a1310">
            <label>44</label>
            <p>
               <mixed-citation id="d1086e882" publication-type="other">
Sophie Stoffel,
op. cit.</mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
