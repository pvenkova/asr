<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">demography</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100446</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Demography</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Springer</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00703370</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15337790</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">42920545</article-id>
         <title-group>
            <article-title>Family Instability and Early Initiation of Sexual Activity in Western Kenya</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Rachel E.</given-names>
                  <surname>Goldberg</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>4</month>
            <year>2013</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">50</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40112110</issue-id>
         <fpage>725</fpage>
         <lpage>750</lpage>
         <permissions>
            <copyright-statement>© 2013 Population Association of America</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:href="http://dx.doi.org/10.1007/s13524-012-0150-8"
                   xlink:title="an external site"/>
         <abstract>
            <p>Epidemiological, economic, and social forces have produced high levels of volatility in family and household structure for young people growing up in sub-Saharan Africa in recent decades. However, scholarship on the family to date has not examined the influence of this family instability on young people's well-being. The current study employs unique life history calendar data from Western Kenya to investigate the relationship between instability in caregiving and early initiation of sexual activity. It draws on a body of work on parental union instability in the United States, and examines new dimensions of family change. Analyses reveal a positive association between transitions in primary caregiver and the likelihood of early sexual debut that is rapidly manifested following caregiver change and persists for a short period. The association is strongest at early ages, and there is a cumulative effect of multiple caregiver changes. The results highlight the importance of studying family stability in sub-Saharan Africa, as distinct from family structure, and for attention to dimensions such as age and recency.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d46e189a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d46e196" publication-type="other">
UN AIDS, UNICEF, and US AID 2004</mixed-citation>
            </p>
         </fn>
         <fn id="d46e203a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d46e210" publication-type="other">
Kisumu in the year 2007,</mixed-citation>
            </p>
         </fn>
         <fn id="d46e217a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d46e224" publication-type="other">
Mberu 2006</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d46e230" publication-type="other">
Osborne and McLanahan 2007;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d46e236" publication-type="other">
Wu 1996</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d46e252a1310">
            <mixed-citation id="d46e256" publication-type="other">
Ainsworth, M., Beegle, K., &amp; Koda, G. (2005). The impact of adult mortality and parental deaths on
primary schooling in North-Western Tanzania. Journal of Development Studies, 41, 412-439.</mixed-citation>
         </ref>
         <ref id="d46e266a1310">
            <mixed-citation id="d46e270" publication-type="other">
Ainsworth, M., &amp; Filmer, D. (2002). Inequalities in children's schooling: AIDS, orphanhood, poverty, and
gender. World Development, 34, 1099-1128.</mixed-citation>
         </ref>
         <ref id="d46e280a1310">
            <mixed-citation id="d46e284" publication-type="other">
Albrecht, C., &amp; Teachman, J. D. (2003). Childhood living arrangements and the risk of premarital
intercourse. Journal of Family Issues, 24, 867-894.</mixed-citation>
         </ref>
         <ref id="d46e294a1310">
            <mixed-citation id="d46e298" publication-type="other">
Ansell, N., &amp; van Blerk, L. (2004). Children's migration as a household/family strategy: Coping with AIDS
in Lesotho and Malawi. Journal of Southern African Studies, 30, 673-690.</mixed-citation>
         </ref>
         <ref id="d46e309a1310">
            <mixed-citation id="d46e313" publication-type="other">
Atwani Akwara, P., Noubary, B., Lim Ah Ken, P., Johnson, K., Yates, R., Winfrey, W., . . . Luo, C. (2010).
Who is the vulnerable child? Using survey data to identify children at risk in the era of HIV and AIDS.
AIDS Care, 22, 1066-1085.</mixed-citation>
         </ref>
         <ref id="d46e326a1310">
            <mixed-citation id="d46e330" publication-type="other">
Atwine, B., Cantor-Graae, E., &amp; Bajunirwe, F. (2005). Psychological distress among AIDS orphans in rural
Uganda. Social Science &amp; Medicine, 61, 555-564.</mixed-citation>
         </ref>
         <ref id="d46e340a1310">
            <mixed-citation id="d46e344" publication-type="other">
Axinn, W. G., &amp; Pearce, L. D. (2006). Mixed method data collection strategies. New York: Cambridge
University Press.</mixed-citation>
         </ref>
         <ref id="d46e354a1310">
            <mixed-citation id="d46e358" publication-type="other">
Beegle, K., De Weerdt, J., &amp; Dercon, S. (2010). Orphanhood and human capital destruction: Is there
persistence into adulthood? Demography, 47, 163-180.</mixed-citation>
         </ref>
         <ref id="d46e368a1310">
            <mixed-citation id="d46e372" publication-type="other">
Biddlecom, A., Gregory, R., Lloyd, C. B., &amp; Mensch, B. S. (2008). Associations between premarital sex
and leaving school in four sub-Saharan African countries. Studies in Family Planning, 39, 337-350.</mixed-citation>
         </ref>
         <ref id="d46e382a1310">
            <mixed-citation id="d46e386" publication-type="other">
Birdthistle, I. J., Floyd, S., Machingur, A., Mudziwapasi, N., Gregson, S., &amp; Glynn, J. R. (2008). From
affected to infected? Orphanhood and HIV risk among female adolescents in urban Zimbabwe. AIDS,
22, 759-766.</mixed-citation>
         </ref>
         <ref id="d46e400a1310">
            <mixed-citation id="d46e404" publication-type="other">
Brauner-Otto, S. R., &amp; Axinn, W. G. (2010). Parental family experiences, the timing of first sex, and
contraception. Social Science Research, 39, 875-893.</mixed-citation>
         </ref>
         <ref id="d46e414a1310">
            <mixed-citation id="d46e418" publication-type="other">
Brockerhoff, M., &amp; Biddlecom, A. E. (1999). Migration, sexual behavior, and the risk of HIV in Kenya.
International Migration Review, 33, 833-856.</mixed-citation>
         </ref>
         <ref id="d46e428a1310">
            <mixed-citation id="d46e432" publication-type="other">
Brown, S. L. (2006). Family structure transitions and adolescent well-being. Demography, 43, 447-461.</mixed-citation>
         </ref>
         <ref id="d46e439a1310">
            <mixed-citation id="d46e443" publication-type="other">
Brown, S. L. (2010). Marriage and child well-being: Research and policy perspectives. Journal of
Marriage and Family, 72, 1059-1077.</mixed-citation>
         </ref>
         <ref id="d46e453a1310">
            <mixed-citation id="d46e457" publication-type="other">
Bulanda, R. E., &amp; Manning, W. D. (2008). Parental cohabitation experiences and adolescent behavioral
outcomes. Population Research and Policy Review, 27, 593-618.</mixed-citation>
         </ref>
         <ref id="d46e467a1310">
            <mixed-citation id="d46e471" publication-type="other">
Capaldi, D. M., Crosby, L., &amp; StoofanfŤler, M. (1996). Predicting the timing of first sexual intercourse for
at-risk adolescent males. Child Development, 67, 344-359.</mixed-citation>
         </ref>
         <ref id="d46e482a1310">
            <mixed-citation id="d46e486" publication-type="other">
Case, A., &amp; Ardington, C. (2006). The impact of parental death on school outcomes: Longitudinal evidence
from South Africa. Demography, 43, 401-420.</mixed-citation>
         </ref>
         <ref id="d46e496a1310">
            <mixed-citation id="d46e500" publication-type="other">
Case, A., Paxson, C., &amp; Ableidinger, J. (2004). Orphans in Africa: Parental death, poverty, and school
enrollment. Demography, 41, 483-508.</mixed-citation>
         </ref>
         <ref id="d46e510a1310">
            <mixed-citation id="d46e514" publication-type="other">
Cavanagh, S. E. (2008). Family structure history and adolescent adjustment. Journal of Family Issues, 29,
944-980.</mixed-citation>
         </ref>
         <ref id="d46e524a1310">
            <mixed-citation id="d46e528" publication-type="other">
Cavanagh, S. E., Crissey, S. R., &amp; Raley, R. K. (2008). Family structure history and adolescent romance.
Journal of Marriage and Family, 70, 698-714.</mixed-citation>
         </ref>
         <ref id="d46e538a1310">
            <mixed-citation id="d46e542" publication-type="other">
Cavanagh, S. E., &amp; Huston, A. C. (2006). Family instability and children's early problem behavior. Social
Forces, 85, 551-581.</mixed-citation>
         </ref>
         <ref id="d46e552a1310">
            <mixed-citation id="d46e556" publication-type="other">
Cavanagh, S. E., &amp; Huston, A. C. (2008). The timing of family instability and children's social develop-
ment. Journal of Marriage and Family, 70, 1258-1269.</mixed-citation>
         </ref>
         <ref id="d46e567a1310">
            <mixed-citation id="d46e571" publication-type="other">
Central Bureau of Statistics (CBS) [Kenya], Ministry of Health (MOH) [Kenya], and ORC Macro. (2004).
Kenya demographic and health survey 2003. CBS, MOH, and ORC Macro.</mixed-citation>
         </ref>
         <ref id="d46e581a1310">
            <mixed-citation id="d46e585" publication-type="other">
Chase-Lansdale, R L., &amp; Hetherington, E. M. (1990). The impact of divorce on life-span development:
Short and long-term effects. In R B. Baltes, D. L. Featherman, &amp; R. M. Lerner (Eds.), Life-span
development and behavior (vol. 10, pp. 107-151). Hillsdale, NJ: Lawrence Erlbaum.</mixed-citation>
         </ref>
         <ref id="d46e598a1310">
            <mixed-citation id="d46e602" publication-type="other">
Cherlin, A. J., Furstenberg, F. F., Chase-Lansdale, R L., Kiernan, K. E., Robins, R K., Morrison, D. R., . . .
Teitler, J. O. (1991). Longitudinal studies of effects of divorce on children in Great Britain and the
United States. Science, 252, 1386-1389.</mixed-citation>
         </ref>
         <ref id="d46e615a1310">
            <mixed-citation id="d46e619" publication-type="other">
Clark, S. C., Collinson, M. A., Kahn, K., Drullinger, K., &amp; Tollman, S. M. (2007). Returning home to die:
Circular labour migration and mortality in South Africa. Scandinavian Journal of Public Health, 35
(Suppl. 69), 35-44.</mixed-citation>
         </ref>
         <ref id="d46e632a1310">
            <mixed-citation id="d46e636" publication-type="other">
Curtis, S. L., &amp; Blanc, A. (1997). Determinants of contraceptive failure, switching, and discontinuation: An
analysis of DHS contraceptive histories (Demographic and Health Surveys Analytical Reports No. 6).
Calverton, MD: Macro International Inc.</mixed-citation>
         </ref>
         <ref id="d46e649a1310">
            <mixed-citation id="d46e653" publication-type="other">
Dinkelman, T., Lam, D., &amp; Leibbrandt, M. (2007). Household and community income, economic shocks,
and risky sexual behavior of young adults: Evidence from the Cape Area Panel Study 2002 and 2005.
AIDS, 27(Suppl. 7), S49-S56.</mixed-citation>
         </ref>
         <ref id="d46e667a1310">
            <mixed-citation id="d46e671" publication-type="other">
Dixon-Mueller, R. (2008). How young is "too young"? Comparative perspectives on adolescent sexual,
marital, and reproductive transitions. Studies in Family Planning, 39, 247-262.</mixed-citation>
         </ref>
         <ref id="d46e681a1310">
            <mixed-citation id="d46e685" publication-type="other">
Dodoo, F. N., Zulu, E. M., &amp; Ezeh, A. C. (2007). Urban-rural differences in the socioeconomic deprivation-
sexual behavior link in Kenya. Social Science &amp; Medicine, 64, 1019-1031.</mixed-citation>
         </ref>
         <ref id="d46e695a1310">
            <mixed-citation id="d46e699" publication-type="other">
Elder, G. H., Jr. (1994). Time, human agency, and social change: Perspectives on the life course. Social
Psychology Quarterly, 57, 4-15.</mixed-citation>
         </ref>
         <ref id="d46e709a1310">
            <mixed-citation id="d46e713" publication-type="other">
Evans, D. K., &amp; Miguel, E. (2007). Orphans and schooling in Africa: A longitudinal analysis. Demography,
44, 35-57.</mixed-citation>
         </ref>
         <ref id="d46e723a1310">
            <mixed-citation id="d46e727" publication-type="other">
Fomby, P., &amp; Cherlin, A. J. (2007). Family instability and child well-being. American Sociological Review,
72, 181-204.</mixed-citation>
         </ref>
         <ref id="d46e737a1310">
            <mixed-citation id="d46e741" publication-type="other">
Fomby, P., Mollborn, S., &amp; Sennott, C. A. (2010). Race/ethnic differences in effects of family instability on
adolescents' risk behavior. Journal of Marriage and Family, 72, 234-253.</mixed-citation>
         </ref>
         <ref id="d46e752a1310">
            <mixed-citation id="d46e756" publication-type="other">
Freedman, D., Thornton, A., Camburn, D., Alwin, D., &amp; Young-DeMarco, L. (1988). The life history
calendar: A technique for collecting retrospective data. Sociological Methodology, 18, 37-68.</mixed-citation>
         </ref>
         <ref id="d46e766a1310">
            <mixed-citation id="d46e770" publication-type="other">
George, L. (1993). Sociological perspectives on life transitions. Annual Review of Sociology, 19, 353-373.</mixed-citation>
         </ref>
         <ref id="d46e777a1310">
            <mixed-citation id="d46e781" publication-type="other">
Goldman, N., Moreno, L., &amp; Westoff, C. F. (1989). Collection of survey data on contraception: An
evaluation of an experiment in Peru. Studies in Family Planning, 20, 147-157.</mixed-citation>
         </ref>
         <ref id="d46e791a1310">
            <mixed-citation id="d46e795" publication-type="other">
Gregson, S., Nyamukapa, C. A., Garnett, G. P., Wambe, M., Lewis, J. J. C., Mason, P. R., . . . Anderson, R.
M. (2005). HIV infection and reproductive health in teenage women orphaned and made vulnerable by
AIDS in Zimbabwe. AIDS Care, 17, 785-794.</mixed-citation>
         </ref>
         <ref id="d46e808a1310">
            <mixed-citation id="d46e812" publication-type="other">
Hallett, T. B., Gregson, S., Lewis, J. J. C., Lopman, B. A., &amp; Garnett, G. P. (2007). Behaviour change in
generalised HIV epidemics: Impact of reducing cross-generational sex and delaying age at sexual
debut. Sexually Transmitted Infections, 83, i50-i54.</mixed-citation>
         </ref>
         <ref id="d46e825a1310">
            <mixed-citation id="d46e829" publication-type="other">
Hallman, K. (2005). Gendered socioeconomic conditions and HIV risk behaviors among young people in
South Africa. African Journal of AIDS Research, 4, 37-50.</mixed-citation>
         </ref>
         <ref id="d46e840a1310">
            <mixed-citation id="d46e844" publication-type="other">
Hao, L., &amp; Xie, G. (2002). The complexity and endogeneity of family structure in explaining children's
misbehavior. Social Science Research, 31, 1-28.</mixed-citation>
         </ref>
         <ref id="d46e854a1310">
            <mixed-citation id="d46e858" publication-type="other">
Harrison, A., Cleland, J., Gouws, E., &amp; Fröhlich, J. (2005). Early sexual debut among young men in rural
South Africa: Heightened vulnerability to sexual risk? Sexually Transmitted Infections, 81, 259-261.</mixed-citation>
         </ref>
         <ref id="d46e868a1310">
            <mixed-citation id="d46e872" publication-type="other">
Heard, H. E. (2007). Fathers, mothers, and family structure: Family trajectories, parent gender, and
adolescent schooling. Journal of Marriage and Family, 69, 435-450.</mixed-citation>
         </ref>
         <ref id="d46e882a1310">
            <mixed-citation id="d46e886" publication-type="other">
Hofferth, S. L., &amp; Goldscheider, F. (2010). Family structure and the transition to early parenthood.
Demography, 47, 415^37.</mixed-citation>
         </ref>
         <ref id="d46e896a1310">
            <mixed-citation id="d46e900" publication-type="other">
Hogan, D. P., &amp; Astone, N. M. (1986). The transition to adulthood. Annual Review of Sociology, 12, 109-
130.</mixed-citation>
         </ref>
         <ref id="d46e910a1310">
            <mixed-citation id="d46e914" publication-type="other">
Hosegood, V., Floyd, S., Marston, M., Hill, C., McGrath, N., Isingo, R., . . . Zaba, B. (2007). The effects of
high HIV prevalence on orphanhood and living arrangements of children in Malawi, Tanzania, and
South Africa. Population Studies, 61, 327-336.</mixed-citation>
         </ref>
         <ref id="d46e928a1310">
            <mixed-citation id="d46e932" publication-type="other">
Jukes, M., Simmons, S., &amp; Bundy, D. (2008). Education and vulnerability: The role of schools in protecting
young women and girls from HIV in Southern Africa. AIDS, 22(Suppl. 4), S41-S56.</mixed-citation>
         </ref>
         <ref id="d46e942a1310">
            <mixed-citation id="d46e946" publication-type="other">
Lichter, D. J. (2001). Marriage as public policy (Progressive Policy Institute (PPI) policy report, September).
Washington, DC: PPI.</mixed-citation>
         </ref>
         <ref id="d46e956a1310">
            <mixed-citation id="d46e960" publication-type="other">
Lloyd, C. B. (2007). The role of schools in promoting sexual and reproductive health among adolescents in
developing countries (Poverty, Gender, and Youth Working Paper No. 6). New York: Population
Council.</mixed-citation>
         </ref>
         <ref id="d46e973a1310">
            <mixed-citation id="d46e977" publication-type="other">
Lloyd, C. B., &amp; Blanc, A. K. (1996). Children's schooling in sub-Saharan Africa: The role of fathers,
mothers, and others. Population and Development Review, 22, 265-298.</mixed-citation>
         </ref>
         <ref id="d46e987a1310">
            <mixed-citation id="d46e991" publication-type="other">
Luke, N., Clark, S., &amp; Zulu, E. M. (2011). The relationship history calendar: Improving the scope and
quality of data on youth sexual behavior. Demography, 48, 1151-1176.</mixed-citation>
         </ref>
         <ref id="d46e1001a1310">
            <mixed-citation id="d46e1005" publication-type="other">
Luke, N., Xu, H., Mberu, B. U., &amp; Goldberg, R. E. (2012). Migration experience and premarital sexual
initiation in Urban Kenya: An event history analysis. Studies in Family Planning, 43, 115-126.</mixed-citation>
         </ref>
         <ref id="d46e1016a1310">
            <mixed-citation id="d46e1020" publication-type="other">
Madhavan, S. (2004). Fosterage patterns in the age of AIDS: Continuity and change. Social Science &amp;
Medicine, 58, 1443-1454.</mixed-citation>
         </ref>
         <ref id="d46e1030a1310">
            <mixed-citation id="d46e1034" publication-type="other">
Magnuson, K., &amp; Berger, L. M. (2009). Family structure states and transitions: Associations with children's
well-being during middle childhood. Journal of Marriage and Family, 71, 575-591.</mixed-citation>
         </ref>
         <ref id="d46e1044a1310">
            <mixed-citation id="d46e1048" publication-type="other">
Mberu, B. U. (2006). Internal migration and household living conditions in Ethiopia. Demographic
Research, 14, 509-540.</mixed-citation>
         </ref>
         <ref id="d46e1058a1310">
            <mixed-citation id="d46e1062" publication-type="other">
McGrath, N., Nyirenda, M., Hosegood, V., &amp; Newell, M.-L. (2009). Age at first sex in Rural South Africa.
Sexually Transmitted Infections, 85(Suppl. I), i49-i55.</mixed-citation>
         </ref>
         <ref id="d46e1072a1310">
            <mixed-citation id="d46e1076" publication-type="other">
Meadows, S. O., McLanahan, S. S., &amp; Brooks-Gunn, J. (2008). Stability and change in family structure and
maternal health trajectories. American Sociological Review, 73, 314-334.</mixed-citation>
         </ref>
         <ref id="d46e1086a1310">
            <mixed-citation id="d46e1090" publication-type="other">
Mensch, B. S., Clark, W. H., Lloyd, C. B., «fe Erulkar, A. S. (2001). Premarital sex, schoolgirl pregnancy,
and school quality in rural Kenya. Studies in Family Planning, 32, 285-301.</mixed-citation>
         </ref>
         <ref id="d46e1101a1310">
            <mixed-citation id="d46e1105" publication-type="other">
Monasch, R., &amp; Boerma, J. T. (2004). Orphanhood and childcare patterns in sub-Saharan Africa: An
analysis of national surveys from 40 countries. AIDS, 7#(Suppl.2), S55-S65.</mixed-citation>
         </ref>
         <ref id="d46e1115a1310">
            <mixed-citation id="d46e1119" publication-type="other">
Morrison, D. R., &amp; Cherlin, A. J. (1995). The divorce process and young children's wellbeing: A
prospective analysis. Journal of Marriage and Family, 57, 800-812.</mixed-citation>
         </ref>
         <ref id="d46e1129a1310">
            <mixed-citation id="d46e1133" publication-type="other">
National AIDS and STI Control Programme (NASCOP), Ministry of Health. (2009). Kenya AIDS indicator
survey 2007 : Final report. Retrieved from http://www.aidskenya.org/public_site/webroot/cache/arti-
cle/file/Official KAIS Report 20091.pdf</mixed-citation>
         </ref>
         <ref id="d46e1146a1310">
            <mixed-citation id="d46e1150" publication-type="other">
National Research Council and Institute of Medicine. (2005). Growing up global: The changing transitions
to adulthood in developing countries. Panel on transitions to adulthood in developing countries. In
C. B. Lloyd (Ed.), Committee on population and board on children, youth, and families. Division of
behavioral and social sciences and education. Washington, DC: The National Academies Press.</mixed-citation>
         </ref>
         <ref id="d46e1166a1310">
            <mixed-citation id="d46e1170" publication-type="other">
Ngom, P., Magadi, M. A., &amp; Omuor, T. (2003). Parental presence and adolescent reproductive health
among the Nairobi Urban Poor. Journal of Adolescent Health, 33, 369-377.</mixed-citation>
         </ref>
         <ref id="d46e1180a1310">
            <mixed-citation id="d46e1184" publication-type="other">
Nnko, S., Boerma, J. T. J., Urassa, M., Mwaluko, G., &amp; Zaba, B. (2004). Secretive females or swaggering
males? An assessment of the quality of sexual partnership reporting in Rural Tanzania. Social Science
&amp; Medicine, 59, 299-310.</mixed-citation>
         </ref>
         <ref id="d46e1198a1310">
            <mixed-citation id="d46e1202" publication-type="other">
Nyamukapa, C., &amp; Gregson, S. (2005). Extended family's and women's roles in safeguarding orphans'
education in AIDS-afflicted Rural Zimbabwe. Social Science &amp; Medicine, 60, 2155-2167.</mixed-citation>
         </ref>
         <ref id="d46e1212a1310">
            <mixed-citation id="d46e1216" publication-type="other">
Nyamukapa, C. A., Gregson, S., Lopman, B., Salto, S., Watts, H. J., Monasch, R., . . . Jukes, M. L. H.
(2008). HIV-associated orphanhood and children's psychosocial distress: Theoretical framework tested
with data from Zimbabwe. American Journal of Public Health, 98, 133-141.</mixed-citation>
         </ref>
         <ref id="d46e1229a1310">
            <mixed-citation id="d46e1233" publication-type="other">
Operario, D., Pettifor, A., Cluver, L., MacPhail, C., &amp; Rees, H. (2007). Prevalence of parental death among
young people in South Africa and risk for HIV infection. Journal of Acquired Immune Deficiency
Syndromes, 44, 93-98.</mixed-citation>
         </ref>
         <ref id="d46e1246a1310">
            <mixed-citation id="d46e1250" publication-type="other">
Orubuloye, I. O., Caldwell, J. C., &amp; Caldwell, P. (1997). Perceived male sexual needs and male sexual
behaviour in southwest Nigeria. Social Science &amp; Medicine, 44, 1195-1207.</mixed-citation>
         </ref>
         <ref id="d46e1260a1310">
            <mixed-citation id="d46e1264" publication-type="other">
Osborne, C., &amp; McLanahan, S. (2007). Partnership instability and child well-being. Journal of Marriage
and Family, 69, 1065-1083.</mixed-citation>
         </ref>
         <ref id="d46e1274a1310">
            <mixed-citation id="d46e1278" publication-type="other">
Palermo, T., &amp; Peterman, A. (2009). Are female orphans at risk for early marriage, early sexual debut, and
teen pregnancy? Evidence from Sub-Saharan Africa. Studies in Family Planning, 40, 101-112.</mixed-citation>
         </ref>
         <ref id="d46e1289a1310">
            <mixed-citation id="d46e1293" publication-type="other">
Pettifor, A. E., van der Straten, A., Dunbar, M. S., Shiboski, S. C., &amp; Padian, N. S. (2004). Early age at first
sex: A risk factor for HIV infection among women in Zimbabwe. AIDS, 18, 1435-1442.</mixed-citation>
         </ref>
         <ref id="d46e1303a1310">
            <mixed-citation id="d46e1307" publication-type="other">
Plummer, M. L., Ross, D. A., Wight, D., Changalucha, J., Mshana, G., Wamoyi, J., . . . Hayes, R. J. (2004).
"A bit more truthful": The validity of adolescent sexual behaviour data collected in rural Northern
Tanzania using five methods. Sexually Transmitted Infections, 80(Suppl. II), ii49-ii56.</mixed-citation>
         </ref>
         <ref id="d46e1320a1310">
            <mixed-citation id="d46e1324" publication-type="other">
Poulin, M. (2010). Reporting on first sexual experience: The importance of interviewer-respondent
interaction. Demographic Research, 22(article 11), 237-288. doi:10.4054/DemRes.20 10.22. 11</mixed-citation>
         </ref>
         <ref id="d46e1334a1310">
            <mixed-citation id="d46e1338" publication-type="other">
Reniers, G. (2008). Marital strategies for regulating exposure to HIV. Demography, 45, 417-438.</mixed-citation>
         </ref>
         <ref id="d46e1345a1310">
            <mixed-citation id="d46e1349" publication-type="other">
Rwenge, M. (2000). Sexual risk behaviors among young people in Bamenda, Cameroon. International
Family Planning Perspectives, 26, 118-123, 130.</mixed-citation>
         </ref>
         <ref id="d46e1359a1310">
            <mixed-citation id="d46e1363" publication-type="other">
Ryan, S., Franzetta, K., Schelar, E., &amp; Manlove, J. (2009). Family structure history: Links to relationship
formation behaviors in young adulthood. Journal of Marriage and Family, 71, 935-953.</mixed-citation>
         </ref>
         <ref id="d46e1374a1310">
            <mixed-citation id="d46e1378" publication-type="other">
Sambisa, W. C., &amp; Stokes, S. (2006). Rural/urban residence, migration, HIV/AIDS, and safe sex practices
among men in Zimbabwe. Rural Sociology, 71, 183-211.</mixed-citation>
         </ref>
         <ref id="d46e1388a1310">
            <mixed-citation id="d46e1392" publication-type="other">
Shanahan, M. J. (2000). Pathways to adulthood in changing societies: Variability and mechanisms in life
course perspective. Annual Review of Sociology, 26, 667-692.</mixed-citation>
         </ref>
         <ref id="d46e1402a1310">
            <mixed-citation id="d46e1406" publication-type="other">
South, S. J., Haynie, D. L., &amp; Bose, S. (2005). Residential mobility and the onset of adolescent sexual
activity. Journal of Marriage and Family, 67, 499-514.</mixed-citation>
         </ref>
         <ref id="d46e1416a1310">
            <mixed-citation id="d46e1420" publication-type="other">
Stack, S. (1994). The effect of geographic mobility on premarital sex. Journal of Marriage and the Family,
56, 204-208.</mixed-citation>
         </ref>
         <ref id="d46e1430a1310">
            <mixed-citation id="d46e1434" publication-type="other">
Sun, Y., &amp; Li, Y. (2002). Children's well-being during parents' marital disruption process: A pooled time-
series analysis. Journal of Marriage and Family, 64, 472-488.</mixed-citation>
         </ref>
         <ref id="d46e1444a1310">
            <mixed-citation id="d46e1448" publication-type="other">
Teachman, J. D. (2008). The living arrangements of children and their educational well-being. Journal of
Family Issues, 29, 734-761.</mixed-citation>
         </ref>
         <ref id="d46e1459a1310">
            <mixed-citation id="d46e1463" publication-type="other">
Townsend, N., Madhavan, S., Tollman, S., Garenne, M., &amp; Kahn, K. (2002). Children's residence patterns
and educational attainment in rural South Africa, 1997. Population Studies, 56, 215-225.</mixed-citation>
         </ref>
         <ref id="d46e1473a1310">
            <mixed-citation id="d46e1477" publication-type="other">
Uhlenberg, R, &amp; Mueller, M. (2003). Family context and individual well-being: Patterns and mechanisms
in life course perspective. In J. T. Mortimer &amp; M. J. Shanahan (Eds.), Handbook of the life course (pp.
123-148). New York: Kluwer Academic/Plenum Publishers.</mixed-citation>
         </ref>
         <ref id="d46e1490a1310">
            <mixed-citation id="d46e1494" publication-type="other">
UNAIDS. (2008). Report on the global AIDS epidemic. New York: UNAIDS.</mixed-citation>
         </ref>
         <ref id="d46e1501a1310">
            <mixed-citation id="d46e1505" publication-type="other">
UNAIDS, UNICEF, and USAID. (2004). Children on the brink 2004: A joint report of new orphan
estimates and a framework for action. New York: UNAIDS.</mixed-citation>
         </ref>
         <ref id="d46e1515a1310">
            <mixed-citation id="d46e1519" publication-type="other">
United Nations. (2007). World youth report 2007, young people's transition to adulthood: Progress and
challenges. New York: Department of Economic and Social Affairs, United Nations.</mixed-citation>
         </ref>
         <ref id="d46e1529a1310">
            <mixed-citation id="d46e1533" publication-type="other">
Varga, C. A. (2003). How gender roles influence sexual and reproductive health among South African
adolescents. Studies in Family Planning, 34, 160-172.</mixed-citation>
         </ref>
         <ref id="d46e1544a1310">
            <mixed-citation id="d46e1548" publication-type="other">
White, M. J., Muhidin, S., Andrzejewski, C., Tagoe, E., Knight, R., &amp; Reed, H. (2008). Urbanization and
fertility: An event-history analysis of coastal Ghana. Demography, 45, 803-816.</mixed-citation>
         </ref>
         <ref id="d46e1558a1310">
            <mixed-citation id="d46e1562" publication-type="other">
Wu, L. L. (1996). Effects of family instability, income, and income instability on the risk of a premarital
birth. American Sociological Review, 61, 386-406.</mixed-citation>
         </ref>
         <ref id="d46e1572a1310">
            <mixed-citation id="d46e1576" publication-type="other">
Wu, L. L., &amp; Thomson, E. (2001). Race differences in family experience and early sexual initiation:
Dynamic models of family structure and family change. Journal of Marriage and Family, 63, 682-696.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
