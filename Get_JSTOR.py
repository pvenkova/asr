import sys, os
#ElementTree is used for xml parsing
from lxml import etree as ET
from lxml import html as HT
#pandas is used for easy data analysis
import pandas as pd
#numpy is used for better arrays
import numpy as np
#Time for waiting
import time
#Glob for file listing
import glob
#unidecoed is for removing accents
import unidecode as UN
#Sqlalchemy for database management
from sqlalchemy import create_engine


def Get_JSTOR(email, tool, search_term, name):
    #Sets database name for JSTOR
    db = 'JSTOR'
    engine = create_engine('sqlite:///DB/papers_DB_'+ name + '.db')
    #previous_call = './' + tool + '_' + name + '_' + db + '.csv'

    #Checks if the process has already been done for some papers in prevous search
    #   if that is the case, stores in a dataFrame
    if engine.dialect.has_table(engine, db):
        print ('JSTOR Data was already processed. Stopping...')
        return
        
    #sets the location of the metadata files
    meta_dir = './JSTOR Data/' + name + '/metadata/'
    #sets the location of the full text dumps
    FT_dir = './JSTOR Data/' + name + '/ocr/'
    if not (os.path.isdir(meta_dir)): #Checks if the path exists
        print ('JSTOR Data not available. Stopping ...')
        return
        

    print ('Extracting DOI')
    DOI = []
    for files in glob.glob(meta_dir + '*.xml'):     #Checks for DOI in the meta, because FT not always available
        DOI.append("-".join(files.split('-')[2:]).split('.xml')[0])

    #for all extracted IDs
    #Extracts the data
    print ('Extracting the data for the set of the request')

    #Creates an empty dataframe to be filled
    Date, Journal, Title, Authors, Aff, Abs, MesH, FullT = [],[], [], [], [], [], [], []
    PMID, PMC_ID, URL = [], [], []
    for ID in DOI:
        print ('Getting DOI', ID)
 
        try:
            file_meta = glob.glob(meta_dir + '*' + ID +'.xml')[0]
        except IndexError:
            file_meta = 'boo.xml'
        try:
            file_FT = glob.glob(FT_dir + '*' + ID +'.txt')[0]
        except IndexError:
            #File does not exist
            FullT.append(np.nan)
            file_FT = 'boo.txt'
            
        #Extracts Full Text (easiest to do first)
        if os.path.isfile(file_FT):                     #The file containing the full text exists
            xml_doc = HT.parse(file_FT).getroot() #Parses the file as if it was a HTML (cases for some of them)
            full_text = ''
            for tag in xml_doc.findall('.//p'):        #The HTML like dumps have text inside the <p> tags
                if tag.text != None:
                    full_text = full_text + ' ' + tag.text
            if full_text != '' :
                FullT.append(UN.unidecode(full_text))
            else:
                #Might be the other type of file, plain text transformed in HTML in tags <page_sequence>
                for tag in xml_doc.findall('.//page'):
                    if tag.text != None:
                        full_text = full_text + ' ' + tag.text
                if full_text != '':
                    FullT.append(UN.unidecode(full_text))
                else:
                    FullT.append(np.nan)               

        #Gets the other data given in the metadat folder, true XML files
        try:
            xml_doc = ET.parse(file_meta).getroot()              
        except ET.XMLSyntaxError:
            xml_doc = ET.parse(file_meta,ET.XMLParser(recover=True)).getroot()
        except IOError:
            xml_doc = ET.fromstring("<root>blabla</root>")
            
        #Gets the date
        date = ''
        for tag in xml_doc.findall('.//pub-date//year'):
            date = tag.text
        for tag in xml_doc.findall('.//pub-date//month'):
            try:
                if len(tag.text) < 2:
                    date = date + '-0' + tag.text
                else:
                    date = date + '-' + tag.text
            except: #AttributeError:
                date = date + '-01'
        if date != '':
            Date.append(date)
        else:
            Date.append(np.nan)

        #Gets the journal name
        journal = ''
        for tag in xml_doc.findall('.//journal-title'):
            journal = journal + tag.text
        if journal != '':
            Journal.append(UN.unidecode(journal))
        else:
            Journal.append(np.nan)
            
        #Gets the title of the paper
        title = ''
        for tag in xml_doc.findall('.//article-title'):
            if tag.text != None:
                title = title + tag.text
        if title != '':
            Title.append(UN.unidecode(title))
        else:
            Title.append(np.nan)
            
        #Gets the  author list. Careful, the tags are not the sames for books and journal articles
        full_name = ''
        author = []
        if 'book' in file_meta:
            for tag in xml_doc.findall('.//contrib-group//contrib//name'):
                try:
                    full_name = tag.find('surname').text
                except AttributeError:
                    pass
                #Sometimes only the surname is given, nit the names
                try:
                    full_name = full_name + ' ' + tag.find('given-names').text
                except AttributeError:
                    pass
                except TypeError:
                    pass
                if full_name != '' and full_name != None:
                    author.append(UN.unidecode(full_name))
        elif 'journal' in file_meta:
            for tag in xml_doc.findall('.//contrib//string-name'):
                try:
                    full_name = tag.find('surname').text
                except AttributeError:
                    pass
                #Sometimes only the surname is given, nit the names
                try:
                    full_name = full_name + ' ' + tag.find('given-names').text
                except AttributeError:
                    pass
                except TypeError:
                    pass
                if full_name != '' and full_name != None:
                    author.append(UN.unidecode(full_name))
        if len(author) != 0:
            Authors.append(";".join(list(set(author))))
        else:
            Authors.append(np.nan)
            
        #Gets the affiliation
        aff = []
        for tag in xml_doc.findall('.//aff'):
            try:
                aff.append(UN.unidecode(tag.text))
            except:
                pass
        if len(aff) != 0:
            Aff.append(";".join(aff))
        else:
            Aff.append(np.nan)
            
        #Gets the abstract
        abstract = ''
        for tag in xml_doc.findall('.//abstract'):
            abstract = ' '.join(tag.itertext())
        if abstract != '':
            Abs.append(UN.unidecode(abstract))
        else:
            Abs.append(np.nan)


    #Makes the dataFrame (JSTOR does not provide Keywords, PaperityID)
    dict = {'Date':Date, 'DOI':DOI, 'Journal':Journal, \
            'Title':Title, 'Authors':Authors, 'Affiliation':Aff, \
            'Abstract':Abs, 'Full text':FullT}
    dataFrame = pd.DataFrame(dict)
    #Exports to CSV for later use
    #dataFrame.to_csv('./' + tool + '_' + name + '_' + db + '.csv')
    dataFrame.to_sql(db, engine, if_exists = 'replace', chunksize = 50)
    print ('Data acquired from the database')

    return
