
import sys, os
#urllib2 is used to simulate online files as local files
from urllib.request import urlopen
import requests
import json as JS
#pandas is used for easy data analysis
import pandas as pd
#numpy is used for better arrays
import numpy as np
#Time for waiting
import time
#unidecoed is for removing accents
import unidecode as UN
#SQLAlchemy is for database management
from sqlalchemy import create_engine
#Downloading PDF
from PDF_download import *

def Get_PAP(email, tool, search_term, name):
    #Sets database name for paperity
    engine = create_engine('sqlite:///DB/papers_DB_'+ name + '.db')
    db = 'PAP'
    #previous_call = './' + tool + '_' + name + '_' + db + '.csv'

    #Checks if the process has already been done for some papers in prevous search
    #   if that is the case, stores in a dataFrame
    if engine.dialect.has_table(engine, db):
        print ('Reading the previous call')
        query = 'SELECT * FROM '+ db +';'
        dataFrame_previous = pd.read_sql(query, engine)
        
    #sets the location of the API for the query
    api_query = 'http://paperity.org/api/1.0/search/'
    #sets the location of the API for the fetching
    api_fetch = 'http://paperity.org/api/1.0/p/'

    #creates the query to the DB to get the number of papers matching the
    #   search_term:    
    #   1)Creates the base query
    #   2)Parses the response to get the total number of pages and papers
    query_base = api_query + '?text=' + search_term
    print ('Sending request for ' + query_base)

    #gets the response to extract the total number of papers
    json_data = requests.get(query_base).json()
    #.split(',')
    total = json_data['total']
    item_per_page = json_data['items_per_page']
    time.sleep(1.1)
    #gets the maximum pages. Paperity does 20 per page, impossible to change
    max_page = int(total/item_per_page) + 1

    #Populates the PID (=paperity ID) list by either None or with the values from the
    #   previous data
    if engine.dialect.has_table(engine, db):
        PID_i = dataFrame_previous['PaperityID'].tolist()
        PID_i = [str(int(x)) for x in PID_i]
    else:
        PID_i = []
    PID = []

    #gets the papers ID from the search
    for i in range(1, max_page):
        print ('Page %s' % str(i))
        query_base = api_query + str(i) + '?text=' + \
                     search_term
        try:
            json_data = requests.get(query_base).json()
        except:
            pass
        for paper in json_data['papers']:
            #if the ID is not in the list already, then we add it to the list
            if (str(paper['pid']) not in PID_i):
                PID.append(paper['pid'])
        #Wait 1s before making anothe request, as demanded by paperity staff
        time.sleep(1.1)

    #Checks if there was more entries than previously. If not, then does nothing
    if engine.dialect.has_table(engine,db):
        if len(PID) == 0:
            print ('No new entry from Paperity')
            return
        else:
            print ('There are %s new entries' % str(len(PID)))

    #for all extracted IDs
    #Extracts the informations (COntrary to PMC et pUbmed, not in a spearate place)
    print ('Extracting the data for the set of the request')

    #Creates an empty dataframe to be filled
    Date, Journal, Title, Authors, Aff, Abs, MesH, FullT = [],[], [], [], [], [], [], []
    DOI, PMID, PMC_ID, URL, Fetched = [], [], [], [], []
    dump_in = 0
    for ID in PID:
        print ('Fetching ID', ID)
        fetch = api_fetch + str(ID)

        dump_in = dump_in + 1
        try:
            json_data = requests.get(fetch).json()
        except:
            json_data = JS.dumps({u'paper': {u'category':None, u'doi':None, \
                                  u'date':None, u'journal':{u'title':None},\
                                  u'title':None, u'authors_10':None, u'abstract':None, \
                                  u'keywords':None, u'url_pdf':None}}, sort_keys=True, \
                                separators=(',', ': '))
            json_data = JS.loads(json_data)
            
        #Gets the DOI, PMCID, PMID (the last two are not given)
        Fetched.append(ID)
        PMC_ID.append(np.nan)
        PMID.append(np.nan)
        if json_data['paper']['doi'] != None:
            DOI.append(json_data['paper']['doi'])
            file_name = json_data['paper']['doi'].replace('.', '_').replace('/', '_').replace('-', '_') 
        else:
            DOI.append(np.nan)
            file_name = 'doi_NA_PAP_' + str(ID)
        #Gets the date
        if json_data['paper']['date'] != None:
            date = json_data['paper']['date'].split('-')
            if len(date[1]) < 2:
                Date.append(date[0] + '-0' + date[1])
            else:
                Date.append(date[0] + '-' + date[1])
        else:
            Date.append(np.nan)
        #Gets the journal name
        if json_data['paper']['journal']['title'] != None:
            Journal.append(UN.unidecode(json_data['paper']['journal']['title']))
        else:
            Journal.append(np.nan)
        #Gets the title of the paper
        if json_data['paper']['title'] != None:
            Title.append(UN.unidecode(json_data['paper']['title']))
        else:
            Title.append(np.nan)
        #Gets the  author list
        if json_data['paper']['authors_10'] != None:
            temp = json_data['paper']['authors_10']
            author = []
            for items in temp:
                temp2 = items.split(' ')
                author.append(UN.unidecode(temp2[-1]+ ' ' +' '.join(temp2[:-1])))
            Authors.append(";".join(author))
        else:
            Authors.append(np.nan)
        #Affiliations are not given
        Aff.append(np.nan)
        #Gets the abstract
        if json_data['paper']['abstract'] != None:
            Abs.append(UN.unidecode(json_data['paper']['abstract']))
        else:
            Abs.append(np.nan)
        #Gets the keywords
        if (json_data['paper']['keywords'] != None) and (len(json_data['paper']['keywords']) != 0):
            MesH.append(";".join([UN.unidecode(x) for x in json_data['paper']['keywords']]))
        else:
            MesH.append(np.nan)
        #Gets the URL, might be useful
        if json_data['paper']['url_pdf'] != None:
            URL.append(json_data['paper']['url_pdf'])
            #There's no full text, so we're downloading it directly
            status, file_size = download_from_URL(json_data['paper']['url_pdf'], './DownloadedPDF/' + name + '/', file_name)
            #We have an actual file, so we store it, otherwise, do nothong
            if (file_size > 0):
                FullT.append(convert_PDF(file_name, name))
            else:
                FullT.append(np.nan)
        else:
            URL.append(np.nan)
            #No URL to download PDF so, appending NaN too
            FullT.append(np.nan)

        if dump_in >= int(50):
            print ("Dumping some data")
            #Dumps data as paperity reset connections randomly to not have to start all over agin
            dict = {'Date':Date, 'DOI':DOI, 'PMCID':PMC_ID, 'PMID':PMID, 'Journal':Journal, \
            'Title':Title, 'Authors':Authors, 'Affiliation':Aff, \
            'Abstract':Abs, 'Keywords': MesH, 'Full text':FullT, 'PaperityID':Fetched, 'URL':URL}
            
            dataFrame = pd.DataFrame(dict)
            #Exports to DB for later use
            dataFrame.to_sql(db, engine, if_exists='append', chunksize = 50)
            #Resets entries
            Date, Journal, Title, Authors, Aff, Abs, MesH, FullT = [], [], [], [], [], [], [], []
            DOI, PMID, PMC_ID, URL, Fetched = [], [], [], [], []
            dump_in = 0
        
        #Wait 1s before making another request, as demanded by paperity staff
        time.sleep(1.1)


    #Takes care of remaining data that was not dumped
    dict = {'Date':Date, 'DOI':DOI, 'PMCID':PMC_ID, 'PMID':PMID, 'Journal':Journal, \
            'Title':Title, 'Authors':Authors, 'Affiliation':Aff, \
            'Abstract':Abs, 'Keywords': MesH, 'Full text':FullT, 'PaperityID':Fetched, 'URL':URL}
    dataFrame = pd.DataFrame(dict)
    #Exports to DB for later use
    dataFrame.to_sql(db, engine, if_exists = 'append', chunksize = 50)
    print ('Data acquired from the database')

    return
